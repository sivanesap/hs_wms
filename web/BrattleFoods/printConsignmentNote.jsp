<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>

        <script>
 function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
</script>

    </head>
    <body >
        <form name="settle" method="post">
            <div id="printDiv">
            <table cellpadding="3" cellspacing="0" width="95%" border="0px" style="border-color: #fff3ef" align="center">
                <tr>
                 <td class="contenthead"  colspan="4">Consignment Note Type</td>
                </tr>
                <tr>
                    <td >Consignment Note </td>
                    <td >CN001</td>
                    <td >Consignment Note Date</td>
                    <td >01:11:2013</td>
                </tr>
                <tr>
                    <td >Customer Type</td>
                    <td >Contract </td>
                    <td >Product Category</td>
                    <td >Food Items </td>
                </tr>
            </table>
            <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Customer Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>                    
                    <td>
                <table width="100%">
                <tr>                    
                    <td>&nbsp;</td>                    
                    <td>Name</td>
                    <td>John Smith</td>
                    <td>&nbsp;</td>                    
                </tr>
                <tr>
                    
                    <td>&nbsp;</td>
                    <td >Code</td>
                    <td >10021</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Address</td>
                    <td >No.1/267, Block 1, Subhash </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td ></td>
                    <td >&nbsp;Nagar, New Delhi, DL 110027</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Pincode</td>
                    <td >110021</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Mobile</td>
                    <td>9845658978</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >E-Mail ID</td>
                    <td >JohnSmith@gmail.com</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="380"> Phone No</td>
                    <td>044-1611976</td>
                </tr>
                </table>
                </td>
                </tr>
            </table>
            <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Consignment Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Origin</td>
                    <td>Delhi</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Destination</td>
                    <td >Chandigarh</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Business </td>
                    <td >Primary</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td >Multi Pickup</td>
                    <td >Yes/No</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Multi Delivery</td>
                    <td>Yes/No</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
              <td width=350"> Special Instruction</td>
                    <td>044-1611976</td>
                </tr>
                
                </table>
                </td>
                </tr>
            </table>
            <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Vehicle (Required) Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Service Type</td>
                    <td>FTL</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Vehicle Type</td>
                    <td >Ashok Leyland/2516</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Reefer Required </td>
                    <td >Yes</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td >Vehicle Required Date</td>
                    <td >20:08:2013</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Vehicle Required Time</td>
                    <td>6:30 AM</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
              <td width=370">Special Instruction</td>
                    <td ></td>
                </tr>

                </table>
                </td>
                </tr>
            </table>
            <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Consignor Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Consignor Name</td>
                    <td>James</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Mobile No</td>
                    <td >9845124585</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width=350">Address </td>
                    <td >Delhi</td>
                </tr>

                

                </table>
                </td>
                </tr>
            </table>
            <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Consignee Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Consignee Name</td>
                    <td>rajul</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Mobile No</td>
                    <td >8978524755</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width=350">Address </td>
                    <td >Chandigarh</td>
                </tr>



                </table>
                </td>
                </tr>
            </table>
            <br><br>
            <table border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" >
                      
                        <tr >
                            <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" Width="100" >Starting Point</td>
                            <td class="contenthead" Width="100" >Type</td>
                            <td class="contenthead" Width="150" >St Address</td>
                            <td class="contenthead" Width="40" >St Date</td>
                            <td class="contenthead" Width="40" >St Time</td>
                            <td class="contenthead" Width="40" >Ending Point</td>
                            <td class="contenthead" Width="150" >End Address</td>
                            <td class="contenthead" Width="40" >End Date</td>
                            <td class="contenthead" Width="40" >End Time</td>
                            <td class="contenthead" Width="40" >Total Distance</td>
                            <td class="contenthead" Width="40" >Travel Hours</td>
                        </tr>
                    
                        <tr>
                            <td class="text1">1</td>
                            <td class="text1">Delhi</td>
                            <td class="text1">Pickup</td>
                            <td class="text1" width="90">Laxman logistics Pvt Ltd 15/1, Asaf All Road New Delhi 110002</td>
                            <td class="text1">1:11:2013</td>
                            <td class="text1">9.00 AM</td>
                            <td class="text1">Chandigarh</td>
                            <td class="text1"> Innovative Foods Ltd A-Block 1c Chakolas Habitat Cochin-13</td>
                            <td class="text1">01:11:2013</td>
                            <td class="text1">1:00 AM</td>
                            <td class="text1">248.8km</td>
                            <td class="text1">3hrs</td>
                        </tr>
                        <tr>
                            <td class="text1">2</td>
                            <td class="text1">Tamil Nadu</td>
                            <td class="text1">Pickup</td>
                            <td class="text1" width="90">Laxman logistics Pvt Ltd 15/1, Asaf All Road New Delhi 110002</td>
                            <td class="text1">02:05:2013</td>
                            <td class="text1">8.00 AM</td>
                            <td class="text1">Chandigarh</td>
                            <td class="text1"> India tubes Ltd A-Block 1c tamil Nadu-13</td>
                            <td class="text1">04:05:2013</td>
                            <td class="text1">8:00 AM</td>
                            <td class="text1">800.8km</td>
                            <td class="text1">10hrs</td>
                        </tr>
                        <tr>
                            <td class="text1">3</td>
                            <td class="text1">Chennai</td>
                            <td class="text1">Pickup</td>
                            <td class="text1" width="90">Tvs Pvt Ltd 15/1, Asaf All Road New Delhi 110002</td>
                            <td class="text1">1:11:2013</td>
                            <td class="text1">11:09 AM</td>
                            <td class="text1">Cochin</td>
                            <td class="text1">  Foods Ltd A-Block 1c Chakolas Habitat Cochin-13</td>
                            <td class="text1">02:11:2013</td>
                            <td class="text1">4:00 AM</td>
                            <td class="text1">1000.8km</td>
                            <td class="text1">21hrs</td>
                        </tr>
                        
                    </table>
            <br><br>

             <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Payment Details</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Billing Customer</td>
                    <td>Consignor</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Freight Charges</td>
                    <td >1500</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="380" >Service Tax Paid By </td>
                    <td >Transporter</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td >FOV Rate(%Invoice)</td>
                    <td >10%</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >FOV Charged</td>
                    <td>150</td>
                </tr>

                </table>
                </td>
                </tr>
            </table>
            <br><br>

             <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Additional Charges</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td>Document Charges</td>
                    <td>100</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >ODA Charges</td>
                    <td >1000</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="400" >Multi Pickup Charges </td>
                    <td >200</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td >Multi Delivery Charges</td>
                    <td >200</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Handling Charges</td>
                    <td>150</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Other Charges</td>
                    <td>150</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Fuel Surcharges</td>
                    <td >200</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Unloading Charges</td>
                    <td>200</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Loading Charges</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Sub Total</td>
                      <td >5000</td>
                </tr>

                </table>
                </td>
                </tr>
            </table>
            <br><br>

             <table  border="0"  align="center" width="95%" cellpadding="5" cellspacing="0" >
                <tr>
                    <td class="contenthead" colspan="3" >Applicable Charges</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                    <td >Service Tax Rate(%)</td>
                     <td >0.5%</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>

                    <td>&nbsp;</td>
                    <td >Educational Cess Rate(%)</td>
                            <td >150</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Higher Educational Cess Rate(%)</td>
                            <td >150</td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td >Higher Educational Cess Rate(%)</td>
                            <td >150</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                      <td >Educational Cess on Service Tax</td>
                            <td >120</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="400">Higher Educational Cess on Service Tax</td>
                            <td >500</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td >Service Tax Collected</td>
                            <td >600</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                     <td >Educational Cess Collected</td>
                            <td >1200</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                       <td >Higher Educational Cess Collected</td>
                            <td >120</td>
                </tr>

                </table>
                </td>
                </tr>
            </table>
        </div>
            <br>
            <br>
             <center>
                        <input align="center" type="button" onclick="print();" value = " Print "   />
                    </center>
        </form>
    </body>
    </html>