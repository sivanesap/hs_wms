<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


 <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>

    <body>
                 <form name="customerWise" method="post">
             <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:900;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Trip Settlement Report</li>
    </ul>
            <div id="first">
<table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td><font color="red">*</font>Trip Id</td>
                                        <td height="30">
                                            <input name="tripId" id="tripid" type="text" class="textbox" size="20" value="" autocomplete="off">
                                        </td>
                                        <td><font color="red">*</font>Route Name</td>
                                        <td height="30">
                                            <input name="customerName" id="customerName" type="text" class="textbox" size="20" value="" autocomplete="off">
                                        </td>


                                    </tr>
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td>&nbsp;</td>
                                        <td><input type="button" class="button" name="search" onClick="submitPage(this);" value="Search"></td>
                                    </tr>
                                </table>
            </div>
            </div>
    </td>
    </tr>
    </table>
            <br>
            <br>
            <h2>Trip Details</h2>
                                           <br>
             <table width="1270" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="100" class="contentsub">Trip Id</th>
                        <th width="150" class="contentsub">Trip Date</th>
                        <th width="130" class="contentsub">Vehicle No.</th>
                        <th width="130" class="contentsub">Route Name</th>
                        <th width="130" class="contentsub">Actual Km</th>
                        <th width="170" class="contentsub">Origin Pickup Date & Time</th>
                        <th width="170" class="contentsub">End Delivery Date & Time</th>
                        <th width="130" class="contentsub">Km OUT Start Point</th>
                        <th width="130" class="contentsub">Km IN End Point</th>
                        <th width="130" class="contentsub">Trip Status</th>
                        <th width="130" class="contentsub">Delivered Weight</th>
                        <th width="130" class="contentsub">Total Freight Amt</th>

                    </tr>
             </thead>
             
                <tbody>
                    <tr>
				<td>1</td>
                                <td><a href="/throttle/viewtripSettlment2.jsp"target="">T4237</a></td>
                                <td>12-03-2013</td>
                                <td>TN 17 AD 239</td>
                                <td>Chennai</td>
                                <td>570</td>
                                <td>10/09/2013</td>
                                <td>12/09/2013</td>
                                <td>100</td>
                                <td>123</td>
                                <td>100</td>
                                <td>1000</td>
                                <td>100</td>
			</tr>
                    <tr>
				<td>2</td>
                                <td><a href="/throttle/viewtripSettlment2.jsp"target="">T4135</a></td>
                                <td>12-03-2013</td>
                                <td>TN 17 AD 239</td>
                                <td>kerala</td>
                                <td>570</td>
                                <td>10/03/2013</td>
                                <td>12/04/2013</td>
                                <td>4</td>
                                <td>44</td>
                                <td>10450</td>
                                <td>123</td>
                                <td>123</td>
			</tr>
                    <tr>
				<td>3</td>
                                <td><a href="/throttle/viewtripSettlment2.jsp"target="">T4231</a></td>
                                <td>12-03-2013</td>
                                <td>TN 17 AD 239</td>
                                <td>Chennai</td>
                                <td>570</td>
                                <td>10/09/2013</td>
                                <td>12/03/2013</td>
                                <td>11</td>
                                <td>342</td>
                                <td>9888</td>
                                <td>677</td>
                                <td>676</td>
			</tr>
                    <tr>
				<td>4</td>
                                <td><a href="/throttle/viewtripSettlment2.jsp"target="">T4237</a></td>
                                <td>22-03-2013</td>
                                <td>TN 34 EED </td>
                                <td>Delhi</td>
                                <td>45670</td>
                                <td>20/01/2013</td>
                                <td>31/03/2013</td>
                                <td>200</td>
                                <td>113</td>
                                <td>123</td>
                                <td>124</td>
                                <td>2349</td>
			</tr>
                 
                  </tbody>
            </table>


        </form>
        
    </body>
</html>
