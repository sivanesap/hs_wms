<%-- 
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.* "%>
<%@ page import="java.text.* "%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "ConsignmentNotes_For_Trip_Planning-" + curDate + ".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>
    </head>
    <style type="text/css">
        .contentsub {
            padding:3px;
            height:24px;
            text-align:left;
            font-weight:bold;
            font-size:14px;
            background:#129fd4;
            color:#ffffff;
            background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
        }

    </style>
    <body>
        <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                        <td width="35" class="contentsub">Sno</td>
                        <td width="55" class="contentsub">CN No</td>
                        <td width="135" class="contentsub">Customer Name </td>
                        <td width="135" class="contentsub">Customer Code </td>
                        <td width="127" class="contentsub">Origin </td>
                        <td width="121" class="contentsub">Destination </td>
                        <td width="121" class="contentsub">Vehicle Type </td>
                        <td width="74" class="contentsub">Weight(MT)</td>
                        <td width="174" height="30" class="contentsub">Schedule Pickup Date &amp; Time </td>
                        <td width="174" height="30" class="contentsub">Estimated Delivery Date &amp; Time</td>
                        <td width="174" height="30" class="contentsub">Transit Days</td>
                        <td width="104" height="30" class="contentsub">Business Type </td>
                        <td width="134" height="30" class="contentsub">Service Type </td>
                    </tr>
                </thead>
                    <tr>
                        <td class="text1">1</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10012</a></td>
                        <td class="text1">M/S Sundaram & Co</td>
                        <td class="text1">BF001</td>
                        <td class="text1">Kashipur</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">TATA/3118</td>
                        <td class="text1">20</td>
                        <td class="text1">07/11/2013&amp;07:30AM</td>
                        <td class="text1">10/11/2013&amp;05:30PM</td>
                        <td class="text1">4.5Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">2</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10879</a></td>
                        <td class="text2">M/S John & Co</td>
                        <td class="text2">BF019</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Balwal</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">35</td>
                        <td class="text2">07/11/2011&amp;10:00AM</td>
                        <td class="text2">08/11/2011&amp;10:00AM</td>
                        <td class="text2" >1 Day</td>
                        <td class="text2" >Primary</td>
                        <td class="text2">FTL</td>
                    </tr>
                    <tr>
                        <td class="text1">3</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10457</a></td>
                        <td class="text1">M/S Balaji & Co</td>
                        <td class="text1">BF005</td>
                        <td class="text1">Balwal</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">AL/2516</td>
                        <td class="text1">25</td>
                        <td class="text1">07/11/2013&amp;04:00PM</td>
                        <td class="text1">09/11/2013&amp;04:00PM</td>
                        <td class="text1">2 Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10144</a></td>
                        <td class="text2" >M/S Prakash & Co</td>
                        <td class="text2">BF147</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Chennai</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">27</td>
                        <td class="text2">07/11/2013&amp;11:00AM</td>
                        <td class="text2">10/11/2013&amp;11:00AM</td>
                        <td class="text2">4 Days</td>
                        <td class="text2">Primary</td>
                        <td class="text2" >FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">CN/13-14/10879</a></td>
                        <td class="text2">M/S John & Co</td>
                        <td class="text2">BF019</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Balwal</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">35</td>
                        <td class="text2">07/11/2011&amp;10:00AM</td>
                        <td class="text2">08/11/2011&amp;10:00AM</td>
                        <td class="text2" >1 Day</td>
                        <td class="text2" >Primary</td>
                        <td class="text2">FTL</td>
                    </tr>

            </table>

    </form>
</body>
</html>
