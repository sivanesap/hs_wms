<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript">
function getfunction() {
    var errStr = "";
    if(document.tripEmailSettings.stakeHolderId.value==0){
            alert("Select Function should not be Empty");
             document.tripEmailSettings.stakeHolderId.focus();
            return;
        }
    if(errStr == "") {
        document.tripEmailSettings.action = '/throttle/getEmailFunction.do';
        document.tripEmailSettings.reqfor.value = 'getFunctions';
        document.tripEmailSettings.submit();
    }

}

function copyAddress(availableFunc)
{
var avilableFunction = document.getElementById("availableFunc");
var index = 0;
var selectedLength = 0;
if(avilableFunction.length != 0){
for (var i=0; i<avilableFunction.options.length ; i++) {
    if(avilableFunction.options[i].selected == true){
            selectedLength++;
    }
  }
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<avilableFunction.options.length ; i++) {
            if(avilableFunction.options[i].selected == true){

                    var optionCounter = document.tripEmailSettings.assignedfunc.length;
                    document.tripEmailSettings.assignedfunc.length = document.tripEmailSettings.assignedfunc.length +1;
                    document.tripEmailSettings.assignedfunc.options[optionCounter].text = avilableFunction.options[i].text ;
                    document.tripEmailSettings.assignedfunc.options[optionCounter].value =  avilableFunction.options[i].value;
                    avilableFunction.options[i] = null;
                   break;
            }
          }
}
/*if(availableFunc != ""){
var optionCounter = document.tripEmailSettings.assignedfunc.length;
document.tripEmailSettings.assignedfunc.length = document.tripEmailSettings.assignedfunc.length +1;
document.tripEmailSettings.assignedfunc.options[optionCounter].text = document.tripEmailSettings.availableFunc.options[document.tripEmailSettings.availableFunc.selectedIndex].text ;
document.tripEmailSettings.assignedfunc.options[optionCounter].value = document.tripEmailSettings.availableFunc.options[document.tripEmailSettings.availableFunc.selectedIndex].value ;
document.tripEmailSettings.availableFunc.options[document.tripEmailSettings.availableFunc.selectedIndex]=null;

}*/
}
else {
alert("Please Select any Value");
}
}


function copyAddress1(assRole)
{
//var value =
var selectedValue = document.getElementById('assignedfunc');
var index = 0;
var selectedLength = 0;

if(selectedValue.length != 0){
for (var i=0; i<selectedValue.options.length ; i++) {
    if(selectedValue.options[i].selected == true){
            selectedLength++;
    }
  }
//alert(selectedLength);
//alert(selectedValue.options.length);
for (var j=0; j<selectedLength ; j++) {
        for (var i=0; i<selectedValue.options.length ; i++) {
            if(selectedValue.options[i].selected == true){

                    var optionCounter = document.tripEmailSettings.availableFunc.length;
                    document.tripEmailSettings.availableFunc.length = document.tripEmailSettings.availableFunc.length +1;
                    document.tripEmailSettings.availableFunc.options[optionCounter].text = selectedValue.options[i].text ;
                    document.tripEmailSettings.availableFunc.options[optionCounter].value =  selectedValue.options[i].value;
                    selectedValue.options[i] = null;
                   break;
            }
          }
}




//document.tripEmailSettings.availableFunc.length = document.tripEmailSettings.availableFunc.length +1;
//document.tripEmailSettings.availableFunc.options[optionCounter].text = document.tripEmailSettings.assignedfunc.options[document.tripEmailSettings.assignedfunc.selectedIndex].text ;
//document.tripEmailSettings.availableFunc.options[optionCounter].value = document.tripEmailSettings.assignedfunc.options[document.tripEmailSettings.assignedfunc.selectedIndex].value ;
//document.tripEmailSettings.assignedfunc.options[document.tripEmailSettings.assignedfunc.selectedIndex] = null;

}
else {
alert("Please Select any Value");
}
}

function setRole() {
var roleId = '<%= request.getAttribute("stackHolderId") %>';
//alert("hi");
if(roleId != 'null') {
document.tripEmailSettings.stakeHolderId.value=roleId;

} else {
//
}
}
function submitPage() {

var length = document.tripEmailSettings.availableFunc.length;
var counter = document.tripEmailSettings.assignedfunc.length;
for(var j = 0; j < counter; j++){
document.tripEmailSettings.assignedfunc.options[j].selected = true;
}
var errStr = "";
    if(document.tripEmailSettings.stakeHolderId.value==0){
            alert("Select Function should not be Empty");
             document.tripEmailSettings.stakeHolderId.focus();
            return;
        }
if(errStr == "") {
document.tripEmailSettings.action='/throttle/insertEmailFunction.do';
document.tripEmailSettings.reqfor.value='assignFunctions';
document.tripEmailSettings.submit();
}
}

window.onload = setRole;
</script>
</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Trip Email" text="Trip Email"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Trip Email" text="Trip Email"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
<body>

<form name="tripEmailSettings" method="post">
<%--<%@ include file="/content/common/path.jsp" %>--%>

<!-- pointer table -->


<%@ include file="/content/common/message.jsp" %>
<!-- message table -->
<!-- copy there  end -->
<br>
<br>
<c:if test = "${holdersList != null}">
<table class="table table-info mb30 table-hover" id="bg"  >
    <thead><tr><th colspan="3">Trip Email</th></tr></thead>
<!--
<tr height="30">
    <td  colspan="4"><div align="center">Assign Role-Functions</div></td>
</tr>
-->
<tr>
<td>Trip Status</td>
    <td align="center" height="30">
<select name="stakeHolderId" class="form-control" style="width:250px;height:40px">
<option value="0">-Select-</option>

<c:forEach items="${holdersList}" var="holder">
<option value='<c:out value="${holder.statusId}" />'><c:out value="${holder.statusName}" /></option>
</c:forEach >

</select>  </td>
<td height="45" ><input type="button" value="Go" class="btn btn-success" onClick="getfunction()"></td>
</tr>
</table><br><br>





<table class="table table-info mb30 table-hover" align="center" border="1" cellpadding="0" cellspacing="0" width="500"  id="bg">
    <thead><tr>
<th  valign="center" height="35" >Available StakeHolders </th>
<th  height="35"></th>
<th  valign="center" height="35">Assigned StakeHolders </th>
        </tr></thead>
<tr>
<td valign="top">
<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>
<td width="150"><select style="width:200px;" multiple size="10" id="availableFunc" name="availableFunc" class="textbox">
<c:if test = "${statusList != null}" >
<c:forEach items="${statusList}" var="statusList">
<option value='<c:out value="${statusList.stakeHolderId}" />'><c:out value="${statusList.holderName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
</table>

</td>

<td height="100" valign="center">
<table width="20" height="70" cellpadding="2" cellspacing="2" align="center" id="bg">
<tr>
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; border-bottom-style:solid; "><input type="button" class="textbox" value=">" onClick="copyAddress(availableFunc.value)"></td></tr>
<tr >
<td height="15" align="center" bgcolor="#F4F4F4" style=" border:1px; "><input type="button" class="textbox" value="<" onClick="copyAddress1(assignedfunc.value)"></td></tr>


</table></td>
<td valign="top">

<table width="150" height="150" cellpadding="0" cellspacing="0" align="center" id="bg">
<tr>
<td width="150"><select style="width:200px;" multiple size="10" id="assignedfunc" name="assignedfunc"  class="textbox">
<c:if test = "${assignedFunc != null}">
<c:forEach items="${assignedFunc}" var="assignedFunc">
<option value='<c:out value="${assignedFunc.stakeHolderId}" />'><c:out value="${assignedFunc.holderName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
</table></td>


</tr>




</table>
</c:if>
<br><center>

<input type="button" name="save" value="Save" onClick="submitPage();" class="btn btn-success">


<input type="hidden" value="" name="reqfor">
</center>
<br>

</form>
</body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
