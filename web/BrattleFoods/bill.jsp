<%-- 
    Document   : bill
    Created on : Oct 29, 2013, 8:32:18 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>
    <body>
        <%
            String menuPath = "Report >> Invoice To Customer";
            request.setAttribute("menuPath", menuPath);
        %>
        <form>
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <div id="print" >
                <table bgcolor="#E3E3E3" width="950" border="1" align="center">
                    <tr>
                        <th scope="col" colspan="2"><p> LATHHA NARAYANAN</p>
                            <p>48/310 thambu chetty st,chennai-600001.  </p>
                            <p>&nbsp;</p></th>
                    </tr>
                    <tr>
                        <td width="470" scope="col">G.C.NO:DATE:</td>
                        <td width="329" bgcolor="#FFFFFF" align="center">PAY&nbsp;<b>/</b>&nbsp;TO PAY </td>

                    </tr>
                    <tr>
                        <td><p>CONSIGNOR:</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p></td>
                        <td><p>CONSIGNEE:</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p></td>

                    </tr>
                </table>
                <table width="950" border="1"  align="center">
                    <tr>
                        <th scope="col">S.NO</th>
                        <th scope="col">Product Type</th>
                        <th scope="col">Product Code</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">QUANTITY </th>
                        <th scope="col">Weight </th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Vehicle No:</b></td>
                        <td><b>Total</b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br>
                <table width="950" border="1"  align="center">
                    <tr>
                        <th scope="col">S.NO</th>
                        <th scope="col">Route Code</th>
                        <th scope="col">Route Name</th>
                        <th scope="col">Distance</th>
                        <th scope="col">Rate/Km</th>
                        <th scope="col">Total Amount (INR)</th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right"><b>Total : </b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                
                <table width="950" border="1" bgcolor="#E3E3E3" align="center">
                    <tr>
                        <td width="279" height="174" ><p>Gross Weight </p>
                            <p>Tare Weight</p>
                            <p>Net weight </p></td>
                        <td width="212">Document No.<p>&nbsp;</p>
                            <p>&nbsp;</p> </td>
                        <td width="247" rowspan="2"><p>Freight:</p>
                            <p>Service Tax For 25% of:</p>
                            <p>Freight Tax @12.33%:</p>
                            <p>Others: </p>
                            <p>Less Advance:</p>
                            <p>Total Freight:</p>
                            <p>&nbsp;</p></td>
                    </tr>

                    <tr>
                        <td height="69">Driver's Signature </td>
                        <td scope="col" colspan="1">D.L.No.:</td>

                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td><p>Person Liable For Paying</p>
                            <p>Service Tax:</p>
                            <p>Service Tax Regn. No:   </p></td>
                        <td><p>&nbsp;</p>
                            <p>&nbsp;</p> Receiver's Signature With Seal</td>
                        <td>For<p>&nbsp;</p>
                            <p>&nbsp;</p></td>
                    </tr>
                </table>
                <center>
                    <input type="button" class="button" name="Print" value="Print" onClick="print('print');" > &nbsp;
                </center>
            </div>    
            <script language="">
                function print(val)
                {
                    var DocumentContainer = document.getElementById(val);
                    var WindowObject = window.open('', "TrackHistoryData",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                }
            </script>
        </form>
