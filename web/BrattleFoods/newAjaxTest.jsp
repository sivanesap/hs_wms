<%-- 
    Document   : newAjaxTest
    Created on : Oct 5, 2013, 3:50:04 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>jQuery AutoComplete tutorial using multiple input boxes</title>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript">
            $(document).ready(function(){
                // Get the table object to use for adding a row at the end of the table
                var $itemsTable = $('#itemsTable');

                // Create an Array to for the table row. ** Just to make things a bit easier to read.
                var rowTemp = [
                    '<tr class="item-row">',
                    '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                    '<td><input name="itemCode" class="tInput" value="" id="itemCode" /> <input name="itemStatus" class="tInput" value="" id="itemStatus" type="text" /></td>',
                    '<td><input name="itemDesc" class="tInput" value="" id="itemDesc"  readonly="readonly" /></td>',
                    '<td><input name="itemQty" class="tInput" value="" id="itemQty" /></td>',
                    '<td><input name="itemPrice" class="tInput" value="" id="itemPrice" /></td>',
                   // '<td></td>',
                    '</tr>'
                ].join('');

                // Add row to list and allow user to use autocomplete to find items.
                $("#addRow").bind('click',function(){

                    var $row = $(rowTemp);

                    // save reference to inputs within row
                    var $itemCode 	        = $row.find('#itemCode');
                    var $itemDesc 	        = $row.find('#itemDesc');
                    var $itemPrice	        = $row.find('#itemPrice');
                    var $itemQty	        = $row.find('#itemQty');
                    var $itemStatus	        = $row.find('#itemStatus');

                    if ( $('#itemCode:last').val() !== '' ) {
                        // apply autocomplete widget to newly created row
                        $row.find('#itemCode').autocomplete({
                            source: function(request, response) {
                                $.ajax({
                                    url: "/throttle/getRouteName.do",
                                    dataType: "json",
                                    data: {
                                        term: request.term
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        //console.log( data);
                                        var items = data;
                                        response(items);
                                    },
                                    error: function(data,type){
                                        console.log(type);
                                    }
                                });
                            },
                            minLength: 1,
                            select: function(event, ui) {
                                //                        $( "#itemCode" ).val( ui.item.Name);
                                var $itemrow = $(this).closest('tr');
                                // Populate the input fields from the returned values
                                var value = ui.item.Name;
//                                alert(value);
                                var tmp = value.split('-');
                                $itemrow.find('#itemCode').val(tmp[0]);
                                $itemrow.find('#itemStatus').val(tmp[1]);
                                // Give focus to the next input field to recieve input from user

                                $itemQty.focus();
                                return false;
                            }
                            // Format the list menu output of the autocomplete
                        }).data( "autocomplete" )._renderItem = function( ul, item ) {
                            //alert(item);
                            var itemVal = item.Name;
                            var temp = itemVal.split('-');
                            itemVal = '<font color="green">'+temp[0]+'</font>';
                            return $( "<li></li>" )
                            .data( "item.autocomplete", item )
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append( "<a>"+ itemVal + "</a>" )
                            .appendTo( ul );
                        };
                    
                        // Add row after the first row in table
                        $('.item-row:last', $itemsTable).after($row);
                        $($itemCode).focus();

                    } // End if last itemCode input is empty
                    return false;
                });

                $('#itemCode').focus(function(){
                    window.onbeforeunload = function(){ return "You haven't saved your data.  Are you sure you want to leave this page without saving first?"; };
                });

            }); // End DOM

            // Remove row when clicked
            $("#deleteRow").live('click',function(){
                $(this).parents('.item-row').remove();
                // Hide delete Icon if we only have one row in the list.
                if ($(".item-row").length < 2) $("#deleteRow").hide();
            });

            $(document).ready(function(){
                // Use the .autocomplete() method to compile the list based on input from user
                $('#itemCode').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRouteName.do",
                            dataType: "json",
                            data: {
                                term: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data,type){
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        //                        $( "#itemCode" ).val( ui.item.Name);
                        var $itemrow = $(this).closest('tr');
                        // Populate the input fields from the returned values
                        var value = ui.item.Name;
//                        alert(value);
                        var tmp = value.split('-');
                        $itemrow.find('#itemCode').val(tmp[0]);
                        $itemrow.find('#itemStatus').val(tmp[1]);
                        // Give focus to the next input field to recieve input from user
                        
                        $itemrow.find('#itemQty').focus();
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data( "autocomplete" )._renderItem = function( ul, item ) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">'+temp[0]+'</font>';
                    return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    //.append( "<a>"+ item.Name + "</a>" )
                    .append( "<a>"+ itemVal + "</a>" )
                    .appendTo( ul );
                };
                
            });



        </script>

    </head>
    <body>
        <div id="container">

            <div class="panel">
                <div class="title-large">
                    <div class="theme"></div>
                </div>

                <div class="content inpad">

                    <div id="messageBox" style="margin-left:15px; padding-left:20px; padding-bottom:5px; border:1px #ccc solid; display:none;"></div>

                    <form action="" id="itemsForm">

                        <table id="itemsTable" class="general-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Item Code</th>
                                    <th>Item Description</th>
                                    <th>Item Qty</th>
                                    <th>Item Price</th>
<!--                                    <th>Item Status</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="item-row">
                                    <td></td>
                                    <td><input name="itemCode" value="" class="tInput" id="itemCode" tabindex="1"/> <input name="itemStatus" value="" class="tInput" id="itemStatus" type="text"/> </td>
                                    <td><input name="itemDesc" value="" class="tInput" id="itemDesc"  readonly="readonly" /></td>
                                    <td><input name="itemQty" value="" class="tInput" id="itemQty" tabindex="2"/></td>
                                    <td><input name="itemPrice" value="" class="tInput" id="itemPrice" readonly="readonly" /> </td>
<!--                                    <td></td>-->
                                </tr>
                            </tbody>
                        </table>

                    </form>

                    <a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Item</span></a>

                </div>


            </div>
        </div>

    </body>
</html>