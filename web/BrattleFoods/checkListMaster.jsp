<%-- 
    Document   : checkListMaster
    Created on : Oct 29, 2013, 5:11:19 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <% String menuPath = "Master >>  Add New CheckList Details";
                    request.setAttribute("menuPath", menuPath);
        %>
        <form name="standardCharges"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contenthead" colspan="4" >CheckList Details</td>
                </tr>
                <tr>
                    <td class="text1">CheckList Name</td>
                    <td class="text1"><input type="text" name="Name" class="textbox"></td>
                    <td class="text1">Stage</td>
                    <td class="text1"><select name="checkListType">
                            <option value="0">--Select--</option>
                            <option value="1">C-Note Creation</option>
                            <option value="2">Trip Sheet Creation</option>
                            <option value="3">Trip Start</option>
                            <option value="3">Trip Inprogress</option>
                            <option value="3">Trip End</option>
                            <option value="3">POD Generate</option>
                            <option value="3">Invoice Raise</option>
                        </select> </td>
                </tr>
                <tr>
                    <td class="text2">Effective Date</td>
                    <td class="text2"><input type="textbox" class="datepicker" value="" /></td>
                    <td class="text2" colspan="2" align="left"><input type="submit" class="button" value=" Save " /></td>
                </tr>
            </table>

            <br>
            <h2 align="center">CheckList View</h2>
            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>CheckList Name</h3></th>
                        <th><h3>Stage</h3></th>
                        <th><h3>Effective Date</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">State Highways Permit</td>
                        <td align="left" class="text2">Trip Start</td>
                        <td align="left" class="text2">01-10-2013</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">2</td>
                        <td align="left" class="text2">National Highways Permit</td>
                        <td align="left" class="text2">Trip Start</td>
                        <td align="left" class="text2">01-10-2103</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="selected" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">3</td>
                        <td align="left" class="text2">Vehicle Insurance Copy</td>
                        <td align="left" class="text2">Trip Start</td>
                        <td align="left" class="text2">01-10-2103</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">4</td>
                        <td align="left" class="text2">Consignor &amp; Consignee Details</td>
                        <td align="left" class="text2">C-Note Creation</td>
                        <td align="left" class="text2">01-10-2103</td>
                        <td align="left" class="text2"><input type="checkbox" name="select" id="select" /></td>
                    </tr>
                </tbody>
            </table>
            <br>

            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>
        </form>
    </body>
</html>
