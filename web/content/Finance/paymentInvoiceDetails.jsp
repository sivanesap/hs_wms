<%--  
    Document   : receiptEntry
    Created on : Mar 21, 2013, 10:50:00 PM
    Author     : Entitle
--%>

<%@page import="java.util.Date"%>
<html>
    <head>
        <%@page import="java.text.SimpleDateFormat" %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            function setInstValues() {
                var paymentMode = document.getElementById("paymentMode").value;
                var instName = "";
                if (paymentMode == 1) {
                    instName = "Cheque";
                } else if (paymentMode == 2) {
                    instName = "DD";
                } else if (paymentMode == 3) {
                    instName = "RTGS";
                }
                document.getElementById("instTag1").innerHTML = instName;
                document.getElementById("instTag2").innerHTML = instName;
                document.getElementById("instTag3").innerHTML = instName;
            }
            function setInvoiceDetails() {
                var invoiceName = document.getElementById('invoiceIdTemp').options[document.getElementById('invoiceIdTemp').selectedIndex].text;

                var invoiceNames = document.getElementsByName('invoiceNames');

                var errStatus = false;
                for (var i = 0; i < invoiceNames.length; i++) {
                    if (invoiceNames[i].value == invoiceName) {
                        alert("adjustment for this invoice no is already done. please select different invoice no");
                        errStatus = true;
                    }
                }
                if (!errStatus) {
                    var invoiceId = document.getElementById("invoiceIdTemp").value;
                    var temp = invoiceId.split("~");
                    document.getElementById("invoiceId").value = temp[0];
                    document.getElementById("grandTotal").value = temp[1];
                    document.getElementById("payAmount").value = 0;
                    document.getElementById("pendingAmount").value = temp[3];
                    document.getElementById("discountAmount").value = temp[4];
                }
            }
            function getInvoiceDetails() {
                var invoiceId = document.getElementById("invoiceId").value;
                if (invoiceId != "") {
                    $('#invoiceId').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getInvoiceDetails.do",
                                dataType: "json",
                                data: {
                                    invoiceId: request.term
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //alert("dgdfg");
                                    var items = data;
                                    response(items);
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $('#grandTotal').val(tmp[0]);
                            $('#pendingAmount').val(tmp[1]);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[1] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                }
            }


        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">

        $(document).ready(function() {
            $("#chequeId").hide();
            $("#DDId").hide();
            $("#RTGSId").hide();
        });

        function submitPage() {
            var selectedRowCount = $("#selectedRowCount").val();
            
            if($('#paymentMode').val() == 0){
                alert("Please select payment mode");
                $('#paymentMode').focus();
            }else if($('#receiveBankLedgerId').val() == 0){
                alert("Please select received bank");
                $('#receiveBankLedgerId').focus();
            }else if($('#bankName').val() == ''){
                alert("Please enter bank name");
                $('#bankName').focus();
            }else if($('#referenceNo').val() == ''){
                alert("Please enter reference no");
                $('#referenceNo').focus();
            }else if($('#receiptDate').val() == ''){
                alert("Please select receipt date");
                $('#receiptDate').focus();
            }else if($('#receiptAmount').val() == ''){
                alert("Please enter receipt amount");
                $('#receiptAmount').focus();
            }else if($('#invoiceIdTemp').val() == 0 && selectedRowCount <= 1){
                alert("Please select invoice");
                $('#invoiceIdTemp').focus();
            }else if(($('#payAmount').val() == '' || $('#payAmount').val() == 0 )  && (selectedRowCount <= 1)){
                alert("Please enter pay amount");
                $('#payAmount').focus();
            }else{
            document.invoice.action = '/throttle/insertInvoiceDetails.do';
            document.invoice.submit();
                
            }
        }

    </script>
    <script>
        function showPayment() {
            var mode = document.getElementById("paymentMode").value;
            if (mode == "1") {
                $("#chequeId").show();
                $("#DDId").hide();
                $("#RTGSId").hide();
            } else if (mode == "2") {
                $("#chequeId").hide();
                $("#DDId").show();
                $("#RTGSId").hide();
            } else if (mode == "3") {
                $("#chequeId").hide();
                $("#DDId").hide();
                $("#RTGSId").show();
            } else {
                $("#chequeId").hide();
                $("#DDId").hide();
                $("#RTGSId").hide();
            }
        }
    </script>
    <body>
        <form name="invoice" method="post" >
            <%
       Date today = new Date();
       SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
       String todayDate = sdf.format(today);
            %>
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                    <td >
                        &nbsp;
                    </td></tr></table>
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br>
            <br>



            <table align="center" border="0" cellpadding="0" cellspacing="0" width="1000" id="bg" class="border">
                <tr>
                    <td colspan="10" class="contenthead" height="30">
                        <div class="contenthead" align="center">RECEIPT DETAILS</div></td>
                </tr>
                <tr>
                    <td colspan="2" class="contenthead" align="right" height="30">
                        Customer Name  </td><td colspan="2" align="left"  class="contenthead" height="30"><c:out value="${customerName}"/>
                    </td>
                    <td colspan="4" class="contenthead" height="30">
                        Date </td><td colspan="2" class="contenthead" height="30">  <input type="hidden" id="ddDate" name="ddDate" value="<%=todayDate%>"  size="20" class="datepicker"><%=todayDate%></td>
                <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" /></td>
                <input type="hidden" name="customerLedgerId" id="customerLedgerId" value="<c:out value="${customerLedgerId}"/>" /></td>
                <input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" /></td>
                </tr>
            </table> 
            <br>
            <br> 
            <center>
                <h2>Payment Info</h2>
            </center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="1000" id="bg" class="border">    
                <tr class="text2" >
                    <td width="120" >Payment Mode </td>
                    <td  width="120"> <select name="paymentMode" id="paymentMode" onChange="setInstValues();" class="textbox" style="height:20px; width:122px;"  >
                            <option value="0" selected>--Select--</option>
                            <option value="1" >Cheque</option>
                            <option value="2" >DD</option>
                            <option value="3" >RTGS</option>
                        </select></td>
                    <td width="120" >&nbsp;Received At (Bank) </td>
                    <td  width="120">
                        <select name="receiveBankLedgerId" id="receiveBankLedgerId" class="textbox" style="height:20px; width:300px;"  >
                            <c:if test="${bankLists != null}">
                                <option value="0" selected>--Select--</option>
                                <c:forEach items="${bankLists}" var="bankVar">
                                    <option value='<c:out value="${bankVar.ledgerId}"/>'><c:out value="${bankVar.bankname}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td  height="30">&nbsp;Party Bank Name</td>
                    <td  height="30"><input type="textbox" id="bankName" name="bankName" value=""  ></td>
                </tr>
                <tr class="text2" >
                    <td  height="30"><div id="instTag1"></div> No</td>
                    <td  height="30"><input type="textbox" name="referenceNo" id="referenceNo" value="" /></td>
                    <td  height="30"><div id="instTag2"></div> Date</td>
                    <td  height="30"><input type="textbox" id="receiptDate" name="receiptDate" value=""  size="20" class="datepicker"></td>
                    <td  height="30"><div id="instTag3"></div> Amount </td>
                    <td  height="30"><input type="textbox" id="receiptAmount" name="receiptAmount" value="0" onKeyPress='return onKeyPressBlockCharacters(event);'></td>
                </tr>


            </table>
            <br>
            <br> 

            <center>
                <h2>Amount Details</h2>
            </center>


            <table align="center" border="0" cellpadding="0" cellspacing="0" width="1000" id="bg" class="border">    
                <tr>
                    <td class="text1" height="30">Invoice</td>
                <input type="hidden" name="invoiceId" id="invoiceId" value=""/>
                <td  width="80"> <select name="invoiceIdTemp" id="invoiceIdTemp" class="textbox" style="height:20px; width:122px;" onchange="setInvoiceDetails();" >
                        <c:if test="${invoiceList != null}">
                            <option value="0" selected>--Select--</option>
                            <c:forEach items="${invoiceList}" var="invoiceList">
                                <option value='<c:out value="${invoiceList.invoiceId}"/>~<c:out value="${invoiceList.grandTotal}"/>~<c:out value="${invoiceList.payAmount}"/>~<c:out value="${invoiceList.pendingAmount}"/>~<c:out value="${invoiceList.creditNoteAmount}"/>'><c:out value="${invoiceList.invoiceCode}"/></option>
                            </c:forEach>
                        </c:if>
                    </select>
                    <input type="hidden" name="customerName" id="customerName" value="" > </td>
                <td class="text1" height="30">Total Value</td>
                <td class="text1" height="30"><input type="textbox" name="grandTotal" id="grandTotal" value="" onKeyPress='return onKeyPressBlockCharacters(event);' /></td>
                <td class="text1" height="30">Pending Amount</td>
                <td class="text1" height="30"><input type="textbox" name="pendingAmount" id="pendingAmount" value="" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                <td class="text1" height="30">Credited Amount</td>
                <td class="text1" height="30"><input type="textbox" name="discountAmount" id="discountAmount" value="" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                <td class="text1" height="30">Ready to pay</td>
                <td class="text1" height="30"><input type="textbox" name="payAmount" id="payAmount" value="" onKeyPress='return onKeyPressBlockCharacters(event);'/></td>
                <td class="text1" height="30"><input type="button" value="Add" name="add" id="add" class="button" onclick="addRow();"/></td>
                </tr>

            </table>
            <br/>
            <br/>
            <script>
                function setValues(count) {
                    var invoiceName = document.getElementById('invoiceIdTemp').options[document.getElementById('invoiceIdTemp').selectedIndex].text;
                    document.getElementById('invoiceNames' + count).value = invoiceName;
                    document.getElementById('invoiceIds' + count).value = document.getElementById("invoiceId").value;
                    document.getElementById('paidAmounts' + count).value = document.getElementById("payAmount").value;
                    document.getElementById("invoiceId").value = "";
                    document.getElementById("payAmount").value = "";
                    document.getElementById("pendingAmount").value = "";
                    document.getElementById("discountAmount").value = "";
                    document.getElementById("grandTotal").value = "";
                    document.getElementById("invoiceIdTemp").value = "0";
                }
            </script>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="invoice" class="border">
                <tr align="left">
                    <td class="contenthead" height="30">S.No</td>
                    <td class="contenthead" height="30">Invoice No</td>
                    <td class="contenthead" height="30">Paid Amount</td>
                    <td class="contenthead" height="30">Delete</td>
                <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/> </tr>
            </table>

            <script>
                var podRowCount1 = 1;
                function addRow() {
                    var paidAmount = document.getElementsByName('paidAmounts');
                    var payAmount = document.getElementById('payAmount').value;
                    if (payAmount <= 0) {
                        alert("Please enter pay amount");
                        document.getElementById('payAmount').focus();
                    } else {
                        var total = 0;
                        //alert(paidAmount.length);
                        for (var i = 0; i < paidAmount.length; i++) {
                            total = parseFloat(total) + parseFloat(paidAmount[i].value);
                        }
                        total = parseFloat(total) + parseFloat(payAmount);
                        //alert(total);
                        var grandTotal = document.getElementById("receiptAmount").value;
                        //alert(grandTotal);
                        if (parseFloat(grandTotal) < total) {
                            alert("The adjusted amount is greater than pay amount. please cross check");
                        } else {
                            var podSno1 = document.getElementById('selectedRowCount').value;
                            var tab = document.getElementById("invoice");
                            var newrow = tab.insertRow(podSno1);

                            var newrow = tab.insertRow(podSno1);
                            newrow.id = 'rowId' + podSno1;

                            var cell = newrow.insertCell(0);
                            var cell0 = "<br><td class='text2' height='25' >" + podSno1 + "</td>";
                            cell.innerHTML = cell0;


                            cell = newrow.insertCell(1);
                            var cell0 = "<br><td class='text2' height='25' ><input type='hidden' readonly value='' style='width:120px;'  name='invoiceIds'  id='invoiceIds" + podSno1 + "'   value='' ><input type='text' readonly value='' style='width:120px;'  name='invoiceNames'  id='invoiceNames" + podSno1 + "'   value='' ></td>";
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(2);
                            var cell0 = "<br><td class='text2' height='25' ><input type='text' value='' style='width:120px;' readonly name='paidAmounts'  id='paidAmounts" + podSno1 + "'   value='' onKeyPress='return onKeyPressBlockCharacters(event);'></td>";
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(3);
                            cell0 = "<br><td class='text2' height='25' align='left'><input type='button' class='button' readonly style='width:130px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(" + podSno1 + ");' ></td>";
                            cell.innerHTML = cell0;


                            setValues(podSno1);
                            podSno1++;
                            if (podSno1 > 0) {
                                document.getElementById('selectedRowCount').value = podSno1;
                            }
                        }
                    }

                }

                function deleteRow(sno) {
                    var rowId = "rowId" + sno;
                    $("#" + rowId).remove();
                    document.getElementById('selectedRowCount').value--;
                }

            </script>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="350" id="bg" class="border">

                <tr>
                    <td class="text1" width="40%" height="30">Remarks :</td>
                    <td class="text1" width="60%"  height="30"><textarea name="Remarks" rows="4" cols="30">
                        </textarea></td>
                </tr>
            </table>

            <br>
            <table align="center" width="100%" border="0">
                <tr>
                    <td colspan="2" align="center"> <input type="button" id="ENTER" value="SAVE" class="button" onClick="submitPage();"></td>
                </tr>
            </table>

        </table>         
</body>
</html>