<%-- 
    Document   : alterLedger
    Created on : 25 Oct, 2012, 6:58:30 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ledger Alter</title>
          <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>
    <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    <script>
        function submitPage()
        {
//            alert("hi");
           if(document.alter.openingBalance.value==''){
                alert("Please Enter openingBalance");
                return 'false';
            }
           if(document.alter.amountType.value=='0'){
                alert("Please Enter openingBalance");
                return 'false';
            }
            document.alter.action='/throttle/saveAlterLedger.do';
            document.alter.submit();
        }


    </script>
    <body>
        <form name="alter" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${ledgeralterList != null}">
                <c:forEach items="${ledgeralterList}" var="LL">
                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                        <tr height="30">
                            <Td colspan="2" class="contenthead">Edit Ledger</Td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Ledger Name</td>
                            <td class="text2"><c:out value="${LL.ledgerName}"/>
                                <input type="hidden" name="ledgerID" value="<c:out value="${LL.ledgerID}"/>"> </td>
                        </tr>
                        <tr height="30">
                            <td class="text1"><font color="red">*</font>Group Name</td>
                            <td class="text1"><c:out value="${LL.groupname}"/></td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Amount Type</td>
                            <td class="text2">
                                <select name="amountType" class="textbox" style="width:125px" >
                                    <c:choose>
                                        <c:when test="${LL.amountType =='CREDIT'}" >
                                            <option value="CREDIT" selected > CREDIT </option>
                                            <option value="DEBIT" > DEBIT </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="CREDIT" > CREDIT </option>
                                            <option value="DEBIT" selected > DEBIT </option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text1"><font color="red">*</font>Opening Balance</td>
                            <td class="text1"> <input type="text" name="openingBalance" id="openingBalance" value="<c:out value="${LL.openingBalance}"/>" /> </td>
                        </tr>
                         <tr height="30">
                    
                     <td class="text2">Opening Balance Date </td>
                     <td class="text2">  <input type="text" id="date" name="date" value="<c:out value="${LL.openingBalanceDate}"/>"   class="datepicker"></td>
                </tr>

                    </table>
                </c:forEach>
            </c:if>

            <br>
            <br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        </form>
    </body>
</html>

