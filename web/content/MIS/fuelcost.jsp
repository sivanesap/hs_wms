<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
        
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page  pageEncoding="UTF-8" import="java.sql.*"%>
<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/json2.js"></script>
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>

        <meta charset="utf-8">
        <title></title>
<!--        <link rel="stylesheet" href="css/jquery.ui.theme.css">
        <script src="js/jquery-1.4.4.js"></script>
        <script src="js/jquery.ui.core.js"></script>
        <script src="js/jquery.ui.widget.js"></script>
        <script src="js/jquery.ui.mouse.js"></script>
        <script src="js/jquery.ui.sortable.js"></script>-->
        <style type="text/css">
            .link {
                font: normal 12px Arial;
                text-transform:uppercase;
                padding-left:10px;
                font-weight:bold;
            }

            .link a  {
                color:#7f8ba5;
                text-decoration:none;
            }

            .link a:hover {
                color:#7f8ba5;
                text-decoration:underline;

            }

        </style>
        <style type="text/css">
            #expand {
                width:100%;
            }
            .column { width: 435px; float: left; }
            .portlet { margin: 0 1em 1em 0; }
            .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
            .portlet-header .ui-icon { float: right; }
            .portlet-content { padding: 0.4em; }
            .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
            .ui-sortable-placeholder * { visibility: hidden; }
        </style>
        <script>
            $(function() {
                $( ".column" ).sortable({
                    connectWith: ".column"
                });

                $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
                .find( ".portlet-header" )
                .addClass( "ui-widget-header ui-corner-all" )
                .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
                .end()
                .find( ".portlet-content" );

                $( ".portlet-header .ui-icon" ).click(function() {
                    $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
                    $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
                });

                $( ".column" ).disableSelection();
            });
        </script>
        <script language="javascript">
            function submitPage() {
                document.customer.action = '/throttle/fuelcostperbill.do';
                document.customer.submit();
            }
        </script>
    </head>
    <body>
        <form name="customer" method="post">
            <%@ include file="/content/common/message.jsp" %>
            
            <table class="table table-info mb30 table-hover" id="report" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >Fuel Cost</th>
		</tr>
               </thead>
            
                                <table class="table table-info mb30 table-hover"  >

                                    <%!
                                        public String NullCheck(String inputString) {
                                            try {
                                                if ((inputString == null) || (inputString.trim().equals(""))) {
                                                    inputString = "";
                                                }
                                            } catch (Exception e) {
                                                inputString = "";
                                            }
                                            return inputString.trim();
                                        }
                                    %>

                                    <%

                                                String fromday = NullCheck((String) request.getAttribute("fromDate"));
                                                String today = NullCheck((String) request.getAttribute("toDate"));                                                

                                                if (today.equals("") && fromday.equals("")) {
                                                    Date dNow = new Date();
                                                    int month = 0;
                                                    int year = 0;
                                                    Calendar cal = Calendar.getInstance();
                                                    cal.setTime(dNow);
                                                    cal.add(Calendar.DATE, 0);
                                                    dNow = cal.getTime();

                                                    int day = 1;
                                                    month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                                                    year = Calendar.getInstance().get(Calendar.YEAR);

                                                    SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                                                    today = ft.format(dNow);
                                                    today = today;
                                                    fromday = day + "-" + month + "-" + year;
                                                }


                                                Connection conn = null;
                                                int count = 0;
                                                String fromDate="";
                                                String toDate="";

                                                try {

                                                    String fileName = "jdbc_url.properties";
                                                    Properties dbProps = new Properties();

                                                    InputStream is = getClass().getResourceAsStream("/" + fileName);
                                                    dbProps.load(is);//this may throw IOException
                                                    String dbClassName = dbProps.getProperty("jdbc.driverClassName");

                                                    String dbUrl = dbProps.getProperty("jdbc.url");
                                                    String dbUserName = dbProps.getProperty("jdbc.username");
                                                    String dbPassword = dbProps.getProperty("jdbc.password");

                                                    Class.forName(dbClassName).newInstance();
                                                    conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                                                    if (!fromday.equals("")) {
                                                        fromDate = " and ts.start_date >= str_to_date('" + fromday + "', '%d-%m-%Y')";
                                                    }
                                                    if (!today.equals("")) {
                                                        toDate = " and ts.start_date <= str_to_date('" + today + "', '%d-%m-%Y')";
                                                    }

                                                    String ovAllTrips="SELECT concat(monthname(start_date),'-',year(start_date)) as month, sum(average_price) as fuelexp  "
                                                            + " from ts_fuel_price ts "
                                                            + " where 1=1 "
                                                            + fromDate + toDate
                                                            + " group by month(start_date), year(start_date)";

                                                    String Name = "", totalAmount = "0";
                                                    //out.println(ovAllTrips);
                                                    PreparedStatement pstmOvAllTrips = conn.prepareStatement(ovAllTrips);
                                                    ResultSet res = pstmOvAllTrips.executeQuery();
                                                    String ovAllTripsXML = "";

                                                    while (res.next()) {
                                                        Name = res.getString(1);
                                                        totalAmount = res.getString(2);
                                                        ovAllTripsXML = ovAllTripsXML + " <set value='" + totalAmount + "' label='" + Name + "' alpha='60'/>";
                                                    }

                                                    ovAllTripsXML = "<chart yAxisName='Values' caption='Fuel Cost' numberPrefix=' ' useRoundEdges='1' bgColor='FFFFFF,FFFFFF' "
                                                    + "showBorder='0'> "+ovAllTripsXML+" </chart> ";


                                    %>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" ></td>
                                    </tr>
                                    <tr>                                        
<!--                                        <td><font color="red">*</font>Bill Type</td>
                                        <td><select name="billtype" class="form-control" style="width:250px;height:40px">
                                                    <option value="">-Select Any One-</option>
                                                        <option value="1">Primary</option>
                                                        <option value="2">Secondary</option>
                                             </select>
                                        </td>-->
                                    <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onClick="submitPage();" value="Search"></td>
                                    </tr>
                                </table>
                            
            
            
            <div id="expand">
                <div class="portlet" >
                    <div class="portlet-header" style="width:525px;" id="tableDesingTH">&nbsp;&nbsp;Fuel Cost</div>
                    <div class="portlet-content">
                        <table align="center"   cellspacing="0px" >
                            <tr>
                                <td>
                                    <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                        <jsp:param name="chartSWF" value="/throttle/swf/Column2D.swf" />
                                        <jsp:param name="strURL" value="" />
                                        <jsp:param name="strXML" value="<%=ovAllTripsXML%>" />
                                        <jsp:param name="chartId" value="vehiclerevenue" />
                                        <jsp:param name="chartWidth" value="650" />
                                        <jsp:param name="chartHeight" value="275" />
                                        <jsp:param name="debugMode" value="false" />
                                        <jsp:param name="registerWithJS" value="false" />
                                    </jsp:include>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div></div>

            <%
                            if (res != null) {
                                res.close();
                            }

                        } catch (FileNotFoundException fne) {
                            System.out.println("File Not found " + fne.getMessage());
                        } catch (SQLException se) {
                            System.out.println("SQL Exception " + se.getMessage());
                        } finally {
                            if (conn == null) {
                                conn.close();
                            }
                        }

            %>

        </form>
    </body>

</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
