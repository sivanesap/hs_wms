<%-- 
    Document   : PhysicalStockEntry2
    Created on : Nov 1, 2012, 5:29:17 PM
    Author     : admin
--%>

<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*,java.io.*,java.text.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <title>JSP Page</title>
        <script>

            function submitpage(){
                var errMsg = "";
                if(document.stockEntry2.newStock.value != "") {
                    document.stockEntry2.action = "PhysicalStockEntry3.jsp";
                    document.stockEntry2.method = "post";
                    document.stockEntry2.submit();
                } else {
                    alert("New Stock not filled");
                }
            }

        </script>
    </head>
    <body>
       <form name="stockEntry2">
            
        <%
        Connection conn = null;
        int count = 0;
        try{

            String fileName = "jdbc_url.properties";
            Properties dbProps = new Properties();
        //The forward slash "/" in front of in_filename will ensure that
        //no package names are prepended to the filename when the Classloader
        //search for the file in the classpath

        InputStream is = getClass().getResourceAsStream("/"+fileName);
        dbProps.load(is);//this may throw IOException
        String dbClassName = dbProps.getProperty("jdbc.driverClassName");
        
        String dbUrl = dbProps.getProperty("jdbc.url");
        String dbUserName = dbProps.getProperty("jdbc.username");
        String dbPassword = dbProps.getProperty("jdbc.password");
        /*out.println(dbClassName);
        out.println(dbUserName+"<br>");
        out.println(dbPassword+"<br>");*/

        String itemCode = request.getParameter("itemCode");

        DecimalFormat df2 = new DecimalFormat("0.00");
        Class.forName(dbClassName).newInstance();
        conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
        String query = "SELECT i.item_id, item_name,uom_name, sum(quantity) as qty FROM papl_item_master i, papl_uom u, "
                         + "papl_stock_balance s WHERE i.uom_id = u.uom_id AND i.item_id = s.item_id AND "
                         + "i.active_ind = 'Y' AND u.active_ind = 'Y' AND u.active_ind = 'Y' AND"
                         + " i.papl_code = ? GROUP BY s.item_id ";
        PreparedStatement pstm = conn.prepareStatement(query);        
        pstm.setString(1, itemCode);
      //  out.print(pstm);
        String itemName = "", uomName = "";
        float qty = 0.000f;
        int itemId = 0;
        ResultSet res = pstm.executeQuery();
        while(res.next()) {
            count++;
            itemId = res.getInt("item_id");
            itemName = res.getString("item_name");
            uomName = res.getString("uom_name");
            qty = res.getFloat("qty");

        }
        //out.println("<h1>"+count);
         if(count == 0) {
                String url = "PhysicalStockEntry1.jsp?message=Invalid Item Code";
                response.sendRedirect(url);
            }

        if(pstm != null) {
            pstm.close();
        }
        if(res != null) {
            res.close();
        }
        %>
          <table width="350" cellpadding="0" cellspacing="0" height="250" align="center" class="table2">
                <tr><td colspan="2" align="center" class="contenthead">Stock Manual Entry- Step:2</td>
                </tr>
                <tr>
                    <td>Item Code</td>
                    <td><%=itemCode%>
                        <input type="hidden" name="itemName" id="itemName" value="<%=itemName%>" />
                        <input type="hidden" name="uomName" id="uomName" value="<%=uomName%>" />
                        <input type="hidden" name="itemCode" id="itemCode" value="<%=itemCode%>" />
                        <input type="hidden" name="itemId" id="itemId" value="<%=itemId%>" />
                    </td>
                </tr>
                <tr>
                    <td>Item Name</td>
                    <td><%=itemName%></td>
                </tr>
                <tr>
                    <td>UOM</td>
                    <td><%=uomName%></td>
                </tr>
                <tr>
                    <td>Current Stock</td>
                    <td><%=qty%></td>
                </tr>
                <tr>
                    <td>New Stock</td>
                    <td><input type="text" name="newStock" id="newStock" maxlength="5" /></td>
                </tr>
                <tr><td colspan="2" align="center">
                        <input type="button" name="proceed" value="Add Stock" onclick="submitpage();" />
                    </td></tr>
            </table>
        <%


        }
        catch (IOException ioe){
            System.err.println("Properties loading failed in AppConfig");

        }finally{


            if(conn != null) {
                conn.close();
            }
           
        }

        %>
</form>
    </body>
</html>
