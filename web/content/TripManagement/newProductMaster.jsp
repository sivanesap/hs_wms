
<%--
    Document   :  newProductMaster
    Created on :  27/111/2012
    Author     :  DINESHKUMAR.S
--%>
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add Account Type</title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()

    {
        if(textValidation(document.add.productCode,'productCode')){
            return;
        }
        if(textValidation(document.add.productName,'productName')){
            return;
        }
        if(textValidation(document.add.remarks,'remarks')){
            return;
        }
        document.add.action='/throttle/saveProductMaster.do';
        document.add.submit();
}
function setFocus(){
    document.add.productName.focus();
    }

</script>
<body onload="setFocus();">
<form name="add" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Add Product</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Product Code</td>
    <td class="text2"><input name="productCode" type="text" class="textbox" value="" size="20"></td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Product Name</td>
    <td class="text1"><input name="productName" type="text" class="textbox" value="" size="20"></td>
  </tr>
   <tr height="30">
    <td class="text2"><font color="red">*</font>Remarks</td>
    <td class="text2"><textarea name="remarks"></textarea>
  </tr>
</table>
<br>
<br>
<center>
    <input type="button" class="button" value="Save" onclick="submitPage();" />
    &emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>

