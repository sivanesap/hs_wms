
<%--
    Document   : addLPS
    Created on : Nov 21, 2012, 7:35:29 PM
    Author     : DINESH KUMAR.S
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add Ledger</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>-->
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>

    <script>

        function getBagsValue()
        {
            var MT = parseFloat(document.add.quantity.value);
            var bags = (MT * 1000)/50;
            document.add.bags.value = bags;
        }
        function getTonnageValue()
        {
            var bags = parseFloat(document.add.bags.value);
            var ton = (bags * 50) / 1000;
            document.add.quantity.value = ton;
        }
        function submitPage()
        {
            //alert(document.add.partyName.value);
            if(document.add.partyName.value=='0'){
                alert("Please Select party's Name");
                return 'false';
            }
            if(document.add.productId.value=='0'){
                alert("Please Select Product Name");
                return 'false';
            }
            if(document.add.billStatus.value=='0'){
                alert("Please Select Bill Status");
                return 'false';
            }

            if(textValidation(document.add.lpsNumber,'LPSNumber')){
                return;
            }

            if(textValidation(document.add.lpsDate,'lpsDate')){
                return;
            }

            if(textValidation(document.add.orderNumber,'OrderNumber')){
                return;
            }

            if(textValidation(document.add.contractor,'Contractor')){
                return;
            }

//            if(textValidation(document.add.destination,'Destination')){
//                return;
//            }

            if(textValidation(document.add.packerNumber,'PackerNumber')){
                return;
            }

            if(textValidation(document.add.quantity,'Quantity')){
                return;
            }

            if(textValidation(document.add.bags,'Bags')){

                return;
            }

//            if(textValidation(document.add.packing,'Packing')){
//                return;
//            }

            if(textValidation(document.add.clplPriority,'ClplPriority')){
                return;
            }

//            if(textValidation(document.add.lorryNo,'lorryNo')){
//                return;
//            }

//            if(textValidation(document.add.gatePassNo,'gatePassNo')){
//                return;
//            }
//            if(textValidation(document.add.watchandward,'watchandward')){
//                return;
//            }
            var temp = document.add.clplPriority.value;
            document.add.action='/throttle/saveLPS.do';
            document.add.submit();
        }
        function setFocus(){
            document.add.LPSNumber.focus();
        }
        function getDestination(){
            //alert("Hi...");
            var oTextbox = new AutoSuggestControl(document.getElementById("destination"),new ListSuggestions("destination","/throttle/handleToDestination.do?"));
        }
    </script>
    <body onload="setFocus();">
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <table  align="center" width="80%" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="6" class="contenthead">Add LPS</Td>
                </tr>
                <br>
                <tr height="30">
                    <td class="texttitle1"><font color="red">*</font>Party's Name</td>
                    <td class="text1">
                        <select class="textbox" name="partyName"  style="width:200px" multiple="multiple">
                            <c:if test="${customerList != null}">
                                <option value="0">---Select---</option>
                                <c:forEach items="${customerList}" var="cus">
                                    <option value='<c:out value="${cus.custId}"/>'><c:out value="${cus.custName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="texttitle1">&emsp;</td>
                    <td class="texttitle1" colspan="3" align="left"><font color="red">*</font>Slip No &emsp;&emsp;&emsp;&emsp;
                        <input name="lpsNumber" type="text" class="textbox" value="" maxlength="10" size="20"><br><br>
                        <font color="red">*</font>Date &emsp;&emsp;&emsp;&emsp;&emsp;
                        <input name="lpsDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  />&nbsp;<br><br>
<!--                        Lorry No &emsp;&emsp;&emsp;-->
                        <input name="lorryNo" type="hidden" class="textbox" value="" size="20">
                    </td>
                </tr>
                <br><br>
                <tr height="30">
                    <td class="texttitle2"><font color="red">*</font>Order No</td>
                    <td class="text2"><input name="orderNumber" type="text" class="textbox" value="" maxlength="10" size="20"></td>
                    <td class="texttitle2"><font color="red">*</font>Packer No</td>
                    <td class="text2" ><input name="packerNumber" type="text" class="textbox" value="" size="20"></td>
                    <td class="texttitle2"><font color="red">*</font>Bill Status</td>
                    <td class="text2" >
                        <select class="textbox" name="billStatus" id="billStatus"  style="width:125px">
                            <option value="0">---Select---</option>
                            <option value='Paid'>Paid</option>
                            <option value='ToPay'>To Pay</option>
                        </select>
                    </td>
                </tr>

                <tr height="30">
                    <td class="texttitle1"><font color="red">*</font>Contractor</td>
                    <td class="text1"><input name="contractor" type="text" class="textbox" value="Chettinad Logistics Primary Ltd" size="20" readonly></td>
                    <td class="texttitle1"><font color="red">*</font>M.T</td>
                    <td class="text1"><input name="quantity" id="quantity" type="text" class="textbox" onchange="getBagsValue();" value="" size="20"></td>
                    <td class="texttitle1"><font color="red">*</font>Bags</td>
                    <td class="text1"><input name="bags" id="bags" type="text" class="textbox" onchange="getTonnageValue();"value="" size="20"></td>
                </tr>

                <tr height="30">

                    <td class="texttitle2"><font color="red">*</font>Product Name</td>
                    <td class="texttitle2">
                        <select class="textbox" name="productId" style="width:125px">
                            <c:if test="${productList != null}">
                                <option value="0">---Select---</option>
                                <c:forEach items="${productList}" var="proList">
                                    <option value='<c:out value="${proList.productId}"/>-<c:out value="${proList.productname}"/>'><c:out value="${proList.productname}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <!--                    <td class="text2"><input name="productName" type="text" class="textbox" value="" size="20"></td>-->
                    <td class="texttitle2"><font color="red">*</font>Packing</td>
                    <td class="text2">                        
                        <select class="textbox" name="packing"  style="width:125px">
                            <c:if test="${packList != null}">
                                <option value="0">---Select---</option>
                                <c:forEach items="${packList}" var="pack">
                                    <option value='<c:out value="${pack.packingName}"/>'><c:out value="${pack.packingName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="texttitle2">Gate Pass No</td>
                    <td class="text2"><input name="gatePassNo" type="text" class="textbox" value="" size="20"></td>
                </tr>

                <tr height="30">
                    <td class="texttitle1"><font color="red">*</font>Priority</td>
                    <td class="text1">
                        <select class="textbox" name="clplPriority"  style="width:125px">
                            <option>---Select---</option>
                            <option value='High'>High</option>
                            <option value='Medium'>Medium</option>
                            <option value='Low'>Low</option>
                        </select>
                    <td class="texttitle1"><font color="red">*</font>Destination</td>
                    <td class="text1">
                        <input name="destination" id="destination" type="text" class="textbox" value="" size="20" onFocus="getDestination();" autocomplete="off">
                    </td>
                    <td class="texttitle1">Watch & Ward</td>
                    <td class="text1"><input name="watchandward" type="text" class="textbox" value="" size="20"></td>
                </tr>

            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        </form>
    </body>
</html>


