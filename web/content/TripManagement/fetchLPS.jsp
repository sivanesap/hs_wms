<%--
    Document   : viewLPS
    Created on : Nov 21, 2012, 4:48:05 PM
    Author     : DINESHKUMAR.S
--%>
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add Ledger</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(val){
                if(val == 'Add'){
                    document.lps.action = '/throttle/addLPSPage.do';
                    document.lps.submit();
                }
               else if(val == 'add'){
                    document.lps.action = '/throttle/addLPSPage.do';
                    document.lps.submit();
               }else if(val == 'modify'){
                    document.lps.action = '/throttle/alterLPSDetail.do';
                    document.lps.submit();
               }else if(val == 'fetchData'){
                    document.lps.action = '/throttle/manageLPS.do';
                    document.lps.submit();

                }

            }




        </script>


    </head>
    <body onload="setImages(1,0,0,0,0,0);">
        <form name="lps">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:700px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Search LPS </li>
                            </ul>
                            <div id="first">
                                <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Date</td>
                                        <td><input type="text" id="fromDate" name="date" value="" class="datepicker" autocomplete="off"></td>

                                        <td>LPS No</td>
                                        <td><input type="text" id="lpsNo" name="lpsNo" value="" class="textbox" autocomplete="off"></td>

                                        <td>Destination</td>
                                        <td><input type="text" id="destination" name="destination" value="" class="textbox" autocomplete="off"></td>

                                       <td><input type="button" class="button" onclick="submitPage(this.name);" name="fetchData" value="FetchData"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <c:if test = "${lpsList != null}" >
                <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" class="border">

                    <tr height="30">
                        <td  align="left" class="contenthead" scope="col"><b>S.No</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>LPS No</b></td>      
                        <td  align="left" class="contenthead" scope="col"><b>LPS Date</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Destination</b></td>
                        <td  align="left" class="contenthead" scope="col"><b>Edit</b></td>
                    </tr>
                    <% int index = 0;%>
                    <c:forEach items="${lpsList}" var="LL">
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                            <td class="<%=classText%>" align="left"> <c:out value="${LL.lpsNumber}" /></td>     
                            <td class="<%=classText%>"  align="left"> <c:out value="${LL.lpsDate}"/> </td>
                            <td class="<%=classText%>"  align="left"> <c:out value="${LL.destination}"/> </td>
                            <td class="<%=classText%>" align="left"> <a href="/throttle/alterLPSDetail.do?lpsID=<c:out value='${LL.lpsID}' />" > Edit </a> </td>
                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <br>
</form>
</body>
</html>
