<%-- 
    Document   : displayPinkSlip
    Created on : Dec 11, 2012, 11:24:45 AM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Pink Slip</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
    
        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv">
                <br>
                <br>
                <br>


                <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <tr>
                        <td>
                            <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;" >
                    <tr>
                        <td height="27" colspan="5" class="text2"  align="center"  scope="row" style="border:0px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;">
                            <b><font size="5"> PINK SLIP </font></b>
                        </td>
                        <c:if test="${pinkSliplist != null}">
                        </tr>


                        <c:forEach items="${pinkSliplist}" var="PSL">
                            <c:set var="pinkSlipID" value="${PSL.pinkSlipID}" />
                            <c:set var="driverID" value="${PSL.driverID}" />
                            <c:set var="vehicleID" value="${PSL.vehicleID}" />
                            <c:set var="routeID" value="${PSL.routeID}" />
                            <c:set var="routeName" value="${PSL.routeName}" />


                            <tr>
                                <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="padding-bottom:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" >
                                    <b>   <p>Pink Slip No <br>  </b>
                                </td>
                                <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                    <c:if test= "${PSL.pinkSlipID != ''}"  >
                                        <b>:</b>&nbsp;<c:out value="${PSL.pinkSlipID}" />
                                    </c:if>
                                </td>

                                <td  class="text2"  align="left"  width="148" scope="col"  style="border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                    <b> <p>Route Name <br></b></td>
                                <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                    <b>:</b>&nbsp;<c:out value="${PSL.routeName}" />
                                </td>
                            </tr>

                            <tr>
                                <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                    <b> <p>Vehicle No <br></b>
                                <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                                    <b>:</b>&nbsp;<c:out value="${PSL.vehicleID}" />  </td>

                                <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                    <b> <p>Driver Name & ID <br></b></td>
                                <td class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                    <b>:</b>&nbsp;<c:out value="${PSL.driverID}" /> </td>
                                </c:forEach>

                        </c:if>
                    </tr>

                </table>

                    <%int index = 0;%>

                <table align="center" width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                    <br>
                    <br>
                    <tr>

                        <c:if test="${pinkSlipLPSList != null}">
                        <tr>

                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>S.No</b></td>
                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Order No</b></td>
                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Tonnage</b></td>
                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Bags</b></td>
                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Destination</b></td>
                            <td style="border:1px;  border-bottom-color:#CCCCCC; border-bottom-style:solid; background:#CCCCCC; color:#000000;"><b>Priority</b></td>

                        </tr>

                        <c:forEach items="${pinkSlipLPSList}" var="PSLL">


                            <tr>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;" height="30"><%=index + 1%></td>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${PSLL.orderNumber}"/></td>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${PSLL.quantity}"/></td>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${PSLL.bags}"/></td>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${PSLL.destination}"/></td>
                                <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;"><c:out value="${PSLL.clplPriority}"/></td>
                            </tr>
                            <%index++;%>
                        </c:forEach>
                    </c:if>
                    </tr>

                </table>
                    <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                <br>
                <br>
                <br>
                <br>
                <tr align="center">
                    <td style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">

                        <input align="center" type="button" onclick="print();" value = " Print "   />
                    </td>
                </tr>

            </table>

                        </td>
                    </tr>

                </table>

            </div>

            




        </form>
    </body>
</html>
