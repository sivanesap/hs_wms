<%-- 
    Document   : viewPinkSlipNew
    Created on : Jan 25, 2013, 3:09:02 PM
    Author     : Entitle
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            function submitPage(val){
                document.pinkSlip.action = '/throttle/managePinkSlipSummary.do';
                document.pinkSlip.submit();

            }


            function setValues(){

                var lpsNo='<%=request.getAttribute("lpsNo")%>';
                if(lpsNo!='null'){
                    document.pinkSlip.lpsNo.value=lpsNo;
                }
                var pinkSlipNo='<%=request.getAttribute("pinkSlipNo")%>';
                if(pinkSlipNo!='null'){
                    document.pinkSlip.pinkSlipNo.value=pinkSlipNo;
                }
                var vehicleNo='<%=request.getAttribute("vehicleNo")%>';
                if(vehicleNo!='null'){
                    document.pinkSlip.vehicleNo.value=vehicleNo;
                }
                var pinkSlipStatus='<%=request.getAttribute("pinkSlipStatus")%>';
                //alert(pinkSlipStatus);
                if(pinkSlipStatus !='null'){
                    document.pinkSlip.pinkSlipStatus.value=pinkSlipStatus;
                }
            }

        </script>
    </head>

    <body onload="setImages(0,0,0,0,0,0);setValues();">
        <form name="pinkSlip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:950px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Search Pink Slip </li>
                            </ul>
                            <div id="first">
                                <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>

                                        <td>Pink Slip No</td>
                                        <td><input type="text" id="pinkSlipNo" name="pinkSlipNo" value="" class="textbox" autocomplete="off"></td>
                                        <td>LPS No</td>
                                        <td><input type="text" id="lpsNo" name="lpsNo" value="" class="textbox" autocomplete="off"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Vehicle No</td>
                                        <td><input type="text" id="vehicleNo" name="vehicleNo" value="" class="textbox" autocomplete="off"></td>
                                        <td>Status</td>
                                        <td>
                                            <select name="pinkSlipStatus">
                                                <option value="0">ALL</option>
                                                <option value="1">Trip Not Created</option>
                                                <option value="2">Trip Generated</option>
                                            </select>
                                        </td>

                                        <td><input type="button" class="button" onclick="submitPage(this.name);" name="fetchData" value="FetchData"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>

            
                    <c:if test="${pinkSlipSummaryDetail != null}">

                        <table  border="0" class="border" align="center" width="83%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td class="table">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                                            <tr>
                                            <td class="bottom" align="left"><img src="images/left_status.jpg" alt=""  /></td>
                                            <td class="bottom" height="35" align="right"><img src="images/icon_active.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left"><%= request.getAttribute("tripNotCreated")%></span></td>
                                            <td class="bottom" align="right"><img src="images/icon_ps_closed.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;"><%= request.getAttribute("tripCreated")%></span></td>
                                            <td align="center"><h2>Total  <%= request.getAttribute("totalCount")%></h2></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                                            <br>

                        <table width="100%" align="center" border="0"  id="table" class="sortable">
                        <thead>
                            <%int index2 = 1;%>
                        <tr>
                            <th><h3>S.No</h3></th>
                            <th><h3>PinkSlip No</h3></th>
                            <th><h3>LPS No</h3></th>
                            <th><h3>Route Name</h3></th>
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Driver Name</h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>View</h3></th>
                            <th><h3>Delete</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${pinkSlipSummaryDetail}" var="pinkList">

                            <%--                <c:set var="DriverN1" value="${pinkList.driverID)}" />
                                            <c:set var="DriverN" value="${fn:split(DriverN1, '-')}" />
                            --%>

                            <%
                                        String classText2 = "";
                                        int oddEven2 = index2 % 2;
                                        if (oddEven2 > 0) {
                                            classText2 = "text2";
                                        } else {
                                            classText2 = "text1";
                                        }
                            %>

                            <tr>
                                <td class="<%=classText2%>"><%=index2%></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.pinkSlipID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.lpsID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.routeName}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.vehicleID}"/></td>
                                <td class="<%=classText2%>"><c:out value="${pinkList.driverID}"/></td>
                                <td class="<%=classText2%>">
                                    <c:if test="${pinkList.tripId != null}">
                                        Trip Created
                                    </c:if>
                                    <c:if test="${pinkList.tripId == null}">
                                        Trip Not Yet Created
                                    </c:if>
                                </td>
    <!--    <td class="<%=classText2%>"><c:out value="${DriverN[0]}"/></td>-->
                                <td class="<%=classText2%>" align="left"> <a href="/throttle/PinkSlipDetail.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>"  > View </a> </td>
                                <c:if test="${pinkList.tripId != null}">
                                    <td class="<%=classText2%>" align="left"> </td>
                                </c:if>
                                <c:if test="${pinkList.tripId == null}">
                                    <td class="<%=classText2%>" align="left"> <a onclick="return confirm('Are you sure you want to delete')" href="/throttle/PinkSlipDelete.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>" > Delete </a> </td>
                                </c:if>
<!--                                <td class="<%=classText2%>" align="left"> <a  href="/throttle/PinkSlipDelete.do?pinkSlipID=<c:out value='${pinkList.pinkSlipID}' />&lpsID=<c:out value="${pinkList.lpsID}"/>"  > Delete </a> </td>-->
                            </tr>
                            <%index2++;%>
                        </c:forEach>
                        </tbody>
                    </table>
                    </c:if>
                
        </form>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",1);
        </script>
    </body>
</html>
