<%--
    Document   : viewMarketVehicle
    Created on : Dec 23, 2012, 6:21:30 PM
    Author     : Entitle
--%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">

            function setDefaultValue(){
                document.settle.commissionAmount.value = ((parseInt(document.settle.totalamount.value) * parseInt(document.settle.commissionPercentage.value))/100 );
                document.settle.settlementAmount.value = (parseInt(document.settle.totalamount.value) - parseInt(document.settle.commissionAmount.value));
                document.settle.totalshortage.value = (parseInt(document.settle.totalTannage.value) - parseInt(document.settle.totalDeliveredTonnage.value));
            }
            function submitPage(obj){
                    document.settle.action='/throttle/saveVendorSettlement.do';
                    document.settle.submit();
            }


        </script>


    </head>

    <body onload="setDefaultValue();">
        <%
                    // OperationTO operationTO = new OperationTO();
                    //float laborTotal = (float) Float.parseFloat(operationTO.getCommissionPercentage());
                    String fromDate = (String) request.getAttribute("fromDate");
                    String toDate = (String) request.getAttribute("toDate");
                    String regno = (String) request.getAttribute("regno");
                    String vendorName = (String) request.getAttribute("vendorName");
                    String vendorId = (String) request.getAttribute("vendorId");
        %>

        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
             <%@ include file="/content/common/message.jsp" %>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Vendor Settlement</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vendor Name </td>
                                        <td height="30">
                                            <input name="vendorName" id="vendorName" type="text" class="textbox" size="20" value="<%=vendorName%>" readonly>
                                            <input name="vendorId" id="vendorId" type="hidden" class="textbox" size="20" value="<%=vendorId%>" >
                                        </td>
                                        <td>Vehicle No</td>
                                        <td><input name="totalregno" id="totalregno" type="text" class="textbox" size="20" value="<%=regno%>" readonly></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="30">From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="textbox" size="20" value="<%=fromDate%>" readonly ></td>
                                        <td height="30">To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="textbox" size="20" value="<%=toDate%>" readonly ></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <div id="tabs">
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#summary"><span>summary</span></a></li>
                </ul>
                <div id="tripDetail">
                    <c:if test = "${marketVehicleList != null}" >
                        <%
                                    int index = 0;
                        %>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">OUT KM</td>
                                <td class="contentsub" height="30">OUT DateTime</td>
                                <td class="contentsub" height="30">IN KM</td>
                                <td class="contentsub" height="30">IN DateTime</td>
                                <td class="contentsub" height="30">Total Tonnage</td>
                                <td class="contentsub" height="30">Delivered Tonnage</td>
                                <td class="contentsub" height="30">Shortage</td>
                                <td class="contentsub" height="30">Total Amount</td>
                                <td class="contentsub" height="30">Trip Status</td>
                            </tr>
                            <c:forEach items="${marketVehicleList}" var="mvl">
                                <c:set var="total" value="${total+1}"></c:set>
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>
                                <tr>
                                    <td class="<%=classText%>"  height="30"><%=index + 1%></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="tripId" name="tripId" value="<c:out value="${mvl.tripId}"/>"/><c:out value="${mvl.tripId}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="regno" name="regno" value="<c:out value="${mvl.regno}"/>"/><c:out value="${mvl.regno}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="routeName" name="routeName" value="<c:out value="${mvl.routeId}"/>"/><c:out value="${mvl.routeName}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="outKM" name="outKM" value="<c:out value="${mvl.outKM}"/>"/><c:out value="${mvl.outKM}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="outDateTime" name="outDateTime" value="<c:out value="${mvl.outDateTime}"/>"/><c:out value="${mvl.outDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="inKM" name="inKM" value="<c:out value="${mvl.inKM}"/>"/><c:out value="${mvl.inKM}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="inDateTime" name="inDateTime" value="<c:out value="${mvl.inDateTime}"/>"/><c:out value="${mvl.inDateTime}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="totalTonnage" name="totalTonnage" value="<c:out value="${mvl.totalTonnage}"/>"/><c:out value="${mvl.totalTonnage}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="deliveredTonnage" name="deliveredTonnage" value="<c:out value="${mvl.deliveredTonnage}"/>"/><c:out value="${mvl.deliveredTonnage}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="shortage" name="shortage" value="<c:out value="${mvl.shortage}"/>"/><c:out value="${mvl.shortage}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="totalamaount" name="totalamaount" value="<c:out value="${mvl.revenue}"/>"/><c:out value="${mvl.revenue}"/></td>
                                    <td class="<%=classText%>"  height="30"><input type="hidden" id="status" name="status" value="<c:out value="${mvl.status}"/>"/><c:out value="${mvl.status}"/></td>
                                </tr>
                                <%index++;%>
                            </c:forEach>
                        </table>
                    </c:if>
                    <input name="check" type="hidden" value='0'>
                    <br/>
                    <br/>
                </div>
                <div id="summary" align="center">
                    <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                        <tr style="width:50px;">
                            <th colspan="8" class="contenthead">Vendor Details</th>
                        </tr>
                        <c:if test="${totalMarketVehicleList != null}">
                            <c:forEach items="${totalMarketVehicleList}" var="tmvl">
                                <c:set var="vendorName" value="${tmvl.vendorName}" />
                                <c:set var="noOfTrip" value="${tmvl.noOfTrip}" />
                                <c:set var="totalTonnage" value="${tmvl.totalTonnage}" />
                                <c:set var="totalDeliveredTonnage" value="${tmvl.totalDeliveredTonnage}" />
                                <c:set var="totalamount" value="${tmvl.revenue}" />
                            </c:forEach>
                        </c:if>
                        <c:if test="${commissionVal != null}">
                            <c:forEach items="${commissionVal}" var="cv">
                                <c:set var="commissionPercentage" value="${cv.commissionPercentage}" />
                            </c:forEach>
                        </c:if>



                        <tr>
                            <td class="contenletter">Vendor Name</td>
                            <td><input id="vendorName" name="vendorName" value="<c:out value="${vendorName}"/>" /></td>
                            <td class="contenletter">Number Of Trip</td>
                            <td><input id="noOfTrip" name="noOfTrip" value="<c:out value="${noOfTrip}"/>" /></td>
                        </tr>
                        <tr>
                            <td class="contenletter">Total Tonnage</td>
                            <td><input id="totalTannage" name="totalTannage" value="<c:out value="${totalTonnage}"/>" /></td>
                            <td class="contenletter">Total Delivered Tonnage</td>
                            <td><input id="totalDeliveredTonnage" name="totalDeliveredTonnage" value="<c:out value="${totalDeliveredTonnage}"/>" /></td>
                        </tr>
                        <tr>
                            <td class="contenletter">Shortage</td>
                            <td><input id="totalshortage" name="totalshortage" value="" /></td>
                            <td class="contenletter">Commission Percentage</td>
                            <td><input id="commissionPercentage" name="commissionPercentage" value="<c:out value="${commissionPercentage}"/> " /></td>
                        </tr>
                        <tr>
                            <td class="contenletter">Total Amount</td>
                            <td><input id="totalamount" name="totalamount" value="<c:out value="${totalamount}"/>" /></td>
                            <td class="contenletter">Commission Amount</td>
                            <td><input id="commissionAmount" name="commissionAmount" value="" /></td>
                        </tr>
                        <tr>
                            <td class="contenletter">Settlement Amount</td>
                            <td><input id="settlementAmount" name="settlementAmount" value="" /></td>
                        </tr>
                        <tr>
                            <td colspan="8" align="center">
                                <input type="button" name="proceed" value="Proceed to Settlement" onclick="submitPage(this)" id="proceed" class="button" />
                            </td>
                        </tr>
                    </table>
                    <br/>

                    <br/>

                    <br>

                </div>
            </div>
        </form>
    </body>
</html>