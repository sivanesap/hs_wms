<%-- 
    Document   : displayVendorSettlement
    Created on : Dec 28, 2012, 4:15:54 PM
    Author     : Entitle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>Pink Slip</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv"></div>
            <br>
            <br>
            <br>
            <br>
            <c:if test="${settlementHeader != null}">
                <c:f  orEach items="${settlementHeader}" var="sh">
                    <c:set var="settlementid" value="${sh.settlementid}"/>
                    <c:set var="voucherCode" value="${sh.voucherCode}"/>
                    <c:set var="vendorName" value="${sh.vendorName}"/>
                    <c:set var="fromDate" value="${sh.fromDate}"/>
                    <c:set var="toDate" value="${sh.toDate}"/>
                    <c:set var="numberOfTrip" value="${sh.numberOfTrip}"/>
                    <c:set var="totalTonnage" value="${sh.totalTonnage}"/>
                    <c:set var="deliveredTonnage" value="${sh.deliveredTonnage}"/>
                    <c:set var="shortage" value="${sh.shortage}"/>
                    <c:set var="totalAmount" value="${sh.totalAmount}"/>
                    <c:set var="commissionPercentage" value="${sh.commissionPercentage}"/>
                    <c:set var="commisssionAmount" value="${sh.commisssionAmount}"/>
                    <c:set var="settlementAmount" value="${sh.settlementAmount}"/>
                    <c:set var="remarks" value="${sh.remarks}"/>
                    <c:set var="createddate" value="${sh.createddate}"/>
                </c:forEach>
            </c:if>


            <table align="center" width="800" border="0" cellspacing="1" cellpadding="0"  style="border:2px; border-color:#E8E8E8; border-style:solid;" >
                <tr>
                    <td>
                        <table align="center" cellspacing="0" cellpadding="0" width="100%" border="0" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                            <tr colspan="2">
                                <td height="27" colspan="1" align="left">
                                    <font size="3"><b>CHETTINAD LOGISTICS PRAIVATE LIMITED , KARIKKALI</b></font>
                                </td>
                                <td height="27" colspan="1" align="right">
                                    <font size="3"><b>Hire Payment voucher-Advance</b></font>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="55%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <br>
                                    <font size="3"><b>NO  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;: <c:out value="${voucherCode}"/></b></font>
                                </td>
                                <td align="right" width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Date &emsp;:<c:out value="${createddate}"/></b></font>
                                </td>                               
                            </tr>
                            <tr>
                                <td align="left" width="55%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Credit A/c  &emsp;&emsp;&emsp;&nbsp;: KARIKKALI CASH </b></font>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="55%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Remark &emsp;&emsp;&emsp;&emsp;:<c:out value="${remarks}"/></b></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>
                        <table align="center" cellspacing="0" cellpadding="0" width="800" border="1" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                            <tr>
                                <td align="left" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Debit A/C </b></font>
                                </td>
                                <td align="center" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>&emsp;</b></font>
                                </td>
                                <td align="right" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Amount(Rs.) </b></font>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="32%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b><c:out value="${vendorName}"/></b></font>
                                </td>
                                <td align="center" width="32%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>&emsp; </b></font>
                                </td>
                                <td align="right" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b><c:out value="${settlementAmount}"/></b></font>
                                </td>
                            </tr>
                            <tr>                          
                                <td align="right" colspan="2" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Voucher Total :</b></font>
                                </td>
                                <td align="right" width="32%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b><c:out value="${settlementAmount}"/></b></font>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="32%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Movement Detail :</b></font>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" cellspacing="0" cellpadding="0" width="800" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;">
                            <%int index = 0;%>
                            <c:if test="${settlementDetail != null}">
                                <tr style="background:#CCCCCC; color:#000000;">
                                    <td><b>S.No</b></td>
                                    <td><b>Trip No</b></td>
                                    <td><b>Trip Date</b></td>
                                    <td><b>Route Name</b></td>
                                    <td><b>Vehicle</b></td>
                                    <td><b>Tonnage</b></td>
                                    <td><b>Total amount</b></td>
                                </tr>
                                <c:forEach items="${settlementDetail}" var="sd">

                                    <%
                                                String classText1 = "";
                                                int oddEven1 = index % 2;
                                                if (oddEven1 > 0) {
                                                    classText1 = "text2";
                                                } else {
                                                    classText1 = "text1";
                                                }
                                    %>
                                    <tr>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><%=index + 1%></td>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.tripId}"/></td>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.tripDate}"/></td>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.routeName}"/></td>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.vehicleNo}"/></td>
                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.totalTonnage}"/></td>
                                        <td align="right"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${sd.totalAmountL}"/></td>
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </c:if>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>
                        <br>
                        <br>
                        <table align="center" cellspacing="0" cellpadding="0" width="800" border="1" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                            <tr>
                                <td align="left" width="20%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Prepared by</b></font>
                                </td>
                                <td align="center" width="20%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Checked by</b></font>
                                </td>
                                <td align="right" width="20%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Passed by</b></font>
                                </td>
                                <td align="right" width="20%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Receiver</b></font>
                                </td>
                                <td align="right" width="20%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                    <font size="3"><b>Cashier</b></font>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>


            <table align="center" width="800" border="0" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                <br>
              
                <tr align="center">
                    <td >

                        <input align="center" type="button" onclick="print();" value = " Print "   />
                    </td>
                </tr>

            </table>




        </form>
    </body>
</html>

