
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script>
        function submitPage()
        {
            //alert('hi');
            document.billDetails.action = "/throttle/viewbillpageforsubmit.do";
            document.billDetails.submit();
        }
        function showBillDeatil(invoiceId) {
            //alert("hi");
            document.billDetails.action = "/throttle/showinvoicedetail.do?invoiceId=" + invoiceId;
            document.billDetails.submit();

        }
        function submitBillCourieDetails(invoiceId, tripId) {
            //alert("hi");
            document.billDetails.action = "/throttle/submitBillCourierDetails.do?invoiceId=" + invoiceId + '&tripId=' + tripId;
            document.billDetails.submit();

        }

        function billWithFreights(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function billToCustomers(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function reimbursements(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function cancellation(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function tspInvoice(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function royaltyInvoice(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function tspDestinationInvoice(invoiceId, invoiceType) {
            window.open('/throttle/displayInvoicePrint.do?invoiceId=' + invoiceId + "&invoiceType=" + invoiceType, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

    </script>
</head>
<%
    String menuPath = "Finance >> View Bills";
    request.setAttribute("menuPath", menuPath);
%>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.SubmitBill" text="Submit Bill"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Billing" text="Billing"/></a></li>
            <li class=""><spring:message code="hrms.label.Submit Bill" text="Submit Bill"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="hideTd();">
                <form name="billDetails" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp"%>
                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="5" height="30" > View Bill Details</th>
                            </tr>
                        </thead>

                        <tr>
                            <td><font color="red">*</font>Customer Name</td>
                            <td height="30">
                                <select class="form-control" style="width:250px;height:40px" name="customerId" id="customerId"  style="width:125px;"><option value="">---Select---</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                                document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                </script>
                            </td>
                             <td>Invoice Type</td>
                            <td height="30">
                                    <select class="form-control" style="width:250px;height:40px" name="cancelInvoiceFlag" id="cancelInvoiceFlag" onchange="hideTd();" style="width:125px;">
                                        <option value="2" >Normal</option>
                                        <option value="1">Cancellation</option>
                                        <option value="3" >Origin TSP</option>
                                        <option value="4" >Royalty</option>
                                        <option value="5" >Destination TSP</option>
                                    </select>
                                </td>
                            <script>

                                document.getElementById("cancelInvoiceFlag").value =<c:out value="${cancelInvoiceId}"/>;
                            </script>
                        <td height="30" style="display:none">
                                    <select class="form-control" style="width:250px;height:40px" name="fuelInvoiceFlag" id="fuelInvoiceFlag"  style="width:125px;">
                                        <option value="1">Seperate</option>
                                        <option value="2" >Bundled</option>
                                    </select>
                                </td>
                            <script>
                                function hideTd(){
                                    var cancelInvoiceFlag = document.getElementById("cancelInvoiceFlag").value;
//                                    alert("cancelInvoiceFlagjjjjj="+cancelInvoiceFlag)
                                    if(cancelInvoiceFlag == 2){
                                        $("#fuelInvoiceFlag").show();
                                    }else{
                                        $("#fuelInvoiceFlag").hide();
                                    }
                                }
                                document.getElementById("fuelInvoiceFlag").value =<c:out value="${fuelInvoiceFlag}"/>;
                            </script>


                        </tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker"  style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                            <td> <input type="hidden" name="days" id="days" value="" /></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center"><input type="button" class="btn btn-success"   value="FETCH DATA" onclick="submitPage();"></td>
                        </tr>
                    </table>




                    <c:if test="${closedBillList !=nul}">
                        <table width="1000px;" class="table table-info mb30 table-hover" id="table" >
                            <thead>

                                <tr >
                                    <th>S.No</th>
<!--                                    <th>BillWithFreight</th>
                                    <th>BillToCustomer</th>
                                    <th>Reimbursement</th>-->
                                    <th>Cancellation</th>
                                    <th>Origin TSP </th>
                                    <th>Royalty</th>
                                    <th>Destination</th>
                                    <th>Bill Date</th>
                                    <th>Customer</th>
                                    <th>Contract Type</th>
                                    <th>Amount</th>
                                    <th>POD Status</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%int sno=1;
                                int index =0;
                                String classText="";
                                %>
                                <c:forEach items="${closedBillList}" var="closedBillList">
                                    <%int oddEven = index % 2;
                                  if (oddEven > 0) {
                                             classText = "text2";
                                         } else {
                                             classText = "text1";
                                         }
                                    %>
                                    <tr>
                                        <td  height="30"><%=sno++%></td>
<!--                                        <td  height="30">
                                        </td>
                                        <td  height="30">
                                        </td>
                                        <td  height="30">
                                        </td>-->

                                        <td  height="30">
                                            <c:if test="${cancelInvoiceId == 1 }">
                                            <a href="#" onclick="cancellation('<c:out value="${closedBillList.invoiceId}"/>', 'cancellation')"><c:out value="${closedBillList.cancellationInvoice}"/></a>
                                            <%-- <c:if test="${closedBillList.cancellationInvoice != '' && closedBillList.cancellationInvoice != null}">
                                                <a href="displayInvoiceSummaryExcel.do?invoiceId=<c:out value="${closedBillList.invoiceId}"/>&invoiceType=cancellation">
                                                    <c:out value="${closedBillList.cancellationInvoice}"/> Summary</a>
                                                </c:if>--%>
                                                </c:if>
                                        </td>
                                        <td  height="30">
                                             <c:if test="${cancelInvoiceId == 3 }">
                                            <a href="#" onclick="tspInvoice('<c:out value="${closedBillList.invoiceId}"/>', 'tspInvoice')"><c:out value="${closedBillList.billToCustomer}"/></a>
                                            <%--<c:if test="${closedBillList.cancellationInvoice != '' && closedBillList.cancellationInvoice != null}">
                                                <a href="displayInvoiceSummaryExcel.do?invoiceId=<c:out value="${closedBillList.invoiceId}"/>&invoiceType=cancellation">
                                                    <c:out value="${closedBillList.cancellationInvoice}"/> Summary</a>
                                                </c:if>--%>
                                                </c:if>
                                        </td>
                                        
                                        <td  height="30">
                                             <c:if test="${cancelInvoiceId == 4 }">
                                            <a href="#" onclick="royaltyInvoice('<c:out value="${closedBillList.invoiceId}"/>', 'royaltyInvoice')"><c:out value="${closedBillList.billToCustomer}"/></a>
                                            <%--  <c:if test="${closedBillList.cancellationInvoice != '' && closedBillList.cancellationInvoice != null}">
                                                <a href="displayInvoiceSummaryExcel.do?invoiceId=<c:out value="${closedBillList.invoiceId}"/>&invoiceType=cancellation">
                                                    <c:out value="${closedBillList.cancellationInvoice}"/> Summary</a>
</c:if>--%>
                                                </c:if>
                                        </td>
                                         <td  height="30">
                                             <c:if test="${cancelInvoiceId == 5 }">
                                            <a href="#" onclick="tspDestinationInvoice('<c:out value="${closedBillList.invoiceId}"/>', 'tspDestinationInvoice')"><c:out value="${closedBillList.billToCustomer}"/></a>
                                            <%--<c:if test="${closedBillList.cancellationInvoice != '' && closedBillList.cancellationInvoice != null}">
                                                <a href="displayInvoiceSummaryExcel.do?invoiceId=<c:out value="${closedBillList.invoiceId}"/>&invoiceType=cancellation">
                                                    <c:out value="${closedBillList.cancellationInvoice}"/> Summary</a>
                                                </c:if>--%>
                                                </c:if>
                                        </td>

                                        <td  height="30"><c:out value="${closedBillList.createddate}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.invoicecustomer}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.billingTypeName}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.grandTotal}"/></td>
                                        <c:set var="tripId" value="${closedBillList.tripId}" />
                                        <td  height="30" >

                                            <c:if test="${closedBillList.tripPod == 0}">
                                                <a href="viewTripPod.do?tripSheetId=<c:out value="${tripId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                </c:if>
                                                <c:if test="${closedBillList.tripPod > 0}">
                                                <a href="viewTripPod.do?tripSheetId=<c:out value="${tripId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                </c:if>
                                        </td>
                                        <td  height="30"><c:if test="${closedBillList.tripPod == 0}">
                                                Waiting For POD Upload
                                            </c:if>
                                        </td>
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>

                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 7);
                    </script>

                </form>

            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>

