<%-- 
    Document   : addParts
    Created on : Mar 2, 2009, 2:49:16 PM
    Author     : karudaiyar Subramaniam
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>PAPL</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>



<script language="javascript">    
 
function computeMrp(){
    var spriceVal = document.addparts.sprice.value;
    var w = document.addparts.vatId.selectedIndex;
    var taxPercentage = document.addparts.vatId.options[w].text;
    //alert(taxPercentage);
    document.addparts.mrp.value = parseFloat(spriceVal) + (parseFloat(spriceVal)* parseFloat(taxPercentage)/100);
}
function submitPage(value){ 
if(value == "save"){
  
       if(isSelect(document.addparts.mfrId,"manufactureName")){
           return;
       } else if(isSelect(document.addparts.modelId,"modelName")){
           return;
       } else if(isSelect(document.addparts.paplCode,"PaplCode")){
           return;
       }  else if(isSelect(document.addparts.itemName,"ItemName")){
           return;
       }else if(isSelect(document.addparts.uomId,"uomName")){
           return;
       }else if(isSelect(document.addparts.vatId,"vatName")){
           return;
       }else if(isSelect(document.addparts.groupId,"groupName")){
           return;
       }else if(textValidation(document.addparts.specification,"specification")){
           return;
       }else if(isSelect(document.addparts.reConditionable,"reConditionable")){
           return;
       }else if(isSelect(document.addparts.scrapUomId,"Scrap Uom Name")){
           return;
       }else if(textValidation(document.addparts.maxQuandity,"maxQuandity")){
           return;
       }else if(textValidation(document.addparts.roLevel,"RoLevel")){
           return;
       }else if(isSelect(document.addparts.rackId,"RackName")){
           return;
       }else if(isSelect(document.addparts.subRackId,"subRackName")){
           return;       
       }else if(isSelect(document.addparts.sprice,"Selling Price")){
           return;
       }

document.addparts.action = '/throttle/addParts.do';

if(confirm("Please check Whether item already exists")){
document.addparts.submit();
}
}
}



    
var httpReq;
var temp = "";
function ajaxData()
{
var url = "/throttle/getModels1.do?mfrId="+document.addparts.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}

function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();        
        setOptions(temp,document.addparts.modelId);
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}

    
function ajaxRackData()
{
var url = "/throttle/getSubRack.do?rackId="+document.addparts.rackId.value;  
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processRackAjax(); } ;
httpReq.send(null);
}


function processRackAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();        
        setOptions(temp,document.addparts.subRackId);
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}
    
    
    
    
/*function setOptions(text){    
    document.addparts.modelId.options.length=0;
    option0 = new Option("--select--",'0');
    document.addparts.modelId.options[0] = option0;
    
    if(text != ""){          
    var splt = text.split('~');
    var temp1;
    document.addparts.modelId.options[0] = option0;    
    for(var i=0;i<splt.length;i++){
        temp1 = splt[i].split('-');
        option0 = new Option(temp1[1],temp1[0])
        document.addparts.modelId.options[i+1] = option0;
    }    
    }
}*/
    

        function getItemNames(){
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
        } 


var checkItem;
function checkItemExists(choice,param)
{

var url='';
if(choice=='mfr' && param != '' ){    
url = "/throttle/handleCheckItem.do?choice=mfr&param="+document.addparts.itemCode.value;  
}else if(choice=='papl' && param != '' ){    
url = "/throttle/handleCheckItem.do?choice=papl&param="+document.addparts.paplCode.value;  
}else if(choice=='name' && param != '' ){    
url = "/throttle/handleCheckItem.do?choice=name&param="+document.addparts.itemName.value;  
}

if(url != ''){
        if (window.ActiveXObject)
        {
        checkItem = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else if (window.XMLHttpRequest)
        {
        checkItem = new XMLHttpRequest();
        }
        checkItem.open("GET", url, true);
        checkItem.onreadystatechange = function() { processCheckItem(choice); } ;
        checkItem.send(null);
}        
}


function processCheckItem(choice)
{
if (checkItem.readyState == 4)
{
	if(checkItem.status == 200)
	{
	temp = checkItem.responseText.valueOf();    
        document.addparts.checkName.value=temp;
        //alert(temp);
         if(temp!=''){
            document.getElementById("userNameStatus").innerHTML="Item Already Exists "+temp;
            if(choice=='mfr'){
                setTimeout(function () { document.addparts.itemCode.focus() }, 50);
                document.addparts.itemCode.select();
            }else if(choice=='papl' ){
                setTimeout(function () { document.addparts.paplCode.focus() }, 50);
                document.addparts.paplCode.select();
            }else if(choice=='name'){
                setTimeout(function () { document.addparts.itemName.focus() }, 50);
                document.addparts.itemName.select();
            }
        }else{
       		
            document.getElementById("userNameStatus").innerHTML="";
        }
	}
	else
	{
	alert("Error loading page\n"+ checkItem.status +":"+ checkItem.statusText);
	}
}
}





</script>
</head>
<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
<body onLoad="">
<form name="addparts"  method="post" >
<%@ include file="/content/common/path.jsp" %>
       
<%@ include file="/content/common/message.jsp" %>

<font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "> 
<div align="center" id="userNameStatus">&nbsp;&nbsp;</div></font>
<br>
     <table  border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
 <tr>
<td colspan="4" height="30" class="contenthead"><div class="contenthead">Add Item</div></td>
</tr>

<input type="hidden" name="sectionId" value='0'>

<tr>
<td class="text2" height="30"><font color="red">*</font><b>Category</b></td>
<td class="text2"  ><select class="textbox" name="categoryId" style="width:125px; " >
<option value="0">---Select---</option>
<c:if test = "${CategoryList != null}" >
<c:forEach items="${CategoryList}" var="Dept"> 
<option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
</c:forEach >
</c:if>   	
</select></td>

<td class="text2" height="30"><font color="red">*</font><b>MFR</b></td>
<td class="text2"  ><select class="textbox" name="mfrId" style="width:125px; " onChange="ajaxData();" >
<option value="0">---Select---</option>
<c:if test = "${MfrList != null}" >
<c:forEach items="${MfrList}" var="Dept"> 
<option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
</c:forEach >
</c:if>  	
</select></td>
</tr>
<tr>
<td class="text1" height="30"><font color="red">*</font><b>Model</b></td>
<td class="text1"  ><select class="textbox" style="width:125px; " name="modelId" >
<option value="0">---Select---</option>
<c:if test = "${ModelList != null}" >
<c:forEach items="${ModelList}" var="Dept"> 
<option value='<c:out value="${Dept.modelId}" />'><c:out value="${Dept.modelName}" /></option>
</c:forEach >
</c:if>  	
</select></td>   

<td class="text1" height="30"><font color="red">*</font><b>Item Code</b></td>
<td class="text1" height="30"><input name="itemCode" type="text" class="textbox" onfocusout="checkItemExists('mfr',this.value)" style="width:125px; " value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font><b>PAPL Code</b></td>
<td class="text2" height="30"><input name="paplCode" type="text" style="width:125px; " class="textbox"  onfocusout="checkItemExists('papl',this.value)" value=""></td>

<td class="text2" height="30"><font color="red">*</font><b>Item Name</b></td>
<td class="text2" height="30">
<input name="itemName" style="width:125px; " type="text" class="textbox"  onfocusout="checkItemExists('name',this.value)" value="">  </td>
</tr>
<tr>
<td class="text1" height="30"><font color="red">*</font><b>Uom</b></td>
<td class="text1"  ><select class="textbox" name="uomId" style="width:125px; " >
<option value="0">---Select---</option>
<c:if test = "${UomList != null}" >
<c:forEach items="${UomList}" var="Dept"> 
<option value='<c:out value="${Dept.uomId}" />'><c:out value="${Dept.uomName}" /></option>
</c:forEach >
</c:if>  	
</select></td>   

<td class="text1" height="30"><font color="red">*</font><b>Group Name</b></td>
<td class="text1"  ><select class="textbox" name="groupId" style="width:125px; " >
<option value="0">---Select---</option>
<c:if test = "${groupList != null}" >
<c:forEach items="${groupList}" var="group">
<option value='<c:out value="${group.groupId}" />'><c:out value="${group.groupName}" /></option>
</c:forEach >
</c:if>
</select></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font><b>Tax</b></td>
<td class="text2"  ><select class="textbox" name="vatId" style="width:125px; " >
<option value="0">---Select---</option>
<c:if test = "${vatList != null}" >
<c:forEach items="${vatList}" var="vat">
<option value='<c:out value="${vat.vatId}" />'><c:out value="${vat.vat}" /></option>
</c:forEach >
</c:if>
</select></td>

<td class="text2" height="30"><font color="red">*</font><b>Item Specification</b></td>
<td class="text2" height="30"><textarea class="textbox" name="specification" style="width:125px; "></textarea></td>
</tr>
<tr>
<td class="text1" height="30"><font color="red">*</font><b>Is Reconditionable</b></td>
<td class="text1" height="30"><select name="reConditionable" style="width:125px; " class="textbox">
    <option value="0">---Select---</option>
<option>Y</option>
<option>N</option>
</select></td>

<td class="text1" height="30"><font color="red">*</font><b>Scrap Unit</b></td>
<td class="text1"  ><select class="textbox" style="width:125px; " name="scrapUomId" >
<option value="0">---Select---</option>
<c:if test = "${UomList != null}" >
<c:forEach items="${UomList}" var="Dept"> 
<option value='<c:out value="${Dept.uomId}" />'><c:out value="${Dept.uomName}" /></option>
</c:forEach >
</c:if>  	
</select></td>   
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font><b>Maximum Qty</b></td>
<td class="text2" height="30"><input name="maxQuandity" style="width:125px; " type="text" class="textbox" value=""></td>

<td class="text2" height="30"><font color="red">*</font><b>Ro Level</b></td>
<td class="text2" height="30"><input name="roLevel" style="width:125px; " type="text" class="textbox" value=""></td>
</tr>
<tr>
<td class="text1" height="30"><font color="red">*</font><b>Rack</b></td>
<td class="text1"  ><select class="textbox" style="width:125px; " name="rackId" onChange="ajaxRackData();" >
<option value="0">---Select---</option>
<c:if test = "${getRackList != null}" >
<c:forEach items="${getRackList}" var="Dept"> 
<option value='<c:out value="${Dept.rackId}" />'><c:out value="${Dept.rackName}" /></option>
</c:forEach >
</c:if>  	
</select></td>   

<td class="text1" height="30"><font color="red">*</font><b>Sub Rack</b></td>
<td class="text1"  ><select class="textbox" style="width:125px; " name="subRackId" >
<option value="0">---Select---</option>
<c:if test = "${getSubRackList != null}" >
<c:forEach items="${getSubRackList}" var="Dept"> 
<option value='<c:out value="${Dept.subRackId}" />'><c:out value="${Dept.subRackName}" /></option>
</c:forEach >
</c:if>  	
</select>
<input type="hidden" value="" name="checkName" >
</td>   

</tr>

<tr>
<td class="text2" height="30"><font color="red">*</font><b>Selling Price</b></td>
<td class="text2" height="30"><input name="sprice" style="width:125px; " type="text" class="textbox" value="" onchange="computeMrp();"></td>

<td class="text2" height="30"><font color="red">*</font><b>MRP (with Tax)</b></td>
<td class="text2" height="30"><input name="mrp" style="width:125px; " readonly type="text" class="textbox" value=""></td>
</tr>

 


</table>
<br>
<center>
<input type="button" class="button" id="addButton" value="Save" name="save" onClick="checkItemExists('papl','papl');submitPage(this.name)">
&emsp;<input type="reset" class="button" value="Clear">
<input type="hidden" value="" name="temp" >

</center>
</form>
</body>
 
</html>


