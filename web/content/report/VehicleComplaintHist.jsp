
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    </head>
    <script>

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(){
            var chek=Validation();
            if(chek==1){            
            document.vehicleReport.action='/throttle/vehicleComplaints.do';
            document.vehicleReport.submit();
            }
        }
        function Validation(){              
            if(textValidation(document.vehicleReport.fromDate,'From Date')){
               return 0;              
                }
            if(textValidation(document.vehicleReport.toDate,'To Date')){
               return 0;              
                }
                                
                return 1;
            }
        function getVehicleNos(){
            
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }    
        function setValues(){
            document.vehicleReport.regNo.focus();
            if('<%=request.getAttribute("fromDate")%>'!='null'){
            document.vehicleReport.fromDate.value ='<%=request.getAttribute("fromDate")%>';
            document.vehicleReport.toDate.value ='<%=request.getAttribute("toDate")%>';
            document.vehicleReport.regNo.value='<%=request.getAttribute("regNo")%>';
           
            }
            if('<%=request.getAttribute("sectionId")%>' != 'null'){
             document.vehicleReport.sectionId.value='<%=request.getAttribute("sectionId")%>';
             }if('<%=request.getAttribute("technicianId")%>' != 'null'){
              document.vehicleReport.technicianId.value='<%=request.getAttribute("technicianId")%>';
              }
              if(document.vehicleReport.sectionId.value != '0'){
              getProblems(document.vehicleReport.sectionId.value);
              }
              if( '<%=request.getAttribute("problemId")%>' != 'null'){
              document.vehicleReport.probId.value = '<%=request.getAttribute("problemId")%>';
              }              
            }
            
            var httpRequest1;          
            function getProblems(secId) {  
                if(secId!='null' && secId!=0){
                
                var list1=eval("document.vehicleReport.probId");
                
                while (list1.childNodes[0]) {                          
                list1.removeChild(list1.childNodes[0])            
            }                           
            
                var url='/throttle/getProblems.do?secId='+secId;  
            
                if (window.ActiveXObject)
                    {
                        httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest1 = new XMLHttpRequest();
                        }
                        httpRequest1.open("POST", url, true);
                        httpRequest1.onreadystatechange = function() {go(); } ;
                        httpRequest1.send(null);
                    }
                    }

     function go() {                         
            if (httpRequest1.readyState == 4) {
                if (httpRequest1.status == 200) {
                    var response = httpRequest1.responseText;
                    var list=eval("document.vehicleReport.probId");
                    var details=response.split(','); 
                        var probId=0;
                        var probName ='--select--';
                        var x=document.createElement('option');
                        var name=document.createTextNode(probName);
                        x.appendChild(name);
                        x.setAttribute('value',probId)                                        
                        list.appendChild(x);                                                                                     
                    for (i=1; i <details.length; i++) {
                         temp = details[i].split('-');
                         probId= temp[0];                                        
                         probName = temp[1];
                         x=document.createElement('option');
                         name=document.createTextNode(probName);
                        x.appendChild(name);
                        x.setAttribute('value',probId)                                        
                        list.appendChild(x);                                                                                     
                    }   
                    setTimeout("fun()",1);
                }
            }
        }
            
            
    function fun()
    {
        var mod='<%=request.getAttribute("probId")%>';
        if(mod!='null'){
        document.vehicleReport.probId.value=mod;
        }
    }            
            
    
    function printPage()
    {       
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            

            
            
    </script>
    <body onload="getVehicleNos();setValues();">
    <form name="vehicleReport">
        <%@ include file="/content/common/path.jsp" %>                 
        <%@ include file="/content/common/message.jsp" %>

        <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Vehicle Complaint</li>
	</ul>
        <div id="first">
        <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
             <td align="left" height="30"><font color="red">*</font>Vehicle Number</td>
                <td height="30"><input name="regNo" id="regno" type="text" class="textbox"  size="20"></td>
                <td align="left" height="30">Section</td>
                <td><select class="textbox" name="sectionId" style="width:125px" onchange="getProblems(this.value);" >
                        <option value="0">---Select---</option>
                        <c:if test = "${SectionList != null}" >
                            <c:forEach items="${SectionList}" var="sec">
                                <option value='<c:out value="${sec.sectionId}" />'><c:out value="${sec.sectionName}" /></option>
                            </c:forEach >
                        </c:if>
                </select></td>
                <td> Problem </td>
                <td>
                <select  style='width:200px;' class="textbox" id="probId"  name="probId" >
                        <option  selectd value=''>---Select---</option>
                </select>
                </td>
            <td>&nbsp;</td>
            </tr>

        <tr>
        <td align="left" height="30">&nbsp;&nbsp;Technician</td>
        <td><select class="textbox" name="technicianId" style="width:125px">
            <option value="0">---Select---</option>
            <c:if test = "${TechnicianList != null}" >
                <c:forEach items="${TechnicianList}" var="tec">
                    <option value='<c:out value="${tec.technicianId}" />'><c:out value="${tec.technician}" /></option>
                </c:forEach >
            </c:if>
        </select>
        </td>
        <td align="left" height="30"><font color="red">*</font>From Date</td>
        <td height="30"><input name="fromDate" type="text" class="textbox"  size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.fromDate,'dd-mm-yyyy',this)"/></td>
        <td align="left" height="30"><font color="red">*</font>To Date </td>
        <td height="30"><input name="toDate" type="text" class="textbox"  size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.toDate,'dd-mm-yyyy',this)"/></td>
        <td><input type="button" class="button" value="Search" onclick="submitPage();">    </td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
        
        
        <c:if test = "${complaintList != null}" >
            <center> <input type="button" class="button" name="print" value="print" onClick="printPage();" > </center>
        <div id="printPage" >
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td height="30" class="contenthead" colspan="11" align="center" ><b>Vehicle Complaint </b></td>
                </tr>
                <tr>
                    <td class="contentsub" height="30">Date</td>
                    <td class="contentsub" height="30">JobCard </td>
<!--                    <td class="contentsub" height="30">Vehicle <br>Number</td>-->
                    <td class="contentsub" height="30">Section</td>
                    <td class="contentsub" height="30">Complaint</td>
                    <td class="contentsub" height="30">Activity</td>
                    <td class="contentsub" height="30">Technician</td>
                    <td class="contentsub" height="30">PCD</td>                    
                    <td class="contentsub" height="30">ACD</td>
                    <td class="contentsub" height="30">status</td>
                </tr>
                <% int index = 0;%> 
                <c:forEach items="${complaintList}" var="complaint"> 
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
                    
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.createdDate}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.jobCardId}" /></td>
<!--                        <td class="<%=classText %>" height="30"><c:out value="${complaint.regNo}" /></td>-->
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.section}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.problem}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.activity}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.technician}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.pcd}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${complaint.acd}" /></td>
                        <c:choose>
		                                               <c:when test="${complaint.problemStatus=='C' || comp.status=='c'}" >
		                                               <td class="<%=classText %>" height="30">Completed</td>                                               
		                                               </c:when>
		                                               <c:otherwise>
		                                                <td class="<%=classText %>" height="30">Pending</td>                                                                                          
		                                               </c:otherwise>    
                                    </c:choose>

                        <td class="<%=classText %>" height="30"><c:out value="${complaint.remarks}" /></td>
                    </tr>
                    <% index++;%>
                </c:forEach>
                
            </table>
<style>
.text1 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}        
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}

.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:center;
padding-bottom:8px;
}

</style>              
            </div>
        </c:if>
    </form>
</html>

