
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PutAway List</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>


</head>
<body>
    <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
        <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>
                <div id="printDiv" >
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 80%;"  align="center"  cellpadding="0"  cellspacing="0" border="1">
                        <tr >
                            <td  style="margin: 0px;height:25px;width:175px;font-weight:bold;">
                                <div ><img src="/throttle/images/HSSupplyLogo.png" style="width: 80px; height: 50px;"></div>                                
                            </td>
                            <td align="left" style="height:25px;width:525px;font-size:15px;font-weight:bold;size: 5px;">
                                &ensp;HS Supply Solutions Pvt. Ltd.<br>
                            &ensp;30, Chandralaya Apartment, <br>
                            &ensp;Judge Jambulingam Rd, Mylapore, <br>
                            &ensp;Chennai, Tamil Nadu. 600004
                            </td>
                            <td >
                                <div id="externalbox" style="width:3in;margin: 10px;">
                                    <div align="center" style="font-size:12px;"><b></b></div>
                                    <div id="inputdata"></div>
                                    <div align="center" style="font-size:12px;"><b>Date : 01-11-2020 10:10:00</b></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                                     
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 80%;"  align="center"  cellpadding="0"  cellspacing="0" border>
                            <thead>
                                <tr height="40">
                                    <th style="font-size:10px;width: 5%;">S.No</th>
                                    <th style="font-size:10px;width: 18%;">Customer</th>
                                    <th style="font-size:10px;width: 5%;">GRN No</th>
                                    <th style="font-size:10px;width: 9%;">Item Code</th>
                                    <th style="font-size:10px;width: 7%;">Spec</th>
                                    <th style="font-size:10px; width: 7%;">Qty</th>
                                    <th style="font-size:10px;width: 10%;">UOM</th>
                                    <th style="font-size:10px;width: 10%;">Rack</th>
                                    <th style="font-size:10px;width: 10%;">SubRack</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">1</td>
                                        <td align="center" style="font-size:10px;">Voltas </td>
                                        <td align="center" style="font-size:10px;">1337 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH45 </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1</td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A101 </td>
                                        <td align="center" style="font-size:10px;">12 </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">2</td>
                                        <td align="center" style="font-size:10px;">Voltas </td>
                                        <td align="center" style="font-size:10px;">1337 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH45 </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1</td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">B101 </td>
                                        <td align="center" style="font-size:10px;">15 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">3</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER101 </td>
                                        <td align="center" style="font-size:10px;">2019 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A101 </td>
                                        <td align="center" style="font-size:10px;">13 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">4</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER101 </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A101 </td>
                                        <td align="center" style="font-size:10px;">13 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">5</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">6</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">7</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">8</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">9</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">10</td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">COOKER104</td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">1 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        <td align="center" style="font-size:10px;">A103 </td>
                                        <td align="center" style="font-size:10px;">16 </td>
                                    </tr>

                            </tbody>    
                    </table>
                    <br>
                    <br>
                    <b>&emsp;&emsp;Put Away Person :</b> Ramkumar .K
                </div>
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
        <%--</c:if>--%>
    </form>
    <script type="text/javascript">
        /* <![CDATA[ */
        function get_object(id) {
            var object = null;
            if (document.layers) {
                object = document.layers[id];
            } else if (document.all) {
                object = document.all[id];
            } else if (document.getElementById) {
                object = document.getElementById(id);
            }
            return object;
        }
        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
        get_object("inputdata").innerHTML = DrawCode39Barcode('PK3345', 0);
        /* ]]> */
    </script>
</body>
</html>









