<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.accountReceivable.action = '/throttle/stockReport.do?param=ExportExcel';
                    document.accountReceivable.submit();
                } else {
                    document.accountReceivable.action = '/throttle/stockReport.do?param=Search';
                    document.accountReceivable.submit();
                }
            }
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
                
            function viewStockDetails(itemId,whId,tot) {
                window.open('/throttle/viewStockQtyDetails.do?itemId=' + itemId + '&whId=' + whId +'&inv='+tot+ '&param=Search', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Stock Report" text="Stock Report"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.WMS Report" text="WMS Report"/></a></li>
                <li class=""><spring:message code="hrms.label.Stock Report" text="Stock Report"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();sorter.size(5);">
                    <form name="accountReceivable"   method="post">
                        <%@ include file="/content/common/message.jsp" %>
                            <table class="table table-info mb10 table-hover" id="bg" >
                                <thead>
                                    <tr>
                                        <th colspan="2" height="30" >Stock Report</th>
                                    </tr>
                                </thead>
                            </table>
                            <table class="table table-info mb30 table-hover" id="report" >
                                <tr height="30">
                                    <td>Item Code</td>
                                    <td height="30">
                                        <input type="text" name="itemCode" id="itemCode" value="<c:out value='${itemCode}'/>" class="form-control" style="width:250px;height:40px"/>
                                    </td>
                                    <td><input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    <td><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                </tr>
                            </table>

                            <c:if test="${stockQtyList != null}">
                                <table class="table table-info mb30 table-hover" id="table" >
                                    <thead>
                                        <tr>
                                            <th align="center">S.No</th>
                                            <th align="center">Item Name</th>
                                            <th align="center">Item Code</th>
                                            <th align="center">Warehouse Name</th>                                            
                                            <th align="center">HSN Code</th>
                                            <th align="center">Qty</th>
                                            <th align="center">BBND</th>
                                            <th align="center">Total</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <% int index = 1;%>
                                        <c:forEach items="${stockQtyList}" var="stock">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                    <tr>
                                                        <td><%=index++%></td>
                                                        <td align="center"><c:out value="${stock.itemName}"/></td>
                                                        <td align="center"><c:out value="${stock.productCode}"/></td>
                                                        <td align="center"><c:out value="${stock.whName}"/></td>                                                        
                                                        <td align="center"><c:out value="${stock.hsnCode}"/></td>
                                                        <td align="center">
                                                           <a href="#" onclick="viewStockDetails('<c:out value="${stock.itemId}"/>','<c:out value="${stock.whId}"/>','qty');"><c:out value="${stock.itemQty}"/><c:set var="totqty" value="${totqty+stock.itemQty}"/></a>
                                                        </td>
                                                        <td align="center"> <a href="#" onclick="viewStockDetails('<c:out value="${stock.itemId}"/>','<c:out value="${stock.whId}"/>','bbnd');"><c:out value="${stock.bbnd}"/><c:set var="bbndtot" value="${bbndtot+stock.bbnd}"/></a></td>
                                                        <td align="center"><c:out value="${stock.itemQty+stock.bbnd}"/><c:set var="tot" value="${tot+stock.itemQty+stock.bbnd}"/></td>
                                                    </tr>

                                        </c:forEach>
                                                    <tr>
                                                        <td align="center"></td>
                                                        <td align="center" colspan="2"><b>Total</b></td>
                                                        <td align="center"></td>
                                                        <td align="center"></td>
                                                        <td align="center"><b><c:out value="${totqty}"/></b></td>
                                                        <td align="center"><b><c:out value="${bbndtot}"/></b></td>
                                                        <td align="center"><b><c:out value="${tot}"/></b></td>
                                                    </tr>
                                    </tbody>
                                </table>
                            </c:if>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        
                    </form>
                </body>    
            </div>
        </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>