<%-- 
    Document   : gpsLogReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">



            function viewLogDetails(tripId) {
                window.open('/throttle/viewTripLog.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }


            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleGPSLog.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleGPSLog.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
                if('<%=request.getAttribute("tripId")%>' != 'null'){
                    document.getElementById('tripId').value='<%=request.getAttribute("tripId")%>';
                }
            }
              
                
                function viewVehicleDetails(vehicleId) {
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="GPS Log Report"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body onload="setValue();sorter.size(20);">
        <form name="accountReceivable"   method="post">
            <%@ include file="/content/common/message.jsp" %>
                  <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >GPS Log Report</th>
		</tr>
		    </thead>
    </table>
            
               <table class="table table-info mb30 table-hover" id="report" >
                                    <tr height="30">
                                         <td align="center">Vehicle No</td>
                                        <td height="30">
                                            <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                            <input type="text" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>"  class="form-control" style="width:250px;height:40px"/>
                                        </td>
                                         <td align="center">Trip Code</td>
                                        <td height="30">
                                            <input type="text" name="tripId" id="tripId" value=""  class="form-control" style="width:250px;height:40px"/>
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <td align="center"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                                        <td align="center"><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>" ></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td  align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td><input type="button" class="btn btn-success" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <br>
            <c:if test="${logDetailsSize != '0'}">
                   <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="30">
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>Trip Code</h3></th>
                           <th align="center"><h3>Trip Date</h3></th>
                           <th align="center"><h3>Current Location</h3></th>
                            <th align="center"><h3>Distance Travelled</h3></th>
                            <th align="center"><h3>Current Temperature</h3></th>
                            <th align="center"><h3>Log Date</h3></th>
                        </tr> 
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${logDetails}" var="logDetails">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="90" class="<%=classText%>">
                                               <a href="#" onclick="viewVehicleDetails('<c:out value="${logDetails.vehicleId}"/>')"><c:out value="${logDetails.vehicleNo}"/></a></td>

                                        <td width="90" class="<%=classText%>">
                                                <a href="#" onclick="viewLogDetails('<c:out value="${logDetails.tripId}"/>')"><c:out value="${logDetails.tripCode}"/></a>
                                       </td>
                                            <td class="<%=classText%>" ><c:out value="${logDetails.tripDate}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.location}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.distance}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.currentTemperature}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.logDate}"/></td>
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
          <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" >5</option>
                        <option value="10">10</option>
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            </c:if>
        </form>
    </body>    

    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>