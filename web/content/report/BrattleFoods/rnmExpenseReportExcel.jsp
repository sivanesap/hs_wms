<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "RnmExpenseSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                  <c:if test="${vehicleList != null }">
                  <table align="center" border="1" id="table" class="sortable" style="width:1000px;" >

                    <thead>
                    <tr>
                    <th><h3> S.No</h3></th>
                    <th><h3>Vehicle no</h3></th>
                    <th colspan="3" align="center" ><center><h3><c:out value="${firstmonth}"/></h3></center></th>
                    <th colspan="3" align="center" ><center><h3><c:out value="${secondMonth}"/></h3></center></th>
                    <th colspan="3" align="center" ><center><h3><c:out value="${thirdMonth}"/></h3></center></th>



<!--                    <th colspan="3" ><h3>Second month</h3></th>
                    <th colspan="3"><h3>Third month</h3></th>-->
                    </tr>
                    <tr>
                    <th><h3></h3></th>
                    <th><h3></h3></th>

                    <th><h3>Container</h3></th>
                    <th><h3>Chasis</h3></th>
                    <th><h3>Reffer</h3></th>

                    <th><h3>Container</h3></th>
                    <th><h3>Chasis</h3></th>
                    <th><h3>Reffer</h3></th>

                    <th><h3>Container</h3></th>
                    <th><h3>Chasis</h3></th>
                    <th><h3>Reffer</h3></th>
<!--                    <th><h3>Container</h3></th>
                    <th><h3>Chesis</h3></th>
                    <th><h3>Reffer</h3></th>
                    <th><h3>Container</h3></th>
                    <th><h3>Chesis</h3></th>
                    <th><h3>Reffer</h3></th>-->
                                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                       
                            
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                             <c:forEach items="${vehicleList}" var="vehicle">
                                 <tr>
                                <td width="30" class="text1"><%=index++%></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.regNo}"/></td>
                                 <c:forEach items="${RnMExpenseList1}" var="expense1">
                                      <c:if test="${vehicle.vehicleId eq expense1.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense1.chasisAmount}"/>
                                </td>
                                <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense1.referAmount}"/>
                                    </td>
                                <td  width="100" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense1.containerAmount}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>
                                    <c:forEach items="${RnMExpenseList2}" var="expense2">
                                    <c:if test="${vehicle.vehicleId eq expense2.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense2.chasisAmount}"/>
                                </td>
                                <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense2.referAmount}"/>
                                </td>
                                <td  width="100" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense2.containerAmount}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>
                                 <c:forEach items="${RnMExpenseList3}" var="expense3">
                                    <c:if test="${vehicle.vehicleId eq expense3.vehicleId  }">
                                 <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense3.chasisAmount}"/>
                                </td>
                                <td  width="80" class="text1">
                                  <fmt:formatNumber pattern="##0.00" value="${expense3.referAmount}"/>
                                </td>
                                <td  width="100" class="text1">
                                   <fmt:formatNumber pattern="##0.00" value="${expense3.containerAmount}"/>
                                </td>
                                  </c:if>
                                 </c:forEach>


                            </tr>
                        </c:forEach>




                    </tbody>
                </table>

                
            </c:if>
                     
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </form>
    </body>
</html>
