<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
          

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.fcWiseTripSummary.action = '/throttle/handleRNMReportExcel.do?param=ExportExcel';
                    document.fcWiseTripSummary.submit();
                } else {
                    document.fcWiseTripSummary.action = '/throttle/handleRNMReportExcel.do?param=Search';
                    document.fcWiseTripSummary.submit();
                }
            }
        </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Repair And Maintainance" text="Repair And Maintainance"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.Repair And Maintainance" text=" Repair And Maintainance"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

    </head>
    <body>
        <form name="fcWiseTripSummary" method="post">
            
            <%@ include file="/content/common/message.jsp"%>
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="2">Repair And Maintainance Analysis Report</th></tr></thead>
                            <!--        <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>   -->
                                    <tr>
                                       <td><font color="red">*</font>Type</td>
                                       <td height="30">
                                           <select id="type" name="type" class="form-control" style="width:250px;height:40px">
                                               <option value="2">Primary</option>

                                               <option value="1">Secondary</option>
                                           </select>
                                       </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search">
                                       <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel">
                                        </td>
                                    </tr>

                                </table>
                                 
                     <br/>               
                     <c:if test="${rnmList != null }">
                    <table  id="table" class="table table-info mb30 table-hover" >
                    <thead>
                    <tr style="border-top:1px solid white;height:50px">
                    <th style="border-left:1px solid white"><h3>S.No</h3></th>
                    <th style="border-left:1px solid white"><h3>Vehicle no</h3></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${firstmonth}"/></h3></center></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${secondMonth}"/></h3></center></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${thirdMonth}"/></h3></center></th>
                                 
                     

                    </tr>
                    <tr  style="height:50px">
                    <th style="border-left:1px solid white"><h3></h3></th>
                    <th style="border-left:1px solid white"><h3></h3></th>
                    
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>

                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>

                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>


                 
                                        </tr>
                    </thead>
                    <tbody>
                                     
                        <% int index = 1;%>
                                 <c:forEach items="${rnmList}" var="vehicle">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                     
                      
                                     
                           
                             <c:set var="totalmonth1Accidental" value="${totalmonth1Accidental+vehicle.month1Accidental}"/>
                             <c:set var="totalmonth2Accidental" value="${totalmonth2Accidental+vehicle.month2Accidental}"/>
                             <c:set var="totalmonth3Accidental" value="${totalmonth3Accidental+vehicle.month3Accidental}"/>
                             <c:set var="totalmonth1Asset" value="${totalmonth1Asset+vehicle.month1Asset}"/>
                             <c:set var="totalmonth2Asset" value="${totalmonth2Asset+vehicle.month2Asset}"/>
                             <c:set var="totalmonth3Asset" value="${totalmonth3Asset+vehicle.month3Asset}"/>
                             <c:set var="totalmonth1BreakDownReeferWorks" value="${totalmonth1BreakDownReeferWorks+vehicle.month1BreakDownReeferWorks}"/>
                             <c:set var="totalmonth2BreakDownReeferWorks" value="${totalmonth2BreakDownReeferWorks+vehicle.month2BreakDownReeferWorks}"/>
                             <c:set var="totalmonth3BreakDownReeferWorks" value="${totalmonth3BreakDownReeferWorks+vehicle.month3BreakDownReeferWorks}"/>
                              <c:set var="totalmonth1BreakDownVehicleWorks" value="${totalmonth1BreakDownVehicleWorks+vehicle.month1BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth2BreakDownVehicleWorks" value="${totalmonth2BreakDownVehicleWorks+vehicle.month2BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth3BreakDownVehicleWorks" value="${totalmonth3BreakDownVehicleWorks+vehicle.month3BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth1RegularReeferService" value="${totalmonth1RegularService+vehicle.month1RegularReeferService}"/>
                             <c:set var="totalmonth2RegularReeferService" value="${totalmonth2RegularReeferService+vehicle.month2RegularReeferService}"/>
                             <c:set var="totalmonth3RegularReeferService" value="${totalmonth3RegularReeferService+vehicle.month3RegularReeferService}"/>
                             <c:set var="totalmonth1RegularVehicleService" value="${totalmonth1RegularVehicleService+vehicle.month1RegularVehicleService}"/>
                             <c:set var="totalmonth2RegularVehicleService" value="${totalmonth2RegularVehicleService+vehicle.month2RegularVehicleService}"/>
                             <c:set var="totalmonth3RegularVehicleService" value="${totalmonth3RegularVehicleService+vehicle.month3RegularVehicleService}"/>
                                 <tr>
                                <td width="10" class="text1"><%=index++%></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.regNo}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1RegularVehicleService}"/></td>

                                <td  width="80" class="text1"><c:out value="${vehicle.month2Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2RegularVehicleService}"/></td>

                                <td  width="80" class="text1"><c:out value="${vehicle.month3Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3RegularVehicleService}"/></td>
                   
                                                                                         
                            </tr>
                        </c:forEach>
                       
                  <tr>
                                 <td  width="30" class="text1"  align="center"><b>&nbsp;Grand</b></td>
                                 <td  width="30" class="text1"  align="center"><b>&nbsp;Total</b></td>
                                 <td  width="30" class="text1"><b> <fmt:formatNumber pattern="##0.00"  value="${totalmonth1Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1RegularVehicleService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2RegularVehicleService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3RegularVehicleService}"/></b></td>
                                 
                            </tr>

                                          </tbody>
                </table>
                  </c:if>
            
                     
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

