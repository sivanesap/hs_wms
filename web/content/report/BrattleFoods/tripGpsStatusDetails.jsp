<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <script type="text/javascript">


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#tripCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getTripCode.do",
                    dataType: "json",
                    data: {
                       tripCode: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                            $('#tripId').val('');
                            $('#tripCode').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#tripId').val(tmp[0]);
                $('#tripCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        });
        
        
         function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{ 
                  if(value == "ExportExcel"){
                     document.tripGpsStatusDetails.action = '/throttle/viewGpsStatusDetais.do?param=ExportExcel';
                     document.tripGpsStatusDetails.submit();
                    }
                else{
                        document.tripGpsStatusDetails.action = '/throttle/viewGpsStatusDetais.do?param=Search';
                        document.tripGpsStatusDetails.submit();
                    }
                }     
            
        }
    </script>
    
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="default text"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body>
        <form name="tripGpsStatusDetails" method="post" >
            <%@ include file="/content/common/message.jsp" %>
         <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >View Finance Advice</th>
		</tr>
		    </thead>
    </table>
    <table class="table table-info mb30 table-hover" id="bg" >
                           <%!
                           public String NullCheck(String inputString)
                                {
                                        try
                                        {
                                                if ((inputString == null) || (inputString.trim().equals("")))
                                                                inputString = "";
                                        }
                                        catch(Exception e)
                                        {
                                                                inputString = "";
                                        }
                                        return inputString.trim();
                                }
                           %>

                           <%

                            String today="";
                            String fromday="";
                           
                            fromday = NullCheck((String) request.getAttribute("fromdate"));
                            today = NullCheck((String) request.getAttribute("todate"));

                            if(today.equals("") && fromday.equals("")){
                            Date dNow = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dNow);
                            cal.add(Calendar.DATE, 0);
                            dNow = cal.getTime();

                            Date dNow1 = new Date();
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(dNow1);
                            cal1.add(Calendar.DATE, -6);
                            dNow1 = cal1.getTime();

                            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                            today = ft.format(dNow);
                            fromday = ft.format(dNow1);
                            }

            %>
                                    <tr>
                                        <td><font color="red">*</font>Trip Code</td>
                                        <td height="30"><input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>" class="form-control" style="width:250px;height:40px"><input type="text" name="tripCode" id="tripCode" value="<c:out value="${tripCode}"/>" class="form-control" style="width:250px;height:40px"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  style="width:250px;height:40px"  onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker"  style="width:250px;height:40px" onclick="ressetDate(this);" value="<%=today%>"></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>Trip Status</td>
                                        <td height="30"><select name="tripStatus" id="tripStatus"    class="form-control" style="width:240px;height:40px">
                                                <c:if test="${tripStatus ==''}">
                                                <option value="0" selected>All</option>
                                                <option value="1">Trip Start Not Updated</option>
                                                <option value="2">Trip End Not Updated</option>
                                                <option value="3">Trip Details Not Updated</option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='0'}">
                                                <option value="0" selected>All</option>
                                                <option value="1">Trip Start Not Updated</option>
                                                <option value="2">Trip End Not Updated</option>
                                                <option value="3">Trip Details Not Updated</option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='1'}">
                                                <option value="0">All</option>
                                                <option value="1" selected>Trip Start Not Updated</option>
                                                <option value="2">Trip End Not Updated</option>
                                                <option value="3">Trip Details Not Updated</option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='2'}">
                                                <option value="0">All</option>
                                                <option value="1">Trip Start Not Updated</option>
                                                <option value="2" selected>Trip End Not Updated</option>
                                                <option value="3">Trip Details Not Updated</option>
                                                </c:if>     
                                                <c:if test="${tripStatus =='3'}">
                                                <option value="0">All</option>
                                                <option value="1">Trip Start Not Updated</option>
                                                <option value="2">Trip End Not Updated</option>
                                                <option value="3" selected>Trip Details Not Updated</option>
                                                </c:if>     
                                            </select> </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                         <td><input type="button" class="btn btn-success"  name="search" onclick="submitPage(this.name);" value="Search"></td>
                                         <td><input type="button" class="btn btn-success"  name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <c:if test = "${tripGpsStatusDetails != null}" >
                        <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="60">
                            <th>S.No</th>
                            <th>TripCode</th>                            
                            <th>VehicleNo</th>                            
                            <th>GPSStatus</th>
                            <th>CustomerRoute</th>
                            <th>TripStatus</th>
                            <th>StartInd</th>
                            <th>StartTime</th>
                            <th>StartGPSInd</th>
                            <th>StartGPSTime</th>
                            <th>TripStartAttempt</th>
                            <th>StartErrorCode</th>
                            <th>StartErrorMessage</th>
                            <th>EndStatus</th>
                            <th>EndTime</th>
                            <th>EndGPSStatus</th>
                            <th>EndGPSUpdateTime</th>
                            <th>EndAttempt</th>
                            <th>EndErrorCode</th>
                            <th>EndErrorMessage</th>
                            <th>DetailsGPSStatus</th>
                            <th>TripDetailsGPSUpdateTime</th>
                            <th>TripDetailsAttempt</th>
                            <th>TripDetailsErrorCode</th>
                            <th>TripDetailsErrorMessage</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${tripGpsStatusDetails}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${fd.tripCode}"/> </td>                                
                                <td align="left" ><c:out value="${fd.vehicleNo}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsSystem}"/> </td>                                
                                <td align="left" ><c:out value="${fd.routeName}"/> </td>                                
                                <td align="left" ><c:out value="${fd.statusName}"/> </td>                                
                                <td align="left" ><c:out value="${fd.tsStartInd}"/> </td>                                
                                <td align="left" ><c:out value="${fd.tripStartDateTime}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsStartInd}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsStartUpdateTime}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsStartAttempt}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsStartErrorCode}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsStartErrorMsg}"/> </td>                                
                                <td align="left" ><c:out value="${fd.tsEndInd}"/> </td>                                
                                <td align="left" ><c:out value="${fd.tripEndDateTime}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsEndInd}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsEndUpdateTime}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsEndAttempt}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsEndErrorCode}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsEndErrorMsg}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsTripDetailsInd}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsTripDetailsDatetime}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsTripDetailsAttempt}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsTripDetailsErrorCode}"/> </td>                                
                                <td align="left" ><c:out value="${fd.gpsTripDetailsErrorMsg}"/> </td>                                
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

        </form>
    </body>


</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>