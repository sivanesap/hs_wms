<%-- 
    Document   : driverSettlementReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
        <script type="text/javascript">
            

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getVehicleNo.do",
                            dataType: "json",
                            data: {
                                vehicleNo: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if(items == ''){
                                    alert("Invalid Vehicle No");
                                    $('#vehicleNo').val('');
                                    $('#vehicleId').val('');
                                    $('#vehicleNo').fous();
                                }else{
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        $('#vehicleNo').val(value);
                        $('#vehicleId').val(id);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };

                $('#primaryDriver').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getDriverName.do",
                            dataType: "json",
                            data: {
                                driverName: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                var primaryDriver = $('#primaryDriver').val();
                                if(items == '' && primaryDriver != ''){
                                    alert("Invalid Primary Driver Name");
                                    $('#primaryDriver').val('');
                                    $('#primaryDriverId').val('');
                                    $('#primaryDriver').focus();
                                }else{
                                }
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        $('#primaryDriver').val(value);
                        $('#primaryDriverId').val(id);
                        $('#secondaryDriverOne').focus();
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };
                });


                function submitPage(value) {
                    if(document.getElementById('fromDate').value == ''){
                        alert("please select from date");
                        document.getElementById('fromDate').focus();
                    }else{
                        if(value == 'ExportExcel'){
                            document.accountReceivable.action = '/throttle/handleDriverSettlementReport.do?param=ExportExcel';
                            document.accountReceivable.submit();
                        }else{
                            document.accountReceivable.action = '/throttle/handleDriverSettlementReport.do?param=Search';
                            document.accountReceivable.submit();
                        }
                    }
                }
            
                function setValue(){
                    if('<%=request.getAttribute("page")%>' !='null'){
                        var page = '<%=request.getAttribute("page")%>';
                        if(page == 1){
                            submitPage('search');
                        }
                    }
                }


        </script>

        <style type="text/css">





            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
            }



        </style>
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Customer Wise Profit"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body onload="">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/message.jsp" %>
               <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >Customer Wise Profit</th>
		</tr>
		    </thead>
    </table>
            <table class="table table-info mb30 table-hover" id="report" >
                                    <tr>
                                        <td class="text1" height="30">Primary Driver Name</td>
                                        <td class="text1" height="30">
                                            <input type="text"  class="form-control" style="width:250px;height:40px" id="primaryDriver"  name="primaryDriver" autocomplete="off" value="<c:out value="${primaryDriverName}"/>"/>
                                            <input type="hidden"  class="form-control" style="width:250px;height:40px" id="primaryDriverId"  name="primaryDriverId" autocomplete="off" value="<c:out value="${primaryDriverId}"/>"/>
                                        </td>
                                        <td class="text2" height="30">Vehicle No</td>
                                        <td class="text2" height="30">
                                            <input type="text"  class="form-control" style="width:250px;height:40px" id="vehicleNo"  name="vehicleNo" autocomplete="off" value="<c:out value="${vehicleNo}"/>"/>
                                            <input type="hidden"  class="form-control" style="width:250px;height:40px" id="vehicleId"  name="vehicleId" autocomplete="off" value="<c:out value="${vehicleId}"/>"/>
                                        </td>
                                        <td class="text1" height="30">Trip Code</td>
                                        <td class="text1" height="30"><input type="text"  class="form-control" style="width:250px;height:40px" id="tripId"  name="tripId" autocomplete="off" value="<c:out value="${tripId}"/>"/></td>
                                    </tr>
                                    <tr>
                                        <td class="text2" height="30">Settlement From Date</td>
                                        <td class="text2" height="30"><input type="text" class="datepicker" id="fromDate" style="width:250px;height:40px" name="fromDate" autocomplete="off" value="<c:out value="${fromDate}"/>"/></td>
                                        <td class="text2" height="30">Settlement To Date</td>
                                        <td class="text2" height="30"><input type="text" class="datepicker" style="width:250px;height:40px"  id="toDate"  name="toDate" autocomplete="off" value="<c:out value="${toDate}"/>"/></td>
                                        <td class="text2" height="30" colspan="2"></td>
                                    </tr>

                                    <tr>

                                        <td class="text1" height="30" colspan="2" align="center"></td>
                                        <td class="text1" height="30"  align="center"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                        <td class="text1" height="30"  align="center"><input type="button" class="btn btn-success" value="Search" name="search" onClick="submitPage(this.name)"/></td>

                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <c:if test = "${driverSettlementDetails == null}" >
                <center><font color="red">No Records Found</font></center>
            </c:if>
           

            <c:if test = "${driverSettlementDetails != null}" >
                    <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="80" >
                            <th>S.No</th>
                            <th>Trip Code</th>
                            <th>Vehicle No</th>
                            <th>Settlement Date</th>
                            <th>Driver Name</th>
                            <th>Run Km</th>
                            <th>Run Hour</th>
                            <th>Fuel Price</th>
                            <th>Diesel Used</th>
                            <th>RCM Allocation</th>
                            <th>BPCL Allocation</th>
                            <th>Extra Expense</th>
                            <th>Total Misc</th>
                            <th>Bhatta</th>
                            <th>Total Expense</th>
                            <th>Balance</th>
                            <th>Start Balance</th>
                            <th>End Balance</th>
                            <th>Pay Mode</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${driverSettlementDetails}" var="dsList">

                            <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left"><c:out value="${dsList.tripCode}"/></td>
                                <td align="left"><c:out value="${dsList.vehicleNo}"/></td>
                                <td align="left"><c:out value="${dsList.settlementDate}"/></td>
                                <td align="left"><c:out value="${dsList.empName}"/></td>
                                <td align="left"><c:out value="${dsList.distance}"/></td>
                                <td align="left"><c:out value="${dsList.runHour}"/></td>
                                <td align="left"><c:out value="${dsList.fuelPrice}"/></td>
                                <td align="left"><c:out value="${dsList.dieselUsed}"/></td>
                                <td align="left"><c:out value="${dsList.rcmAllocation}"/></td>
                                <td align="left"><c:out value="${dsList.bpclAllocation}"/></td>
                                <td align="left"><c:out value="${dsList.extraExpense}"/></td>
                                <td align="left"><c:out value="${dsList.totalMiscellaneous}"/></td>
                                <td align="left"><c:out value="${dsList.bhatta}"/></td>
                                <td align="left"><c:out value="${dsList.totalExpense}"/></td>
                                <td align="left"><c:out value="${dsList.balance}"/></td>
                                <td align="left"><c:out value="${dsList.startingBalance}"/></td>
                                <td align="left"><c:out value="${dsList.endingBalance}"/></td>
                                <td align="left"><c:out value="${dsList.payMode}"/></td>
                                <td align="left"><c:out value="${dsList.remarks}"/></td>
                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>

                <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                </script>

                <table>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                </table>
                <br/>
                <br/>
                <c:if test = "${customerWiseProfitList != null}" >
                    <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                        <tr height="25" align="right">
                            <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                            <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRevenue}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${expenses}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                            <td width="150" align="right">
                                <c:if test="${profit > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                </c:if>
                                <c:if test="${profit <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                </c:if>
                            </td>
                        </tr>
                        <tr height="25">
                            <td style="background-color: #6374AB; color: #ffffff">Total Profit % </td>
                            <td width="150" align="right">
                                <c:if test="${perc/count > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                </c:if>
                                <c:if test="${perc/count <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                </c:if>
                            </td>
                        </tr>
                    </table>
                </c:if>
            </c:if>
        </form>
    </body>    

    
    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

