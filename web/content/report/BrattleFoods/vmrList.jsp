<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>

<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <body onload="sorter.size(1);">
        <form name="BPCLTransaction" method="post">
            <c:if test="${vmrList == null}">
                <center><font color="red">No Records Found</font></center>
            </c:if>
            <c:if test="${vmrList != null}">
                <% int index = 1;%>
                <c:forEach items="${vmrList}" var="update">

                    <table  class="table table-info mb30 table-hover">
                        <thead>
                        <tr height="50">
                            <th colspan='6'>Trip VMR Data From Date <c:out value="${update.fromDate}"/> To Date <c:out value="${update.toDate}"/> </th>
                        </tr>
                        </thead>
                        <tr>
                            <td align='left' colspan='2'>Status</td>
                            <td align='center'>No Of Trips</td>
                            <td align='center' colspan='3'>Revenue</td>
                        </tr>
                        <tr>
                            <td align='left' colspan='2'>Trip End</td>
                            <td align='center'> <c:out value="${update.endCount}"/> </td>
                        <td align='right' colspan='3'> <c:out value="${update.endRevenue}"/> </td>
                        </tr>
                        <tr>
                            <td align='left' colspan='2'>Trip Start</td>
                            <td align='center'> <c:out value="${update.startCount}"/>  </td>
                        <td align='right' colspan='3'> <c:out value="${update.startRevenue}"/> </td>
                        </tr>
                        <tr><td colspan='6'></td></tr>
                        <tr>
                            <th colspan='6'>Previous Month Data </th>
                        </tr>
                        <tr>
                            <td align='left' colspan='2'>Status</td>
                            <td align='center'>No Of Trips</td>
                            <td align='center' colspan='3'>Revenue</td>
                        </tr>
                        <tr>
                            <td align='left' colspan='2'>Trip End</td>
                            <td align='center'> <c:out value="${update.previoisStartCount}"/> </td>
                        <td align='right' colspan='3'>  <c:out value="${update.previoisStartRevenue}"/></td>
                        </tr>
                        <tr>
                            <td align='left' colspan='2'>Trip Start</td>
                            <td align='center'>  <c:out value="${update.previoisEndCount}"/></td>
                        <td align='right' colspan='3'> <c:out value="${update.previoisEndRevenue}"/> </td>
                        </tr>
                        <tr><td colspan='6'></td></tr>
                        <tr>
                        <th colspan='2'>Total Vehicle</th>
                        <th> <c:out value="${update.totalVehicle}"/></th>
                        <th colspan='3'>&nbsp;</th>
                        </tr>
                        <tr>
                            <td align='left'  colspan='2'>In Progress</td>
                            <td align='center'><c:out value="${update.inProgress}"/></td>
                        <td  colspan='3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left'  colspan='2'>WFL</td>
                            <td align='center'><c:out value="${update.wfl}"/></td>
                        <td colspan='3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left'  colspan='2'>WFU</td>
                            <td align='center'><c:out value="${update.wfu}"/></td>
                        <td colspan='3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left'  colspan='2'>Trip Not Started</td>
                            <td align='center'><c:out value="${update.driverIssue}"/></td>
                        <td colspan='3'>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align='left'  colspan='2'>Driver Issue, R&M</td>
                            <td align='center'><c:out value="${update.repairMaintenance}"/></td>
                        <td colspan='3'>&nbsp;</td>
                        </tr>
                </c:forEach>
            </c:if>

        </table>

    </form>

</body>
</html>

