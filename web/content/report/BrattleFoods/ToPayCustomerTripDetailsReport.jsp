<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
        
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleToPayCustomerTripDetailsExcel.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            } else {
                document.BPCLTransaction.action = '/throttle/handleToPayCustomerTripDetailsExcel.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Customer trip Report" text="Customer trip Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.Customer trip Report" text=" Customer trip Report"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="BPCLTransaction" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <!-- pointer table -->
                    <!-- message table -->
                    <%@ include file="/content/common/message.jsp"%>

                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4">To pay Customer trip Report</th></tr></thead>
                        <!--                                    <tr>
                                                                <td><font color="red">*</font>Driver Name</td>
                                                                <td height="30">
                                                                    <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                            </tr>-->
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , bform-control" style="width:250px;height:40px"" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search"></td>
                            <td><input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                            <td></td>
                        </tr>

                    </table>

                    <c:if test="${ToPayCustomerTripDetails != null}">
                        <table  id="table" class="table table-info mb30 table-hover" >

                            <thead>
                                <tr height="50">
                                    <th  align="center">S.No</th>
                                    <th  align="center">Trip code</th>
                                    <th  align="center">Customer Name</th>
                                    <th  align="center">Customer Type</th>
                                    <th  align="center">Route Info</th>
                                    <th  align="center">Start Time</th>
                                    <th  align="center">End Time</th>
                                    <th  align="center">Trip Status</th>
                                    <th  align="center">Estimated Revenue</th>
                                    <th  align="center">Estimated Expense(RCM)</th>
                                    <th  align="center">Actual Expense</th>
                                    <th  align="center">Vehicle NO</th>

                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:set var="rechargeAmount" value="${0}" />
                                <c:set var="transactionAmount" value="${0}" />
                                <c:forEach items="${ToPayCustomerTripDetails}" var="BPCLTD">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text2";
                                                } else {
                                                    classText = "text1";
                                                }
                                    %>
                                    <tr>
                                        <td><%=index++%></td>
                                        <td><c:out value="${BPCLTD.tripCode}"/></td>
                                        <td><c:out value="${BPCLTD.customerName}"/></td>
                                        <td ><c:out value="${BPCLTD.customerType}"/></td>
                                        <td  ><c:out value="${BPCLTD.routeInfo}"/></td>
                                        <td   ><c:out value="${BPCLTD.tripstarttime}"/></td>
                                        <td   ><c:out value="${BPCLTD.tripendtime}"/></td>
                                        <td   ><c:out value="${BPCLTD.tripStatus}"/></td>
                                        <td   ><c:out value="${BPCLTD.estimatedRevenue}"/>

                                        </td>
                                        <td   ><c:out value="${BPCLTD.estimatedExpenses}"/></td>
                                        <td   ><c:out value="${BPCLTD.actualExpense}"/></td>
                                        <td   ><c:out value="${BPCLTD.regNo}"/></td>

                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>        

                    </c:if>

                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

