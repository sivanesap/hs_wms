<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            } else {
                document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="VehicleDriverAdvance"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="VehicleDriverAdvance"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="">
                <form name="BPCLTransaction" method="post">

                    <table class="table table-info mb30 table-hover" style="width:90%">

                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="form-control datepicker" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="form-control datepicker" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Type</td>
                            <td height="30"><select name="expensetype" id="expensetype" class="form-control">
                                    <option value=" ">--Select---</option>
                                    <option value="Food">Food</option>
                                    <option value="Repair">R&M</option>
                                </select>
                                <script>
                                    document.getElementById("expensetype").value = '<c:out value="${expensetype}"/>';
                                </script>
                            </td>
                            <td><font color="red">*</font>Fleet </td>
                            <td height="30"><select name="fleet" id="fleet" class="form-control">
                                    <option value=" ">--Select---</option>
                                    <option value="2">Primary </option>
                                    <option value="1">Secondary</option>
                                </select>
                                <script>
                                    document.getElementById("fleet").value = '<c:out value="${fleet}"/>';
                                </script>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="4" align="center"><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search">
                                &nbsp;&nbsp;<input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    <c:if test="${vehicleDriverAdvanceDetails == null}">
                        <center><font color="red">No Records Found</font></center>
                            </c:if>
                            <c:if test="${vehicleDriverAdvanceDetails != null}">
                        <table class="table table-info mb30 table-hover" id="table" width="100%" >
                            <thead>
                                <tr height="50">
                                    <th align="center">S.No</th>
                                    <th align="center">Vehicle No</th>
                                    <th align="center">Primary Driver</th>
                                    <th align="center">Secondary Driver</th>
                                    <th align="center">Advance Paid</th>
                                    <th align="center">Advance paid Date</th>
                                    <th align="center">Expense Type</th>
                                    <th align="center">Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>
                                <c:forEach items="${vehicleDriverAdvanceDetails}" var="BPCLTD">
                                    <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                    %>
                                    <tr>
                                        <td width="2" class="<%=classText%>"><%=index++%></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.regNo}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.primaryDriver}"/></td>
                                        <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.secondaryDriver}"/></td>
                                        <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.paidAdvance}"/></td>
                                        <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.advancePaidDate}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseType}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.advanceRemarks}"/></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20" selected="selected">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
