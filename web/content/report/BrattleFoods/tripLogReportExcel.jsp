<%--
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

      <form name="tripLogReport" method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleUtilizationReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <script type="text/javascript">
      $(function () {

         //dateTime = (Object) report.getLogTime();
         //temperature = (Object) report.getCurrentTemperature();
         var timeValues = '<%= request.getAttribute("timeValues") %>';
         var tempValues = '<%= request.getAttribute("tempValues") %>';
         var timeValueArray = timeValues.split(",")
         //var tempValueArray = tempValues.split(",");
         var tempValueArray = tempValues.split(",").map(Number);
         //var input = ['9','10','11','12','15','18','21','24','25'];
         var testArray = [9,10,11,12,1,2,3,4,5];
         alert(timeValues);
         alert(tempValues);
        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'Trip Temperature Report'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                categories: timeValueArray
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                }
            },
            tooltip: {
                enabled: false,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'°C';
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'temperature',
                //data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3]
                data: tempValueArray
            }]
        });
    });


    </script>


            <div style="margin: 5px">
                <script src="http://code.highcharts.com/highcharts.js"></script>
                <script src="http://code.highcharts.com/modules/exporting.js"></script>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
<br/>
<br/> <input type="text" name="tripId" id="tripId" value="<c:out value="${tripId}"/>"/>
<c:if test="${tripLogDetailsSize != '0'}">
    <table align="center" border="1" id="table" class="sortable" width="100%" >

        <thead>
            <tr height="30">
                <th align="center" class="contentsub"><h3>S.No</h3></th>
                <th align="center" class="contentsub"><h3>Vehicle No</h3></th>
                <th align="center" class="contentsub"><h3>Trip Code</h3></th>
                <th align="center" class="contentsub"><h3>Trip Date</h3></th>
                <th align="center" class="contentsub"><h3>Current Location</h3></th>
                <th align="center" class="contentsub"><h3>Distance Travelled</h3></th>
                <th align="center" class="contentsub"><h3>Current Temperature</h3></th>
                <th align="center" class="contentsub"><h3>Log Date</h3></th>
            </tr>
        </thead>
        <tbody>
            <% int index = 1;%>
            <c:forEach items="${tripLogDetails}" var="tripLogDetails">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr>
                    <td class="<%=classText%>"><%=index++%></td>
                    <td  width="90" class="<%=classText%>">
                        <c:out value="${tripLogDetails.vehicleNo}"/>
                        <input type="hidden" value="<c:out value="${tripLogDetails.vehicleNo}"/>" name="vehicleNo" id="vehicleNo"/></td>
                    <td width="90" class="<%=classText%>">
                        <c:out value="${tripLogDetails.tripCode}"/>
                        <input type="hidden" value="<c:out value="${tripLogDetails.tripCode}"/>" name="tripCode" id="tripCode"/>
                    </td>
                    <td class="<%=classText%>" ><c:out value="${tripLogDetails.tripDate}"/></td>
                    <td class="<%=classText%>"  ><c:out value="${tripLogDetails.location}"/></td>
                    <td width="140" class="<%=classText%>"  ><c:out value="${tripLogDetails.distance}"/></td>
                    <td width="140" class="<%=classText%>"  ><c:out value="${tripLogDetails.currentTemperature}"/></td>
                    <td class="<%=classText%>"  ><c:out value="${tripLogDetails.logDate}"/></td>
                </tr>

            </c:forEach>
        </tbody>
    </table>
</c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </form>
    </body>
</html>
