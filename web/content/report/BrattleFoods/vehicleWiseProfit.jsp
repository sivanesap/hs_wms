<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<style type="text/css">
    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }
</style>
<script>
    $(document).ready(function () {
        $('.ball, .ball1').removeClass('stop');
        $('.trigger').click(function () {
            $('.ball, .ball1').toggleClass('stop');
        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    //auto com

    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getRegistrationNo.do",
                    dataType: "json",
                    data: {
                        regno: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                $('#vehicleNo').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>



<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/handleVehicleWiseProfit.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/handleVehicleWiseProfit.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewCustomerProfitDetails(tripIds) {
//            alert(tripIds);
        window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>
</head>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Vehicle Wise Profitability"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
            <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValue();">
                <form name="accountReceivable" action=""  method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb10 table-hover" id="bg" >
                        <thead>
                            <tr>
                                <th colspan="2" height="30" >Vehicle Wise Profitability </th>
                            </tr>
                        </thead>
                    </table>

                    <table class="table table-info mb30 table-hover" id="report" >
                        <tr>
                            <td><font color="red"></font>Vehicle No</td>
                            <td height="30">
                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                <input type="text" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>"  class="form-control" style="width:250px;height:40px"/>
                            </td>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px"  value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>
                            <td><font color="red"></font>Operation Type</td>
                            <td height="30">
                                <select name="operationTypeId" id="operationTypeId"  class="form-control" style="width:250px;height:40px">
                                    <option value="0">--Select--</option>
                                    <option value="Primary">Primary</option>
                                    <option value="Secondary">Secondary</option>
                                    <option value="All">All</option>
                                </select>
                                <script>
                                    document.getElementById("operationTypeId").value = '<c:out value="${operationTypeId}"/>'
                                </script>
                            </td>
                            <td><font color="red"></font>Fleet Center</td>
                            <td height="30">
                                <select name="fleetCenterId" id="fleetCenterId"  class="form-control" style="width:250px;height:40px">
                                    <option value="0">--Select--</option>
                                    <c:if test="${companyList != null}">
                                        <c:forEach items="${companyList}" var="cmpList">
                                            <option value="<c:out value="${cmpList.cmpId}"/>"><c:out value="${cmpList.name}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <script>
                                    document.getElementById("fleetCenterId").value = '<c:out value="${fleetCenterId}"/>'
                                </script>
                            </td>
                            <td><font color="red"></font>Profit/Non-Profit</td>
                            <td height="30">
                                <select name="profitType" id="profitType"  class="form-control" style="width:250px;height:40px">
                                    <option value="All">All</option>
                                    <option value="Profit">Profitable</option>
                                    <option value="nonProfit">Non-Profitable</option>
                                </select>
                                <script>
                                    document.getElementById("profitType").value = '<c:out value="${profitType}"/>'
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right"><input type="button"class="btn btn-success"  name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                            <td colspan="3"><input type="button"class="btn btn-success"  name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                        </tr>
                    </table>
        </div></div>
</td>
</tr>
</table>



<c:if test="${vehicleDetailsList == null && vehicleWiseProfitListSize == null}">
    <center>
        <font color="blue">Please Wait Your Request is Processing</font>
        <br>
        <div class="container">
            <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
            </div>
        </div>
    </center>
</c:if>

<c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
    <table class="table table-info mb30 table-hover">
        <thead>
            <tr>
                <th rowspan="2" >S.No</th>
                <th rowspan="2" >Vehicle No</th>
                <th rowspan="2" >Vehicle Model</th>
                <!--                            <td rowspan="2" >MFR Name</td>-->
                <th rowspan="2" >Trips</th>
                <th rowspan="2" >Earnings</td>
                <th colspan="5"  style="text-align:center">Fixed Expenses </th>
                <th colspan="5"  style="text-align:center">Operation Expenses </th>
                <th rowspan="2"  style="text-align:center">Per Day<br> Fixed Expenses </th>
                <th rowspan="2"  style="text-align:center">Fixed Expenses <br>for the Report Period</th>
                <th rowspan="2"  style="text-align:center">Operation Expenses</th>
                <th rowspan="2" >Maint <br>Expenses</th>
                <th rowspan="2" >Nett <br>Expenses</th>
                <th rowspan="2" >Profit</th>
                <th rowspan="2" >Profit %</th>
            </tr> 
            <tr>
                <th  >Insurance</th>
                <th >Road Tax </th>
                <th >FC Amount </th>
                <th >Permit </th>
                <th >EMI </th>
                <th >Toll Amount</th>
                <th >Fuel Amount </th>
                <th >Other Exp Amount </th>
                <th >Driver / Cleaner Salary </th>
                <th >Driver(Incentive,<br>Bata,Misc)</th>
            </tr>
        </thead>
        <% int index = 0,sno = 1;%>
        <c:set var="totalTrip" value="${0}"/>    
        <c:set var="totalIncome" value="${0}"/>    
        <c:set var="totalFixedExpense" value="${0}"/>    
        <c:set var="totalOperationExpense" value="${0}"/>    
        <c:set var="totalNetExpense" value="${0}"/>    
        <c:set var="totalProfitGained" value="${0}"/>    
        <c:forEach items="${vehicleDetailsList}" var="veh">
            <c:if test="${vehicleWiseProfitList != null}">
                <c:forEach items="${vehicleWiseProfitList}" var="profitList">
                    <c:if test="${veh.vehicleId == profitList.vehicleId}">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tbody>
                            <c:set var="profit" value="${0}"/>    
                            <c:set var="profitPercent" value="${0}"/>
                            <c:set var="countPercent" value="${0}"/>
                            <c:if test="${profitType == 'All'}">
                                <tr>
                                    <td  rowspan="2" align="center"><%=sno++%></td>
                                    <td  rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><c:out value="${veh.regNo}"/></a></td>
                                    <td   rowspan="2" ><c:out value="${veh.modelName}"/></td>
<!--                                            <td  rowspan="2" ><c:out value="${veh.mfrName}"/></td>-->
                                    <c:if test="${profitList.tripNos > 0}">
                                        <td  rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                        </c:if>
                                        <c:if test="${profitList.tripNos == 0}">
                                        <td  rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                    </c:if>
                                    <td  rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                    <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.netProfit gt 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                                <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>    
                                            <c:if test="${profitPercent <= 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:if test="${profitPercent > 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>    
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                            <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>  
                                </tr>
                            </c:if>    
                            <c:if test="${profitType == 'Profit' && profitList.netProfit > 0}">
                                <tr>
                                    <td  rowspan="2" align="center"><%=sno++%></td>
                                    <td  rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><c:out value="${veh.regNo}"/></a></td>
                                    <td   rowspan="2" align="center"><c:out value="${veh.modelName}"/></td>
<!--                                            <td  rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                    <c:if test="${profitList.tripNos > 0}">
                                        <td  rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                        </c:if>
                                        <c:if test="${profitList.tripNos == 0}">
                                        <td  rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                    </c:if>
                                    <td  rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                    <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.netProfit gt 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                                <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>  
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                            <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>
                                </tr>
                            </c:if>
                            <c:if test="${profitType == 'nonProfit' && profitList.netProfit <= 0}">
                                <tr>
                                    <td  rowspan="2" align="center"><%=sno++%></td>
                                    <td  rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><c:out value="${veh.regNo}"/></a></td>
                                    <td   rowspan="2" align="center"><c:out value="${veh.modelName}"/></td>
<!--                                            <td  rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                    <c:if test="${profitList.tripNos > 0}">
                                        <td  rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><c:out value="${profitList.tripNos}"/></a></td>
                                        </c:if>
                                        <c:if test="${profitList.tripNos == 0}">
                                        <td  rowspan="2" align="center"><c:out value="${profitList.tripNos}"/></td>
                                    </c:if>
                                    <td  rowspan="2" align="right"><c:out value="${profitList.freightAmount}"/></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></td>
                                    <td  rowspan="2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></td>
                                    <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.netProfit gt 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                            </c:if>
                                            <c:if test="${profitList.freightAmount != '0.00'}">
                                                <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                <c:set var="profitPercent" value="${profit*100}"/>
                                            </c:if>
                                            <c:if test="${profitPercent <= 0}">
                                        <td  rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if>       
                                            <c:if test="${profitPercent > 0}">
                                        <td  rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                            </c:if> 
                                            <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                            <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                            <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                            <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                            <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                            <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                            <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                            <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                            <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                            <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                            <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                            <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                            <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                            <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                            <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                            <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                </tr>
                            </c:if>
                        </tbody>
                    </c:if>

                </c:forEach>
            </c:if>
            <%
       index++;
            %>
        </c:forEach>
        <tr>
            <td colspan="3"  style="text-align:center">Total</td>
            <td  style="text-align:center"><c:out value="${totalTrip}"/></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalInsurance}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRoadTax}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFcAmount}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPermitAmount}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEmiAmount}" /></td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTollAmount}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFuelAmount}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOtherExpenseAmount}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverSalaryAmount}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverExpense}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpensePerDay}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalMaintainExpense}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </td>
            <c:set var="totalProfitPercentValue1" value="${0}"/>
            <c:set var="totalProfitPercentValue" value="${0}"/>
            <c:if test="${totalIncome != 0}">
                <c:set var="totalProfitPercentValue1" value="${totalProfitGained / totalIncome}"/>
                <c:set var="totalProfitPercentValue" value="${totalProfitPercentValue1 * 100}"/>
            </c:if>
            <td  style="text-align:right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercentValue}" /> %</td>
        </tr>                   
    </table>
</c:if>
<br/>
<br/>
<br/>
<br/>
<br/>
<c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">

    <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
    <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 

    <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
            <td width="150" align="right"><c:out value="${totalTrip}"/></td>
        </tr>
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
        </tr>
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
        </tr>
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Operation Expenses</td>
            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
        </tr>
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Nett Expenses</td>
            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
        </tr>
        <tr height="25">
            <td style="background-color: #6374AB; color: #ffffff">Total Profit gained</td>
            <c:if test="${totalProfitGained lt 0 || totalProfitGained eq 0}">
                <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                    </c:if>
                    <c:if test="${totalProfitGained gt 0 }">
                <td width="150" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                    </c:if>
        </tr>
        <tr height="25">
            <c:set var="totalProfit" value="${0}"/>    
            <c:set var="totalProfitPercent" value="${0}"/>
            <c:if test="${totalIncome > 0}">
                <c:set var="totalProfit" value="${totalProfitGained/totalIncome}"/>    
                <c:set var="totalProfitPercent" value="${totalProfit*100}"/>
            </c:if>
            <td style="background-color: #6374AB; color: #ffffff">Total Profit gained &nbsp;%</td>
            <c:if test="${totalProfitPercent lt 0 || totalProfitPercent eq 0}">
                <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                    </c:if>
                    <c:if test="${totalProfitPercent gt 0 }">
                <td width="150" align="right"><font color="green" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                    </c:if>
        </tr>
    </table>
</c:if>

</form>
</body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
