<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceReportExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceReportExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <body>
        <form name="BPCLTransaction" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Vehicle Driver Advance Report</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="button" class="button" name="search" onclick="submitPage(this.name);" value="Search"></td>
                                        <td><input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <c:if test="${VehicleDriverAdvanceDetails != null}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>Primary Driver</h3></th>
                            <th align="center"><h3>Advance Paid Date</h3></th>
                            <th align="center"><h3>Expense Type</h3></th>
                            <th align="center"><h3>Paid Amount</h3></th>
                            <th align="center"><h3>Approved By</h3></th>
                            <th align="center"><h3>Remarks</h3></th>
                            
                                                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${VehicleDriverAdvanceDetails}" var="BPCLTD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.vehicleNo}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.driver}"/>
                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.advancePaidDate}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.expenseType}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.paidAmount}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.approvedBy}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.remarks}"/></td>
                                                                                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

               
            </c:if>

        </form>
    </body>
</html>

