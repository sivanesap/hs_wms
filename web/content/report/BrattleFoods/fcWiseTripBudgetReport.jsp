<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
         <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {
                if (value == 'ExportExcel') {
                    document.fcWiseTripSummary.action = '/throttle/handleTripBudgetReportExcel.do?param=ExportExcel';
                    document.fcWiseTripSummary.submit();
                } else {
                    document.fcWiseTripSummary.action = '/throttle/handleTripBudgetReportExcel.do?param=Search';
                    document.fcWiseTripSummary.submit();
                }
            }
        </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FC Wise Trip Performance Summary" text="FC Wise Trip Performance Summary"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.FC Wise Trip Performance Summary" text=" FC Wise Trip Performance Summary"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

    <body>
        <form name="fcWiseTripSummary" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp"%>
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="4">FC Wise Trip Performance Summary</th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px"" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="btn btn-success"  name="search" onclick="submitPage(this.name);" value="Search">
                                        <input type="button" class="btn btn-success"  name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                    </tr>

                                </table>
                         

            <c:if test="${FcWiseTripBudgetDetails != null}">
                  <table  id="table" class="table table-info mb30 table-hover" >

                    <thead>
                    <tr>
                    <th><h3> S.No</h3></th>
                    <th width="120"><h3>FleetCenterName</h3></th>
                    <th><h3>Cost</h3></th>
                    <th><h3>TotalTrucks</h3></th>
                    <th><h3>TotalTrips</h3></th>
                    <th><h3>TotalKms</h3></th>
                    <th><h3>TotalReferHrs</h3></th>
                    <th><h3>ReferCost</h3></th>
                    <th><h3>TotalCost</h3></th>
                    <th><h3>WFURefer(Approx)</h3></th>
                    <th><h3>Cost/KM</h3></th>
                    <th><h3>ReferCost/KM</h3></th>
                    <th><h3>RCM</h3></th>
                    <th><h3>DeviationperTruck/KM</h3></th>
                    <th><h3>%Empty/Loaded(KM)</h3></th>
                    <th><h3>AvgKm</h3></th>
                    <th><h3>Fooding Advance</h3></th>
                    <th><h3>R&M Advance</h3></th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:set var="costPerKM" value="${0}"/>
                        <c:set var="averageKmEmpty" value="${0}"/>
                        <% int index = 1;%>
                        <c:set var="loadedKm" value="${0}"/>
                        <c:forEach items="${FcWiseTripBudgetDetails}" var="BPCLTD">
                            <c:set var="referCost" value="${BPCLTD.totalHms * 160}"/>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="costPerKM" value="${BPCLTD.totalCost / BPCLTD.totalkms}"/>
                            </c:if>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="referCostPerKM" value="${referCost/(BPCLTD.totalkms - BPCLTD.emptyTripKM)}"/>
                            </c:if>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="averageKmEmpty" value="${(BPCLTD.emptyTripKM/BPCLTD.totalkms)*100}"/>
                            </c:if>
                            <c:set var="averageKm" value="${BPCLTD.totalkms/BPCLTD.noOfVehicles}"/>
                            <c:set var="totalKmEmpty" value="${TotalKmEmpty+BPCLTD.emptyTripKM}"/>


                            <c:set var="totalReferCost" value="${totalReferCost + referCost }"/>
                            <c:set var="totalReferCostPerKm" value="${totalReferCostPerKm + referCostPerKM }"/>
                            <c:set var="totaltrips" value="${totaltrips +  BPCLTD.totalTrip }"/>
                            <c:set var="totalNoofVehicles" value="${totalNoofVehicles + BPCLTD.noOfVehicles }"/>
                            <c:set var="totalTotalKMs" value="${totalTotalKMs + BPCLTD.totalkms }"/>
                            <c:set var="totalTotalHMs" value="${totalTotalHMs + BPCLTD.totalHms }"/>
                            <c:set var="totalTotalCost" value="${totalTotalCost + BPCLTD.totalCost }"/>
                            <c:set var="totalWfuAmount" value="${totalWfuAmount + BPCLTD.wfuAmount }"/>
                            <c:set var="totalrcm" value="${totalrcm + BPCLTD.rcm }"/>
                            <c:set var="totalFoodingAdvance" value="${totalFoodingAdvance + BPCLTD.foodingAdvance }"/>
                            <c:set var="totalRnmAdvance" value="${totalRnmAdvance + BPCLTD.repairAdvance }"/>
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="text1"><%=index++%></td>
                                <td  width="30" class="text1"><c:out value="${BPCLTD.companyName}"/></td>
                                <td  width="30" class="text1">&nbsp;</td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.noOfVehicles}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.totalTrip}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.totalkms}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.totalHms}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${referCost}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.totalCost}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.wfuAmount}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${costPerKM}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${referCostPerKM}"/></td>
                                <td  width="30" class="text1" align="right"><c:out value="${BPCLTD.rcm}"/></td>
                                <td  width="30" class="text1">&nbsp;</td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${averageKmEmpty}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${averageKm}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.foodingAdvance}"/></td>
                                <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.repairAdvance}"/></td>

                            </tr>
                        </c:forEach>
                            <c:set var="totalCostPerKM" value="${0}"/>
                            <c:set var="totalAverageKmEmpty" value="${0}"/>
                            <c:set var="totalAverageKm" value="${totalTotalKMs /totalNoofVehicles}"/>
                            <c:if test="${totalTotalKMs > 0.0}">
                            <c:set var="totalCostPerKM" value="${totalTotalCost /totalTotalKMs}"/>
                            </c:if>
                            <c:if test="${totalTotalKMs > 0.0}">
                            <c:set var="totalAverageKmEmpty" value="${(TotalKmEmpty / totalTotalKMs)*100}"/>
                            </c:if>
                                <tr>
                                     <td  width="30" class="text1" colspan="3" align="right">&nbsp;Total</td>
                                     <td  width="30" class="text1" align="right"><c:out value="${totalNoofVehicles}"/></td>
                                     <td  width="30" class="text1" align="right"><c:out value="${totaltrips}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalTotalKMs}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalTotalHMs}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalReferCost}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalTotalCost}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalWfuAmount}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalCostPerKM}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalReferCostPerKm}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalrcm}"/></td>
                                     <td  width="30" class="text1">&nbsp;</td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalAverageKmEmpty}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalAverageKm}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalFoodingAdvance}"/></td>
                                     <td  width="30" class="text1" align="right"><fmt:formatNumber pattern="##0.00" value="${totalRnmAdvance}"/></td>
                                </tr>
                    </tbody>
                </table>
                            

            </c:if>
                     
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
