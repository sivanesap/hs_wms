<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>


        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

  <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.tripSheet.action = '/throttle/handleOrderReport.do?param=ExportExcel';
                        document.tripSheet.submit();
                    } else {
                        document.tripSheet.action = '/throttle/handleOrderReport.do?param=Search';
                        document.tripSheet.submit();
                    }
                }
            }
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
                if('<%=request.getAttribute("tripId")%>' != 'null'){
                    document.getElementById('tripId').value='<%=request.getAttribute("tripId")%>';
                }
                if('<%=request.getAttribute("fleetCenterId")%>' != 'null'){
                    document.getElementById('fleetCenterId').value='<%=request.getAttribute("fleetCenterId")%>';
                }
            }
            function viewVehicleDetails(vehicleId) {
            //alert(vehicleId);
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
               function viewTripDetails(tripId) {
               //alert(tripId);
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        </script>
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="ORDER REPORT"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body onload="setValue();sorter.size(20);">
    <form name="tripSheet" method="post">

        <%@include file="/content/common/message.jsp" %>
         <%@ include file="/content/common/message.jsp" %>
               <table class="table table-info mb10 table-hover" id="bg" style="width:850px;">
		    <thead>
		<tr>
		    <th colspan="2" height="30" >ORDER REPORT </th>
		</tr>
		    </thead>
    </table>
        
          <table class="table table-info mb30 table-hover" id="report" style="width:100%;">
                                    <tr>
                                        <td align="center"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><input type="button"class="btn btn-success"  name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button"class="btn btn-success"  name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                    </td>
                </tr>
                <style>
                    div.wrap {
                      word-wrap: break-word;
                    }
                </style>
        <c:if test="${tripDetails != null}">
                   <table class="table table-info mb30 table-hover"  style="width:550px;" id="table">
                <thead>
                    <tr height="40" >
                        <th rowspan="2">S.No</th>
                         <th rowspan="2" >Request</th>
                         <th rowspan="2">Response</th>
                         <th rowspan="2">Parameter Value</th>
                         <th rowspan="2">Source</th>
                    </tr>
                </thead>
                <tbody>
                <% int index = 1;%>

                    <c:forEach items="${tripDetails}" var="tripDetails">
                         <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                    
                            <td class="<%=classText%>" align="center"><%=index++%></td>   
                            <td class="<%=classText%>" ><div class="wrap"><font size="2"><c:out value="${tripDetails.jsonValue}"/></font></div></td>
                            <td class="<%=classText%>" ><c:out value="${tripDetails.responseValue}"/></td>
                            <td class="<%=classText%>" ><c:out value="${tripDetails.parameterValue}"/></td>
                            <td class="<%=classText%>"><c:out value="${tripDetails.fromSource}"/></td>

                            
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

        
 <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>

    </form>
</body>

</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>