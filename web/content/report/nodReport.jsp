<!DOCTYPE html>
<!--<html>-->
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script>
    function redirectMethod(statusName, statusId, day) {
        document.nodReport.action = '/throttle/nodReportLevel2.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
        document.nodReport.submit();
    }

    function openLevel3Report(statusName, statusId, day) {
        document.nodReport.action = '/throttle/nodReportLevel3.do?statusName=' + statusName + '&statusId=' + statusId + '&day=' + day;
        document.nodReport.submit();
    }

</script>

<body>
<center>
    <%--<%@include file="/content/common/message.jsp"%>--%>
</center>
<form name="nodReport"  method="post">

    <c:set var="zeroDayTotal" value="0"/>
    <c:set var="oneDayTotal" value="0"/>
    <c:set var="twoDayTotal" value="0"/>
    <c:set var="threeDayTotal" value="0"/>
    <c:set var="fourDayTotal" value="0"/>
    <c:set var="fiveDayTotal" value="0"/>
    <c:set var="sixDayTotal" value="0"/>
    <c:set var="sevenDayTotal" value="0"/>
    <c:set var="moreDayTotal" value="0"/>

    <c:set var="zeroDayTotals" value="0"/>
    <c:set var="oneDayTotals" value="0"/>
    <c:set var="twoDayTotals" value="0"/>
    <c:set var="threeDayTotals" value="0"/>
    <c:set var="fourDayTotals" value="0"/>
    <c:set var="fiveDayTotals" value="0"/>
    <c:set var="sixDayTotals" value="0"/>
    <c:set var="sevenDayTotals" value="0"/>
    <c:set var="moreDayTotals" value="0"/>

    <c:set var="orderCreatedTotal" value="0"/>
    <c:set var="tripCreatedTotal" value="0"/>
    <c:set var="tripFreezedTotal" value="0"/>
    <c:set var="tripStartTotal" value="0"/>
    <c:set var="tripEndTotal" value="0"/>
    <c:set var="tripClosureTotal" value="0"/>
    <c:set var="tripSettledTotal" value="0"/>
    <c:set var="billCreatedTotal" value="0"/>
    <c:set var="billSubmittedTotal" value="0"/>
    <c:set var="summaryTotal" value="0"/>


        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Aging Report" text="Aging Report"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Report"/></a></li>
                    <li class=""><spring:message code="hrms.label.Aging Report" text="Aging Report"/></li>
                </ol>
            </div>
        </div>
        <br/>
        
    <c:if test="${nodList != null}">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <center> <font color="blue" size='4'><b>Monthly Ageing Report</b></font></center>
                    <br>
                    <table class="table table-info mb30 table-bordered"  style="width:85%" align="center">
                        <thead >
                        <th >&emsp; &emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;Status </th>
                        <th>1 to 7</th>
                        <th>8 to 14</th>
                        <th>15 to 21</th>
                        <th>21 to 28</th>
                        <th>>28</th>
                        <th>Last Month</th>
                        <th>Total</th>
                        </thead>
                        <c:if test = "${monthNodList != null}" >
                            <c:forEach items="${monthNodList}" var="mnl">
                                
                                <tr>
                                    <td><b>
                                        <c:out value="${mnl.statusName}"/>
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.week1==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <c:if test="${mnl.week1!=0}">
                                            <font size='3' color="green">
                                        <c:out value="${mnl.week1}"/>
                                        <c:set var="week1total" value="${week1total+mnl.week1}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.week2==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <c:if test="${mnl.week2!=0}">
                                            <font size='3' color="green">
                                        <c:out value="${mnl.week2}"/>
                                        <c:set var="week2total" value="${week2total+mnl.week2}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.week3==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <c:if test="${mnl.week3!=0}">
                                            <font size='3' color="green">
                                        <c:out value="${mnl.week3}"/>
                                        <c:set var="week3total" value="${week3total+mnl.week3}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.week4==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <c:if test="${mnl.week4!=0}">
                                            <font size='3' color="green">
                                        <c:out value="${mnl.week4}"/>
                                        <c:set var="week4total" value="${week4total+mnl.week4}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.week5==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <c:if test="${mnl.week5!=0}">
                                            <font size='3' color="green">
                                        <c:out value="${mnl.week5}"/>
                                        <c:set var="week5total" value="${week5total+mnl.week5}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    <td align="center"><b><c:if test="${mnl.lastMonth==0}">
                                        <font size='1'>-</font> 
                                        </c:if>
                                        <font color="#FF3D03" size='3'>
                                        <c:if test="${mnl.lastMonth!=0}">
                                        <c:out value="${mnl.lastMonth}"/>
                                        <c:set var="lastMonthTotal" value="${lastMonthTotal+mnl.lastMonth}"/> 
                                        </font>
                                        </c:if>
                                        
                                        </b>
                                    </td>
                                    
                                    <td align="center"><b>
                                            <font color="#FF3D03" size='3'>
                                        <c:out value="${mnl.week1+mnl.week2+mnl.week3+mnl.week4+mnl.week5+mnl.lastMonth}"/>
                                        <c:set var="totalTotal" value="${totalTotal+mnl.week1+mnl.week2+mnl.week3+mnl.week4+mnl.week5+mnl.lastMonth}"/>
                                        </font></b>
                                    </td>
                                    </tr>
                            </c:forEach>
                           </c:if>
                         <tr>
                            <td style="text-align: center"><font size='3'><b>Total</b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week1total}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week2total}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week3total}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week4total}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${week5total}"/></b></font></td>
                            <td style="text-align: center"><font color="#FF3D03" size='3'><b><c:out value="${lastMonthTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="#FF3D03" size='3'><b><c:out value="${totalTotal+enconeDayTotal+enctwoDayTotal+encthreeDayTotal+encfourDayTotal+encfiveDayTotal+encsixDayTotal+encsevenDayTotal+encmoreThanSevenDay}"/></b></font></td>
                        </tr>
                      </table>
                    
                    
<!--                      <center> <font color="blue" size='4'><b>Order Ageing Report</b></font></center>
                    <br>
                    <table class="table table-info mb30 table-bordered"  style="width:85%" align="center">
                        <thead >
                        <th >&emsp; &emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Status </th>
                        <th>0 Day</th>
                        <th>1 Day</th>
                        <th>2 Days</th>
                        <th>3 Days</th>
                        <th>4 Days</th>
                        <th>5 Days</th>
                        <th>6 Days</th>
                        <th>7 Days</th>
                        <th> >7 Days</th>
                        <th> Total</th>
                        </thead>
                        <c:if test = "${enqStatusList != null}" >
                            <c:forEach items="${enqStatusList}" var="enq">
                                <tr>
                                    <td><b>
                                        <c:out value="${enq.statusName}"/>
                                        </b>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.zeroDay== 0}" >
                                            <font size='1'>-</font>  
                                        </c:if>
                                        <c:if test = "${enq.zeroDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 0);">
                                                <font size='3' color="green"><b>
                                                    <c:out value="${enq.zeroDay}"/>
                                                    <c:set var="enczeroDayTotal" value="${enczeroDayTotal+enq.zeroDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.oneDay== 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.oneDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 1);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.oneDay}"/>
                                                    <c:set var="enconeDayTotal" value="${enconeDayTotal+enq.oneDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.twoDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.twoDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 2);">
                                                <font color="green" size='3'>
                                                <b> <c:out value="${enq.twoDay}"/>
                                                    <c:set var="enctwoDayTotal" value="${enctwoDayTotal+enq.twoDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.threeDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.threeDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 3);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.threeDay}"/>
                                                    <c:set var="encthreeDayTotal" value="${encthreeDayTotal+enq.threeDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.fourDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.fourDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 4);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.fourDay}"/>
                                                    <c:set var="encfourDayTotal" value="${encfourDayTotal+enq.fourDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.fiveDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.fiveDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 5);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.fiveDay}"/>
                                                    <c:set var="encfiveDayTotal" value="${encfiveDayTotal+enq.fiveDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.sixDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.sixDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 6);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.sixDay}"/>
                                                    <c:set var="encsixDayTotal" value="${encsixDayTotal+enq.sixDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.sevenDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.sevenDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 7);">
                                                <font color="green" size='3'>
                                                <b>
                                                    <c:out value="${enq.sevenDay}"/>
                                                    <c:set var="encsevenDayTotal" value="${encsevenDayTotal+enq.sevenDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test = "${enq.moreThanSevenDay== 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.moreThanSevenDay!= 0}" >
                                            <a href="#" onclick="redirectMethod('<c:out value="${enq.statusName}"/>','<c:out value="${enq.statusId}"/>', 8);">
                                                <font color="#FF3D03" size='3'>
                                                <b>
                                                    <c:out value="${enq.moreThanSevenDay}"/>
                                                    <c:set var="encmoreThanSevenDay" value="${encmoreThanSevenDay+enq.moreThanSevenDay}"/>
                                                </b>
                                                </font>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td style="text-align: center">
                                        <c:if test = "${enq.zeroDay+enq.oneDay+enq.twoDay+enq.threeDay+enq.fourDay+enq.fiveDay+enq.sixDay+enq.sevenDay+enq.moreThanSevenDay == 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${enq.zeroDay+enq.oneDay+enq.twoDay+enq.threeDay+enq.fourDay+enq.fiveDay+enq.sixDay+enq.sevenDay+enq.moreThanSevenDay!= 0}" >
                                            <font color="#FF3D03"><b>
                                                <c:out value="${enq.zeroDay+enq.oneDay+enq.twoDay+enq.threeDay+enq.fourDay+enq.fiveDay+enq.sixDay+enq.sevenDay+enq.moreThanSevenDay}"/>
                                            </b></font>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>  
                        </c:if>
                        <tr>
                            <td style="text-align: center"><font size='3'><b>Total</b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${enczeroDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${enconeDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${enctwoDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${encthreeDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${encfourDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${encfiveDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${encsixDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="green" size='3'><b><c:out value="${encsevenDayTotal}"/></b></font></td>
                            <td style="text-align: center"><font color="#FF3D03" size='3'><b><c:out value="${encmoreThanSevenDay}"/></b></font></td>
                            <td style="text-align: center"><font color="#FF3D03" size='3'><b><c:out value="${enczeroDayTotal+enconeDayTotal+enctwoDayTotal+encthreeDayTotal+encfourDayTotal+encfiveDayTotal+encsixDayTotal+encsevenDayTotal+encmoreThanSevenDay}"/></b></font></td>
                        </tr>
                    </table>-->


<!--
                    <center> <font color="blue" size='4'><b>Operation Ageing Report</b></font></center>
                    <br>
                    <table class="table table-info mb30 table-bordered" style="width:85%" align="center">
                        <thead >
                        <th >&emsp; &emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Status </th>
                        <th>0 Day</th>
                        <th>1 Day</th>
                        <th>2 Days</th>
                        <th>3 Days</th>
                        <th >4 Days</th>
                        <th>5 Days</th>
                        <th>6 Days</th>
                        <th>7 Days</th>
                        <th> >7 Days</th>                          
                        <th>Total</th>                          
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <%
                            sno++;
                            String className = "text1";
                            if ((sno % 1) == 0) {
                            className = "text1";
                            } else {
                            className = "text2";
                            }
                            int checkBillType=0;
                            %>
                            <c:if test="${nodLists != null}">
                                <c:forEach items="${nodLists}" var="lists">
                                    <tr>
                                        <td ><b><c:out value="${lists.statusName}"/></b></td>
                                        <td  align="center" > 

                                            <c:if test = "${lists.zeroDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.zeroDay!= 0}" >

                                                <a  href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 0);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.zeroDay}"/></b></font>
                                                    <c:set var="zeroDayTotals" value="${zeroDayTotal+lists.zeroDay}"/>
                                                </a>

                                            </c:if>
                                        </td>
                                        <td  align="center" > 
                                            <c:if test = "${lists.oneDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.oneDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 1);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.oneDay}"/></b></font>
                                                        <c:set var="oneDayTotals" value="${oneDayTotal+lists.oneDay}"/>
                                                </a> 
                                            </c:if>    
                                        </td>
                                        <td align="center" > 
                                            <c:if test = "${lists.twoDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.twoDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 2);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.twoDay}"/></b></font>
                                                        <c:set var="twoDayTotals" value="${twoDayTotal+lists.twoDay}"/>
                                                </a> 
                                            </c:if>    
                                        </td>
                                        <td align="center"> 
                                            <c:if test = "${lists.threeDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.threeDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 3);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.threeDay}"/> </b></font><c:set var="threeDayTotals" value="${threeDayTotal+list.threeDay}"/></a> 
                                                </c:if>    
                                        </td>
                                        <td align="center">

                                            <c:if test = "${lists.fourDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.fourDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 4);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.fourDay}"/> </b></font> <c:set var="fourDayTotals" value="${fourDayTotal+lists.fourDay}"/> </a>
                                                </c:if>
                                        </td>
                                        <td align="center"> 
                                            <c:if test = "${lists.fiveDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.fiveDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 5);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.fiveDay}"/></b></font>  <c:set var="fiveDayTotals" value="${fiveDayTotal+lists.fiveDay}"/> </a>
                                                    </c:if>
                                        </td>
                                        <td align="center"> 
                                            <c:if test = "${lists.sixDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.sixDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 6);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.sixDay}"/> </b></font> <c:set var="sixDayTotals" value="${sixDayTotal+lists.sixDay}"/></a>
                                                </c:if>
                                        </td>
                                        <td align="center"> 
                                            <c:if test = "${lists.sevenDay == 0}" >
                                                <font size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.sevenDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 7);" >
                                                    <font color="green" size='3'><b><c:out value="${lists.sevenDay}"/> </b></font> <c:set var="sevenDayTotals" value="${sevenDayTotal+lists.sevenDay}"/></a>
                                                </c:if>
                                        </td>
                                        <td  align="center"> 
                                            <c:if test = "${lists.moreThanSevenDay == 0}" >
                                                <font  size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.moreThanSevenDay!= 0}" >
                                                <a href="#" onclick="openLevel2Report('<c:out value="${lists.statusName}"/>',<c:out value="${lists.statusId}"/>, 8);" >
                                                    <font color="#FF3D03" size='3'><b><c:out value="${lists.moreThanSevenDay}"/></b></font> <c:set var="moreDayTotals" value="${moreDayTotal+lists.moreThanSevenDay}"/> </a>
                                                    </c:if>
                                        </td>
                                        <td  style="text-align: center">
                                            <c:if test = "${lists.zeroDay+oneDayTotal+lists.twoDay+lists.threeDay+lists.fourDay+lists.fiveDay+lists.sixDay+lists.sevenDay+lists.moreThanSevenDay == 0}" >
                                                <font  size='1'>-</font>
                                            </c:if>
                                            <c:if test = "${lists.zeroDay+oneDayTotal+lists.twoDay+lists.threeDay+lists.fourDay+lists.fiveDay+lists.sixDay+lists.sevenDay+lists.moreThanSevenDay != 0}" >
                                                <font color="#FF3D03"><b>
                                                    <c:out value="${lists.zeroDay+oneDayTotal+lists.twoDay+lists.threeDay+lists.fourDay+lists.fiveDay+lists.sixDay+lists.sevenDay+lists.moreThanSevenDay}"/>
                                                </b></font>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                            <c:forEach items="${nodList}" var="list">
                                <tr>
                                    <td ><b> <c:out value="${list.statusName}"/></b></td>

                                    <td align="center" > 
                                        <c:if test = "${list.zeroDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.zeroDay!= 0}" >
                                            <a  href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 0);" >
                                                <font color="green" size='3'><b><c:out value="${list.zeroDay}"/></b></font>
                                                <c:set var="zeroDayTotal" value="${zeroDayTotal+list.zeroDay}"/></a>
                                            </c:if>
                                    </td>
                                    <td align="center" > 
                                        <c:if test = "${list.oneDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.oneDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 1);" >
                                                <font color="green" size='3'><b><c:out value="${list.oneDay}"/></b></font>
                                                    <c:set var="oneDayTotal" value="${oneDayTotal+list.oneDay}"/>
                                            </a>
                                        </c:if>
                                    </td>
                                    <td align="center" >
                                        <c:if test = "${list.twoDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.twoDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 2);" >
                                                <font color="green" size='3'><b><c:out value="${list.twoDay}"/></b></font>
                                                    <c:set var="twoDayTotal" value="${twoDayTotal+list.twoDay}"/>
                                            </a> 
                                        </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.threeDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.threeDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 3);" >
                                                <font color="green" size='3'><b><c:out value="${list.threeDay}"/></b></font> <c:set var="threeDayTotal" value="${threeDayTotal+list.threeDay}"/></a> 
                                                </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.fourDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.fourDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 4);" >
                                                <font color="green" size='3'><b><c:out value="${list.fourDay}"/> </b></font> <c:set var="fourDayTotal" value="${fourDayTotal+list.fourDay}"/> </a>
                                            </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.fiveDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.fiveDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 5);" >
                                                <font color="green" size='3'><b> <c:out value="${list.fiveDay}"/> </b></font> <c:set var="fiveDayTotal" value="${fiveDayTotal+list.fiveDay}"/> </a>
                                            </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.sixDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.sixDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 6);" >
                                                <font color="green" size='3'><b> <c:out value="${list.sixDay}"/> </b></font> <c:set var="sixDayTotal" value="${sixDayTotal+list.sixDay}"/></a>
                                            </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.sevenDay == 0}" >
                                            <font size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.sevenDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 7);" >
                                                <font color="green" size='3'><b><c:out value="${list.sevenDay}"/> </b></font> <c:set var="sevenDayTotal" value="${sevenDayTotal+list.sevenDay}"/></a>
                                            </c:if>
                                    </td>
                                    <td align="center"> 
                                        <c:if test = "${list.moreThanSevenDay == 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${list.moreThanSevenDay!= 0}" >
                                            <a href="#" onclick="openLevel3Report('<c:out value="${list.statusName}"/>',<c:out value="${list.statusId}"/>, 8);" >
                                                <font color="#FF3D03" size='3'><b> <c:out value="${list.moreThanSevenDay}"/></b></font> <c:set var="moreDayTotal" value="${moreDayTotal+list.moreThanSevenDay}"/> </a>                                            
                                            </c:if>
                                    </td>
                                    <td style="text-align: center">

                                        <c:if test = "${orderCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay == 0}" >
                                            <font  size='1'>-</font>
                                        </c:if>
                                        <c:if test = "${orderCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay!= 0}" >
                                            <font color="#FF3D03" size='3'><b>
                                                <c:if test="${list.statusId == '5'}">
                                                    <c:set var="orderCreatedTotal" value="${orderCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${orderCreatedTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '6'}">
                                                    <c:set var="tripCreatedTotal" value="${tripCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripCreatedTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '8'}">
                                                    <c:set var="tripFreezedTotal" value="${tripFreezedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripFreezedTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '10'}">
                                                    <c:set var="tripStartTotal" value="${tripStartTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripStartTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '12'}">
                                                    <c:set var="tripEndTotal" value="${tripEndTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripEndTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '13'}">
                                                    <c:set var="tripClosureTotal" value="${tripClosureTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripClosureTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '14'}">
                                                    <c:set var="tripSettledTotal" value="${tripSettledTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${tripSettledTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '16'}">
                                                    <c:set var="billCreatedTotal" value="${billCreatedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${billCreatedTotal}"/>
                                                </c:if>
                                                <c:if test="${list.statusId == '17'}">
                                                    <c:set var="billSubmittedTotal" value="${billSubmittedTotal+list.zeroDay+list.oneDay+list.twoDay+list.threeDay+list.fourDay+list.fiveDay+list.sixDay+list.sevenDay+list.moreThanSevenDay}"/>
                                                    <c:out value="${billSubmittedTotal}"/>
                                                </c:if>
                                                <c:set var="summaryTotal" value="${list.moreThanSevenDay+orderCreatedTotal+tripCreatedTotal+tripFreezedTotal+tripStartTotal+tripEndTotal+tripClosureTotal+tripSettledTotal+billCreatedTotal+billSubmittedTotal}"/>
                                            </b></font>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>

                            <tr>
                                <td style="text-align: center"><font size='3'><b> Total</b></font></td>
                                <td style="text-align: center">
                                    <c:if test = "${zeroDayTotal+zeroDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${zeroDayTotal+zeroDayTotals != 0}" >
                                        <font color="green" size='3'><b> 
                                            <c:out value="${zeroDayTotal+zeroDayTotals}"/>                                            
                                        </b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${oneDayTotal+oneDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${oneDayTotal+oneDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${oneDayTotal+oneDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${twoDayTotal+twoDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${twoDayTotal+twoDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${twoDayTotal+twoDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${threeDayTotal+threeDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${threeDayTotal+threeDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${threeDayTotal+threeDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${fourDayTotal+fourDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${fourDayTotal+fourDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${fourDayTotal+fourDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${fiveDayTotal+fiveDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${fiveDayTotal+fiveDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${fiveDayTotal+fiveDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${sixDayTotal+sixDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${sixDayTotal+sixDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${sixDayTotal+sixDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${sevenDayTotal+sevenDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${sevenDayTotal+sevenDayTotals != 0}" >
                                        <font color="green" size='3'><b>  <c:out value="${sevenDayTotal+sevenDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center">
                                    <c:if test = "${moreDayTotal+moreDayTotals == 0}" >
                                        <font  size='1'>-</font>
                                    </c:if>
                                    <c:if test = "${moreDayTotal+moreDayTotals != 0}" >
                                        <font color="#FF3D03" size='3'><b> <c:out value="${moreDayTotal+moreDayTotals}"/></b></font>
                                    </c:if>
                                </td>
                                <td style="text-align: center"><font color="#FF3D03" size='3'><b>
                                        <c:out value="${zeroDayTotal+oneDayTotal+twoDayTotal+threeDayTotal+fourDayTotal+fiveDayTotal+sixDayTotal+sevenDayTotal+moreDayTotal+zeroDayTotals+oneDayTotals+twoDayTotals+threeDayTotals+fourDayTotals+fiveDayTotals+sixDayTotals+sevenDayTotals+moreDayTotals}"/>
                                    </b></font></td>
                            </tr>
                        </tbody>
                    </c:if>
                </table>-->
           
            
            </div>
        </div>
    </div>
</body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>