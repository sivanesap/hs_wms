
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
</head> 
<script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

    
    function toexcel(){
        
         document.stockWorthReport.action='/throttle/handleStockWorthExcelRep.do';
        document.stockWorthReport.submit();     
    }
    
      var httpReq;
var temp = "";
function ajaxData()
{
 if(document.stockWorthReport.mfrId.value !='0'){
var url = "/throttle/getModels1.do?mfrId="+document.stockWorthReport.mfrId.value;    
if (window.ActiveXObject)
{
httpReq = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpReq = new XMLHttpRequest();
}
httpReq.open("GET", url, true);
httpReq.onreadystatechange = function() { processAjax(); } ;
httpReq.send(null);
}
}
function processAjax()
{
if (httpReq.readyState == 4)
{
	if(httpReq.status == 200)
	{
	temp = httpReq.responseText.valueOf();           
        setOptions(temp,document.stockWorthReport.modelId);        
        
            if(('<%= request.getAttribute("modelId")%>')!='null' ){
            document.stockWorthReport.modelId.value = <%=request.getAttribute("modelId")%>;
        }
	}
	else
	{
	alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
	}
}
}
function submitPage(){
     var chek=validation();     
      if(chek=='true'){

document.stockWorthReport.action = '/throttle/stockWorthReport.do';
document.stockWorthReport.submit();
}
}
function validation(){
            if(document.stockWorthReport.companyId.value==0){
                alert("Please Select Service Point");
                document.stockWorthReport.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.stockWorthReport.fromDate,'From Date')){
                return 'false';
            }            
            return 'true';
        }
function setValues(){
             
        if('<%=request.getAttribute("companyId")%>'!='null'){
            document.stockWorthReport.companyId.value='<%=request.getAttribute("companyId")%>';
            document.stockWorthReport.fromDate.value='<%=request.getAttribute("fromDate")%>';
        }
        if('<%=request.getAttribute("sectionId")%>'!='null'){
            document.stockWorthReport.sectionId.value='<%=request.getAttribute("sectionId")%>';
        }
        if('<%=request.getAttribute("mfrId")%>'!='null'){
            document.stockWorthReport.mfrId.value='<%=request.getAttribute("mfrId")%>';
        }
        if('<%=request.getAttribute("modelId")%>'!='null'){
            document.stockWorthReport.modelId.value='<%=request.getAttribute("modelId")%>';
             ajaxData();
        }
        if('<%=request.getAttribute("mfrCode")%>'!='null'){
            document.stockWorthReport.mfrCode.value='<%=request.getAttribute("mfrCode")%>';
        }
        if('<%=request.getAttribute("paplCode")%>'!='null'){
            document.stockWorthReport.paplCode.value='<%=request.getAttribute("paplCode")%>';
        }
        if('<%=request.getAttribute("itemName")%>'!='null'){
            document.stockWorthReport.itemName.value='<%=request.getAttribute("itemName")%>';
        }

    }
    function getItemNames(){
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));

    }

    function rateDetails(indx){
        var itemName = document.getElementsByName("itemNam");
        var processId=document.getElementsByName("processId");
         var url = '/throttle/rateDetails.do?processId='+processId[indx].value+'&itemName='+itemName[indx].value;
        window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
        }


function print()
{       
    var DocumentContainer = document.getElementById('print');
    var WindowObject = window.open('', "TrackHistoryData",
        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
    WindowObject.document.writeln(DocumentContainer.innerHTML);
    WindowObject.document.close();
    WindowObject.focus();
    WindowObject.print();
    //WindowObject.close();
}            
            
            
            
</script>
<body onload="setValues();getItemNames();" >
<form name="stockWorthReport" method="post" >
    <%@ include file="/content/common/path.jsp" %>                        
            <%@ include file="/content/common/message.jsp" %>



<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Stock Value Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30">Category</td>
        <td height="30">
            <select name="sectionId" class="textbox" style="width:125px">
                <option value="0">-select-</option>
                <c:if test = "${SectionList != null}" >
                    <c:forEach items="${SectionList}" var="section">
                        <option value='<c:out value="${section.categoryId}"/>'> <c:out value="${section.categoryName}"/> </option>
                    </c:forEach>
                </c:if>
            </select>
        </td>
        <td height="30">MFR</td>
        <td height="30">
            <select name="mfrId" onchange="ajaxData();" class="textbox" style="width:125px">
                <option value="0">-select-</option>
                <c:if test = "${MfrLists != null}" >
                    <c:forEach items="${MfrLists}" var="mfr">
                        <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                    </c:forEach>
                </c:if>
            </select>
        </td>
        <td height="30">Model</td>
        <td height="30"><select name="modelId" class="textbox" style="width:125px" >
                            <option value="0">-select-</option>
                            <c:if test = "${ModelLists != null}" >
                                <c:forEach items="${ModelLists}" var="test">
                                   <option value="<c:out value="${test.modelId}" />"><c:out value="${test.modelName}" /></option>
                                </c:forEach>
                            </c:if>

                    </select>
        </td>
        <td height="30" rowspan="3" valign="middle"><input type="button"  value="Search" class="button" name="Search" onClick="submitPage()"><input type="hidden" value="" name="reqfor"></td>
    </tr>
    <tr>
        <td height="30">Location</td>
        <td height="30">
            <select  class="textbox" name="companyId" style="width:125px">
                    <c:if test = "${companyId == 1011}" >
                        <option value="0">---Select---</option>
                    </c:if>
                    <c:if test = "${LocationLists != null}" >
                    <c:forEach items="${LocationLists}" var="company">
                        <c:if test = "${company.companyTypeId == 1012}" >
                        <c:if test = "${company.cmpId== companyId && companyId != 1011}" >
                            <option selected value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                        </c:if>
                            <c:if test = "${companyId==1011}" >
                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                        </c:if>
                        </c:if>
                    </c:forEach>
                </c:if>
            </select>
        </td>
        <td height="30">Item Name</td>
        <td height="30"><input name="itemName" id="itemName" type="text"  class="textbox" value=""> </td>
        <td height="30">Mfr Item Code</td>
        <td height="30"><input name="mfrCode" type="text" class="textbox" value="" size="20"> </td>        
    </tr>
    <tr>
        <td height="30">PAPL Code</td>
        <td height="30"><input name="paplCode" type="text" class="textbox" value="" size="20"></td>
        <td height="30"><font color="red">*</font>Date</td>
        <td height="30"><input name="fromDate" type="text" class="textbox" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.stockWorthReport.fromDate,'dd-mm-yyyy',this)"/></td>
        <td height="30">Item Type</td>
        <td height="30"><select name="itemType">
                        <option value="NEW">New</option>
                        <option value="RC">RC</option>
                    </select>
        </td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

 <c:set var="totalValue" value="0"/>
 <c:set var="rcValue" value="0"/>
 <c:set var="newValue" value="0"/>
 <c:if test = "${stockWorthList != null}" >
<div id="print">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <tr>
         <td class="contenthead" height="30" colspan="12">Stock Worth Report </td>        
    </tr>
  <tr>
    <td width="30" class="contentsub">Sno</td>
    <!-- <td width="102" height="30" class="contentsub">Manufacturer</td> -->
    <!-- <td width="82" class="contentsub">Model</td> -->
    <td width="80" height="30" class="contentsub">MfrItemCode </td>
    <td width="82" class="contentsub">PAPL Code </td>
    <td width="142" height="30" class="contentsub">Item Name</td>
    <td width="80" height="30" class="contentsub">Category</td>
    <td width="80" height="30" class="contentsub">Group</td>
    <td width="71" height="30" class="contentsub">Item Type</td>
    <td width="70" height="30" class="contentsub">Price</td>
    <td width="70" height="30" class="contentsub">Qty</td>    
    <td width="70" height="30" class="contentsub">Stock Value(INR) </td>
  </tr>
   <% int index = 0;%> 
                    <c:forEach items="${stockWorthList}" var="stockWorth"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>	
   <tr>
    <td class="<%=classText %>"><%=index+1%></td>
    <!--<td class="<%=classText %>" height="30"><c:out value="${stockWorth.mfrName}" /></td> -->
    <!-- <td class="<%=classText %>"><c:out value="${stockWorth.modelName}" /></td> -->
    <input type="hidden" name="sno" value=<%=index+1%>>
    <input type="hidden" name="paplcodes" value='<c:out value="${stockWorth.paplCode}" />'>
    <input type="hidden" name="itemNames" value='<c:out value="${stockWorth.itemName}" />'>    
    <input type="hidden" name="categoryNames" value='<c:out value="${stockWorth.categoryName}" />'>
    <input type="hidden" name="groupNames" value='<c:out value="${stockWorth.groupName}" />'>
    <input type="hidden" name="itemTypes" value='<c:out value="${stockWorth.itemTypes}" />'>
    <input type="hidden" name="itemPrice" value='<c:out value="${stockWorth.itemPrice}" />'>
    <input type="hidden" name="totalQty" value='<c:out value="${stockWorth.totalQty}" />'>
    <input type="hidden" name="stworth" value='<c:out value="${stockWorth.stworth}" />'>
    
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.mfrCode}" /></td> 
    <td class="<%=classText %>"><c:out value="${stockWorth.paplCode}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.itemName}" /> </td>
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.categoryName}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.itemTypes}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.itemPrice}" /></td>
    <td class="<%=classText %>" height="30"><c:out value="${stockWorth.totalQty}" /></td>
    
    <td class="<%=classText %>" align="right" height="30"><c:out value="${stockWorth.stworth}" /><td>
    <c:set var="totalValue" value="${stockWorth.stworth + totalValue}" />
    <c:if test = "${stockWorth.itemTypes =='New'}" >
      <c:set var="newValue" value="${stockWorth.stworth + newValue}" />
      </c:if>
    <c:if test = "${stockWorth.itemTypes =='RC'}" >
      <c:set var="rcValue" value="${stockWorth.stworth + rcValue}" />
      </c:if>
  <input type="hidden" name="processId" value='<c:out value="${stockWorth.processId}" />'>
  
  </tr>
   <% index++;%>
  </c:forEach> 


  <tr>
    <!--<td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td> -->
    <td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2">&nbsp;</td>
     <td class="text2" height="30"><b>New Stock Worth</b> </td>
    <td class="text2" height="30">
    <fmt:setLocale value="en_US" /><b>Rs:<fmt:formatNumber value="${newValue}" pattern="##.00"/></b></td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>

  </tr>
  <tr>
    <!--<td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td> -->
    <td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2">&nbsp;</td>
     <td class="text2" height="30"><b>RC Stock Worth</b> </td>
    <td class="text2" height="30">
    <fmt:setLocale value="en_US" /><b>Rs:<fmt:formatNumber value="${rcValue}" pattern="##.00"/></b></td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>

  </tr>
   <tr>
    <!--<td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td> -->
    <td class="text2">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2">&nbsp;</td>
     <td class="text2" height="30"><b>Total Stock Worth</b> </td>
    <td class="text2" height="30">
    <fmt:setLocale value="en_US" /><b>Rs:<fmt:formatNumber value="${totalValue}" pattern="##.00"/></b></td> 
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
    <td class="text2" height="30">&nbsp;</td>
           
  </tr>
</table>
<style>
.text1 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
}        
.contentsub {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:left;
padding-bottom:8px;
}

.contenthead {
background-image:url(/throttle/images/button.gif);
background-repeat:repeat-x;
height:15px;
font-family:Tahoma;
padding-left:5px;
font-size:11px;
font-weight:bold;
color:#0070D4;
text-align:center;
padding-bottom:8px;
}

</style>    



</div> 
<br>
<center> <input class="button" type="button" value="Print" onClick="print();">    
<input class="button" type="button" value="ExportToExcel" onClick="toexcel();">  
</center>

  </c:if>
</form>
</body>

</html>
