




<%--This is the chart processor code which is used to customise the chart genearted by ceowlf tags below --%>
 

<%--From here its Displaying Fluidity Reports-- --%>

     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
         <script language="javascript">
             function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}
         </script>
    </head>

    
<body onLoad="setValues()" >
    
<form name="chart" method="post" >
    

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Work Order Report</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
    <td>From Date</td>
    <td>
       <input type="text" class="textbox" readonly name="fromDate" value="" >
       <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.fromDate,'dd-mm-yyyy',this)"/>
    </td>

    <td>To Date</td>
    <td>
        <input type="text" class="textbox" readonly name="toDate" value="" >
        <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.chart.toDate,'dd-mm-yyyy',this)"/>
    </td>
    <td><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
</tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>


</form>



<table width='186%' align="center" bgcolor='white' border='0' cellpadding='5' cellspacing='0' class='repcontain'>

    
    
    <c:if test = "${serviceSummary != null}" >
        <tr>
            <td width="2%" class="contentsub">S.No</td>
            <td width="5%" class="contentsub">Job Card</td>
            <td width="2%" class="contentsub">Age</td>
            <td width="5%" class="contentsub">Regn No</td>
            <td width="6%" class="contentsub">Make / Model</td>
            <td width="8%" class="contentsub">Customer</td>
            <td width="22%" class="contentsub">Complaint</td>
            <td width="6%" class="contentsub">In Time</td>
            <td width="8%" class="contentsub">Planned Due Date</td>
            <td width="8%" class="contentsub">Expected Dely Date</td>
            <td width="8%" class="contentsub">Actual Dely Date</td>
            <td width="5%" class="contentsub">Status</td>
            <td width="2%" class="contentsub">Bay</td>
            <td width="13%" class="contentsub">Remarks</td>
        </tr>
        <%
					int index = 1 ;
                    %>
    <c:forEach items="${serviceSummary}" var="serviceSummary">
        <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
    <tr>
        <td class="<%=classText %>"><%=index %></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.jobCardId}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.age}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.regNo}"/></td>
        <td class="<%=classText %>"><c:out value="${serviceSummary.mfrName}"/>/<c:out value="${serviceSummary.modelName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.custName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.problemName}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.inTime}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.scheduledDate}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.pcd}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.acd}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.status}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.bayNo}"/></td>
           <td class="<%=classText %>"><c:out value="${serviceSummary.remarks}"/></td>
        
        
        
     
        
        
</tr>
<%
            index++;
                        %>

   </c:forEach> 
</c:if>


</table>
  
</body>

<script>
    function searchSubmit()
    {        
        document.chart.action="/throttle/handleServiceDailyMIS.do"
        document.chart.submit();
    }
    
function setValues(){    
    if('<%= request.getAttribute("fromDate") %>' != 'null'){
        document.chart.fromDate.value='<%= request.getAttribute("fromDate") %>';
    }        
    if('<%= request.getAttribute("toDate") %>' != 'null'){
        document.chart.toDate.value='<%= request.getAttribute("toDate") %>';
    }    
}    
    
</script>   




