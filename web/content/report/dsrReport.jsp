<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script type="text/javascript">
    function submitPage(value) {

        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == "ExportExcel") {
                document.BPCLTransaction.action = '/throttle/handleVehileUtilize.do?param=ExportExcel';
                document.BPCLTransaction.submit();
            }
            else {
                document.BPCLTransaction.action = '/throttle/handleVehileUtilize.do?param=Search';
                document.BPCLTransaction.submit();
            }
        }
    }
    function submitPage1(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
            return;
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
            return;
        }
        if (value == "ExportExcel") {
            document.DSRReport.action = '/throttle/handleDSRReport.do?param=ExportExcel';
            document.DSRReport.submit();
        }
        else {
            document.DSRReport.action = '/throttle/handleDSRReport.do?param=Search';
            document.DSRReport.submit();
        }

        //    document.BPCLTransaction.action = '/throttle/handleVehileUtilize.do?param=Search';
        //            document.BPCLTransaction.submit();
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Utalization Report" text="DSR Report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.Vehicle Utalization Report" text=" DSR Report"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="DSRReport" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <!-- pointer table -->
                    <!-- message table -->
                    <%@ include file="/content/common/message.jsp"%>

                    <table class="table table-info mb30 table-hover" style="width:45%">
                        <thead><tr><th colspan="4">DSR Report</th></tr></thead>                        
                        <tr>
                            <td><font color="red">*</font>Customer</td>
                            <td height="30">
                                <select class="form-control" style="width:250px;height:40px" name="customerId" id="customerId"  style="width:125px;">
                                    <option value="0">-select-</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    $('#customerId').select2({placeholder: 'Fill Customer Name'});
                                </script>
                            </td>
                            <td>Vehicle Type</td>
                            <td height="30">
                                <select class="form-control" style="width:250px;height:40px" name="vehicleTypeId" id="vehicleTypeId"  style="width:125px;">
                                    <option value="0">-select-</option>
                                    <c:forEach items="${TypeList}" var="vehType">
                                        <option value='<c:out value="${vehType.typeId}"/>'><c:out value="${vehType.typeName}"/></option>
                                    </c:forEach>
                                </select>                             
                            </td>
                        </tr> 
                        
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate"    type="text" class="datepicker" style="width:250px;height:40px" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text"    class="datepicker" style="width:250px;height:40px"></td>
                        </tr>
                        
                        <tr>
                            <td colspan="4" align="center">
                                <input type="button" class="btn btn-success" name="search" onclick="submitPage1(this.name);" value="Search">
                                &ensp;&ensp;
                                <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel">
                            </td>                            
                        </tr>

                    </table>
                    <%
                        String month = "0";
                        String year = "0";
                        if (request.getAttribute("month") != null) {
                            month = (String) request.getAttribute("month");
                        }
                        if (request.getAttribute("year") != null) {
                            year = (String) request.getAttribute("year");
                        }
                    %>
                    <script>
                        document.getElementById("month").value = '<%=month%>';
                        document.getElementById("year").value = '<%=year%>';
                    </script>
                    <c:if test="${dsrReport != null}">
                        <table  id="table" class="table table-info mb30 table-hover">

                            <thead>
                                <tr height="50">
                                    <th class="text1" align="center">S.No</th>
                                    <th class="text1" align="center">Transport type</th>
                                    <th class="text1" align="center">Invoice</th>
                                    <th class="text1" align="center">Carrier Name</th>
                                    <th class="text1" align="center">Docket/LR/AWB</th>
                                    <th class="text1" align="center">Second Leg Docket</th>
                                    <th class="text1" align="center">Second Leg  Invoice</th>
                                    <th class="text1" align="center">MOT</th>
                                    <th class="text1" align="center">Shipping warehouse</th>
                                    <th class="text1" align="center">Destination City</th>
                                    <th class="text1" align="center">Qty</th>
                                    <th class="text1" align="center">No of Boxes </th>
                                    <th class="text1" align="center">No of Pallets</th>
                                    <th class="text1" align="center">Actual weight in kg</th>
                                    <th class="text1" align="center">Type of Vehicle</th>
                                    <th class="text1" align="center">Vehicle Reg</th>
                                    <th class="text1" align="center">Pickup Date</th>
                                    <th class="text1" align="center">Pickup Time</th>
                                    <th class="text1" align="center">Permit</th>
                                    <th class="text1" align="center">Customer Name</th>
                                    <th class="text1" align="center">End Customer Name</th>
                                    <th class="text1" align="center">EDD</th>
                                    <th class="text1" align="center">Vehicle Arrival Date</th>
                                    <th class="text1" align="center">Actual Delivered Date</th>
                                    <th class="text1" align="center">Delivered Time</th>
                                    <th class="text1" align="center">Status</th>
                                    <th class="text1" align="center">Vehicle Remarks</th>
                                    <th class="text1" align="center">Total Distance (KMS)</th>
                                    <th class="text1" align="center">Distance Covered</th>
                                    <th class="text1" align="center">Distance remaining</th>
                                    <th class="text1" align="center">Reasons for delay</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>

                                <c:forEach items="${dsrReport}" var="dsr">
                                    <%
                                        String classText = "";

                                        int oddEven = index % 2;

                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }

                                    %>

                                    <tr>
                                        <td class="<%=classText%>"><%=index++%></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.orderType}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.invoiceNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.companyName}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.docketNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.secondaryDocketNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.secondaryInvoice}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripmode}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.origin}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.destination}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.quantity}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.boxes}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.totalPackages}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.invoiceWeight}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.vehicletype}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.regNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripStartDate}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripstarttime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.eWayBillNo}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.consignorName}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.consigneeName}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.planeDate}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.destinationReportingDateTime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.actualEndDateTime}"/></td>
                                        <td class="<%=classText%>"><c:out value="${dsr.tripEndTime}"/></td>
                                        <td class="<%=classText%>">
                                        <c:if test="${dsr.status == 8}">
                                        Yet to Start
                                        </c:if>
                                        <c:if test="${dsr.status == 10}">
                                        In-Transit
                                        </c:if>
                                        <c:if test="${dsr.status > 10}">
                                        Delivered
                                        </c:if>
                                        </td>
                                        <td  class="<%=classText%>"  >NA</td>
                                        
                                        <td  class="<%=classText%>"  ><c:out value="${dsr.startKM}"/></td>
                                        <td  class="<%=classText%>"  >0</td>
                                        <td  class="<%=classText%>"  >0</td>
                                        <td  class="<%=classText%>"  >NA</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>


                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20" selected="selected">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>

                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
