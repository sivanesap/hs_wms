
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

    <head>
        <%@page import="java.text.SimpleDateFormat" %>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
            <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
            <%@ page import="java.util.* "%>
            <%@ page import=" javax. servlet. http. HttpServletRequest" %>
            <%@ page import="java.text.DecimalFormat" %>
            <%@ page import="java.text.NumberFormat" %>
            <script type="text/javascript" src="/throttle/js/validate.js"></script>
            <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
            <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
            <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
            <style type="text/css" title="currentStyle">
                @import "/throttle/css/layout-styles.css";
                @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
            </style>

            <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

            <!-- jQuery libs -->
            <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
            <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

            <!-- Our jQuery Script to make everything work -->
            <!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
            <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
            <script src="/throttle/js/jquery.ui.core.js"></script>
            <script src="/throttle/js/jquery.ui.datepicker.js"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    //alert("cv");
                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        yearRange: '1900:' + new Date().getFullYear(),
                        changeMonth: true, changeYear: true
                    });
                });

            </script>
            <script  type="text/javascript" src="js/jq-ac-script.js"></script>


            <script type="text/javascript" language="javascript">
                $(document).ready(function() {
                    $("#tabs").tabs();
                });
            </script>
            <script type="text/javascript">
                function runApp() {
                    document.enter.action = '/throttle/sendWFLWFUAlertsMail.do?param=Data';
                    document.enter.submit();
                }
                function viewWflWfuBlobData(wfuWflLogId) {
                    window.open('/throttle/content/trip/displayWflWfuData.jsp?wfuWflLogId='+wfuWflLogId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                }
            </script>
        </head>
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="ManageUser"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="Settings"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="ManageUser"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
        <body>
            <form name="enter" action=""  method="post">
                <br>
                <br>
                <br>
                <!--<a href="runApp('file://c:/Apps1/BrattleFoods/batchprogram/throttlealerts/version1/throttlealerts/batchScripts/brattleWflWfuAlerts.bat');">Batch File</a>-->
                <!--<a href="runApp('/throttle/sendWFLWFUAlertsMail.do');" onclick="runApp();">Batch File</a>-->
                <table>
                    <tr>
                        <td><input type="button" name="sendMail" id="sendMail" class="btn btn-success" style="width: 260px" value="Click TO Download" onclick="runApp()"/></td>
                        <c:if test="${wfuWflLogId != null}">
                        <td>&nbsp;&nbsp;<input type="button" name="exportExcel" id="exportExcel" class="btn btn-success" width="120px" value="Export Excel" onclick="viewWflWfuBlobData('<c:out value="${wfuWflLogId}"/>')"/></td>
                        </c:if>
                    </tr>
                </table>
                <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String startDate = sdf.format(today);
                %>
                <br>


                <div id="tabs" >
                    <ul>
                        <li><a href="#summaryDetails"><span>Summary Details</span></a></li>
                        <li><a href="#wflDetails"><span>WFL</span></a></li>
                        <li><a href="#wfuDetails"><span>WFU</span></a></li>
                        <li><a href="#tripInProgress"><span>Trip-In Progress</span></a></li>
                        <li><a href="#tripNotStarted"><span>Trip Not Started</span></a></li>
                        <li><a href="#jobCard"><span>Job Card</span></a></li>
                        <li><a href="#futureTrips"><span>Future Trips</span></a></li>
                    </ul>

                    <div id="summaryDetails">
                         <table class="table table-info mb10 table-hover" id="bg" >
                             <thead>
                            <tr>
                                <th >&nbsp;</th>
                                <th >North-South-Corridor</th>
                                <th >North-West-Corridor</th>
                                <th >Dedicate</th>
                                <th >General</th>
                                <th >&nbsp;</th>
                                <th >Total</th>
                            </tr>
                            </thead>
                            <c:set var="wflTotal" value="0"/>
                            <c:set var="wfuTotal" value="0"/>
                            <c:set var="progressTotal" value="0"/>
                            <c:set var="notStartTotal" value="0"/>
                            <c:set var="jobTotal" value="0"/>
                            <thead>
                            <tr >
                                <td>WFL</td>
                                <td><c:out value="${wfl_North_South_Corridor}"/></td>
                                <td><c:out value="${wfl_North_West_Corridor}"/></td>
                                <td><c:out value="${wfl_Dedicate}"/></td>
                                <td><c:out value="${wfl_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${wfl_North_West_Corridor+wfl_North_South_Corridor+wfl_Dedicate+wfl_General}"/></td>
                                <c:set var="wflTotal" value="${wfl_North_West_Corridor+wfl_North_South_Corridor+wfl_Dedicate+wfl_General}"/>
                            </tr>
                            
                            <tr class="text2">
                                <td>WFU</td>
                                <td><c:out value="${wfu_North_South_Corridor}"/></td>
                                <td><c:out value="${wfu_North_West_Corridor}"/></td>
                                <td><c:out value="${wfu_Dedicate}"/></td>
                                <td><c:out value="${wfu_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${wfu_North_West_Corridor+wfu_North_South_Corridor+wfu_Dedicate+wfu_General}"/></td>
                                <c:set var="wfuTotal" value="${wfu_North_West_Corridor+wfu_North_South_Corridor+wfu_Dedicate+wfu_General}"/>
                            </tr>
                            <tr >
                                <td>TRIP IN PROGRESS</td>
                                <td><c:out value="${progress_North_South_Corridor}"/></td>
                                <td><c:out value="${progress_North_West_Corridor}"/></td>
                                <td><c:out value="${progress_Dedicate}"/></td>
                                <td><c:out value="${progress_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${progress_North_West_Corridor+progress_North_South_Corridor+progress_Dedicate+progress_General}"/></td>
                                <c:set var="progressTotal" value="${progress_North_West_Corridor+progress_North_South_Corridor+progress_Dedicate+progress_General}"/>
                            </tr>
                            <tr class="text2">
                                <td>TRIP NOT START</td>
                                <td><c:out value="${notStart_North_South_Corridor}"/></td>
                                <td><c:out value="${notStart_North_West_Corridor}"/></td>
                                <td><c:out value="${notStart_Dedicate}"/></td>
                                <td><c:out value="${notStart_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${notStart_North_West_Corridor+notStart_North_South_Corridor+notStart_Dedicate+notStart_General}"/></td>
                                <c:set var="notStartTotal" value="${notStart_North_West_Corridor+notStart_North_South_Corridor+notStart_Dedicate+notStart_General}"/>
                            </tr>
                            <tr >
                                <td>JOB</td>
                                <td><c:out value="${job_North_South_Corridor}"/></td>
                                <td><c:out value="${job_North_West_Corridor}"/></td>
                                <td><c:out value="${job_Dedicate}"/></td>
                                <td><c:out value="${job_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${job_North_West_Corridor+job_North_South_Corridor+job_Dedicate+job_General}"/></td>
                                <c:set var="jobTotal" value="${job_North_West_Corridor+job_North_South_Corridor+job_Dedicate+job_General}"/>
                            </tr>
                            <tr class="text2">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr >
                                <td>Total</td>
                                <td><c:out value="${wfl_North_South_Corridor+wfu_North_South_Corridor+progress_North_South_Corridor+notStart_North_South_Corridor+job_North_South_Corridor}"/></td>
                                <td><c:out value="${wfl_North_West_Corridor+wfu_North_West_Corridor+progress_North_West_Corridor+notStart_North_West_Corridor+job_North_West_Corridor}"/></td>
                                <td><c:out value="${wfl_Dedicate+wfu_Dedicate+progress_Dedicate+notStart_Dedicate+job_Dedicate}"/></td>
                                <td><c:out value="${wfl_General+wfu_General+progress_General+notStart_General+job_General}"/></td>
                                <td>&nbsp;</td>
                                <td><c:out value="${wflTotal+wfuTotal+progressTotal+notStartTotal+jobTotal}"/></td>
                            </tr>
                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                        <br/>

                    </div>
                    <div id="wflDetails">
                         <table class="table table-info mb10 table-hover" id="bg" >
                            <c:if test="${wflList != null}">
                                <% int index = 0;%> 
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Capacity</th>
                                    <th >Current Location</th>
                                    <th >Responsible Person</th>
                                    <th >Zone</th>
                                    <th >Status</th>
                                    <th >Customer</th>
                                    <th >Loading Plan</th>
                                    <th >Revenue</th>
                                </tr>
                                </thead>
                                <c:forEach items="${wflList}" var="wfl">
                                    <%
                                    String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index+1%></td>
                                        <td  height="30"><c:out value="${wfl.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${wfl.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${wfl.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${wfl.tonnage}"/></td>
                                        <td  height="30"><c:out value="${wfl.cityName}"/></td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30"><c:out value="${wfl.zoneName}"/></td>
                                        <td  height="30">WFL</td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30">&nbsp;</td>
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </c:if>

                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                        <br>
                    </div>

                    <div id="wfuDetails">
                        <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test="${wfuList != null}">
                                <% int index1 = 0;%> 
                                
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >Trip No</th>
                                    <th >Capacity</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Current Location</th>
                                    <!--<td >Responsible Person</td>-->
                                    <th >Status</th>
                                    <th >Customer Name</th>
                                    <th >WFU(Reporting Time)</th>
                                    <th >Client</th>
                                    <th >Loading Person</th>
                                    <th >Revenue</th>
                                </tr>
                                </thead>
                                <c:forEach items="${wfuList}" var="wfu">
                                    <%
                                    String classText1 = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText1 = "text2";
                                        } else {
                                            classText1 = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index1+1%></td>
                                        <td  height="30"><c:out value="${wfu.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${wfu.tripCode}"/></td>
                                        <td  height="30"><c:out value="${wfu.tonnage}"/></td>
                                        <td  height="30"><c:out value="${wfu.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${wfu.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${wfu.cityName}"/></td>
                                        <!--<td  height="30">&nbsp;</td>-->
                                        <td  height="30">WFU</td>
                                        <td  height="30"><c:out value="${wfu.customerName}"/></td>
                                        <td  height="30"><c:out value="${wfu.vehiclewfutime}"/></td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30">&nbsp;</td>
                                    </tr>
                                    <%index1++;%>
                                </c:forEach>
                            </c:if>

                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <div id="tripInProgress">
                         <table class="table table-info mb10 table-hover" id="bg" >
                            <c:if test="${tripInProgressList != null}">
                                <% int index2 = 0;%> 
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >Trip No</th>
                                    <th >Capacity</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Destination Location</th>
                                    <th >Current Location</th>
                                    <!--<td >Responsible Person</td>-->
                                    <th >Status</th>
                                    <th >Customer Name</th>
                                    <th >Trip Start Time</th>
                                    <th >Revenue</th>
                                </tr>
                                </thead>
                                <c:forEach items="${tripInProgressList}" var="progress">
                                    <%
                                    String classText2 = "";
                                        int oddEven2 = index2 % 2;
                                        if (oddEven2 > 0) {
                                            classText2 = "text2";
                                        } else {
                                            classText2 = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index2+1%></td>
                                        <td  height="30"><c:out value="${progress.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${progress.tripCode}"/></td>
                                        <td  height="30"><c:out value="${progress.tonnage}"/></td>
                                        <td  height="30"><c:out value="${progress.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${progress.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${progress.location}"/></td>
                                        <td  height="30"><c:out value="${progress.cityName}"/></td>
                                        <!--<td  height="30">&nbsp;</td>-->
                                        <td  height="30">Trip In-Progress</td>
                                        <td  height="30"><c:out value="${progress.customerName}"/></td>
                                        <td  height="30"><c:out value="${progress.tripstarttime}"/></td>
                                        <td  height="30"><c:out value="${progress.estimatedRevenue}"/></td>
                                    </tr>
                                    <%index2++;%>
                                </c:forEach>
                            </c:if>

                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </div>
                    <div id="tripNotStarted">
                        <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test="${tripNotStartedList != null}">
                                <% int index3 = 0;%> 
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >Trip No</th>
                                    <th >Capacity</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Current Location</th>
                                    <!--<td >Responsible Person</td>-->
                                    <th >Status</th>
                                    <th >Customer Name</th>
                                    <th >Trip Start Time</th>
                                    <th >Revenue</th>
                                </tr>
                                </thead>
                                <c:forEach items="${tripNotStartedList}" var="notStart">
                                    <%
                                    String classText3 = "";
                                        int oddEven3 = index3 % 2;
                                        if (oddEven3 > 0) {
                                            classText3 = "text2";
                                        } else {
                                            classText3 = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index3+1%></td>
                                        <td  height="30"><c:out value="${notStart.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${notStart.tripCode}"/></td>
                                        <td  height="30"><c:out value="${notStart.tonnage}"/></td>
                                        <td  height="30"><c:out value="${notStart.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${notStart.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${notStart.cityName}"/></td>
                                        <!--<td  height="30">&nbsp;</td>-->
                                        <td  height="30">Trip Not Start</td>
                                        <td  height="30"><c:out value="${notStart.customerName}"/></td>
                                        <td  height="30"><c:out value="${notStart.tripstarttime}"/></td>
                                        <td  height="30"><c:out value="${notStart.estimatedRevenue}"/></td>
                                    </tr>
                                    <%index3++;%>
                                </c:forEach>
                            </c:if>

                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                    </div>
                    <div id="jobCard">
                        <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test="${jobCardList != null}">
                                <% int index4 = 0;%> 
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Capacity</th>
                                    <th >Current Location</th>
                                    <th >Status</th>
                                    <th >Type</th>
                                </tr>
                                </thead>
                                <c:forEach items="${jobCardList}" var="job">
                                    <%
                                    String classText4 = "";
                                        int oddEven4 = index4 % 2;
                                        if (oddEven4 > 0) {
                                            classText4 = "text2";
                                        } else {
                                            classText4 = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index4+1%></td>
                                        <td  height="30"><c:out value="${job.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${job.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${job.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${job.tonnage}"/></td>
                                        <td  height="30"><c:out value="${job.cityName}"/></td>
                                        <td  height="30"><c:out value="${job.statusName}"/></td>
                                        <td  height="30"><c:out value="${job.serviceTypeName}"/></td>
                                    </tr>
                                    <%index4++;%>
                                </c:forEach>
                            </c:if>

                        </table>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                        </center>
                        <br>
                        <br>
                    </div>
                    <div id="futureTrips">
                        <table  border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test="${futureTripList != null}">
                                <% int index5 = 0;%> 
                                <thead>
                                <tr>
                                    <th >S No</th>
                                    <th >Truck No</th>
                                    <th >Trip No</th>
                                    <th >Capacity</th>
                                    <th >FC</th>
                                    <th >FC Person</th>
                                    <th >Current Location</th>
                                    <th >Responsible Person</th>
                                    <th >Status</th>
                                    <th >Current Customer</th>
                                    <th >Trip Start Time</th>
                                    <th >Revenue</th>
                                </tr>
                                </thead>
                                <c:forEach items="${futureTripList}" var="future">
                                    <%
                                    String classText5 = "";
                                        int oddEven5 = index5 % 2;
                                        if (oddEven5 > 0) {
                                            classText5 = "text2";
                                        } else {
                                            classText5 = "text1";
                                        }
                                    %>
                                    <tr width="208" height="40">
                                        <td  height="30"><%=index5+1%></td>
                                        <td  height="30"><c:out value="${future.vehicleNo}"/></td>
                                        <td  height="30"><c:out value="${future.tripCode}"/></td>
                                        <td  height="30"><c:out value="${future.tonnage}"/></td>
                                        <td  height="30"><c:out value="${future.operationPoint}"/></td>
                                        <td  height="30"><c:out value="${future.fleetCenterHead}"/></td>
                                        <td  height="30"><c:out value="${future.cityName}"/></td>
                                        <td  height="30">&nbsp;</td>
                                        <td  height="30"><c:out value="${future.statusName}"/></td>
                                        <td  height="30"><c:out value="${future.customerName}"/></td>
                                        <td  height="30"><c:out value="${future.tripstarttime}"/></td>
                                        <td  height="30"><c:out value="${future.estimatedRevenue}"/></td>
                                    </tr>
                                    <%index5++;%>
                                </c:forEach>
                            </c:if>

                        </table>

                        <br/>
                        <br/>
                    </div>
                    <script>
                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                    </script>
                </div>
            </form>
        </body>
    </html>
</form>
</body>


</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
