<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>



<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>

<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>



<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>-->

<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>




<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel" style="overflow: auto;height:1600px">

    <div class="row">

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">
                                
                                <h4> <spring:message code="dashboard.label.Vendors"  text="Vendors"/></h4>

                                <h1><span id="totalVendors"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>

                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->


        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-happy"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Customers"  text="Customers"/></h4>
                                <h1><span id="totalCustomers"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
   
      

<!--    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns 
                                <h5  class="panel-title"> <font color="white"><b><spring:message code="dashboard.label.RegionWise"  text="Region Wise Order"/></b></font></h5>
                                <div  id="regionWise" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div></div>
            </td>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns 
                                <h5  class="panel-title"> <font color="white"><b><spring:message code="dashboard.label.StatusWise"  text="Status Wise Trip"/></b></font></h5>
                                <div  id="statusWise" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div></div>
            </td>
        </tr>
    </table>-->
    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
<!--                                <div >
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>     
                                <h5  class="panel-title"> <font color="white"><b><spring:message   text="Delivery Status"/></b></font></h5>
                                </div>-->
                                <!--<font color="white"> Delivery Status  </font>-->
                                <div  id="deliverystatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            
            
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
<!--                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns -->
                                <!--<h5  class="panel-title"> <font color="white"><b><spring:message   text="Order Status"/></b></font></h5>-->
                                <div  id="orderstatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            
            
            
            
        </tr>
    </table>
    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
<!--                                <div >
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>     
                                <h5  class="panel-title"> <font color="white"><b><spring:message   text="Invoice Status"/></b></font></h5>
                                </div>-->
                                <!--<font color="white"> Delivery Status  </font>-->
                                <div  id="invoicestatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
<!--                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns -->
                                <!--<h5  class="panel-title"> <font color="white"><b><spring:message   text="Trip Status"/></b></font></h5>-->
                                <div  id="tripstatus" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            
        </tr>
    </table>
    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
<!--                                <div >
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>     
                                <h5  class="panel-title"> <font color="white"><b><spring:message   text="Pending Delivery OverView"/></b></font></h5>
                                </div>-->
                                <!--<font color="white"> Delivery Status  </font>-->
                                <div  id="pendingdeliveryoverview" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer5">
                                <!--<div class="panel-btns">-->
                                    <!--<a href="" class="panel-close">&times;</a>-->
                                    <!--<a href="" class="minimize">&minus;</a>-->
                                <!--</div> panel-btns--> 
                                <!--<h5  class="panel-title"> <font color="white"><b><spring:message   text="Delivered Overview(Month-iswW)"/></b></font></h5>-->
                                <div  id="deliveredoverview" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            </div>                        
                        </div>
                    </div>
                        
                </div>
            </td>
            
            
        </tr>
    </table>
                                
                                
    <table style="width:100%">
        <tr>
            <td>
                <div class="row" >
                    <!--<div class="col-sm-8 col-md-9">-->
                        <div class="panel panel-default">

                            <!--<div class="outer5">-->
<!--                                <div >
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div>     
                                <h5  class="panel-title"> <font color="white"><b><spring:message   text="Order OverView(MonthWise)"/></b></font></h5>
                                </div>-->
                                <!--<font color="white"> Delivery Status  </font>-->
                                <div  id="orderoverview" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                            <!--</div>-->                        
                        </div>
                    <!--</div>-->
                        
                </div>
            </td>
            
            
            
            
        </tr>
    </table>
                                
                                
                                
  
    
    <br>
    <br>
    
       

    <style>
        .outer1 {
            width: 600px;
            color: navy;            
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 600px;
            color: navy;
            background-color: #ccad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer5 {
            width: 500px;
            color: navy;            
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
      
        .ben{
            width: 600px;
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        .highcharts-figure, .highcharts-data-table table {
    min-width: 320px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}


input[type="number"] {
	min-width: 50px;
}





.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

        
        
    </style>



</div><!-- contentpanel -->

</div><!-- mainpanel -->

<%@ include file="../common/NewDesign/settings.jsp" %>



<script>

    regionWise();
    function regionWise() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getOverAllTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])

                $('#regionWise').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
    
    
    
    
    
    
    
    deliverystatus();
    function deliverystatus() {
                Highcharts.chart('deliverystatus', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Delivery Status'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    accessibility: {
        point: {
            valueSuffix: ''
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}'
            }
        }
    },
    series: [{
        name: 'Nos',
        colorByPoint: true,
        data: [{
            name: 'Delivered',
            y: 6,
            sliced: false,
            selected: false
        }, {
            name: 'Delivery Scheduled',
            y: 10
        }, {
            name: 'Delivery Pending',
            y: 4
        }]
    }]
});
    }
    
    
    
    pendingdeliveryoverview();
    function pendingdeliveryoverview() {
                Highcharts.chart('pendingdeliveryoverview', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Month Wise Pending Delivery'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    accessibility: {
        point: {
            valueSuffix: ''
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}'
            }
        }
    },
    series: [{
        name: 'Nos',
        colorByPoint: true,
        data: [{
            name: 'Jan-2020',
            y: 6,
            sliced: false,
            selected: false
        }, {
            name: 'Feb-2020',
            y: 10
        }, 
        {
            name: 'March-2020',
            y: 4
        },
        {
            name: 'April-2020',
            y: 10
        },
        {
            name: 'May-2020',
            y: 6
        },
        {
            name: 'June-2020',
            y: 7
        },
        {
            name: 'July-2020',
            y: 1
        },
        {
            name: 'August-2020',
            y: 11
        },
        {
            name: 'September-2020',
            y: 0
        },
        {
            name: 'October-2020',
            y: 2
        },
        {
            name: 'November-2020',
            y: 3
        },
        {
            name: 'December-2020',
            y: 2
        },
    ]
    }]
});
    }
    
    
    deliveredoverview();
    function deliveredoverview() {
                Highcharts.chart('deliveredoverview', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Month Wise Delivered Count'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    accessibility: {
        point: {
            valueSuffix: ''
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}'
            }
        }
    },
    series: [{
        name: 'Nos',
        colorByPoint: true,
        data: [{
            name: 'Jan-2020',
            y: 100,
            sliced: false,
            selected: false
        }, {
            name: 'Feb-2020',
            y: 50
        }, 
        {
            name: 'March-2020',
            y: 40
        },
        {
            name: 'April-2020',
            y: 100
        },
        {
            name: 'May-2020',
            y: 60
        },
        {
            name: 'June-2020',
            y: 70
        },
        {
            name: 'July-2020',
            y: 10
        },
        {
            name: 'August-2020',
            y: 110
        },
        {
            name: 'September-2020',
            y: 90
        },
        {
            name: 'October-2020',
            y: 20
        },
        {
            name: 'November-2020',
            y: 30
        },
        {
            name: 'December-2020',
            y: 20
        },
    ]
    }]
});
    }
    
    
    
    
    
    
    
    
    
    tripstatus();
    function tripstatus() {
                Highcharts.chart('tripstatus', {
    chart: {
        type: 'cylinder',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    title: {
        text: 'Trip Status'
    },
    plotOptions: {
        series: {
            depth: 35,
            colorByPoint: true
        }
    },
    xAxis: {
        categories: ['Trip Frezed', 'Trip Started', 'Trip End','Trip Closure'],
        
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        max: 100,
        title: {
            text: 'Number of Vehicle',
            skew3d: true
        }
    },
    series: [{
    
        data: [{
            name: 'Trip Frezed',
            y: 16
         
        }, {
            name: 'Trip Started',
            y: 10
        }, {
            name: 'Trip End',
            y: 24
        }, {
            name: 'Trip Closure',
            y: 0
        }],
        name: 'Nos',
        showInLegend: false
    }]
});
    }
    
    
    invoicestatus();
    function invoicestatus() {
    Highcharts.chart('invoicestatus', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Invoice Status'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: ''
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}</b>'
    },

    series: [
        {
            name: "Invoice Status",
            colorByPoint: true,
            data: [
                {
                    name: "Hire Vedor to be Invoiced",
                    y: 6,
                    
                },
                {
                    name: "Invoice Genrated For End Customer",
                    y: 11,
                    
                },
                {
                    name: "Dedicated Vendor To Be Invoiced",
                    y: 7,
                    
                },
                
            ]
        }
    ],
    
});
    }
    
    
    orderoverview();
    function orderoverview() {
    Highcharts.chart('orderoverview', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Month Wise Order Count'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Booking Count(Nos)'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}</b>'
    },

    series: [
        {
            name: "Count",
            colorByPoint: true,
            data: [
                {
                    name: "January",
                    y: 6,
                    
                },
                {
                    name: "Febuary",
                    y: 11,
                    
                },
                {
                    name: "March",
                    y: 17,
                    
                },
                {
                    name: "April",
                    y: 27,
                    
                },
                {
                    name: "May",
                    y: 37,
                    
                },
                {
                    name: "June",
                    y: 7,
                    
                },
                {
                    name: "July",
                    y: 17,
                    
                },
                {
                    name: "August",
                    y: 30,
                    
                },
                {
                    name: "Septemper",
                    y: 43,
                    
                },
                {
                    name: "October",
                    y: 23,
                    
                },
                
                {
                    name: "November",
                    y:17,
                    
                },
                
                {
                    name: "December",
                    y: 7,
                    
                },
                
            ]
        }
    ],
    
});
    }
    orderstatus();
    function orderstatus() {
    Highcharts.chart('orderstatus', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Order Status'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: ''
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<b>{point.y}</b>'
    },

    series: [
        {
            name: "Order Status",
            colorByPoint: true,
            data: [
                {
                    name: "Order Created",
                    y: 6,
                    
                },
                {
                    name: "Order Canceled",
                    y: 11,
                    
                }
                
            ]
        }
    ],
    
});
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

  routeWiseTrip();
    function routeWiseTrip() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getRouteWiseTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#routeWiseTrip').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }

    //custRevenue();
    function custRevenue() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getCustRevenu.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.CustName;
                    x_values_sub['y'] = parseInt(value.Revenu);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#custRevenue').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }


    //custProfit();
    function custProfit() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getCustProfit.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.CustName;
                    x_values_sub['y'] = parseInt(value.Profit);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#custProfit').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
    
    loadMonthlyTripStatus();
    function loadMonthlyTripStatus() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/getStatusWiseTripNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    x_values_sub['name'] = value.Name;
                    x_values_sub['y'] = parseInt(value.Count);
                    x_values.push(x_values_sub);
                    x_values_sub = {};
                });

                $('#statusWise').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y} ',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }
</script>

 <script>
    loadTrucknos();
    function loadTrucknos() {
        $.ajax({
            url: '/throttle/getDashBoardOperationNos.do',
            dataType: 'json',
            success: function (data) {
            //alert(data);
                $.each(data, function (key, value) {
                    //alert(value.Name);
                    if (value.Name == 'Vendor') {
                        $("#totalVendors").text(value.Count);
                    }  else if (value.Name == 'Customer') {
                        $("#totalCustomers").text(value.Count);
                    }

               

                });
//                        vehicleMakeArray.push(value.Make);
//                        vehicleCountArray.push([parseInt(value.Count)]);
            }
        });
    }
</script>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>-->
<!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->

