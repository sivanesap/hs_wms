<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Run Sheet Waybill Print Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
        <%@page import="java.util.ArrayList"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" language="javascript" src="/throttle/js/qrcode.min.js"></script>
    <script type="text/javascript" language="javascript" src="/throttle/js/qrcode.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>
    <head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
    <script>
        function initMap() {
            var service = new google.maps.DirectionsService;
            var start = new google.maps.LatLng("22.81434", "86.18916");
            var mapOptions = {
                zoom: 7

            };
            var map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
            var routePoint = document.getElementsByName("locId");
            var latitude = document.getElementsByName("latitude");
            var longitude = document.getElementsByName("longitude");
            var w = routePoint.length;
            // list of points
            var stations = [];
            var bounds = [];

            bounds.push({
                lat: "22.81434", lng: "86.18916"
            });

            for (var j = 0; j < w; j++) {

                stations.push({
                    lat: latitude[j].value, lng: longitude[j].value, name: routePoint[j].value
                });
                bounds.push({
                    lat: latitude[j].value, lng: longitude[j].value
                });
            }

            // Zoom and center map automatically by stations (each station will be in visible map area)
            var lngs = stations.map(function(station) {
                return station.lng;
            });
            var lats = stations.map(function(station) {
                return station.lat;
            });
            var lngss = bounds.map(function(station) {
                return station.lng;
            });
            var latss = bounds.map(function(station) {
                return station.lat;
            });

            map.fitBounds({
                west: Math.min.apply(null, lngss),
                east: Math.max.apply(null, lngss),
                north: Math.min.apply(null, latss),
                south: Math.max.apply(null, latss),
            });

            var marker = start;
            var image = '/throttle/images/warehouse.png';
            var beachMarker = new google.maps.Marker({
                map: map,
                position: marker,
                animation: google.maps.Animation.DROP,
                title: "WAREHOUSE",
                icon: image

            });
            var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var labelIndex = 0;
            // Show stations on the map as markers
            for (var i = 0; i < stations.length; i++) {

                var marker = new google.maps.LatLng(stations[i].lat, stations[i].lng);

                //            image = '/throttle/images/wowPIC.jpg';

                var image = '/throttle/images/truck.png';
                var beachMarker = new google.maps.Marker({
                    map: map,
                    position: marker,
                    animation: google.maps.Animation.DROP,
                    title: stations[i].name,
                    label: labels[labelIndex++ % labels.length]
                });
            }

            // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
            for (var i = 0, parts = [], max = 25 - 1; i < stations.length; i = i + max)
                parts.push(stations.slice(i, i + max + 1));
            // Service callback to process service results
            var service_callback = function(response, status) {
                if (status != 'OK') {
                    console.log('Directions request failed due to  ' + status);
                    return;
                }
                var renderer = new google.maps.DirectionsRenderer;
                renderer.setMap(map);
                renderer.setOptions({suppressMarkers: true, preserveViewport: true, polylineOptions: {
                        strokeColor: 'black', strokeWeight: 3, strokeOpacity: 1.0
                    }});
                renderer.setDirections(response);
                //            computeTotalDistance(response);

            };

            for (var i = 0; i < parts.length; i++) {
                // Waypoints does not include first station (origin) and last station (destination)
                var waypoints = [];
                for (var j = 0; j < parts[i].length - 1; j++)
//                waypoints.push({location: parts[i][j], stopover: false});
                    waypoints.push({location: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng), stopover: false});
                // Service options
                var service_options = {
                    origin: start,
                    destination: new google.maps.LatLng(parts[i][parts[i].length - 1].lat, parts[i][parts[i].length - 1].lng),
                    waypoints: waypoints,
                    optimizeWaypoints: true,
                    travelMode: 'WALKING'
                };

                // Send request
                service.route(service_options, service_callback);

            }


        }


    </script>

</head>
<body>
    <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <c:if test = "${runsheetprintList != null}" >
    <table class="table table-info mb30 table-hover" id="table" style="width:90%" >
        <thead>
            <tr height="40">
                <th>S.No</th>
                <th>Status</th>
                <th>Created ON</th>
                <th>Created By</th>
                </tr>
        </thead>
        <tbody>
            
            <% int index = 0, sno = 1;%>
            <c:forEach items="${runsheetprintList}" var="customer">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr height="30">
                    <td align="left" ><%=sno%></td>
                     <td align="left" ><c:out value="${customer.point1Name}"/> </td>
                    <td align="left" ><c:out value="${customer.tripStartDate}"/> </td>
                     <td align="left" ><c:out value="${customer.tripCode}"/> </td>
                    
                    </tr>
                <%
                            index++;
                            sno++;
                %>
            </c:forEach>

        </tbody>
    </table>
</c:if>

    </form>



</body>
<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>
</html>









