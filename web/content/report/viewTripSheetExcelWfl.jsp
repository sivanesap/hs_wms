<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripSheet-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

        <c:if test="${tripDetails != null}">
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="70">
                         <th><h3>Sno</h3></th>
			 <th><h3>Trip Code</h3></th>
			 <th><h3>Vehicle No </h3></th>
			 <th><h3>Wfl Hours</h3></th>
			 <th><h3>Wfu Hours</h3></th>
			 <th><h3>Loading Transit Hours</h3></th>
			 <th><h3>Transit Hours</h3></th>
			 <th><h3>Start Date Time</h3></th>
			 <th><h3>End Date Time</h3></th>
			 <th><h3>WFU Date Time</h3></th>
			 <th><h3>Origin Report Date Time</h3></th>
			 <th><h3>Destination Report Date Time</h3></th>
			 <th><h3>Run Km </h3></th>
			 <th><h3>Run Hm </h3></th>
			 <th><h3>Estimated Revenue</h3></th>
			 <th><h3>Estimated Expense</h3></th>
			 <th><h3>Paid Advance</h3></th>
			 <th><h3>Actual Expense</h3></th>
			 <th><h3>Customer Name </h3></th>
			 <th><h3>Route </h3></th>
			 <th><h3>Vehicle Type </h3></th>
			 <th><h3>Fleet Center</h3></th>
                        <th><h3>Trip Status</h3></th>

                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="" width="40" align="center"><%=index%></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.regno}"/></td>
                            <td class="" width="40" align="center"><c:out value="${tripDetails.wflHours}"/></td>
                            <td class="" width="40" align="center"><c:out value="${tripDetails.wfuHours}"/></td>
                            <td class="" width="40" align="center"><c:out value="${tripDetails.loadingTransitHours}"/></td>
                            <td class="" width="40" align="center"><c:out value="${tripDetails.transitHours}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.startDateTime}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.endDateTime}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.wfuDateTime}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.originReportingDateTime}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.destinationReportingDateTime}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.totalRunKm}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.totalRunHm}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.estimatedRevenue}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.rcm}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.actualAdvancePaid}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.actualExpense}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.customerName}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.routeInfo}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.fleetCenterName}"/></td>
                            <td class="" width="40" align="left"><c:out value="${tripDetails.statusName}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <
    </form>
</body>
</html>
