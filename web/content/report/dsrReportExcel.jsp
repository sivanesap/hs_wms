<%-- 
    Document   : displayTripSheetWiseExcel
    Created on : Jun 11, 2013, 10:38:22 PM
    Author     : Entitle
--%>

<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body>
        <form name="tripwise" action=""  method="post">
            <%
                        DecimalFormat df = new DecimalFormat("0.00##");
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "DSR Report.xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
            <br/>
            
             <c:if test="${dsrReport != null}">
                        <table  id="table" class="table table-info mb30 table-hover">

                            <thead>
                                <tr height="50">
                                    <th align="center">S.No</th>
                                    <th align="center">Transport type</th>
                                    <th align="center">Invoice</th>
                                    <th align="center">Carrier Name</th>
                                    <th align="center">Docket/LR/AWB</th>
                                    <th align="center">Second Leg Docket</th>
                                    <th align="center">Second Leg  Invoice</th>
                                    <th align="center">MOT</th>
                                    <th align="center">Shipping warehouse</th>
                                    <th align="center">Destination City</th>
                                    <th align="center">Qty</th>
                                    <th align="center">No of Boxes </th>
                                    <th align="center">No of Pallets</th>
                                    <th align="center">Actual weight in kg</th>
                                    <th align="center">Type of Vehicle</th>
                                    <th align="center">Vehicle Reg</th>
                                    <th align="center">Pickup Date</th>
                                    <th align="center">Pickup Time</th>
                                    <th align="center">Permit</th>
                                    <th align="center">Customer Name</th>
                                    <th align="center">End Customer Name</th>
                                    <th align="center">EDD</th>
                                    <th align="center">Vehicle Arrival Date</th>
                                    <th align="center">Actual Delivered Date</th>
                                    <th align="center">Delivered Time</th>
                                    <th align="center">Status</th>
                                    <th align="center">Vehicle Remarks</th>
                                    <th align="center">Total Distance (KMS)</th>
                                    <th align="center">Distance Covered</th>
                                    <th align="center">Distance remaining</th>
                                    <th align="center">Reasons for delay</th>


                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;%>

                                <c:forEach items="${dsrReport}" var="dsr">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>

                                    <tr>
                                        <td class="<%=classText%>"><%=index++%></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.orderType}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.invoiceNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.companyName}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.docketNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.secondaryDocketNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.secondaryInvoice}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.tripmode}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.origin}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.destination}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.quantity}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.boxes}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.totalPackages}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.invoiceWeight}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.vehicletype}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.regNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.tripStartDate}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.tripstarttime}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.eWayBillNo}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.consignorName}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.consigneeName}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.planeDate}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.destinationReportingDateTime}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.actualEndDateTime}"/></td>
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.tripEndTime}"/></td>
                                        <td width="30" class="<%=classText%>"  >
                                        <c:if test="${dsr.status == 8}">
                                        Yet to Start
                                        </c:if>
                                        <c:if test="${dsr.status == 10}">
                                        In-Transit
                                        </c:if>
                                        <c:if test="${dsr.status >= 12}">
                                        Delivered
                                        </c:if>
                                        </td>
                                        <td width="30" class="<%=classText%>"  >NA</td>
                                        
                                        <td width="30" class="<%=classText%>"  ><c:out value="${dsr.startKM}"/></td>
                                        <td width="30" class="<%=classText%>"  >0</td>
                                        <td width="30" class="<%=classText%>"  >0</td>
                                        <td width="30" class="<%=classText%>"  >NA</td>


                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>


                    </c:if>
        </form>
    </body>
</html>
