<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
    <head>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.department.business.DepartmentTO" %>  
        <title>Parveen Auto Care</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
     <script type="text/javascript">
        function submitPage(value){            
            if(value == 'add'){
                document.manageDepartment.action = "/throttle/addDepartmentPage.do";
                document.manageDepartment.submit();
            }else if(value == 'alter'){
            document.manageDepartment.action = "/throttle/viewAlterDept.do";
            document.manageDepartment.submit();
        }
    }
    </script>
    
    <body>
        <form name="manageDepartment" action="file:///F|/Tomcat%205.5/webapps/throttle/content/Department/addDepartment.do" method="post" >

    
                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageDepartment" text="ManageDepartment"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ManageDepartment" text="ManageDepartment"/></li>
		
		                </ol>
		            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                     <%@ include file="/content/common/message.jsp" %>
      <c:if test = "${DepartmentLists != null}" >
            <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                <tr id="tableDesingTH" height="30">   
                    <th width="60" align="left"  scope="col">Sno</th>
                    <th width="92" align="left"  scope="col">Name</th>
                    <th width="83" align="left"  scope="col">Description</th>
                    <th width="67" align="center"  scope="col">Status</th>
                </tr>
                </thead>
                <% int index = 0;%>              
                    <c:forEach items="${DepartmentLists}" var="department"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr height="30">    
                            <td align="left" ><%= index + 1  %></td>
                            <td align="left" ><c:out value="${department.name}"/></td>
                            <td align="left" ><c:out value="${department.description}"/></td>
                            <td align="left" >
                                <c:if test="${(department.status=='y') || (department.status=='Y')}" >
                                    Active
                                </c:if>
                                <c:if test="${(department.status=='n') || (department.status=='N')}" >
                                    InActive
                                </c:if>                            
                            </td>
                        </tr>   
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>   
            </table>
            <br>
            <center><input type="button" class="btn btn-success" value="Add"  onclick="submitPage('add')"  /> &nbsp; <input type="button" class="btn btn-success" value="Alter" onClick="submitPage('alter');"/></center>
        </form>
                                    <script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 7);
        </script>
    </body>   
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
