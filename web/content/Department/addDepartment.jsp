<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
    function submitpage1()
    {
        if (isEmpty(document.dept.name.value))
        {
         alert("please enter the Name");
         document.dept.name.focus();
        }else if (isEmpty(document.dept.description.value))
        {
         alert("please enter the Description");
         document.dept.description.focus();
        }else{
       document.dept.action= "/throttle/addDepartment.do";
       document.dept.submit();}
    }
    function setFocus(){
        document.dept.name.focus();
        }
</script>
<body onload="setFocus();">
    
                    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Add Department" text="Add Department"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Add Department" text="Add Department"/></li>
		
		                </ol>
		            </div>
        </div> 
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
<form name="dept"  method="post">    
<%--<%@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" %>--%>


<table class="table table-info mb30 table-hover" id="bg" >
    <thead>
<tr id="tableDesingTH">
<th colspan="4" ><div >Add Department</div></th>
</tr>
<tr>
<td  height="20"><font color="red">*</font>Department Name</td>
<td  height="20"><input name="name" type="text" class="form-control" style="width:250px;height:40px" value=""></td>

<td  height="20"><font color="red">*</font>Description</td>
<td  height="20"><textarea class="form-control" style="width:250px;height:40px" name="description"></textarea></td>
</tr>
    </thead>
</table>
<br>
<center>
<input type="button" value="Save" class="btn btn-success" onclick="submitpage1();">
&emsp;<input type="reset" class="btn btn-success" value="Clear">
</center>
</form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
