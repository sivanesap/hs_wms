<%--
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });


    var httpRequest;
    function checkRouteCode() {
        var cityFromId = document.getElementById('cityFromId').value;
        var cityToId = document.getElementById('cityToId').value;
        if (cityFromId != '' && cityToId != '') {
            var url = '/throttle/checkSecondaryRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#routeStatus").text('Route Exists Code is :' + val);
                } else {
                    $("#routeStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    //savefunction
    function submitPage(value) {
        var count = validateRouteDetails();
        if(document.getElementById('distance').value == '' ){
            alert("enter the travel distance");
            document.getElementById('distance').focus();
        }else if(count == 0){
        document.route.action = '/throttle/updateSecondaryRoute.do';
        document.route.submit();
        }
    }


function validateRouteDetails(){
        var vehMileage = document.getElementsByName("vehMileage");
        var reefMileage =  document.getElementsByName("reefMileage");
        var fuelCostPerKms =  document.getElementsByName("fuelCostPerKms");
        var fuelCostPerHrs = document.getElementsByName("fuelCostPerHrs");
        var tollAmounts = document.getElementsByName("tollAmounts");
        var miscCostKm = document.getElementsByName("miscCostKm");
        var driverIncenKm = document.getElementsByName("driverIncenKm");
        var factor = document.getElementsByName("factor");
        var vehExpense = document.getElementsByName("vehExpense");
        var reeferExpense = document.getElementsByName("reeferExpense");
        var totExpense = document.getElementsByName("totExpense");

        var a = 0;
        var count=0;
        for(var i=0 ; i<vehMileage.length; i++ ){
            a = i+1;
            if(fuelCostPerKms[i].value == ''){
                alert("please fill fuel cost per km for row "+a);
                fuelCostPerKms[i].focus();
                count=1;
                return count;
            }else if(fuelCostPerHrs[i].value == ''){
                alert("please fill fuel cost per hrs for row "+a);
                fuelCostPerHrs[i].focus();
                count=1;
                return count;
            }else if(tollAmounts[i].value == ''){
                alert("please fill toll rate per km for row "+a);
                tollAmounts[i].focus();
                count=1;
                return count;
            }else if(miscCostKm[i].value == ''){
                alert("please fill miscellaneous cost per km for row "+a);
                miscCostKm[i].focus();
                count=1;
                return count;
            }else if(driverIncenKm[i].value == ''){
                alert("please fill driver incentive per km for row "+a);
                driverIncenKm[i].focus();
                count=1;
                return count;
            }else if(factor[i].value == ''){
                alert("please fill factor for row "+a);
                factor[i].focus();
                count=1;
                return count;
            }else if(vehExpense[i].value == ''){
                alert("please fill vehExpense for row "+a);
                vehExpense[i].focus();
                count=1;
                return count;
            }else if(reeferExpense[i].value == ''){
                alert("please fill reeferExpense for row "+a);
                reeferExpense[i].focus();
                count=1;
                return count;
            }else if(totExpense[i].value == ''){
                alert("please fill totExpense for row "+a);
                totExpense[i].focus();
                count=1;
            }
        }
        return count;
    }


//    function numbersOnly(oToCheckField, oKeyEvent) {
//    return oKeyEvent.charCode === 0 || /\d/.test(String.fromCharCode(oKeyEvent.charCode));
//  }
function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


//
//    function numeralsOnly(evt) {
//       evt = (evt) ? evt : event;
//        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
//           ((evt.which) ? evt.which : 0));
//        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//           alert("Enter numerals only in this field.");
//           return false;
//          }
//           return true;
//   }


   
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body onload="setTollCost()">
        <form name="secondaryRoute"  method="post">
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <c:if test="${routeList != null}">


            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <c:forEach items="${routeList}" var="rl">
                <tr>
                    <td class="contenthead" colspan="4" >Route  Edit</td>
                </tr>
                <tr>
                    <td class="text1">Route Code</td>
                    <td class="text1"><input type="hidden" name="routeId" id='routeId' class="textbox" value="<c:out value="${rl.routeId}"/>"/><input type="hidden" name="routeCode" id='routeCode' class="textbox" value="<c:out value="${rl.routeCode}"/>" readonly><c:out value="${rl.routeCode}"/></td>
                    <td class="text1">Fuel Cost</td>
                    <td class="text1"><c:out value="${rl.fuelPrice}"/></td>
                </tr>
                <tr>
                    <td class="text2">From Location</td>
                    <td class="text2"><input type="hidden" name="cityFromId" id="cityFromId" class="textbox" value="<c:out value="${rl.cityFromId}"/>"/><c:out value="${rl.cityFromName}"/></td>
                    <td class="text2">To Location</td>
                    <td class="text2"><input type="hidden" name="cityToId" id="cityToId" class="textbox" value="<c:out value="${rl.cityToId}"/>"/><c:out value="${rl.cityToName}"/></td>
                </tr>
                <tr>
                    <td class="text1">KM</td>
                    <td class="text1"><input type="text" name="distance" id="distance" class="textbox" onkeyup="calcFuleCostPerKm()" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;" value="<c:out value="${rl.distance}"/>"></td>
                    <td class="text1">Travel Time(Hrs)</td>
                    <td class="text1">HR:<input type="text"  style="width:38px" name="travelHour" id="travelHour" class="textbox" onkeyup="calcReferHours()" value="<c:out value="${rl.travelHour}"/>" readonly/>MI:<input type="text" name="travelMinute" style="width:38px" id="travelMinute" class="textbox" onkeyup="calcReferHours()" value="<c:out value="${rl.travelMinute}"/>" readonly/></td>
                </tr>
                <tr>
                    <td class="text2">Reefer Running Hours</td>
                    <td class="text2">HR:<input type="text"  style="width:38px" name="reeferHour" id="reeferHour" class="textbox" value="<c:out value="${rl.reeferHour}"/>" readonly/>MI:<input type="text" name="reeferMinute" style="width:38px" id="reeferMinute" class="textbox" value="<c:out value="${rl.reeferMinute}"/>" readonly/></td>
                    <td class="text2">Road Type</td>
                    <td class="text2">
                        <select name="roadType" id="roadType">
                            <c:if test="${rl.roadType == 'National Highway'}">
                            <option value="NH" selected>National Highway</option>
                            <option value="SH" >State Highway</option>
                            </c:if>
                            <c:if test="${rl.roadType == 'State Highway'}">
                            <option value="NH">National Highway</option>
                            <option value="SH" selected>State Highway</option>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text1">Toll Amount Type</td>
                    <td class="text1">
                        <input type="hidden" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />Average
                       <%-- <c:if test="${rl.tollAmountType == '1'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" checked  />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" />Fixed
                        </c:if>
                        <c:if test="${rl.tollAmountType == '2'}">
                        <input type="radio" name="tollAmountType" id="tollAmountType" onclick="setTollCost(1);" value="1" />Average
                        <input type="radio" name="tollAmountType" id="tollAmountType" value="2"  onclick="setTollCost(2);" value="2" checked  />Fixed
                        </c:if>  --%>
                        </td>
                    <td class="text1">Current Fuel Cost</td>
                    <td class="text1"><input type="hidden" name="fuelCost" id="fuelCost"   value="<%=request.getAttribute("currentFuelPrice")%>" readonly/><label><%=request.getAttribute("currentFuelPrice")%></label></td>
                </tr>
                <tr>
                    <td class="text2">Status</td>
                    <td class="text2">
                        <select name="status" id="status">
                            <option value="Y">Active</option>
                            <option value="N">In Active</option>
                        </select>
                        <script>
                            document.getElementbyId('status').value = '<c:out value="${rl.status}"/>'
                        </script>
                    </td>

                    <td colspan="2" class="text2">
                        <center>
                            <input type="button" class="button" value="Save"  name='Save' onclick="submitPage()"/>
                        </center>
                    </td>
                </tr>
                 <tr>
                    <td><input type="hidden" id="avgTollAmount" name="avgTollAmount" value="<c:out value="${avgTollAmount}"/>" ></td>
                    <td><input type="hidden" name="avgMisCost" id="avgMisCost"  value="<c:out value="${avgMisCost}"/>"/></td>
                    <td><input type="hidden" name="avgDriverIncentive" id="avgDriverIncentive"  value="<c:out value="${avgDriverIncentive}"/>"/></td>
                    <td><input type="hidden" name="avgFactor" id="avgFactor" value="<c:out value="${avgFactor}"/>"/></td>
                </tr>
                </c:forEach>
            </table>
            </c:if>
            <script type="text/javascript">
                //concard value get from the addrout configDetails
                function setTollCost(type) {
                    if (type == 1) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var reeferExpense = document.getElementsByName('reeferExpense');
                        var factor = document.getElementsByName('factor');
                        var vehExpense = document.getElementsByName('vehExpense');
                        for (var i = 0; i < tollAmounts.length; i++) {
                            tollAmounts[i].value = document.getElementById('avgTollAmount').value;
                            misCost[i].value = document.getElementById('avgMisCost').value;
                            driverIncentive[i].value = document.getElementById('avgDriverIncentive').value;
                            factor[i].value = document.getElementById('avgFactor').value;
                            if (fuelCostPerKms[i].value != '') {
                                var fuelAmnt = fuelCostPerKms[i].value * km;
                            } else {
                                var fuelAmnt = 0 * km;
                            }
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (misCost[i].value != '') {
                                var misCostPerKm = misCost[i].value * km;
                            } else {
                                var misCostPerKm = 0 * km;
                            }
//                            if (factor[i].value != '') {
//                                var factorPerKm = factor[i].value * km;
//                            } else {
//                                var factorPerKm = 0 * km;
//                            }
                            if (driverIncentive[i].value != '') {
                                var driverIncentivePerKm = driverIncentive[i].value * km;
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            if (fuelCostPerKms[i].value != '') {
//                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt) + parseFloat(factorPerKm);
                                var total = parseFloat(tollAmnt) + parseFloat(misCostPerKm) + parseFloat(driverIncentivePerKm) + parseFloat(fuelAmnt);
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                var driverIncentivePerKm = 0 * km;
                            }
                            driverIncentive[i].readOnly = true;
                            misCost[i].readOnly = true;
                            tollAmounts[i].readOnly = true;
                            factor[i].readOnly = true;
                            calcTotExp();
                        }
                    } else if (type == 2) {
                        var km = document.getElementById("distance").value;
                        var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                        var vehExpense = document.getElementsByName('vehExpense');
                        var tollAmounts = document.getElementsByName("tollAmounts");
                        var misCost = document.getElementsByName("miscCostKm");
                        var driverIncentive = document.getElementsByName("driverIncenKm");
                        var factor = document.getElementsByName("factor");
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            if (fuelCostPerKms[i].value != '') {
                                var total = fuelCostPerKms[i].value * km
                                vehExpense[i].value = total.toFixed(2);
                            } else {
                                vehExpense[i].value = 0.00;
                            }
                            tollAmounts[i].value = "";
                            misCost[i].value = '';
                            driverIncentive[i].value = '';
                            factor[i].value = '';
                            tollAmounts[i].readOnly = false;
                            driverIncentive[i].readOnly = false;
                            misCost[i].readOnly = false;
                            factor[i].readOnly = false;
                            calcTotExp();
                        }
                    }
                }

                //calculate fuelCostPerKm
                function calcFuleCostPerKm() {
                    var km = document.getElementById("distance").value;
                    var fuelCostPerKm = 0;
                    var fuelCost = document.getElementById('fuelCost').value;
                    var vehMileage = document.getElementsByName('vehMileage');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factor = document.getElementsByName('factor');
                    if (km != '') {
                        for (var i = 0; i < vehMileage.length; i++) {
                            var totFuelCost = parseInt(km) / parseInt(vehMileage[i].value) * parseInt(fuelCost);
                            fuelCostPerKm = parseInt(totFuelCost) / parseInt(km);
                            fuelCostPerKms[i].value = fuelCostPerKm.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < vehMileage.length; i++) {
                            fuelCostPerKms[i].value = '';
                        }
                    }
                    calcVehExp();

                    //Calc Vehicle Running Hours
                    var perDayTravelKm = 450;
                    var travelKm = km / perDayTravelKm;
                    var hours = travelKm.toFixed(2) * 24;
                    var travelHrs = hours.toFixed(2);
                    //alert(travelHrs);
                    var temp = travelHrs.split(".");
                    travelHrs = temp[0];
                    //alert(travelHrs);
                    var travelMinute = temp[1];
                    var hour = 0;
                    var minute = 0;
                    if (temp[1] > 59) {
                        for (var travelHour = 0; travelMinute > 59; travelHrs++) {
                            hour = travelHour + 1;
                            travelMinute = parseInt(travelMinute) - 60;
                            minute = travelMinute;
                        }
                    } else {
                        hour = 0;
                        minute = travelMinute;
                    }
                    var tothour = hour + parseInt(temp[0]);
                    //alert(tothour);
                     if (tothour < 10) {
                        tothour = '0' + tothour;
                    }
                    //if (minute < 10 && (minute.indexOf("0") != -1) == 'false') {
                    if (minute < 10) {
                        minute = '0' + minute;
                    }
//                    alert(minute.indexOf("0") != -1);
                    document.getElementById("travelHour").value = tothour;
                    //alert(document.getElementById("travelHour").value);
                    document.getElementById("travelMinute").value = minute;
                    calcReferHours();
                }
                //calculate Vehicle Expenses cost
                function calcVehExp() {
                    var km = document.route.distance.value;
                    var tollAmounts = document.getElementsByName('tollAmounts');
                    var miscCostKm = document.getElementsByName('miscCostKm');
                    var driverIncenKm = document.getElementsByName('driverIncenKm');
                    var fuelCostPerKms = document.getElementsByName('fuelCostPerKms');
                    var factorPerKm = document.getElementsByName('factor');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    if (km != '') {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            var expCost = parseInt(km) * fuelCostPerKms[i].value;
                            if (tollAmounts[i].value != '') {
                                var tollAmnt = tollAmounts[i].value * km;
                            } else {
                                var tollAmnt = 0 * km;
                            }
                            if (miscCostKm[i].value != '') {
                                var misCost = miscCostKm[i].value * km;
                            } else {
                                var misCost = 0 * km;
                            }
                            if (driverIncenKm[i].value != '') {
                                var driverIncentive = driverIncenKm[i].value * km;
                            } else {
                                var driverIncentive = 0 * km;
                            }
//                            if (factorPerKm[i].value != '') {
//                                var factor = factorPerKm[i].value * km;
//                            } else {
//                                var factor = 0 * km;
//                            }
//                            var total = parseFloat(expCost) + parseFloat(tollAmnt) + parseFloat(misCost) + parseFloat(driverIncentive) + parseFloat(factor);
//                            alert("parseFloat(expCost)"+parseFloat(expCost));
//                            alert("parseFloat(tollAmnt)"+parseFloat(tollAmnt));
//                            alert("parseFloat(misCost)"+parseFloat(misCost));
//                            alert("parseFloat(driverIncentive)"+parseFloat(driverIncentive.toFixed(2)));
                            var total = parseFloat(expCost.toFixed(2)) + parseFloat(tollAmnt.toFixed(2)) + parseFloat(misCost.toFixed(2)) + parseFloat(driverIncentive.toFixed(2));
                            vehExpense[i].value = total.toFixed(2);
                        }
                    } else {
                        for (var i = 0; i < fuelCostPerKms.length; i++) {
                            vehExpense[i].value = '';
                            fuelCostPerHrs[i].value = '';
                            reeferExpense[i].value = '';
                            totExpense[i].value = '';
                        }
                    }
                    calcTotExp();
                }

                //calculate reffer hours
                //calculate reefer hours
                function calcReferHours() {
                    var km = document.getElementById("distance").value;
                      //Calc reefer Running Hours
                    var perDayTravelKm1 = 600;
                    var travelKm1 = km / perDayTravelKm1;
                    var hours = travelKm1.toFixed(2) * 24;
                    var travelHrs = hours.toFixed(2);
                        var travelHour2 = travelHrs / 100;
                        var travelHour3 = (travelHour2 * 66).toFixed(2);
                        var refMinute = travelHour3;
                        var reeferHrs = travelHour3;
                        var temp = reeferHrs.split(".");
                        var reeferHrs1 = temp[0];
                        var reeferMinute = temp[1];
                        var hour = 0;
                        var minute = 0;
                        if (reeferMinute > 59) {
                            for (var reeferHour = 0; reeferMinute > 59; reeferHrs1++) {
                                var hour1 = reeferHour + 1;
                                reeferMinute = parseInt(reeferMinute) - 60;
                                minute = reeferMinute;
                            }
                                 hour=reeferHrs1 + hour
                        } else {
                            hour = reeferHrs1;
                            minute = reeferMinute;
                        }
                    if (hour < 10) {
                        hour = '0' + hour;
                    }

                    if (minute < 10) {
                        minute = '0' + minute;
                    }
                    if(minute == ''){
                        minute == '0';
                    }
                    document.getElementById("reeferHour").value = parseFloat(hour);
                    document.getElementById("reeferMinute").value = parseFloat(minute);


                    if (refMinute != 00) {
//                    alert(refMinute);
                        calcFuleCostPerHr(refMinute);
                    }
                }

                //calculate fuelCostPerHr
                function calcFuleCostPerHr(refMinute) {
//  
                    var fuelCost = document.getElementById('fuelCost').value;
                    var reefMileage = document.getElementsByName('reefMileage');
                    var fuelCostPerHrs = document.getElementsByName('fuelCostPerHrs');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    for (var i = 0; i < reefMileage.length; i++) {
                        //alert("refMinute==="+refMinute.toFixed(2));
                   /*     var totFuleCostPerMinute = parseInt(refMinute) / parseInt(reefMileage[i].value) * parseInt(fuelCost);
                        var fuleCostPerMinute = parseInt(totFuleCostPerMinute) / parseInt(refMinute);
                        var fuelCostPerHour = fuleCostPerMinute * 60;
                        fuelCostPerHrs[i].value = fuelCostPerHour.toFixed(2);
                        var totalReeferCastPerMinute = parseInt(refMinute) * parseInt(fuelCostPerHrs[i].value);
                        var refExpCal = totalReeferCastPerMinute / 60;
                        reeferExpense[i].value = refExpCal.toFixed(2);
                    */
                        //alert(reefMileage[i].value);
                        
                        var referExp =  reefMileage[i].value * fuelCost;
                        fuelCostPerHrs[i].value = referExp.toFixed(2);
                        //alert(fuelCostPerHrs[i].value);
                        //alert(refMinute.toFixed(2));
                        var totalReefExp = fuelCostPerHrs[i].value * refMinute;
                        //alert(totalReefExp.toFixed(2));
                        reeferExpense[i].value = totalReefExp.toFixed(2);


                    }
                    calcTotExp();
                }


                function calcTotExp() {
                    var km = document.route.distance.value;
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var vehExpense = document.getElementsByName('vehExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    for (var i = 0; i < vehExpense.length; i++) {
                        if (vehExpense[i].value != '') {
                            var vehExp = vehExpense[i].value;
                        } else {
                            var vehExp = 0;
                        }
                        if (reeferExpense[i].value != '') {
                            var refExp = reeferExpense[i].value;
                        } else {
                            var refExp = 0;
                        }
                        var totCost = parseFloat(vehExp) + parseFloat(refExp);
                        totExpense[i].value = totCost.toFixed(2);
                    }
                }

                function calcTollExp(index) {
                    alert("hi...");
                    var tollAmounts = document.getElementById('tollAmounts' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(tollAmounts != ''){
                    var totExp = parseFloat(tollAmounts) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }
                }
                function calcMiscExp(index) {
                    var miscCostKm = document.getElementById('miscCostKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(miscCostKm != ''){
                        var totExp = parseFloat(miscCostKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcDriExp(index) {
                    var driverIncenKm = document.getElementById('driverIncenKm' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(driverIncenKm != ''){
                    var totExp = parseFloat(driverIncenKm) * km + parseFloat(vehExp);
                    document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }
                function calcFactorExp(index) {
                    var factorPerKm = document.getElementById('factor' + index).value;
                    var km = document.getElementById('distance').value;
                    var vehExp = document.getElementById('vehExpense' + index).value;
                    //var reeferExp = document.getElementById('reeferExpense'+index).value;
                    if(factorPerKm !=''){
                        var totExp = parseFloat(factorPerKm) * km + parseFloat(vehExp);
                        document.getElementById('totExpense' + index).value = totExp.toFixed(2);
                    }

                }

            </script>
            <br>
            <br>
            <br>
            <br>
            <% int count = 0;%>
            <c:if test="${routeDetailsList != null}">
                <table width="96%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="70">
                            <th><h3>S.No</h3></th>
                            <th><h3>Vehicle Type </h3></th>
                          
                            <th><h3>Fuel Cost Per Km's</h3></th>
                            <th><h3>Fuel Cost Per Hr's </h3></th>
                            <th><h3>Toll Cost Per KM1 </h3></th>
                            <th><h3>Misc Cost Per KM</h3></th>
                            <th><h3>Driver Incentive Per KM</h3></th>
                            <th><h3>Factor</h3></th>
                            
                            <th><h3>Vehicle Expense Cost</h3></th>
                            <th><h3>Reefer Expense Cost</h3></th>
                            <th><h3>Total Expense Cost</h3></th>
                        </tr>
                    </thead>
                 <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${routeDetailsList}" var="rdl">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>

                            <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>" style="width: 200px"><input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${rdl.vehicleTypeId}"/>" readonly/><c:out value="${rdl.vehicleTypeName}"/>
                                    <input type="hidden" name="vehMileage" id="vehMileage" value="<c:out value="${rdl.vehicleMileage}"/>"/>
                                    <input type="hidden" name="reefMileage" id="reefMileage" value="<c:out value="${rdl.reeferMileage}"/>"/>
                                </td>
                                <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerKms" id="fuelCostPerKms<%=index%>"   style="width: 90px" value="<c:out value="${rdl.fuelCostKm}"/>" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="fuelCostPerHrs" id="fuelCostPerHrs<%=index%>"  style="width: 90px" value="<c:out value="${rdl.fuelCostHr}"/>"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="tollAmounts" id="tollAmounts<%=index%>"  style="width: 90px" onkeyup="calcTollExp('<%=index%>')" value="<c:out value="${rdl.tollAmountperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="miscCostKm" id="miscCostKm<%=index%>"   style="width: 90px" onkeyup="calcMiscExp('<%=index%>')" value="<c:out value="${rdl.miscCostperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="driverIncenKm" id="driverIncenKm<%=index%>"  style="width: 90px" onkeyup="calcDriExp('<%=index%>')" value="<c:out value="${rdl.driverIncentperkm}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="factor" id="factor<%=index%>"  style="width: 90px" onkeyup="calcFacExp('<%=index%>')" value="<c:out value="${rdl.factors}"/>" onkeypress="return onKeyPressBlockCharacters(event)"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="vehExpense" id="vehExpense<%=index%>"style="width: 90px"  value="<c:out value="${rdl.vehiExpense}"/>" readonly/>
                                 <input type="hidden" name="varExpense" id="varExpense" value="<c:out value="${rdl.variExpense}"/>"/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="reeferExpense" id="reeferExpense<%=index%>"  style="width: 90px" value="<c:out value="${rdl.reefeExpense}"/>" readonly/></td>
                                <td align="left" class="<%=classText%>"><input type="text" name="totExpense" id="totExpense<%=index%>"   style="width: 90px" value="<c:out value="${rdl.totaExpense}"/>" readonly/></td>
                            </tr>
                            <%sno++;%>
                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");</script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
            <br>
        </form>
    </body>
</html>
