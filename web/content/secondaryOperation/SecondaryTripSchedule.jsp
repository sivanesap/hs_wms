<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    </head>
    <script language="javascript">
        function submitPage(value) {
            if (value == 'nextWeek') {
                //alert("nextWeek");
                document.viewSecondaryTrip.action = '/throttle/secondaryTripSheduleNext.do';
                document.viewSecondaryTrip.submit();
            } else {
                var checkedValue = document.getElementsByName('checkedValue');
                var secRouteIds = document.getElementsByName('secRouteId');
                var cntr = 0;
                var cntr1 = 0;
                var temp = 0;
                var temp1 = 0;
                var statusTrue = "";
                var statusFalse = "";
                //alert(checkedValue.length);
                for (var i = 0; i < checkedValue.length; i++) {
                    if (checkedValue[i].checked == true) {
                        statusTrue = checkedValue[i].value + "~Y";
                        if (cntr == 0) {
                            temp = statusTrue;
                            document.getElementById("scheduleDatesTrue").value = statusTrue;
                        } else {
                            temp = temp + "," + statusTrue;
                            document.getElementById("scheduleDatesTrue").value = temp;


                        }
                        cntr++;
                    } else {
                        statusFalse = checkedValue[i].value + "~N";
                        if (cntr1 == 0) {
                            temp1 = statusFalse;
                            document.getElementById("scheduleDatesFalse").value = statusFalse;
                        } else {
                            temp1 = temp1 + "," + statusFalse;
                            document.getElementById("scheduleDatesFalse").value = temp1;
                        }
                        cntr1++;
                    }
                }
                var passedValue = document.getElementById("scheduleDatesTrue").value + "," + document.getElementById("scheduleDatesFalse").value;
                document.getElementById("passedValue").value = passedValue;
                document.viewSecondaryTrip.action = '/throttle/saveSecondaryTripSchedule.do';
                document.viewSecondaryTrip.submit();
            }
        }

    </script>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ScheduleTrip" text="ScheduleTrip"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.SecondaryOperations" text="SecondaryOperations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ScheduleTrip" text="ScheduleTrip"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body>
        <form name="viewSecondaryTrip" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>

                <table class="table table-info mb30 table-hover">
                    <thead><tr><th colspan="4">Trip Schedule</th></tr></thead>
                    <tr>
                        <td height="30">Customer Code</td>
                        <td>
                            <input name="customerCode" id="customerCode" type="hidden" class="form-control"  onclick="ressetDate(this);">
                            <input name="passedValue" id="passedValue" type="hidden" class="form-control"  value="">
                            <c:out value="${customerCode}"/></td>
                        <td height="30">Customer Name</td>
                            <td><input name="customerId" id="customerId" type="hidden" class="textbox" value="<c:out value="${customerId}"/>" onClick="ressetDate(this);"><c:out value="${customerName}"/></td>
                    </tr>
                    <tr>
                        <td height="30">Contract Start Date</td>
                        <td><input name="fromDate" id="fromDate" type="hidden" class="textbox"  onclick="ressetDate(this);"><c:out value="${fromDate}"/></td>
                        <td height="30">Contract End Date</td>
                        <td><input name="toDate" id="toDate" type="hidden" class="textbox" onClick="ressetDate(this);"><c:out value="${toDate}"/></td>
                    </tr>
                    <!--                                    <tr>
                                                            <td height="30">Contract Code</td>
                                                            <td><input name="contractCode" id="contractCode" type="text" class="textbox"  onclick="ressetDate(this);"></td>
                                                            <td height="30">Billing Type</td>
                                                            <td><input name="billingType" id="billingType" type="text" class="textbox" onClick="ressetDate(this);"></td>
                                                        </tr>-->
                </table>
                

            <%--<c:if test = "${ds != null}" >--%>
            <table class="table table-info mb30 table-hover sortable" id="table" style="width:100%">
                <thead>
                    <tr height="40">
                        <th>S.No</th>
                <th>Route Code-Route Name</th>
                <%int a = 0;%>
                <c:if test = "${dateList != null}" >
                    <c:forEach items="${dateList}" var="customer">
                        <%a++;%>
                        <input type="hidden" name="scheduledate<%=a%>" id="scheduledate<%=a%>" value="<c:out value="${customer.dateName}" />"/>
                        <input type="hidden" name="scheduledate" id="scheduledate" value="<c:out value="${customer.dateName}" />"/>
                        <th><c:out value="${customer.dateName}"/></th>
                        </c:forEach>
                    </c:if>   
                </tr>
                </thead>
                <tbody>
                    <% int index = 0,sno = 1;%>
                    <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                    %>
                    <c:if test = "${SecondaryContractRouteList != null}" >
                        <c:forEach items="${SecondaryContractRouteList}" var="scrl">
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><input type="hidden" name="secRouteId" id="secRouteId" value="<c:out value="${scrl.secondaryRouteId}"/>" /><c:out value="${scrl.secondaryRouteName}"/>"</td>
                                    <c:forEach items="${dateList}" var="customer">
                                    <td align="left" class="text2"><input type="checkbox" name="checkedValue" id="checkedValue" value="<c:out value="${customer.date}" />~<c:out value="${scrl.secondaryRouteId}" />"/></td>
                                    </c:forEach>
                            </tr>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <table width="100%" align="center" border="0" id="table">
                <tr>
                    <td colspan="4" align="center">
                        <input type="hidden" name="scheduleDatesTrue" id="scheduleDatesTrue" value=""/>
                        <input type="hidden" name="scheduleDatesFalse" id="scheduleDatesFalse" value=""/>
                        <input type="button" value="Save" class="btn btn-success" name="save" onClick="submitPage('Save')">
                        <input type="button" value="nextWeek" class="btn btn-success" name="nextWeek"  onClick="submitPage('nextWeek')">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>