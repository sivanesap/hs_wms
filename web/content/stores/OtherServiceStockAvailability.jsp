
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
 <%@ page import="ets.domain.mrs.business.MrsTO" %> 
  </head>
  <body>
       <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->

<!-- title table -->
<br>

<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
  <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">

<tr>
<td class="contentsub" height="30"><div class="contentsub">MFR Item Code</div></td>
<td class="contentsub" height="30"><div class="contentsub">PAPL Code</div></td>
<td class="contentsub" height="30">Item Name</td>
<td class="contentsub" height="30"><div class="contentsub">Service Point</div></td>
<td class="contentsub" height="30"><div class="contentsub">Stk Availbty</div></td>
<td class="contentsub" height="30"><div class="contentsub">RC Qty</div></td>
</tr>
<% int index=0; %>
<c:if test = "${servicePointItemLists != null}" >
<c:forEach items="${servicePointItemLists}" var="item"> 
<%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td class="<%=classText %>" height="30"><c:out value="${item.mrsItemMfrCode}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${item.mrsPaplCode}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${item.mrsItemName}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${item.mrsCompanyName}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${item.newStock}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${item.rcStock}"/></td>
</tr>
 <%
   index++;
 %>
</c:forEach>
 </c:if> 

</table>
  </body>
</html>
