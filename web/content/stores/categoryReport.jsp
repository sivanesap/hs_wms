
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">
       function submitPage(){
           var chek=validation();
           if(chek=='true'){
               
               if(document.salesBillTrend.itemType.value=='1')
                   {
                       
                document.salesBillTrend.action='/throttle/categoryReport.do';
                document.salesBillTrend.submit();
                }
                else
                    {
                        
                        document.salesBillTrend.action='/throttle/categoryNewReport.do';
                document.salesBillTrend.submit();
                        }
           }
       }
       function validation(){
               if(textValidation(document.salesBillTrend.fromDate,'From Date')){
                   document.salesBillTrend.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.salesBillTrend.toDate,'To Date')){
                   document.salesBillTrend.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           var b='<%=request.getAttribute("itemType")%>';
           if(a!='null'){
               
                document.salesBillTrend.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.salesBillTrend.toDate.value='<%=request.getAttribute("toDate")%>';
           }
           if(b!='null'){
               document.salesBillTrend.itemType.value=b;
               }
       }
    </script>
    <body onload="setValues();">
        <form  name="salesBillTrend" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <br>
           
            <table align="center" width="600" class="border" border="0" cellspacing="0" cellpadding="0">
                <tr >
                    <Td colspan="4" class="contenthead">Categorywise Report</Td>
                </tr>
                
                <tr>
                    <td class="text1"><font color="red">*</font>From Date</td>
                    <td class="text1"><input name="fromDate" class="textbox" type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.fromDate,'dd-mm-yyyy',this)"/></td>
                   <td class="text1" height="25"><font color="red">*</font>To Date</td>
                    <td class="text1"><input name="toDate" class="textbox" type="text" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.toDate,'dd-mm-yyyy',this)"/></td>
               </tr>
               <tr>
                    <td class="text2"><font color="red">*</font>Item Type</td>
                    <td class="text2">
                        <select name="itemType" style="width:120px;">
                            <option value="1">RC</option>
                            <option value="2">New</option>
                    </select></td>
               </tr>
            </table>
            
            <br><br>
            <center><input type="button" class="button" value="Search" onclick="submitPage();" />          
                
            </center>
            <br>
                <table width='50%'  border='0' cellpadding='5' cellspacing='0' class='repcontain' align="center">
            <c:if test = "${categoryRcReport != null}" >
                <tr>
                    <td class="contentsub">S.No</td>
                    <td class="contentsub">Category Name</td>
                    <td class="contentsub">Stock Worth</td>
                </tr>
                <%
					int index = 1 ;
                    %>
                <c:forEach items="${categoryRcReport}" var="pd">
                          <%

            String classText = "";

            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                    <tr>  
                       <td class="<%=classText %>"><%=index%></td>
                    <td class="<%=classText %>"><c:out value="${pd.categoryName}"/></td>
                    <td class="<%=classText %>"><c:out value="${pd.amount}"/></td>
                    </tr>
                       <%
            index++;
                        %>
                </c:forEach> 
            </c:if>
        </table>
            
            
        </form>
    </body>
</html>
