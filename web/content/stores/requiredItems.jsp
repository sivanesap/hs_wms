<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <title>MRSList</title>
    </head>

        
        
        <script>                         
function submitPag(val){
    if(validate()=='0'){        
        return;
    }if(val=='purchaseOrder'){
        document.mpr.purchaseType.value="1012"
        document.mpr.action = '/throttle/generateMprForReqItems.do'
        document.mpr.submit();
    }else if(val == 'localPurchase'){
        document.mpr.purchaseType.value="1011"
        document.mpr.action = '/throttle/generateMprForReqItems.do'
        document.mpr.submit();    
    } 
}

function validate()
{
var selectedMrs = document.getElementsByName("selectedIndex");
var reqQtys = document.getElementsByName("reqQtys");
var vendorIds = document.getElementsByName("vendorIds");
var counter=0;
    for(var i=0;i<selectedMrs.length;i++){
        if(selectedMrs[i].checked == 1){
            counter++;
            if( parseFloat(reqQtys[i].value)== 0  ||  isFloat(reqQtys[i].value) ){
                alert("Please Enter Required Quantity");
                reqQtys[i].select();
                reqQtys[i].focus();
                return '0'
            }
            if( vendorIds[i].value=='0' ){
                alert("Please Select Vendor");
                vendorIds[i].focus();                
                return '0'
            }            
        }       
    }
    if(counter==0){
        alert("Please Select Atleast one item");
        return '0';
    }     
    return counter;
}
      
function selectBox(val){
    var checkBoxs = document.getElementsByName("selectedIndex");
    checkBoxs[val].checked=1; 
}      


function submitPage(val){
       
        if(val=='GoTo'){
            var temp=document.mpr.GoTo.value;                 
            if(temp!='null'){
            document.mpr.pageNo.value=temp;
            }
        }        
        document.mpr.button.value=val;        
        document.mpr.action = '/throttle/handleSearchRequiredItems.do'
        document.mpr.submit();        
}

function setValues()
{
    if('<%= request.getAttribute("mfrCode") %>' !='null'){
        document.mpr.mfrCode.value = '<%= request.getAttribute("mfrCode") %>';
    }
    if('<%= request.getAttribute("paplCode") %>' !='null'){
        document.mpr.paplCode.value = '<%= request.getAttribute("paplCode") %>';
    }
    if('<%= request.getAttribute("categoryId") %>' !='null'){
        document.mpr.categoryId.value = '<%= request.getAttribute("categoryId") %>';
    }
    if('<%= request.getAttribute("searchAll") %>' !='null'){
        document.mpr.searchAll.value = '<%= request.getAttribute("searchAll") %>';
    }
}    





        </script>
        
        
        
        
    <body onLoad="setValues();" >        
        <form name="mpr" method="post">                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
             <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <br>
                    <%
				    int pageIndex = (Integer)request.getAttribute("pageNo");					
					int index1 = ((pageIndex-1)*10)+1 ;
                    %>  


            <table  border="0" class="border" align="center" style="margin-bottom:50px;" width="700" cellpadding="0" cellspacing="0" id="bg">
            <tr>
            <td colspan="6" height="30" class="contenthead"><div class="contenthead">Search Parts</div></td>
          
            </tr>
            <tr>
            <td class="text2" height="30"><font color="red">*</font>Item Code</td>
            <td class="text2" height="30"><input name="mfrCode" type="text" class="textbox" value="" ></td>
            <td class="text2" height="30"><font color="red">*</font>PAPL Code</td>
            <td class="text2" height="30"><input name="paplCode" type="text" class="textbox" value=""></td>
            
            
            <tr> 
            <td class="text1" height="30"><font color="red">*</font>Category</td>
            <td class="text1"  >
                <select class="textbox" name="categoryId" style="width:125px;" >
                <option value="0">---Select---</option>
                <c:if test = "${CategoryList != null}" >
                <c:forEach items="${CategoryList}" var="Dept"> 
                <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                </c:forEach >
                </c:if>   	
                </select>
            </td>
            <td class="text1" height="30"><font color="red">*</font>Search Type</td>
            <td class="text1" height="30">
                <select class="textbox" name="searchAll" style="width:125px;" >
                <option value="">---Select---</option> 	
                <option value="Y">All</option> 	
                <option value="N">Required Items</option> 	
                </select>                
            </td>            
            </tr>
            <tr> <td>&nbsp;  </td> </tr>
            <tr>                
                <td class="text2" colspan="4" align="center" height="30"><input type="button" class="button" name="Search" value="Search" onClick="submitPage(this.value)" >  </td>
            </tr>    
            </table>


           


                <c:if test = "${requiredItemsList != null}" >   
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="776" id="bg" class="border">              
                <%
            String classText = "";
            int oddEven = 0;
            int index = 0;
                %>

             
                        <tr>
                            <td width="25" height="30" class="contenthead"><b>Sno</b></td>						
                            <td width="69" height="30" class="contenthead"><b>Mfr Code</b></td>
                            <td width="71" height="30" class="contenthead"><b>Papl Code</b></td>                       
                            <td width="144" height="30" class="contenthead"><b>Item Name</b></td>
                            <td width="31" height="30" class="contenthead"><b>Uom</b></td>
                            <td width="60" height="30" class="contenthead"><b>Reorder Level</b> </td>                                                                                                                                               
                            <td width="58" height="30" class="contenthead"><b>Stock Balance</b> </td>                                                                                                                                                                                                                                                                                                                
                            <td width="69" height="30" class="contenthead"><b>Po Raised Qty</b> </td>                                                                                                                                               
                            <td width="125" height="30" class="contenthead"><b>Vendor</b> </td>                                                                                                                                               
                            <td width="51" height="30" class="contenthead"><b>Select</b></td>
                        </tr>                
                    <c:forEach items="${requiredItemsList}" var="item"> 
                        <%

            oddEven = index1 % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><%= index1 %></td>						
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.roLevel}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.stockBalance}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${item.poRaisedQty}"/> </td>
                            <td class="<%= classText %>" height="30"> <select name="vendorIds" class="textbox" style="width:125px;">
                                    <option value='0'> --Select -- </option>
                            <c:if test = "${vendorItemList != null}" >       
                                <c:forEach items="${vendorItemList}" var="vend">                                      
                                        <c:if test="${vend.itemId==item.itemId}" >
                                            <option value='<c:out value="${vend.vendorId}" />' > <c:out value="${vend.vendorName}" /> </option>                                    
                                        </c:if>                            
                                </c:forEach>      
                            </c:if>                                
                            </select>                    
                            </td>                             
                            <td class="<%=classText %>" height="30"> <input type="checkbox" class="textbox" name="selectedIndex" value="<%= index %>" > <input type="hidden" name="itemIds" value='<c:out value="${item.itemId}"/>' >  </td>
                            
                        </tr>
                        <%
            index++;
            index1++;
                        %>
                    </c:forEach>
                  
            </table>

            <br>           
                <input type="hidden" name="purchaseType" value="" >          
            <br>               
</c:if>     
     
<br>
 <%@ include file="/content/common/pagination.jsp"%>      
<br>

        </form>
    </body>
</html>
