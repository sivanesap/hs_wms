
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">                

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>   
        <script type="text/javascript" language="javascript" src="/throttle/js/ajaxFunction.js"></script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>       
        <title>Material Issue</title>
    </head>



    <script>


        var stkAvailability = 0;
        var httpReq;

        function stkValidate(){
            var temp = (document.mrs.priceIds.value).split('~');
            document.mrs.priceId.value = temp[0];
            stkAvailability = temp[1];
            document.mrs.amount.value=parseFloat(temp[2]);
            document.mrs.amount.value=parseFloat(document.mrs.issueQty.value)*parseFloat(temp[2]);
            document.mrs.nettAmount.value=document.mrs.amount.value;
    
        }




        function setOptions5(text,variab){
            variab.options.length=0;
            option0 = new Option("--select--",'0');
            variab.options[0] = option0;
            if(text != ""){
                var splt = text.split('~');
                var temp1;
                var make1;
                var make2;
                variab.options[0] = option0;
                for(var i=0;i<splt.length;i++){
                    temp1 = splt[i].split('-');
                    make1 ='Rs.'+temp1[1]+'('+temp1[2]+'/'+temp1[3]+')';
                    make2 =   temp1[0]+'~'+temp1[2]+'~'+temp1[1];
                    //alert('1-'+temp1[val2]);
                    //alert('2-'+temp1[val1]);
                    option0 = new Option(make1,make2);
                    variab.options[i+1] = option0;
                }
                correctNettAmount();
            }
        }

    


        function setTyreOptions(text,variab){
            variab.options.length=0;
            option0 = new Option("--select--",'0');
            variab.options[0] = option0;
            if(text != ""){
                var splt = text.split('~');
                var temp1;
                var make1;
                var make2;
                variab.options[0] = option0;
                for(var i=0;i<splt.length;i++){
                    temp1 = splt[i].split('-');
                   // make1 =   temp1[1]+'(Rs. '+temp1[3]+'/'+temp1[4]+')';
                    make1 =   temp1[1]+'(Rs. '+temp1[3]+'/'+temp1[4]+'/PO.NO:'+temp1[5]+')';
                    make2 =   temp1[0]+'~'+temp1[2]+'~'+temp1[3];
                    //alert('1-'+temp1[val2]);
                    //alert('2-'+temp1[val1]);
                    option0 = new Option(make1,make2);
                    variab.options[i+1] = option0;
        
                }
            }
    }

     function setRtTyrePriceOptions(text,variab){
        variab.options.length=0;
        option0 = new Option("--select--",'0');
        variab.options[0] = option0;
        if(text != ""){
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for(var i=0;i<splt.length;i++){
                //alert('Have RT WO'+splt[i]);
                temp1 = splt[i].split('-');
                make1 =   temp1[2]+'(Rs. '+temp1[0]+'/WO NO'+temp1[3]+')';
                make2 =   temp1[1]+"~"+temp1[0]+"~"+temp1[3];
                //alert(make2);
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1,make2);
                variab.options[i+1] = option0;
            }
        }
    }


    function setRcItemPriceOptions(text,variab){
        variab.options.length=0;
        option0 = new Option("--select--",'0');
        variab.options[0] = option0;
        if(text != ""){
            var splt = text.split('~');
            var temp1;
            var make1;
            var make2;
            variab.options[0] = option0;
            for(var i=0;i<splt.length;i++){
                temp1 = splt[i].split('-');
                //alert("WO"+splt[i]);
                make1 =   temp1[1]+'(Rs. '+temp1[0]+'/WO NO'+temp1[2]+')';
                make2 =   temp1[1]+"~"+temp1[0]+"~"+temp1[2];
                //alert('1-'+temp1[val2]);
                //alert('2-'+temp1[val1]);
                option0 = new Option(make1,make2);
                variab.options[i+1] = option0;
            }
        }
    }


    function onTyreSelect()
    {
        if(document.mrs.itemType.value=='1011'){
            var splt = (document.mrs.rcItemIds.value).split('~');
            //alert(splt);
            document.mrs.priceId.value = splt[1];
            document.mrs.tyreId.value = splt[0];
            document.mrs.rcPriceWoId.value = splt[2];
            //alert("rcPriceWoId"+document.mrs.rcPriceWoId.value);
            
        }
    }

    function onRcItemSelect()
    {
        var splt;
        if(document.mrs.itemType.value !='1011' &&  document.mrs.categoryId.value == '1011'){
             splt = (document.mrs.rcItemIds.value).split('~');
            document.mrs.rcPriceWoId.value = splt[2];
            document.mrs.price.value = splt[1];
            document.mrs.tyreId.value = splt[0];
        }else if(document.mrs.itemType.value !='1011' &&  document.mrs.categoryId.value != '1011'){
             splt = (document.mrs.rcItemIds.value).split('~');
            document.mrs.rcPriceWoId.value = splt[2];
            document.mrs.price.value = splt[1];
            document.mrs.rcItemId.value = splt[0];
        }
            
    }


    function getPriceAvailable(indx){
        <%--alert(document.mrs.counterFlag.value);--%>
            var itemType = document.getElementsByName("itemType");
            var mrsItemId =  document.mrs.itemId.value;
            var categoryId =  document.mrs.categoryId.value;
            //alert(categoryId);
            var url;
            var item;
            var counter = 0;
            var caseType = '';

            //document.mrs.issueQty.readOnly=0;
            if(itemType[indx].value=='1011' && categoryId != '1011'  ){
                item = 'itemType='+itemType[indx].value+"&mrsItemId="+mrsItemId;
                url = '/throttle/itemPrice.do?'+item;
                caseType = 'case1';
                counter++;
            }else if(itemType[indx].value=='1011' && categoryId == '1011'  ){
                item = "mrsItemId="+mrsItemId;
                url = '/throttle/handleNewTyresStock.do?'+item;
                caseType = 'case2';
                counter++;
            }else if(itemType[indx].value =='1012'  ){
                item = "mrsItemId="+mrsItemId+"&categoryId="+categoryId;
                url = '/throttle/getRcPriceList.do?'+item;
                caseType = 'case3';
                counter++;
            }
            if(counter > 0)  {
                if (window.ActiveXObject){
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest){
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function() { processPriceAjax(caseType); } ;
                httpReq.send(null);
            }
        }


   
        function processPriceAjax(caseType)
        {
            if (httpReq.readyState == 4)
            {
                if(httpReq.status == 200)
                {
                    var temp = httpReq.responseText.valueOf();
                    if(caseType=='case1'  ){
                        setOptions5(temp,document.mrs.priceIds);
                        document.mrs.issueQty.value="1";
                        document.mrs.issueQty.readOnly=0;
                        document.mrs.priceIds.disabled = false;
                        document.mrs.rcItemIds.disabled = true;
                    }
                    else if(caseType=='case2'  ){
                        //alert(temp);
                        setTyreOptions(temp,document.mrs.rcItemIds);
                        document.mrs.rcItemIds.disabled = false;
                        document.mrs.priceIds.disabled = true;
                        document.mrs.issueQty.value="1";
                        document.mrs.issueQty.readOnly=1;
                    }
                    else if(caseType=='case3' && document.mrs.categoryId.value =='1011'  ){
                        //alert(caseType);
                        setRtTyrePriceOptions(temp,document.mrs.rcItemIds);
                        document.mrs.rcItemIds.disabled = false;
                        document.mrs.priceIds.disabled = true;
                        document.mrs.issueQty.value="1";
                        document.mrs.issueQty.readOnly=1;
                    }
                    else if(caseType=='case3' && document.mrs.categoryId.value !='1011'  ){
                        setRcItemPriceOptions(temp,document.mrs.rcItemIds);
                        document.mrs.rcItemIds.disabled = false;
                        document.mrs.priceIds.disabled = true;
                        document.mrs.issueQty.value="1";
                        document.mrs.issueQty.readOnly=1;
                    }
                }
                else
                {
                    alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                }
            }
        }



        function validateStk()
        {
            var test='fail';
            if(document.mrs.selectedItem.checked != true) {
                test = "pass";
                return test;
            }
            if(document.mrs.itemType.value=='1011'  &&  document.mrs.categoryId.value!='1011'){
                if( isFloat(document.mrs.issueQty.value) ) {
                    alert('Please Enter Valid data');
                    document.mrs.issueQty.focus();
                    document.mrs.issueQty.select();
                    return test;
                }else if( parseFloat(stkAvailability) < parseFloat(document.mrs.issueQty.value) ) {
                    alert('Requested Stock is Not Available in this Price');
                    document.mrs.issueQty.focus();
                    return test;
                }else{
                    test = "pass";
                    return test;
                }
            }else{
                test = "pass";
                return test;
            }
        }


        function submitValidation()
        {
            var selectedIndx = document.getElementsByName("selectedItem");
            var retSelectedIndx = document.getElementsByName("retselectedIndex");
            var fault = document.mrs.faultQty.value;
            var index=0;
    
            if(selectedIndx[0].checked==true){
                return "pass";
            }
            for(var i=0;i<retSelectedIndx.length;i++){
                if(retSelectedIndx[i].checked==true){
                    index++;
                }
            }
            if(index != 0){
                return "pass";
            }
            if(!isDigit(fault)){
                if(fault != '0'){
                    return "pass";
                }else{
                    alert('Quantity Should not be zero');
                    document.mrs.faultQty.focus();
                    document.mrs.faultQty.select();
                    return "fail";
                }
            }
            alert("Please Select Any Choice");
            return "fail";
        }


        function submitPage()
        {
            var seletcedItems = document.getElementsByName('retselectedIndex');
            if(submitValidation()=='fail'  ){
                return;
            }
            //if(document.mrs.selectedItem.checked==true){
            if(validateStk()=='fail'  ){
                return;
            }
            //}
            //if(seletcedItems[0].checked == true){
            //  alert('k');
            if(validateIssue()=='fail'  ){
                return;
            }
            //}
            <%--var counterFlag=document.mrs.counterFlag.value;
            var counterId=document.mrs.counterId.value;
            alert("Oh God"+counterFlag);--%>
            //alert("positionId"+document.mrs.positionId.value);
            if(document.mrs.itemType.value=='1011'){
                document.mrs.actionType.value = '1011';
                document.mrs.action = '/throttle/handleMrsTyresIssue.do';
                //alert("am here 1");
                document.mrs.submit();
            }else if(document.mrs.itemType.value =='1012'){
                document.mrs.action = '/throttle/handleMrsRcItemsIssue.do';
                //alert("am here 2");
                document.mrs.submit();
            }else{
                document.mrs.action = '/throttle/handleMrsTyresIssue.do';
                //alert("am here 3");
                document.mrs.submit();
            }
        }
        function changeTotal()
        {
            if(document.mrs.issueQty.value!=""){
                var total=parseFloat(document.mrs.issueQty.value)*parseFloat(document.mrs.amount.value);
                document.mrs.total.value=total;
                document.mrs.nettAmount.value=total;
            }
        }
        function correctNett() {
            if(isFloat(document.mrs.discount.value) ){
                alert("Please Enter Discount");
                document.mrs.discount.focus();
                document.mrs.discount.select();
                return;
            }
            if (parseFloat(document.mrs.discount.value) > parseFloat(document.mrs.nettAmount.value)) {
                alert("Discount value cannot be greater than bill value");
                document.mrs.discount.value="";
                document.mrs.discount.focus();
            }else {
                var result = parseFloat(document.mrs.nettAmount.value) - (parseFloat(document.mrs.nettAmount.value) * (parseFloat(document.mrs.discount.value)/100));
                document.mrs.nettAmount.value = result;
            }
        }
        function correctNettAmount() {
            var flag=1;
            var tax=0;
            var hikePercentage=0;
            var discount=0;
            var amount= document.mrs.amount.value;
            var issueQty= document.mrs.issueQty.value
            //alert("In Correct Nett Amount");
            if(!isEmpty(document.mrs.tax.value) ){
                tax=parseFloat(document.mrs.tax.value);
            }

            if(!isEmpty(document.mrs.hikePercentage.value))
            {
                hikePercentage=parseFloat(document.mrs.hikePercentage.value);
            }
            if(!isEmpty(document.mrs.discount.value))
            {
                    
                discount=parseFloat(document.mrs.discount.value);
                
            }
        <%--if(isFloat(document.mrs.tax.value) ){
            flag=0;
            alert("Please Enter Tax");
            document.mrs.tax.focus();
            document.mrs.tax.select();
            return;
        }
        else if(isFloat(document.mrs.hikePercentage.value))
            {
                flag=0;
            alert("Please Enter Hike Percentage");
            document.mrs.hikePercentage.focus();
            document.mrs.hikePercentage.select();
            return;
            }
        else if(isFloat(document.mrs.discount.value))
            {
                flag=0;
            alert("Please Enter Discount");
            document.mrs.discount.focus();
            document.mrs.discount.select();
            return;
            }--%>
                        if(flag==1)
                        {

                            //alert("iss"+issueQty+"tax"+tax+"hike"+hikePercentage+"disc"+discount)
                            var taxAmount = parseFloat(amount) + (parseFloat(amount) * (parseFloat(tax)/100));
                            var hikeAmount = parseFloat(taxAmount) +(parseFloat(taxAmount) * (parseFloat(hikePercentage)/100));
                            var hikeTotal= parseFloat(issueQty)*parseFloat(hikeAmount);
                            var discountTotal = parseFloat(hikeTotal) - (parseFloat(hikeTotal) * (parseFloat(discount)/100));
                            document.mrs.total.value = parseFloat(discountTotal);
                            document.mrs.nettAmount.value = parseFloat(discountTotal).toFixed(2);
                        }
                    }



                    function validateIssue()
                    {

                        var retIssuedQty1 =0;
                        var seletcedItems = document.getElementsByName('retselectedIndex');
                        var retIssuedQty = document.getElementsByName("retIssuedQty");
                        var retReturnedQty = document.getElementsByName("retReturnedQty");
                        var issueQty = document.mrs.issueQty.value;
                        var approvedQty = document.mrs.approvedQty.value;
                        var totalIssued = 0;
                        if(document.mrs.selectedItem.checked==true){
                            issueQty = document.mrs.issueQty.value
                        }else{
                            issueQty = 0;
                        }
    
                        for(var i=0;i<retIssuedQty.length;i++){
                            retIssuedQty1 = parseFloat(retIssuedQty1) + parseFloat(retIssuedQty[i].value );
                        }
                        totalIssued = parseFloat(retIssuedQty1) + parseFloat(issueQty)
    
                        if(document.mrs.selectedItem.checked == true) {
                            if(parseFloat(approvedQty) < parseFloat(totalIssued)  ){
                                alert('Items Cannot be Issued more than Approved Quantity');
                                return "fail";
                            }
                        }
    
                        if(seletcedItems[0].checked == true){
                            if(retReturnedQty[0].value != ''){
                                if(isFloat(retReturnedQty[0].value) ){
                                    alert("Please Enter Valid Return Quantity");
                                    retReturnedQty[0].focus();
                                    retReturnedQty[0].select();
                                    return "fail";
                                }
                                if(parseFloat(retIssuedQty[0].value) < parseFloat(retReturnedQty[0].value) ){
                                    alert("Returned Quantity should not exceeds Issued Quantity");
                                    retReturnedQty[0].focus();
                                    retReturnedQty[0].select();
                                    return "fail";
                                }
                            }else{
                                alert("Please Enter Return Quantity");
                                retReturnedQty[0].focus();
                                retReturnedQty[0].select();
                                return "fail";
                            }
                        }
                        return "pass";
                    }
            
    </script>

    <body>

        <form name="mrs" method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->

            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>                              
            <br> 

            <% String classText = "";
                        int index = 0;%>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                <tr>
                    <td colspan="10" align="center" class="text2" height="30"><strong>Item Name : <%= request.getAttribute("itemName")%></strong> </td>
                <input type="hidden" name="itemId" value="<%= request.getAttribute("itemId")%>" >
                <input type="hidden" name="mrsId" value="<%= request.getAttribute("mrsId")%>" >
                <input type="hidden" name="categoryId" value="<%= request.getAttribute("categoryId")%>" >
                <input type="hidden" name="positionId" value="<%= request.getAttribute("positionId")%>" >
                <input type="hidden" name="rcStatus" value="<%= request.getAttribute("rcStatus")%>" >
                <input type="hidden" name="approvedQty" value="<%= request.getAttribute("approvedQty")%>" >
                <input type="hidden" name="rcWorkId" value="<%= request.getAttribute("rcWorkId")%>" >
                <input type="hidden" name="rcItemsId" value="<%= request.getAttribute("rcItemsId")%>" >
                <input type="hidden" name="mrsItemId" value="<%= request.getAttribute("mrsItemId")%>" >
                <%--<input type="hidden" name="counterFlag" value="<%= request.getAttribute("counterFlag")%>" > >
                <input type="hidden" name="counterId" value="<%= request.getAttribute("counterId")%>" > >--%>
                <input type="hidden" name="actionType" value="" >
                <input type="hidden" name="priceId" value="" >
                <input type="hidden" name="tyreId" value="" >
                <input type="hidden" name="price" value="" >
                <input type="hidden" name="rcPriceWoId" value="" >
                <input type="hidden" name="rcItemId" value="" >
                <input type="hidden" name="amount" value="" >
                <input type="hidden" name="total" value="" >
                </tr>  
                <tr>
                    <td width="84"  class="contentsub">Item Type </td>
                    <td width="84"  class="contentsub">RC/RT/TyreNo ItemsList</td>
                    <td width="84"  class="contentsub">Price</td>
                    <td width="82" height="30"  class="contentsub">Issued Qty</td>
                    <%//if (request.getAttribute("counterFlag").equals("Y")) {
                    %>
                    <%--<td width="82" height="30"  class="contentsub">Tax</td>
                    <td width="82" height="30"  class="contentsub">Hike %</td>
                    <td width="82" height="30"  class="contentsub">Discount</td>
                    <td width="82" height="30"  class="contentsub">Nett Amount</td>--%>
                    <%//}%>
                    <td width="82" height="30"  class="contentsub">select</td>
                </tr>


                <tr>
                    <td width="84"  class="text1">
                        <select class="textbox" name="itemType" onChange="getPriceAvailable('0');" >
                            <option>-Select-</option>
                            <option value="1011">New</option>
                            <% if (request.getAttribute("rcStatus").equals("Y")) {%>
                            <option value="1012">RC</option>
                            <% }%>
                        </select>
                    </td>
                    <td width="84"  class="text1">
                        <select class="textbox" name="rcItemIds" onChange="onTyreSelect();onRcItemSelect();"  style="width:200px">
                            <option value='0'>-Select-</option>
                        </select>
                    </td>
                    <td width="84"  class="text1">
                        <select class="textbox" name="priceIds" onChange="stkValidate();"  style="width:200px" >
                            <option value='0'>-Select-</option>
                        </select>
                    </td>
                    <% if (request.getAttribute("categoryId").equals("1011")) {%>
                    <td width="82" height="30" class="text1"> <input type="text" name="issueQty" value="1" readonly class="textbox" > </td>
                        <% } else {%>
                    <td width="82" height="30" class="text1"> <input type="text" name="issueQty" value="" class="textbox" onchange="changeTotal();" > </td>
                        <% }
                                    //if (request.getAttribute("counterFlag").equals("Y")) {
                        %>

                    <%--<td width="82" height="30" class="text1"> <select name="tax" class="textbox" onchange="correctNettAmount();">
                            <option value="12.5">12.5</option>
                            <option value="4.00">4.00</option>
                            <option value="0.50">0.50</option>
                            <option value="0.00">0.00</option>
                        </select>
                    <td width="82" height="30" class="text1"> <input type="text" name="hikePercentage" value="" class="textbox" onchange="correctNettAmount();" > </td>
                    <td width="82" height="30" class="text1"> <input type="text" name="discount" value="" class="textbox" onchange="correctNettAmount();" > </td>
                    <td width="82" height="30" class="text1"> <input type="text" name="nettAmount" value="" class="textbox" > </td>--%>
                        <%//}%>
                    <td width="82" height="30" class="text1"> <input type="checkbox" name="selectedItem" value="1" class="textbox" > </td>
                </tr>




            </table>
            <br>
            <br>

            <c:if test = "${itemList != null}" >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                    <tr>
                        <td colspan="9" align="center" class="text2" height="30"><strong>Returnables</strong></td>
                    </tr>
                    <tr>
                        <td width="68" height="30"  class="contentsub">Issued Qty</td>
                        <td width="84"  class="contentsub">Item Type </td>                        
                        <td width="84" class="contentsub">RT Item</td>
                        <td width="84"  class="contentsub">Price</td>
                        <td width="84"  class="contentsub">Wo No</td>
                        <td width="82" height="30"  class="contentsub">Received Qty</td>                                                                                             
                        <td width="82" height="30"  class="contentsub">select</td>                                                                                             
                    </tr>
                    <c:forEach items="${itemList}" var="item">
                        <% if ((index % 2) == 0) {
                                        classText = "text1";
                                    } else {
                                        classText = "text2";
                                    }
                        %>
                        <input type="hidden" name="retIssueId" value="<c:out value="${item.issueId}"/>" >
                        <input type="hidden" name="retPriceId" value="<c:out value="${item.priceId}"/>" >                        
                        <input type="hidden" name="retTyreId" value="<c:out value="${item.tyreId}"/>" >                        
                        <input type="hidden" name="retIssuedQty" value="<c:out value="${item.totalIssued}"/>" >                        
                        <tr>
                            <td width="68" height="30"  class="<%= classText%>"> <c:out value="${item.totalIssued}"/> </td>
                            <td width="84"  class="<%= classText%>"> <input type="text" class="textbox"  size='5' readonly name="retItemType" value="<c:out value="${item.itemType}"/>" > </td>
                            <td width="84" class="<%= classText%>">
                                <input type="text" class="textbox"  readonly name="retRcItemId" value="<c:out value="${item.rcItemId}"/>" >
                            </td>
                            <td width="84"  class="<%= classText%>" > <input type="text" class="textbox"  readonly name="retPrice" value="<c:out value="${item.price}"/>" >   </td>
                            <c:choose>
                                    <c:when test="${item.itemType=='RC'}">
                            <td width="84"  class="<%= classText%>" > <input type="text" class="textbox"  readonly name="retRcPriceWoId" value="<c:out value="${item.rcPriceWoId}"/>" >   </td>
                            </c:when>
                            <c:otherwise>
                            <td width="84"  class="<%= classText%>" > &nbsp;   </td>
                            </c:otherwise>
                            </c:choose>
                                <c:choose>
                                    <c:when test="${item.itemType=='NEW'}">
                                        <% if (request.getAttribute("categoryId").equals("1011")) {%>
                                    <td width="82" height="30"  class="<%= classText%>" ><input type="text" class="textbox" size='5' name="retReturnedQty" value="1" readonly > </td>
                                        <% } else {%>
                                    <td width="82" height="30"  class="<%= classText%>" ><input type="text" size='5'  class="textbox" name="retReturnedQty" value="" > </td>
                                        <% }%>
                                    </c:when>
                                    <c:otherwise>
                                    <td width="82" height="30" class="<%= classText%>" ><input type="text" class="textbox"  readonly size='5' name="retReturnedQty" value="1" > </td>
                                    </c:otherwise>
                                </c:choose>
                            <td width="82" height="30"  class="<%= classText%>" ><input type="checkbox" class="textbox"  name="retselectedIndex" value="<%= index%>" size='5' > </td>
                        </tr>   
                        <% index++;%>
                    </c:forEach>
                </table>
            </c:if>


            <c:if test = "${itemList == null}" >
                <input type="hidden" name="retIssueId" value="0" >
                <input type="hidden" name="retPriceId" value="0" >
                <input type="hidden" name="retTyreId" value="0" >
                <input type="hidden" name="retIssuedQty" value="0" >


                <input type="hidden"  size='5'  name="retItemType" value="0" >
                <input type="hidden"   name="retRcItemId" value="0" >
                <input type="hidden"    name="retPrice" value="0"  >
                <input type="hidden"  size='5' name="retReturnedQty" value="0"  >
                <div style="visibility:hidden;" ><input type="checkbox" class="textbox"  name="retselectedIndex" value="" size='5' ></div>
                </c:if>


            <br>

            <table align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">                              
                <tr>
                    <td colspan="2" align="center" class="contenthead" height="30" ><div class="contenthead">Fault Items</div></td>
                </tr>
                <tr>
                    <td width="68" height="30" align="center" class="text2">Fault Items</td>
                    <td width="68" height="30" align="center" class="text2"><input type="text" class="textbox"  name="faultQty" value="" ></td>
                </tr>

            </table> 
            <br>
            <input type="hidden" name="status" value="" >
            <br>
            <center>
                <input type="button" class="button" name="Issue" value="Issue/Receive" onClick="submitPage();" > &nbsp;
            </center>                
        </form>
    </body>
</html>
