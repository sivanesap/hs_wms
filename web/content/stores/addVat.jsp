
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="/throttle/js/validate.js"></script>   
 <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
</head>
<script>
    function submitpage()
    {
       if(floatValidation(document.dept.vat,"VAT")){
           return;
       }
       if(textValidation(document.dept.effectiveDate,"effectiveDate")){
           return;
       }
       if(textValidation(document.dept.desc,"Description")){
           return;
       }
       document.dept.action= "/throttle/handleAddVat.do";
       document.dept.submit();
    }
</script>
<body>
<form name="dept"  method="post">
    <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
<td colspan="2" class="contentsub" height="30"><div class="contentsub">ADD VAT(%)</div></td>
</tr>
<tr>
<td class="text1" height="30"><font color=red>*</font>VAT Rate(%)</td>
<td class="text1" height="30"><input name="vat" maxlength="10" type="text" class="textbox" value=""></td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font>Effective Date</td>
<td class="text2" height="30">
    <input name="effectiveDate" type="text" class="datepicker"  readonly="readonly" id="effectiveDate" value=""/>
</td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font>Description</td>
<td class="text2" height="30"><textarea class="textbox" name="desc"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" onclick="submitpage();">

</center>
</form>
</body>
</html>
