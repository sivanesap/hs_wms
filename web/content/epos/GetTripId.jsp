<%
    Connection conn = null;
    int status=0;
    String identityNo = request.getParameter("IdentityNo");    
    try{    
        String applicationPath = application.getRealPath("WEB-INF/classes");
        applicationPath = applicationPath.replace("\\", "/");
        java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
        java.util.Properties props = new java.util.Properties();
        props.load(fis);

        String driverClass = props.getProperty("jdbc.driverClassName");
        String jdbcUrl = props.getProperty("jdbc.url");
        String userName = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        Class.forName(driverClass);
        conn = DriverManager.getConnection(jdbcUrl, userName, password);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT max(TripId) as TripId FROM ra_trip_open_close_epos where Status='Open' and IdentityNo='"+identityNo+"'");
        String tripId="";
        while(rs.next()){
            tripId = rs.getString("TripId");
        }
        out.println(tripId.trim()+"$");
        if(conn != null) {
            conn.close();
        }
    }catch(Exception e) {
        out.println("0$");
        FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Vehicle IN OUT --> ").append(e.getMessage()).toString());
    }
    finally{
        if(conn != null) {
            conn.close();

        }
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.util.Date,java.text.SimpleDateFormat,ets.domain.util.FPLogUtils" %>