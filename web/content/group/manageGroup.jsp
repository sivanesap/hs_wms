<%-- 
    Document   : manageGroup
    Created on : Jun 21, 2010, 6:55:50 PM
    Author     : Hari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

</head>
<script>
function submitPage(value){
if(value == "add"){
document.manageGroup.action = '/throttle/handleViewAddGroup.do';
document.manageGroup.submit();
}else if(value == 'alter'){
document.manageGroup.action = '/throttle/handleViewGroupAlter.do';
document.manageGroup.submit();
}
}
</script>
<body>
<form name="manageGroup" method="post" >
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table width="388" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td  align="left" width="90" height="30" class="contentsub"><div class="contentsub">Group Name</div></td>
<td  align="left" width="120" height="30" class="contentsub"><div class="contentsub">Group Description</div></td>
<td align="left" width="65" height="30" class="contentsub"><div class="contentsub">Status</div></td>
</tr>
<% int index=0; %>
<c:if test = "${groupList != null}" >
      <c:forEach items="${groupList}" var="group">
<%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td class="<%=classText %>" width="90"  height="30" style="padding-left:30px; "><c:out value="${group.groupName}"/></td>
<td class="<%=classText %>" width="265" height="30" style="padding-left:30px; "><c:out value="${group.description}"/></td>
<td class="<%=classText %>" width="65" height="30" style="padding-left:20px; ">
<c:if test="${(group.activeInd=='n') || (group.activeInd=='N')}" >
InActive
 </c:if>
<c:if test="${(group.activeInd=='y') || (group.activeInd=='Y')}" >
Active
</c:if>
</td>
</tr>
<%
   index++;
 %>
</c:forEach>
  </c:if>
</table>
<br>
<center>
<input type="button" class="button" value="Add" name="add" onClick="submitPage(this.name)">
<input type="button" class="button" value="Alter" name="alter" onClick="submitPage(this.name)">
</center>
<input type="hidden" value="" name="reqfor">

</form>
</body>


</html>

