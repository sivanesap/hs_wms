<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/sweetalert.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/sweetalert.css">
        <style>
            .errorClass { 
                border:  1px solid red; 
            }
            .noErrorClass { 
                border:  1px solid black; 
            }
        </style>
        <script type="text/javascript">

            function onKeyPressBlockCharacters(e){
            var sts = false;
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
//                reg = /[a-zA-Z-#$@!%^&*()~`_+=|}{/?><,.;:'"[\ ]+$/;
            reg = /[a-zA-Z-`~!@#$%^&*() _|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            sts = !reg.test(keychar);
            return sts;
            }

            //this autocomplete is used for consignee and consignor names
            $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#consignorName').autocomplete({
            source: function(request, response) {
            $.ajax({
            url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                    consignorName: request.term,
                            customerId: document.getElementById('customerId').value

                    },
                    success: function(data, textStatus, jqXHR) {
                    var items = data;
                    response(items);
                    },
                    error: function(data, type) {
                    console.log(type);
                    }
            });
            },
                    minLength: 1,
                    select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consignorName').val(value);
                    $('#consignorPhoneNo').val(ui.item.Mobile);
                    $('#consignorAddress').val(ui.item.Address);
                    return false;
                    }
            }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
            };
            $('#consigneeName').autocomplete({
            source: function(request, response) {
            $.ajax({
            url: "/throttle/getConsigneeName.do",
                    dataType: "json",
                    data: {
                    consigneeName: request.term,
                            customerId: document.getElementById('customerId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                    var items = data;
                    response(items);
                    },
                    error: function(data, type) {
                    console.log(type);
                    }
            });
            },
                    minLength: 1,
                    select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consigneeName').val(value);
                    $('#consigneePhoneNo').val(ui.item.Mobile);
                    $('#consigneeAddress').val(ui.item.Address);
                    return false;
                    }
            }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
            }

            });</script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
            $("#tabs").tabs();
            });</script>
        <script type="text/javascript">

            function convertStringToDate(y, m, d, h, mi, ss) {
            //1
            //var dateString = "2013-08-09 20:02:03";
            var dateString = y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + ss;
            //alert(dateString);
            var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
            var dateArray = reggie.exec(dateString);
            var dateObject = new Date(
                    ( + dateArray[1]),
                    ( + dateArray[2]) - 1, // Careful, month starts at 0!
                    ( + dateArray[3]),
                    ( + dateArray[4]),
                    ( + dateArray[5]),
                    ( + dateArray[6])
                    );
            //alert(dateObject);
            return dateObject;
            }

            function viewCustomerContract() {
            window.open('/throttle/viewCustomerContractDetails.do?custId=' + $("#customerId").val(), 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

        </script>


        <script type="text/javascript">

            $(document).ready(function() {
            $("#datepicker").datepicker({
            showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

            });
            });
            $(function() {
            $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
            });
            });</script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = '';
            var snumber = '';
            function estimateFreight() {
            var billingTypeId = document.getElementById("billingTypeId").value;
            var customerTypeId = document.getElementById("customerTypeId").value;
            //customertypeid 1-> contract; 2-> walk in
            //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
            var totalKm = 0;
            var totalHr = 0;
            var rateWithReefer = 0;
            var rateWithoutReefer = 0;
            if (customerTypeId == 1 && billingTypeId == 1) {//point to point
            var temp = document.cNote.vehTypeIdContractTemp.value;
            if (temp == 0) {
            alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
            var temp = document.cNote.vehTypeIdContractTemp.value;
            //alert(temp);
            var tempSplit = temp.split("-");
            rateWithReefer = tempSplit[5];
            rateWithoutReefer = tempSplit[6];
            totalKm = tempSplit[2];
            totalHr = tempSplit[3];
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = rateWithReefer;
            } else {
            document.cNote.totFreightAmount.value = rateWithoutReefer;
            }
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
            }
            } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
            var temp = document.cNote.vehTypeIdContractTemp.value;
            if (temp == 0) {
            alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
            var tempSplit = temp.split("-");
            rateWithReefer = tempSplit[7];
            rateWithoutReefer = tempSplit[8];
            totalKm = tempSplit[2];
            totalHr = tempSplit[3];
            var totalWeight = document.cNote.totalWeightage.value;
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
            } else {
            document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
            }
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
            }
            } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
            var pointRouteKm = document.getElementsByName("pointRouteKm");
            var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
            //calculate total km and total hrs
            for (var i = 0; i < pointRouteKm.length; i++) {
            totalKm = totalKm + parseFloat(pointRouteKm[i].value);
            totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
            }

            var temp = document.cNote.vehTypeIdTemp.value;
            if (temp == 0) {
            alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
            var tempSplit = temp.split("-");
            var vehicleRatePerKm = tempSplit[1];
            var reeferRatePerHour = tempSplit[2];
            var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
            var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
            } else {
            document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
            }
            }
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
            } else if (customerTypeId == 2) { //walk in

            var pointRouteKm = document.getElementsByName("pointRouteKm");
            var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
            var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
            //calculate total km and total hrs
            for (var i = 0; i < pointRouteKm.length; i++) {
            totalKm = totalKm + parseFloat(pointRouteKm[i].value);
            totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
            }
            if (walkInBillingTypeId == 1) {//fixed rate
            rateWithReefer = document.cNote.walkinFreightWithReefer.value;
            rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
            } else {
            document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
            }
            } else if Per Kg Rate(walkInBillingTypeId == 2) {//rate per km
            rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
            } else {
            document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);
            }
            } else if (walkInBillingTypeId == 3) {// rate per kg
            var totalWeight = document.cNote.totalWeightage.value;
            rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
            rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
            if (document.cNote.reeferRequired.value == 'Yes') {
            document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
            } else {
            document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
            }

            }
            document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
            }
            }

        </script>
        <script language="">
            function print(val)
            {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            }
        </script>
    </head>

    <script>

        $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({

        source: function(request, response) {
        $.ajax({
        url: "/throttle/getCustomerDetails.do",
                dataType: "json",
                data: {
                customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                },
                success: function(data, textStatus, jqXHR) {
                var items = data;
                response(items);
                },
                error: function(data, type) {
                console.log(type);
                }
        });
        },
                minLength: 1,
                select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                $('#customerAddress').text(tmp[3]);
                $('#pincode').val(tmp[4]);
                $('#customerMobileNo').val(tmp[6]);
                $('#customerPhoneNo').val(tmp[5]);
                $('#mailId').val(tmp[7]);
                $('#billingTypeId').val(tmp[8]);
                $('#billingTypeName').text(tmp[9]);
                $('#billTypeName').text(tmp[9]);
                $('#contractNo').text(tmp[10]);
                var endDates = document.getElementById("startDate").value;
                var tempDate1 = endDates.split("-");
                var stDates = tmp[11];
                var tempDate2 = stDates.split("-");
                var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]); // Feb 1, 2011
                var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);
                if (prevTime.getTime() > thisTime.getTime()) {
                $('#contractExpDate').text(tmp[11]);
                } else {
                $('#contractExpDate1').text(tmp[11]);
                alert("Order Cannot Processed Customer Contract Expired");
                }



                $('#contractId').val(tmp[12]);
                $('#paymentType').val(tmp[13]);
                $('#consignorName').val(tmp[1]);
                $('#consignorPhoneNo').val(tmp[6]);
                $('#consignorAddress').val(tmp[3]);
                $('#consigneeName').val(tmp[1]);
                $('#consigneePhoneNo').val(tmp[6]);
                $('#consigneeAddress').val(tmp[3]);
                $('#creditLimitTemp').text(tmp[14]);
                $('#creditDaysTemp').text(tmp[15]);
                $('#outStandingTemp').text(tmp[16]);
                $('#customerRankTemp').text(tmp[17]);
                if (tmp[18] == "0") {
                $('#approvalStatusTemp').text("No");
                } else {
                $('#approvalStatusTemp').text("Yes");
                }
                $('#outStandingDateTemp').text(tmp[19]);
                $('#creditLimit').val(tmp[14]);
                $('#creditDays').val(tmp[15]);
                $('#outStanding').val(tmp[16]);
                $('#customerRank').val(tmp[17]);
                $('#approvalStatus').val(tmp[18]);
                $('#outStandingDate').val(tmp[19]);
//                $('#creditLimitTable').show();
                //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                $("#contractDetails").show();
                document.getElementById('customerCode').readOnly = true;
                $('#origin').empty();
                $('#origin').append(
                        $('<option style="width:150px"></option>').val(0).html('--Select--')
                        )
                        if (tmp[8] == '3') {//actual kms contract
                setVehicleTypeForActualKM(tmp[12]);
                $('#vehicleTypeDiv').show();
                $('#vehicleTypeContractDiv').hide();
                $('#contractRouteDiv2').show();
                $('#contractRouteDiv1').hide();
                } else {
                $('#vehicleTypeDiv').hide();
                $('#vehicleTypeContractDiv').show();
                $('#contractRouteDiv1').show();
                $('#contractRouteDiv2').hide();
                }
                $("#customerName").removeClass('errorClass');
                $("#customerName").addClass('noErrorClass');
                $("#productCategoryId").focus();
                return false;
                }
        }).data("autocomplete")._renderItem = function(ul, item) {
        var itemVal = item.Name;
        var temp = itemVal.split('~');
        itemVal = '<font color="green">' + temp[1] + '</font>';
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
        };
        $('#customerCode').autocomplete({
        source: function(request, response) {
        $.ajax({
        url: "/throttle/getCustomerDetails.do",
                dataType: "json",
                data: {
                customerCode: request.term,
                        customerName: document.getElementById('customerName').value
                },
                success: function(data, textStatus, jqXHR) {
                var items = data;
                response(items);
                },
                error: function(data, type) {
                console.log(type);
                }
        });
        },
                minLength: 1,
                select: function(event, ui) {
                $("#customerCode").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                $itemrow.find('#customerAddress').val(tmp[3]);
                $itemrow.find('#customerMobileNo').val(tmp[4]);
                $itemrow.find('#customerPhoneNo').val(tmp[5]);
                $itemrow.find('#mailId').val(tmp[6]);
                $itemrow.find('#billingTypeId').val(tmp[7]);
                $('#billingTypeName').val(tmp[8]);
                $('#billTypeName').val(tmp[8]);
                $("#contractDetails").show();
                document.getElementById('customerName').readOnly = true;
                return false;
                }
        }).data("autocomplete")._renderItem = function(ul, item) {
        var itemVal = item.Name;
        var temp = itemVal.split('-');
        itemVal = '<font color="green">' + temp[2] + '</font>';
        return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
        };
        });
    </script>

    <script type="text/javascript">
        $(function () {
        $('#orderReferenceAwb,#orderReferenceAwbNo,#orderReferenceEnd').keyup(function (e) {
        var name = this.id;
        var chAwb1 = document.getElementById("orderReferenceAwb").value + document.getElementById("orderReferenceAwbNo").value + document.getElementById("orderReferenceEnd").value;
        if (name == 'orderReferenceEnd'){
        if ($(this).val().length == 4){
        if (chAwb1.length == 11){
        var awbStatus = checkAWBnoExist();
        } else{
        $("#orderReferenceAwb").removeClass('noErrorClass');
        $("#orderReferenceAwbNo").removeClass('noErrorClass');
        $("#orderReferenceEnd").removeClass('noErrorClass');
        $("#orderReferenceAwb").addClass('errorClass');
        $("#orderReferenceAwbNo").addClass('errorClass');
        $("#orderReferenceEnd").addClass('errorClass');
        }
        if (awbStatus == 1){
        alert("Invalid AWB NO");
        $('#orderReferenceEnd').focus();
        } else if (awbStatus == 2){
        alert("AWB No Should Have 11 Digit,You Entered Only: " + chAwb1.length);
        $('#orderReferenceEnd').focus();
        } else{
        $('#truckOriginName1').focus();
        }
        } else{
        $("#orderReferenceAwb").removeClass('noErrorClass');
        $("#orderReferenceAwbNo").removeClass('noErrorClass');
        $("#orderReferenceEnd").removeClass('noErrorClass');
        $("#orderReferenceAwb").addClass('errorClass');
        $("#orderReferenceAwbNo").addClass('errorClass');
        $("#orderReferenceEnd").addClass('errorClass');
        }
        } else{
        if ($(this).val().length == $(this).attr('maxlength')){
        $(this).addClass('noErrorClass');
        $(this).next(':input').focus();
        } else{
        $(this).addClass('errorClass');
        }
        }
        })
        })
    </script>

    <body onload="cretaeOrderSubmit(''); setAwbPackages(1); enableContract(1); setShipmentType(1);">
        <% String menuPath = "Operations >>  Create New Booking";
                    request.setAttribute("menuPath", menuPath);
        %>
        <form name="cNote" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@include file="/content/common/message.jsp" %>
            <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String startDate = sdf.format(today);
            %>
            <input type="hidden" name="startDate" id="startDate" value='<%=startDate%>' />



            <script type="text/javascript">

                function enableContract(val) {
                //alert(val);
                if (val == 1) {
                $("#contractCustomerTable").show();
                $("#walkinCustomerTable").hide();
                $("#contractFreightTable").show();
                $("#walkinFreightTable").hide();
                document.getElementById('customerAddress').readOnly = true;
                document.getElementById('pincode').readOnly = true;
                document.getElementById('customerMobileNo').readOnly = true;
                document.getElementById('mailId').readOnly = true;
                document.getElementById('customerPhoneNo').readOnly = true;
                document.getElementById('walkinCustomerName').value = "";
                document.getElementById('walkinCustomerCode').value = "";
                document.getElementById('walkinCustomerAddress').value = "";
                document.getElementById('walkinPincode').value = "";
                document.getElementById('walkinCustomerMobileNo').value = "";
                document.getElementById('walkinMailId').value = "";
                document.getElementById('walkinCustomerPhoneNo').value = "";
                }
                freightDetails(val);
                }

                function setAwbType(value){
                if (parseInt(value) == 2){
                document.getElementById("orderReferenceAwb").readOnly = true;
                document.getElementById("orderReferenceAwbNo").readOnly = true;
                document.getElementById("orderReferenceEnd").readOnly = true;
                } else{
                document.getElementById("orderReferenceAwb").value = "";
                document.getElementById("orderReferenceAwbNo").value = "";
                document.getElementById("orderReferenceEnd").value = "";
                $("#orderReferenceAwb").removeClass('noErrorClass');
                $("#orderReferenceAwbNo").removeClass('noErrorClass');
                $("#orderReferenceEnd").removeClass('noErrorClass');
                $("#orderReferenceAwb").addClass('errorClass');
                $("#orderReferenceAwbNo").addClass('errorClass');
                $("#orderReferenceEnd").addClass('errorClass');
                document.getElementById("awbno").value = "0";
                document.getElementById("orderReferenceAwb").readOnly = false;
                document.getElementById("orderReferenceAwbNo").readOnly = false;
                document.getElementById("orderReferenceEnd").readOnly = false;
                }
                }

                function checkAWBnoExist(){
                var chAwbs = document.getElementById("orderReferenceAwb").value + " " + document.getElementById("orderReferenceAwbNo").value + " " + document.getElementById("orderReferenceEnd").value;
                var shipmentType = document.getElementById("shipmentType").value;
                $.ajax({
                url: '/throttle/checkAwbNo.do',
                        data: {chAwb:chAwbs, shipmentType:shipmentType},
                        dataType: 'json',
                        success: function (data) {

                        if (data == '' || data == null) {
                        //                        alert('');
                        } else {
                        $.each(data, function (i, data) {
//                                alert(data.Id);
//                                        alert(data.AwbTotalPackages);
//                                        alert(data.AwbReceivedPackages);
//                                        alert(data.AwbPendingPackages);
                        if ($("#shipmentType").val() == 1){
                        if (data.Id == 1){
                        alert("Booking Already Exists");
                        $("#orderReferenceAwb").removeClass('noErrorClass');
                        $("#orderReferenceAwbNo").removeClass('noErrorClass');
                        $("#orderReferenceEnd").removeClass('noErrorClass');
                        $("#orderReferenceAwb").addClass('errorClass');
                        $("#orderReferenceAwbNo").addClass('errorClass');
                        $("#orderReferenceEnd").addClass('errorClass');
                        document.getElementById("orderReferenceAwb").value = "";
                        document.getElementById("orderReferenceAwbNo").value = "";
                        document.getElementById("orderReferenceEnd").value = "";
                        $('#orderReferenceAwb').focus();
                        } else{
                        alert("Booking Already Exists");
                        $("#orderReferenceAwb").removeClass('noErrorClass');
                        $("#orderReferenceAwbNo").removeClass('noErrorClass');
                        $("#orderReferenceEnd").removeClass('noErrorClass');
                        $("#orderReferenceAwb").addClass('errorClass');
                        $("#orderReferenceAwbNo").addClass('errorClass');
                        $("#orderReferenceEnd").addClass('errorClass');
                        document.getElementById("orderReferenceAwb").value = "";
                        document.getElementById("orderReferenceAwbNo").value = "";
                        document.getElementById("orderReferenceEnd").value = "";
                        $('#orderReferenceAwb').focus();
                        }
                        }
                        else{
                        if (data.Id == 1){
                        alert("Booking Already Exists");
                        $("#orderReferenceAwb").removeClass('noErrorClass');
                        $("#orderReferenceAwbNo").removeClass('noErrorClass');
                        $("#orderReferenceEnd").removeClass('noErrorClass');
                        $("#orderReferenceAwb").addClass('errorClass');
                        $("#orderReferenceAwbNo").addClass('errorClass');
                        $("#orderReferenceEnd").addClass('errorClass');
                        document.getElementById("orderReferenceAwb").value = "";
                        document.getElementById("orderReferenceAwbNo").value = "";
                        document.getElementById("orderReferenceEnd").value = "";
                        $('#orderReferenceAwb').focus();
                        } else if (parseInt(data.AwbTotalPackages) == parseInt(data.AwbReceivedPackages)){
                        alert("Booking Already Exists");
                        $("#orderReferenceAwb").removeClass('noErrorClass');
                        $("#orderReferenceAwbNo").removeClass('noErrorClass');
                        $("#orderReferenceEnd").removeClass('noErrorClass');
                        $("#orderReferenceAwb").addClass('errorClass');
                        $("#orderReferenceAwbNo").addClass('errorClass');
                        $("#orderReferenceEnd").addClass('errorClass');
                        document.getElementById("orderReferenceAwb").value = "";
                        document.getElementById("orderReferenceAwbNo").value = "";
                        document.getElementById("orderReferenceEnd").value = "";
                        $('#orderReferenceAwb').focus();
                        } else{
                        document.getElementById("awbTotalPackages").value = data.AwbTotalPackages;
                        document.getElementById("awbReceivedPackages").value = data.AwbReceivedPackages;
                        document.getElementById("awbPendingPackages").value = data.AwbPendingPackages;
                        document.getElementById("awbTotalGrossWeight").value = data.AwbTotalGrossWeight;
                        document.getElementById("awbTotalPackagesOld").value = data.AwbTotalPackages;
                        document.getElementById("awbReceivedPackagesOld").value = data.AwbReceivedPackages;
                        document.getElementById("awbPendingPackagesOld").value = data.AwbPendingPackages;
                        }
                        }
//                                   
                        });
                        }
                        }
                });
                var chAwb = document.getElementById("orderReferenceAwb").value + " " + document.getElementById("orderReferenceAwbNo").value + " " + document.getElementById("orderReferenceEnd").value;
                var chAwb1 = document.getElementById("orderReferenceAwb").value + document.getElementById("orderReferenceAwbNo").value + document.getElementById("orderReferenceEnd").value;
                var awbNo = document.getElementById("orderReferenceAwbNo").value + document.getElementById("orderReferenceEnd").value;
                var prodnum = awbNo.toString(); // add this if prodnum is numeric 
                var lastDigit = prodnum.substr(prodnum.length - 1);
//                    alert(lastDigit);
                var awbStatus = 0;
                if (parseInt(lastDigit) > 6){
                awbStatus = 1;
                } else{
                var rmDigit = awbNo.substring(0, awbNo.length - 1);
                var value = parseInt(rmDigit) / 7;
                var value1 = parseInt(value) * 7;
                var value3 = parseInt(rmDigit) - parseInt(value1);
                if (parseInt(value3) > 6 || parseInt(value3) != parseInt(lastDigit)){
                awbStatus = 1;
                }
                }
                if (awbStatus > 0){
                $("#orderReferenceAwb").removeClass('noErrorClass');
                $("#orderReferenceAwbNo").removeClass('noErrorClass');
                $("#orderReferenceEnd").removeClass('noErrorClass');
                $("#orderReferenceAwb").addClass('errorClass');
                $("#orderReferenceAwbNo").addClass('errorClass');
                $("#orderReferenceEnd").addClass('errorClass');
                $('#orderReferenceEnd').focus();
                } else{
                $("#orderReferenceAwb").removeClass('errorClass');
                $("#orderReferenceAwbNo").removeClass('errorClass');
                $("#orderReferenceEnd").removeClass('errorClass');
                $("#orderReferenceAwb").addClass('noErrorClass');
                $("#orderReferenceAwbNo").addClass('noErrorClass');
                $("#orderReferenceEnd").addClass('noErrorClass');
                $('#orderReferenceEnd').focus();
                }
                if (parseInt(chAwb1.length) < 11){
                awbStatus = 2;
                }
                return awbStatus;
                }
            </script>
            <div id="tabs" style="height: auto">
                <ul>
                    <li><a href="#customerDetail"><span>Order Booking</span></a></li>
                    <!--                    <li><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>-->
                    <!--                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>-->

                </ul>

                <div id="customerDetail" style="height: auto">


                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable1">
                        <tr>
                            <td class="contenthead" colspan="4">Booking </td>
                        </tr>

                        <tr>
                            <!--   <td class="text1">Consignment Note </td>
                <td class="text1"><input type="text" name="consignmentNoteNo" id="consignmentNoteNo" value="<c:out value="${consignmentOrderNo}"/>" class="textbox"readonly style="width: 120px"/></td>-->
                            <td class="text1"><font color="red">*</font>Customer Type</td>
                            <td class="text1">
                                <c:if test="${customerTypeList != null}">
                                    <select name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" class="textbox" style="width:120px;" >
                                        <option value="1">Contract</option>
                                    </select>
                                </c:if>
                            </td>
                            <td class="text1">Booking Date </td>
                            <td class="text1" ><input type="text" name="consignmentDate" id="consignmentDate" value="<c:out value="${consignmentOrderDate}"/>" class="textbox"readonly style="width: 120px"/></td>
                        </tr>
                        <tr class="text2">
                            <td class="text1"><font color="red">*</font>Movement Type</td>
                            <td class="text1">
                                <select name="awbMovementType" id="awbMovementType" class="textbox" onchange="cretaeOrderSubmit('')">
                                    <option value="0">--Select--</option>
                                    <option value="1">Bonded-Import</option>
                                    <option value="2">Bonded-Export</option>
                                    <option value="3">Non Bonded-Import</option>
                                    <option value="4">Non Bonded-Export</option>
                                    <option value="5">Non Bonded</option>
                                </select>
                            </td>
                        <script>
                            document.getElementById("awbMovementType").value = '<c:out value='${awbMovementType}'/>';
                        </script>

                        <td >Shipment Type</td>
                        <td> 
                            <c:if test="${shipmentType == 1}">
                                Full-Shipment
                            </c:if>
                            <c:if test="${shipmentType == 2}">
                                Part-Shipment
                            </c:if>
                            <input type="hidden" name="shipmentType" id="shipmentType" value="<c:out value='${shipmentType}'/>"/>
                        </td>
                        </tr>
                        <tr class="text1">
                            <td>AWB Type</td>
                            <td>
                                <c:if test="${awbType == 1}">Non Auto generated</c:if>
                                <c:if test="${awbType == 2}">Auto generated</c:if>
                                    <input type="hidden" name="awbno" id="awbno" value="0"/>
                                    <input type="hidden" name="awbType" id="awbType" value="<c:out value='${awbType}'/>"/>
                            </td>
                        <script>
                            setAwbType('<c:out value='${awbType}'/>');
                        </script>
                        <td><font color="red">*</font>AWB No</td>
                        <td>
                            <c:if test="${awbType == 2}">
                            <input type="hidden" name="orderReferenceAwb" id="orderReferenceAwb" value="<c:out value='${orderReferenceAwb}'/>" maxlength="3" size="3" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 30px" readonly/>
                            <c:out value='${orderReferenceAwb}'/>
                            <input type="hidden" name="orderReferenceAwbNo" id="orderReferenceAwbNo" value="<c:out value='${orderReferenceAwbNo}'/>" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width:40px" readonly/>
                            &nbsp;<c:out value='${orderReferenceAwbNo}'/>
                            <input type="hidden" name="orderReferenceEnd" id="orderReferenceEnd" value="<c:out value='${orderReferenceEnd}'/>" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 40px" readonly/>
                            &nbsp;<c:out value='${orderReferenceEnd}'/>
                            </c:if>
                            <c:if test="${awbType == 1}">
                            <input type="text" name="orderReferenceAwb" id="orderReferenceAwb" value="<c:out value='${orderReferenceAwb}'/>" maxlength="3" size="3" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 30px" readonly/>
                            <input type="text" name="orderReferenceAwbNo" id="orderReferenceAwbNo" value="<c:out value='${orderReferenceAwbNo}'/>" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width:40px" readonly/>
                            <input type="text" name="orderReferenceEnd" id="orderReferenceEnd" value="<c:out value='${orderReferenceEnd}'/>" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 40px" readonly/>
                            </c:if>
                        </td>
                        </tr>
                        <tr>

                            <td class="text2"><font color="red">*</font>Transit Origin</td>
                            <td class="text2">
                                <input type="hidden" name="awbOriginId" id="awbOriginId" class="textbox" style="width: 120px" value="<c:out value='${awbOriginId}'/>"/>
                                <input type="hidden" name="awbOrigin" id="awbOrigin" value="" class="textbox" style="width: 120px"/>
                                <input type="hidden" name="truckOriginId" id="truckOriginId1" value="<c:out value='${awbOriginId}'/>" class="textbox">
                                <input type="hidden" name="truckOriginName" id="truckOriginName1" value="<c:out value='${awbOriginName}'/>" class="textbox" style="width: 120px;" onchange="cretaeOrderSubmit('');" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" onkeyup="return checkKey(this, event, 'truckOriginId1')">
                                <c:out value='${awbOriginName}'/>
                            </td>
                            <td class="text2"><font color="red">*</font>Transit Destination</td>
                            <td class="text2">
                                <input type="hidden" name="awbDestinationId" id="awbDestinationId" class="textbox" style="width: 120px" value="<c:out value='${awbDestinationId}'/>"/>
                                <input type="hidden" name="awbDestination" id="awbDestination" value="" class="textbox" style="width: 120px"/>
                                <input type="hidden" name="truckDestinationId" id="truckDestinationId1" value="<c:out value='${awbDestinationId}'/>">
                                <input type="hidden" name="truckDestinationName" id="truckDestinationName1" value="<c:out value='${awbDestinationName}'/>" class="textbox" style="width: 120px;" onchange="cretaeOrderSubmit('');" onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" onkeyup="return checkKey(this, event, 'truckDestinationId1')">
                                <c:out value='${awbDestinationName}'/>
                            </td>
                        </tr>
                        <script>
                            function checkKey(obj, e, id){
                            var idVal = $('#' + id).val();
                            if (e.keyCode == 46 || e.keyCode == 8){
                            $('#' + obj.id).val('');
                            $('#' + obj.id).removeClass('noErrorClass');
                            $('#' + obj.id).addClass('errorClass');
                            $('#' + id).val('');
                            document.getElementById("routeInfo").innerHTML = "<b> Truck Information Route Information()</b>";
                            } else if (idVal != ''){
                            $('#' + obj.id).val('');
                            $('#' + obj.id).removeClass('noErrorClass');
                            $('#' + obj.id).addClass('errorClass');
                            $('#' + id).val('');
                            document.getElementById("routeInfo").innerHTML = "<b> Truck Information Route Information()</b>";
                            }
                            }
                            $(document).ready(function() {

                            $('#awbOrigin').autocomplete({
                            source: function(request, response) {
                            $.ajax({
                            url: "/throttle/getTruckCityList.do",
                                    dataType: "json",
                                    data: {
                                    cityName: request.term,
                                            textBox: 1
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                    var items = data;
                                    response(items);
                                    },
                                    error: function(data, type) {

                                    //console.log(type);
                                    }
                            });
                            },
                                    minLength: 1,
                                    select: function(event, ui) {
                                    var value = ui.item.Name;
                                    var id = ui.item.Id;
                                    //alert(id+" : "+value);
                                    $('#awbOriginId').val(id);
                                    $('#awbOrigin').val(value);
                                    document.getElementById("routeInfo").innerHTML = "<b>Truck Information Route Information (" + document.getElementById("awbOrigin").value + " - " + document.getElementById("awbDestination").value + ")<b>";
                                    $('#awbDestination').focus();
                                    return false;
                                    }

                            // Format the list menu output of the autocomplete
                            }).data("autocomplete")._renderItem = function(ul, item) {
                            //alert(item);
                            var itemVal = item.Name;
                            itemVal = '<font color="green">' + itemVal + '</font>';
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + itemVal + "</a>")
                                    .appendTo(ul);
                            };
                            $('#awbDestination').autocomplete({
                            source: function(request, response) {
                            $.ajax({
                            url: "/throttle/getTruckCityList.do",
                                    dataType: "json",
                                    data: {
                                    cityName: request.term,
                                            textBox: 1
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                    var items = data;
                                    response(items);
                                    },
                                    error: function(data, type) {

                                    //console.log(type);
                                    }
                            });
                            },
                                    minLength: 1,
                                    select: function(event, ui) {
                                    var value = ui.item.Name;
                                    var id = ui.item.Id;
                                    //alert(id+" : "+value);
                                    $('#awbDestinationId').val(id);
                                    $('#awbDestination').val(value);
                                    document.getElementById("routeInfo").innerHTML = "<b> Truck Information Route Information (" + document.getElementById("awbOrigin").value + " - " + document.getElementById("awbDestination").value + ")</b>";
                                    return false;
                                    }

                            // Format the list menu output of the autocomplete
                            }).data("autocomplete")._renderItem = function(ul, item) {
                            //alert(item);
                            var itemVal = item.Name;
                            itemVal = '<font color="green">' + itemVal + '</font>';
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + itemVal + "</a>")
                                    .appendTo(ul);
                            };
                            });</script>    
                        <script>
                            $(document).ready(function() {

                            $('#awbOriginRegion').autocomplete({
                            source: function(request, response) {
                            $.ajax({
                            url: "/throttle/getTruckCityCodeList.do",
                                    dataType: "json",
                                    data: {
                                    cityCode: request.term,
                                            textBox: 1
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                    var items = data;
                                    response(items);
                                    },
                                    error: function(data, type) {

                                    //console.log(type);
                                    }
                            });
                            },
                                    minLength: 1,
                                    select: function(event, ui) {
                                    var value = ui.item.Name;
                                    var code = ui.item.cityCode;
                                    var id = ui.item.Id;
                                    //alert(id+" : "+value);
                                    $('#awbOriginRegionId').val(1);
                                    $('#awbOriginRegion').val(code);
                                    $('#awbDestinationRegion').focus();
                                    $("#awbOriginRegion").removeClass('errorClass');
                                    $("#awbOriginRegion").addClass('noErrorClass');
                                    return false;
                                    }

                            // Format the list menu output of the autocomplete
                            }).data("autocomplete")._renderItem = function(ul, item) {
                            //alert(item);
                            var itemVal = item.cityCode;
                            itemVal = '<font color="green">' + itemVal + '</font>';
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + itemVal + "</a>")
                                    .appendTo(ul);
                            };
                            $('#awbDestinationRegion').autocomplete({
                            source: function(request, response) {
                            $.ajax({
                            url: "/throttle/getTruckCityCodeList.do",
                                    dataType: "json",
                                    data: {
                                    cityCode: request.term,
                                            textBox: 1
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                    var items = data;
                                    response(items);
                                    },
                                    error: function(data, type) {

                                    //console.log(type);
                                    }
                            });
                            },
                                    minLength: 1,
                                    select: function(event, ui) {
                                    var value = ui.item.Name;
                                    var id = ui.item.Id;
                                    var code = ui.item.cityCode;
                                    //alert(id+" : "+value);
                                    $('#awbDestinationRegionId').val(1);
                                    $('#awbDestinationRegion').val(code);
                                    $('#orderReferenceNo').focus();
                                    $("#awbDestinationRegion").removeClass('errorClass');
                                    $("#awbDestinationRegion").addClass('noErrorClass');
                                    return false;
                                    }

                            // Format the list menu output of the autocomplete
                            }).data("autocomplete")._renderItem = function(ul, item) {
                            //alert(item);
                            var itemVal = item.cityCode;
                            itemVal = '<font color="green">' + itemVal + '</font>';
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + itemVal + "</a>")
                                    .appendTo(ul);
                            };
                            });</script>    
                        <tr>
                            <td class="text1"><font color="red"></font>Order Delivery Date</td>
                            <td class="text1">
                                <input type="hidden" name="awbOrderDeliveryDate" autocomplete="off" id="awbOrderDeliveryDate" value="<c:out value='${awbOrderDeliveryDate}'/>" class="datepicker" style="width: 120px" onchange="cretaeOrderSubmit('');" readonly/>
                                <c:out value='${awbOrderDeliveryDate}'/>
                            </td>
                            <td class="text1"><font color="red"></font>Order Delivery Time</td>
                            <td class="text1">
                                HH:<select name="orderDeliveryHour" id="orderDeliveryHour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="orderDeliveryMinute" id="orderDeliveryMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
                            </td>
                        <script>
                            document.getElementById("orderDeliveryHour").value = '<c:out value='${orderDeliveryHour}'/>';
                            document.getElementById("orderDeliveryMinute").value = '<c:out value='${orderDeliveryMinute}'/>';
                        </script>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>AWB Origin City</td>
                            <td class="text2">
                                <input type="hidden" id="awbOriginRegionId" value="1"/>
                                <input type="hidden" name="awbOriginRegion" id="awbOriginRegion" value="<c:out value='${awbOriginRegion}'/>" class="textbox" style="width: 120px;" onchange="cretaeOrderSubmit('');" onkeyup="return checkKey(this, event, 'awbOriginRegionId')"/>
                                <c:out value='${awbOriginRegion}'/>
                            </td>
                            <td class="text2"><font color="red">*</font>AWB Destination City</td>
                            <td class="text2">
                                <input type="hidden" id="awbDestinationRegionId" value="1"/>
                                <input type="hidden" name="awbDestinationRegion" id="awbDestinationRegion" value="<c:out value='${awbDestinationRegion}'/>" class="textbox" style="width: 120px;" onchange="cretaeOrderSubmit('');" onkeyup="return checkKey(this, event, 'awbDestinationRegionId')"/>
                                <c:out value='${awbDestinationRegion}'/>
                            </td>

                        </tr>
                        <tr>
                            <td >Customer Order Reference No</td>
                            <td ><input name="orderReferenceNo" id="orderReferenceNo" value="<c:out value='${consignmentRefNo}'/>" class="textbox" maxlength="10"   style="width: 120px" value="0"/></td>
                            <td class="text1">Airline Name</td>
                            <td class="text1">
                                <input type="text" name="airlineName" id="airlineName" value="<c:out value='${airlineName}'/>" class="textbox" style="width: 120px;"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div id="awbPackages" style="display:none">

                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td class="contenthead" colspan="4" >AWB packages</td>
                            </tr>
                            <tr>
                                <td class="text1"><font color="red">*</font>AWB Total Packages</td> 
                                <td class="text1"><input type="text" name="awbTotalPackages" id="awbTotalPackages" value="0" style="width: 120px;" /></td>
                            <input type="hidden" name="awbTotalPackagesOld" id="awbTotalPackagesOld" value="0" style="width: 120px;" />
                            <td class="text1"><font color="red">*</font>AWB Total Gross Weight</td> 
                            <td class="text1"><input type="text" name="awbTotalGrossWeight" id="awbTotalGrossWeight" value="0" style="width: 120px;"/></td>
                            </tr>
                            <tr>
                                <td class="text1"><font color="red">*</font>AWB Received Packages</td> 
                                <td class="text1"><input type="text" name="awbReceivedPackages" id="awbReceivedPackages" value="0" style="width: 120px;"  readonly/></td>
                            <input type="hidden" name="awbReceivedPackagesOld" id="awbReceivedPackagesOld" value="0" style="width: 120px;" />
                            <td class="text1"><font color="red">*</font>AWB Pending Packages</td> 
                            <td class="text1"><input type="text" name="awbPendingPackages" id="awbPendingPackages" readonly value="0" style="width: 120px;"/></td>
                            <input type="hidden" name="awbPendingPackagesOld" id="awbPendingPackagesOld" value="0" style="width: 120px;" />
                            </tr>
                        </table>
                    </div>
                    <br>
                    <script>
                        var val = document.getElementById("shipmentType").value;
                        setAwbPackages(val);
                        function setAwbPackages(val){
                        if (val == 2){
                        document.getElementById("awbPackages").style.display = "block";
                        checkAWBnoExist();
                        } else{
                        document.getElementById("awbPackages").style.display = "none";
                        }
                        }

                        function checkAWBnoExist() {
                        var chAwbs = document.getElementById("orderReferenceAwb").value + " " + document.getElementById("orderReferenceAwbNo").value + " " + document.getElementById("orderReferenceEnd").value;
                        var shipmentType = document.getElementById("shipmentType").value;
                        $.ajax({
                        url: '/throttle/checkAwbNo.do',
                                data: {chAwb: chAwbs, shipmentType: shipmentType},
                                dataType: 'json',
                                success: function (data) {

                                if (data == '' || data == null) {
                                //                        alert('');
                                } else {
                                $.each(data, function (i, data) {
//                                alert(data.Id);
//                                        alert(data.AwbTotalPackages);
//                                        alert(data.AwbReceivedPackages);
//                                        alert(data.AwbPendingPackages);
                                if ($("#shipmentType").val() == 1) {
                                if (data.Id == 1) {
                                alert("Booking Already Exists");
                                document.getElementById("orderReferenceAwb").value = "";
                                document.getElementById("orderReferenceAwbNo").value = "";
                                document.getElementById("orderReferenceEnd").value = "";
                                $('#orderReferenceAwb').focus();
                                } else {
                                alert("Booking Already Exists");
                                document.getElementById("orderReferenceAwb").value = "";
                                document.getElementById("orderReferenceAwbNo").value = "";
                                document.getElementById("orderReferenceEnd").value = "";
                                $('#orderReferenceAwb').focus();
                                }
                                } else {
                                if (data.Id == 1) {
                                alert("Booking Already Exists");
                                document.getElementById("orderReferenceAwb").value = "";
                                document.getElementById("orderReferenceAwbNo").value = "";
                                document.getElementById("orderReferenceEnd").value = "";
                                $('#orderReferenceAwb').focus();
                                } else if (parseInt(data.AwbTotalPackages) == parseInt(data.AwbReceivedPackages)) {
                                alert("Booking Already Exists");
                                document.getElementById("orderReferenceAwb").value = "";
                                document.getElementById("orderReferenceAwbNo").value = "";
                                document.getElementById("orderReferenceEnd").value = "";
                                $('#orderReferenceAwb').focus();
                                } else {
                                document.getElementById("awbTotalPackages").value = data.AwbTotalPackages;
                                document.getElementById("awbReceivedPackages").value = data.AwbReceivedPackages;
                                document.getElementById("awbPendingPackages").value = data.AwbPendingPackages;
                                document.getElementById("awbTotalGrossWeight").value = data.AwbTotalGrossWeight;
                                document.getElementById("awbTotalPackagesOld").value = data.AwbTotalPackages;
                                document.getElementById("awbReceivedPackagesOld").value = data.AwbReceivedPackages;
                                document.getElementById("awbPendingPackagesOld").value = data.AwbPendingPackages;
                                }
                                }
//                                   
                                });
                                }
                                }
                        });
                        }


                        function awbPackageRestrict() {
                        var shipmentType = document.getElementById("shipmentType").value;
                        if (parseInt(shipmentType) == 2){
//                        alert("Hi I Am Here ....................");
                        var awbTotalPackeges = document.getElementById("awbTotalPackages").value;
                        var awbReceivedPackeges = document.getElementById("awbReceivedPackages").value;
                        var awbPendingPackeges = document.getElementById("awbPendingPackages").value;
                        var awbTotalPackegesOld = document.getElementById("awbTotalPackagesOld").value;
                        var awbReceivedPackegesOld = document.getElementById("awbReceivedPackagesOld").value;
                        var awbPendingPackegesOld = document.getElementById("awbPendingPackagesOld").value;
//                           var noOfPieces=document.getElementById("noOfPieces1").value; 
                        var noOfPiecesValue = document.getElementsByName("noOfPieces");
                        var noOfPieces = 0;
                        var k = 0;
                        for (var i = 0; i < noOfPiecesValue.length; i++) {
                        if (noOfPiecesValue[i].value != '') {
                        noOfPieces += parseInt(noOfPiecesValue[i].value);
                        k = i + 1;
//                                alert(k);
                        }
                        }
//                           alert(awbTotalPackeges)
//                           alert(awbReceivedPackeges)
                        var averageAwbReceivedPackegesOld = parseInt(awbReceivedPackegesOld) + parseInt(noOfPieces);
//                                alert(averageAwbReceivedPackegesOld);

                        if (averageAwbReceivedPackegesOld > awbTotalPackeges){
//                        alert("Hi I Am Here 1....................");
                        alert("pls enter within" + awbTotalPackeges + " total packages");
                        document.getElementById("noOfPieces" + k).value = "";
                        }
                        else{
//                        alert("Hi I Am Here 2....................");
//                        alert("Hi I Am Here 1...................." + " noOfPieces " + noOfPieces + " awbReceivedPackegesOld " + awbReceivedPackegesOld + " awbPendingPackegesOld " + awbPendingPackegesOld);
//                        var cal = awbTotalPackeges - awbReceivedPackeges;

                        document.getElementById("awbReceivedPackages").value = parseInt(noOfPieces) + parseInt(awbReceivedPackegesOld);
                        document.getElementById("awbPendingPackages").value = parseInt(awbTotalPackeges) - parseInt(noOfPieces) - parseInt(awbReceivedPackegesOld);
//                                document.getElementById("awbReceivedPackeges").value = noOfPieces;
//                                document.getElementById("awbPendingPackeges").value = cal;
                        }
                        }
                        }
                        function checkNoOfPieces(){
                        var awbReceivedPackeges = document.getElementById("awbReceivedPackeges").value;
                        var awbPendingPackeges = document.getElementById("awbPendingPackeges").value;
                        var noOfPieces = document.getElementById("noOfPieces1").value;
                        if (noOfPieces == awbPendingPackeges){
                        document.getElementById("awbPendingPackeges").value = "0";
                        }
                        else if (noOfPieces < awbPendingPackeges){
                        var cal = awbPendingPackeges - noOfPieces;
                        document.getElementById("awbPendingPackeges").value = cal;
                        }
                        else if (noOfPieces > awbPendingPackeges){
                        alert("pls enter within" + awbPendingPackeges + " pending packages");
                        document.getElementById("noOfPieces1").value = "";
                        }


                        }

                    </script>

                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Contract Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Customer Name</td>
                            <td class="text1"><input type="hidden" name="customerId" value="<c:out value='${customerId}'/>" id="customerId" class="textbox" />
                                <input type="text" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" value="<c:out value='${customerName}'/>" class="textbox" onchange="cretaeOrderSubmit('');" onkeyup="return checkKey(this, event, 'customerId')"/></td>
                            <td class="text1"><font color="red">*</font>Customer Code</td>
                            <td class="text1"><input type="text" name="customerCode" id="customerCode" value="<c:out value='${customerCode}'/>" class="textbox"  /></td>
                        <input type="hidden" name="paymentType" id="paymentType" class="textbox" value="<c:out value='${customerBillingType}'/>" />
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="customerAddress" name="customerAddress" readonly><c:out value='${customerAddress}'/></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value='${customerPincode}'/>" readonly class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" value="<c:out value='${customerMobile}'/>" readonly class="textbox"  /></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="mailId" id="mailId" class="textbox"  value="<c:out value='${customerMobile}'/>" readonly/></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="textbox" maxlength="10"   value="<c:out value='${customerPhone}'/>"  readonly /></td>

                            <td class="text2">Billing Type</td>
                            <td class="text2" id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="textbox"  value="<c:out value='${customerBillingType}'/>"/><label id="billingTypeName" ><c:out value='${billingTypeName}'/></label></td>

                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Walkin Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font> Customer Name</td>
                            <td class="text1"><input type="text" name="walkinCustomerName" id="walkinCustomerName" class="textbox"   /></td>
                            <td class="text1"><font color="red">*</font> Customer Code</td>
                            <td class="text1"><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress"></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="textbox"  /></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="walkinMailId" id="walkinMailId" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text"     onKeyPress='return onKeyPressBlockCharacters(event);' name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="textbox" maxlength="10"   /></td>

                            <td class="text2" colspan="2">&nbsp</td>

                        </tr>
                    </table>
                    <div id="contractDetails" style="display: ">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b><input type="hidden" name="contractId" id="contractId" class="textbox" value="<c:out value='${customerContractId}'/>"/><label id="contractNo"><c:out value='${customerContractId}'/></label></b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><font color="green"><b><label id="contractExpDate"><c:out value='${contractTo}'/>
                                            <font color="red"><b><label id="contractExpDate1"></label></b></font></td>
                                            <td class="text1">&nbsp;&nbsp;&nbsp;</td>
                                            <!--                                <td class="text1"><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>-->
                                            <td class="text1">
                                                <!--                                    <input type="button" class="button" value="View Contract" onclick="viewCustomerContract()">-->
                                                <a href="#" onclick="viewCustomerContract();">view</a>
                                            </td>
                                            </tr>
                                            </table>
                                            </div>




                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                                                <tr>
                                                    <td class="contenthead" colspan="6" >Product Info</td>
                                                </tr>
                                                <tr>

                                                    <td class="text1"><font color="red">*</font>Freight Type</td>
                                                    <td class="text1"><c:if test="${productCategoryList != null}">
                                                            <!--<input type="hidden" name="productCategoryId" id="productCategoryId" class="textbox" />-->
                                                            <select name="productCategoryId" id="productCategoryId"  class="textbox" style="width:120px" onchange="cretaeOrderSubmit('');">
                                                                <option value="0">--Select--</option>
                                                                <c:forEach items="${productCategoryList}" var="proList">
                                                                    <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                                                </c:forEach>
                                                            </select>
                                                        </c:if>
                                                    </td>
                                                    <!--<td class="text1" colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label></td>-->
                                                    <td class="text1"><font color="red">*</font>Product Rate</td>
                                                    <td class="text1">
                                                        <select name="productRate" id="productRate" class="textbox" onchange="cretaeOrderSubmit(''); setReefer(this.value);">
                                                            <option value="0">--Select--</option>
                                                            <option value="1">Rate With Reefer</option>
                                                            <option value="2">Rate Without Reefer</option>
                                                        </select>
                                                    </td>
                                                    <script>
                                                        var productId = '<c:out value='${productCategoryId}'/>';
                                                        document.getElementById("productCategoryId").value = productId.split("~")[0];
                                                        document.getElementById("productRate").value = '<c:out value='${productRate}'/>';
                                                    </script>
                                                </tr>
                                                <tr>
                                                    <td class="text2">Commodity</td>
                                                    <td class="text2">
                                                        <input name="commodity" id="commodity" class="textbox" value="<c:out value='${commodity}'/>"  style="width:120px"/>
                                                    </td>
                                                    <td class="text2" colspan="2"></td>

                                                </tr>
                                            </table>

                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                                                <tr>
                                                    <td class="contentBar" colspan="6" ><b>Load Details</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addLoad" style="margin-top: -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>No. Of Pieces</td>
                                                                <!--<td class="contenthead" height="30" >Gross Wt</td>-->
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>UOM</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Length</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Breadth</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Height</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Chargeable Wt </td>
                                                                <td class="contenthead" height="30" style="width : 105px"><font color='red'>*</font>Volume(CBM)</td>
                                                            </tr>
                                                            <br>
                                                            <%int sno=1;%>
                                                            <c:if test="${consignmentArticles != null}">                                                                    
                                                                <c:forEach items="${consignmentArticles}" var="consignmentArticles">
                                                            <tr>
                                                                <td><%=sno%></td>
                                                                <td><input type="text" name="noOfPieces" id="noOfPieces<%=sno%>" value="<c:out value="${consignmentArticles.packageNos}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value="" style="width : 120px" onchange="getChargeableWeight(1); awbPackageRestrict();" onkeyup="checkEmpty(this); emptyTruckTable(); cretaeOrderSubmit(''); getChargeableWeight(1);"></td>
                                                                <!--<td><input type="text" name="grossWeight" id="grossWeight1" value="" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" ></td>-->
                                                                <td><select name="uom" id="uom<%=sno%>" style="width : 120px" onchange="getChargeableWeight(1)"><option value="1">Inch</option> <option value="2" selected >Cm</option></select> </td>
                                                                <!--                                        <td><input type="text" name="packagesNos" id="packagesNos1"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" onkeyup="calcTotalPacks(this.value)"></td>-->
                                                                <script>
                                                                        document.getElementById("uom<%=sno%>").value = '<c:out value="${consignmentArticles.unitOfMeasurement}"/>';
                                                                </script>
                                                                <td><input type="text" name="lengths" id="lengths<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${consignmentArticles.length}"/>"  style="width : 120px" onkeyup="checkEmpty(this); emptyTruckTable(); cretaeOrderSubmit(''); getChargeableWeight(1)" ></td>
                                                                <td><input type="text" name="widths" id="widths<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${consignmentArticles.breadth}"/>"  style="width : 120px" onkeyup="checkEmpty(this); emptyTruckTable(); cretaeOrderSubmit(''); getChargeableWeight(1)" ></td>
                                                                <td><input type="text" name="heights" id="heights<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${consignmentArticles.height}"/>"  style="width : 120px" onkeyup="checkEmpty(this); emptyTruckTable(); cretaeOrderSubmit(''); getChargeableWeight(1)" ></td>
                                                                <td>
                                                                    <input type="text" name="chargeableWeight" id="chargeableWeight<%=sno%>" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${consignmentArticles.chargeAbleWeigth}"/>" style="width : 120px" readonly >
                                                                    <input type="hidden" name="chargeableWeightId" id="chargeableWeightId<%=sno%>" value="">
                                                                    <input type="hidden" name="consignmentArticleId" id="consignmentArticleId<%=sno%>" value='<c:out value="${consignmentArticles.consignmentArticleId}"/>'>
                                                                </td>
                                                                <td><input type="text" name="volumes" id="volumes<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="<c:out value="${consignmentArticles.volume}"/>"  readonly="" style="width : 105px"></td>
                                                </tr>
                                                        <%++sno;%>
                                                    </c:forEach>
                                                </c:if>
                                                
                                                <script>
                                                                var cm = "6000";
                                                                var inch = "366";
                                                                var vol = "166.67";
                                                                function getChargeableWeight(val) {
//                                                                        alert("Hi I Am Here ................." + val);
                                                                var noOfPieces = document.getElementById("noOfPieces" + val).value;
                                                                var uom = document.getElementById("uom" + val).value;
                                                                var length = document.getElementById("lengths" + val).value;
                                                                var width = document.getElementById("widths" + val).value;
                                                                var height = document.getElementById("heights" + val).value;
                                                                var chargeableWeight = "";
                                                                var volumes = "";
                                                                if (length != "" && width != "" && height != "" && noOfPieces != "") {
                                                                var lbh = parseFloat(length) * parseFloat(width) * parseFloat(height);
                                                                var totalVolume = parseFloat(lbh) * parseInt(noOfPieces);
                                                                if (uom == 1) {
                                                                chargeableWeight = parseFloat(totalVolume) / parseFloat(inch);
                                                                volumes = parseFloat(chargeableWeight) / parseFloat(vol);
                                                                } else if (uom == 2) {
                                                                chargeableWeight = parseFloat(totalVolume) / parseFloat(cm);
                                                                volumes = parseFloat(chargeableWeight) / parseFloat(vol);
                                                                }
                                                                document.getElementById("chargeableWeight" + val).value = (chargeableWeight).toFixed(2);
                                                                document.getElementById("volumes" + val).value = (volumes).toFixed(2);
                                                                }

                                                                getChargeableWeightCalc(val);
                                                                }

                                                                function getChargeableWeightCalc(val) {
//                                                                alert("Hi Iam Here   "+ val);
                                                                var noOfPieces = 'noOfPieces' + val;
                                                                var grossWeight = 'grossWeight' + val;
//                                                                        $("#" + noOfPieces).keyup(function() {
                                                                var noOfPiecesValue = document.getElementsByName("noOfPieces");
                                                                var totVal = 0;
                                                                for (var i = 0; i < noOfPiecesValue.length; i++) {
                                                                if (noOfPiecesValue[i].value != '') {
                                                                totVal += parseInt(noOfPiecesValue[i].value);
                                                                }
                                                                }
//                                                                alert("totVal" + totVal);
                                                                $("#totalPackages").text(totVal);
                                                                $("#totalPackage").val(totVal);
//                                                                });
                                                                var chargeableWeight = document.getElementsByName("chargeableWeight");
                                                                var uom = document.getElementsByName("uom");
                                                                var totVal = 0;
                                                                for (var i = 0; i < chargeableWeight.length; i++) {
                                                                if (chargeableWeight[i].value != '') {
                                                                totVal += parseFloat(chargeableWeight[i].value);
                                                                }
                                                                }
                                                                $("#totalChargeableWeight").text(totVal.toFixed(2));
                                                                $("#totalChargeableWeights").val(totVal.toFixed(2));
                                                                var volumes = document.getElementsByName("volumes");
                                                                var totVal = 0;
                                                                for (var i = 0; i < volumes.length; i++) {
                                                                if (volumes[i].value != '') {
                                                                totVal += parseFloat(volumes[i].value);
                                                                }
                                                                }
                                                                $("#totalVolumes").text(totVal.toFixed(2));
                                                                $("#totalVolume").val(totVal.toFixed(2));
                                                                $("#totalVolumeActual").val(totVal.toFixed(2));
                                                                }

                                                            </script>
                                            </table>
                                            <script>
                                                $(document).ready(function () {
                                                    var loadCnt = document.getElementById("loadCount").value;
                                                    //alert(cnt);
                                                    $("#load_add").click(function () {
                                                        $('#addLoad tr').last().after('<tr><td>' + loadCnt + '</td><td><input type="text" name="noOfPieces" id="noOfPieces' + loadCnt + '" value="" class="textbox" style="width : 120px" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="checkEmpty(this);emptyTruckTable();cretaeOrderSubmit(' + "" + ');getChargeableWeight(' + loadCnt + ')" onchange="getChargeableWeight(' + loadCnt + ');awbPackageRestrict();" ></td><td><select name="uom" id="uom' + loadCnt + '" style="width : 120px" onchange="getChargeableWeight(' + loadCnt + ')" ><option value="1">Inch</option> <option value="2" selected>Cm</option></select> </td><td><input type="text" name="lengths" id="lengths' + loadCnt + '"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value=""  style="width : 120px" onkeyup="checkEmpty(this);emptyTruckTable();cretaeOrderSubmit(' + "" + ');getChargeableWeight(' + loadCnt + ')" ></td><td><input type="text" name="widths" id="widths' + loadCnt + '"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" style="width : 120px" onkeyup="checkEmpty(this);emptyTruckTable();cretaeOrderSubmit(' + "" + ');getChargeableWeight(' + loadCnt + ')" ></td><td><input type="text" name="heights" id="heights' + loadCnt + '"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" style="width : 120px" onkeyup="checkEmpty(this);emptyTruckTable();cretaeOrderSubmit(' + "" + ');getChargeableWeight(' + loadCnt + ')" ></td><td><input type="text" name="chargeableWeight" id="chargeableWeight' + loadCnt + '" value="" style="width : 120px" class="textbox"  readonly><input type="hidden" name="chargeableWeightId" id="chargeableWeightId' + loadCnt + '" value="" ><input type="hidden" name="consignmentArticleId" id="consignmentArticleId' + loadCnt + '" value="" ></td><td><input type="text" name="volumes" id="volumes' + loadCnt + '"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value=""  style="width : 105px" readonly=""></td>');
                                                        loadCnt++;
                                                    });

                                                    $("#load_rem").click(function () {
                                                        if ($('#addLoad tr').size() > 2) {
                                                            $('#addLoad').each(function () {
//                                                                $('#addLoad tr:last-child').remove();
                                                                $('#addLoad tr:last').remove();
                                                            });
                                                            var noOfPiecesValue = document.getElementsByName("noOfPieces");
                                                            var totVal = 0;
                                                            for (var i = 0; i < noOfPiecesValue.length; i++) {
                                                                if (noOfPiecesValue[i].value != '') {
                                                                    totVal += parseInt(noOfPiecesValue[i].value);
                                                                }
                                                            }
                                                            $("#totalPackages").text(totVal);
                                                            $("#totalPackage").val(totVal);


                                                            var chargeableWeight = document.getElementsByName("chargeableWeight");
                                                            var totVal = 0;
                                                            for (var i = 0; i < chargeableWeight.length; i++) {
                                                                if (chargeableWeight[i].value != '') {
                                                                    totVal += parseFloat(chargeableWeight[i].value);
                                                                }
                                                            }
                                                            var grossWeight = document.getElementById("totalWeightage").value;
                                                            var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                            if (parseInt(totVal) > parseInt(grossWeight)) {
                                                                $("#totalChargeableWeight").text(totVal.toFixed(2));
                                                                $("#totalChargeableWeights").val(totVal.toFixed(2));
                                                                reValidateAvailableWeightNew();
                                                            } else {
//                                                            if (parseInt(totalChargeableWeights) > parseInt(grossWeight)) {
//                                                                $("#totalChargeableWeight").text(totalChargeableWeights);
//                                                                $("#totalChargeableWeights").val(totalChargeableWeights);
//                                                            } else {
//                                                                alert("grossWeight "+grossWeight);
                                                                $("#totalChargeableWeight").text(grossWeight);
                                                                $("#totalChargeableWeights").val(grossWeight);
//                                                            }
                                                            }

                                                            var volumes = document.getElementsByName("volumes");
                                                            var totVal = 0;
                                                            for (var i = 0; i < volumes.length; i++) {
                                                                if (volumes[i].value != '') {
                                                                    totVal += parseFloat(volumes[i].value);
                                                                }
                                                            }
                                                            $("#totalVolumes").text(totVal);
                                                            $("#totalVolume").val(totVal);


                                                            loadCnt = loadCnt - 1;
                                                        } else {
                                                            alert('Not Allowed To Delete Rows');
                                                        }
                                                    });

                                                });
                                            </script>
                                            </td>

                                            </tr>
                                            <tr>
                                                <td colspan="1">
                                                    <a href="javascript:void(0);cretaeOrderSubmit('');" id='load_add'><input type="button" class="button" value="Add Row"/></a>
                                                    <a href="javascript:void(0);cretaeOrderSubmit('');" id="load_rem"><input type="button" class="button" value="Remove Row"/></a>
                                                </td>
                                            </tr>
                                            </table>
                                            <input type="hidden" name="loadCount" id="loadCount" value="<%=sno%>"/>
                                            <br>
                                            <table>
                                                <tr>

                                                <script type="text/javascript">
                                                    function checkGrossWeight() {
                                                    var totalWeightage = document.getElementById("totalWeightage").value;
                                                    document.getElementById("remainWeight").value = totalWeightage;
                                                    var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                    if (parseFloat(totalWeightage) < parseFloat(totalChargeableWeights)) {
                                                    document.getElementById("totalChargeableWeights").value = totalChargeableWeights;
                                                    } else {
                                                    document.getElementById("totalChargeableWeights").value = totalWeightage;
                                                    }
                                                    var volume = parseFloat(document.getElementById("totalChargeableWeights").value) / 166.67;
                                                    $("#totalVolumes").text(parseFloat(volume).toFixed(2));
                                                    document.getElementById("totalVolume").value = parseFloat(volume).toFixed(2);
                                                    document.getElementById("totalVolumeActual").value = parseFloat(volume).toFixed(2);
                                                    var awbOriginId = document.getElementById("awbOriginId").value;
                                                    var awbDestinationId = document.getElementById("awbDestinationId").value;
                                                    var customerId = document.getElementById("customerId").value;
                                                    var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                    var productRate = document.getElementById("productRate").value;
                                                    //alert("awbOriginId"+awbOriginId+"awbDestinationId"+awbDestinationId+"customerId"+customerId+"chargableWeight"+chargableWeight+"productRate"+productRate);
                                                    $.ajax({
                                                    url: '/throttle/getChargeableRateValue.do',
                                                            data: {customerId: customerId, chargeableWeight: chargableWeight, awbOriginId: awbOriginId, awbDestinationId: awbDestinationId, productRate: productRate},
                                                            dataType: 'json',
                                                            success: function(data) {
                                                            if (data == '' || data == null) {
                                                            alert('No contract for this chargeable weight. please check.');
                                                            } else {
                                                            $.each(data, function(i, data) {//alert(data.Name);
                                                            document.getElementById("rateValue").value = data.Name;
                                                            document.getElementById("totFreightAmount").value = data.Name;
                                                            document.getElementById("perKgAutoRate").value = data.perKgAutoRate;
//                                                                                    alert("perKgAutoRate " + data.perKgAutoRate);
                                                            });
                                                            }
                                                            }
                                                    });
                                                    }
                                                    function calculateToChargebleWeight(){
                                                    var totalWeightage = document.getElementById("totalWeightage").value;
                                                    var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                    if (parseFloat(totalWeightage) < parseFloat(totalChargeableWeights)) {
                                                    document.getElementById("totalChargeableWeights").value = totalChargeableWeights;
                                                    } else {
                                                    document.getElementById("totalChargeableWeights").value = totalWeightage;
                                                    }
                                                    var volume = parseFloat(document.getElementById("totalChargeableWeights").value) / 166.67;
                                                    $("#totalVolumes").text(parseFloat(volume).toFixed(2));
                                                    document.getElementById("totalVolume").value = parseFloat(volume).toFixed(2); ;
                                                    document.getElementById("totalVolumeActual").value = parseFloat(volume).toFixed(2); ;
                                                    }

                                                    function checkEmpty(obj){
                                                    if (obj.value == ''){
                                                    obj.value = 0;
                                                    } else{
                                                    var myString = obj.value;
                                                    var result = (myString[0] == '0') ? myString.substr(1) : myString;
                                                    obj.value = result;
                                                    }
                                                    }
                                                </script>

                                                <td>
                                                    <label class="contentsub">Pcs</label>
                                                    <label id="totalPackages"><c:out value="${totalPackages}"/></label>
                                                    <input type="hidden" id="totalPackage" name="totalPackage" value="<c:out value="${totalPackages}"/>"/>
                                                </td>
                                                <td>
                                                    <label class="contentsub">Gross Weight (Kg)</label>
                                                    <!--<label id="totalWeight">0</label>-->
                                                    <input type="text" id="totalWeightage" name="totalWeightage" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" style="width : 120px" onkeyup="checkEmpty(this); cretaeOrderSubmit(''); emptyTruckTable();checkGrossWeight(); reValidateAvailableWeight();" onchange="calculateToChargebleWeight();" value="<c:out value="${totalWeight}"/>" />
                                                    <input type="hidden" name="remainWeight" id="remainWeight" value="0">

                                                </td>
                                                <td>
                                                    <label class="contentsub">Chargeable Weight (Kg)</label>
                                                    <label id="totalChargeableWeight" style="display : none"><c:out value="${chargeAbleWeigth}"/></label>
                                                    <input type="text" id="totalChargeableWeights" id="totalChargeableWeights" name="totalChargeableWeights" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="checkEmpty(this);cretaeOrderSubmit(''); emptyTruckTable();" onchange="cretaeOrderSubmit(''); calcTotalChargeableWeights(); calculateToChargebleWeight();" value="<c:out value="${chargeAbleWeigth}"/>"/>
                                                </td>
                                                <td>
                                                    <label class="contentsub">Volume(CBM)</label>
                                                    <input type="text" id="totalVolume" value="<c:out value="${totalVolume}"/>" name="totalVolume" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="checkEmpty(this);cretaeOrderSubmit(''); emptyTruckTable();" />
                                                    <label id="totalVolumes"><c:out value="${totalVolume}"/></label>
                                                    <input type="hidden" id="totalVolumeActual" value="" name="totalVolumeActual" />
                                                </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" >

                                                <tr>
                                                    <td class="contentBar" colspan="6" ><b>Dangerous Goods</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="1" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="tbl1" style="margin-top : -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 120px">DG Handling Code</td>
                                                                <td class="contenthead" height="30" style="width : 120px">UN /ID Number</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Class Code</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Pkg Instruction</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Pkg Group</td>
                                                                <td class="contenthead" height="30" style="width : 95px">Net Quantity</td>
                                                                <td class="contenthead" height="30" style="width : 95px">Net Units</td>
                                                            </tr>
                                                            <br>

                                                            <tr>
                                                                <td>1</td>
                                                                <td><input type="text" name="dgHandlingCode" id="dgHandlingCode1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="unIdNo" id="unIdNo1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="classCode" id="classCode1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="pkgInstruction" id="pkgInstruction1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="pkgGroup" id="pkgGroup1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="netQuantity" id="netQuantity1" value="" style="width : 95px"></td>
                                                                <td><input type="text" name="netUnits" id="netUnits1" value="" style="width : 95px"></td>
                                                            </tr>
                                                        </table>
                                                        <script>
                                                            $(document).ready(function() {
                                                            var cnt = 2;
                                                            //alert(cnt);
                                                            $("#anc_add").click(function() {
                                                            $('#tbl1 tr').last().after('<tr><td>' + cnt + '</td><td><input type="text" name="dgHandlingCode" id="dgHandlingCode' + cnt + '" value="" style="width : 120px"></td><td><input type="text" name="unIdNo" id="unIdNo' + cnt + '" value="" style="width : 120px"></td><td><input type="text" name="classCode" id="classCode' + cnt + '" value="" style="width : 120px"></td><td><input type="text" name="pkgInstruction" id="pkgInstruction' + cnt + '" value="" style="width : 120px"></td><td><input type="text" name="pkgGroup" id="pkgGroup' + cnt + '" value="" style="width : 120px"></td><td><input type="text" name="netQuantity" id="netQuantity' + cnt + '" value="" style="width : 95px"></td><td><input type="text" name="netUnits" id="netUnits' + cnt + '" value="" style="width : 95px"></td></tr>');
                                                            cnt++;
                                                            });
                                                            $("#anc_rem").click(function() {
                                                            if ($('#tbl1 tr').size() > 2) {
                                                            $('#tbl1 tr:last-child').remove();
                                                            cnt = cnt - 1;
                                                            } else {
                                                            alert('One row should be present in table');
                                                            }
                                                            });
                                                            });</script>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <a href="javascript:void(0);" id='anc_add'><input type="button" class="button" value="Add Row"/></a>
                                                        <a href="javascript:void(0);" id="anc_rem"><input type="button" class="button" value="Remove Row"/></a></td>

                                                </tr>
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" >
                                                <tr>
                                                    <td class="contentBar" id="routeInfo"><b>Truck Information,  Route Information()</b></td>

                                                </tr>
                                                <tr>
                                                    <td class="contentBar" colspan="3" align="center">
                                                        <table  border="0" class="border" align="left" width="auto" cellpadding="0" cellspacing="0" id="bg" >
                                                            <tr style="display:block;" align="left">
                                                                <td class="contenthead" style="width: 100px;" align="center" ><b>Available Capacity:&nbsp;&nbsp;</b><label id="availableCapacity">0</label></td>
                                                                <td class="contenthead" style="width: 100px;" align="center" ><b>Available Volume:&nbsp;&nbsp;</b><label id="availableVolume">0</label></td>
                                                                <td class="contenthead" style="width: 100px;" align="center"><b>Loaded Capacity:&nbsp;&nbsp;</b><label id="assignedCapacity">0</label></td>
                                                                <td class="contenthead" style="width: 100px;" align="center"><b>Loaded CBM:&nbsp;&nbsp;</b><label id="loadedVolume">0</label></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <table border="1" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="truckTable" style="margin-top : -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 70px">Truck Dep. Date</td>
                                                                <td class="contenthead" height="30" >Fleet Type</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Fleet Code</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Loaded Weight(KG)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Loaded Volume(CBM)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Available Weight(KG)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Available Volume(CBM)</td>
                                                                <td  class="contenthead"  height="30" style="width: 95px">Total Pieces</td>
                                                            </tr>
                                                            <br>
                                                            <%int truckRowNo=1;%>
                                                            <c:if test="${consignmentPoint !=null}">
                                                                <c:forEach items="${consignmentPoint}" var="consignmentPoint">
                                                            <tr>
                                                                <input type="hidden" name="consignmentRouteCourseId" id="consignmentRouteCourseId<%=truckRowNo%>"  value='<c:out value="${consignmentPoint.consignmentRouteCourseId}"/>'>
                                                                <td><%=truckRowNo%></td>
                                                                <td><input type="text" class="datepicker" name="truckDepDate" id="truckDepDate<%=truckRowNo%>" value="<c:out value="${consignmentPoint.departOnTime}"/>" style="width : 70px" onchange="cretaeOrderSubmit('');setAvailableVolume(document.getElementById('vehicleNo<%=truckRowNo%>').value,<%=truckRowNo%>)" readonly></td>
                                                                <input type="hidden" name="truckOriginId" id="truckOriginId<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityFromId}"/>'>
                                                                <input type="hidden" name="truckOriginName" id="truckOriginName<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityFromName}"/>' >


                                                                <input type="hidden" name="truckDestinationId" id="truckDestinationId<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityToId}"/>'>
                                                                <input type="hidden" name="truckDestinationName" id="truckDestinationName<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityToName}"/>'>

                                                                <input type="hidden" name="truckRouteId" id="truckRouteId<%=truckRowNo%>" value="<c:out value="${consignmentPoint.routeId}"/>">
                                                                <input type="hidden" name="truckTravelKm" id="truckTravelKm<%=truckRowNo%>" value="<c:out value="${consignmentPoint.distance}"/>" style="width: 60px">


                                                                <input type="hidden" name="truckTravelHour" id="truckTravelHour<%=truckRowNo%>" value="<c:out value="${consignmentPoint.travelHour}"/>" style="width: 60px">


                                                                <input type="hidden" name="truckTravelMinute" id="truckTravelMinute<%=truckRowNo%>" value="<c:out value="${consignmentPoint.travelMinute}"/>" style="width: 60px">

                                                            
                                                            <script>

                                                                function setVehicleNo(str, val) {
//                                                   //alert(val);
//                                                   alert(str);
                                                                document.getElementById("vehicleTypeId").value = str;
                                                                $('#vehicleNo' + val).empty();
                                                                $('#vehicleNo' + val).append(
                                                                        $('<option></option>').attr("value", '0').text('--select--'));
                                                                var vehilceNo = "vehicleNo" + val;
                                                                var truckOriginId = "truckOriginId1";
                                                                var truckDestinationId = "truckDestinationId1";
                                                                var vehicleId = "vehicleId" + val;
                                                                var fleetTypeId = "fleetTypeId" + val;
//                                                                        alert($("#" + fleetTypeId).val());
                                                                var truckAdd = "truck_add" + val;
                                                                $.ajax({
                                                                url: '/throttle/handleVehicleTypeVehicleRegNo.do',
                                                                        data: {vehicleRegNo:vehilceNo, vehicleTypeId:$("#" + fleetTypeId).val(), textBox:1, truckOriginId:$("#" + truckOriginId).val(), truckDestinationId:$("#" + truckDestinationId).val()},
                                                                        dataType: 'json',
                                                                        success: function(data) {
                                                                        if (data == '' || data == null) {
                                                                        alert('Truck Information Not There');
                                                                        $('#vehicleNo' + val).empty();
                                                                        $('#vehicleNo' + val).append(
                                                                                $('<option></option>').attr("value", '0').text('--select--'))
                                                                        } else {
                                                                        //alert(data);
                                                                        $.each(data, function(i, data) {
                                                                        if (data.Id != "" & data.Id != null){
                                                                        $('#vehicleNo' + val).append(
                                                                                $('<option style="width:90px"></option>').attr("value", data.Id + "-" + data.Name).text(data.Name)
                                                                                )

                                                                        }
                                                                        });
                                                                        }
                                                                        }
                                                                });
                                                                cretaeOrderSubmit('');
                                                                }

                                                            </script>
                                                            <td>
                                                                <select name="fleetTypeId" id="fleetTypeId<%=truckRowNo%>" onchange="setVehicleNo(this.value, '<%=truckRowNo%>');">
                                                                    <option value="0">--select--</option>
                                                                    <c:if test="${vehicleTypeList != null}">
                                                                        <c:forEach items="${vehicleTypeList}" var="vehType">
                                                                            <option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                                <c:if test="${vehicleTypeList != null}">
                                                                    <c:forEach items="${vehicleTypeList}" var="vehType">
                                                                        <input type="hidden" name="vehicleTypeNameNew" id="vehicleTypeNameNew<c:out value='${vehType.vehicleTypeId}'/>" style="width:95px"   maxlength="15" value='<c:out value="${vehType.vehicleTypeName}"/>'>           
                                                                        <input type="hidden" name="vehicleTonnage" id="vehicleTonnage<c:out value='${vehType.vehicleTypeId}'/>" style="width:95px"   maxlength="15" value='<c:out value="${vehType.vehicleTonnage}"/>'>           
                                                                        <input type="hidden" name="vehicleCapacity" id="vehicleCapacity<c:out value='${vehType.vehicleTypeId}'/>" style="width:95px"   maxlength="15" value='<c:out value="${vehType.vehicleCapacity}"/>'>           
                                                                    </c:forEach>
                                                                </c:if>
                                                                <script>
                                                                    document.getElementById("fleetTypeId"+<%=truckRowNo%>).value='<c:out value='${consignmentPoint.fleetTruckId}'/>';
                                                                </script>    
                                                            </td>
                                                            <td>
                                                                <select name="vehicleNo"   id="vehicleNo<%=truckRowNo%>" style="width:95px" onchange="cretaeOrderSubmit(''); setAvailableVolume(this.value,<%=truckRowNo%>)">
                                                                    <option value='<c:out value="${consignmentPoint.vehicleID}"/>-<c:out value="${consignmentPoint.vehicleNo}"/>'><c:out value="${consignmentPoint.vehicleNo}"/></option>
                                                                </select>
                                                                <input type="hidden" name="vehicleNogg" id="vehicleNo11<%=truckRowNo%>" style="width:95px"  value='<c:out value="${consignmentPoint.vehicleNo}"/>' >
                                                                <input type="hidden" name="vehicleId" id="vehicleId<%=truckRowNo%>" style="width:95px" value='<c:out value="${consignmentPoint.vehicleID}"/>' >
                                                                <input type="hidden" name="usedVehicleStatus" id="usedVehicleStatus<%=truckRowNo%>" style="width:95px" value='0' >
                                                            </td>
                                                            <input type="hidden" name="vehicleRegNo" id="vehicleRegNo<%=truckRowNo%>" style="width:95px"   maxlength="15" value='0'>
                                                            <td>
                                                                <input type="text" name="usedCapacity" id="usedCapacity<%=truckRowNo%>" style="width:95px" value="<c:out value="${consignmentPoint.truckUsedCapacity}"/>" onkeyup="cretaeOrderSubmit(''); setUsedCapacity(<%=truckRowNo%>);">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="usedVol" id="usedVol<%=truckRowNo%>" style="width:95px" value="<c:out value="${consignmentPoint.truckUsedVol}"/>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="availableCap" id="availableCap<%=truckRowNo%>" readonly style="width:95px" value="<c:out value="${consignmentPoint.truckAvailable}"/>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="assignedCap" id="assignedCap<%=truckRowNo%>" readonly style="width:95px" value="<c:out value="${consignmentPoint.truckAssigned}"/>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="receivedPackages" id="receivedPackages<%=truckRowNo%>" style="width:95px" onkeyup="cretaeOrderSubmit('');" value="<c:out value="${consignmentPoint.receivedPackage}"/>">
                                                            </td>
                                                            <script>
                                                                        handleVehicleTypeVehicleRegNo(<%=truckRowNo%>);
                                                                        function handleVehicleTypeVehicleRegNo(val) {

                                                                            var vehilceNo = "vehicleNo" + val;
                                                                            var truckOriginId = "truckOriginId1";
                                                                            var truckDestinationId = "truckDestinationId1";
                                                                            var vehicleId = "vehicleId" + val;
                                                                            var fleetTypeId = "fleetTypeId" + val;
                                                                            var truckAdd = "truck_add" + val;

                                                                            $('#' + vehilceNo).autocomplete({
                                                                                source: function (request, response) {
                                                                                    $.ajax({
                                                                                        url: "/throttle/handleVehicleTypeVehicleRegNo.do",
                                                                                        dataType: "json",
                                                                                        data: {
                                                                                            vehicleRegNo: request.term,
                                                                                            vehicleTypeId: $("#" + fleetTypeId).val(),
                                                                                            textBox: 1,
                                                                                            truckOriginId: $("#" + truckOriginId).val(),
                                                                                            truckDestinationId: $("#" + truckDestinationId).val()
                                                                                        },
                                                                                        success: function (data, textStatus, jqXHR) {
                                                                                            var items = data;
                                                                                            response(items);
                                                                                        },
                                                                                        error: function (data, type) {

                                                                                            //console.log(type);
                                                                                        }
                                                                                    });
                                                                                },
                                                                                minLength: 1,
                                                                                select: function (event, ui) {
                                                                                    var value = ui.item.Name;
                                                                                    var id = ui.item.Id;
                                                                                    //alert(id+" : "+value);
                                                                                    $('#' + vehicleId).val(id);
                                                                                    $('#' + vehilceNo).val(value);
                                                                                    //$('#' + truckAdd).focus();
                                                                                    //validateRoute(val,value);

                                                                                    return false;
                                                                                }

                                                                                // Format the list menu output of the autocomplete
                                                                            }).data("autocomplete")._renderItem = function (ul, item) {
                                                                                //alert(item);
                                                                                var itemVal = item.Name;

                                                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                                                return $("<li></li>")
                                                                                        .data("item.autocomplete", item)
                                                                                        .append("<a>" + itemVal + "</a>")
                                                                                        .appendTo(ul);
                                                                            };
                                                                        }
                                                                    </script>   
                                                </tr>
                                                <c:if test="${consignmentPoint.fleetStatus == '1'}">
                                                    <script>
                                                        document.getElementById("truckDepDate" +<%=truckRowNo%>).d = true;
                                                        document.getElementById("fleetTypeId" +<%=truckRowNo%>).disabled = true;
                                                        document.getElementById("vehicleNo" +<%=truckRowNo%>).disabled = true;
                                                        document.getElementById("usedCapacity" +<%=truckRowNo%>).readOnly = true;
                                                        document.getElementById("usedVol" +<%=truckRowNo%>).readOnly = true;
                                                        document.getElementById("availableCap" +<%=truckRowNo%>).readOnly = true;
                                                        document.getElementById("assignedCap" +<%=truckRowNo%>).readOnly = true;
                                                        document.getElementById("receivedPackages" +<%=truckRowNo%>).readOnly = false;
                                                    </script>
                                                </c:if> 
                                                <%++truckRowNo;%>
                                                    </c:forEach>
                                                </c:if>
                                            </table>
                                              <input type="hidden" name="truckCount" id="truckCount" value="<%=truckRowNo-1%>"/>
                                              
                                            <script>
                                                callOriginAjax(1);
                                                callDestinationAjax(1);
    //                                                                callOriginCodeAjax(1);
    //                                                                callDestinationCodeAjax(1);
                                                function setReefer(val){
                                                if (val == 1){
                                                document.getElementById("reeferRequired").value = "Yes";
                                                }
                                                if (val == 2){document.getElementById("reeferRequired").value = "No"; }
                                                }
                                                function callOriginAjax(val) {
                                                // Use th
                                                // e .autocomplete() method to compile the list based on input from user
                                                // alert(val);
                                                var pointNameId = 'truckOriginName' + val;
                                                var pointIdId = 'truckOriginId' + val;
                                                var desPointName = 'truckDestinationName' + val;
                                                var desPointId = 'truckDestinationId' + val;
                                                //alert(prevPointId);
                                                $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                        cityName: request.term,
                                                                textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                        var items = data;
                                                        response(items);
                                                        },
                                                        error: function(data, type) {

                                                        //console.log(type);
                                                        }
                                                });
                                                },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                        var value = ui.item.Name;
                                                        var id = ui.item.Id;
    //                                                                                var cityCode = ui.item.cityCode;
                                                        //alert(id+" : "+value);
                                                        $('#' + pointIdId).val(id);
                                                        $('#' + pointNameId).val(value);
                                                        $('#awbOriginId').val(id);
                                                        $('#awbOrigin').val(value);
    //                                                                                $('#awbOriginRegion').val(cityCode);
                                                        document.getElementById("routeInfo").innerHTML = "<b>Truck Information Route Information (" + document.getElementById("awbOrigin").value + " - " + document.getElementById("awbDestination").value + ")<b>";
                                                        $('#' + desPointName).focus();
                                                        $('#' + pointNameId).removeClass('errorClass');
                                                        $('#' + pointNameId).addClass('noErrorClass');
                                                        $('#' + desPointName).val('');
                                                        $('#' + desPointId).val('');
                                                        cretaeOrderSubmit('');
                                                        return false;
                                                        }

                                                // Format the list menu output of the autocomplete
                                                }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                                };
                                                }

                                                function callDestinationAjax(val) {
                                                // Use the .autocomplete() method to compile the list based on input from user
                                                //alert(val);
                                                var pointNameId = 'truckDestinationName' + val;
                                                var pointIdId = 'truckDestinationId' + val;
                                                var originPointId = 'truckOriginId' + val;
                                                var truckRouteId = 'truckRouteId' + val;
                                                var travelKm = 'truckTravelKm' + val;
                                                var travelHour = 'truckTravelHour' + val;
                                                var travelMinute = 'truckTravelMinute' + val;
                                                var fleetType = 'fleetTypeId' + val;
                                                //alert(prevPointId);
                                                $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                        cityName: request.term,
                                                                originCityId: $("#" + originPointId).val(),
                                                                textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                        var items = data;
                                                        response(items);
                                                        },
                                                        error: function(data, type) {

                                                        //console.log(type);
                                                        }
                                                });
                                                },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                        var value = ui.item.Name;
                                                        var id = ui.item.Id;
    //                                                                                var cityCode = ui.item.cityCode;
                                                        //alert(id+" : "+value);
                                                        $('#' + pointIdId).val(id);
                                                        $('#awbDestinationId').val(id);
                                                        $('#' + pointNameId).val(value);
                                                        $('#awbDestination').val(value);
    //                                                                                $('#awbDestinationRegion').val(cityCode);
                                                        $('#' + travelKm).val(ui.item.TravelKm);
                                                        $('#' + travelHour).val(ui.item.TravelHour);
                                                        $('#' + travelMinute).val(ui.item.TravelMinute);
                                                        $('#' + truckRouteId).val(ui.item.RouteId);
                                                        document.getElementById("totalKm").value = ui.item.TravelKm;
                                                        document.getElementById("totalHours").value = ui.item.TravelHour;
                                                        document.getElementById("totalMinutes").value = ui.item.TravelMinute;
                                                        document.getElementById("routeId").value = ui.item.RouteId;
                                                        document.getElementById("routeInfo").innerHTML = "<b> Truck Information Route Information (" + document.getElementById("awbOrigin").value + " - " + document.getElementById("awbDestination").value + ") Travel KM :" + ui.item.TravelKm + ",Travel Hours :" + ui.item.TravelHour + ",Travel Minutes :" + ui.item.TravelMinute + "</b>";
                                                        // $('#' + fleetType).focus();
                                                        $('#awbOrderDeliveryDate').focus();
                                                        $('#' + pointNameId).removeClass('errorClass');
                                                        $('#' + pointNameId).addClass('noErrorClass');
                                                        cretaeOrderSubmit('');
                                                        return false;
                                                        }

                                                // Format the list menu output of the autocomplete
                                                }).data("autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                                };
                                                }

                                            </script>
                                            <script>

                                                function emptyTruckTable(){
                                                var fleetTypeIds = document.getElementsByName('fleetTypeId');
                                                if (fleetTypeIds.length >= 2){
                                                for (var i = fleetTypeIds.length; i > 1; i--) {
                                                $("#truckDepDate" + i).val('');
                                                $("#fleetTypeId" + i).val('0');
                                                $("#vehicleNo" + i).val('0');
                                                $("#usedCapacity" + i).val('0');
                                                $("#usedVol" + i).val('0');
                                                $("#availableCap" + i).val('0');
                                                $("#assignedCap" + i).val('0');
                                                $("#receivedPackages" + i).val('0');
                                                if ($('#truckTable tr').size() > i) {//alert(truckCnt);
                                                var removeWeight = document.getElementById("usedCapacity" + i).value;
                                                var removeVolume = document.getElementById("usedVol" + i).value;
                                                var availablecapcity = document.getElementById("availableCapacity").innerHTML;
                                                var assignedCapacity = document.getElementById("assignedCapacity").innerHTML;
                                                var availableVolume = document.getElementById("availableVolume").innerHTML;
                                                //   alert(removeWeight);
                                                document.getElementById("availableCapacity").innerHTML = parseFloat(parseFloat(availablecapcity) + parseFloat(removeWeight)).toFixed(2);
                                                document.getElementById("assignedCapacity").innerHTML = parseFloat(parseFloat(assignedCapacity) - parseFloat(removeWeight)).toFixed(2);
                                                document.getElementById("availableVolume").innerHTML = parseFloat(parseFloat(availableVolume) + parseFloat(removeVolume)).toFixed(2);
                                                document.getElementById("loadedVolume").innerHTML = parseFloat(parseFloat(availableVolume) - parseFloat(removeVolume)).toFixed(2);
                                                var remainWeight = document.getElementById("remainWeight").value;
                                                remainWeight = parseInt(remainWeight) + parseInt(removeWeight);
                                                document.getElementById("remainWeight").value = remainWeight;
                                                $('#truckTable tr:last-child').remove();
                                                }

                                                }
                                                var i = 1;
                                                $("#truckDepDate" + i).val('');
                                                $("#fleetTypeId" + i).val('0');
                                                $("#vehicleNo" + i).val('0');
                                                $("#usedCapacity" + i).val('0');
                                                $("#usedVol" + i).val('0');
                                                $("#availableCap" + i).val('0');
                                                $("#assignedCap" + i).val('0');
                                                $("#receivedPackages" + i).val('0');
                                                document.getElementById("availableCapacity").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("assignedCapacity").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("availableVolume").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("loadedVolume").innerHTML = parseFloat(0).toFixed(2);
                                                } else{
                                                var i = 1;
                                                $("#truckDepDate" + i).val('');
                                                $("#fleetTypeId" + i).val('0');
                                                $("#vehicleNo" + i).val('0');
                                                $("#usedCapacity" + i).val('0');
                                                $("#usedVol" + i).val('0');
                                                $("#availableCap" + i).val('0');
                                                $("#assignedCap" + i).val('0');
                                                $("#receivedPackages" + i).val('0');
                                                document.getElementById("availableCapacity").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("assignedCapacity").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("availableVolume").innerHTML = parseFloat(0).toFixed(2);
                                                document.getElementById("loadedVolume").innerHTML = parseFloat(0).toFixed(2);
                                                }

                                                }

                                                function setUsedCapacity(val){
//                                                if (parseInt(document.getElementById("shipmentType").value) == 2){
                                                var usdCap = document.getElementById("usedCapacity" + val).value;
                                                var usdCapTemp = document.getElementById("usedCapacity" + val).value;
                                                var totalWeightage = document.getElementById("totalWeightage").value;
                                                var usedCapacity = document.getElementsByName("usedCapacity");
                                                var value = 0;
                                                for (var i = 1; i <= usedCapacity.length; i++){
                                                value = parseFloat(value) + parseFloat(document.getElementById("usedCapacity" + i).value);
                                                }
                                                remainWeight = parseInt(totalWeightage) - parseInt(value);
                                                document.getElementById("remainWeight").value = remainWeight;
                                                var vol = "166.67";
                                                var totalVolume = parseInt(usdCap) / parseFloat(vol);
                                                var varray = document.getElementById("vehicleNo" + val).value.split("-");
                                                document.getElementById("vehicleId" + val).value = varray[0];
                                                var vehicleId = varray[0];
                                                var vehicleNo = varray[0];
                                                truckCapacity = usdCap;
                                                truckVol = totalVolume;
                                                var availCapacity = 0;
                                                var truckDepDate = document.getElementById("truckDepDate" + val).value;
                                                var fleetTypeId = document.getElementById("fleetTypeId" + val).value;
                                                var deptTag = document.getElementsByName("truckDepDate");
                                                var availCapacity = parseFloat(totalWeightage) - parseFloat(value);
//                                                alert("truckCapacity == "+truckCapacity);
                                                if (parseFloat(availCapacity) >= 0) {
                                                $.ajax({
                                                url: '/throttle/getAvailableWeightAndVolume.do',
                                                        data: {usdCapTemp:usdCapTemp, vehicleId: vehicleId, truckDepDate: truckDepDate, truckCapacity: truckCapacity, truckVol: truckVol, fleetTypeId: fleetTypeId},
                                                        dataType: 'json',
                                                        success: function(data) {
                                                        if (data == '' || data == null) {

                                                        } else {

                                                        $.each(data, function(i, data) {
                                                        if (data.Id != "" & data.Id != null){
                                                        var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                        var chargableWeight = document.getElementById("totalWeightage").value;
                                                        var totalVolume = document.getElementById("totalVolume").value;
                                                        document.getElementById("availableCap" + val).value = parseFloat(data.Name).toFixed(2);
                                                        document.getElementById("assignedCap" + val).value = parseFloat(data.Id).toFixed(2);
//                                                                                document.getElementById("availableCap" + val).value = Math.round(parseFloat(data.Name));
//                                                                                document.getElementById("assignedCap" + val).value = Math.round(parseFloat(data.Id));
                                                        if (data.usedTruckCapcity != "") {//alert('ddd');
                                                        document.getElementById("usedCapacity" + val).value = parseFloat(data.usedTruckCapcity);
                                                        $("#usedCapacity" + val).removeClass("errorClass");
                                                        $("#usedCapacity" + val).addClass("noErrorClass");
                                                        $("#availableCap" + val).removeClass("errorClass");
                                                        $("#availableCap" + val).addClass("noErrorClass");
                                                        $("#usedVol" + val).removeClass("errorClass");
                                                        $("#usedVol" + val).addClass("noErrorClass");
                                                        $("#receivedPackages" + val).removeClass("errorClass");
                                                        $("#receivedPackages" + val).addClass("noErrorClass");
                                                        }
                                                        document.getElementById("usedVol" + val).value = parseFloat(data.usedTruckVol).toFixed(2);
                                                        var totalUsedCapacity = 0, totalUsedVolume = 0;
                                                        var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                        var usedVolTag = document.getElementsByName("usedVol");
                                                        var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                        for (var i = 0; i < vehicleNoTag.length; i++){
                                                        totalUsedCapacity = parseFloat(totalUsedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                        totalUsedVolume = parseFloat(totalUsedVolume) + parseFloat(usedVolTag[i].value);
                                                        }

                                                        document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity;
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(chargableWeight) - parseFloat(totalUsedCapacity);
                                                        document.getElementById("availableVolume").innerHTML = parseInt(totalVolume) - parseInt(totalUsedVolume);
                                                        document.getElementById("loadedVolume").innerHTML = totalUsedVolume;
                                                        var availableVol = parseFloat(totalVolume) - parseFloat(totalUsedVolume);
                                                        var availWeight = parseFloat(chargableWeight) - parseFloat(totalUsedCapacity);
                                                        if (availableVol > 1 && availWeight == 0){// alert("i am in iff");
                                                        alert("You can't Use this model for the specify Volume.");
                                                        var avilC = document.getElementById("usedVol" + val).value;
                                                        var avlV = document.getElementById("usedCapacity" + val).value;
                                                        //alert("perticular"+avlV);
                                                        document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity - parseFloat(avlV);
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(chargableWeight) - (totalUsedCapacity - parseFloat(avlV));
                                                        document.getElementById("availableVolume").innerHTML = parseFloat(availableVol) + parseFloat(avilC);
                                                        document.getElementById("availableCap" + val).value = 0;
                                                        document.getElementById("assignedCap" + val).value = 0;
                                                        document.getElementById("usedCapacity" + val).value = 0;
                                                        document.getElementById("usedVol" + val).value = 0;
                                                        document.getElementById("vehicleNo" + val).selectedIndex = 0;
                                                        }
                                                        } else{
                                                        alert("Assign another Vehicle");
                                                        document.getElementById("availableCap" + val).value = '0';
                                                        document.getElementById("assignedCap" + val).value = '0';
                                                        }
                                                        });
                                                        }
                                                        }
                                                });
                                                } else{
                                                alert("No Capacity Exist,Already Assigned Truck No Capacity");
                                                document.getElementById("usedCapacity" + val).value = '0';
                                                document.getElementById("availableCap" + val).value = '0';
                                                document.getElementById("assignedCap" + val).value = '0';
                                                document.getElementById("usedVol" + val).value = 0;
                                                }

//                                                }

                                                }
                                                function setAvailableVolume(vId, val) {
                                                //alert("vId " + vId + " val " + val);
                                                if (parseInt(vId) > 0){
                                                var vehicleStatus = 0;
                                                var truckCapacity = 0;
                                                var truckVol = 0;
                                                var usdCap = document.getElementById("usedCapacity" + val).value;
                                                var remainWeight = document.getElementById("remainWeight").value;
//                                                        alert("usdCap " + usdCap + " remainWeight " + remainWeight);
                                                if (parseInt(usdCap) > 0) {
                                                remainWeight = parseFloat(remainWeight) + parseFloat(usdCap);
//                                                            alert("remainWeight 1"+remainWeight);
                                                }
                                                if (remainWeight == ""){
                                                remainWeight = 0;
//                                                            alert("remainWeight 2"+remainWeight);
                                                }
                                                document.getElementById("remainWeight").value = remainWeight;
                                                document.getElementById("usedCapacity" + val).value = '0';
                                                document.getElementById("usedVol" + val).value = '0';
                                                document.getElementById("availableCap" + val).value = '0';
                                                document.getElementById("assignedCap" + val).value = '0';
                                                var varray = document.getElementById("vehicleNo" + val).value.split("-");
//                                                        alert("set Awail weight Vehicle no split"+varray[0]);
                                                document.getElementById("vehicleId" + val).value = varray[0];
                                                var vehicleId = varray[0];
                                                var vehicleNo = varray[0];
                                                truckCapacity = document.getElementById("totalWeightage").value;
//                                                         alert("capa Arul Here "+truckCapacity);
                                                var splitPieces = 0;
                                                var totalPackage = $("#totalPackage").val();
                                                splitPieces = parseFloat(truckCapacity) / parseFloat(totalPackage);
                                                truckVol = document.getElementById("totalVolume").value;
//                                                        alert("vol"+truckVol);
                                                var truckDepDate = document.getElementById("truckDepDate" + val).value;
                                                var fleetTypeId = document.getElementById("fleetTypeId" + val).value;
                                                var deptTag = document.getElementsByName("truckDepDate");
                                                if (vehicleId == "") {
                                                return;
                                                }
                                                if (truckDepDate == "") {
                                                alert("Please Select Departure Date");
                                                document.getElementById("truckDepDate" + val).focus();
                                                return;
                                                }
                                                var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                var usedVolTag = document.getElementsByName("usedVol");
                                                var vno;
                                                var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                for (var i = 0; i < vehicleNoTag.length; i++){
                                                if (parseInt(i + 1) != parseInt(val)){
                                                vno = vehicleNoTag[i].value.split("-");
                                                //alert(vno);
                                                if (parseInt(document.getElementById("shipmentType").value) == 1){
                                                if (vno[0] == vehicleNo){
                                                vehicleStatus = 1;
                                                }
                                                } else{
                                                vehicleStatus = 0;
                                                }
                                                }
                                                }
                                                if (parseInt(vehicleStatus) == parseInt(1)){
                                                alert("Vehicle No Already Select Try Another");
//                                                            document.getElementById("vehicleNo"+val).value="";
//                                                            document.getElementById("vehicleId"+val).value="";
                                                return;
                                                }
                                                var usedCapacity = 0;
                                                var usedVol = 0;
                                                for (var i = 0; i < usedCapacityTag.length; i++){
                                                if (usedCapacityTag[i].value != ""){
                                                //if(deptTag[i].value==truckDepDate & vehicleNoTag[i].value==vehicleNo ){
                                                usedCapacity = parseFloat(usedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                usedVol = parseFloat(usedVol) + parseFloat(usedVolTag[i].value);
                                                //}
                                                }
                                                }
                                                //alert("used cap"+usedCapacity+"used vol"+usedVol);
//                                                truckVol = Math.round(parseFloat(truckVol)) - Math.round(parseFloat(usedVol));
//                                                        truckCapacity = Math.round(parseFloat(truckCapacity)) - Math.round(parseFloat(usedCapacity));
                                                truckVol = parseFloat(truckVol) - parseFloat(usedVol);
                                                truckCapacity = parseFloat(truckCapacity) - parseFloat(usedCapacity);
                                                if (parseFloat(truckCapacity) >= 0) {
                                                //  alert("vid"+vehicleId+"truckD"+truckDepDate+"grossWeight"+truckCapacity+"truckVol"+truckVol+"Fleet Type"+fleetTypeId);
                                                $.ajax({
                                                url: '/throttle/getAvailableWeightAndVolume.do',
                                                        data: {vehicleId: vehicleId, truckDepDate: truckDepDate, truckCapacity:truckCapacity, truckVol:truckVol, fleetTypeId:fleetTypeId},
                                                        dataType: 'json',
                                                        success: function(data) {
                                                        if (data == '' || data == null) {

                                                        } else {

                                                        $.each(data, function(i, data) {
                                                        if (data.Id != "" & data.Id != null) {
                                                        //alert('hi');
                                                        var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                        var chargableWeight = document.getElementById("totalWeightage").value;
                                                        var totalVolume = document.getElementById("totalVolume").value;
                                                        // alert("Available Cap"+data.Name+"assign Cap"+data.Id+"usedCap"+data.usedTruckCapcity+"used Vol"+data.usedTruckVol);

                                                        document.getElementById("availableCap" + val).value = parseFloat(data.Name).toFixed(2);
                                                        document.getElementById("assignedCap" + val).value = parseFloat(data.Id).toFixed(2);
//                                                                document.getElementById("availableCap" + val).value = Math.round(parseFloat(data.Name));
//                                                                document.getElementById("assignedCap" + val).value = Math.round(parseFloat(data.Id));
                                                        if (data.usedTruckCapcity != "") {//alert('ddd');
//                                                        document.getElementById("usedCapacity" + val).value = Math.round(parseFloat(data.usedTruckCapcity));
                                                        if (parseInt(data.usedTruckCapcity) > 0){
                                                        document.getElementById("usedCapacity" + val).value = parseFloat(data.usedTruckCapcity);
                                                        $("#usedCapacity" + val).removeClass("errorClass");
                                                        $("#usedCapacity" + val).addClass("noErrorClass");
                                                        $("#availableCap" + val).removeClass("errorClass");
                                                        $("#availableCap" + val).addClass("noErrorClass");
                                                        $("#usedVol" + val).removeClass("errorClass");
                                                        $("#usedVol" + val).addClass("noErrorClass");
                                                        $("#receivedPackages" + val).removeClass("errorClass");
                                                        $("#receivedPackages" + val).addClass("noErrorClass");
                                                        var remainWeight = document.getElementById("remainWeight").value;
                                                        remainWeight = parseFloat(remainWeight) - parseFloat(document.getElementById("usedCapacity" + val).value);
                                                        document.getElementById("remainWeight").value = remainWeight;
                                                        } else{
                                                        $("#vehicleNo" + val).val(0);
                                                        alert("Full capacity utilized, use another fleet.")
                                                        }
                                                        }
//                                                        document.getElementById("usedVol" + val).value = Math.round(parseFloat(data.usedTruckVol));
                                                        document.getElementById("usedVol" + val).value = parseFloat(data.usedTruckVol);
                                                        var usedCap = document.getElementById("usedCapacity" + val).value;
                                                        splitPieces = parseFloat(usedCap) / parseFloat(splitPieces);
                                                        document.getElementById("receivedPackages" + val).value = Math.round(splitPieces);
                                                        var totalUsedCapacity = 0, totalUsedVolume = 0, recPackages = 0;
                                                        var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                        var usedVolTag = document.getElementsByName("usedVol");
                                                        var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                        var receivedPackagesNew = document.getElementsByName("receivedPackages");
                                                        for (var i = 0; i < vehicleNoTag.length; i++){
                                                        totalUsedCapacity = parseFloat(totalUsedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                        totalUsedVolume = parseFloat(totalUsedVolume) + parseFloat(usedVolTag[i].value);
                                                        }
//                                                        alert("totalUsedVolume == "+totalUsedVolume);
                                                        for (var i = 0; i < vehicleNoTag.length - 1; i++){
                                                        recPackages = parseFloat(recPackages) + parseFloat(receivedPackagesNew[i].value);
                                                        }
                                                        var res = 0;
//                                                        alert(parseFloat(totalUsedCapacity));
                                                        var totalWeightageNew = $("#totalWeightage").val();
                                                        if (parseFloat(totalUsedCapacity) == parseFloat(totalWeightageNew)){
                                                        res = parseInt(totalPackage) - parseInt(recPackages);
                                                        document.getElementById("receivedPackages" + val).value = res;
                                                        }
                                                        document.getElementById("assignedCapacity").innerHTML = parseFloat(totalUsedCapacity).toFixed(2);
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(parseFloat(chargableWeight) - parseFloat(totalUsedCapacity)).toFixed(2);
                                                        document.getElementById("availableVolume").innerHTML = parseFloat(parseFloat(totalVolume) - parseFloat(totalUsedVolume)).toFixed(2);
                                                        document.getElementById("loadedVolume").innerHTML = parseFloat(totalUsedVolume).toFixed(2);
                                                        var availableVol = parseFloat(totalVolume) - parseFloat(totalUsedVolume);
                                                        var availWeight = parseFloat(chargableWeight) - parseFloat(totalUsedCapacity);
//                                                        alert("availableVol == " + availableVol);
//                                                        alert("availWeight == " + availWeight);
                                                        if (availableVol > 1 && availWeight == 0) {// alert("i am in iff");
                                                        alert("You can't Use this model for the specified Volume test.");
                                                        var avilC = document.getElementById("usedVol" + val).value;
                                                        var avlV = document.getElementById("usedCapacity" + val).value;
                                                        //alert("perticular"+avlV);
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(parseFloat(chargableWeight) - (parseFloat(totalUsedCapacity) - parseFloat(avlV))).toFixed(2);
                                                        document.getElementById("availableVolume").innerHTML = parseFloat(parseFloat(availableVol) + parseFloat(avilC)).toFixed(2);
                                                        document.getElementById("assignedCapacity").innerHTML = parseFloat(parseFloat(totalUsedCapacity) - parseFloat(avlV)).toFixed(2);
                                                        document.getElementById("loadedVolume").innerHTML = parseFloat(parseFloat(totalUsedVolume) - parseFloat(avilC)).toFixed(2);
                                                        document.getElementById("availableCap" + val).value = 0;
                                                        document.getElementById("assignedCap" + val).value = 0;
                                                        document.getElementById("usedCapacity" + val).value = 0;
                                                        document.getElementById("usedVol" + val).value = 0;
                                                        document.getElementById("vehicleNo" + val).selectedIndex = 0;
                                                        document.getElementById("receivedPackages" + val).value = 0;
                                                        }
                                                        } else{
                                                        alert("Assign another Vehicle");
                                                        document.getElementById("availableCap" + val).value = '0';
                                                        document.getElementById("assignedCap" + val).value = '0';
                                                        }
                                                        });
                                                        }
                                                        }

                                                });
                                                } else{
                                                alert("No Capacity Exist,Already Assigned Truck No Capacity");
                                                document.getElementById("availableCap" + val).value = '0';
                                                document.getElementById("assignedCap" + val).value = '0';
                                                }
                                                } else{
                                                var usdCap = document.getElementById("usedCapacity" + val).value;
                                                var remainWeight = document.getElementById("remainWeight").value;
                                                if (parseInt(usdCap) > 0){remainWeight = parseFloat(remainWeight) + parseFloat(usdCap); }
                                                document.getElementById("remainWeight").value = remainWeight;
//                                                alert(remainWeight);
                                                if (remainWeight == ""){remainWeight = 0; }
//                                                alert(remainWeight);
                                                document.getElementById("usedCapacity" + val).value = '0';
                                                document.getElementById("usedVol" + val).value = '0';
                                                document.getElementById("availableCap" + val).value = '0';
                                                document.getElementById("assignedCap" + val).value = '0';
                                                document.getElementById("receivedPackages" + val).value = "0";
                                                }
                                                cretaeOrderSubmit('');
                                                }

                                                function reValidateAvailableWeight() {
                                                var checkFleetId = document.getElementById("fleetTypeId1").value;
                                                if (parseInt(checkFleetId) > 0){
                                                var truckCapacity = 0;
                                                var truckVol = 0;
                                                truckCapacity = document.getElementById("totalWeightage").value;
                                                // alert("capa"+truckCapacity);
                                                truckVol = document.getElementById("totalVolume").value;
                                                // alert("vol"+truckVol);
                                                var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                var usedVolTag = document.getElementsByName("usedVol");
                                                var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                var availableCap = document.getElementsByName("availableCap");
                                                var usedCapacity = 0;
                                                //alert('check');
                                                var usedVol = 0; var countValue = 0; var flag = 0; var cunt1 = 0;
                                                for (var i = 0; i < usedCapacityTag.length; i++){
                                                if (usedCapacityTag[i].value != ""){
                                                //if(deptTag[i].value==truckDepDate & vehicleNoTag[i].value==vehicleNo ){

                                                var avlCapacity = availableCap[i].value;
                                                //alert("Avail able Cap"+avlCapacity);
                                                if (parseInt(avlCapacity) > 0){
                                                //   alert("hello");
                                                // usedCapacity=Math.round(parseFloat(usedCapacity))-Math.round(parseFloat(usedCapacityTag[i].value));
                                                //usedVol=Math.round(parseFloat(usedVol))-Math.round(parseFloat(usedVolTag[i].value));

                                                flag = 1;
                                                } else{
                                                //alert('if')
                                                usedCapacity = parseFloat(usedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                usedVol = parseFloat(usedVol) + parseFloat(usedVolTag[i].value); }
//                                                usedCapacity = Math.round(parseFloat(usedCapacity)) + Math.round(parseFloat(usedCapacityTag[i].value));
//                                                        usedVol = Math.round(parseFloat(usedVol)) + Math.round(parseFloat(usedVolTag[i].value)); }
//                                                                                  
                                                } countValue = i;
                                                }
                                                // if(parseInt(flag==0)){alert('flag');return;}
                                                if (parseInt(flag) == 0){return; } else{countValue = countValue + 1; }
                                                //alert("counter value"+countValue);
                                                document.getElementById("usedCapacity" + countValue).value = '0';
                                                document.getElementById("usedVol" + countValue).value = '0';
                                                document.getElementById("availableCap" + countValue).value = '0';
                                                document.getElementById("assignedCap" + countValue).value = '0';
                                                var vehicleId = document.getElementById("vehicleNo" + countValue).value.split("-")[0];
                                                //alert("revalidate ka vehicletag"+vehicleId);
                                                var truckDepDate = document.getElementById("truckDepDate" + countValue).value;
                                                var fleetTypeId = document.getElementById("fleetTypeId" + countValue).value;
                                                // alert(vehicleId+" "+truckDepDate+" "+fleetTypeId) ;
                                                // alert("used cap"+usedCapacity+"used vol"+usedVol);
                                                truckVol = parseFloat(truckVol) - parseFloat(usedVol);
                                                truckCapacity = parseFloat(truckCapacity) - parseFloat(usedCapacity);
//                                                        truckVol = Math.round(parseFloat(truckVol)) - Math.round(parseFloat(usedVol));
//                                                        truckCapacity = Math.round(parseFloat(truckCapacity)) - Math.round(parseFloat(usedCapacity));
                                                // alert(truckCapacity);
                                                if (parseFloat(truckCapacity) >= 0){
                                                //  alert("vid"+vehicleId+"truckD"+truckDepDate+"grossWeight"+truckCapacity+"truckVol"+truckVol+"Fleet Type"+fleetTypeId);
                                                $.ajax({
                                                url: '/throttle/getAvailableWeightAndVolume.do',
                                                        data: {vehicleId: vehicleId, truckDepDate: truckDepDate, truckCapacity:truckCapacity, truckVol:truckVol, fleetTypeId:fleetTypeId},
                                                        dataType: 'json',
                                                        success: function(data) {
                                                        if (data == '' || data == null) {

                                                        } else {

                                                        $.each(data, function(i, data) {
                                                        if (data.Id != "" & data.Id != null){
//                                                                      chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                        var chargableWeight = document.getElementById("totalWeightage").value;
                                                        var totalVolume = document.getElementById("totalVolume").value;
                                                        // alert("Available Cap"+data.Name+"assign Cap"+data.Id+"usedCap"+data.usedTruckCapcity+"used Vol"+data.usedTruckVol);

                                                        document.getElementById("availableCap" + countValue).value = parseFloat(data.Name);
                                                        document.getElementById("assignedCap" + countValue).value = parseFloat(data.Id);
//                                                                document.getElementById("availableCap" + countValue).value = Math.round(parseFloat(data.Name));
//                                                                document.getElementById("assignedCap" + countValue).value = Math.round(parseFloat(data.Id));
                                                        if (data.usedTruckCapcity != ""){
//                                                                document.getElementById("usedCapacity" + countValue).value = Math.round(parseFloat(data.usedTruckCapcity));
                                                        document.getElementById("usedCapacity" + countValue).value = parseFloat(data.usedTruckCapcity);
                                                        var remainWeight = document.getElementById("remainWeight").value;
                                                        remainWeight = parseFloat(remainWeight) - parseFloat(document.getElementById("usedCapacity" + countValue).value);
                                                        document.getElementById("remainWeight").value = remainWeight;
                                                        }//alert(data.usedTruckVol);
//                                                        document.getElementById("usedVol" + countValue).value = Math.round(parseFloat(data.usedTruckVol));
                                                        document.getElementById("usedVol" + countValue).value = parseFloat(data.usedTruckVol);
                                                        var totalUsedCapacity = 0, totalUsedVolume = 0;
                                                        var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                        var usedVolTag = document.getElementsByName("usedVol");
                                                        var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                        for (var i = 0; i < vehicleNoTag.length; i++){
                                                        totalUsedCapacity = parseFloat(totalUsedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                        totalUsedVolume = parseFloat(totalUsedVolume) + parseFloat(usedVolTag[i].value);
                                                        }

                                                        document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity;
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(chargableWeight) - parseFloat(totalUsedCapacity);
                                                        document.getElementById("availableVolume").innerHTML = parseFloat(totalVolume) - parseFloat(totalUsedVolume);
                                                        var availableVol = parseFloat(totalVolume) - parseFloat(totalUsedVolume);
                                                        var availWeight = parseFloat(chargableWeight) - parseFloat(totalUsedCapacity);
                                                        if (availableVol > 1 && availWeight == 0){
                                                        alert("You can't Use this model for the specify Volume.");
                                                        var avilC = document.getElementById("usedVol" + countValue).value;
                                                        var avlV = document.getElementById("usedCapacity" + countValue).value;
//                                                        alert("perticular" + avlV);
//                                                        alert("totalUsedCapacity " + totalUsedCapacity);
                                                        document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity - parseFloat(avlV);
                                                        document.getElementById("availableCapacity").innerHTML = parseFloat(totalUsedCapacity) - (totalUsedCapacity - parseFloat(avlV));
                                                        document.getElementById("availableVolume").innerHTML = parseFloat(availableVol) + parseFloat(avilC);
                                                        document.getElementById("availableCap" + countValue).value = 0;
                                                        document.getElementById("assignedCap" + countValue).value = 0;
                                                        document.getElementById("usedCapacity" + countValue).value = 0;
                                                        document.getElementById("usedVol" + countValue).value = 0;
                                                        document.getElementById("usedVol" + countValue).selectedIndex = 0;
                                                        }

                                                        } else{
                                                        alert("Assign another Vehicle its Full");
                                                        document.getElementById("availableCap" + countValue).value = 0;
                                                        document.getElementById("assignedCap" + countValue).value = 0;
                                                        document.getElementById("usedCapacity" + countValue).value = 0;
                                                        document.getElementById("usedVol" + countValue).value = 0;
                                                        }
                                                        });
                                                        }
                                                        }
                                                });
                                                }
                                                } else{
                                                // alert("Not requires");

                                                }

                                                }




                                                handleVehicleTypeVehicleRegNo(1);
                                                function handleVehicleTypeVehicleRegNo(val) {
                                                var vehilceNo = "vehicleNo" + val;
                                                var truckOriginId = "truckOriginId1";
                                                var truckDestinationId = "truckDestinationId1";
                                                var vehicleId = "vehicleId" + val;
                                                var fleetTypeId = "fleetTypeId" + val;
                                                var truckAdd = "truck_add" + val;
                                                }

                                                $(document).ready(function() {
                                                var truckCnt = 2;
                                                // alert(cnt);
                                                $("#truck_add").click(function() {
                                                var remainWeight = document.getElementById("remainWeight").value;
                                                //alert(remainWeight);
                                                if (parseInt(remainWeight) > 0){
                                                //alert("naved here");
                                                var originId = document.getElementById("awbOriginId").value;
                                                var originName = document.getElementById("awbOrigin").value;
                                                var destinationId = document.getElementById("awbDestinationId").value;
                                                var destinationName = document.getElementById("awbDestination").value;
                                                // alert(originId+destinationId+originName+destinationName);
                                                var travelKm = document.getElementById("truckTravelKm1").value;
                                                // alert(travelKm)
                                                var travelHours = document.getElementById("truckTravelHour1").value;
                                                var travelMinutes = document.getElementById("truckTravelMinute1").value;
                                                var routeId = document.getElementById("truckRouteId1").value;
                                                //alert(destinationId+originId+" "+originName+destinationId+destinationName+travelKm+travelHours+travelMinutes+routeId);                                                                                                                                                                                                                                                                                                                                                  



                                                $('#truckTable tr').last().after('<tr><td>' + truckCnt + '</td><td><input class="datepicker" type="text" name="truckDepDate" id="truckDepDate' + truckCnt + '" value="" style="width: 70px" onchange="checkValomeAvail(' + truckCnt + ');cretaeOrderSubmit(' + "" + '); readonly></td><input type="hidden" name="truckOriginId" id="truckOriginId' + truckCnt + '" value="' + originId + '"><input type="hidden" name="truckOriginName" id="truckOriginName' + truckCnt + '" value="' + originName + '"><input type="hidden" name="truckDestinationId" id="truckDestinationId' + truckCnt + '" value="' + destinationId + '"><input type="hidden" name="truckDestinationName" id="truckDestinationName' + truckCnt + '" value="' + destinationName + '"><input type="text" name="truckRouteId" id="truckRouteId' + truckCnt + '" value="' + routeId + '"><input type="hidden" name="truckTravelKm" id="truckTravelKm' + truckCnt + '" value="' + travelKm + '" style="width: 60px"><input type="hidden" name="truckTravelHour" id="truckTravelHour' + truckCnt + '" value="' + travelHours + '" style="width: 60px"><input type="hidden" name="truckTravelMinute" id="truckTravelMinute' + truckCnt + '" value="' + travelMinutes + '" style="width: 60px"><td><select name="fleetTypeId" id="fleetTypeId' + truckCnt + '" onchange="setVehicleNo(this.value,' + truckCnt + ');"><option value="0">--select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><select  name="vehicleNo" id="vehicleNo' + truckCnt + '" style="width: 95px" onchange="cretaeOrderSubmit(' + "" + ');setAvailableVolume(this.value,' + truckCnt + ');"><option value="0">-Select-</option><input type="hidden" name="vehicleId" id="vehicleId' + truckCnt + '" style="width:95px" value="0"></td><td><input type="text" name="usedCapacity" id="usedCapacity' + truckCnt + '" style="width:95px" value="0" onkeyup="setUsedCapacity(' + truckCnt + ');cretaeOrderSubmit(' + "" + ');"></td><td> <input type="text"  name="usedVol" id="usedVol' + truckCnt + '" style="width:95px" value="0"></td><td><input readonly type="text" name="availableCap" id="availableCap' + truckCnt + '" style="width:95px" value="0"></td><td><input readonly type="text" name="assignedCap" id="assignedCap' + truckCnt + '" style="width:95px" value="0"></td><td><input type="text" name="receivedPackages" id="receivedPackages' + truckCnt + '" style="width:95px;" value="0"></td></tr>');
                                                if (parseInt(document.getElementById("shipmentType").value) == 1){
                                                document.getElementById("receivedPackages" + truckCnt).style.display = "block";
                                                } else{
                                                document.getElementById("receivedPackages" + truckCnt).style.display = "block";
                                                }
                                                $(".datepicker").datepicker({
                                                changeMonth: true, changeYear: true
                                                });
                                                // callOriginAjax(truckCnt);
                                                // callDestinationAjax(truckCnt);
                                                handleVehicleTypeVehicleRegNo(truckCnt);
                                                truckCnt++;
                                                // alert(truckCnt);
                                                } else{alert("There is No Remaining Weight for Load "); }
                                                });
                                                $("#truck_rem").click(function() {
                                                //alert(truckCnt);
                                                if ($('#truckTable tr').size() > 2) {//alert(truckCnt);
                                                var removeWeight = document.getElementById("usedCapacity" + (parseInt(truckCnt) - 1)).value;
                                                var removeVolume = document.getElementById("usedVol" + (parseInt(truckCnt) - 1)).value;
                                                var availablecapcity = document.getElementById("availableCapacity").innerHTML;
                                                var assignedCapacity = document.getElementById("assignedCapacity").innerHTML;
                                                var availableVolume = document.getElementById("availableVolume").innerHTML;
                                                //   alert(removeWeight);
                                                document.getElementById("availableCapacity").innerHTML = parseFloat(availablecapcity) + parseFloat(removeWeight);
                                                document.getElementById("assignedCapacity").innerHTML = parseFloat(assignedCapacity) - parseFloat(removeWeight);
                                                document.getElementById("availableVolume").innerHTML = parseFloat(availableVolume) + parseFloat(removeVolume);
                                                var remainWeight = document.getElementById("remainWeight").value;
                                                remainWeight = parseInt(remainWeight) + parseInt(removeWeight);
                                                document.getElementById("remainWeight").value = remainWeight;
                                                $('#truckTable tr:last-child').remove();
                                                truckCnt = truckCnt - 1;
                                                } else {
                                                alert('One row should be present in table');
                                                }
                                                });
                                                });
                                                function checkValomeAvail(cnt){
                                                var truckDepDate = $("#truckDepDate" + cnt).val();
                                                var vehicleNo = $("#vehicleNo" + cnt).val();
                                                if (truckDepDate != ''){
                                                setAvailableVolume(vehicleNo, cnt);
                                                }
                                                }

                                                function setShipmentType(value){
                                                var receivedPackages = document.getElementsByName("receivedPackages");
                                                if (parseInt(value) == 1) {
//                                                $("#receivedPieces").hide();
                                                $('#awbType').empty();
                                                $('#awbType').append(
                                                        $('<option selected value=' + 1 + '>Non Auto Generated</option>')
                                                        )
                                                        $('#awbType').append(
                                                        $('<option value=' + 2 + '>Auto Generated</option>')
                                                        )
                                                        for (var i = 1; i <= receivedPackages.length; i++){
                                                document.getElementById("receivedPackages" + i).style.display = "block";
                                                }
                                                } else{
                                                $('#awbType').empty();
                                                $('#awbType').append(
                                                        $('<option selected value=' + 1 + '>Non Auto Generated</option>')
                                                        )
//                                                $("#receivedPieces").show();
                                                        for (var i = 1; i <= receivedPackages.length; i++){
                                                document.getElementById("receivedPackages" + i).style.display = "block";
                                                }
                                                }
                                                }

                                            </script>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">
                                                    <a href="javascript:void(0);cretaeOrderSubmit('');" id='truck_add'><input type="button" class="button" value="Add Truck"/></a>
                                                    <a href="javascript:void(0);cretaeOrderSubmit('');" id="truck_rem"><input type="button" class="button" value="Remove Truck"/></a></td>

                                            </tr>
                                            </table>

                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable1">
                                                <tr>
                                                    <td class="contenthead" colspan="4">RCS (Freight Check Details) </td>
                                                </tr>
                                                <tr>
                                                    <td class="text1"><font color="red">*</font>Document & Freight Receipt</td>
                                                    <td class="text1">
                                                        <select name="freightReceipt" id="freightReceipt">
                                                            <c:if test="${freightReceipt == 'YES'}">
                                                                <option value="YES" selected>YES</option>
                                                                <option value="NO">NO</option>
                                                            </c:if>
                                                            <c:if test="${freightReceipt == 'NO'}">
                                                                <option value="YES">YES</option>
                                                                <option value="NO" selected>NO</option>
                                                            </c:if>
                                                        </select>
                                                    </td>
                                                    <td class="text1"><font color="red">*</font>Warehouse</td>
                                                    <td class="text1">
                                                        <input type="text" name="wareHouseName" id="wareHouseName" value="<c:out value="${wareHouseName}"/>" class="textbox" style="width: 80px"/>
                                                        <input type="text" name="wareHouseLocation" id="wareHouseLocation" value="<c:out value="${wareHouseLocation}"/>" class="textbox" style="width: 80px"/>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="text1"><font color="red"></font>Shipment Acceptance Date</td>
                                                    <td class="text1">
                                                        <input type="text" name="shipmentAcceptanceDate" id="shipmentAcceptanceDate" value="<c:out value="${shipmentAcceptanceDate}"/>" class="datepicker" style="width: 120px"/>
                                                    </td>
                                                    <td class="text1"><font color="red"></font>Shipment Acceptance Time</td>
                                                    <td class="text1">
                                                        HH:<select name="shipmentAccpHour" id="shipmentAccpHour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                        MI:<select name="shipmentAccpMinute" id="shipmentAccpMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>
                                                    </td>
                                                    <script>
                                                            document.getElementById("shipmentAccpHour").value = '<c:out value="${shipmentAccpHour}"/>';
                                                            document.getElementById("shipmentAccpMinute").value = '<c:out value="${shipmentAccpMinute}"/>';
                                                        </script>
                                                </tr>
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2"><font color="red">*</font>Consignor Name</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName" onchange="cretaeOrderSubmit('');" value="<c:out value="${consignorName}"/>" ></td>
                                                    <td class="text2"><font color="red">*</font>Mobile No</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consignorPhoneNo" maxlength="12" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" onchange="cretaeOrderSubmit('');" value="<c:out value="${consignorMobile}"/>" ></td>
                                                    <td class="text2"><font color="red">*</font>Address</td>
                                                    <td class="text2"><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress" onchange="cretaeOrderSubmit('');"><c:out value="${consignorAddress}"/></textarea> </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2"><font color="red">*</font>Consignee Name</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName" onchange="cretaeOrderSubmit('');" value="<c:out value="${consigneeName}"/>" ></td>
                                                    <td class="text2"><font color="red">*</font>Mobile No</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consigneePhoneNo" maxlength="12"  onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" onchange="cretaeOrderSubmit('');" value="<c:out value="${consigneeMobile}"/>" ></td>
                                                    <td class="text2"><font color="red">*</font>Address</td>
                                                    <td class="text2"><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress" onchange="cretaeOrderSubmit('');" ><c:out value="${consigneeAddress}"/></textarea> </td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <table border="0" class="border" align="left" width="97.5%" cellpadding="0" cellspacing="0"  id="creditLimitTable" style="display:none;">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6" >Credit Limit Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2">Credit Days &nbsp; </td>
                                                    <td class="text2"><label id="creditDaysTemp"></label><input type="hidden" name="creditDays" id="creditDays" value="0" /> </td>
                                                    <td class="text2" align="left" >Credit Limit &nbsp; </td>
                                                    <td class="text2"><label id="creditLimitTemp"></label><input type="hidden" name="creditLimit" id="creditLimit" value="0" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text1" align="left" >Customer Rank &nbsp; </td>
                                                    <td class="text1"><label id="customerRankTemp"></label><input type="hidden" name="customerRank" id="customerRank" value="0" /></td>
                                                    <td class="text1" align="left" >Approval Status&nbsp; </td>
                                                    <td class="text1"><label id="approvalStatusTemp"></label><input type="hidden" name="approvalStatus" id="approvalStatus" value="1" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text2" align="left" >Out Standing &nbsp; </td>
                                                    <td class="text2"><label id="outStandingTemp"></label><input type="hidden" name="outStanding" id="outStanding" value="0" /></td>
                                                    <td class="text2" align="left" >Out Standing Date&nbsp; </td>
                                                    <td class="text2"><label id="outStandingDateTemp"></label><input type="hidden" name="outStandingDate" id="outStandingDate" valu="11-11-2015" /></td>
                                                </tr>
                                            </table>

                                            <table  align="left" class="border" width="30%" cellpadding="0" cellspacing="0"  id="contractFreightTable" >
                                                <tr>
                                                    <td class="contentsub"  height="30" >Freight Details</td>
                                                    <td class="contentsub"  height="30" >Currency</td>
                                                    <td class="contentsub" colspan="4" align="left" >

                                                        <select name="currencyType" id="currencyType">
                                                            <option value="1">INR</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>                                                    
                                                    <td class="contentsub"  height="30" colspan="5">Contract Details&nbsp;&nbsp;
                                                        <input type="radio" name="contract" id="contract1" value="1" checked="" onclick="checkContract(1),checkContractDetails(this.value);"/></td>
                                                </tr>
                                                <tr>

                                                    <td class="text1"><label  id="autoRateTd">Auto Rate</label></td>
                                                    <td class="text1">Full Truck Rate</td>
                                                    <td class="text1"></td>
                                                </tr>
                                                <script type="text/javascript">

                                                    function calculateNegotiableRate() {
                                                    if (document.getElementById("rate3").checked) {
                                                    var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                    var perKgRate = document.getElementById("perKgRate").value;
                                                    document.getElementById("rateValue1").value = parseFloat(chargableWeight) * parseFloat(perKgRate);
                                                    } else {
                                                    document.getElementById("rateValue1").value = "";
                                                    }
                                                    }
                                                    function autoRateWeight() {
                                                    if (document.getElementById("rate1").checked) {
                                                    var awbOriginId = document.getElementById("awbOriginId").value;
                                                    var awbDestinationId = document.getElementById("awbDestinationId").value;
                                                    var customerId = document.getElementById("customerId").value;
                                                    var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                    var productRate = document.getElementById("productRate").value;
                                                    $.ajax({
                                                    url: '/throttle/getChargeableRateValue.do',
                                                            data: {customerId: customerId, chargeableWeight: chargableWeight, awbOriginId: awbOriginId, awbDestinationId: awbDestinationId, productRate: productRate},
                                                            dataType: 'json',
                                                            success: function(data) {
                                                            if (data == '' || data == null) {
                                                            alert('chosen contract route does not exists. please check.');
                                                            } else {
                                                            $.each(data, function(i, data) {
                                                            document.getElementById("rateValue").value = data.Name;
                                                            document.getElementById("totFreightAmount").value = data.Name;
                                                            document.getElementById("perKgAutoRate").value = data.perKgAutoRate;
//                                                                            alert("perKgAutoRate " + data.perKgAutoRate);
                                                            });
                                                            }
                                                            }
                                                    });
                                                    } else {
                                                    document.getElementById("rateValue").value = "";
                                                    }
                                                    }
                                                    function fullTruckRate() {
                                                    var totalRevenue = 0;
                                                    if (document.getElementById("rate2").checked) {
                                                    var awbOriginId = document.getElementById("awbOriginId").value;
                                                    var awbDestinationId = document.getElementById("awbDestinationId").value;
                                                    var customerId = document.getElementById("customerId").value;
                                                    var productRate = document.getElementById("productRate").value;
                                                    var fleetTypeId = 0;
                                                    var cnt = $('#truckTable tr').size();
                                                    for (var i = 0; i < cnt - 1; i++){
                                                    fleetTypeId = document.getElementById("fleetTypeId" + (i + 1)).value;
                                                    // alert("awbOriginId"+awbOriginId+"awbDestinationId"+awbDestinationId+"customerId"+customerId+"productRate"+productRate+"fleetTypeId"+fleetTypeId);
                                                    $.ajax({
                                                    url: '/throttle/getFullTruckRateValue.do',
                                                            data: {customerId: customerId, fleetTypeId: fleetTypeId, awbOriginId:awbOriginId, awbDestinationId:awbDestinationId, productRate: productRate},
                                                            dataType: 'json',
                                                            success: function(data) {
                                                            if (data == '' || data == null) {
                                                            alert('chosen contract route does not exists with this Vehicle Type' + fleetTypeId);
                                                            totalRevenue = 0;
                                                            } else {
                                                            $.each(data, function(i, data) {
                                                            //alert(data.Name);

                                                            totalRevenue = totalRevenue + data.Name;
                                                            });
                                                            }
                                                            }
                                                    });
                                                    }
                                                    document.getElementById("totFreightAmount").value = totalRevenue;
                                                    document.getElementById("rateValue").value = totalRevenue;
                                                    } else {
                                                    document.getElementById("rateValue").value = "";
                                                    }
                                                    }

                                                </script>

                                                <tr>

                                                    <td class="text1"><input type="radio" name="rateMode" id="rate1" value="1" checked onclick="autoRateWeight(), checkContractDetails(this.value);"/></td>
                                                    <td class="text1"><input type="radio" name="rateMode" id="rate2" value="2" onclick="fullTruckRate(), checkContractDetails(this.value);" /></td>
                                                    <td class="text1"><input type="hidden" name="perKgAutoRate" id="perKgAutoRate" value="" style="width:60px"/><input type=text" name="rateValue" id="rateValue" value="" style="width:60px"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td></td>
                                                    <td class="text1"></td>
                                                </tr>
                                                <tr>

                                                    <td class="contentsub"  height="30" colspan="5">Non Contract&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="contract" id="contract2" value="2"   onclick="checkContract(2), checkContractDetails(this.value);"/></td>
                                                </tr>
                                                <tr>

                                                    <td class="text1"><label  id="autoRateTd">Per Kg Rate</label></td>
                                                    <td class="text1">Full Truck Rate</td>
                                                    <td class="text1">Freight Amount</td>
                                                </tr>
                                                <tr>

                                                    <td class="text1"><input type="radio" name="rateMode1" id="rate3" value="3" onclick="setTextBoxEnable(this.value), checkContractDetails(this.value);"/>
                                                        <input type="text" name="perKgRate" id="perKgRate" value=""  style="width:60px" onchange="calculateNegotiableRate(), checkContractDetails(this.value);"  class="textbox" maxlength="6" /></td>
                                                    <td class="text1"><input type="radio" name="rateMode1" id="rate4" size="10" value="2"    onclick="setTextBoxEnable(this.value), checkContractDetails(this.value);"/></td>
                                                    <td class="text1"><input type=text" name="rateValue1" id="rateValue1" value="" style="width:60px"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly maxlength="6" class="textbox" /></td>
                                                </tr>
                                                <tr>

                                                </tr>
                                                <script>

                                                    function checkContractDetails(val) {
                                                    // alert(val);
                                                    if (document.getElementById('contract1').checked) {

                                                    var rateValues = document.getElementById('rateValue').value;
                                                    document.getElementById('totFreightAmount').value = rateValues;
                                                    document.getElementById('totFreightAmount').focus();
                                                    }else {
                                                    var rateValues1 = document.getElementById('rateValue1').value;
                                                    var tot1 = document.getElementById('totFreightAmount').value = rateValues1;
                                                    document.getElementById('totFreightAmount').focus();
                                                    }
//
                                                    }



                                                    function setTextBoxEnable(str){

                                                    if (parseInt(str) == 3){
                                                    document.getElementById("rateValue1").readOnly = true;
                                                    }
                                                    if (parseInt(str) == 2){
                                                    document.getElementById("rateValue1").readOnly = false;
                                                    }
                                                    }
                                                    function freightDetails(val) {
                                                    if (val == 1) {
                                                    $("#contract1").attr('checked', true);
                                                    $("#contract1").attr('disabled', false);
                                                    $("#contract2").attr('checked', false);
                                                    checkContract(val);
                                                    } else if (val == 2) {
                                                    $("#contract1").attr('checked', false);
                                                    $("#contract1").attr('disabled', true);
                                                    $("#contract2").attr('checked', true);
                                                    checkContract(val);
                                                    }
                                                    }
                                                    function checkContract(val) {
                                                    if (val == 1) {
                                                    // Checked

                                                    $("#rate1").attr('checked', true);
                                                    $("#rate2").attr('checked', false);
                                                    $("#rate3").attr('checked', false);
                                                    $("#rate4").attr('checked', false);
                                                    //Disable
                                                    $("#rate1").attr('disabled', false);
                                                    $("#rate2").attr('disabled', false);
                                                    $("#rate3").attr('disabled', true);
                                                    $("#rate4").attr('disabled', true);
                                                    $("#rateValue1").val("");
                                                    $("#perKgRate").val("");
//                                                                $("#perKgRate").attr('disabled', true);
//                                                                $("#rateValue").attr('disabled', false);
                                                    } else if (val == 2) {
                                                    //Checked
                                                    $("#rate1").attr('checked', false);
                                                    $("#rate2").attr('checked', false);
                                                    $("#rate3").attr('checked', true);
                                                    $("#rate4").attr('checked', false);
                                                    //Disabled
                                                    $("#rate1").attr('disabled', true);
                                                    $("#rate2").attr('disabled', true);
                                                    $("#rate3").attr('disabled', false);
                                                    $("#rate4").attr('disabled', false);
                                                    $("#rateValue").val("0");
                                                    $("#rateValue").attr('disabled', true);
//                                                                $("#perKgRate").attr('disabled', false);
                                                    var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                    var perKgRate = document.getElementById("perKgRate").value;
                                                    document.getElementById("rateValue1").value = parseFloat(chargableWeight) * parseFloat(perKgRate);
                                                        
                                                    }
                                                    }
                                                </script>

                                                <tr style="display:none;">
                                                    <td class="text1">Billing Type</td>
                                                    <td class="text1"><label id="billTypeName"></label></td>
                                                    <td class="text1" align="right" >Estimated Freight Charges &nbsp; </td>
                                                    <td class="text1">INR.
                                                        <input type="text" class="textbox" name="totFreightAmount" id="totFreightAmount" value="0"/>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        Freight Agree &nbsp;&nbsp;
                                                        <input type="checkbox" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();" class="textbox" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br>

                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinFreightTable" style="display: none">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6" >Walkin Freight Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text1">Billing Type</td>
                                                    <td class="text1"><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="texttbox" onchange="displayWalkIndetails(this.value)" style="width: 120px">
                                                            <option value="0" selected>--Select--</option>
                                                            <option value="1" >Fixed Freight</option>
                                                            <option value="2" >Rate Per KM</option>
                                                            <option value="3" >Rate Per Kg</option>
                                                        </select></td>
                                                    <td class="text1">Freight Charges (INR.)</td>
                                                    <td class="text1">
                                                        <input type="text" readonly class="textbox" name="totFreightAmount1" id="totFreightAmount1" value="0"/>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;

                                                    </td>
                                                </tr>
                                                <tr id="WalkInptp" style="display: none">
                                                    <td class="text2">Freight With Reefer</td>
                                                    <td class="text2"><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                    <td class="text2">Freight Without Reefer</td>
                                                    <td class="text2"><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                </tr>
                                                <tr id="WalkInPtpw" style="display: none">
                                                    <td class="text2">Rate With Reefer/Kg</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                    <td class="text2">Rate Without Reefer/Kg</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                </tr>
                                                <tr id="WalkinKilometerBased" style="display: none">
                                                    <td class="text2">Rate With Reefer/Km</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                    <td class="text2">Rate Without Reefer/Km</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                                                </tr>
                                            </table>
                                            <script type="text/javascript">
                                                function displayWalkIndetails(val) {
                                                var reeferRequired = document.getElementById('reeferRequired').value;
                                                var vehicleTypeId = document.getElementById('vehTypeIdTemp').value;
                                                //alert(vehicleTypeId);
                                                if (vehicleTypeId == '0') {
                                                alert("Please Choose Vehicle Type");
                                                document.cNote.walkInBillingTypeId.value = 0;
                                                document.getElementById('vehTypeIdTemp').focus();
                                                } else if (reeferRequired == '0') {
                                                alert("Please Choose Reefer Details");
                                                document.getElementById('reeferRequired').focus();
                                                } else {
                                                if (val == 1) {
                                                $("#WalkInptp").show();
                                                $("#WalkInPtpw").hide();
                                                $("#WalkinKilometerBased").hide();
                                                if (reeferRequired == "Yes") {
                                                $('#walkinFreightWithReefer').attr('readonly', false);
                                                $('#walkinFreightWithoutReefer').attr('readonly', true);
                                                } else if (reeferRequired == "No") {
                                                $('#walkinFreightWithReefer').attr('readonly', true);
                                                $('#walkinFreightWithoutReefer').attr('readonly', false);
                                                }
                                                $("#walkinRateWithReeferPerKm").val("");
                                                $("#walkinRateWithoutReeferPerKm").val("");
                                                $("#walkinRateWithReeferPerKg").val("");
                                                $("#walkinRateWithoutReeferPerKg").val("");
                                                } else if (val == 2) {
                                                $("#WalkInptp").hide();
                                                $("#WalkInPtpw").show();
                                                $("#WalkinKilometerBased").hide();
                                                if (reeferRequired == "Yes") {
                                                $('#walkinRateWithReeferPerKg').attr('readonly', false);
                                                $('#walkinRateWithoutReeferPerKg').attr('readonly', true);
                                                } else if (reeferRequired == "No") {
                                                $('#walkinRateWithReeferPerKg').attr('readonly', true);
                                                $('#walkinRateWithoutReeferPerKg').attr('readonly', false);
                                                }
                                                $("#walkinFreightWithReefer").val("");
                                                $("#walkinFreightWithoutReefer").val("");
                                                $("#walkinRateWithReeferPerKm").val("");
                                                $("#walkinRateWithoutReeferPerKm").val("");
                                                } else if (val == 3) {
                                                $("#WalkInptp").hide();
                                                $("#WalkInPtpw").hide();
                                                $("#WalkinKilometerBased").show();
                                                if (reeferRequired == "Yes") {
                                                $('#walkinRateWithReeferPerKm').attr('readonly', false);
                                                $('#walkinRateWithoutReeferPerKm').attr('readonly', true);
                                                } else if (reeferRequired == "No") {
                                                $('#walkinRateWithReeferPerKm').attr('readonly', true);
                                                $('#walkinRateWithoutReeferPerKm').attr('readonly', false);
                                                }
                                                $("#walkinFreightWithReefer").val("");
                                                $("#walkinFreightWithoutReefer").val("");
                                                $("#walkinRateWithReeferPerKg").val("");
                                                $("#walkinRateWithoutReeferPerKg").val("");
                                                }
                                                }
                                                }
                                            </script>

                                            <!--<textarea name="/orderReferenceRemarks" id="orderReferenceRemarks1" style="display: none"></textarea>-->
                                            <table  border="0" class="border" align="left" width="72%" cellpadding="0" cellspacing="0"  id="contractFreightTable1">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="1">Sno</td>
                                                    <td class="contentsub"  height="30" colspan="3" style="width : 500px;">Remarks</td>
                                                </tr>
<!--                                                <tr>
                                                    <td class="text2">1</td>
                                                    <td class="text2">
                                                        <textarea name="bookingReferenceRemarks" id="bookingReferenceRemarks1" style="width : 500px; height : 20px"></textarea>
                                                        <textarea name="orderReferenceRemarks" id="orderReferenceRemarks" style="width : 500px; height : 20px"></textarea>
                                                    </td>
                                                </tr>-->

                                            </table>
                                            <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <td height="30" >
                                                        <a href="javascript:void(0);" id='remarks_add'><input type="button" class="button" value="Add Row"/></a>
                                                        <a href="javascript:void(0);" id="remarks_rem"><input type="button" class="button" value="Remove Row"/></a>
                                                        <!--id="orderButton"-->
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <table border="0"  align="center" width="97.5%" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <td height="30" >
                                                <center><input type="button" class="button" name="Save" value="Update Order" id="createOrder" onclick="checkContractDetails(this.value); cretaeOrderSubmit(this.value);" ></center>
                                                </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <br>
                                            <br>
                                             <script>
                                                $(document).ready(function () {
                                                    var tempValue = "";
                                                    tempValue = '<c:out value="${orderReferenceRemarks}"/>';
                                                    var temp = tempValue.split("~");
                                                    var remarksCnt = temp.length+1;
                                                    var n = 1;
                                                    for (var i = 0; i < temp.length; i++) {
                                                        $('#contractFreightTable1 tr').last().after('<tr><td>' + n + '</td><td class="text2"><textarea name="bookingReferenceRemarks" id="bookingReferenceRemarks' + i + '" style="width : 500px; height : 20px">' + temp[i] + '</textarea></td>');
                                                        n++;
                                                    }

                                                    //alert(cnt);
                                                    $("#remarks_add").click(function () {
                                                        $('#contractFreightTable1 tr').last().after('<tr><td>' + remarksCnt + '</td><td class="text2"><textarea name="bookingReferenceRemarks" id="bookingReferenceRemarks' + remarksCnt + '" style="width : 500px; height : 20px"></textarea></td>');
                                                        remarksCnt++;
                                                    });

                                                    $("#remarks_rem").click(function () {
                                                        if ($('#contractFreightTable1 tr').size() > 2) {
                                                            $('#contractFreightTable1 tr:last-child').remove();
                                                            remarksCnt = remarksCnt - 1;
                                                        } else {
                                                            alert('One row should be present in table');
                                                        }
                                                    });

                                                });
                                            </script>
                                            <input type="hidden" class="textbox" id="subTotal" name="subTotal"  value="0" readonly="">
                                            <input align="right" value="0" type="hidden" readonly class="textbox" id="totalCharges" name="totalCharges" >
                                            <br/>
                                            <br/>


                                            <!--                </div>-->
                                            </div>

                                            </div>

                                            <script>
                                                function setClass(id, type){
                                                if (type == 1){
                                                // ErrorClass
                                                $('#' + id).removeClass('noErrorClass');
                                                $('#' + id).addClass('errorClass');
                                                } else{
                                                // NoErrorClass
                                                $('#' + id).removeClass('errorClass');
                                                $('#' + id).addClass('noErrorClass');
                                                }
                                                }

                                                var alertNames = "";
                                                var alertCount = 0;
                                                function checkCustomerField(field, value, fieldName, checkVal, alertName){
                                                alertCount++;
                                                if (field == checkVal && value == '') {
                                                setClass(fieldName, 1);
                                                validateStatus = 1;
                                                $("#customerName").removeClass('noErrorClass');
                                                $("#customerName").addClass('errorClass');
                                                $('#customerId').val('');
                                                $('#customerCode').val('');
                                                $('#customerAddress').val('');
                                                $('#pincode').val('');
                                                $('#customerMobileNo').val('');
                                                $('#customerPhoneNo').val('');
                                                $('#customerPhoneNo').val('');
                                                $('#mailId').val('');
                                                $('#billingTypeName').text('');
                                                $('#billTypeName').text('');
                                                $('#contractNo').text('');
                                                $('#contractExpDate').text('');
                                                $('#contractExpDate1').text('');
                                                } else if (field == checkVal && value == 'Update Order') {
                                                setClass(fieldName, 1);
                                                validateStatus = 1;
                                                $("#customerName").removeClass('noErrorClass');
                                                $("#customerName").addClass('errorClass');
                                                $('#customerId').val('');
                                                $('#customerCode').val('');
                                                $('#customerAddress').val('');
                                                $('#pincode').val('');
                                                $('#customerMobileNo').val('');
                                                $('#customerPhoneNo').val('');
                                                $('#customerPhoneNo').val('');
                                                $('#mailId').val('');
                                                $('#billingTypeName').text('');
                                                $('#billTypeName').text('');
                                                $('#contractNo').text('');
                                                $('#contractExpDate').text('');
                                                $('#contractExpDate1').text('');
                                                validateStatus = 1;
                                                if (alertCount <= 8){
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                } else {
                                                setClass(fieldName, 2);
                                                validateStatus = 0;
                                                }
                                                return validateStatus;
                                                }

                                                function checkField(field, value, fieldName, checkVal, alertName) {
                                                if (field == checkVal && value == '') {
                                                setClass(fieldName, 1);
                                                validateStatus = 1;
                                                } else if (field == checkVal && value == 'Update Order') {
                                                setClass(fieldName, 1);
                                                validateStatus = 1;
                                                if (fieldName != 'orderReferenceAwbNo' && fieldName != 'orderReferenceEnd'){
                                                alertCount++;
                                                if (alertCount <= 8){
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                }
                                                } else {
                                                setClass(fieldName, 2);
                                                validateStatus = 0;
                                                }

                                                return validateStatus;
                                                }

                                                function cretaeOrderSubmit(value) {
                                                alertNames = "";
                                                alertCount = 0;
                                                $("#createOrder").hide();
                                                var customerType = document.cNote.customerTypeId.value;
                                                var consignmentDate = document.getElementById("consignmentDate").value;
                                                var orderReferenceNo = document.getElementById("orderReferenceNo").value;
                                                var orderReferenceAwb = document.getElementById("orderReferenceAwb").value;
                                                var orderReferenceAwbNo = document.getElementById("orderReferenceAwbNo").value;
                                                var orderReferenceEnd = document.getElementById("orderReferenceEnd").value;
                                                var awbOrigin = document.getElementById("truckOriginName1").value;
                                                var awbDestination = document.getElementById("truckDestinationName1").value;
                                                var awbOrderDeliveryDate = document.getElementById("awbOrderDeliveryDate").value;
                                                var awbMovementType = document.getElementById("awbMovementType").value;
                                                var productRate = document.getElementById("productRate").value;
                                                var totalPackage = document.getElementById("totalPackage").value;
                                                var totalWeightage = document.getElementById("totalWeightage").value;
                                                var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                var awbOrderDeliveryDate = document.getElementById("awbOrderDeliveryDate").value;
                                                var awbOriginRegion = document.getElementById("awbOriginRegion").value;
                                                var awbDestinationRegion = document.getElementById("awbDestinationRegion").value;
                                                var productCategoryId = document.getElementById("productCategoryId").value;
                                                var customerName = document.getElementById("customerName").value;
                                                var productRate = document.getElementById("productRate").value;
                                                var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                var totalVolume = document.getElementById("totalVolume").value;
                                                var consignorName = document.getElementById("consignorName").value;
                                                var consignorPhoneNo = document.getElementById("consignorPhoneNo").value;
                                                var consignorAddress = document.getElementById("consignorAddress").value;
                                                var consigneeName = document.getElementById("consigneeName").value;
                                                var consigneePhoneNo = document.getElementById("consigneePhoneNo").value;
                                                var consigneeAddress = document.getElementById("consigneeAddress").value;
                                                //  alert("totalPackage "+totalPackage);
                                                var validateStatus = 0;
                                                validateStatus = checkField(awbMovementType, value, "awbMovementType", "0", "Select movement type");
                                                validateStatus = checkField(orderReferenceAwb, value, "orderReferenceAwb", '', "AWB no is empty");
                                                validateStatus = checkField(orderReferenceAwbNo, value, "orderReferenceAwbNo", '', "AWB no is empty");
                                                validateStatus = checkField(orderReferenceEnd, value, "orderReferenceEnd", '', "AWB no is empty");
                                                validateStatus = checkField(awbOrigin, value, "truckOriginName1", '', "Transit origin is empty");
                                                validateStatus = checkField(awbDestination, value, "truckDestinationName1", '', "Transit destination is empty");
                                                validateStatus = checkField(awbOrderDeliveryDate, value, "awbOrderDeliveryDate", '', "Order delivery date is empty");
                                                validateStatus = checkField(awbOriginRegion, value, "awbOriginRegion", '', "AWB origin is empty");
                                                validateStatus = checkField(awbDestinationRegion, value, "awbDestinationRegion", '', "AWB destination is empty");
                                                validateStatus = checkCustomerField(customerName, value, "customerName", '', "Customer name is empty");
                                                validateStatus = checkField(productCategoryId, value, "productCategoryId", '0', "Select freight type");
                                                validateStatus = checkField(productRate, value, "productRate", '0', "Select product rate");
                                                validateStatus = validateArticles(value);
                                                if (validateStatus == 1 && value == "Update Order"){
                                                swal(alertNames);
                                                $("#createOrder").show();
                                                return;
                                                }
//                                                if(validateStatus == 1 && value == 'Update Order'){
//                                                    checkField('',value,"noOfPieces1",'',"Cheak load details",2);
//                                                }
                                                validateStatus = checkField(totalWeightage, value, "totalWeightage", '', "Gross Weight is empty");
                                                if (validateStatus == 0){
                                                validateStatus = checkField(totalWeightage, value, "totalWeightage", '0', "Enter Gross Weight");
                                                }
                                                validateStatus = checkField(totalChargeableWeights, value, "totalChargeableWeights", '', "Chargeable Weight is empty");
                                                if (validateStatus == 0){
                                                validateStatus = checkField(totalChargeableWeights, value, "totalChargeableWeights", '0', "Enter Chargeable Weight");
                                                }
                                                validateStatus = checkField(totalVolume, value, "totalVolume", '', "Volume(CBM) is empty");
                                                if (validateStatus == 0){
                                                validateStatus = checkField(totalVolume, value, "totalVolume", '0', "Enter Volume(CBM)");
                                                }
                                                validateStatus = validateVehicle(value);
                                                if (validateStatus == 1 && value == "Update Order"){
                                                swal(alertNames);
                                                $("#createOrder").show();
                                                return;
                                                }
//                                                if(totalWeightage == '' && value == '') {
//                                                }else if(totalWeightage != '' && value == 'Update Order'){
//                                                validateStatus = validateVehicle(value);
//                                                }
                                                validateStatus = checkField(consignorName, value, "consignorName", '', "Consignor name is empty");
                                                validateStatus = checkField(consignorPhoneNo, value, "consignorPhoneNo", '', "Consignor phone no is empty");
                                                validateStatus = checkField(consignorAddress, value, "consignorAddress", '', "Consignor Address is empty");
                                                validateStatus = checkField(consigneeName, value, "consigneeName", '', "Consignee name is empty");
                                                validateStatus = checkField(consigneePhoneNo, value, "consigneePhoneNo", '', "Consignee phone no is empty");
                                                validateStatus = checkField(consigneeAddress, value, "consigneeAddress", '', "Consignee address is empty");
                                                if (validateStatus == 1 && value == "Update Order")
                                                        swal(alertNames);
                                                if (totalWeightage != ''){
                                                var usedCapacity = document.getElementsByName("usedCapacity");
                                                var truckAssignedCapacity = 0;
                                                for (var i = 0; i < usedCapacity.length; i++){
                                                truckAssignedCapacity = parseFloat(truckAssignedCapacity) + parseFloat(usedCapacity[i].value)
                                                }
                                                if (parseFloat(truckAssignedCapacity) > 0) {
                                                if (parseFloat(totalWeightage) > parseFloat(truckAssignedCapacity) && value == "Update Order"){
                                                swal("Remaining (" + ((parseFloat(totalWeightage) - parseFloat(truckAssignedCapacity)) / 1000).toFixed(2) + ") ton to be loaded.");
                                                validateStatus = 1;
                                                $("#createOrder").show();
                                                return;
                                                } else{
                                                validateStatus = 0;
                                                }
                                                }

                                                if (parseInt(truckAssignedCapacity) == 0 && value == "Update Order") {
                                                swal("Truck details for (" + ((parseFloat(totalWeightage) - parseFloat(truckAssignedCapacity)) / 1000).toFixed(2) + ") ton to be filled.");
                                                validateStatus = 1;
                                                $("#createOrder").show();
                                                return;
                                                } else{
                                                validateStatus = 0;
                                                }
                                                document.getElementById("rateValue").disabled = false;
                                                document.getElementById("perKgRate").disabled = false;
                                                document.getElementById("rateValue1").disabled = false;
                                                } else{
                                                $("#createOrder").show();
                                                return;
                                                }

                                                if (validateStatus == 0 && value == 'Update Order') {
                                                document.cNote.action = "/throttle/saveEditBookingOrder.do";
                                                document.cNote.submit();
                                                } else{
                                                $("#createOrder").show();
                                                }
                                                }

                                                function receivedPackageCount(){
                                                var receivedPackages = document.getElementsByName("receivedPackages");
                                                var count = 0;
                                                for (var i = 0; i < receivedPackages.length; i++){
                                                count += parseInt(receivedPackages[i].value);
                                                }
                                                return count;
                                                }

                                                function validateVehicle(value) {
                                                var alertName = "";
                                                var truckDepDate = document.getElementsByName("truckDepDate");
                                                var validateStatus = 0;
                                                for (var i = 1; i <= truckDepDate.length; i++) {
                                                if ($("#truckDepDate" + i).val() == "" && value == '') {
                                                $("#truckDepDate" + i).removeClass('noErrorClass');
                                                $("#truckDepDate" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#truckDepDate" + i).val() == "" && value == 'Update Order') {
                                                $("#truckDepDate" + i).removeClass('noErrorClass');
                                                $("#truckDepDate" + i).addClass('errorClass');
                                                alertName = "Turk Dept Date is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else{
                                                $("#truckDepDate" + i).removeClass('errorClass');
                                                $("#truckDepDate" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }
                                                if ($("#fleetTypeId" + i).val() == "0" && value == '') {
                                                $("#fleetTypeId" + i).removeClass('noErrorClass');
                                                $("#fleetTypeId" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#fleetTypeId" + i).val() == "0" && value == 'Update Order') {
                                                $("#fleetTypeId" + i).removeClass('noErrorClass');
                                                $("#fleetTypeId" + i).addClass('errorClass');
                                                alertName = "select fleet type for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else{
                                                $("#fleetTypeId" + i).removeClass('errorClass');
                                                $("#fleetTypeId" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if ($("#vehicleNo" + i).val() == "0" && value == '') {
                                                $("#vehicleNo" + i).removeClass('noErrorClass');
                                                $("#vehicleNo" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#vehicleNo" + i).val() == "0" && value == 'Update Order') {
                                                $("#vehicleNo" + i).removeClass('noErrorClass');
                                                $("#vehicleNo" + i).addClass('errorClass');
                                                alertName = "select vehicle no for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else{
                                                $("#vehicleNo" + i).removeClass('errorClass');
                                                $("#vehicleNo" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }
                                                if (($("#usedCapacity" + i).val() == "" || $("#usedCapacity" + i).val() == "0") && value == '') {
                                                $("#usedCapacity" + i).removeClass('noErrorClass');
                                                $("#usedCapacity" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if (($("#usedCapacity" + i).val() == "" || $("#usedCapacity" + i).val() == "0") && value == 'Update Order') {
                                                $("#usedCapacity" + i).removeClass('noErrorClass');
                                                $("#usedCapacity" + i).addClass('errorClass');
                                                alertName = "Loaded Weight(Kg) is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else {
                                                $("#usedCapacity" + i).removeClass('errorClass');
                                                $("#usedCapacity" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if (($("#usedVol" + i).val() == "" || $("#usedVol" + i).val() == "0") && value == '') {
                                                $("#usedVol" + i).removeClass('noErrorClass');
                                                $("#usedVol" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if (($("#usedVol" + i).val() == "" || $("#usedVol" + i).val() == "0") && value == 'Update Order') {
                                                $("#usedVol" + i).removeClass('noErrorClass');
                                                $("#usedVol" + i).addClass('errorClass');
                                                alertName = "Loaded Volume(CBM) is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else {
                                                $("#usedVol" + i).removeClass('errorClass');
                                                $("#usedVol" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if ($("#availableCap" + i).val() == "" && value == '') {
                                                $("#availableCap" + i).removeClass('noErrorClass');
                                                $("#availableCap" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#availableCap" + i).val() == "" && value == 'Update Order') {
                                                $("#availableCap" + i).removeClass('noErrorClass');
                                                $("#availableCap" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else{
                                                $("#availableCap" + i).removeClass('errorClass');
                                                $("#availableCap" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }
                                                if ($("#assignedCap" + i).val() == "" && value == '') {
                                                $("#assignedCap" + i).removeClass('noErrorClass');
                                                $("#assignedCap" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#assignedCap" + i).val() == "" && value == 'Update Order') {
                                                $("#assignedCap" + i).removeClass('noErrorClass');
                                                $("#assignedCap" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else{
                                                $("#assignedCap" + i).removeClass('errorClass');
                                                $("#assignedCap" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }
                                                if (($("#receivedPackages" + i).val() == "" || $("#receivedPackages" + i).val() == "0") && value == '') {
                                                $("#receivedPackages" + i).removeClass('noErrorClass');
                                                $("#receivedPackages" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if (($("#receivedPackages" + i).val() == "" || $("#receivedPackages" + i).val() == "0") && value == 'Update Order') {
                                                $("#receivedPackages" + i).removeClass('noErrorClass');
                                                $("#receivedPackages" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else {
                                                $("#receivedPackages" + i).removeClass('errorClass');
                                                $("#receivedPackages" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                }
                                                return validateStatus;
                                                }

                                                function validateArticles(value){
                                                var alertName = "";
                                                var noOfPieces = document.getElementsByName("noOfPieces");
                                                var validateStatus = 0;
                                                for (var i = 1; i <= noOfPieces.length; i++) {
                                                if ($("#noOfPieces" + i).val() == '' && value == ''){
                                                $("#noOfPieces" + i).removeClass('noErrorClass');
                                                $("#noOfPieces" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#noOfPieces" + i).val() == '' && value == "Update Order"){
                                                $("#noOfPieces" + i).removeClass('noErrorClass');
                                                $("#noOfPieces" + i).addClass('errorClass');
                                                alertName = "No of pieces is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else{
                                                $("#noOfPieces" + i).removeClass('errorClass');
                                                $("#noOfPieces" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if ($("#lengths" + i).val() == "" && value == ''){
                                                $("#lengths" + i).removeClass('noErrorClass');
                                                $("#lengths" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#lengths" + i).val() == "" && value == 'Update Order'){
                                                $("#lengths" + i).removeClass('noErrorClass');
                                                $("#lengths" + i).addClass('errorClass');
                                                alertName = "length is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else {
                                                $("#lengths" + i).removeClass('errorClass');
                                                $("#lengths" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if ($("#widths" + i).val() == "" && value == ''){
                                                $("#widths" + i).removeClass('noErrorClass');
                                                $("#widths" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#widths" + i).val() == "" && value == 'Update Order'){
                                                $("#widths" + i).removeClass('noErrorClass');
                                                $("#widths" + i).addClass('errorClass');
                                                alertName = "width is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else {
                                                $("#widths" + i).removeClass('errorClass');
                                                $("#widths" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }

                                                if ($("#heights" + i).val() == "" && value == ''){
                                                $("#heights" + i).removeClass('noErrorClass');
                                                $("#heights" + i).addClass('errorClass');
                                                validateStatus = 1;
                                                } else if ($("#heights" + i).val() == "" && value == 'Update Order'){
                                                $("#heights" + i).removeClass('noErrorClass');
                                                $("#heights" + i).addClass('errorClass');
                                                alertName = "height is empty for row " + i;
                                                alertCount++;
                                                if (alertCount <= 8) {
                                                alertNames = alertNames + '\n' + alertName + ",";
                                                }
                                                validateStatus = 1;
                                                } else {
                                                $("#heights" + i).removeClass('errorClass');
                                                $("#heights" + i).addClass('noErrorClass');
                                                validateStatus = 0;
                                                }
                                                }
                                                return validateStatus;
                                                }

                                            </script>
                                            <script type="text/javascript">
                                                function calcTotalChargeableWeights() {
                                                var totalWeightage = document.getElementById("totalWeightage").value;
                                                var totalChargeableWeights = document.getElementById("totalChargeableWeights").value;
                                                if (totalWeightage != "" && totalChargeableWeights != "") {
                                                if (parseFloat(totalChargeableWeights) <= parseFloat(totalWeightage)) {
                                                alert("Chargeable Weight is less than the Gross Weight");
                                                document.getElementById("totalChargeableWeights").value = totalWeightage;
                                                document.getElementById("totalChargeableWeights").focus();
                                                }
                                                }
                                                }
                                            </script>
                                            <!--Unuse codes-->
                                            <input type="hidden" class="textbox" name="destinationId" id="destinationId" >
                                            <input type="hidden" class="textbox" name="routeContractId" id="routeContractId" >
                                            <input type="hidden" class="textbox" name="routeId" id="routeId" >
                                            <input type="hidden" class="textbox" name="routeBased" id="routeBased" >
                                            <input type="hidden" class="textbox" name="contractRateId" id="contractRateId" >
                                            <input type="hidden" class="textbox" name="totalKm" id="totalKm" >
                                            <input type="hidden" class="textbox" name="totalHours" id="totalHours" >
                                            <input type="hidden" class="textbox" name="totalMinutes" id="totalMinutes" >
                                            <input type="hidden" class="textbox" name="totalPoints" id="totalPoints" >
                                            <input type="hidden" class="textbox" name="vehicleTypeId" id="vehicleTypeId" >
                                            <input type="hidden" class="textbox" name="rateWithReefer" id="rateWithReefer" >
                                            <input type="hidden" class="textbox" name="rateWithoutReefer" id="rateWithoutReefer" >
                                            <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >

                                            <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" >
                                            <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute" >
                                            <input type="hidden" name="entryType" value="1" checked="">
                                            <input type="hidden" class="textbox" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N">
                                            <input type="hidden" class="textbox" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N">
                                            <input type="hidden" class="textbox" name="consignmentOrderInstruction" id="consignmentOrderInstruction">

                                            <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="textbox"  value="No" />
                                            <input type="hidden" name="consignmentOrderId"  value='<c:out value="${consignmentOrderId}"/>'/>
                                            <!--Unuse codes-->
                                            </form>
                                            </body>
                                            </html>
                                            <script>
                                                var totalUsedCapacity = 0;
                                                var totalUsedVolume = 0;
                                                var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                var usedVolTag = document.getElementsByName("usedVol");
                                                var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                var totalWeightage = document.getElementById("totalWeightage").value;
                                                var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                var totalVolume = document.getElementById("totalVolume").value;
                                                for (var i = 0; i < vehicleNoTag.length; i++) {
                                                    totalUsedCapacity = parseFloat(totalUsedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                    totalUsedVolume = parseFloat(totalUsedVolume) + parseFloat(usedVolTag[i].value);
                                                }
                                                //alert(totalUsedVolume+" "+totalVolume);
                                                document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity;
                                                document.getElementById("availableCapacity").innerHTML = (parseFloat(totalWeightage) - parseFloat(totalUsedCapacity)).toFixed(2);
                                                document.getElementById("loadedVolume").innerHTML = totalUsedVolume;
                                                document.getElementById("availableVolume").innerHTML = (parseFloat(totalVolume) - parseFloat(totalUsedVolume)).toFixed(2);
                                            </script>
                                            <script>
                                                var contractTypeId = '<c:out value="${contractId}"/>';
                                                var rateMode = '<c:out value="${rateMode}"/>';
                                                var rateValue = '<c:out value="${rateValue}"/>';
                                                var perKgRate = '<c:out value="${perKgRate}"/>';
                                                var freightCharges = '<c:out value="${freightCharges}"/>';

//                                                alert("contractTypeId" + contractTypeId + " rateMode" + rateMode + "rateValue" + rateValue + "perKgRate" + perKgRate + "freightCharges" + freightCharges);
                                                if (contractTypeId == "1") {
                                                    document.getElementById("contract1").checked = true;

                                                    document.getElementById("contract2").checked = false;
                                                    if (rateMode == "1") {
                                                        document.getElementById('rateValue').value = freightCharges;

                                                        document.getElementById('rate1').checked = true;
                                                        document.getElementById("rate2").disabled = true;

                                                    } else {
                                                        document.getElementById('rate1').disabled = true;
                                                        document.getElementById("rate2").checked = true;
                                                        document.getElementById('rateValue').value = freightCharges;

                                                    }
                                                    document.getElementById('rate3').disabled = true;
                                                    document.getElementById('rate4').disabled = true;
                                                    document.getElementById('perKgRate').readOnly = false;
                                                    document.getElementById('rateValue1').readOnly = true;
                                                } else {
                                                    document.getElementById('rateValue').readOnly = false;
                                                    document.getElementById("contract1").checked = false;
                                                    document.getElementById("contract1").disabled = true;
                                                    document.getElementById("contract2").checked = true;
                                                    document.getElementById('rate1').disabled = true;
                                                    document.getElementById("rate2").disabled = true;
                                                    if (rateMode == "3") {
//                                                        alert("Hi I am Here 1");
                                                        document.getElementById('rate3').checked = true;
                                                        document.getElementById('rate3').disabled = false;
                                                        document.getElementById('rate4').disabled = false;
                                                        document.getElementById('perKgRate').value = perKgRate;
                                                        document.getElementById('rateValue1').value = freightCharges;
                                                        document.getElementById('perKgRate').readOnly = false;
                                                        document.getElementById('rateValue1').readOnly = true;
                                                    } else {
//                                                        alert("Hi I am Here 2");
                                                        document.getElementById('rate4').checked = true;
                                                        document.getElementById('rate3').disabled = false;
                                                        document.getElementById('rate4').disabled = false;
                                                        document.getElementById('rate3').disabled = false;
//                                                        alert("freightCharges" + freightCharges);
                                                        document.getElementById('rateValue1').value = freightCharges;
                                                        document.getElementById('perKgRate').readOnly = false;
                                                        document.getElementById('rateValue1').readOnly = true;
                                                    }
                                                }
                                                document.getElementById("contract1").disabled = false;
                                                document.getElementById("contract2").disabled = false;
                                                awbTotalOldPackage();
                                                function awbTotalOldPackage() {
                                                    var noOfPiecesValue = document.getElementsByName("noOfPieces");
                                                    var noOfPieces = 0;
                                                    for (var i = 0; i < noOfPiecesValue.length; i++) {
                                                        if (noOfPiecesValue[i].value != '') {
                                                            noOfPieces += parseInt(noOfPiecesValue[i].value);
//                                alert(k);
                                                        }
                                                    }
                                                    document.getElementById("awbReceivedTotalOldPackage").value = noOfPieces;
                                                }

                                            </script>