<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function configsubmit()
    {
       var errStr = "";
       var nameCheckStatus = $("#parameterNameStatus").text();
        if(document.getElementById("parameterName").value == "") {
            errStr = "Please enter parameterName.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        else if(nameCheckStatus != "") {
            errStr ="parameter Name Status Already Exists.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        else if(document.getElementById("parameterValue").value == "") {
            errStr ="Please enter parameterValue.\n";
            alert(errStr);
            document.getElementById("parameterValue").focus();
        }
        else if(document.getElementById("parameterUnit").value == "") {
            errStr ="Please enter parameterUnit.\n";
            alert(errStr);
            document.getElementById("parameterUnit").focus();
        }else if(nameCheckStatus != "") {
            errStr ="parameter Name Status Already Exists.\n";
            alert(errStr);
            document.getElementById("parameterName").focus();
        }
        if(errStr == "") {
            document.configMaster.action=" /throttle/saveConfigMaster.do";
            document.configMaster.method="post";
            document.configMaster.submit();
        }






    }
    function setValues(sno,parameterName,parameterValue,parameterUnit,parameterDescription,id,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("id").value = id;
        document.getElementById("parameterDescription").value = parameterDescription;
        document.getElementById("parameterName").value = parameterName;
        document.getElementById("parameterValue").value = parameterValue;
        document.getElementById("parameterUnit").value = parameterUnit;
        document.getElementById("status").value = status;
    }






    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function validateEmail(alerttoEmail){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(alerttoEmail.value) == false)
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

}
var httpRequest;
    function checkParameterName() {

        var parameterName = document.getElementById('parameterName').value;

            var url = '/throttle/checkParameterName.do?parameterName=' + parameterName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#parameterNameStatus").text('Please Check Parameter Name  :' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#parameterNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }



</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ConfigMaster" text="ConfigMaster"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ConfigMaster" text="ConfigMaster"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    
    <body onload="document.configMaster.parameterName.focus();">


        <form name="configMaster"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
            
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="id" id="id" value=""  />
                <tr>
                <table class="table table-info mb30 table-hover" id="bg" >	
			<thead>
                <tr>
                    <th  colspan="4" >config Master</th>
                </tr>
                        </thead>
                <tr>
                    <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="parameterNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Name</td>
                    <td ><input type="text" name="parameterName" id="parameterName" class="form-control" style="width:250px;height:40px" maxlength="100" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkParameterName();" autocomplete="off"</td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Value</td>
                    <td ><input type="text" name="parameterValue" id="parameterValue" class="form-control" style="width:250px;height:40px" onkeypress="return onKeyPressBlockCharacters(event)"</td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Parameter Unit</td>
                    <td ><input type="text" name="parameterUnit" id="parameterUnit" class="form-control" style="width:250px;height:40px" maxlength="10" onkeypress="return onKeyPressBlockNumbers(event);"</td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>parameter Description</td>
                    <td ><input type="text" name="parameterDescription" id="parameterDescription" class="form-control" style="width:250px;height:40px"</td>
<!--                        <select  align="center" class="textbox" name="parameterUnit" id="parameterUnit" >
                            <option value='0'>--select--</option>
                            <option value='1'>Hour</option>
                            <option value='2' >Km</option>
                            <option value='3' >Stage</option>
                            <option value='4' ></option>
                        </select>-->
<!--                    </td>-->
                </tr>
                <tr>
                    <td >Status</td>
                    <td >
                        <select  align="center" style="width:250px;height:40px"  name="status"  >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                </tr>
             </table>
                </tr>
                <tr>
                    <td>
                        
                        <center>
                            <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="configsubmit()">
                        </center>
                    </td>
                </tr>
            </table>
            
            

            <h2 align="center">List The Config Details</h2>


           <table class="table table-info mb30 table-hover" id="table" >	
			

                <thead>

                    <tr height="30">
                        <th>S.No</th>
                        <th>Parameter Name </th>
                        <th>Parameter Value</th>
                        <th>Parameter Unit</th>
                        <th>Parameter Description</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${configDetailsList != null}">
                        <c:forEach items="${configDetailsList}" var="cdl">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno + 1%> </td>
                                <td   align="left"> <c:out value="${cdl.parameterName}" /></td>
                                <td   align="left"> <c:out value="${cdl.parameterValue}" /></td>
                                <td   align="left"> <c:out value="${cdl.parameterUnit}"/></td>
                                <td   align="left"> <c:out value="${cdl.parameterDescription}"/></td>
                                <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${cdl.parameterName}" />','<c:out value="${cdl.parameterValue}" />','<c:out value="${cdl.parameterUnit}" />','<c:out value="${cdl.parameterDescription}" />','<c:out value="${cdl.id}" />','<c:out value="${cdl.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>