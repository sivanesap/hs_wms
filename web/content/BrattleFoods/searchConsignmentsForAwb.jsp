<%-- 
    Document   : searchConsignmentsForAwb
    Created on : Apr 7, 2016, 10:35:34 AM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    </head>
    <body>
        <form name="consignments" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Air WayBill Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td height="30"><font color="red">*</font>AWB No</td>
                                            <td >
                                                <input type="text" class="textbox" id="awbNo" name="awbNo" value="<c:out value='${orderReferenceAwb}'/> <c:out value='${orderReferenceAwbNo}'/> <c:out value='${orderReferenceEnd}'/>" onclick="clearAwb();">           
<!--                                <input type="text" name="orderReferenceAwb" id="orderReferenceAwb" value="" maxlength="3" size="3"  class="textbox" style="width: 30px" />
                                <input type="text" name="orderReferenceAwbNo" id="orderReferenceAwbNo" value="" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width:40px" />
                                <input type="text" name="orderReferenceEnd" id="orderReferenceEnd" value="" maxlength="4" size="4" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 40px" />-->
                            </td>
                                        <td>
                                            <!--<input type="text" class="textbox" id="awbNo" name="awbNo">-->  
                                            <input type="hidden" class="textbox" id="consignmentId" name="consignmentId">  
                                        </td>
                                        <td colspan="2">
                                            <input type="button" value="Search" class="button" name="search" onClick="viewAwbDetails()">&nbsp;&nbsp;</td>
                                    </tr>


                                </table>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <script>
         function clearAwb(){
       document.getElementById("awbNo").value="";
        }
      </script>
          <!------------------------------------------------------------>
          <c:if test="${consignmentNoteNo != null }"> 
          
          <div id="awbDetails">
                <div id="customerDetail">
                       



                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable1">
                        <tr>
                            <td class="contenthead" colspan="4">Booking -<b><label><c:out value="${consignmentNoteNo}"/></label></b></td>
                        </tr>

                        <tr>
                            <!--                    <td class="text1">Consignment Note </td>
                                                <td class="text1"><input type="text" name="consignmentNoteNo" id="consignmentNoteNo" value="<c:out value="${consignmentOrderNo}"/>" class="textbox"readonly style="width: 120px"/></td>-->
                            <td class="text1"><font color="red">*</font>Customer Type</td>
                            <td class="text1">
                                <c:if test="${customerTypeList != null}">
                                    <select name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" class="textbox" style="width:120px;" readonly >
                                       <option value="1">Contract</option>
                                    </select>
                                </c:if>
                            </td>
                            <td class="text1">Booking Date </td>
                            <td class="text1" ><input type="text" name="consignmentDate" id="consignmentDate" value="<c:out value='${consignmentOrderDate}'/>" class="textbox"readonly style="width: 120px" readonly/></td>
                        </tr>
                        <tr class="text2">
                            <td >AWB Type</td>
                            <td>
                                <c:if test="${awbType == 1}">Non Auto generated</c:if>
                                <c:if test="${awbType == 2}">Auto generated</c:if>
                               
                                <!--<input type="hidden" name="awbno" id="awbno" value="0"/>-->
                            </td>
                            <td >Shipment Type</td>
                            <td> 
                               <c:if test="${shipmentType == 1}">Full-Shipment</c:if>
                                <c:if test="${shipmentType == 2}">Part-Shipment</c:if>
                                <input type="hidden" name="shipmentType" id="shipmentType" value="<c:out value='${shipmentType}'/>"/>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td >Customer Order Reference No</td>
                            <td ><input name="orderReferenceNo" id="orderReferenceNo" value="<c:out value='${consignmentRefNo}'/>" class="textbox" maxlength="10"   style="width: 120px" readonly/></td>
                            <td ><font color="red">*</font>AWB No</td>
                            <td >
                                <input type="text" name="orderReferenceAwb" id="orderReferenceAwb" value="<c:out value='${orderReferenceAwb}'/>" maxlength="3" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width: 30px" readonly/>
                                <input type="text" name="orderReferenceAwbNo" id="orderReferenceAwbNo" value="<c:out value='${orderReferenceAwbNo}'/>" onKeyPress="return onKeyPressBlockCharacters(event);" readonly  class="textbox" style="width:60px" readonly/>
                                <input type="text" name="orderReferenceEnd" id="orderReferenceEnd" value="<c:out value='${orderReferenceEnd}'/>" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" style="width:60px" maxlength="5"/>
                            </td>
                        </tr>

                        <tr class="text2">
                            <td ><font color="red">*</font>AWB Origin</td>
                            <td >
                                <input type="hidden" name="awbOriginId" id="awbOriginId" value="<c:out value='${awbOriginId}'/>" class="textbox" style="width: 120px"/>
                                <input type="text" name="awbOrigin" id="awbOrigin" value="<c:out value='${awbOriginName}'/>" class="textbox" style="width: 120px" readonly/>
                            </td>
                            <td ><font color="red">*</font>AWB Destination</td>
                            <td >
                                <input type="hidden" name="awbDestinationId" id="awbDestinationId" value="<c:out value='${awbDestinationId}'/>" class="textbox" style="width: 120px" />
                                <input type="text" name="awbDestination" id="awbDestination" value="<c:out value='${awbDestinationName}'/>" class="textbox" style="width: 120px" readonly/>
                            </td>
                        </tr>
                       
                        <tr class="text1">
                            <td ><font color="red"></font>Order Delivery Date</td>
                            <td >
                                <input type="text" name="awbOrderDeliveryDate" id="awbOrderDeliveryDate" value="<c:out value='${awbOrderDeliveryDate}'/>" class="datepicker" style="width: 120px" readonly/>
                            </td>
                            <td ><font color="red"></font>Order Delivery Time</td>
                            <td >
                                HH:<select name="orderDeliveryHour" id="orderDeliveryHour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="orderDeliveryMinute" id="orderDeliveryMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                            </td>
                              </tr>
                        <script>
                            document.getElementById("orderDeliveryHour").value='<c:out value='${orderDeliveryHour}'/>';
                            document.getElementById("orderDeliveryMinute").value='<c:out value='${orderDeliveryMinute}'/>';
                        </script>

                   
                      
                        <tr>
                            <td class="text2"><font color="red">*</font>AWB Destination Region</td>
                            <td class="text2">
                                <input type="text" name="awbDestinationRegion" id="awbDestinationRegion" value="<c:out value='${awbDestinationRegion}'/>" class="textbox" style="width: 120px" readonly/>
                            </td>
                            <td class="text2"><font color="red">*</font>Movement Type</td>
                            <td class="text2">
                                <select name="awbMovementType" id="awbMovementType" class="textbox" readonly >
                                    <option value="0">--Select--</option>
                                    <option value="1">Bonded-Import</option>
                                    <option value="2">Bonded-Export</option>
                                    <option value="3">Non Bonded-Import</option>
                                    <option value="4">Non Bonded-Export</option>
                                    <option value="5">Non Bonded</option>
                                </select>
                            <script>
                            document.getElementById("awbMovementType").value='<c:out value='${awbMovementType}'/>';
                            
                        </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Airline Name</td>
                            <td class="text1">
                               <input type="text" readonly name="airlineName" id="airlineName" value="<c:out value='${airlineName}'/>" />
                            </td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                    <br>
                    
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Contract Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Customer Name</td>
                            <td class="text1"><input type="hidden" name="customerId" value="<c:out value='${customerId}'/>" id="customerId" class="textbox" />
<!--                <input type="text" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" class="textbox"  value="" readonly />-->
                <input type="text" name="customerName" id="customerName" class="textbox"  value="<c:out value='${customerName}'/>" readonly/>
                            </td>
                            <td class="text1"><font color="red">*</font>Customer Code</td>
                            <td class="text1"><input type="text" name="customerCode" id="customerCode" class="textbox"  value="<c:out value='${customerCode}'/>"  readonly /></td>
                        <input type="hidden" name="paymentType" id="paymentType" class="textbox"  value="<c:out value='${customerBillingType}'/>" readonly/>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="customerAddress" name="customerAddress" readonly ><c:out value='${customerAddress}'/></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value='${customerPincode}'/>" readonly  class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="textbox" value="<c:out value='${customerMobile}'/>" readonly /></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="mailId" id="mailId" class="textbox"  value="<c:out value='${customerMobile}'/>" readonly/></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="textbox" maxlength="10" value="<c:out value='${customerPhone}'/>"  readonly /></td>

                            <td class="text2">Billing Type</td>
                            <td class="text2" id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="textbox"  value="<c:out value='${customerBillingType}'/>"/><label id="billingTypeName" ><c:out value='${billingTypeName}'/></label></td>
                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Walkin Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font> Customer Name</td>
                            <td class="text1"><input type="text" name="walkinCustomerName" id="walkinCustomerName" class="textbox"  value="<c:out value='${customerName}'/>" readonly/></td>
                            <td class="text1"><font color="red">*</font> Customer Code</td>
                            <td class="text1"><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="textbox"  value="<c:out value='${customerCode}'/>" readonly/></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress" value="<c:out value='${customerAddress}'/>" readonly></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="textbox" value="<c:out value='${customerPincode}'/>" readonly/></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="textbox"  value="<c:out value='${customerMobile}'/>" readonly/></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="walkinMailId" id="walkinMailId" class="textbox"  readonly/></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text"     onKeyPress='return onKeyPressBlockCharacters(event);' name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="textbox" maxlength="10"  value="<c:out value='${customerPhone}'/>" readonly/></td>

                            <td class="text2" colspan="2">&nbsp</td>

                        </tr>
                    </table>
                    <div id="contractDetails" style="display: ">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b><input type="hidden" name="contractId" id="contractId" class="textbox" value="<c:out value='${customerContractId}'/>" /><label id="contractNo"><c:out value='${customerContractId}'/></label></b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><font color="green"><b><label id="contractExpDate"><c:out value='${contractTo}'/>
                                            <font color="red"><b><label id="contractExpDate1"></label></b></font></td>
                                            <td class="text1">&nbsp;&nbsp;&nbsp;</td>
                                            <!--                                <td class="text1"><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>-->
                                            <td class="text1">
                                                <!--                                    <input type="button" class="button" value="View Contract" onclick="viewCustomerContract()">-->
                                                <a href="#" onclick="viewCustomerContract();">view</a>
                                            </td>
                                            </tr>
                                            </table>
                                            </div>




                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                                                <tr>
                                                    <td class="contenthead" colspan="6" >Product Info</td>
                                                </tr>
                                                <tr>

                                                    <td class="text1"><font color="red">*</font>Freight Type</td>
                                                    <td class="text1"><c:if test="${productCategoryList != null}">
                                                            <!--<input type="hidden" name="productCategoryId" id="productCategoryId" class="textbox" />-->
                                                            <select name="productCategoryId" id="productCategoryId"  class="textbox" style="width:120px" readonly>
                                                                <option value="0">--Select--</option>
                                                                <c:forEach items="${productCategoryList}" var="proList">
                                                                    <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                                                </c:forEach>
                                                            </select>
                                                        </c:if>
                                                    </td>
                                                    <!--<td class="text1" colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label></td>-->
                                                    <td class="text1"><font color="red">*</font>Product Rate</td>
                                                    <td class="text1">
                                                        <select name="productRate" id="productRate" class="textbox" readonly>
                                                            <option value="0">--Select--</option>
                                                            <option value="1">Rate With Reefer</option>
                                                            <option value="2">Rate Without Reefer</option>
                                                        </select>
                                                        <script>
                                                            var productId='<c:out value='${productCategoryId}'/>';
                                                            document.getElementById("productCategoryId").value=productId.split("~")[0];
                                                            document.getElementById("productRate").value='<c:out value='${productRate}'/>';
                                                        </script>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="text2">Commodity</td>
                                                    <td class="text2">
                                                        <input name="commodity" id="commodity" class="textbox" value="<c:out value='${commodity}'/>" style="width:120px" readonly/>
                                                    </td>
                                                    <td class="text2" colspan="2"></td>

                                                </tr>
                                            </table>
                                            <div style="display:none;">
                                                <br>
                                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                                                    <tr>
                                                        <td class="contenthead" colspan="6" >Consignment / Route Details</td>
                                                    </tr>

                                                    <tr id="contractRouteDiv1" style="display:none;">
                                                        <td class="text1"><font color="red">*</font>Origin&nbsp;&nbsp;
                                                            <select id="contractRouteOrigin" name="contractRouteOrigin" onchange='setContractOriginDestination();' style="width:100px" readonly>
                                                                <option value="0">-Select-</option>
                                                            </select>
                                                        </td>
                                                        <td class="text1"><font color="red">*</font>Destination&nbsp;&nbsp;
                                                            <select id="contractRouteDestination" name="contractRouteDestination" onchange='setContractRouteDetails();' style="width:100px" readonly>
                                                                <option value="0">-Select-</option>
                                                            </select>
                                                        </td>
                                                        <td class="text1" colspan="2" >&nbsp;</td>
                                                        <!--                                <td class="text1"><font color="red">*</font>Contract Route</td>
                                                                                        <td class="text1" colspan="3" >
                                                                                            <select id="contractRoute" name="contractRoute" onchange='setOriginDestination();' style="width:350px">
                                                                                                <option value="0">-Select-</option>
                                                                                            </select>
                                                                                        </td>-->
                                                    </tr>
                                                    <input type="hidden" name="origin" value="" />
                                                    <input type="hidden" name="destination" value="" />

                                                    <tr  id="contractRouteDiv2" style="display:none;">
                                                        <td class="text1"><font color="red">*</font>Origin</td>
                                                        <td class="text1">
                                                            <select id="originTemp" name="originTemp" onchange='setDestination();' class="textbox" style="width:150px" readonly>
                                                                <option value="0">-Select-</option>
                                                            </select>
                                                        </td>
                                                        <td class="text1"><font color="red">*</font>Destination</td>
                                                        <td class="text1"><select id="destinationTemp" name="destinationTemp" class="textbox" onchange="setRouteDetails();" style="width:150px" readonly>
                                                                <option value="0">-Select-</option>
                                                            </select></td>
                                                    </tr>

                                                    <tr id="routeChart" style="display:none;" >
                                                        <td colspan="6" align="left" >
                                                            <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" id="routePlan">
                                                                <tr>
                                                                    <td class="contenthead" height="30" ><font color="red">*</font>Order Sequence</td>
                                                                    <td class="contenthead" height="30" ><font color="red">*</font>Point Name</td>
                                                                    <td class="contenthead" height="30" ><font color="red">*</font>Point Type</td>
                                                                    <td class="contenthead" height="30" ><font color="red">*</font>Address</td>
                                                                    <td class="contenthead" height="30" ><font color="red">*</font>Required Date</td>
                                                                    <td class="contenthead" height="30" ><font color="red"></font>Required Time</td>
                                                                </tr>

                                                            </table>
                                                            <br>
                                                            <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" >
                                                                <tr>
                                                                    <td>
                                                                        <input type="button" class="button" id="routePlanAddRow" style="display:none;" value="Interim Point" name="save" onClick="addRouteCourse2();">
                                                                        &nbsp;&nbsp;<input type="button" class="button" id="freezeRoute" style="display:none;"  value="Freeze Route" name="save" onClick="getContractVehicleType();">
                                                                        &nbsp;&nbsp;<input type="button" class="button" id="resetRoute" style="display:none;"  value="Reset" name="save" onClick="resetRouteInfo();">
                                                                        &nbsp;&nbsp;<input type="button" class="button" id="unFreezetRoute" style="display:none;"  value="UnFreeze Route" name="save" onClick="unFreezetRouteFn();">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--                                    <a  class="previoustab" href="#"><input type="button" class="button" value="<back" name="Save" /></a>-->

                                                            <!--                                    <a  class="nexttab" href="#"><input type="button" class="button" value="next>" name="Save" /></a>-->
                                                            </center>
                                                            <br>


                                                        </td>
                                                    </tr>


                                                    <!--                                 <select name="businessType" class="textbox"  id="businessType" style="width:120px;">
                                                                                        <option value="1" selected> Primary  </option>
                                                                                        <option value="2"> Secondary  </option>
                                                                                    </select>-->
                                                    <input type="hidden" name="businessType" value="1" />
                                                    <tr>
                                                        <td class="text2">Special Instruction &nbsp;&nbsp;&nbsp;&nbsp;<textarea rows="1" cols="16" name="consignmentOrderInstruction" id="consignmentOrderInstruction" readonly></textarea></td>
                                                        <!--                            <td class="text2">Multi Pickup</td>-->
                                                        <td class="text2"><input type="hidden" class="textbox" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N"></td>
                                                        <!--                            <td class="text2">Multi Delivery</td>-->
                                                        <td class="text2" colspan="4" ><input type="hidden" class="textbox" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N"></td>
                                                    </tr>

                                                    <tr>


                                                    </tr>
                                                </table>
                                            </div>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                                                <tr>
                                                    <td class="contentBar" colspan="6" ><b>Load Details</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addLoad" style="margin-top: -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>No. Of Pieces</td>
                                                                <!--<td class="contenthead" height="30" >Gross Wt</td>-->
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>UOM</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Length</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Breadth</td>
                                                                <td class="contenthead" height="30" style="width : 120px"><font color='red'>*</font>Height</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Chargeable Wt </td>
                                                                <td class="contenthead" height="30" style="width : 105px"><font color='red'>*</font>Volume</td>
                                                            </tr>
                                                            <br>
                                                            <%int sno=1;%>

                                                                <c:if test="${consignmentArticles != null}">                                                                    
                                                                    <c:forEach items="${consignmentArticles}" var="consignmentArticles">
                                                                    <tr>
                                                                <td><%=sno%></td>
                                                                <td><input type="text" name="noOfPieces" id="noOfPieces<%=sno%>" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value='<c:out value="${consignmentArticles.packageNos}"/>' style="width : 120px" onkeyup="getChargeableWeight('<%=sno%>')" readonly></td>
                                                                <!--<td><input type="text" name="grossWeight" id="grossWeight1" value="" onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" ></td>-->
                                                                <td><select name="uom" id="uom<%=sno%>" style="width : 120px" onchange="getChargeableWeight(1)" readonly><option value="1" selected >Inch</option> <option value="2">Cm</option></select> </td>
                                                                <!--                                        <td><input type="text" name="packagesNos" id="packagesNos1"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" onkeyup="calcTotalPacks(this.value)"></td>-->


                                                                    <script>
                                                                        document.getElementById("uom<%=sno%>").value='<c:out value="${consignmentArticles.unitOfMeasurement}"/>';
                                                                    </script>
                                                                <td><input type="text" name="lengths" id="lengths<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value='<c:out value="${consignmentArticles.length}"/>'  style="width : 120px" onkeyup="getChargeableWeight('<%=sno%>')" readonly></td>
                                                                <td><input type="text" name="widths" id="widths<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value='<c:out value="${consignmentArticles.breadth}"/>'  style="width : 120px" onkeyup="getChargeableWeight('<%=sno%>')" readonly></td>
                                                                <td><input type="text" name="heights" id="heights<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value='<c:out value="${consignmentArticles.height}"/>'  style="width : 120px" onkeyup="getChargeableWeight('<%=sno%>')" readonly></td>
                                                                <td>
                                                                    <input type="text" name="chargeableWeight" id="chargeableWeight<%=sno%>" value='<c:out value="${consignmentArticles.chargeAbleWeigth}"/>' onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value="" style="width : 120px" readonly >
                                                                    <input type="hidden" name="chargeableWeightId" id="chargeableWeightId<%=sno%>" value="">
                                                                    <input type="hidden" name="consignmentArticleId" id="consignmentArticleId<%=sno%>" value='<c:out value="${consignmentArticles.consignmentArticleId}"/>'>
                                                                </td>
                                                                <td><input type="text" name="volumes" id="volumes<%=sno%>"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" value='<c:out value="${consignmentArticles.volume}"/>'  readonly="" style="width : 105px" ></td>
                                                                </tr>

                                                                <%++sno;%>
                                                                </c:forEach>
                                                            </c:if>
                                                            
                                                
                                            </table>
                                            
                                            </td>

                                            </tr>
                                            <tr>
                                                <td class="contenthead" colspan="1">
                                                    <a href="javascript:void(0);" id='load_add'><input type="button" value="Add Row"/></a>
                                                    <a href="javascript:void(0);" id="load_rem"><input type="button" value="Remove Row"/></a>
                                                </td>
                                            </tr>

                                            </table>
                                                 <input type="hidden" name="loadCount" id="loadCount" value="<%=sno%>"/>
                                            <br>
                                            <table>
                                                <tr>

                                                
                                                <td>
                                                    <label class="contentsub">Pcs</label>
                                                    <label id="totalPackages" readonly><c:out value="${totalPackages}"/></label>
                                                    <input type="hidden" id="totalPackage" name="totalPackage" value='<c:out value="${totalPackages}"/>' readonly/>
                                                </td>
                                                <td>
                                                    <label class="contentsub">Gross Weight (Kg)</label>
                                                    <!--<label id="totalWeight">0</label>-->
                                                    <input type="text" id="totalWeightage" name="totalWeightage" class="textbox" style="width : 120px" onchange="checkGrossWeight();reValidateAvailableWeightNew();"  value='<c:out value="${totalWeight}"/>' readonly/>
                                               <input type="hidden" name="remainWeight" id="remainWeight" value="0">
                                                </td>
                                                <td>
                                                    <label class="contentsub">Chargeable Weight (Kg)</label>
                                                    <label id="totalChargeableWeight" style="display : none" readonly>0</label>
                                                    <input type="text" id="totalChargeableWeights" id="totalChargeableWeights" name="totalChargeableWeights" onchange="calcTotalChargeableWeights();calculateToChargebleWeight();reValidateAvailableWeightNew();"  value='<c:out value="${chargeAbleWeigth}"/>' readonly/>
                                                </td>
                                                <td>
                                                    <label class="contentsub">Volume</label>
                                                    <label id="totalVolumes" readonly><c:out value="${totalVolume}"/></label>
                                                    <input type="hidden" id="totalVolume" value='<c:out value="${totalVolume}"/>' name="totalVolume" />
                                                </td>

                                                </tr>
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display:none">

                                                <tr>
                                                    <td class="contentBar" colspan="6" ><b>Dangerous Goods</b></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="1" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="tbl1" style="margin-top : -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 120px">DG Handling Code</td>
                                                                <td class="contenthead" height="30" style="width : 120px">UN /ID Number</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Class Code</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Pkg Instruction</td>
                                                                <td class="contenthead" height="30" style="width : 120px">Pkg Group</td>
                                                                <td class="contenthead" height="30" style="width : 95px">Net Quantity</td>
                                                                <td class="contenthead" height="30" style="width : 95px">Net Units</td>
                                                            </tr>
                                                            <br>

                                                            <tr>
                                                                <td>1</td>
                                                                <td><input type="text" name="dgHandlingCode" id="dgHandlingCode1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="unIdNo" id="unIdNo1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="classCode" id="classCode1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="pkgInstruction" id="pkgInstruction1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="pkgGroup" id="pkgGroup1" value="" style="width : 120px"></td>
                                                                <td><input type="text" name="netQuantity" id="netQuantity1" value="" style="width : 95px"></td>
                                                                <td><input type="text" name="netUnits" id="netUnits1" value="" style="width : 95px"></td>
                                                            </tr>
                                                        </table>
                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contenthead" colspan="1">
                                                        <a href="javascript:void(0);" id='anc_add'><input type="button" value="Add Row"/></a>
                                                        <a href="javascript:void(0);" id="anc_rem"><input type="button" value="Remove Row"/></a></td>

                                                </tr>
                                            </table>
                                            <br>
                                            <input type="hidden" name="newRowFlag"  id="newRowFlag" value="0" >
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" >
                                                <tr>
                                          <td class="contentBar" colspan="5" id="routeInfo"><b>Truck Information,  Route Information </b></td>
                                                      
                                                </tr>
                                                <tr style="display:block;">
                                                     <td class="contenthead" style="width: 100px;" align="center" ><b>Available Capacity:&nbsp;&nbsp;</b><label id="availableCapacity">0</label></td>
                                                     <td class="contenthead" style="width: 100px;" align="center" ><b>Available Volume:&nbsp;&nbsp;</b><label id="availableVolume">0</label></td>
                                                     <td class="contenthead" style="width: 100px;" align="center"><b>Loaded Capacity:&nbsp;&nbsp;</b><label id="assignedCapacity">0</label></td>
                                                     <td class="contenthead" style="width: 100px;" align="center"><b>Loaded CBM:&nbsp;&nbsp;</b><label id="loadedVolume">0</label></td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <table border="1" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="truckTable" style="margin-top : -18px">
                                                            <tr>
                                                                <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                                                <td class="contenthead" height="30" style="width : 70px">Truck Dep. Date</td>
<!--                                                                <td class="contenthead" height="30" >Origin</td>
                                                                <td class="contenthead" height="30" >Destination</td>
                                                                <td class="contenthead" height="30" style="width: 80px">Travel Km</td>
                                                                <td class="contenthead" height="30" style="width: 80px">Travel Hour</td>
                                                                <td class="contenthead" height="30" style="width: 80px">Travel Minute</td>-->
                                                                <td class="contenthead" height="30" >Fleet Type</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Fleet Code</td>
<!--                                                                <td class="contenthead" height="30" style="width: 95px">Vehicle Reg.No</td>-->
                                                                <td class="contenthead" height="30" style="width: 95px">Loaded Weight(KG)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Loaded Volume(CBM)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Available Weight(Ton)</td>
                                                                <td class="contenthead" height="30" style="width: 95px">Available Volume(CBM)</td>
                                                                <!--<td id="receivedPieces"  class="contenthead"  height="30" style="width: 95px">Total Pieces</td>-->

                                                            </tr>
                                                            <br>
                                                                <%int truckRowNo=1;%>
                                                            <c:if test="${consignmentPoint !=null}">
                                                                <c:forEach items="${consignmentPoint}" var="consignmentPoint">
                                                            <tr>
                                                                    
                                                                <input type="hidden" name="consignmentRouteCourseId" id="consignmentRouteCourseId<%=truckRowNo%>"  value='<c:out value="${consignmentPoint.consignmentRouteCourseId}"/>'>
                                                                <td><%=truckRowNo%></td>
                                                                <td><input type="text" class="datepicker" name="truckDepDate" id="truckDepDate<%=truckRowNo%>" value='<c:out value="${consignmentPoint.departOnTime}"/>' onchange="setAvailableVolume(document.getElementById('vehicleNo<%=truckRowNo%>').value,<%=truckRowNo%>)" style="width : 70px" readonly></td>
                                                               
                                                                    <input type="hidden" name="truckOriginId" id="truckOriginId<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityFromId}"/>'>
                                                                    <input type="hidden" name="truckOriginName" id="truckOriginName<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityFromName}"/>' >
                                                               
                                                                
                                                                    <input type="hidden" name="truckDestinationId" id="truckDestinationId<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityToId}"/>'>
                                                                    <input type="hidden" name="truckDestinationName" id="truckDestinationName<%=truckRowNo%>" value='<c:out value="${consignmentPoint.cityToName}"/>'>

                                                                
                                                                
                                                                    <input type="hidden" name="truckRouteId" id="truckRouteId<%=truckRowNo%>" value='<c:out value="${consignmentPoint.routeId}"/>'>
                                                                    <input type="hidden" name="truckTravelKm" id="truckTravelKm<%=truckRowNo%>" value='<c:out value="${consignmentPoint.distance}"/>' style="width: 60px">
                                                               
                                                                
                                                                    <input type="hidden" name="truckTravelHour" id="truckTravelHour<%=truckRowNo%>" value='<c:out value="${consignmentPoint.travelHour}"/>' style="width: 60px">
                                                                
                                                                
                                                                    <input type="hidden" name="truckTravelMinute" id="truckTravelMinute<%=truckRowNo%>" value='<c:out value="${consignmentPoint.travelMinute}"/>'  style="width: 60px">
                                                                
                                                            
                                                                   
                                                            <td>
                                                                <select name="fleetTypeId" id="fleetTypeId<%=truckRowNo%>" onchange="setVehicleNo(this.value,'<%=truckRowNo%>');">
                                                                    <option value="0">--select--</option>
                                                                    <c:if test="${vehicleTypeList != null}">
                                                                        <c:forEach items="${vehicleTypeList}" var="vehType">
                                                                            <option value="<c:out value='${vehType.vehicleTypeId}'/>"><c:out value="${vehType.vehicleTypeName}"/></option>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </select>
                                                               
                                                            </td>
                                                           <script>                                                                    
                                                                  var itemToSelect='<c:out value="${consignmentPoint.fleetTruckId}"/>';                                                                    
                                                                  var myDropdownList=  document.getElementById("fleetTypeId<%=truckRowNo%>");
                                                                   for (iLoop = 0; iLoop< myDropdownList.options.length; iLoop++){
                                                                   if (parseInt(myDropdownList.options[iLoop].value.split("-")[0])== parseInt(itemToSelect)){
                                                                   // Item is found. Set its selected property, and exit the loop
                                                                   myDropdownList.options[iLoop].selected = true;
                                                                   break;
                                                                   }
                                                                   }
                                                                   
                                                                   </script>
                                                            <td>
                                                                <select name="vehicleNo"   id="vehicleNo<%=truckRowNo%>" style="width:95px" onchange="setAvailableVolume(this.value,<%=truckRowNo%>)">
                                                                <!--<select name="vehicleNo"   id="vehicleNo<%=truckRowNo%>" style="width:95px" onchange="reValidateAvailableWeight(this.value,<%=truckRowNo%>)">-->
                                                                        <option value='<c:out value="${consignmentPoint.vehicleID}"/>-<c:out value="${consignmentPoint.vehicleNo}"/>'><c:out value="${consignmentPoint.vehicleNo}"/></option>
                                                                    </select>
                                                                <input type="hidden" name="vehicleNogg" id="vehicleNo11<%=truckRowNo%>" style="width:95px"  value='<c:out value="${consignmentPoint.vehicleNo}"/>' >
                                                                <input type="hidden" name="vehicleId" id="vehicleId<%=truckRowNo%>" style="width:95px" value='<c:out value="${consignmentPoint.vehicleID}"/>' readonly>
                                                                <input type="hidden" name="usedVehicleStatus" id="usedVehicleStatus<%=truckRowNo%>" style="width:95px" value='0' >
                                                            </td>
                                                            <input type="hidden" name="vehicleRegNo" id="vehicleRegNo<%=truckRowNo%>" style="width:95px"   maxlength="15" value='0'>
                                                            <td>
                                                           <input type="text" name="usedCapacity" id="usedCapacity<%=truckRowNo%>" style="width:95px" onchange="setUsedCapacity(1);" value='<c:out value="${consignmentPoint.truckUsedCapacity}"/>' readonly>
                                                            </td>
                                                            <td>
                                                            <input type="text" name="usedVol" id="usedVol<%=truckRowNo%>" style="width:95px" value='<c:out value="${consignmentPoint.truckUsedVol}"/>' readonly>
                                                            </td>
                                                            <td>                                                                
                                                                <input type="text" name="availableCap" id="availableCap<%=truckRowNo%>" style="width:95px" value='<c:out value="${consignmentPoint.truckAvailable}"/>' readonly>
                                                            </td>
                                                            <td>
                                                                                                           
                                                                <input type="text" name="assignedCap" id="assignedCap<%=truckRowNo%>" style="width:95px" value='<c:out value="${consignmentPoint.truckAssigned}"/>' maxlength="10" readonly />
                                                            </td>
<!--                                                            <td>
                                                                <input type="text" name="receivedPackages" id="receivedPackages<%=truckRowNo%>" style="width:95px" value="<c:out value="${consignmentPoint.receivedPackage}"/>" readonly>
                                                            </td>-->
                                                                                                                        
                                                </tr>
                                                                    <c:if test="${consignmentPoint.fleetStatus == '1'}">
                                                                        <script>
                                                                            document.getElementById("truckDepDate"+<%=truckRowNo%>).d = true;
                                                                            document.getElementById("fleetTypeId"+<%=truckRowNo%>).disabled = true;
                                                                            document.getElementById("vehicleNo"+<%=truckRowNo%>).disabled = true;
                                                                            document.getElementById("usedCapacity"+<%=truckRowNo%>).readOnly = true;
                                                                            document.getElementById("usedVol"+<%=truckRowNo%>).readOnly = true;
                                                                            document.getElementById("availableCap"+<%=truckRowNo%>).readOnly = true;
                                                                            document.getElementById("assignedCap"+<%=truckRowNo%>).readOnly = true;
                                                                            document.getElementById("receivedPackages"+<%=truckRowNo%>).readOnly = true;
                                                                            </script>
                                                                    </c:if> 
                                                <%++truckRowNo;%>
                                                </c:forEach>
                                                </c:if>
                                            </table>
                    <input type="hidden" name="truckCount" id="truckCount" value="<%=truckRowNo-1%>"/>
                                            
                                                    </td>
                                                    
                                                    <tr>
                                                        <td class="contenthead" colspan="1">
                                                        <a href="javascript:void(0);" id='truck_add'><input type="button" value="Add Row"/></a>
                                                        <a href="javascript:void(0);" id="truck_rem"><input type="button" value="Remove Row"/></a>
                                                        </td>
                                                    </tr>
                                                    </table>

                                                    <br>

                                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display : none">

                                                        <tr>
                                                            <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
                                                        </tr>


                                                        <!--                            <td class="text1">Service Type</td>
                                                                                    <td class="text1">
                                                                                        <select name="serviceType" class="textbox"  id="paymentType" style="width:120px;">
                                                                                            <option value="0" selected>--Select--</option>
                                                                                            <option value="1"> FTL </option>
                                                                                            <option value="2"> LTL </option>
                                                                                        </select>
                                                                                    </td>-->
                                                        <input type="hidden" name="serviceType" value="1" />
                                                        <input type="hidden" name="vehTypeId" value="0" />
                                                        <tr id="vehicleTypeDiv" style="display:none;">
                                                            <td class="text2">Vehicle Type</td>
                                                            <td class="text2"  colspan="5" > <select name="vehTypeIdTemp" id="vehTypeIdTemp" class="textbox"  style="width:120px;" onchange="setFreightRate();" readonly>
                                                            <c:if test="${vehicleTypeList != null}">
                                                                <option value="0" selected>--Select--</option>
                                                                <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                                                    <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                                                </c:forEach>
                                                            </c:if>
                                                        </select></td>
                                                </tr>
                                                <tr id="vehicleTypeContractDiv" style="display:none;">
                                                    <td class="text2">
                                                        Vehicle Type &nbsp;&nbsp;

                                                    </td>
                                                    <td class="text2"  colspan="5" > <select name="vehTypeIdContractTemp" id="vehTypeIdContractTemp"  onchange="setFreightRate1();" class="textbox"  style="width:120px;" readonly>
                                                            <option value="0" selected>--Select--</option>
                                                        </select></td>
                                                </tr>
                                                <!--                            <tr>
                                                                            <td class="text1"><font color="red">*</font>Reefer Required</td>
                                                                            <td class="text1" colspan="5" >
                                                                                <input type="text" readonly  name="reeferRequired" id="reeferRequired"  class="textbox"  value="" />
                                                                            </td>
                                                                        </tr>-->
                                                <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="textbox"  value="" />
                                                
                                                <tr>
                                                    <!--                            <td class="text2"><font color="red">*</font>Vehicle Required Date</td>
                                                                                <td class="text2">-->
                                                <input type="hidden" class="textbox" name="destinationId" id="destinationId" >
                                                <input type="hidden" class="textbox" name="routeContractId" id="routeContractId" >
                                                <input type="hidden" class="textbox" name="routeId" id="routeId" >
                                                <input type="hidden" class="textbox" name="routeBased" id="routeBased" >
                                                <input type="hidden" class="textbox" name="contractRateId" id="contractRateId" >
                                                <input type="hidden" class="textbox" name="totalKm" id="totalKm" >
                                                <input type="hidden" class="textbox" name="totalHours" id="totalHours" >
                                                <input type="hidden" class="textbox" name="totalMinutes" id="totalMinutes" >
                                                <input type="hidden" class="textbox" name="totalPoints" id="totalPoints" >
                                                <input type="hidden" class="textbox" name="vehicleTypeId" id="vehicleTypeId" >
                                                <input type="hidden" class="textbox" name="rateWithReefer" id="rateWithReefer" >
                                                <input type="hidden" class="textbox" name="rateWithoutReefer" id="rateWithoutReefer" >
                                                <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >

                                                <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" >
                                                <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute" >

                                                <td class="text2">Special Instruction</td>
                                                <td class="text2" colspan="5" ><textarea rows="1" cols="16" name="vehicleInstruction" id="vehicleInstruction"></textarea></td>
                                                </tr>
                                                
                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable1">
                                                <tr>
                                                    <td class="contenthead" colspan="4">RCS (Freight Check Details) </td>
                                                </tr>

                                                <tr>
                                                    <td class="text1"><font color="red">*</font>Document & Freight Receipt</td>
                                                    <td class="text1">
                                                        <input type="text" name="freightReceipt" id="freightReceipt" value='<c:out value="${freightReceipt}"/>' class="textbox" style="width: 120px" readonly/>
                                                    </td>
                                                    <td class="text1"><font color="red">*</font>Warehouse</td>
                                                    <td class="text1">
                                                        <input type="text" name="wareHouseName" id="wareHouseName" value='<c:out value="${wareHouseName}"/>' class="textbox" style="width: 80px" readonly/>
                                                        <input type="text" name="wareHouseLocation" id="wareHouseLocation" value='<c:out value="${wareHouseLocation}"/>' class="textbox" style="width: 80px" readonly/>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="text1"><font color="red"></font>Shipment Acceptance Date</td>
                                                    <td class="text1">
                                                        <input type="text" name="shipmentAcceptanceDate" id="shipmentAcceptanceDate" value='<c:out value="${shipmentAcceptanceDate}"/>' class="datepicker" style="width: 120px" readonly/>
                                                    </td>
                                                    <td class="text1"><font color="red"></font>Shipment Acceptance Time</td>
                                                    <td class="text1">
                                                        HH:<select name="shipmentAccpHour" id="shipmentAccpHour" class="textbox" readonly><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                        MI:<select name="shipmentAccpMinute" id="shipmentAccpMinute" class="textbox" readonly><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                                        <script>
                                                            document.getElementById("shipmentAccpHour").value='<c:out value="${shipmentAccpHour}"/>';
                                                            document.getElementById("shipmentAccpMinute").value='<c:out value="${shipmentAccpMinute}"/>';
                                                        </script>
                                                    </td>

                                                </tr>
                                                <!--                        <tr>
                                                                            <td class="text1"><font color="red"></font>Freight Type</td>
                                                                            <td class="text1">
                                                                                <input type="text" name="freightType" id="freightType" value="" class="textbox" style="width: 120px"/>
                                                                            </td>

                                                                        </tr>-->

                                            </table>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2"><font color="red">*</font>Consignor Name</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName"  value='<c:out value="${consignorName}"/>' readonly></td>
                                                    <td class="text2"><font color="red">*</font>Mobile No</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consignorPhoneNo" maxlength="12" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" value='<c:out value="${consignorMobile}"/>'  readonly></td>
                                                    <td class="text2"><font color="red">*</font>Address</td>
                                                    <td class="text2"><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress" readonly><c:out value="${consignorAddress}"/></textarea> </td>
                                                </tr>
                                            </table>
                                            <br>
                                            <br>
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2"><font color="red">*</font>Consignee Name</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName"  value='<c:out value="${consigneeName}"/>' readonly></td>
                                                    <td class="text2"><font color="red">*</font>Mobile No</td>
                                                    <td class="text2"><input type="text" class="textbox" id="consigneePhoneNo" maxlength="12"  onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" value='<c:out value="${consigneeMobile}"/>' readonly></td>
                                                    <td class="text2"><font color="red">*</font>Address</td>
                                                    <td class="text2"><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress" readonly><c:out value="${consigneeAddress}"/></textarea> </td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <br/>
                                            <!--                    <center>
                                                                    <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" ></a>
                                                                </center>-->
                                            </div>

                                            
                                            <!--                <div id="paymentDetails">-->
                                            <table border="0" class="border" align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="creditLimitTable" style="display: none">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6" >Credit Limit Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text2">Credit Days &nbsp; </td>
                                                    <td class="text2"><label id="creditDaysTemp" ></label><input type="hidden" name="creditDays" id="creditDays" value="0" /> </td>
                                                    <td class="text2" align="left" >Credit Limit &nbsp; </td>
                                                    <td class="text2"><label id="creditLimitTemp"></label><input type="hidden" name="creditLimit" id="creditLimit" value="0" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text1" align="left" >Customer Rank &nbsp; </td>
                                                    <td class="text1"><label id="customerRankTemp"></label><input type="hidden" name="customerRank" id="customerRank" value="0" /></td>
                                                    <td class="text1" align="left" >Approval Status&nbsp; </td>
                                                    <td class="text1"><label id="approvalStatusTemp"></label><input type="hidden" name="approvalStatus" id="approvalStatus" value="1" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="text2" align="left" >Out Standing &nbsp; </td>
                                                    <td class="text2"><label id="outStandingTemp"></label><input type="hidden" name="outStanding" id="outStanding" value="0" /></td>
                                                    <td class="text2" align="left" >Out Standing Date&nbsp; </td>
                                                    <td class="text2"><label id="outStandingDateTemp"></label><input type="hidden" name="outStandingDate" id="outStandingDate" valu="11-11-2015" /></td>
                                                </tr>
                                            </table>
                                            <table  align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="contractFreightTable" >
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6" >Freight Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="contentsub"  height="30"></td>
                                                    <td class="contentsub"  height="30" colspan="5">Contract Details&nbsp;&nbsp;
                                                        <input type="radio" name="contract" id="contract1" value="1" onclick="checkContract(1);checkContractDetails(this.value);" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentsub"  height="30" >Currency</td>
                                                    <td class="text1"><label  id="autoRateTd">Auto Rate</label></td>
                                                    <td class="text1">Full Truck Rate</td>
                                                    <td class="text1"></td>
                                                </tr>
                                                

                                                <tr>
                                                    <td class="contentsub"  height="30" ></td>
                                                    <td class="text1"><input type="radio" name="rateMode" id="rate1"  value="1"  onclick="setTextBoxEnable(this.value),autoRateWeight(),checkContractDetails(this.value);" readonly/></td>
                                                    <td class="text1"><input type="radio" name="rateMode" id="rate2" value="2" onclick="fullTruckRate(),setTextBoxEnable(this.value),checkContractDetails(this.value);" readonly/></td>
                                                    <td class="text1"><input type=text" name="rateValue" id="rateValue"  style="width:60px" value=""  readonly/></td>
                                                    <td class="text1"></td>

                                                </tr>
                                                <tr>
                                                    <td class="contentsub"  height="30"></td>
                                                    <td class="contentsub"  height="30" colspan="5">Non Contract&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="contract" id="contract2" value="2" value='<c:out value="${contract}"/>' readonly/></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentsub" >
                                                        <select name="currencyType" id="currencyType">
                                                            <option value="1">INR</option>
                                                        </select>
                                                    </td>
                                                    <td class="text1"><label  id="autoRateTd">Per Kg Rate</label></td>
                                                    <td class="text1">Full Truck Rate</td>
                                                    <td class="text1">Freight Amount</td>
                                                </tr>
                                                <tr>
                                                    <td class="contentsub" ></td>
                                                    <td class="text1"><input type="radio" name="rateMode" id="rate3" value='<c:out value="${rateMode}"/>'  readonly  />
                                                        <input type="text" name="perKgRate" id="perKgRate"  value='<c:out value="${perKgRate}"/>' class="textbox" readonly/></td>
                                                    <td class="text1"><input type="radio" name="rateMode" id="rate4" size="10" value="2"   readonly/></td>
                                                    <td class="text1"><input type=text" name="rateValue1" id="rateValue1"  style="width:60px"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly/></td>
                                                </tr>
                                                <tr>

                                                </tr>
                                                

                                                <tr style="display: none">
                                                    <td class="text1">Billing Type</td>
                                                    <td class="text1"><label id="billTypeName"></label></td>
                                                    <td class="text1" align="right" >Estimated Freight Charges &nbsp; </td>
                                                    <td class="text1">INR.
                                                        <input type="text" class="textbox" name="totFreightAmount" id="totFreightAmount" value="0"/>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        Freight Agree &nbsp;&nbsp;
                                                        <input type="checkbox" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();" class="textbox" readonly/>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table style="display: none" border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinFreightTable">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="6" >Walkin Freight Details</td>
                                                </tr>
                                                <tr>
                                                    <td class="text1">Billing Type</td>
                                                    <td class="text1"><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="texttbox" onchange="displayWalkIndetails(this.value)" style="width: 120px" readonly>
                                                            <option value="0" selected>--Select--</option>
                                                            <option value="1" >Fixed Freight</option>
                                                            <option value="2" >Rate Per KM</option>
                                                            <option value="3" >Rate Per Kg</option>
                                                        </select></td>
                                                    <td class="text1">Freight Charges (INR.)</td>
                                                    <td class="text1">
                                                        <input type="text" readonly class="textbox" name="totFreightAmount1" id="totFreightAmount1" value="0"/>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;

                                                    </td>
                                                </tr>
                                                <tr id="WalkInptp" style="display: none">
                                                    <td class="text2">Freight With Reefer</td>
                                                    <td class="text2"><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                    <td class="text2">Freight Without Reefer</td>
                                                    <td class="text2"><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                </tr>
                                                <tr id="WalkInPtpw" style="display: none">
                                                    <td class="text2">Rate With Reefer/Kg</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                    <td class="text2">Rate Without Reefer/Kg</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                </tr>
                                                <tr id="WalkinKilometerBased" style="display: none">
                                                    <td class="text2">Rate With Reefer/Km</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                    <td class="text2">Rate Without Reefer/Km</td>
                                                    <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()" readonly/></td>
                                                </tr>
                                            </table>
                                            
                                            <br/>
                                            <br/>
                                            <!--<textarea name="orderReferenceRemarks11" id="orderReferenceRemarks111" style="display: none"></textarea>-->
                                            <table  border="0" class="border" align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="contractFreightTable1">
                                                <tr>
                                                    <td class="contentsub"  height="30" colspan="1">Sno</td>
                                                    <td class="contentsub"  height="30" colspan="3" style="width : 500px;">Remarks</td>
                                                </tr>
<!--                                                <tr>
                                                    <td class="text2">1</td>
                                                    <td class="text2">
                                                        <textarea name="orderReferenceRemarks" id="orderReferenceRemarks" style="width : 500px; height : 20px"><c:out value="${orderReferenceRemarks}"/></textarea>
                                                    </td>

                                                </tr>-->

                                            </table>
                                           
                                            <table border="0" class="border" align="center" width="97.5%" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <td class="contentsub"  height="30" >
                                                        <a href="javascript:void(0);" id='remarks_add'><input type="button" value="Add Row"/></a>
                                                        <a href="javascript:void(0);" id="remarks_rem"><input type="button" value="Remove Row"/></a>
                                                    </td>
                                                </tr>
                                            </table>
                                           
                                            <input type="hidden" class="textbox" id="subTotal" name="subTotal"  value="0" readonly="">
                                            
                                            <br/>
                                            
                                            <input align="right" value="" type="hidden" readonly class="textbox" id="totalCharges" name="totalCharges" >
                                            <br/>
                                            <br/>

<!--                                            <center>
                                                                        <input type="button" class="button" name="Save" value="Estimate Freight" onclick="estimateFreight();" >
                                                <div id="orderButton">
                                                    <input type="button" class="button" name="Save" value="Save" id="createOrder" onclick="checkContractDetails(this.value);cretaeOrderSubmit(this.value);" >
                                                    <input type="button" class="button" name="Back" value="Back" id="Back" onclick="cretaeOrderSubmit(this.value);" >
                                                </div>
                                            </center>-->
                                                            <!--</div>-->

                                            </div>
                                                    </c:if>
          <!------------------------------------------------------------>
             
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                
        </form>
            

      <script>
         var awbNo =document.getElementById("awbNo").value;
          $(document).ready(function() {
              
            // Use the .autocomplete() method to compile the list based on input from user
            $('#awbNo').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getAwbNoList.do",
                        dataType: "json",
                        data: {
                            awbNo: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            
                            var items = data;
                            if (items == '') {
                                $('#awbNo').val('');
                                alert("AWB No Does not Exist");
                            } else {
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#awbNo").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    $itemrow.find('#awbNo').val(value);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
                 
             };
           

        });
          
      function viewAwbDetails() {
       var awbNo =document.getElementById("awbNo").value;
        var url = '/throttle/viewAjaxAwbDetails.do';
        $.ajax({
            url: url,
            dataType: "json",
            data: {
                awbNo: awbNo
            },
            success: function(temp) {
                if (temp != '') {
                    $.each(temp, function(i, data) {
                        $('#consignmentId').val(data.Id);
                    });
                } else {
                    $('#consignmentId').empty();
                }
                submitPage();

            }
        });
       }
         </script>
            
     <script type="text/javascript">
        $(function () {
        $('#orderReferenceAwb,#orderReferenceAwbNo,#orderReferenceEnd').keyup(function (e) {
        if ($(this).val().length == $(this).attr('maxlength'))
            $(this).next(':input').focus()
        })
        })
    </script> 
            
            <script type="text/javascript">
                function submitPage(){
                var awbNo =document.getElementById("awbNo").value;
                var consignmentId=document.getElementById("consignmentId").value;
                document.consignments.action = '/throttle/getConsignmentDetailsForAwb.do?awbNo='+awbNo+'&consignmentId='+consignmentId;
                document.consignments.submit();
                }
                
            </script>
            
             <script>
                                                var contractTypeId = '<c:out value="${contractId}"/>';
                                                var rateMode = '<c:out value="${rateMode}"/>';
                                                var rateValue = '<c:out value="${rateValue}"/>';
                                                var perKgRate = '<c:out value="${perKgRate}"/>';
                                                var freightCharges = '<c:out value="${freightCharges}"/>';

//                    alert("contractTypeId"+contractTypeId+" rateMode"+rateMode+"rateValue"+rateValue+"perKgRate"+perKgRate+"freightCharges"+freightCharges);
                                                if (contractTypeId == "1") {
                                                    document.getElementById("contract1").checked = true;

                                                    document.getElementById("contract2").checked = false;
                                                    if (rateMode == "1") {
                                                        document.getElementById('rateValue').value = freightCharges;

                                                        document.getElementById('rate1').checked = true;
                                                        document.getElementById("rate2").disabled = true;

                                                    } else {
                                                        document.getElementById('rate1').disabled = true;
                                                        document.getElementById("rate2").checked = true;
                                                        document.getElementById('rateValue').value = freightCharges;

                                                    }
                                                    document.getElementById('rate3').disabled = true;
                                                    document.getElementById('rate4').disabled = true;
                                                    document.getElementById('perKgRate').readOnly = false;
                                                    document.getElementById('rateValue1').readOnly = true;
                                                } else {
                                                    document.getElementById('rateValue').readOnly = false;
                                                    document.getElementById("contract1").checked = false;
                                                    document.getElementById("contract1").disabled = true;
                                                    document.getElementById("contract2").checked = true;
                                                    document.getElementById('rate1').disabled = true;
                                                    document.getElementById("rate2").disabled = true;
                                                    if (rateMode == "3") {
                                                        document.getElementById('rate3').checked = true;
                                                        document.getElementById('rate3').disabled = false;
                                                        document.getElementById('rate4').disabled = false;
                                                        document.getElementById('perKgRate').value = perKgRate;
                                                        document.getElementById('rateValue1').value = freightCharges;
                                                        document.getElementById('perKgRate').readOnly = false;
                                                        document.getElementById('rateValue1').readOnly = true;
                                                    } else {
                                                        document.getElementById('rate4').checked = true;
                                                        document.getElementById('rate3').disabled = true;
                                                        document.getElementById('rateValue1').value = freightCharges;
                                                        document.getElementById('perKgRate').readOnly = false;
                                                        document.getElementById('rateValue1').readOnly = true;
                                                    }
                                                }
                                                document.getElementById("contract1").disabled = false;
                                                document.getElementById("contract2").disabled = false;

                                            </script>
                                            <script>

                                                var totalUsedCapacity = 0;
                                                var totalUsedVolume = 0;
                                                var usedCapacityTag = document.getElementsByName("usedCapacity");
                                                var usedVolTag = document.getElementsByName("usedVol");
                                                var vehicleNoTag = document.getElementsByName("vehicleNo");
                                                var chargableWeight = document.getElementById("totalChargeableWeights").value;
                                                var totalVolume = document.getElementById("totalVolume").value;
                                                for (var i = 0; i < vehicleNoTag.length; i++) {
                                                    totalUsedCapacity = parseFloat(totalUsedCapacity) + parseFloat(usedCapacityTag[i].value);
                                                    totalUsedVolume = parseFloat(totalUsedVolume) + parseFloat(usedVolTag[i].value);
                                                }
                                                //alert(totalUsedVolume+" "+totalVolume);
                                                document.getElementById("assignedCapacity").innerHTML = totalUsedCapacity;
                                                document.getElementById("availableCapacity").innerHTML = (parseFloat(chargableWeight) - parseFloat(totalUsedCapacity)).toFixed(2);
                                                document.getElementById("loadedVolume").innerHTML = totalUsedVolume;
                                                document.getElementById("availableVolume").innerHTML = (parseFloat(totalVolume) - parseFloat(totalUsedVolume)).toFixed(2);
                                            </script>
    </body>
    </html>
