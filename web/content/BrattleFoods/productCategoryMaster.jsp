 <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
//    $(document).ready(function() {
//
//        $("#datepicker").datepicker({
//            showOn: "button",
//            buttonImage: "calendar.gif",
//            buttonImageOnly: true
//
//        });
//
//
//
//    });
//
//    $(function() {
//        //	alert("cv");
//        $(".datepicker").datepicker({
//            /*altField: "#alternate",
//             altFormat: "DD, d MM, yy"*/
//            changeMonth: true, changeYear: true
//        });
//
//    });
    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#productCategoryNameStatus").text();
        if(document.getElementById("productCategoryName").value == "") {
            errStr = "Please enter productName.\n";
            alert(errStr);
            document.getElementById("productCategoryName").focus();
        } else if (document.getElementById("hsnCode").value == "") {
            errStr ="Please Enter the HSN Code.\n";
            alert(errStr);
            document.getElementById("hsnCode").focus();
        } else if (document.getElementById("productCode").value == "") {
            errStr ="Please Enter the Product Code.\n";
            alert(errStr);
            document.getElementById("productCode").focus();
        } else if (document.getElementById("skuCode").value == "") {
            errStr ="Please Enter the SKU Code.\n";
            alert(errStr);
            document.getElementById("skuCode").focus();
        } else if (document.getElementById("whId").value == "") {
            errStr ="Please select the Warehouse Name.\n";
            alert(errStr);
            document.getElementById("whId").focus();
        }else if (document.getElementById("binId").value == "") {
            errStr ="Please select the Bin Name.\n";
            alert(errStr);
            document.getElementById("binId").focus();
        } else if (document.getElementById("custId").value == "") {
            errStr ="Please select the Customer Name.\n";
            alert(errStr);
            document.getElementById("custId").focus();
        } else if (nameCheckStatus != "") {
            errStr ="Product Name Already Exists.\n";
            alert(errStr);
            document.getElementById("productCategoryName").focus();
        }

        if(errStr == "") {
            document.productCategory.action=" /throttle/saveProductCategory.do";
            document.productCategory.method="post";
            document.productCategory.submit();
        }



    }
     function setValues(sno,productCategoryName,hsnCode,productCategoryId,status,code,skuCode,whId,customerId,binId){
        var count = parseInt(document.getElementById("count").value);
       // alert("fhskf=="+binId);
        if(whId!=0){
            var whIds=whId.split(',');
            var select=document.getElementById('whId');
            for ( var i = 0, l = select.options.length, o; i < l; i++ )
            {
                o = select.options[i];
                if ( whIds.indexOf( o.value ) != -1 )
                {
                 o.selected = true;
                }else{
                    o.selected=false;
                }
            }
        }else{
            var select=document.getElementById('whId');
            for ( var i = 0, l = select.options.length, o; i < l; i++ )
            {
                o = select.options[i];
                    o.selected=false;
                }
            }
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("productCategoryId").value = productCategoryId;
        document.getElementById("productCategoryName").value = productCategoryName;
        document.getElementById("hsnCode").value = hsnCode;
        document.getElementById("productCode").value = code;
        document.getElementById("skuCode").value = skuCode;
        document.getElementById("binId").value = binId;
        document.getElementById("custId").value = customerId;
        document.getElementById("status").value = status;
    }







    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }



 var httpRequest;
    function checkproductCategoryName() {

        var productCategoryName = document.getElementById('productCategoryName').value;

            var url = '/throttle/checkProductCategory.do?productCategoryName=' + productCategoryName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#productCategoryNameStatus").text('Please Check Product Category Name: ' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#productCategoryNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Product Master" text="Product Master"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Product Master" text="Product Master"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body onload="document.productCategory.productCategoryName.focus();">


        <form name="productCategory"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="productCategoryId" id="productCategoryId" value=""  />
                <tr>
                <!--<table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">-->
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                <tr>
                    <th class="contenthead" colspan="8" >Product Master</th>
                </tr>
                        </thead>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="productCategoryNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Product Name</td>
                    <td class="text1"><input type="text" name="productCategoryName" id="productCategoryName" class="form-control" style="width:240px;height:40px" maxlength="50" onchange="checkproductCategoryName();" autocomplete="off"/></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>HSN Code</td>
                    <td class="text1"><input type="text" name="hsnCode" id="hsnCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                </tr>

                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Product Code</td>
                    <td class="text1"><input type="text" name="productCode" id="productCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>SKU Code</td>
                    <td class="text1"><input type="text" name="skuCode" id="skuCode" class="form-control" style="width:240px;height:40px" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Warehouse Name</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:240px;height:80px" name="whId" id="whId"  multiple="multiple">
                            <c:if test = "${wareHouseList != null}" >
                                <c:forEach items="${wareHouseList}" var="ware"> 
                                    <option value='<c:out value="${ware.whId}" />'><c:out value="${ware.whName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Bin</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:240px;height:40px" name="binId" id="binId">
                            <c:if test = "${binList != null}" >
                                <c:forEach items="${binList}" var="bin"> 
                                    <option value='<c:out value="${bin.binId}" />'><c:out value="${bin.binName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select></td>
                    </tr>
                    <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:240px;height:40px" name="custId" id="custId" >
                            <option value=''>--select--</option>                            
                            <c:if test = "${customerList != null}" >
                                <c:forEach items="${customerList}" var="cust"> 
                                    <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text1">
                        <select  align="center" class="form-control" style="width:240px;height:40px" name="status" id="ActiveInd" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                    <td colspan="2">&emsp;</td>
                </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>

                       <table class="table table-info mb30 table-hover" id="table" >	
                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Product Name</h3></th>
                        <th><h3>HSN Code</h3></th>
                        <th><h3>Product Code</h3></th>
                        <th><h3>SKU Code</h3></th>
                        <th><h3>Warehouse Name</h3></th>
                        <th><h3>Customer Name</h3></th>
                        <th><h3>Bin Name</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${productCategoryList != null}">
                        <c:forEach items="${productCategoryList}" var="pc">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.productCategoryName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.hsnCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.productCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.skuCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.whName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.custName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${pc.binName}" /></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${pc.productCategoryName}" />','<c:out value="${pc.hsnCode}" />','<c:out value="${pc.productCategoryId}" />','<c:out value="${pc.status}" />','<c:out value="${pc.productCode}" />','<c:out value="${pc.skuCode}" />','<c:out value="${pc.whId}" />','<c:out value="${pc.customerId}" />','<c:out value="${pc.binId}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>