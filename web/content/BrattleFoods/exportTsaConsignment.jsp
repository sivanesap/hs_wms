<%-- 
    Document   : exportTsaConsignment
    Created on : Mar 4, 2015, 1:06:20 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javarscript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });

    $(document).ready(function () {



        $(function () {
            $("#flightDate").datepicker({
                changeMonth: true, changeYear: true
            });
        });
    });


</script>


<html>
    <body onload="egmIgm()">
    <style>
        body {
            font:13px verdana;
            font-weight:normal;
        }
    </style>
    <form name="exportTsa"  method="post" >
        <br>
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <table cellpadding="0" cellspacing="0" width="800" border="0px" class="border" align="center">
            <tr>
                <td align="center" style="background-color: silver">TT AVIATION HANDLING SERVICES PVT LTD, CHENNAI - 600 027.<br>TRANSHIPMENT APPLICATION</td>
            </tr>
        </table>                    
        <br>
        <br>
        <div id="tabs">
            <ul>
                <li><a href="#address" id="address"><span>Consignment Address Details</span></a></li>
                <li><a href="#showRouteCourse" id="showRouteCourse"><span>Customs Details</span></a></li>
            </ul>
            <div id="address">
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable" >
                    <tr>
                        <td class="contenthead" colspan="4" >Consigment Address</td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Shipment Type</td>
                        <td class="text2">
                            <select class="text" name="shipmentType" id="shipmentType" onchange="egmIgm();
                                    checkShipmentType(this.value);">
                                <option value="1">Import</option>
                                <option value="2">Export</option>
                            </select>

                        </td>
                    </tr>

                    <td class="text2" style="text-align: center">Enter From Address</td>
                    <td class="text2">
                        <textarea name="fromAddress" id="fromAddress" class="textbox">
                            <c:out value="${fromAddress}"/>
                        </textarea>
                    </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter To Address</td>
                        <td class="text2">
                            <textarea name="toAddress" id="toAddress" class="textbox">
                                <c:out value="${toAddress}"/>
                            </textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="showRouteCourse">
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable" >
                    <tr>
                        <td class="contenthead" colspan="4" >Fleet Details</td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter Flight No</td>
                        <td class="text2">
                            <input type="text" name="fleetDetails" id="fleetDetails" class="textbox" value="<c:out value="${fleetDetails}"/>" maxlength="15"/>

                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter TSA Date</td>
                        <td class="text2">
                            <input type="text" name="flightDate" id="flightDate" class="textbox" value="<c:out value="${flightDate}"/>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter TP No</td>
                        <td class="text2">
                            <input type="text" name="consignmentTpNo" id="consignmentTpNo" class="textbox" value="<c:out value="${consignmentTpNo}"/>" maxlength="15"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter EFO No</td>
                        <td class="text2">
                            <input type="text" name="consignmentEfoNo" id="consignmentEfoNo" class="textbox" value="<c:out value="${consignmentEfoNo}"/>" maxlength="15"/>
                        </td>
                    </tr>


                    <tr>
                        <td class="text2" style="text-align: center"><label id="movement1">&nbsp;</label></td>
                        <td class="text2">
                            <input type="text" name="consignmentEgmNo" id="consignmentEgmNo" class="textbox" value="<c:out value="${consignmentEgmNo}"/>" maxlength="15"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="text2" style="text-align: center">Enter Fleet Details</td>
                        <td class="text2">
                            <textarea name="fleetDetails1" id="fleetDetails1" class="textbox">
                                <c:out value="${fleetDetails}"/>
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter Transhipment No</td>
                        <td class="text2">
                            <input type="text" name="transhipmentNo" id="transhipmentNo" class="textbox" value="<c:out value="${transhipmentNo}"/>" maxlength="15"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Remark</td>
                        <td class="text2">
                            <textarea name="customsRemark" id="customsRemark" class="textbox" >
                                <c:out value="${customsRemark}"/>
                            </textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
        <br>
        <c:if test="${tsiViewList != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                <thead>
                    <tr height="30">
                        <th align="center"><h3>S.No</h3></th>
                        <th align="center"><h3>AWBNo</h3></th>
                        <th align="center" style="display: none" id="sbnoTh"><h3>SBNo</h3></th>
                            <%
                                                   String branchId = "";
                                                   if (session.getAttribute("BranchId") != null) {
                                                       branchId = (String) session.getAttribute("BranchId");
                                                   }
                                                   if ("3".equals(branchId)) {
                            %>
                        <th align="center"><h3>HAWB No</h3></th>
                        <th align="center"><h3>IGM No</h3></th>
                        <th align="center"><h3>Flight No</h3></th>
                            <%}%>
                        <th align="center" id="exporterTh" style="display: none"><h3>Exporter</h3></th>
                        <th align="center"><h3>Consignee</h3></th>
                        <th align="center"><h3>DespOfGoods</h3></th>
                        <th align="center"><h3>Origin</h3></th>
                        <th align="center"><h3>Destination</h3></th>
                        <th align="center"><h3>TotalPcs</h3></th>
                        <th align="center"><h3>Total Grs.Weight</h3></th>
                        <th align="center"><h3>ValueOfGoodsINR</h3></th>

                    </tr>
                </thead>
                <% int index = 1;%>
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <tr>
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <td class="<%=classText%>"  ><%=index%></td>
                        <td class="<%=classText%>"><c:out value="${tsiViewList.consignmentAwbNo}"/></td>

                        <td class="<%=classText%>" id="sbnoTd<%=index%>" style="display: none" > 
                            <input type="hidden" name="consignmentOrderNos"  id="consignmentOrderId<%=index%>" class="textbox"  value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>
                            <input type="text" name="consignmentSbNo"  id="consignmentSbNo<%=index%>" class="textbox" value=""/>
                            <input type="hidden" name="routeContractId"  id="routeContractId<%=index%>" class="textbox" maxlength="10" value="<c:out value="${tsiViewList.routeContractId}"/>"/>
                        </td>
                    <script>
                        $(document).ready(function () {
                            $('#consignmentSbNo' +<%=index%>).val('0');
                        });
                        function checkShipmentType(val) {
//                            alert("Value " + val + "index" +<%=index%>);
                            var consignmentOrderNos = document.getElementsByName("consignmentOrderNos");
                            if ("2" == val) {
                                $('#sbnoTh').show();
                                $('#exporterTh').show();
                                for (var i = 0; i <= consignmentOrderNos.length; i++) {
                                    $('#sbnoTd' + i).show();
                                    $('#exporterTd' + i).show();
                                }
//                                $('#consignmentSbNo').val('0');
                            } else {
                                $('#sbnoTh').hide();
                                $('#exporterTh').hide();
                                for (var i = 0; i <= consignmentOrderNos.length; i++) {
                                    $('#consignmentSbNo' + i).val('0');
                                    $('#sbnoTd' + i).hide();
                                    $('#exporterTd' + i).hide();
                                }
                            }
                        }

                    </script>
                    <%
                                          String branchId1 = "";
                                          if (session.getAttribute("BranchId") != null) {
                                              branchId1 = (String) session.getAttribute("BranchId");
                                          }
                                          if ("3".equals(branchId1)) {
                    %>

                    <td class="<%=classText%>"   > <input type="text" name="hawbNo"  id="hawbNo<%=index%>" class="textbox" /></td>
                    <td class="<%=classText%>"   > <input type="text" name="igmNo"  id="igmNo<%=index%>" class="textbox" /></td>
                    <td class="<%=classText%>"   > <input type="text" name="flightNumber"  id="flightNumber<%=index%>" class="textbox" /></td>
                        <%}else{%>
                    <input type="hidden" name="hawbNo"  id="hawbNo<%=index%>" class="textbox" value="0" maxlength="10"/>
                    <input type="hidden" name="igmNo"  id="igmNo<%=index%>" class="textbox" value="0" maxlength="10"/>
                    <input type="hidden" name="flightNumber"  id="flightNumber<%=index%>" class="textbox" value="0" maxlength="10"/>

                    <%}%>


                    <!--}-->

                    <td class="<%=classText%>"  id="exporterTd<%=index%>" style="display: none" ><c:out value="${tsiViewList.customerName}"/></td>
                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.consigneeName}"/></td>
                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                    <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                    <td class="<%=classText%>"   >
                        <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalPackages}"/>
                            <c:set var="recievedPackages" value="${tsiViewList.totalPackages}"/>
                        </c:if>
                        <c:if test="${tsiViewList.shipmentType == '2' }">
                            <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                            <c:set var="recievedPackages" value="${tsiViewList.totalPackages}"/>
                        </c:if>
                    </td>
                    <td class="<%=classText%>"   >
                        <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalWeight}"/>
                            <c:set var="receivedWeight" value="${tsiViewList.totalWeight}"/>
                        </c:if>
                        <c:if test="${tsiViewList.shipmentType == '2' }">
                            <c:set var="receivedWeight" value="${tsiViewList.totalWeight}"/>
                            <c:out value="${tsiViewList.totalWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                        </c:if>
                    </td>

                    <input type="hidden" name="recievedPackages" style="width: 80px;"  id="recievedPackages<%=index%>" class="textbox" value="<c:out value="${recievedPackages}"/>" maxlength="10"/>
                    <input type="hidden" readonly name="receivedWeight" style="width: 80px;"  id="receivedWeight<%=index%>" class="textbox" value="<c:out value="${receivedWeight}"/>" maxlength="10"/>
                    <td class="<%=classText%>"   >
                        <input type="text" name="totalTsaGoods"  id="totalTsaGoods<%=index%>" class="textbox" maxlength="10" value="" onKeyPress='return onKeyPressBlockCharactersNew(event);'/>
                    </td>
                    <td class="<%=classText%>"   >
                        <input type="hidden" name="customStatus"  id="customStatus<%=index%>" value="<c:out value="${tsiViewList.customStatus}"/>"
                               <c:if test = "${tsiViewList.customStatus == 0}" >
                                   Pending Customs Checking
                               </c:if>
                               <c:if test = "${tsiViewList.customStatus == 1}" >
                                   Went To  Customs Checking
                               </c:if>
                               <c:if test = "${tsiViewList.customStatus == 2}" >
                                   Arrived In Customer Place
                               </c:if>                            
                    </td>

                    <%++index;%>
                    </tr>
                </c:forEach>
                <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
            </table>
            <center>
                <input type="button" class="button" name="saveDetails"  value="Save" onclick="submitPage()"/>
                &nbsp;&nbsp;&nbsp;<input type="button" class="button" name="Close"  id="btn" value="Next" onclick="nextPage(this.value)"/>
            </center>

        </c:if>
    </form>
    <script type="text/javascript">
        function onKeyPressBlockCharactersNew(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z@#%^&!*-+~`_=-}{|'"?><,;:\\/]+$/;
            return !reg.test(keychar);
        }

        function egmIgm() {
            var movement = document.getElementById("shipmentType").value;
            var igm = "IGM No";
            var egm = "EGM No";
            if (movement == 1) {
                document.getElementById("movement1").innerHTML = "IGM No";
            } else {
                document.getElementById("movement1").innerHTML = "EGM No";
            }
        }
        function submitPage() {
            var actionStatus = 0;
            var rowNo = document.getElementById("rowNo").value;
            var fromAddress = document.getElementById("fromAddress").value;
            var toAddress = document.getElementById("toAddress").value;
            var fleetDetails = document.getElementById("fleetDetails").value;

            if (fromAddress == "") {
                alert("Please Enter From Address");
                document.getElementById("fromAddress").focus();
                return;
            } else if (toAddress == "") {
                alert("Please Enter To Address");
                document.getElementById("toAddress").focus();
                return;
            } else if (fleetDetails == "") {
                alert("Please Enter FleetDetails");
                document.getElementById("fleetDetails").focus();
                return;
            }

            for (var i = 1; i < rowNo; i++) {
                if (document.getElementById("consignmentSbNo" + i).value == "") {
                    actionStatus = 1;
                    alert("Please Enter Consignment Note Sb No");
                    document.getElementById("consignmentSbNo" + i).focus();
                    return;
                }
            }
            for (var i = 1; i < rowNo; i++) {
                if (document.getElementById("totalTsaGoods" + i).value == "") {
                    actionStatus = 1;
                    alert("Please Enter totalTsaGoods");
                    document.getElementById("totalTsaGoods" + i).focus();
                    return;
                }
            }
            if (parseInt(actionStatus) == parseInt(0)) {
                document.exportTsa.action = "/throttle/saveExportConsignmentTsa.do";
                document.exportTsa.submit();
            }

        }
        function closeWindow() {
            window.close();
        }
        function nextPage(val) {
            if ("Next" == val) {
                document.getElementById("showRouteCourse").click();
                document.getElementById("btn").value = "Previous";
            } else {
                document.getElementById("address").click();
                document.getElementById("btn").value = "Next";

            }

        }
    </script>
</body>
</html>

