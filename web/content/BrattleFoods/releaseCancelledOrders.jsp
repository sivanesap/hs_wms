<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>

<style>
    .opci{
        background:rgba(101, 237, 221,0.5);
        min-height:100px;
        max-width:180px;
        padding:20px;
        border-radius: 12px;
        font-size: 300%;
        alignment-adjust: 60%;

    }

    /*            table {
                    float:left;
                    background:yellow;
                    margin-left:10px;
                }*/
</style>

<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>


</head>

<script type="text/javascript">
   function searchPage(){
       document.release.action="/throttle/releaseCancelledOrders.do";
       document.release.submit();
       
   }
</script>

<body>
    <%
                String menuPath = "Release Cancelled Orders >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="release" method="post" >
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Release Cancelled Orders" text="Release Cancelled Orders"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operation" text="Operation"/></a></li>
                    <li class=""><spring:message code="hrms.label.Release Cancelled Orders" text="Release Cancelled Orders"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%@include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:100%" >
                        <tr>
                            <td>
                                <table class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead><tr><th colspan="10">Release Cancelled Orders</th></tr></thead>
                                    <tr style="display:none">
                                        <td><font color="red">*</font>Branch Name </td>
                                        <td height="30">
                                            <select name="BranchId" id="BranchId" type="text"  class="form-control" style="width:250px;height:40px" >
                                                <option value="0">--select---</option>  
                                            </select>
                                        </td>
                                        <td>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date </td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  value="<c:out value="${fromDate}"/>"></td>
                                        <td></td>
                                        <td><font color="red">*</font>To Date </td>
                                        <td height="30">
                                            <input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" value="<c:out value="${toDate}"/>">
                                        </td>
                                        <td height="30" align="center">
                                            <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <br>

                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Customer </th>                                    
                                    <th>LoanProposalID</th>
                                    <th>Invoice No</th>
                                    <th>Serial No</th>
                                    <th>Product</th>                                                                                                            
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${getLoanOrderDetails}" var="cnl">
                                   <tr height="30">
                                <td align="left"><%=sno%>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                </td>
                                <td align="left" ><c:out value="${cnl.clientName}"/></td>
                                <td align="left" ><c:out value="${cnl.loanProposalID}"/></td>
                                <td align="left" ><c:out value="${cnl.invoiceCode}"/></td>
                                <td align="left" ><c:out value="${cnl.serialNumber}"/></td>
                                <td align="left" ><c:out value="${cnl.productID}"/></td>
                                <td><a href="releaseCancelledOrders.do?orderId=<c:out value="${cnl.orderId}"/>&param=Release"><span class="label label-Primary">Release</span></a></td>
                                
                                
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
<script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            
                    </form>
                    </body>
                </div>

            </div>
        </div>


        <!--<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>-->
        <%@ include file="../common/NewDesign/settings.jsp" %>