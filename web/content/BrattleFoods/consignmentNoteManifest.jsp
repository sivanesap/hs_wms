<%-- 
    Document   : consignmentNoteManifest
    Created on : Mar 4, 2015, 7:07:25 PM
    Author     : srinientitle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });

    $(document).ready(function() {



                    $(function() {
                    $("#manifestDate").datepicker({
                        changeMonth: true, changeYear: true
                    });
                });
                });


</script>

    <body onload="egmIgm()">
    <style>
        body {
            font:13px verdana;
            font-weight:normal;
        }
    </style>
    <form name="exportTsa"  method="post" >
        <br>
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <table cellpadding="0" cellspacing="0" width="800" border="0px" class="border" align="center">
            <tr>
                <td align="center" style="background-color: silver">TT AVIATION HANDLING SERVICES PVT LTD, CHENNAI - 600 027.<br>TRANSHIPMENT APPLICATION</td>
            </tr>
        </table>
        <br>
        <br>
        <div id="tabs">
            <ul>
                <li><a href="#manifest"><span>Manifest Details</span></a></li>
            </ul>
           <div id="manifest">
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable" >
                    <tr>
                        <td class="contenthead" colspan="4" >Manifest Details</td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Shipment Type</td>
                        <td class="text2">
                            <select class="text" name="shipmentType" id="shipmentType" onchange="egmIgm()">
                                <option value="1">Import</option>
                                <option value="2">Export</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter Operator</td>
                        <td class="text2" ><input type="text" name="operatorName" id="operatorName" maxlength="20" class="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">Enter Flight No</td>
                        <td class="text2" ><input type="text" name="flightNo" id="flightNo" maxlength="20" class="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Manifest Date</td>
                        <td class="text2"><input type="text" name="manifestDate" id="manifestDate" maxlength="20" class="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center"><label id="movement1">&nbsp;</label></td>
                            <td class="text2"><input type="text" name="consignmentNoteEmno" id="consignmentNoteEmno" maxlength="20" class="textbox"/></td>
                    </tr>

                    <tr>
                        <td class="text2" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter ATD  No</td>
                        <td class="text2"><input type="text" name="consignmentNoteAtdNo" id="consignmentNoteAtdNo" maxlength="20" class="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Point Of Loading</td>
                        <td class="text2"><input type="text" name="lodingPoint" id="lodingPoint" maxlength="20" class="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="text2" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Point Of UnLoading</td>
                        <td class="text2"><input type="text" name="unLodingPoint" id="unLodingPoint" maxlength="20" class="textbox"/></td>
                    </tr>
                    
                </table>
            </div>            
        </div>
        <br>
        <br>
        <c:if test="${tsiViewList != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                <thead>
                    <tr>
                        <th align="center"><h3>S.No</h3></th>
                        <th align="center"><h3>AWB No</h3></th>
                        <th align="center"><h3>TotalPcs</h3></th>
                        <th align="center"><h3>Total Grs.Weight</h3></th>
                        <th align="center"><h3>Nature Of Goods</h3></th>                        
                        <th align="center"><h3>Origin</h3></th>
                        <th align="center"><h3>Destination</h3></th>
                        <th align="center"><h3>For Office Use</h3></th>                        
                    </tr>
                </thead>
                <% int index = 1;%>
                <c:forEach items="${tsiViewList}" var="tsiViewList">
                    <tr>
                        <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                        %>
                        <td class="<%=classText%>"  ><%=index%></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.consignmentAwbNo}"/>
                                <input type="hidden" name="consignmentOrderNos"  id="consignmentOrderId<%=index%>" class="textbox"  value="<c:out value="${tsiViewList.consignmentOrderId}"/>"/>                            
                        </td>                    
                        
                        
                       <td class="<%=classText%>"   >
                            <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalPackages}"/>
                            <c:set var="recievedPackages" value="${tsiViewList.totalPackages}"/>
                            </c:if>
                            <c:if test="${tsiViewList.shipmentType == '2' }">
                            <c:out value="${tsiViewList.totalPackages}"/>/<c:out value="${tsiViewList.awbTotalPackages}"/>
                            <c:set var="recievedPackages" value="${tsiViewList.totalPackages}"/>
                            </c:if>
                        </td>
                        <td class="<%=classText%>"   >
                           <c:if test="${tsiViewList.shipmentType == '1' }">
                            <c:out value="${tsiViewList.totalWeight}"/>
                            <c:set var="receivedWeight" value="${tsiViewList.totalWeight}"/>
                            </c:if>
                            <c:if test="${tsiViewList.shipmentType == '2' }">
                                <c:set var="receivedWeight" value="${tsiViewList.totalWeight}"/>
                            <c:out value="${tsiViewList.totalWeight}"/>/<c:out value="${tsiViewList.awbTotalGrossWeight}"/>
                            </c:if>
                        </td>
                        <input type="hidden" name="routeContractId" style="width: 80px;"  id="routeContractId<%=index%>" class="textbox" value="<c:out value="${tsiViewList.routeContractId}"/>" maxlength="10"/>
                        <input type="hidden" name="recievedPackages" style="width: 80px;"  id="recievedPackages<%=index%>" class="textbox" value="<c:out value="${recievedPackages}"/>" maxlength="10"/>
                       <input type="hidden" readonly name="receivedWeight" style="width: 80px;"  id="receivedWeight<%=index%>" class="textbox" value="<c:out value="${receivedWeight}"/>" maxlength="10"/>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.commodity}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbOriginName}"/></td>
                        <td class="<%=classText%>"   ><c:out value="${tsiViewList.awbDestinationName}"/></td>
                        <td class="<%=classText%>"   ><input type="text" name="consignmentOfficeUse" id="consignmentOfficeUse<%=index%>"  class="textbox"/></td>
                        <%++index;%>
                    </tr>
                </c:forEach>
                    <input type="hidden" name="rowNo" id="rowNo" value="<%=index%>"/>
            </table>
            <center>
                <input type="button" class="button" name="saveDetails"  value="saveDetails" onclick="submitPage()"/>&nbsp;&nbsp;&nbsp;<input type="button" class="button" name="Close"  value="Close" onclick="closeWindow()"/>
        </center>

        </c:if>
    </form>
        <script type="text/javascript">
            function egmIgm() {
                    var movement = document.getElementById("shipmentType").value;
                    var igm ="IGM No";
                    var egm ="EGM No";
                    if (movement == 1){
                        document.getElementById("movement1").innerHTML="IGM No";
                    } else {
                        document.getElementById("movement1").innerHTML="EGM No";
                    }
                }
            
            var status=0;
            status='<c:out value="${saveStatus}"/>';            
            if(parseInt(status)==parseInt(1)){
                alert("Manifest Saved SuccessFully");
                closeWindow();
                
            }
            function submitPage(){
                var operatorName=document.getElementById("operatorName").value;
                var flightNo=document.getElementById("flightNo").value;
                var manifestDate=document.getElementById("manifestDate").value;
                var lodingPoint=document.getElementById("lodingPoint").value;
                var unLodingPoint=document.getElementById("unLodingPoint").value;

                if(operatorName==""){
                alert("Please Operator Name");
                document.getElementById("operatorName").focus();
                return;
                }else if(flightNo==""){
                alert("Please Operator Flight No");
                document.getElementById("flightNo").focus();
                return;
                }else if(manifestDate==""){
                alert("Please Operator Manifest Date");
                document.getElementById("manifestDate").focus();
                return;
                }else if(lodingPoint==""){
                    alert("Please Operator LodingPoint ");
                document.getElementById("lodingPoint").focus();
                return;
                }else if(unLodingPoint==""){
                    alert("Please Operator UnLodingPoint ");
                document.getElementById("unLodingPoint").focus();
                return;
                }else{
                        document.exportTsa.action = "/throttle/saveConsignmentNoteManifest.do";
                        document.exportTsa.submit();
                    
                }

                

            }
            function closeWindow(){
                window.close();
            }
        </script>
</body>
</html>

