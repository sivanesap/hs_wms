<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  

<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions1.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>         
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.mrs.business.MrsTO" %>  
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<link href="css/box-style.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.9.1.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script src="/throttle/js/jquery.ui.core.js"></script>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

</head>




<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  Rate Master </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="general.label.Master"  text="Master"/></a></li>
            <li class="active"> Rate Master</li>
        </ol>
    </div>
</div>
        <script>
            function categoryHide(val){
//                alert(val);
//                alert(val2);
                if(val == '0'){
                     $("#serviceName").hide();
                     $("#categoryTemp").hide();
                     $("#productNames").hide();
                     $("#productTemp").hide();
                }else if(val == '1'){
                     $("#serviceName").show();
                     $("#categoryTemp").show();
                     $("#productNames").hide();
                     $("#productTemp").hide();
                 }else{
                     $("#serviceName").hide();
                     $("#categoryTemp").hide();
                     $("#productNames").show();
                     $("#productTemp").show();
                 }
            }
            </script>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onload="categoryHide(<c:out value="${gstType}"/>)">
                <form name="gstRates" method="post" >


                    <%@ include file="/content/common/message.jsp"%>
                    <td colspan="4"  style="background-color:#5BC0DE;">
                        <%--<c:forEach items="${gstRatemaster}" var="list">--%>
                        <%--<c:if test="${gstRatemaster}">--%>
                        <table class="table table-info mb30 table-hover" style="width:100%">
                            <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">GST Rates Master</td></tr>
                            <tr>
                                <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                            </tr>
                            <input type="hidden" name="gstRateDetailId" id="gstRateDetailId" value='<c:out value="${list.gstRateDetailId}"/>'>
                                <input name="gstCategoryCode" type="hidden" class="form-control" value="<c:out value="${list.gstCategoryCode}"/>" >
                                <%--<c:out value="${gstRateDetailId}"/>--%>
                                <%--<c:out value="${getGstCategoryCode}"/>--%>
                                <%--<c:out value="${state}"/>--%>
                                <%--<c:out value="${gstProductCategoryCode}"/>--%>
                                <%--<c:out value="${gstType}"/>--%>
                                <c:out value="${list.gstRateId}"/>
                            <tr>
                                <td>GST Type</td>
                                <td  >
                                    <c:if test="${gstType == '1'}">
                                        Service
                                </c:if>
                                    <c:if test="${gstType == '2'}">
                                        Product
                                </c:if>
                                </td>
                                <td >&nbsp;&nbsp;<font color="red">*</font>States</td>
                                <td >
                                    <!--<input type="text" class="form-control" id="categoryName" readonly name="categoryName" value='<c:out value="${state}"/>'>-->
                                    <select class="form-control" name="state" id="state" readonly disabled required style="width:240px;height:40px">
                                    <c:if test="${getStateList != null}">
                                        <option value="0" >--select--</option>
                                        <c:forEach items="${getStateList}" var="statelist">
                                            <option value="<c:out value="${statelist.stateId}"/>" ><c:out value="${statelist.stateName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                            <c:out value="${state}"/>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td id="serviceName"><font color="red">*</font>Service Name</td>
                                <td  id="categoryTemp">
<!--                                    <input type="text" class="form-control" id="serviceName" readonly name="categoryName" value='<c:out value="${getGstCategoryCode}"/>'>-->
                                <select class="form-control" name="categoryName" readonly id="categoryName" disabled required style="width:240px;height:40px">
                                        <c:if test="${getCategoryName != null}">
                                            <option value="0" >--select--</option>
                                            <c:forEach items="${getCategoryName}" var="catName">
                                                <option value="<c:out value="${catName.gstCategoryId}"/>"><c:out value="${catName.categoryName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                                <c:out value="${getGstCategoryCode}"/>
                                    </select>
                                </td>
                                <td id="productNames"><font color="red">*</font>Product Name</td>
                                <td  id="productTemp">
                                    <!--<input type="text" class="form-control" id="ProductName" readonly name="categoryName" value='<c:out value="${gstProductCategoryCode}"/>'>-->
                                <select class="form-control" name="productName" id="productName" readonly disabled required style="width:240px;height:40px">
                                        <c:if test="${getProductName != null}">
                                            <option value="0" >--select--</option>
                                            <c:forEach items="${getProductName}" var="proName">
                                                <option value="<c:out value="${proName.gstProductId}"/>"><c:out value="${proName.hsnName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>
                            <script>
                                     document.getElementById("productName").value = '<c:out value="${gstProductCategoryCode}"/>';
                                     document.getElementById("state").value = '<c:out value="${state}"/>';
                                     document.getElementById("categoryName").value = '<c:out value="${getGstCategoryCode}"/>';
                                </script>
                        </table>
                        <%--</c:forEach >--%>
                        <%--</c:if>--%>
                            <!--</table>-->
                            <br>    
                            <table class="table table-info mb30 table-hover" id="items" >
                                <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th><h3>S.No</h3></th>
                        <th><h3>GST Code</h3></th>
                        <th><h3>GST Percentage </h3></th>
                        <th><h3>Valid From </h3></th>
                        <th><h3>Valid To</h3></th>
                                <input type="hidden" name="selectedIndex" value="">
                        </tr>
                        </thead>
                                <%int rIndex = 0;
                                int index = 1;
                                %>
                                <c:forEach items="${getRateMasterDetails}" var="list">
                                    <%
            

                        String classText = "";

                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                                    %>
                                    <tr >
                                        <td class="<%=classText %>"  height="30"><%=index %></td>
                                        <td class="<%=classText %>"  height="30">
                                            <!--<input type='text' class="form-control" readonly name='gstCode' value='' >-->
                                            <c:if test="${list.gstCode == '1'}">
                                    CGST
                                </c:if>
                                <c:if test="${list.gstCode == '2'}">
                                    SGST
                                </c:if>
                                    <c:if test="${list.gstCode == '3'}">
                                    IGST
                                </c:if>
                                    <c:if test="${list.gstCode == '4'}">
                                    UTGST
                                </c:if>
                                        </td>
                                        <td class="<%=classText %>"  height="30"><input type='text' class="form-control" readonly name='gstPercentage' value='<c:out value="${list.gstPercentage}"/>' >
                                        </td>
                                        <td class="<%=classText %>"  height="30"><input type='text' class="form-control" readonly name='validFrom' value='<c:out value="${list.validFrom}"/>' >
                                        </td>
                                        <td class="<%=classText %>"  height="30"><input type='text' class="form-control" readonly  name='validTo' value='<c:out value="${list.validTo}"/>' >
                                        </td>
                                    <%
                        index++;
                        rIndex++;
                                    %>
                                </c:forEach >
                                <%
                                System.out.println("rIndex:"+rIndex);
                                rIndex--;
                                %>
                                <input type="hidden" name="rIndex" value=<%=rIndex%> />
                            </table>
                            <br>
<!--                            <div id="buttonStyle" style="visibility:visible;" align="center" >
                                <input value="<spring:message code="vendors.label.AddRow"  text="default text"/>" class="btn btn-info" type="button" onClick="addRow();" >
                                <input type="button" class="btn btn-info" value="<spring:message code="vendors.label.SAVE"  text="default text"/>" name="Save"   onClick="Submit()"/>
                            </div>-->
                            <br>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
