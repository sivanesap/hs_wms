<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>

<style>
    .opci{
        background:rgba(101, 237, 221,0.5);
        min-height:100px;
        max-width:180px;
        padding:20px;
        border-radius: 12px;
        font-size: 300%;
        alignment-adjust: 60%;

    }

    /*            table {
                    float:left;
                    background:yellow;
                    margin-left:10px;
                }*/
</style>

<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>

<script>
    function initMap() {
        var service = new google.maps.DirectionsService;
        var start = new google.maps.LatLng("22.81434", "86.18916");
        var mapOptions = {
            zoom: 7

        };
        var map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
        var routePoint = document.getElementsByName("locId");
        var latitude = document.getElementsByName("latitude");
        var longitude = document.getElementsByName("longitude");
        var w = routePoint.length;

        // list of points
        var stations = [];
        var bounds = [];

        bounds.push({
            lat: "22.81434", lng: "86.18916"
        });

        for (var j = 0; j < w; j++) {

            stations.push({
                lat: latitude[j].value, lng: longitude[j].value, name: routePoint[j].value
            });
            bounds.push({
                lat: latitude[j].value, lng: longitude[j].value
            });
        }

        // Zoom and center map automatically by stations (each station will be in visible map area)
        var lngs = stations.map(function(station) {
            return station.lng;
        });
        var lats = stations.map(function(station) {
            return station.lat;
        });
        var lngss = bounds.map(function(station) {
            return station.lng;
        });
        var latss = bounds.map(function(station) {
            return station.lat;
        });

        map.fitBounds({
            west: Math.min.apply(null, lngss),
            east: Math.max.apply(null, lngss),
            north: Math.min.apply(null, latss),
            south: Math.max.apply(null, latss),
        });


        var marker = start;




        var image = '/throttle/images/warehouse.png';
        var beachMarker = new google.maps.Marker({
            map: map,
            position: marker,
            animation: google.maps.Animation.DROP,
            title: "OWM ",
            icon: image

        });

        // Show stations on the map as markers
        for (var i = 0; i < stations.length; i++) {

            var marker = new google.maps.LatLng(stations[i].lat, stations[i].lng);

            //            image = '/throttle/images/wowPIC.jpg';

            var image = '/throttle/images/truck.png';
            var beachMarker = new google.maps.Marker({
                map: map,
                position: marker,
                animation: google.maps.Animation.DROP,
                title: stations[i].name,
                icon: image
            });
        }

        // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
        for (var i = 0, parts = [], max = 25 - 1; i < stations.length; i = i + max)
            parts.push(stations.slice(i, i + max + 1));
        // Service callback to process service results
        var service_callback = function(response, status) {
            if (status != 'OK') {
                console.log('Directions request failed due to  ' + status);
                return;
            }
            var renderer = new google.maps.DirectionsRenderer;
            renderer.setMap(map);
            renderer.setOptions({suppressMarkers: true, preserveViewport: true, polylineOptions: {
                    strokeColor: 'black', strokeWeight: 3, strokeOpacity: 1.0
                }});
            renderer.setDirections(response);
            //            computeTotalDistance(response);

        };
        //        alert("Parts===" + parts.length);
        // Send requests to service to get route (for stations count <= 25 only one request will be sent)
        for (var i = 0; i < parts.length; i++) {
            // Waypoints does not include first station (origin) and last station (destination)
            var waypoints = [];
            for (var j = 0; j < parts[i].length; j++) {
                //                waypoints.push({location: parts[i][j], stopover: false});
                waypoints.push({location: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng), stopover: false});
                // Service options
                var service_options = {
                    origin: start,
                    destination: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng),
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING
                };

                // Send request
                service.route(service_options, service_callback);
            }
        }

    }
</script>

</head>

<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>
    function scheduleDelivery(orderId) {
        var param = "0";
        document.schedule.action = "/throttle/updateDeliverySchedule.do?orderId=" + orderId + "&param=" + param;
        document.schedule.submit();
    }

    function searchPage(val) {
        document.schedule.action = "/throttle/viewPlanningListMH.do?param=" + val;
        document.schedule.submit();
    }

    function viewTripPlanning() {
//         alert("sssss")
                            var inputs = document.getElementsByName("selectedIndex");
                            var m=0;
                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        m++;
                                    }
                                }
                            }
       if(m==0){
           alert("Select atleast any one of the order");
       }else{
        document.schedule.action = "/throttle/createTripSheetMH.do";
        document.schedule.submit();
       }
        }
    

</script>

<body>
    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="schedule" method="post" >
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.MicroHub RunSheet Planning" text="MicroHub RunSheet Planning"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operation" text="Operation"/></a></li>
                    <li class=""><spring:message code="hrms.label.MicroHub RunSheet Planning" text="MicroHub RunSheet Planning"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%@include file="/content/common/message.jsp" %>
<div class="contentpanel">
    <div class="row" align="center"style="overflow:auto">
        
        <div class="col-sm-1 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">

                                <h4> <spring:message code="dashboard.label.Trips"  text="Trips"/></h4>

                                <h1><span id="totalVendors"><c:out value="${runsheetSize}"/></span></h1>
                            </div>
                        </div>
                    </div>

                </div
            </div>
        </div>

       </div>
     </div>
                            </div>
<!--                            <table class="table table-info mb30 table-hover" id="table2" >
                            <thead><tr><th colspan="10">DayWise order Count</th></tr></thead>
                           
                            <tbody>
                                <tr height="40">
                                    <th colspan="1">Invoice Date</th>
                                    <c:forEach items="${getTripPlanningDayCount}" var="cd">  <th><c:out value="${cd.invoiceDate}"/></th></c:forEach>
                                </tr>
                                   <tr height="30">
                                <td colspan="1">Orders</td>
                                <c:forEach items="${getTripPlanningDayCount}" var="cd">  <td><c:out value="${cd.invoiceAge}"/></td></c:forEach>
                                
                                </tr>
                            </tbody>
                        </table>-->
                    <table class="table table-info mb30 table-hover" style="width:100%" >
                        <tr>
                            <td>
                                <table class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead><tr><th colspan="10">MicroHub RunSheet Planning</th></tr></thead>
                                    <tr style="display:none">
                                        <td><font color="red">*</font>Branch Name </td>
                                        <td height="30">
                                            <select name="BranchId" id="BranchId" type="text"  class="form-control" style="width:250px;height:40px" >
                                                <option value="0">--select---</option>  
                                            </select>
                                        </td>
                                        <td>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date </td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  value="<c:out value="${fromDate}"/>"></td>
                                        <td></td>
                                        <td><font color="red">*</font>To Date </td>
                                        <td height="30">
                                            <input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" value="<c:out value="${toDate}"/>">
                                        </td>
                                        <td height="30" align="center">
                                            <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <br>
                    <c:if test = "${getTripPlanningList != null}" >

                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Customer </th>                                    
                                    <th>LoanProposalID</th>
                                    <th>Invoice No</th>
                                    <th>Serial No</th>
                                    <th>Batch No</th>
                                    <th>Batch Date</th>
                                    <th>Branch</th>
                                    <th>Ageing</th>
                                    <th>Product</th>                                                                                                            
                                    <th>Select 
                                        <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/>
                                    </th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${getTripPlanningList}" var="cnl">
                                    <c:if test="${cnl.invoiceAge<=2}">
                                   <tr height="30">
                                <td align="left"><%=sno%>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                </td>
                                <td align="left" ><c:out value="${cnl.clientName}"/></td>
                                <td align="left" ><c:out value="${cnl.loanProposalID}"/></td>
                                <td align="left" ><c:out value="${cnl.invoiceCode}"/></td>
                                <td align="left" ><c:out value="${cnl.serialNumber}"/></td>
                                <td align="left" ><c:out value="${cnl.batchNumber}"/></td>                                        
                                <td align="left" ><c:out value="${cnl.batchDate}"/></td>
                                <td align="left" ><c:out value="${cnl.branchName}"/></td>
                                <td align="left" ><c:out value="${cnl.ageing}"/></td>
                                <td align="left" ><c:out value="${cnl.productID}"/></td>                                
                                <td align="center" >
                                    <c:if test="${cnl.serialNumber=='' || cnl.serialNumber=='0'}">
                                        <font color="red"><b>InValid SerialNo</b></font>
                                    </c:if>
                                    <c:if test="${cnl.serialNumber!='' && cnl.serialNumber!='0'}">
                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="checkSelectStatus(<%=sno%>, this);" />
                                    </c:if>
                                    <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />                                    
                                </td>
                                    </c:if>   
                                <c:if test="${cnl.invoiceAge>2}"> 
                                <tr height="30">
                                <td align="left" style="color:red"><%=sno%>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                </td>
                                <td align="left" style="color:red" ><c:out value="${cnl.clientName}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.loanProposalID}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.invoiceCode}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.serialNumber}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.batchNumber}"/></td>                                        
                                <td align="left" style="color:red"><c:out value="${cnl.batchDate}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.branchName}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.ageing}"/></td>
                                <td align="left" style="color:red"><c:out value="${cnl.productID}"/></td>                                
                                <td align="center" style="color:red">
                                    <c:if test="${cnl.serialNumber=='' || cnl.serialNumber=='0'}">
                                        <font color="red"><b>InValid SerialNo</b></font>
                                    </c:if>
                                    <c:if test="${cnl.serialNumber!='' && cnl.serialNumber!='0'}">
                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="checkSelectStatus(<%=sno%>, this);" />
                                    </c:if>
                                    <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />                                    
                                </td>
                                </c:if>
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                                
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
<script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            
                    <script>
                        function checkAll() {
                            var inputs = document.getElementsByName("selectedIndex");
                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        inputs[i].checked = false;
                                    } else if (inputs[i].checked == false) {
                                        inputs[i].checked = true;
                                    }
                                }
                            }
                        }
                    </script>


                    <br>
                    <center>
                        <input type="button" class="btn btn-success" name="tripPlanning" id="tripPlanning" value="RunSheet Plan" onclick="viewTripPlanning()" />
                    </center>
                    </form>
                    </body>
                </div>
                <script>

                    function checkSelectStatus(sno, obj) {
//                            alert(sno)
                        var val = document.getElementsByName("selectedStatus");
                        if (obj.checked == true) {
                            document.getElementById("selectedStatus" + sno).value = 1;
                        } else if (obj.checked == false) {
                            document.getElementById("selectedStatus" + sno).value = 0;
                        }
                        var vals = 0;
                        for (var i = 0; i <= val.length; i++) {
                        }
                    }
                </script>          

            </div>
        </div>


        <!--<script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>-->
        <%@ include file="../common/NewDesign/settings.jsp" %>