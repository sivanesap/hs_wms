<%-- 
    Document   : viewMhTripSheet
    Created on : 12 Mar, 2021, 11:44:36 AM
    Author     : entitle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
   
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.WarehouseWise Order report"  text="WarehouseWise Order report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.WarehouseWise Order report"  text="Report"/></a></li>
            <li class="active">WarehouseWise Order report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setDate();">
                <form name="mhreport" method="post">
                   
               <table class="table table-info mb30 table-hover" id="pincode">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">Search MicroHub Orders</th>
                          </tr></thead>  
                        <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                MicroHub Executive
                            </td>
                            <td><br><select id="empId" name="empId" style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    <c:forEach items="${mhExecutiveList}" var="st">
                                      <option value="<c:out value="${st.empId}"/>"><c:out value="${st.empName}"/></option>
                            </c:forEach>
                              </select></td>
                           
                            <td> <input type="button" class="btn btn-success" value="Search" onclick="searchPage()"></td>
                           
                            </tr>
                           
                           
                       
                </table>  
                         
                 
                       
                   <c:if test="${viewMhTripSheet!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr>
                    <th class="contenthead" colspan="8">View MicroHub Trip Sheet</th>
                          </tr>
                            <tr >
                                <th>S.No</th>
                                <th>Loan Proposal Id</th>
                                <th>Product Name</th>
                                <th>Serial No</th>
                                <th>MicroHub Incharge</th>
                                <th>Planned Date</th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${viewMhTripSheet}" var="mh">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td ><%=sno%></td>
                                        <td><c:out value="${mh.loanProposalId}"/></td>
                                        <td><c:out value="${mh.itemName}"/></td>
                                        <td><c:out value="${mh.serialNo}"/></td>
                                        <td><c:out value="${mh.empName}"/></td>
                                        <td><c:out value="${mh.scheduleDate}"/></td>
                                        
                                        </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                                    </tr>
                            </tbody>
                            
                    </table> 
                            </c:if>  
 <script>
   function setDate(){
       var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = dd+'-'+mm+'-'+yyyy;
    document.getElementById("fromDate").value=today;
    document.getElementById("toDate").value=today;
   }
     function searchPage() {
            var empId = document.getElementById("empId").value;
            document.mhreport.action = '/throttle/viewMhTripSheet.do?&param=search&empId='+empId;
            document.mhreport.submit();
    }
   
   
    
    
                </script>
                            
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

