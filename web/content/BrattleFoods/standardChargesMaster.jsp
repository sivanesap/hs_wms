<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function submitPage()
    {var errStr = "";
        var nameCheckStatus = $("#StandardChargeNameStatus").text();
        if(document.getElementById("stChargeName").value == "") {
            errStr = "Please enter standard Charge Name.\n";
            alert(errStr);
            document.getElementById("stChargeName").focus();
        }
        else if(nameCheckStatus != "") {
            errStr ="StandardCharge Name Already Exists.\n";
            alert(errStr);
            document.getElementById("stChargeName").focus();
        }
        else if(document.getElementById("stChargeDesc").value == "") {
            errStr ="Please enter standard  Description.\n";
            alert(errStr);
            document.getElementById("stChargeDesc").focus();
        }
        else if(document.getElementById("stChargeUnit").value == "") {
            errStr ="Please enter standard Charge Unit.\n";
            alert(errStr);
            document.getElementById("stChargeUnit").focus();
        }
        if(errStr == "") {
            document.standardCharges.action=" /throttle/saveStandardCharge.do";
            document.standardCharges.method="post";
            document.standardCharges.submit();
        }



    }
    function setValues(sno,stChargeName,stChargeDesc,stChargeUnit,stChargeId,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("stChargeId").value = stChargeId;
        document.getElementById("stChargeName").value = stChargeName;
        document.getElementById("stChargeDesc").value = stChargeDesc;
        document.getElementById("stChargeUnit").value = stChargeUnit;
        document.getElementById("status").value = status;
    }






    function onKeyPressBlockNumbers(e)
	{

		var key = window.event ? e.keyCode : e.which;
		var keychar = String.fromCharCode(key);
		reg = /\d/;
		return !reg.test(keychar);

	}




var httpRequest;
    function checkstChargeName() {

        var stChargeName = document.getElementById('stChargeName').value;
        var nameCheckStatus = $("#productCategoryNameStatus").text();
            var url = '/throttle/checkStandardCharge.do?stChargeName=' + stChargeName ;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
       
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                     $("#nameStatus").show();
                    $("#StandardChargeNameStatus").text('Please Check Standard Charge Name  :' + val+' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#StandardChargeNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>-->
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.StandardChargeMster" text="StandardChargeMster"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.StandardChargeMster" text="StandardChargeMster"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    
    <body onload="document.standardCharges.stChargeName.focus();">


        <form name="standardCharges"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
               <table class="table table-info mb30 table-hover" id="bg" >	
			<thead>

                <tr>
                    <th  colspan="4" >Standard Charges Master</th>
                </tr>
                        </thead>
                <tr>
                    <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="StandardChargeNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Name</td>
                    <td ><input type="text" name="stChargeName" id="stChargeName" class="form-control" style="width:250px;height:40px"  maxlength="15" onkeypress="return onKeyPressBlockNumbers(event);"  onchange="checkstChargeName();" autocomplete="off"/></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Description</td>
                    <td ><input type="text" name="stChargeDesc" id="stChargeDesc" class="form-control" style="width:250px;height:40px" 
onkeyup="maxlength(this.form.stChargeDesc,75)"></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Unites</td>
                    <td >
                        <select  align="center" class="form-control" style="width:250px;height:40px"  name="stChargeUnit" id="stChargeUnit" >
                            <option value='0'>--select--</option>
                            <c:if test="${unitList != null}">
                                <c:forEach items="${unitList}" var="ul">
                                    <option value='<c:out value="${ul.stChargeUnit}"/>'><c:out value="${ul.unitName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td >
                        <select style="width:250px;height:40px"    name="status"  >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        
                        <center>
                            <input type="button" class="btn btn-success" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
          </table>
            


            <h2 align="center">List The standard Charge's</h2>


           <table class="table table-info mb30 table-hover" id="table" >	
			<thead>

                    <tr height="30">
                        <th>S.No</th>
                        <th>Name </th>
                        <th>Description</th>
                        <th>Unites</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${standardCharge != null}">
                        <c:forEach items="${standardCharge}" var="stCharge">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.stChargeName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.stChargeDesc}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${stCharge.unitName}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${stCharge.stChargeName}" />','<c:out value="${stCharge.stChargeDesc}" />','<c:out value="${stCharge.stChargeUnit}" />','<c:out value="${stCharge.stChargeId}" />','<c:out value="${stCharge.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>