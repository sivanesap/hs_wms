<%--
    Document   : vehicleUtilizationReport
    Created on : Mar 30, 2015, 1:45:18 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.customer.business.CustomerTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
    </head>
    <body>
        <form name="vehicleUtilization" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Truck Master</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td height="30"><font color="red">*</font>Select Truck</td>
                                        <td>
                                            <select name="truckCodeId" id="truckCodeId">
                                                   <option value="">---Select One ---</option>
                                                
                                                <c:forEach items="${truckNameList}" var="truckNameList">
                                                    <option value="<c:out value="${truckNameList.truckId}"/>"><c:out value="${truckNameList.truckName}"/></option>
                                                </c:forEach>
                                                
                                            </select>
                                        </td>
                                        <td colspan="2"><input type="button"   value="Search" class="button" name="search" onClick="submitPage('search')">&nbsp;&nbsp;</td>

                                    </tr>


                                </table>
                                <script>
                                    var truckCodeId='<c:out value="${truckCodeId}"/>';
                                    if(truckCodeId != null){
                                    document.getElementById("truckCodeId").value=truckCodeId;
                                    }
                                </script>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <c:if test = "${truckCapacityList != null}" >
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>
                            <th><h3>Truck Code</h3></th>
                            <th><h3>Total Capacity</h3></th>
                            <th><h3>Total Volume </h3></th>
                            <th><h3>Available Capacity</h3></th>
                            <th><h3>Available Volume</h3></th>
                            <th><h3>Used Capacity</h3></th>
                            <th><h3>Used Volume</h3></th>
                            <th><h3>Truck Assigned Date</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${truckCapacityList}" var="truckCapacityList">
                            <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                            classText = "text2";
                            } else {
                            classText = "text1";
                            }
                            %>
                            <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno%></td>
                                <td align="left" class="<%=classText%>" ><c:out value='${truckCapacityList.fleetCode}'/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.fleetCapacity}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.fleetVolume}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.truckCurrentCap}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.truckCurrentVol}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.usedTruckCapcity}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.usedTruckVol}"/></td>
                                <td align="left" class="<%=classText%>"><c:out value="${truckCapacityList.truckDepDate}"/></td>
                            </tr>
                            <%++sno;%>
                        </c:forEach>
                    </tbody>
                </table>
                        <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </c:if>
        </form>
            <script type="text/javascript">
                function submitPage(){                
                document.vehicleUtilization.action = '/throttle/getTruckUsedDetails.do';
                document.vehicleUtilization.submit();

                }
            </script>
    </body>
</html>
