<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
  
    //Update Vehicle Planning
    function setValues(zoneId,zoneName,sno,activeInd) {
        var count = parseInt(document.getElementById("count").value);
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("zoneId").value = zoneId;
        document.getElementById("zoneName").value = zoneName;
        document.getElementById("activeInd").value = activeInd;
    }

    function submitPage() {

            document.zone.action = '/throttle/saveZoneMaster.do';
            document.zone.submit();
        }
     function importExcel() {
            document.cityMaster.action = '/throttle/content/BrattleFoods/zoneImport.jsp';
            document.cityMaster.submit();
    }
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="zone"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>

            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <tr>
                    <td class="contenthead" colspan="4" >Zone Master</td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Zone Name</td>
                    <td class="text1"><input type="hidden" name="zoneId" id="zoneId" class="textbox" value=""  >
                     <input type="text" name="zoneName" id="zoneName" class="textbox" value=""  >
                    </td>
                    <td class="text">&nbsp;&nbsp;Status</td>
                    <td class="text2" colspan="3" align="left" height="25" >
                         <select  align="center" class="textbox" name="activeInd" id="activeInd" >
                            <option value='Y'>Active</option>
                            <option value='N'>In Active</option>
                        </select>
                    </td>
                </tr>
     
                <tr>
                    <td class="text1" colspan="4" align="center"><input type="button" class="button" value="Save" id="save" onClick="submitPage();"   />
                     <input type="button"   value="Upload Zone" class="button" name="search" onClick="importExcel()" style="width:200px"></td>
                </tr>
                
               
            </table>
            <br>
            <br/>
            <table width="815" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Zone Name</h3></th>
                        <th><h3>Status</h3></th>
                       <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${zoneList != null}">
                        <c:forEach items="${zoneList}" var="zone">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno %> </td>
                                <td class="<%=className%>"  align="left"><c:out value="${zone.zoneName}"/></td>
                                <td class="<%=className%>"  align="left">
                                    <c:if test = "${zone.activeInd == 'Y'}">
                                       Active
                                    </c:if>
                                    <c:if test = "${zone.activeInd == 'N'}">
                                      In Active
                                    </c:if>
                                    </td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${zone.zoneId}" />', '<c:out value="${zone.zoneName}" />','<%=sno%>','<c:out value="${zone.activeInd}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="text" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</html>