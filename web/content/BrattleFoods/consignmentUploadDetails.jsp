<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="OrderUploadDetails"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="OrderUploadDetails"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="consignment"  method="POST">
                    <%@ include file="/content/common/message.jsp" %>

                    <c:if test = "${consignExcelcorect >0 }">
                        <table align="center">
                            <tr align="center">
                                <td align="center">
                                    <center> <input type="button" class="btn btn-success" name="Save" value="Create Order" id="createOrder" onclick="submitPage(this.value);" ></center>
                                </td>
                            </tr>
                        </table>
                    </c:if>
                    <br>
                    <% int sno = 0;%>
                    <c:if test = "${consignExcelcorect>0}">
                        <table class="table table-info mb30 table-hover" id="bg" >	
                            <thead>
                                <tr height="30">
                                    <th> S.No </th>
                                    <th> Vendor Code </th>
                                    <th> Vendor Name </th>
                                    <th> Order No</th>
                                    <th> UMC No </th>
                                    <th> ECCI NO </th>
                                    <th> ECCI Date </th>
                                    <th> Qty </th>
                                    
                                    <th> UOM </th>
                                    <th> Type</th>
                                    <th> Weight</th>
                                    <th> Length</th>
                                    <th> Breadth</th>
                                    <th> Height</th>                                    
                                </tr>
                            </thead>
                            
                            <c:set var="totalCNFQty" value="${0}" />

                            <tbody>
                                <c:if test = "${consignmentExcelUpload!=null}">
                                    <c:forEach items="${consignmentExcelUpload}" var="cng">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>
                                        <tr>
                                            <td align="left"> <%= sno + 0%> <input type="hidden" name="uploadId" id="uploadId" value="<c:out value="${cng.id}" />" class="form-control" style="width:250px;height:40px"  /></td>
                                            <td align="left"> <c:out value="${cng.consigneeCode}" /></td>
                                            <td align="left"> <c:out value="${cng.customerName}" /></td>
                                            <td align="left"> <c:out value="${cng.orderNo}" /></td>
                                            <td align="left"> <c:out value="${cng.umcno}" /></td>
                                            <td align="left"> <c:out value="${cng.documentNo}" /></td>
                                            <td align="left"> <c:out value="${cng.documentDate}" /></td>
                                            <td align="left"> <c:out value="${cng.confirmQty}" /></td>
                                            
                                            <td align="left"> <c:out value="${cng.orderUom}" /></td>
                                            <td align="left"> <c:out value="${cng.orderType}" /></td>
                                            <td align="left"> <c:out value="${cng.packageWeight}" /></td>
                                            <td align="left"> <c:out value="${cng.length}" /></td>
                                            <td align="left"> <c:out value="${cng.breadth}" /></td>
                                            <td align="left"> <c:out value="${cng.height}" /></td>
                                            
                                            <c:set var="totalCNFQty" value="${totalCNFQty+cng.confirmQty}" />
                                        </tr>
                                    </c:forEach>
                                    <tr>
                                        <td colspan="7"  align="right"><b>Total</b></td>                                
                                        
                                        <td><b><c:out value="${totalCNFQty}"/></b></td>                                
                                    </tr>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%=sno%>" />
                            </c:if>
                        </c:if>
                    </table>



                    <c:if test = "${consignExcelIncorect >0}">
                        <table class="table table-info mb30 table-hover" id="bg" >	
                            <thead>
                                <tr height="30" align="center">
                                    <th colspan="11" align="center"> <center><font>InValid Records</font></center></th>
                                </tr>

                                <tr height="30">
                                    <th> S.No </th>
                                    <th> Customer Name  </th>
                                    <th> Origin </th>
                                    <th> Touch Point </th>
                                    <th> Destination </th>
                                    <th> Pallets </th>
                                    <th> Vehicle Type </th>
                                    <th> Consignor </th>
                                    <th> Consignee </th>
                                    <th> Vehicle Req.Date </th>
                                    <th> First Docket </th>
                                    <th> Second Docket </th>
                                    <th>Issue</th>
                                </tr>
                            </thead>
                          
                            <tbody>

                                <c:set var="totalQtyIn" value="${0}" />
                                <c:set var="totalCNFQtyIn" value="${0}" />
                                <% int sno1 = 0;%>
                                <c:if test = "${consignmentExcelUploadIncorect!=null}">
                                    <c:forEach items="${consignmentExcelUploadIncorect}" var="cngInc">
                                        <%
                                                    sno1++;
                                                    String className = "text1";
                                                    if ((sno1 % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>
                                        <tr>
                                            <td align="left"> <%= sno1 + 0%> <input type="hidden" name="uploadId" id="uploadId" value="<c:out value="${cng.id}" />" class="form-control" style="width:250px;height:40px"  /></td>
                                            <td align="left"> <c:out value="${cngInc.customerName}" /></td>
                                            <td align="left"> <c:out value="${cngInc.originName}" /></td>
                                            <td align="left"> <c:out value="${cngInc.touchPoint1}" /></td>
                                            <td align="left"> <c:out value="${cngInc.destinationName}" /></td>
                                            <td align="left"> <c:out value="${cngInc.pallets}" /></td>
                                            <td align="left"> <c:out value="${cngInc.vehicleType}" /></td>
                                            <td align="left"> <c:out value="${cngInc.consignor}" /></td>
                                            <td align="left"> <c:out value="${cngInc.consignee}" /></td>
                                            <td align="left"> <c:out value="${cng.vehicleRequiredDate}" /></td>
                                            <td align="left"> <c:out value="${cng.orderQty}" /></td>
                                            <td align="left"> <c:out value="${cng.confirmQty}" /></td>
                                            <c:if test = "${cngInc.flage == 0}">
                                                <td align="left">Location Not Valid</td>
                                            </c:if>
                                            <c:if test = "${cngInc.flage == 1}">
                                                <td align="left">Consignor / Consignee Not Valid</td>
                                            </c:if>
                                        </tr>
                                        <c:set var="totalQtyIn" value="${totalQtyIn+cng.orderQty}" />
                                        <c:set var="totalCNFQtyIn" value="${totalCNFQtyIn+cng.confirmQty}" />
                                    </c:forEach>

                                    <tr>
                                        <td colspan="7"  align="right"><b>Total</b></td>                                
                                        <td align="center"><b><c:out value="${totalQtyIn}"/></b></td>                                
                                        <td align="center"><b><c:out value="${totalCNFQtyIn}"/></b></td>                                
                                    </tr>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%=sno1%>" />
                            </c:if>
                        </c:if>
                    </table>
                    <script>
                        function submitPage() {
                            if (confirm("Are you Sure Want to Upload")) {
                                $("#createOrder").hide();
                                document.consignment.action = "/throttle/InsertConsignmentExcelUpload.do";
                                document.consignment.submit();
                            }
                        }
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>