<%-- 
    Document   : paymentType
    Created on : Jan 28, 2014, 12:22:43 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });


        function onKeyPressBlockNumbers(e)
        {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /\d/;
            return !reg.test(keychar);
        }


        function extractNumber(obj, decimalPlaces, allowNegative)
        {
            var temp = obj.value;

            // avoid changing things if already formatted correctly
            var reg0Str = '[0-9]*';
            if (decimalPlaces > 0) {
                reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
            } else if (decimalPlaces < 0) {
                reg0Str += '\\.?[0-9]*';
            }
            reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
            reg0Str = reg0Str + '$';
            var reg0 = new RegExp(reg0Str);
            if (reg0.test(temp)) return true;

            // first replace all non numbers
            var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
            var reg1 = new RegExp(reg1Str, 'g');
            temp = temp.replace(reg1, '');

            if (allowNegative) {
                // replace extra negative
                var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
                var reg2 = /-/g;
                temp = temp.replace(reg2, '');
                if (hasNegative) temp = '-' + temp;
            }

            if (decimalPlaces != 0) {
                var reg3 = /\./g;
                var reg3Array = reg3.exec(temp);
                if (reg3Array != null) {
                    // keep only first occurrence of .
                    //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                    var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                    reg3Right = reg3Right.replace(reg3, '');
                    reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                    temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
                }
            }

            obj.value = temp;
        }
        function blockNonNumbers(obj, e, allowDecimal, allowNegative)
        {
            var key;
            var isCtrl = false;
            var keychar;
            var reg;

            if(window.event) {
                key = e.keyCode;
                isCtrl = window.event.ctrlKey
            }
            else if(e.which) {
                key = e.which;
                isCtrl = e.ctrlKey;
            }

            if (isNaN(key)) return true;

            keychar = String.fromCharCode(key);

            // check for backspace or delete, or if Ctrl was pressed
            if (key == 8 || isCtrl)
            {
                return true;
            }

            reg = /\d/;
            var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
            var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

            return isFirstN || isFirstD || reg.test(keychar);
        }
    </script>
    <script type="text/javascript">

        function submitPage() {
            if(document.getElementById('paymentModeId').value == 0){
                alert("Please select payment mode");
                document.getElementById('paymentModeId').focus();
            }else if(document.getElementById('paymentModeId').value == 2 && document.getElementById('chequeNo').value=="" ){
                alert("Please Enter Cheque No");
                document.getElementById('chequeNo').focus();
            }else if(document.getElementById('paymentModeId').value == 2 && document.getElementById('chequeRemarks').value== ""){
                alert("Please Enter Cheque Remarks");
                document.getElementById('chequeRemarks').focus();
            }else if(document.getElementById('paymentModeId').value == 6 && document.getElementById('rtgsNo').value==""){
                alert("Please enter rtgs no");
                document.getElementById('rtgsNo').focus();
            }else if(document.getElementById('paymentModeId').value == 6 && document.getElementById('rtgsRemarks').value==""){
                alert("Please enter rtgs remarks");
                document.getElementById('rtgsRemarks').focus();
            }else if(document.getElementById('paymentModeId').value == 6 && document.getElementById('draftNumber').value==""){
                alert("Please enter draft no");
                document.getElementById('draftNumber').focus();
            }else if(document.getElementById('paymentModeId').value == 6 && document.getElementById('draftRemarks').value==""){
                alert("Please enter draft remarks");
                document.getElementById('draftRemarks').focus();
            }else if(document.getElementById('paidAmount').value == ""){
                alert("Please enter amount to be pay");
                document.getElementById('paidAmount').focus();
            }else if(document.getElementById('paidDate').value == ""){
                alert("Please select pay date");
                document.getElementById('paidDate').focus();
            }else{
            document.paymentType.action = '/throttle/savePaymentDetails.do';
            document.paymentType.submit();
            }
        }
    </script>


</head>
<body >
    <form name="paymentType" method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@include file="/content/common/message.jsp" %>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bg" class="border">
            <tr align="center">
                <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Payment Details</div></td>
            </tr>
            <tr>
                <td class="text1" height="30"><font color="red">*</font>Payment Type</td>
                <td class="text1" width="80"> <input type="hidden" name="paymentType" id="paymentType" value="<c:out value="${paymentType}"/>" class="textbox" style="width:125px;" >
                    <c:if test="${paymentType == 1}">
                        Credit
                    </c:if>
                    <c:if test="${paymentType == 2}">
                        Advance
                    </c:if>
                    <c:if test="${paymentType == 3}">
                        To Pay and Advance
                    </c:if>
                    <c:if test="${paymentType == 4}">
                        To Pay
                    </c:if>
                </td>
                <td class="text1" height="30"><font color="red">*</font>Payment Mode</td>
                <td class="text1" height="30">
                    <select class="textbox" name="paymentModeId" id="paymentModeId" style="width:125px;" onchange="showPayment();">
                        <c:if test="${paymentMode != null}">
                            <option value="0">--select--</option>
                            <c:forEach items="${paymentMode}" var="pay">
                                <option value="<c:out value="${pay.paymentModeId}"/>" ><c:out value="${pay.paymentModeName}"/></option>
                            </c:forEach>
                        </c:if>
                    </select>
                </td>
            </tr>
            <script>
                function showPayment(){
                    var mode = document.getElementById("paymentModeId").value;
                    if(mode == "6"){
                        $("#showRTGSDetails").show();
                        $("#showDraftDetails").hide();
                        $("#showChequeDetails").hide();
                    }else if(mode == "2"){
                        $("#showRTGSDetails").hide();
                        $("#showDraftDetails").hide();
                        $("#showChequeDetails").show();
                    }else if(mode == "7"){
                        $("#showRTGSDetails").hide();
                        $("#showDraftDetails").show();
                        $("#showChequeDetails").hide();
                    }else{
                        $("#showRTGSDetails").hide();
                        $("#showDraftDetails").hide();
                        $("#showChequeDetails").hide();
                    }
                }
            </script>
            <tr id="showRTGSDetails" style="display: none">
                <td class="text2" height="30"><font color="red">*</font>RTGS Number</td>
                <td class="text2" height="30"><input name="rtgsNo" st id="rtgsNo" type="text" class="textbox" value=""></td>
                <td class="text2" height="30"><font color="red">*</font>RTGS Remarks</td>
                <td class="text2" height="30"><textarea class="textbox" name="rtgsRemarks" id="rtgsRemarks" ></textarea></td>
            <tr id="showDraftDetails" style="display: none">
                <td class="text2" height="30"><font color="red">*</font>Draft Number</td>
                <td class="text2" height="30"><input name="draftNumber" id="draftNumber" type="text" class="textbox" value=""></td>
                <td class="text2" height="30"><font color="red">*</font>Draft Remarks</td>
                <td class="text2" height="30"><textarea class="textbox" name="draftRemarks" id="draftRemarks" ></textarea></td>
            </tr>
            <tr id="showChequeDetails" style="display: none">
                <td class="text2" height="30"><font color="red">*</font>Cheque Number</td>
                <td class="text2" height="30"><input name="chequeNo" id="chequeNo" type="text" class="textbox" value=""></td>
                <td class="text2" height="30"><font color="red">*</font>Cheque Remarks</td>
                <td class="text2" height="30"><textarea class="textbox" name="chequeRemarks" id="chequeRemarks" ></textarea></td>
            </tr>
            <tr>
                <td class="text1" height="30"><font color="red">*</font>Amount</td>
                <td class="text1" height="30"><input name="paidAmount" id="paidAmount" type="text" class="textbox" value="" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="checkPaymentDetails()"></td>
                <td class="text1" height="30"><font color="red">*</font>Estimated Freight Amount</td>
                <td class="text1" height="30"><input name="freightAmount" id="freightAmount" type="text" class="textbox" value="<c:out value="${freightAmount}"/>" readonly>
                    <input name="consignmentOrderId" id="consignmentOrderId" type="hidden" class="textbox" value="<c:out value="${consignmentOrderId}"/>">
                    <input name="totalPaid" id="totalPaid" type="hidden" class="textbox" value="<c:out value="${totalPaid}"/>">
                    <input name="totalAmountWaitingForApproval" id="totalAmountWaitingForApproval" type="hidden" class="textbox" value="<c:out value="${totalAmountWaitingForApproval}"/>"></td>
                    <input name="nextPage" id="nextPage" type="hidden" class="textbox" value="<c:out value="${nextPage}"/>"></td>
            </tr>
            <script type="text/javascript">
                function checkPaymentDetails(){
                    var paidAmount = document.getElementById('paidAmount').value;
                    var freightAmount = document.getElementById('freightAmount').value;
                    var totalPaid = document.getElementById('totalPaid').value;
                    var totalAmountWaitingForApproval = document.getElementById('totalAmountWaitingForApproval').value;
                    var totalamount = (parseFloat(totalPaid)+parseFloat(paidAmount)+parseFloat(totalAmountWaitingForApproval)).toFixed(2);
                    
                    if(parseFloat(totalamount) > parseFloat(freightAmount)){
                        alert("Exceed the freight amount "+paidAmount);
                        document.getElementById('paidAmount').value = "";
                    }
                    
                }
            </script>
            <tr>
                <td class="text2" height="30"><font color="red">*</font>Payment Date</td>
                <td class="text2" height="30"><input name="paidDate" id="paidDate" type="text" class="datepicker" value=""  ></td>
                <td class="text2" height="30" colspan="2">&nbsp;</td>
            </tr>

        </table>
        <br>
        <center>
            <input type="button" value="save" class="button" onClick="submitPage();">
            &emsp;<input type="reset" class="button" value="Clear">
        </center>
        <br>
        <br>

        <c:if test = "${paymentDetails != null}" >
                <table width="100%" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3>S.No</h3></th>
                            <th><h3>Payment Date</h3></th>
                            <th><h3>Consignment No</h3></th>
                            <th><h3>Payment Type</h3></th>
                            <th><h3>Payment Mode</h3></th>
                            <th><h3>PayMode Reference No</h3></th>
                            <th><h3>PayMode Reference Remarks</h3></th>
                            <th><h3>Paid Amount</h3></th>
                            <th><h3>Estimated Freight Amount</h3></th>
                            <th><h3>Status</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${paymentDetails}" var="payment">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${payment.createdDate}"/> </td>
                                <td align="left" class="text2">
                                     <input name="consignmentOrderId" id="consignmentOrderId" type="hidden" class="textbox" value="<c:out value="${payment.consignmentOrderId}"/>"><c:out value="${payment.consignmentNoteNo}"/> </td>
                                <td align="left" class="text2">
                                  <c:if test="${payment.paymentType=='1'}" >
                                    Credit
                                    </c:if>
                                  <c:if test="${payment.paymentType=='2'}" >
                                    Advance
                                    </c:if>
                                  <c:if test="${payment.paymentType=='3'}" >
                                    To Pay and Advance
                                    </c:if>
                                  <c:if test="${payment.paymentType=='4'}" >
                                    To Pay
                                    </c:if>
                                </td>
                                <td align="left" class="text2"><c:out value="${payment.paymentModeName}"/> </td>
                                <td align="left" class="text2">
                                <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                <c:if test="${payment.paymentModeId=='6' }" >
                                   <c:out value="${payment.rtgsNo}"/>
                                    </c:if>

                                     <c:if test="${payment.paymentModeId=='2' }" >
                                   <c:out value="${payment.chequeNo}"/>
                                    </c:if>

                                    <c:if test="${payment.paymentModeId=='7' }" >
                                   <c:out value="${payment.draftNo}"/>
                                    </c:if>
                                </td>
                                       <td align="left" class="text2">
                                      <c:if test="${payment.paymentModeId=='1' }" >
                                    -
                                </c:if>

                                   <c:if test="${payment.paymentModeId=='6' }" >
                                   <c:out value="${payment.rtgsRemarks}"/>
                                   </c:if>

                                      <c:if test="${payment.paymentModeId=='2' }" >
                                   <c:out value="${payment.chequeRemarks}"/>
                                      </c:if>

                                      <c:if test="${payment.paymentModeId=='7' }" >
                                   <c:out value="${payment.draftRemarks}"/>
                                    </c:if>
                                </td>

                                <td align="left" class="text2"><c:out value="${payment.paidAmount}"/></td>
                                <td align="left" class="text2"><c:out value="${payment.freightAmount}"/></td>

                                    <td align="left" class="text2">
                                  <c:if test="${payment.paymentModeId=='2' }" >
                                    <c:if test="${payment.approvalstatus=='0' }" >
                                       <a href="/throttle/viewChequeRequest.do?consignmentOrderId=<c:out value="${payment.consignmentOrderId}"/>&paymentTypeId=<c:out value="${payment.paymentTypeId}"/>&nextPage=<c:out value="${nextPage}"/>/>">view Details</a>
                                    </c:if>
                                    <c:if test="${payment.approvalstatus=='1' }" >
                                       <a href="/throttle/viewChequeRequest.do?consignmentOrderId=<c:out value="${payment.consignmentOrderId}"/>&paymentTypeId=<c:out value="${payment.paymentTypeId}"/>&nextPage=<c:out value="${nextPage}"/>/>">view Details</a>
                                    </c:if>

                                    <c:if test="${payment.approvalstatus=='2' }" >
                                        Approved
                                    </c:if>
                                    <c:if test="${payment.approvalstatus=='3' }" >
                                        Rejected
                                    </c:if>
                                                &nbsp;-
                                    </c:if>
                                     <c:if test="${payment.paymentModeId !='2' }" >
                                         Paid
                                    </c:if>
                              </td>
                                </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>



    </form>
</body>
</html>
