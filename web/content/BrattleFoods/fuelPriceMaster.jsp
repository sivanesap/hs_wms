<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    
    function calculateDiff(val){
        var fuelPrice  =  document.getElementById('fuelPrice'+val).value;
        var basPrice   =  document.getElementById('basePrice').value;
        var diff = parseFloat(fuelPrice) - parseFloat(basPrice);
        document.getElementById('diffPrice'+val).value = diff.toFixed(2);
    }
    
    function calAverage(){
        var fuelPrice1  =  document.getElementById('fuelPrice1').value;
        var fuelPrice2  =  document.getElementById('fuelPrice2').value;
        var fuelPrice3  =  document.getElementById('fuelPrice3').value;
        var fuelPrice4  =  document.getElementById('fuelPrice4').value;
        
        var avgPrice = parseFloat(fuelPrice1)+parseFloat(fuelPrice2)+parseFloat(fuelPrice3)+parseFloat(fuelPrice4);        
        var avg = (avgPrice/4);
        document.getElementById('avgPrice').value = avg.toFixed(2);
        document.getElementById("avg").innerHTML = avg.toFixed(2);
    }
    
    
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#cityName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#cityId').val(Id);
                $itemrow.find('#cityName').val(Name);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    
    
    function resetCityId(){
        if(document.getElementById('cityName').value == ''){
           document.getElementById('cityId').value = '' ;
        }
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Fuel" text="Fuel"/></a></li>
                    <li class=""><spring:message code="hrms.label.FuelPriceMaster" text="FuelPriceMaster"/></li>		
                </ol>
            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <body >
        <%  
            String menuPath = "Fuel  >>  Manage Fuel Price";
            request.setAttribute("menuPath", menuPath);
        %>
        
         <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);                                    
        %>
        
        <form name="fuleprice"  method="post" >
            
            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                <tr align="center">                    
                    <th colspan="4" align="center"  height="30"><div>New Fuel Price</div></th>
                </tr>
                </thead>
                
                  <tr>
                   <td>Base Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;61.36</td>
                  </tr>
                  <tr>
                   <td height="30"></td>
                   <td height="30">
                       <input type="hidden" class="datepicker" style="width:150px;height:30px" id="effectiveDate" name="effectiveDate" value="<%=startDate%>" >
                   </td>
                   <td></td>
               </tr>
               <thead>
                <tr>
                    <th  height="30"><font color="red">*</font>City</th>
                    <th  height="30"><font color="red">*</font>Fuel Price</th>
                    <th  height="30"><font color="red">*</font>Change in Price</th>
                </tr>
                </thead>
                <tr>
                    <td>Chennai<input type="hidden" name="cityId1" value="CHN"/></td>                    
                    <td height="30"><input type="text" id="fuelPrice1" value="0" onKeyPress='return onKeyPressBlockCharacters(event);' onchange="calculateDiff('1');calAverage();" name="fuelPrice1" class="form-control" style="width:150px;height:30px"></td>
                    <td height="30"><input type="text" readonly value="0" id="diffPrice1" name="diffPrice1" class="form-control" style="width:150px;height:30px"></td>
                </tr>
                <tr>
                    <td>Mumbai<input type="hidden" name="cityId2" value="MUM"/></td>                    
                    <td height="30"><input type="text" onKeyPress='return onKeyPressBlockCharacters(event);' value="0" id="fuelPrice2" name="fuelPrice2" onchange="calculateDiff('2');calAverage();" class="form-control" style="width:150px;height:30px"></td>
                    <td height="30"><input type="text" readonly value="0" id="diffPrice2" name="diffPrice2" class="form-control" style="width:150px;height:30px"></td>
                </tr>
                <tr>
                    <td>Delhi<input type="hidden" name="cityId3" value="DEL"/></td>                    
                    <td height="30"><input type="text" onKeyPress='return onKeyPressBlockCharacters(event);' value="0" id="fuelPrice3" name="fuelPrice3" onchange="calculateDiff('3');calAverage();" class="form-control" style="width:150px;height:30px"></td>
                    <td height="30"><input type="text" readonly value="0" id="diffPrice3" name="diffPrice3" class="form-control" style="width:150px;height:30px"></td>
                </tr>
                <tr>
                    <td>Kolkata<input type="hidden" name="cityId4" value="KOL"/></td>                    
                    <td height="30"><input type="text" onKeyPress='return onKeyPressBlockCharacters(event);' value="0" id="fuelPrice4" name="fuelPrice4" onchange="calculateDiff('4');calAverage();" class="form-control" style="width:150px;height:30px"></td>
                    <td height="30">
                        <input type="text" readonly value="0" id="diffPrice4" name="diffPrice4" class="form-control" style="width:150px;height:30px">
                        <input type="hidden" id="basePrice" name="basePrice" class="form-control" value="61.36"/>
                    </td>
                </tr>
                
                <tr>                   
                   <td>Average Price</td>
                   <td id="avg">0</td>
                  </tr>
               
                <tr>
                      <td colspan="4" align="center">
			<input type="button" class="btn btn-success"  value="Save"  onClick="submitPage();">
			<!--<input type="button" class="btn btn-success"  value="Search"  onClick="searchPage();">-->
                        <input type="hidden" id="avgPrice" name="avgPrice" value=""/>
                        <input type="hidden" id="fuelEsc" name="fuelEsc" value="0.5"/>
                      </td>
                  </tr>
            </table>
                 
           <br/>
           <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                <tr height="30">                            
                <th>Chennai </th>
                <th>Mumbai </th>
                <th>Delhi </th>
                <th>Kolkata </th>
                <th>Fuel Price </th>
                <th>Average Price </th>
                <th>Fuel Escalation % </th>
                <th>Fuel Escalation Price</th>
                <th>Fuel Month</th>
                <th>Fuel Year</th>
                </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${viewfuelPriceMaster != null}" >
                        <%
                                            sno++;
                                           String className = "text1";
                                           if ((sno % 1) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                        %>
                        <c:forEach items="${viewfuelPriceMaster}" var="fuelPriceMaster">
                            <tr height="30">
                                <td  align="left" ><c:out value="${fuelPriceMaster.chennaiPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.mumbaiPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.delhiPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.kolkataPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.fuelPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.averagePrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.escalationPercent}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.escalationPrice}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.fuelMonth}"/></td>
                                <td  align="left" ><c:out value="${fuelPriceMaster.fuelYear}"/></td>
                            </tr>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>
      
            <script type="text/javascript">
                function submitPage() {
                    if (document.getElementById('effectiveDate').value == '') {
                        alert("Please select date");
                        document.getElementById('effectiveDate').focus();
                    }
                    
                    else if (document.getElementById('fuelPrice1').value == '') {
                        alert("Please enter chennai fuel price");
                        document.getElementById('fuelPrice1').focus();
                    }
                    else if (document.getElementById('fuelPrice2').value == '') {
                        alert("Please enter mumbai fuel price");
                        document.getElementById('fuelPrice2').focus();
                    }
                    else if (document.getElementById('fuelPrice3').value == '') {
                        alert("Please enter delhi fuel price");
                        document.getElementById('fuelPrice3').focus();
                    }
                    else if (document.getElementById('fuelPrice4').value == '') {
                        alert("Please enter kolkata fuel price");
                        document.getElementById('fuelPrice4').focus();
                    }                    
                     else {
                        document.fuleprice.action = '/throttle/saveFuelPriceMaster.do';
                        document.fuleprice.submit();
                    }
                }
            </script>

        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>

