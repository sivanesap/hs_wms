

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.security.business.SecurityTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>    
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PAPL</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        function submitpage(value) {
            if (value=='add') {
                 document.vehicleListPage.action ='/throttle/addSecurity.do';
                 document.vehicleListPage.submit();
            }else if(value == 'save'){ 
                 var checValidate = selectedItemValidation();        
                if(checValidate == 'SubmitForm'){                
                document.vehicleListPage.action ='/throttle/saveSecurity.do';
                document.vehicleListPage.submit();
                }
            }else if (value == 'search') {
                document.vehicleListPage.action ='/throttle/manageSecurityPage.do';
                document.vehicleListPage.submit();
            }
            
        }
        
        
         function selectedItemValidation(){
        
var index = document.getElementsByName("selectedindex");
var inKm = document.getElementsByName("inKm");
var inHm = document.getElementsByName("inHm");
var vehicleRemarks = document.getElementsByName("vehicleRemarks");
var chec=0;
var mess = "SubmitForm";
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
    if(numberValidation(inKm[i],"in Km")){
    return 'notSubmit';
    }else if(numberValidation(inHm[i],"In Hm")){
    return 'notSubmit';
    }else if(textValidation(vehicleRemarks[i],"vehicleRemarks")){
    return 'notSubmit';
    }
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
//km[0].focus();
return 'notSubmit';
}
return 'SubmitForm';
}


   function validateDetails(name,ind)
        {
          
            var wokm=document.getElementById("workKm"+ind);
            var wohm=document.getElementById("workHm"+ind);            
            var inkm=document.getElementById("inKm"+ind);            
            var inhm=document.getElementById("inHm"+ind);     
            var vehicleRemarks=document.getElementById("vehicleRemarks"+ind);     
            
            
            if(name=='inKm'){                                
                
                if(parseInt(inkm.value) < parseInt(wokm.value)){
                    alert("Enter Valid Km");
                    inkm.value="";                    
                    inhm.focus();
                    inkm.focus();
                }
                
            }else if(name=='inHm'){    
            
            if(parseInt(inhm.value)<parseInt(wohm.value)){                
                    alert("Enter Valid hour meter");
                    inhm.value=""; 
                    vehicleRemarks.focus();
                    inhm.focus();
                }            
            }
        }

    function setSearchValues() {
               var fromDate='<%=request.getAttribute("vehicleFromDate")%>';
                var toDate='<%=request.getAttribute("vehicleToDate")%>';
                var vehicleInPurpose='<%=request.getAttribute("vehicleInPurpose")%>';
                var vehicleStatus='<%=request.getAttribute("vehicleStatus")%>';
                var vno='<%=request.getAttribute("vno")%>';
                if(fromDate!='null'){
                    document.vehicleListPage.vehicleFromDate.value=fromDate;
                }
                if(toDate!='null'){
                    document.vehicleListPage.vehicleToDate.value=toDate;
                }
                if(vehicleInPurpose!='null'){
                    document.vehicleListPage.vehicleInPurpose.value=vehicleInPurpose;
                }
                if(vehicleStatus!='null'){
                    document.vehicleListPage.vehicleStatus.value=vehicleStatus;
                }
                if(vno!='null'){
                    document.vehicleListPage.vno.value=vno;
                } 
                
               document.vehicleListPage.totVehicle.value=document.vehicleListPage.total.value;
               document.vehicleListPage.inVehicle.value=document.vehicleListPage.inTotal.value;
               document.vehicleListPage.outVehicle.value=document.vehicleListPage.outTotal.value;  
        }
        
        
        function setSelectbox(i)
    {       
        
        var selected=document.getElementsByName("selectedindex") ;
        selected[i].checked = 1;
        }
        
        
        
    </script>
    
    <body onLoad="setSearchValues();">
        
        <form method="post" name="vehicleListPage">
            <!-- copy there from end -->
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                    <!-- pointer table -->
                 
                    
                </div>
            </div>
            
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->

<table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
        <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
        </h2></td>
        <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
        </tr>
        <tr id="exp_table" >
        <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
            <div class="tabs" align="left" style="width:850;">
        <ul class="tabNavigation">
		<li style="background:#76b3f1">Manage Vehicle In Out</li>
	</ul>
        <div id="first">
        <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
        <tr>
            <td>Vehicle No</td><td><input name="vno"  class="textbox"  type="text" value="" size="20"></td>
            <td><font color="red">*</font>From Date</td><td><input name="vehicleFromDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleListPage.vehicleFromDate,'dd-mm-yyyy',this)"/></td>
            <td><font color="red">*</font>To Date</td><td><input name="vehicleToDate" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleListPage.vehicleToDate,'dd-mm-yyyy',this)"/></td>
            </tr>
            <tr>
            <td><font color="red">*</font>Vehicle In Purpose</td><td><select class="textbox" name="vehicleInPurpose" >
               <option value="0">---Select---</option>
               <c:if test = "${VehicleInPurpose != null}" >
               <c:forEach items="${VehicleInPurpose}" var="list">
               <option value='<c:out value="${list.purposeId}" />'><c:out value="${list.purposeName}" /></option>
               </c:forEach >
               </c:if>
           </select></td>
            <td><font color="red">*</font>Status</td><td><select class="textbox" name="vehicleStatus" >
            <option value="0">---Select---</option>
            <option value="BOTH">BOTH</option>
            <option value="IN">IN</option>
            <option value="OUT">OUT</option>
            <option value="SCHEDULED">SCHEDULED</option>
          </select></td>
            <td>&nbsp;</td><td><input type="button" name="search" value="Get Vehicles" onClick="submitpage(this.name)" class="button" /></td>
        </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>


<table width="900" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
    <tr>
    <td align="center"><h2 style="font-size:16px; font-weight:bold; ">Summary:</h2></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold; ">In</span></td>
    <td class="bottom1" height="35"><input name="inVehicle" size="5" class="bottom1" type="text" readonly value="0" style="border:none;" /></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold;">Out</span></td>
    <td class="bottom1"><input name="outVehicle"   size="5" class="bottom1" type="text" readonly value="0" style="border:none;" /></td>
    <td class="bottom1" align="left"><span style="font-size:13px; font-weight:bold; ">Total</span></td>
    <td class="bottom1"><input name="totVehicle"  size="5"  class="bottom1" type="text" readonly value="0" style="border:none;" /></td>
    </tr>
    </table>

           
          <c:set var="total" value="0"></c:set>
         <c:set var="inTotal" value="0"></c:set>
         <c:set var="outTotal" value="0"></c:set>
		  <c:if test = "${vehicleList != null}" >
             
                <table width="900" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                   
                    <tr align="center">
                        <td width="33" height="30" class="contentsub">S.No</td>
                        <td width="46" height="30" class="contentsub">WO No</td>
                        <td width="64" height="30" class="contentsub">Vehicle No</td>
                        <td width="65" height="30" class="contentsub">Operation Point</td>
                        <td width="59" height="30" class="contentsub">Purpose</td>
                        <td width="60" height="30" class="contentsub">In Km</td>
                        <td width="60" height="30" class="contentsub">In hm</td>
                        <td width="68" height="30" class="contentsub">Remarks</td>
                        <td width="120" height="30" class="contentsub">IN-Time</td>
                        <td width="120" height="30" class="contentsub">OUT-Time</td>
                        <td width="29" height="30" class="contentsub">IN</td>
                        <td width="49" height="30" colspan="5" class="contentsub">OUT</td>
                        
                    </tr>
         
         <%                          

            String classText = "";
            int index = 0;
         %>     

                    <c:forEach  items="${vehicleList}" var="list"> 
                    
             <%
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr> 
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <input type="hidden" name="woIds" value='<c:out value="${list.woId}"/>'>                            
                            <input type="hidden" id="workKm<%=index%>" name="workKm" value='<c:out value="${list.workKm}"/>'>
                            <input type="hidden" id="workHm<%=index%>" name="workHm" value='<c:out value="${list.workHm}"/>'>
                            <input type="hidden" name="compId" value='<c:out value="${list.vehicleOperationPointId}"/>'>
                            <input type="hidden" name="purposeId" value='<c:out value="${list.purposeId}"/>'>
                            
                            <td class="<%=classText %>" height="30">
                            <c:if test = "${list.woId != 0}" >
                            <c:out value="${list.woId}"/>
                            </c:if>
                            <c:if test = "${list.woId == 0}" >
                            NA
                            </c:if>
                            </td>
                            
                            <input type="hidden" name="vioId" value='<c:out value="${list.vioId}"/>'>
                            <td class="<%=classText %>" height="30"><c:out value="${list.vehicleNumber}"/></td>
                            <input type="hidden" name="vehicleNumbers" value='<c:out value="${list.vehicleNumber}"/>' >
                            <td class="<%=classText %>" height="30"><c:out value="${list.vehicleOperationPoint}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.purposeName}"/></td>                            
                            <c:if test = "${list.vehicleKm != 0}" >
                            <td class="<%=classText %>" height="30"><c:out value="${list.vehicleKm}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.hourMeter}"/></td>                                                        
                            <input type="hidden" name="inKm" value='<c:out value="${list.vehicleKm}"/>'>
                            <input type="hidden" name="inHm" value='<c:out value="${list.hourMeter}"/>'>                            
                            </c:if>
      
                            <c:if test = "${list.vehicleKm == 0}" >
                            <td class="<%=classText %>" height="30">
                            <input type="text" size="5"  id="inKm<%=index%>" onchange="validateDetails(this.name,<%=index%>)"  name="inKm" value="" />
                            </td>
                            <td class="<%=classText %>" height="30">
                            <input type="text" size="5"  id="inHm<%=index%>" onchange="validateDetails(this.name,<%=index%>)" name="inHm"  value="" />
                            </td>
                            </c:if>    
                            <c:if test = "${list.intime == '' || list.outtime == ''}" >                                
                            <td class="<%=classText %>" >
                            <textarea name="vehicleRemarks" id="vehicleRemarks<%=index%>" class="text2"><c:out value="${list.remarks}"/></textarea>
                            </td>
                            </c:if> 
                            <c:if test = "${list.intime != '' && list.outtime != ''}" >                                
                            <td class="<%=classText %>" >                            
                                <c:out value="${list.remarks}"/>                            
                            </td>
                            <input type="hidden" name="vehicleRemarks" value='<c:out value="${list.remarks}"/>'>
                            </c:if> 

                            
                            <td class="<%=classText %>" height="30"><c:out value="${list.intime}"/></td>
                            <td height="30" class="<%=classText %>"><c:out value="${list.outtime}"/></td>
                            
                            <c:if test = "${list.intime == ''}" >
                            <td width="20" height="30" class="<%=classText %>">
                            <input type="checkbox"  onclick="setSelectbox(<%= index %>)"  name="in" value='<%=index%>'>
                            </td>
                            </c:if>    
                            <c:if test = "${list.intime != ''}" >
                                <c:set var="inTotal" value="${inTotal+1}"></c:set>
                            <td width="20" height="30" class="<%=classText %>">
                            <input type="checkbox"  name="in" value='<%=index%>' CHECKED DISABLED>
                            </td>
                            </c:if>                              
                            <c:if test = "${list.outtime == ''}" >
                            <td width="20" height="30" class="<%=classText %>">
                            <input type="checkbox"  onclick="setSelectbox(<%= index %>)"  name="out" value='<%=index%>'>
                            </td>
                            </c:if>    
                            <c:if test = "${list.outtime != ''}" >
                                <c:set var="outTotal" value="${outTotal+1}"></c:set>
                            <td width="20" height="30" class="<%=classText %>">
                            <input type="checkbox"  name="out"  value='<%=index%>' CHECKED  DISABLED>
                            </td>
                            </c:if>  
                            <td width="20" height="30" class="<%=classText %>"><input type="hidden"  name="selectedindex" value='<%=index%>'></td>
                        </tr>
                  <% index++;%>        
                  <c:set var="total" value="${total+1}"></c:set>
           </c:forEach >
           
            </table>
     </c:if>   
<input type="hidden"  name="total"   value='<c:out value="${total}"/>' >        
<input type="hidden"  name="inTotal"   value='<c:out value="${inTotal}"/>' >
<input type="hidden"  name="outTotal"   value='<c:out value="${outTotal}"/>' >
 
        <br>    
            <center>
               <input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
               <input type="button" name="add" value="Add" onClick="submitpage(this.name)" class="button" /> 

            </center>
            <br>
        </form>
    </body>
</html>
