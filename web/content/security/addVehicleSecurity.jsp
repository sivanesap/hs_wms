<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.security.business.SecurityTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
 
</head>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script>
    function alertValidation()
    {
        
          if(textValidation(document.dept.vno,"vehicle No"));               
       else if(isSelect(document.dept.vehicleInPurpose,"purpose"));
      else if(numberValidation(document.dept.vehicleKm,"In KM"));
       else if(numberValidation(document.dept.vehicleHm,"In HM"));                        
        else if(textValidation(document.dept.remarks,"remarks"));                 
        else{
       document.dept.action= "/throttle/addVehicleSecurity.do";
       document.dept.submit();
       }
    }
    
     var httpRequest1;          
            function isThisVehicleIn(regNo) {  
                        
            
                var url='/throttle/isThisVehicleIn.do?regNo='+regNo;  
            
                if (window.ActiveXObject)
                    {
                        httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                        {
                            httpRequest1 = new XMLHttpRequest();
                        }
                        httpRequest1.open("POST", url, true);
                        httpRequest1.onreadystatechange = function() {go(); } ;
                        httpRequest1.send(null);
                    }
                    

     function go() {                         
            if (httpRequest1.readyState == 4) {
                if (httpRequest1.status == 200) {
                    var response = httpRequest1.responseText;                         
                    if(response!="" && response!='null' ){
                     alert(response +" is Already In");   
                     document.dept.vno.value="";
                     document.dept.vno.focus();
                    }
                    
                        
                     
                                     
                }
            }
            
            
        }
</script>
<body>
<form name="dept" method="post" >
 <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
                            <Td height="30" width="500" align="center" colspan="2" class="contenthead"><div class="contenthead">Add Vehicle</div></Td>
                        </tr> 
<tr>
<td class="text2" height="30"><font color=red>*</font>Vehicle Number</td>
<td class="text2" height="30"><input name="vno" type="text" style="width:130px;" class="textbox" onChange="isThisVehicleIn(this.value)" value="" maxlength="10" ></td>
 


</tr>

<tr>
<td class="text1"  ><font color=red>*</font>Operation Point</td>
<td class="text1"  >
     <select class="textbox" name="vehicleOperationPointId" width="20" style="width:130px;margin:5px 0 5px 0;">         
        <c:if test = "${opList != null}" >
        <c:forEach items="${opList}" var="opData"> 
        <c:if test = "${opData.vehicleOperationPointId ==0}" >
        <option selected value='<c:out value="${opData.vehicleOperationPointId}" />'><c:out value="${opData.vehicleOperationPoint}" /></option>
        </c:if>  	
        
        <c:if test = "${opData.vehicleOperationPointId !=0}" >
        <option value='<c:out value="${opData.vehicleOperationPointId}" />'><c:out value="${opData.vehicleOperationPoint}" /></option>
        </c:if>  	
        </c:forEach >
        </c:if>  	
     </select>
</td>
</tr>
<tr>
<td class="text2"  ><font color=red>*</font>Purpose</td>
<td class="text2"  >
     <select class="textbox" name="vehicleInPurpose" style="width:130px;margin:5px 0 5px 0;"> 
        <option value="0">---Select---</option>
        <c:if test = "${VehicleInPurpose != null}" >
        <c:forEach items="${VehicleInPurpose}" var="list"> 
        <option value='<c:out value="${list.purposeId}" />'><c:out value="${list.purposeName}" /></option>
        </c:forEach >
        </c:if>  	
     </select>
</td>
</tr>

<tr>
<td class="text1" height="30"><font color=red>*</font>In Km</td>
<td class="text1" height="30"><input type="text" style="width:130px;" class="textbox" name="vehicleKm"></td>
</tr>
<tr>
<td class="text2" height="30"><font color=red>*</font>In Hm</td>
<td class="text2" height="30"><input type="text" style="width:130px;" class="textbox" name="vehicleHm"></td>
</tr>

<tr>
<td class="text1" height="30"><font color=red>*</font>Remarks</td>
<td class="text1" height="30"><textarea class="textbox" style="width:130px;" name="remarks"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Add" class="button" name="add" onClick="alertValidation();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
