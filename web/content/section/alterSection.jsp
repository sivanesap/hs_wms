<%-- 
Document   : modifyDesig
Created on : Nov 03, 2008, 6:14:28 PM
Author     : Vijay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
 
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.racks.business.RackTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

function submitpage(value)
{
 

var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){
    

document.modify.action='/throttle/alterSection.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var sectionNames = document.getElementsByName("sectionNames");
var sectionCodes = document.getElementsByName("sectionCodes");
var desc = document.getElementsByName("descriptions");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if(textValidation(sectionNames[i],"Section Name")){
            return;
        }else if(textValidation(desc[i],"Description")){
            return;                    
        }else if(textValidation(sectionCodes[i],"Section Code")){
            return;
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
document.modify.action='/throttle/alterSection.do';
document.modify.submit();
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

</head>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onload="document.modify.desigNames[0].focus();">
<form method="post" name="modify"> 
 <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0; %>        

<table width="500" align="center" id="bg" cellpadding="0" cellspacing="0" class="border">

<c:if test = "${SectionList != null}" >
 
<tr>
<td height="30" class="contentsub"><div class="contentsub"></div></td>  
<td height="30" class="contentsub"><div class="contentsub">S.No</div></td>  
<td class="contentsub" height="30"><div class="contentsub">Section Code</div> </td>
<td class="contentsub" height="30"><div class="contentsub">Section</div> </td>
<td class="contentsub" height="30"><div class="contentsub">Description</div></td>
<td class="contentsub" height="30"><div class="contentsub">Service Type</div></td>
<td class="contentsub" height="30"><div class="contentsub">Status</div></td>
<td class="contentsub" height="30"><div class="contentsub">Select</div></td>
</tr>
</c:if>
<c:if test = "${SectionList != null}" >
<c:forEach items="${SectionList}" var="sec"> 		
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
 <td class="<%=classText %>" height="30"><input type="hidden" name="sectionIds" value='<c:out value="${sec.sectionId}"/>'>  </td>
<td class="<%=classText %>" height="30"><%=index + 1%></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="sectionNames" value="<c:out value="${sec.sectionName}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="sectionCodes" value="<c:out value="${sec.sectionCode}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="descriptions" value="<c:out value="${sec.description}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30">
    <c:if test="${serviceTypeList != null}">
    <select name="serviceTypeIds" id="serviceTypeId<%= index %>" class="textbox" onchange="setSelectbox(<%= index %>)">
            <option value="0">--Select--</option>
        <c:forEach items="${serviceTypeList}" var="service">
            <option value="<c:out value="${service.serviceTypeId}"/>"><c:out value="${service.serviceTypeName}"/></option>
        </c:forEach>
    </select>
    <script>
        document.getElementById("serviceTypeId"+<%= index %>).value = '<c:out value="${sec.serviceTypeId}"/>';
    </script>
    </c:if>
</td>
<td height="30" class="<%=classText %>"> <div align="center"><select name="activeInds" class="textbox" onchange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${sec.activeInd == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div> 
</td>
<td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if> 
</table>
<center>
<br>
<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
<input type="hidden" name="reqfor" value="designa" />
</center>
<br>
</form>
</body>
</html>
