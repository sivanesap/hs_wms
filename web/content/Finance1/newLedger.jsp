<%-- 
    Document   : newLedger
    Created on : 24 Oct, 2012, 9:07:54 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add Ledger</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>

        function setLevelValues(levelValue)
        {
            var temp = levelValue.split("~");
            //alert(temp[0]+":::"+temp[1]+":::"+temp[2]);
            document.add.primaryID.value = temp[0];
            document.add.levelgroupId.value = temp[1];
            document.add.fullName.value = temp[2];

        }
        function submitPage()
        {
            if(textValidation(document.add.ledgerName,'ledgerName')){
                return;
            }else if(document.add.primaryLevelList.value=='0'){
                alert("Please Select Group");
                return 'false';
            } else if(isEmpty(document.add.description.value)){
                alert("Please Enter Description");
                document.add.description.focus();
                return;
            
            } else if(isEmpty(document.add.openingBalance.value)){
                alert("Please Enter opening balance");
                document.add.openingBalance.focus();
                return;
            }
            document.add.action='/throttle/saveNewLedger.do';
            document.add.submit();
        }
    </script>
    <body>
        <form name="add" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table align="center" width="800" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="4" class="contenthead">Add Ledger</Td>
                </tr>
                <input type="hidden" name="primaryID" id="primaryID" />
                <input type="hidden" name="levelgroupId" id="LevelGroupID" />
                <input type="hidden" name="fullName" id="fullName"  />
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Ledger Name</td>
                    <td class="text1">
                        <input name="ledgerName" type="text" class="textbox" id="ledgerName" value="" size="20">
                    </td>
                </tr>
                <tr height="30">
                    <td class="text2" height="30"><font color="red">*</font>Primary / Level master</td>
                    <td class="text2" height="30">
                        <select class="textbox" name="primaryLevelList" id="primaryLevelList" style="width:350px;" onchange="setLevelValues(this.value)">
                            <option value="0">---Select---</option>
                            <c:if test = "${ledgerlevelList != null}" >
                                <c:forEach items="${ledgerlevelList}" var="levelM">
                                    <option value='<c:out value="${levelM.groupID}" />~<c:out value="${levelM.levelgroupId}" />~<c:out value="${levelM.levelgroupName}" />'><c:out value="${levelM.levelgroupName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Description</td>
                    <td class="text1" height="30">
                        <input type="text" name="description" id="description" style="width: 120px" class="textbox">
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Opening Balance</td>
                    <td class="text1" height="30">
                        <input type="text" name="openingBalance" id="openingBalance" style="width: 120px" class="textbox">
                        <select class="textbox" name="acctType" id="acctType" >
                            <option value="CREDIT">CREDIT</option>
                            <option value="DEBIT">DEBIT</option>
                        </select>
                    </td>
                </tr>

            </table>
            <br>
            <br>
            <center>
                <input type="button" class="button" value="Save" onclick="submitPage();" />
                &emsp;<input type="reset" class="button" value="Clear">


            </center>
        </form>
    </body>
</html>

