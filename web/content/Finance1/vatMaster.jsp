<%-- 
    Document   : vatMaster
    Created on : Dec 12, 2012, 1:49:25 PM
    Author     : DineshKumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript">

function submitpage(value)
{


var checValidate = selectedItemValidation();
var splt = checValidate.split("-");
if(splt[0]=='SubmitForm' && splt[1]!=0 ){


document.modify.action='/throttle/handleAlterVat.do';
document.modify.submit();
}
}

function setSelectbox(i)
{
var selected=document.getElementsByName("selectedIndex") ;
selected[i].checked = 1;
}

function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var mfrNames = document.getElementsByName("vats");
var desc = document.getElementsByName("descs");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
        if(index[i].checked){
        chec++;
        if(floatValidation(mfrNames[i],"VAT")){
            return;
        }else if(textValidation(desc[i],"Desription")){
            return;
        }
        }
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
desigName[0].focus();
//break;
}
document.modify.action='/throttle/handleAlterVat.do';
document.modify.submit();
}



function addVat(){
    document.modify.action='/throttle/handleAddVatPage.do';
    document.modify.submit();
}

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}
</script>

</head>


<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->

<body onload="document.modify.desigNames[0].focus();">
<form method="post" name="modify">
 <%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>
<br>
 <% int index = 0; %>

<table width="500" align="center" id="bg" cellpadding="0" cellspacing="0" class="border">
 <c:if test = "${vatList != null}" >

<tr>
<td class="contentsub" height="30"> </td>
<td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
<td class="contentsub" height="30"><div class="contentsub">VAT(%)</div> </td>
<td class="contentsub" height="30"><div class="contentsub">Effective Date</div> </td>
<td class="contentsub" height="30"><div class="contentsub">Description</div></td>
<td class="contentsub" height="30"><div class="contentsub">Status</div></td>
<td class="contentsub" height="30"><div class="contentsub">Select</div></td>
</tr>
</c:if>
<c:if test = "${vatList != null}" >
<c:forEach items="${vatList}" var="VL">
<%

String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
<td class="<%=classText %>" height="30"><%=index + 1%></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="vats" value="<c:out value="${VL.vat}"/>" onchange="setSelectbox(<%= index %>)"></td>
<td class="<%=classText %>" height="30"><textarea name="descs" class="textbox" rows="2" cols="60" onchange="setSelectbox(<%= index %>);" > <c:out value="${VL.desc}"/> </textarea> </td>
<td height="30" class="<%=classText %>"> <div align="center"><select name="activeInds" class="textbox" onchange="setSelectbox(<%= index %>)">
<c:choose>
<c:when test="${VL.activeInd == 'Y'}">
<option value="Y" selected>Active</option>
<option value="N">InActive</option>
</c:when>
<c:otherwise>
<option value="Y">Active</option>
<option value="N" selected>InActive</option>
</c:otherwise>
</c:choose>
</select>
</div>
</td>
<td width="77" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
<%
index++;
%>
</c:forEach >
</c:if>
</table>
<center>
<br>
<input type="button" name="save" value="Save" onClick="submitpage(this.name)" class="button" />
<input type="button" name="add" value="Add" onClick="addVat(this.name)" class="button" />

</center>
<br>
<br>
</form>
</body>
</html>
