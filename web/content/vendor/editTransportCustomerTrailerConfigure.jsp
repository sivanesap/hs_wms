
<%-- 
    Document   : configureVehicleTrailerPage
    Created on : Apr 27, 2015, 10:27:19 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib uri='http://java.sun.com/jstl/core' prefix='c'%>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<!--<script language="javascript" type="text/javascript" src="/throttle/js/ajaxFunction.js"></script>-->

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />

<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-latest.js"></script>


<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script>
    function savePage(val) {
//        alert("Val"+val);
// document.getElementById("trailerNo"+val).value="";
        document.vehicleVendorContract.action = "/throttle/saveTrailerConf.do";
        document.vehicleVendorContract.submit();

    }

</script>
<script>
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
    {
        var temp = obj.value;

        // avoid changing things if already formatted correctly
        var reg0Str = '[0-9]*';
        if (decimalPlaces > 0) {
            reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
        } else if (decimalPlaces < 0) {
            reg0Str += '\\.?[0-9]*';
        }
        reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
        reg0Str = reg0Str + '$';
        var reg0 = new RegExp(reg0Str);
        if (reg0.test(temp))
            return true;

        // first replace all non numbers
        var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
        var reg1 = new RegExp(reg1Str, 'g');
        temp = temp.replace(reg1, '');

        if (allowNegative) {
            // replace extra negative
            var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
            var reg2 = /-/g;
            temp = temp.replace(reg2, '');
            if (hasNegative)
                temp = '-' + temp;
        }

        if (decimalPlaces != 0) {
            var reg3 = /\./g;
            var reg3Array = reg3.exec(temp);
            if (reg3Array != null) {
                // keep only first occurrence of .
                //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                reg3Right = reg3Right.replace(reg3, '');
                reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                temp = temp.substring(0, reg3Array.index) + '.' + reg3Right;
            }
        }

        obj.value = temp;
    }
    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        }
        else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
   function checkTrailerAvalible() {

        var trailorUnits =<c:out value="${trailerUnits}"/>;
//        alert("vehicleUnits ==== " + vehicleUnits);
//        alert("trailorUnits ==== " + trailorUnits);

        if (trailorUnits == "" && trailorUnits == "0") {
            $("#trailers1").hide();
            $("#trailers").hide();
            $("#tabs").tabs("select", 2);
            $("ul#myTab li:nth-child(1)").addClass("active");
            $("#vehicles").addClass("active");
        }
    }

   function checkTrailerNos(val) {
//        alert("Sno"+val);
        var sno = val - 1;
        var trailerNoOld = document.getElementById("trailerNo" + sno).value;
        var trailerNoNew = document.getElementById("trailerNo" + val).value;
//        alert("trailerNoOld"+trailerNoOld);
//        alert("trailerNoNew"+trailerNoNew);
        if (trailerNoNew == trailerNoOld) {
            document.getElementById("checkTrailerNo").value = "1";
            document.getElementById("trailerNo" + val).value = "";
            document.getElementById("trailerNo" + val).focus();
            alert("Trailer No's already Exit's");
        } else {
            document.getElementById("checkTrailerNo").value = "0";
        }
    }

    //autofill vehicle No
  //  function vehicleAutoFill(Sno){
     //   alert("hiiii");

   // }
</script>


<html>
    <body onload="checkTrailerAvalible();">

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <input type="hidden" name="ownership" id="ownership" value="2"/>
            <input type="hidden" name="custId" id="custId" value="<c:out value="${custId}"/>" class="textbox">
            <input type="hidden" name="regNoCheck" value='exists' >
            <input type="hidden" name="Status" id="Status" value='' >
            <input type="hidden" name="contractId" id="contractId" value='<c:out value="${contractId}"/>' >
            
            <input type="hidden" name="checkTrailerRegNo" id="checkTrailerRegNo" value='' >
            <div id="tabs">
                

              

                            <!--<script>
                                function saveVendorContract(){
                                    document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                    document.customerContract.submit();
                                }
                             </script>-->
                            <div id="vehicles">
                                 <c:if test= "${trailers != null}">
                            <table border="1" id="table" >
                                <thead>
                                    <tr class="contenthead">
                                        <th>Trailer Type Name's</th>
                                        <th>Trailer Unit's</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><c:out value="${trailerType}"/></td>
                                        <td><c:out value="${trailerUnits}"/></td>
                                    </tr>
                                </tbody>
                                <input type="hidden" name="trailerTypeId" id="trailerTypeId" value="<c:out value="${trailerTypeId}"/>" class="textbox">
                                <input type="hidden" name="typeId" id="typeId" value="<c:out value="${trailerTypeId}"/>"
                            </table>
                            <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                <thead>
                                    <tr>
                                <th align="center"><h3>S.No</h3></th>
                                <th align="center"><h3>Trailer No</h3></th>
                                <th align="center"><h3>Agreed Date</h3></th>
                                 <th align="center"><h3>Remarks</h3></th>
                                <th align="center"><h3>Active Status</h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${trailers}" var="vehi">
                                        <%
                                            String classText = "";
                                            int oddEven = index % 1;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }

                                        %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index%></td>


                                    <input type="hidden" name="contractId" id="contractId<%=index%>" value=" <c:out value="${contractId}"/>"/>
                                    <input type="hidden" name="id" id="id<%=index%>" value="<c:out value="${vehi.id}"/>"/>
                                    <input type="hidden" name="trailerId" id="trailerId<%=index%>" value="<c:out value="${vehi.trailerId}"/>"/>
                                   



                                    <td class="<%=classText%>"   ><input type="text" name="trailerNo" id="trailerNo<%=index%>"  value="<c:out value="${vehi.trailerNo}"/>" readOnly/></td>
                                    <td class="<%=classText%>"   ><input type="text" name="agreedDate" id="agreedDate<%=index%>" class="datepicker" value="<c:out value="${vehi.agreedDate}"/>"/></td>
                                     <td class="<%=classText%>"   ><input type="text" name="trailerRemarks" id="trailerRemarks<%=index%>"  value="<c:out value="${vehi.remarks}"/>"/></td>

                                    <td class="<%=classText%>"   >
                                        <select name="activeIndVehicle" id="activeIndVehicle<%=index%>" onchange="setActiveIndRate('<%=index%>');">
                                            <c:if test="${vehi.activeInd == 'Y'}" >
                                                <option value="Y" selected>Active</option>
                                                <option value="N">In-Active</option>
                                            </c:if>
                                            <c:if test="${vehi.activeInd == 'N'}" >
                                                <option value="N" selected>In-Active</option>
                                                <option value="Y" >Active</option>
                                            </c:if>
                                            <!--                                        <script type="text/javascript">
        //                                            <c:if test="${vehi.activeInd == 'Y'}">
        //                                            document.getElementById("activeIndVehicle<%=index%>").value = 'Y';
        //                                            </c:if>
        //                                            <c:if test="${vehi.activeInd == 'N'}">
        //                                            document.getElementById("activeIndVehicle<%=index%>").value = 'N';
        //                                            </c:if>
        //                                        </script>-->    </select>
                                            <input type="hidden" name="activeInd" id="activeInd<%=index%>" value="<c:out value="${vehi.activeInd}"/>" />

                                        <script type="text/javascript">

                                            function setActiveIndRate(sno) {
                                                document.getElementById("activeInd" + sno).value = document.getElementById("activeIndVehicle" + sno).value;
                                            }
                                        </script>

                                    </td>
                                    </tr>
                                    <%index++;%>
                                </c:forEach>

                                </tbody>
                            </table>
                                
                                <br>
                                 <center>
                                     <a><input type="button" class="button" value="AddNewRow" id="AddNewRowFullTruck" name="AddNewRow" onclick="addNewRowFullTruck1();"/></a></center>
                                <script>
         function autoFill(sno){


       
         $(document).ready(function () {

         //var sno=1;
                // Use the .autocomplete() method to compile the list based on input from user
                $('#trailerNo'+sno).autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getTrailerNoForTransportCustomer.do",
                            dataType: "json",
                            data: {
                                trailerNo: (request.term).trim(),
                                trailerTypeId: 1
                            },
                            success: function (data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        var value = ui.item.Name;

                        var tmp = value.split('-');

                        $('#trailerId'+sno).val(tmp[0]);
                        $('#trailerNo'+sno).val(tmp[1]);

                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("autocomplete")._renderItem = function (ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });


         }




                                </script>
                                <script>
                                    var contain = "";
                                 function addNewRowFullTruck1(){
                                      var vehicleCount = <c:out value="${trailerUnits}"/>;
                                          if (<%=index%> <= vehicleCount) {
                                        $("#AddNewRowFullTruck").hide();
                                        var iCnt = <%=index%>;
                                        var rowCnt = <%=index%> ;
                                        // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                        contain = $($("#routedeD")).css({
                                            padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                            borderTopColor: '#999', borderBottomColor: '#999',
                                            borderLeftColor: '#999', borderRightColor: '#999'
                                        });
                                        $(contain).last().after(' <table class="contentsub" id="routeInfoDetailsFullTrucks' + iCnt + '" border="1"  width="73%">\n\
                                                                <tr>\n\
                                                                <td>Sno</td>\n\
                                                                <td>TrailerNo</td>\n\
                                                                <td>Agreed Date</td>\n\
                                                                <td>Remarks</td>\n\
                                                                </tr>\n\
                                                                <tr><td>' + rowCnt + '</td>\n\
                                                                <input type="hidden" name="trailerId" id="trailerId' + iCnt + '" value=""/>\n\
                                                                <td><input type="hidden" name="trailerTypeIdcheck" id="trailerTypeIdcheck' + iCnt + '" value="<c:out value="${trailerTypeId}"/>" class="textbox">\n\
                                                                 <input type="hidden" name="id" id="id' + iCnt +'" value="0"/> \n\
                                                             <input type="text" name="trailerNo" id="trailerNo' + iCnt + '" value="" onchange="getTrailerDetails(' + iCnt + ');checkTrailerNos(' + iCnt + ');"/></td>\n\
                                                               <td><input type="text" name="agreedDate" id="agreedDate' + iCnt + '" class="datepicker" /></td>\n\
                                                                 <td><input type="text" name="trailerRemarks" id="trailerRemarks' + iCnt + '" value="" /><input type="hidden" name="activeInd" id="activeInd' + iCnt + '" value="Y" /></td>\n\
                                                               <table><tr>\n\
                                                                <td><input class="button" type="button" name="addRouteDetailsFullTrucks" id="addRouteDetailsFullTrucks' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + (parseFloat(iCnt) + parseFloat(rowCnt)) + ',' + iCnt + ',' +<c:out value="${trailerUnits}"/> + ')" />\n\
                                                                <input class="button" type="button" name="removeRouteDetailsFullTrucks" id="removeRouteDetailsFullTrucks' + iCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" /></td>\n\
                                                                </tr></table></td></tr></table><br><br>');autoFill(iCnt);
                                        $('#btAdd').click(function() {

                                            iCnt = iCnt + 1;
                                            $(contain).last().after('<table id="mainTabledeD" ><tr><td>\
                                          <table  class="contenthead" id="routeDetailsdeD' + iCnt + '" border="1" width="100%">\n\
                                         </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxdeD(iCnt);
                                            callDestinationAjaxdeD(iCnt);
                                            $('#maindeD').after(contain);
                                        });
                                        $('#btRemove').click(function() {
                                            //                                        alert($('#routeInfoDetailsFullTruck tr').size());
                                            if ($(contain).size() > 1) {
                                                $(contain).last().remove();
                                                iCnt = iCnt - 1;
                                            }
                                        });
                                        }else{

                                            alert("Vehicle Limit exceeded!!");
                                        }
                                   }

                                    $(document).ready(function() {

                                        $("#datepicker").datepicker({
                                            showOn: "button",
                                            buttonImage: "calendar.gif",
                                            buttonImageOnly: true

                                        });



                                    });

                                    $(function() {
                                        //	alert("cv");
                                        $(".datepicker").datepicker({
                                            /*altField: "#alternate",
                                             altFormat: "DD, d MM, yy"*/
                                            changeMonth: true, changeYear: true
                                        });

                                    });

                                    // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                    var divValue, values = '';
                                    function GetTextValue() {
                                        $(divValue).empty();
                                        $(divValue).remove();
                                        values = '';
                                        $('.input').each(function() {
                                            divValue = $(document.createElement('div')).css({
                                                padding: '5px', width: '200px'
                                            });
                                            values += this.value + '<br />'
                                        });
                                        $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                        $('body').append(divValue);
                                    }
                                    var temp;
                                     function addRows(val, valu, trailerUnit) {
//                          alert("trailerUnit" + trailerUnit);
//                          alert(val);
                            var loadCnt = val;
                            var count12 = <c:out value="${trailerUnits}"/>;

                            //  alert(loadCnt);
                            var routeInfoSize = $('#routeInfoDetailsFullTrucks' + valu + ' tr').size();
                            //  alert(routeInfoSize);
                            var routeInfoSizeNew = routeInfoSize+1;
                            if (parseInt(count12) >= parseInt(routeInfoSize)) {
                                var addRouteDetails = "addRouteDetailsFullTrucks" + valu;
                                var routeInfoDetails = "routeInfoDetailsFullTrucks" + valu;
                                $('#routeInfoDetailsFullTrucks' + valu + ' tr').last().after('<tr><td>' + routeInfoSizeNew + '</td><td><input type="hidden" name="trailerType" id="trailerType' + routeInfoSizeNew + '" value="<c:out value="${trailerType}"/>"/>\n\
                                  <input type="hidden" name="trailerId" id="trailerId' + routeInfoSizeNew + '" value=""/>\n\
                           <input type="hidden" name="id" id="id' + routeInfoSizeNew +'" value="0"/> \n\
                            <input type="hidden" name="trailerTypeIdcheck" id="trailerTypeIdcheck' + routeInfoSizeNew + '" value="<c:out value="${trailerTypeId}"/>" class="textbox">\n\
                             <input type="text" name="trailerNo" id="trailerNo' + routeInfoSizeNew + '" value=""  onchange="getTrailerDetails(' + routeInfoSizeNew + ');checkTrailerNos(' + routeInfoSizeNew + ');"/></td>\n\
                             <td><input type="text" name="agreedDate" id="agreedDate' + routeInfoSizeNew + '" class="datepicker" /></td>\n\
                            <td><input type="text" name="trailerRemarks" id="trailerRemarks' + routeInfoSizeNew + '" value="" /><input type="hidden" name="activeInd" id="activeInd' + routeInfoSizeNew + '" value="Y" /></td></tr>');autoFill(routeInfoSizeNew);
                                routeInfoSizeNew++;
                            }
                        }

                                        //

                                       

             
                                        $(document).ready(function() {

                                            $("#datepicker").datepicker({
                                                showOn: "button",
                                                buttonImage: "calendar.gif",
                                                buttonImageOnly: true

                                            });



                                        });

                                        $(function() {
                                            //	alert("cv");
                                            $(".datepicker").datepicker({
                                                /*altField: "#alternate",
                                                 altFormat: "DD, d MM, yy"*/
                                                changeMonth: true, changeYear: true
                                            });

                                        });

                                    


                                    function deleteRow(val,v1) {
                                        var loadCnt = val;
                                        var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                        var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                        if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                            loadCnt = loadCnt - 1;
                                        } else {
                                            alert('One row should be present in table');
                                        }
                                    }
                                  
                                  
                                  
                                  
             
                    var httpReq;
                var temp = "";
                function getModelAndTonnage(val,str){ 
//                alert(str);
                    $.ajax({
                                       url: "/throttle/getModelForVendor.do",
                                       dataType: "text",
                                       data: {
                                          typeId:document.vehicleVendorContract.typeId.value,
//                                           mfrId:document.addVehicle.mfr.value
                                       },
                                       success: function(temp) {
//                                           alert(data);
                                           if (temp != '') {

                                      setOptions1(temp, document.getElementById("model" + val));
                                           }else{
                                              setOptions1(temp, document.getElementById("model" + val));
                                       alert('There is no model based on Vehicle Type Please add first');
                                           }
                                       }
                                   });
                             // for tonnage now

                      $.ajax({
                                       url: "/throttle/getTonnage.do",
                                       dataType: "text",
                                       data: {
                                          typeId:document.vehicleVendorContract.typeId.value
                                          },
                                       success: function(temp) {
                                          // alert(data);
                                           if (temp != '') {
//                                     setOptions1(temp, document.getElementById("model" + val));
//                                       setOptions2(temp,document.customerContract.seatCapacity);
                                        setOptions2(temp, document.getElementById("seatCapacity" + val));
                                           }else{
                                       alert('Tonnage is not there please set first in vehicle type');
                                           }
                                       }
                                   });
               }
                                   function setOptions1(text,variab) {
//                                  alert("setOptions on page")
//                                alert("text = "+text+ "variab = "+variab)
                                        variab.options.length = 0;
                                        //                                alert("1")
                                        var option0 = new Option("--select--", '0');
                                        //                                alert("2")
                                        variab.options[0] = option0;
                                        //                                alert("3")


                                        if (text != "") {
                                            //                                    alert("inside the condition")
                                            var splt = text.split('~');
                                            var temp1;
                                            variab.options[0] = option0;
                                            for (var i = 0; i < splt.length; i++) {
                                                //                                    alert("splt.length ="+splt.length)
                                                //                                    alert("for loop ="+splt[i])
                                                temp1 = splt[i].split('-');
                                                option0 = new Option(temp1[1], temp1[0])
                                                //alert("option0 ="+option0)
                                                variab.options[i + 1] = option0;
                                            }
                                        }
                                    }

                                function setOptions2(text) {
                                  if (text != "") {
                               document.getElementById('seatCapacity').value=text;

                                    }
                                    }


            // raja   


                                    var httpRequest;
                                    function getVehicleDetails(val) {
                                     //   alert("entered= " + val);
                                        //                alert(document.vehicleVendorContract.vehicleRegNo.value);
                                        var vehicleRegNo = document.getElementById("vehicleRegNo" + val).value;
                                        //alert("vehicleRegNo" + vehicleRegNo);
                                        if (vehicleRegNo != '') {
                                            var url = '/throttle/checkVehicleRegNoExists.do?vehicleRegNo=' + vehicleRegNo;
                                       //     alert("url----" + url);
                                            if (window.ActiveXObject)
                                            {
                                                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                            }
                                            else if (window.XMLHttpRequest)
                                            {
                                                httpRequest = new XMLHttpRequest();
                                            }
                                            httpRequest.open("POST", url, true);
                                            httpRequest.onreadystatechange = function() {
                                           //     alert("Hi I am Here");
                                                go1(val);
                                            };
                                            httpRequest.send(null);
                                        }
                                    }


                                    function go1(val) {
                                        //alert("val" + val);
                                        // alert("hi");
                                        if (httpRequest.readyState == 4) {
                                            if (httpRequest.status == 200) {
                                                var response = httpRequest.responseText;
                                                var temp = response.split('-');
                                                //     alert("hi");
                                                if (response != "") {
                                                    alert('Vehicle Already Exists');
                                                    document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                    document.getElementById("vehicleRegNo" + val).value = "";
                                                    document.getElementById("vehicleRegNo" + val).focus();
                                                    document.getElementById("vehicleRegNo" + val).select();
                                                    document.vehicleVendorContract.regNoCheck.value = 'exists';
                                                } else
                                                {
                                                    document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                    document.getElementById("Status").innerHTML = "";
                                                }
                                            }
                                        }
                                    }

                    </script>

                    <div id="routedeD"> </div>
                  
             </c:if>
                </div>


                <script>

                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                    $(".pretab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected - 1);
                    });
                </script>

            </div>
            <center>
                <a  href=""><input type="button" class="button" value="Save" onclick="savePage(this.val);" /></a>
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>