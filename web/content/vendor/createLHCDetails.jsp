<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


    function setVehicleLHC() {
        var statusId = "8";
        var vehicleCategory = "";
        var consignmentOrderId = $("#orderId").val();
        var consignmentType = $("#consignmentType").val();
        var vendorTemp = document.getElementById("vendor").value;
        var vehicletypename = document.getElementById("vehicleTypeId").value;

        $.ajax({
            url: "/throttle/getVehicleLHCRegNos.do",
            dataType: "json",
            data: {
                vehicleTypeId: vehicletypename,
                vendorId: vendorTemp,
                statusId: statusId,
                vehicleCategory: vehicleCategory,
                consignmentOrderId: consignmentOrderId,
                consignmentType:consignmentType
            },
            success: function(data) {

                $.each(data, function(i, data) {
                    document.getElementById("additionalCost").value = data.Id;
                    document.getElementById("contractHireId").value = data.Name;
                    document.getElementById("advance").value = data.advance;
                    document.getElementById("aggreedkm").value = data.km;

                });
            }
        });
    }
    
    
     function setVehicleType(val) {
        if (val != '0~0') {
            var ownership = "2";
            $('#vehicleNo').empty();
            $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
            var vendorTemp = document.getElementById("vendor").value;
            var temp = vendorTemp.split("~");

            $.ajax({
                url: "/throttle/getVendorVehicleType.do",
                dataType: "json",
                data: {
                    vendorId: temp[0],
                    ownership: ownership
                },
                success: function(data) {
                    //  alert(data);
                    if (data != '') {
                        $('#vehicleTypeId').empty();
                        $('#vehicleTypeId').append(
                                $('<option></option>').val(0).html('--select--'))

                        $.each(data, function(i, data) {
                            var vehicleTypeId1 = data.Id;
                            //alert(vehicleTypeId1);
                            var temp1 = vehicleTypeId1.split("~");
                            $('#vehicleTypeId').append(
                                    $('<option style="width:90px"></option>').attr("value", temp1[0]).text(data.Name)
                                    )
                        });
                    } else {
                        $('#vehicleTypeId').empty();
                        $('#vehicleTypeId').append(
                                $('<option></option>').val(0).html('--select--'))
                    }
                }
            });
        } else {
            resetAll();
            $('#vehicleTypeId').empty();
            $('#vehicleTypeId').append(
                    $('<option></option>').val(0).html('--select--'))
//                    $('#lhcNo').empty();
//                    $('#lhcNo').append(
//                            $('<option></option>').val(0).html('--select--'))
//                    $('#agreedRate').val(0);
        }
    }
    
    
//    function setVehicleLHC() {
//        var statusId = "8";
//        var vehicleCategory = "";
//        var consignmentOrderId = $("#orderId").val();
//        var vendorTemp = document.getElementById("vendor").value;
//        var vehicletypename = document.getElementById("vehicleTypeId").value;
//
//        $.ajax({
//            url: "/throttle/getVehicleLHCRegNos.do",
//            dataType: "json",
//            data: {
//                vehicleTypeId: vehicletypename,
//                vendorId: vendorTemp,
//                statusId: statusId,
//                vehicleCategory: vehicleCategory,
//                consignmentOrderId: consignmentOrderId
//            },
//            success: function(data) {
//
//                $.each(data, function(i, data) {
//                    document.getElementById("additionalCost").value = data.Id;
//                    document.getElementById("contractHireId").value = data.Name;
//                    document.getElementById("advance").value = data.advance;
//
//                });
//            }
//        });
//
//
//    }


    function submitPage() {
        var errStr = "";
//        if (document.getElementById("vehicleNo").value == "") {
//            errStr = "Please enter vehicleNo.\n";
//            alert(errStr);
//            document.getElementById("vehicleNo").focus();
//        }
        if (errStr == "") {
            document.lhc.action = '/throttle/handleAddLHC.do';
            document.lhc.submit();
            $("#Edit").hide();
        }
    }
</script>
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHC DETAILS" text="LHC"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.LHC" text="LHC"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <form name="lhc"  method="post" enctype="multipart/form-data"  >                    

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="bg" >

                        <%                            
                            String order = request.getParameter("orderId");
                            String orderno = request.getParameter("orderno");
                            String orig = request.getParameter("orig");
                            String dest = request.getParameter("dest");
                        %>

                        <thead>
                            <tr>
                                <th colspan="2" > Lorry Hire Challan  </th>
                            </tr>
                        </thead>

                       

                        <tr style="display:none">
                            <td  height="30">Order No</td>
                            <td  height="30"><%=orderno%></td>
                        </tr>

                        <tr>
                            <td height="30">Vehicle Vendor</td>
                            <td height="30">
                                <select name="vendor" id="vendor" style="width:200px;height:25px;" onChange="setVehicleType(this.value)">
                                    <option value="0~0">-Select Any One-</option>
                                    <c:if test = "${vendorList != null}" >
                                        <c:forEach items="${vendorList}" var="cnl">
                                            <option value="<c:out value="${cnl.vendorId}"/>"><c:out value="${cnl.vendorName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td height="30">Vehicle Type</td>
                            <td height="30">
                                <select name="vehicleTypeId" id="vehicleTypeId" style="width:200px;height:25px;" onchange="setVehicleLHC()">
                                    <option value="0">-Select Any One-</option>
                                    <c:if test = "${VehTypeList != null}" >
                                        <c:forEach items="${VehTypeList}" var="vv">
                                            <option value="<c:out value="${vv.vehicleTypeId}"/>"><c:out value="${vv.vehicleTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td height="30">Agreed Advance</td>
                            <td height="30"><input class="textbox" type="text" value='0' name="advance" id="advance" style="width:200px;height:25px;"/></td>
                        </tr>

                        <tr>
                            <td height="30">Agreed Rate</td>
                            <td height="30"><input class="textbox" type="text" name="additionalCost" id="additionalCost" value="0" style="width:200px;height:25px;"/></td>                        
                        </tr>
                        <tr>
                            <td height="30">Agreed KM</td>
                            <td height="30"><input class="textbox" type="text" name="aggreedkm" id="aggreedkm" value="0" style="width:200px;height:25px;"/></td>                        
                        </tr>
                        <tr>
                            <td height="30">Location</td>
                            <td height="30"><textarea class="textbox" type="text" name="location" id="location" value="0" style="width:200px;height:50px;"></textarea></td>                        
                        </tr>
                        <tr>
                            <td height="30">Remarks</td>
                            <td height="30"><textarea class="textbox" type="text" name="remarks" id="remarks" value="0" style="width:200px;height:50px;"></textarea></td>                        
                        </tr>
                        
                        <input class="textbox" type="hidden" name="orderId" value='<%=order%>' id="orderId" style="width:150px;height:25px;"/>
                        <input type="hidden" name="contractHireId" id="contractHireId" class="form-control"  value=""/>
                        <input class="textbox" type="hidden" value='1' name="lhccount" id="lhccount" style="width:200px;height:25px;"/>
                        <input class="textbox" type="hidden" name="vehicleNo" value='0' id="vehicleNo" style="width:150px;height:25px;"/>
                        <input class="textbox" type="hidden" name="driverMobile" id="driverMobile" style="width:150px;height:25px;"/>
                        <input class='textbox'  type='hidden' name='licenseNo' id='licenseNo' style="width:150px;height:25px;" />
                        <input class="textbox" type="hidden" name="driverName" id="driverName" style="width:150px;height:25px;"/>
                    </table>

                    <table class="table table-info mb30 table-hover" id="fileAddRow" style='display:none'>

                        <thead>
                            <tr>
                                <th  height="30">Sno</th>
                                <th  height="30">Document</th>
                                <th  height="30">Expiry Date</th>
                                <th  height="30">Upload</th>
                            </tr>
                        </thead>
                        <tr>
                            <td  height="30">1</td>
                            <td>Insurance</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='insuranceNo' id='insuranceNo' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="insuranceDate" id="insuranceDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='insurance' id='insurance' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">2</td>
                            <td>Road Tax</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='roadTaxNo' id='roadTaxNo' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="roadTaxDate" id="roadTaxDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='roadTax' id='roadTax' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">3</td>
                            <td>FC</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='fcNumber' id='fcNumber' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="fcDate" id="fcDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='fc' id='fc' style="width:200px;height:25px;" /></td>
                        </tr>
                        <tr>
                            <td  height="30">4</td>
                            <td>Permit</td>
                            <!--<td  height="30"><input class='textbox'  type='text' name='permitNumber' id='permitNumber' style="width:150px;height:25px;" /></td>-->
                            <td  height="30"><input type="text" name="permitDate" id="permitDate" value="" class="datepicker form-control"  style="height:22px;width:150px;color:black;"></td>
                            <td  height="30"><input class='textbox'  type='file' name='permit' id='permit' style="width:200px;height:25px;" /></td>
                        </tr>

                    </table>
                    <br/>
                    <center>
                        <br/>
                        <input type="button" class="btn btn-info" id="Edit" name="Edit" value="Save" style="width:90px;height:30px;font-weight: bold;padding:1px;" onclick="submitPage();"/>

                    </center>

                    <br> 
                    <script>
                        
                        function clearVal() {
                            $("#vendor").val("0~0");
                            $("#vehicleTypeId").val("0");
                            $("#advance").val("0");
                            $("#additionalCost").val("0");
                        }
                    </script>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>