<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


</script>



<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage() {
        document.vehicleVendorContract.action = '/throttle/saveEditDriverVendorContract.do';
        document.vehicleVendorContract.submit();
    }

</script>
<script>

    function pastContractDetails(venId, name) {
        window.open('/throttle/pastContractDetails.do?vendorId=' + venId + "&venName=" + name + "&type=search", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Edit Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>

                <style>
                    .errorClass { 
                        background-color: red;
                    }
                    .noErrorClass { 
                        background-color: greenyellow;
                    }
                </style>

                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table border="0"  class="table table-info mb30 table-hover" style="width:70%">
                        <thead ><tr  height="30">
                                <th  colspan="4" >Edit Vendor Contract Info</th>
                            </tr></thead>
                        <tr height="30">
                            <td >Vendor Name</td>

                            <td >
                                <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="textbox">
                                <c:out value="${vendorName}"/></td>
                            <td >Contract Type</td>
                            <td >
                                <select name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);"  style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <!--<option value="1" >Dedicated</option>-->
                                    <option value="2" >Hired</option>
                                    <!--<option value="3" selected>Both</option>-->
                                </select>
                                <script>
                                    document.getElementById("contractTypeId").value =<c:out value="${contractTypeId}" />;</script>
                            </td>
                        </tr>
                        <tr height="30">
                            <td >Contract From</td>
                            <td >
                                <input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />"  class="datepicker form-control" style="height:22px;width:150px;color:black;">
                                <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}" />" >
                            </td>

                            <td >Contract To</td>
                            <td >

                                <input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black;">
                            </td>
                        </tr>
                        <tr height="30">
                            <td >Payment Type</td>
                            <td >
                                <select name="paymentType" id="paymentType"   style="height:22px;width:150px;" disabled>
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                                <script>
                                    document.getElementById("paymentType").value =<c:out value="${paymentType}" />;</script>
                            </td>
                            <td class="text1" colspan="2">&emsp;
                                <!--<a href="#" onclick="pastContractDetails('<c:out value="${vendorId}"/>', '<c:out value="${vendorName}"/>');">Past Contracts</a>-->                                                            
                            </td>

                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li id="active" data-toggle="tab"><a href="#driverContract"><span>Driver Contract</span></a></li>
                        </ul> 

                        <div id="driverContract">
                            <div id="driverCont">
                                <c:if test="${driverContractList != null}">

                                    <table class="table table-info mb30 table-hover sortable" id="dv" align="center" width="100%">
                                        <thead >
                                            <tr>
                                                <th>S.No</th>
                                                <th>Driver Type</th>                                                
                                                <th>Contract Type</th>
                                                <th>Rate</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${driverContractList}" var="driver">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=classText%>"><%=index++%></td>
                                                    <td class="<%=classText%>"><c:if test="${driver.workType == 1}">Batch</c:if><c:if test="${driver.workType == 2}">Heavy</c:if></td>                                                    
                                                    <td class="<%=classText%>"><c:if test="${driver.workContractType == 12}">12 Hrs</c:if><c:if test="${driver.workContractType == 24}">24 Hrs</c:if></td>                                                    
                                                    <td><input type="text" name="rate" id="rate<%=index%>" value="<c:out value="${driver.rateCost}"/>" class="form-control"  />
                                                        <input type="hidden" name="driverWorkTypeId" id="driverWorkTypeId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${driver.workType}"/>"/>
                                                        <input type="hidden" name="driverWorkContractTypeId" id="driverWorkContractTypeId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${driver.workContractType}"/>"/>
                                                        <input type="hidden" name="contractHireId" id="contractHireId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${driver.contractHireId}"/>"/>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" id="activeStatus<%=index%>" name="activeStatus" style="width:240px;height:40px;">
                                                            <option value="Y">Active</option>
                                                            <option value="N">In-Active</option>
                                                        </select>
                                                        <script>
                                                            document.getElementById("activeStatus"+<%=index%>).value ='<c:out value="${driver.activeInd}"/>';
                                                        </script>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>

                                    <script>
                                        var driverCon = "";
                                        function addRowDriverContract() {
                                            $('#addDriverContract').hide();
                                            var iCnt = <%=index%>;
                                            var iCnt1 = 1;
                                            var rowCnt = <%=index%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            driverCon = $($("#driverCont")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            var routeInfoSize = 0;
                                            
                                            routeInfoSize = $('#driverContractTypeWise' + iCnt + ' tr').size();
                                            
                                            rowCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);
                                            $(driverCon).last().after('<table class="table table-info mb30 table-hover" id="driverContractTypeWise' + iCnt1 + rowCnt + '" >\n\
                                <thead><tr><th>Sno</th><th>Driver Type</th><th>Contract Type</th><th>Rate</th></tr></thead>\n\
                                <tr>\n\
                                <td>' + iCnt1 + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt1 + '" value="' + rowCnt + '"/></td>\n\
                                <td><select type="text"  class="form-control" name="driverWorkTypeId" id="driverWorkTypeId' + iCnt1 + '" style="width:150px;height:40px;"><option value="0">--Select--</option><option value="1">Batch</option><option value="2">Heavy</option></select></td>\n\
                                <td><select  type="text" class="form-control" name="driverWorkContractTypeId" id="driverWorkContractTypeId' + iCnt1 + '" style="height:40px;width:150px;"><option value="0">--Select--</option><option value="12">12 Hrs</option><option value="24">24 Hrs</option></select></td>\n\
                                <td><input  type="text" class="form-control" name="rate" id="rate' + iCnt1 + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:40px;width:150px;"/>\n\
                                    <input type="hidden" name="contractHireId" id="contractHireId' + iCnt1 + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:25px;width:150px;"/>\n\
                                    <input type="hidden" name="activeStatus" id="activeStatus' + iCnt1 + '" value="Y" style="height:25px;width:150px;"/></td>\n\
                                </tr></table><br>\n\
                                <table  id="routebutton' + iCnt1 + rowCnt + '" ><tr>\n\
                                <td><input class="btn btn-success"  type="button" name="addDriverCon" id="addDriverCon' + iCnt1 + rowCnt + '" value="Add" onclick="addRowDriverContractE(' + iCnt1 + rowCnt + ',' + rowCnt + ')" />\n\
                                <input class="btn btn-success"  type="button" name="removeDriverContract" id="removeDriverContract' + iCnt1 + rowCnt + '" value="Remove"  onclick="deleteRowDriverContract(' + iCnt1 + rowCnt + ')" /></td>\n\
                                </tr></table></td></tr></table><br>');
                                            //                                    callOriginAjaxFullTruck(rowCnt);
                                            //                                    callDestinationAjaxFullTruck(rowCnt);
                                        }
                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }


                                        function addRowDriverContractE(val, v1) {
                                            var loadCnt = val;
                                            var loadCnt1 = v1;
                                            var routeInfoSize = $('#driverContractTypeWise' + loadCnt + ' tr').size();
                                            var addRouteDetails = "addDriverCon" + loadCnt;
                                            var routeInfoDetails = "driverContractTypeWise" + loadCnt;
                                            $('#driverContractTypeWise' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + routeInfoSize + '" value="' + v1 + '"/></td><td><select type="text"  class="form-control" name="driverWorkTypeId" id="driverWorkTypeId' + routeInfoSize + '" style="width:150px;height:40px;"><option value="0">--Select--</option><option value="1">Batch</option><option value="2">Heavy</option></select></td>\n\
                                        <td><select  type="text" class="form-control" name="driverWorkContractTypeId" id="driverWorkContractTypeId' + routeInfoSize + '" style="height:40px;width:150px;"><option value="0">--Select--</option><option value="12">12 Hrs</option><option value="24">24 Hrs</option></select></td>\n\
                                        <td><input type="text" class="form-control" name="rate" id="rate' + routeInfoSize + '" value="0" style="height:40px;width:150px;"/>\n\
                                            <input type="hidden" class="form-control" name="activeStatus" id="activeStatus' + routeInfoSize + '" value="Y" style="height:25px;width:150px;"/>\n\
                                            <input type="hidden" name="contractHireId" id="contractHireId' + routeInfoSize + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:25px;width:150px;"/></td>\n\</tr>');
                                            loadCnt++;
                                        }
                                        function deleteRowDriverContract(val) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addDriverCon" + loadCnt;
                                            var routeInfoDetails = "driverContractTypeWise" + loadCnt;
                                            if ($('#driverContractTypeWise' + loadCnt + ' tr').size() > 2) {
                                                $('#driverContractTypeWise' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                            </script>


                                        </div>


                            </c:if>
                            <br>
                            <a><input type="button" class="btn btn-success"  value="Add" id="addDriverContract" name="addDriverContract" onclick="addRowDriverContract()" style="width:100px;height:40px;"/></a>
                            <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:40px;"/></a>
                        </div>

                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Submit" onclick="submitPage();" style="width:150px;height:30px;font-weight: bold;padding:1px;"/>

                        </center>
                        <br>
                        <br>
                        <br>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>   

                    <script>
                        $('.btnNext').click(function() {
                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });
                        $('.btnPrevious').click(function() {
                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
                    </script>
                    <%--<%@ include file="/content/common/NewDesign/commonParameters.jsp" %>--%>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>