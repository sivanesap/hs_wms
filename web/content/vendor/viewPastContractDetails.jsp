<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true
        });
    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    
    function searchPage(){
        
        document.vehicleVendorContract.action = '/throttle/pastContractDetails.do?type=view';
        document.vehicleVendorContract.submit();
    }

    function pastContractDetails(venId) {
        window.open('/throttle/pastContractDetails.do?vendorId=' + venId + "", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Past Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">View Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>

                <form name="vehicleVendorContract" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="6" height="30" >Past Contracts</th>
                            </tr>
                        </thead>
                        <%
                            String vendor = request.getParameter("vendorId");                        
                            String vendorname = request.getParameter("venName");                        
                        %>
                        <tr>
                            <td >Vendor Name</td>
                            <td>                                
                                <%=vendorname%>
                                <input type="hidden" class="form-control" style="width:250px;height:40px"  name="vendorId"   id="vendorId"  value="<c:out value="${vendorId}"/>"/>
                                <input type="hidden" class="form-control" style="width:250px;height:40px"  name="venName"   id="venName"  value="<c:out value="${venName}"/>"/>
                                
                            </td>

                            <td >Vehicle Type</td>
                            <td>
                                <select name="vehicleTypeId" id="vehicleTypeId" style="height:22px;width:160px;" >
                                    <option value="">--Select--</option>
                                    <c:if test="${TypeList != null}">
                                        <c:forEach items="${TypeList}" var="vehType">
                                            <option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/>
                                            </option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        <input type="hidden" name="vehType" id="vehType" value="<c:out value="${vehicleTypeId}"/>"/>
                        <script>                                                     
                            document.getElementById("vehicleTypeId").value = document.getElementById("vehType").value;
                        </script>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${startDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${endDate}"/>"></td>

                        <tr>             
                            <td></td>
                            <td></td>
                            <td  colspan="2"><input  class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                        </tr>
                    </table>

                    <br/>
                    <br/>
                    <div id="tabs" >
                        <div id="fullTruck" class="tab-pane active">
                            <div id="routeFullTruck">
                                <c:if test="${pastContractDetails != null}">
                                    <table class="table table-info mb30 table-hover" id="table">
                                        <thead>
                                        <tr style="height: 40px;" id="tableDesingTH">
                                            <th align="center">S.No</th>
                                            <th align="center">From Date</th>
                                            <th align="center">To Date</th>
                                            <th align="center">Vehicle Type</th>
                                            <th align="center">Route</th>
                                            <th align="center">Load Type</th>
                                            <th align="center">Rate</th>
                                            <th align="center">Advance Type</th>
                                            <th align="center">Advance Value</th>
                                            <th align="center">Init. Advance</th>
                                            <th align="center">Remain. Amount</th>
                                        </tr></thead>
                                        <tbody>
                                            <% int sno = 0; int index = 1;%>
                                            <c:forEach items="${pastContractDetails}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr height="30">
                                                    <td class="<%=classText%>"><%=index++%></td>
                                                    <td class="<%=classText%>"><c:out value="${route.startDate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.endDate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.vehicleTypeName}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/>-<c:out value="${route.point1Name}"/>-<c:out value="${route.destinationNameFullTruck}"/></td>
                                                    <td class="<%=classText%>"   >
                                                        <c:if test="${route.loadTypeId =='1'}">
                                                            Empty Trip
                                                        </c:if>
                                                        <c:if test="${route.loadTypeId =='2'}">
                                                            Load Trip
                                                        </c:if>
                                                    </td>
                                                    <td class="<%=classText%>"><c:out value="${route.additionalCost}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.advanceTripMode}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.modeTripRate}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.initialTripAdvance}"/></td>
                                                    <td class="<%=classText%>"><c:out value="${route.endTripAdvance}"/></td>                                                
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                             <input type="hidden" name="count" id="count" value="<%=sno%>" />
                                </c:if>
                            </div>
                            <br>
                            <br>

                            <br>
                        </div>

                    </div>

        </div>

        <br>
        <br>
        <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </body>
    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
