<%-- 
    Document   : manageVendorType
    Created on : 23 Jul, 2016, 5:48:29 PM
    Author     : pavithra
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {

            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                dateFormat: 'dd-mm-yy',
                changeMonth: true, changeYear: true
            });

        });
    </script>
     <script type="text/javascript">
         
    function setValues(sno, vendorTypeId,vendorTypeValue,remarks, activeInd) {
        var count = parseInt(document.getElementById("count").value);
//        document.getElementById('inActive').style.display = 'block';
        for (i = 0; i < count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("vendorTypeId").value = vendorTypeId;
        document.getElementById("vendorTypeValue").value = vendorTypeValue;
        document.getElementById("remarks").value = remarks;
        document.getElementById("activeInd").value = activeInd;
    }
     function submitPage()
    {
//        alert("save")
        
        if (document.getElementById("vendorTypeValue").value == "") {
            alert("Please enter vendorType");
            document.getElementById("vendorTypeValue").focus();
            return;
        }
        if (document.getElementById("activeInd").value == "") {
            alert("Please enter Status");
            document.getElementById("activeInd").focus();
            return;
        }
           document.vendor.action = "/throttle/saveVendorType.do";
            document.vendor.submit();
       
    }
    </script>

    
     <style>
    #index td {
   color:white;
}
</style>
<%
     DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
     Date date = new Date();
     String fDate = "";
     String tDate = "";
     if (request.getAttribute("fromDate") != null) {
         fDate = (String) request.getAttribute("fromDate");
     } else {
         fDate = dateFormat.format(date);
     }
     if (request.getAttribute("toDate") != null) {
         tDate = (String) request.getAttribute("toDate");
     } else {
         tDate = dateFormat.format(date);
     }
%>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="vendors.label.VendorType"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="vendors.label.Vendor"  text="default text"/></a></li>
            <li class="active"><spring:message code="vendors.label.VendorType"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
<body>
    <form name="vendor" action=""  method="post"  class="form-horizontal form-bordered">
            <%@ include file="/content/common/message.jsp" %>
                 <table class="table table-bordered">
                 <tr height="30" id="index">
                        <td colspan="4" style="background-color:#5BC0DE;"><b><spring:message code="vendors.label.AddVendorType"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
               
                <tr height="30">
                    <td ><font color="red">*</font><spring:message code="vendors.label.VendorType"  text="default text"/></td>
                    <td ><input type="hidden" name="vendorTypeId" id="vendorTypeId" value="">
                        <input name="vendorTypeValue" type="text" class="form-control" id="vendorTypeValue" value="" style="width:260px;height:40px;">
                    </td>
                    <td  height="30"><font color="red">*</font><spring:message code="vendors.label.Status"  text="default text"/></td>
                    <td  height="30">
                        <select class="form-control" name="activeInd" id="activeInd" style="width:260px;height:40px;">
                            <option value="">---<spring:message code="vendors.label.Select"  text="default text"/>---</option>
                           <option value='Y'><spring:message code="vendors.label.Active"  text="default text"/></option>
                                <option value='N' id="inActive"><spring:message code="vendors.label.In-Active"  text="default text"/></option>
                        </select>
                      
                    </td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font><spring:message code="vendors.label.Description"  text="default text"/></td>
                    <td  height="30">
                        <input type="text" name="remarks" id="remarks" style="width:260px;height:40px;" class="form-control">
                    </td>
                    <td  height="30">&nbsp;</td>
                    <td  height="30">&nbsp;</td>
                </tr>

            </table>
            <br>
            <center>
                <input type="button"  value="<spring:message code="vendors.label.SAVE"  text="default text"/>" onclick="submitPage();" class="btn btn-info" style="width:100px;height:35px;" />
                &emsp;<input type="reset" class="btn btn-info" style="width:100px;height:35px;" value="<spring:message code="vendors.label.Clear"  text="default text"/>">
            </center>
            <br>

        <c:if test = "${VendorTypeList != null}" >
              <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                         <th><spring:message code="vendors.label.SNo"  text="default text"/></th>
                         <th><spring:message code="vendors.label.VendorType"  text="default text"/></th>
                         <th><spring:message code="vendors.label.Description"  text="default text"/></th>
                         <th><spring:message code="vendors.label.Status"  text="default text"/></th>
                         <th><spring:message code="vendors.label.Edit"  text="default text"/></th>
                        
                </thead>
                <tbody>
                    <% int sno = 0;
                            int index = 1;%>
                    <c:forEach items="${VendorTypeList}" var="list"> 

                        <tr  width="208" height="40" > 
                            <td  height="20"><%=index%></td>
                            <td  height="20"><c:out value="${list.vendorTypeValue}"/></td>
                            <td  height="20"><c:out value="${list.remarks}"/></td>
                            <td  height="20">
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    Inactive
                                </c:if>
                               
                            </td>
                            <td  height="20"><input type="checkbox" class="form-control" id="edit<%=sno%>" onclick="setValues(<%=sno%>, '<c:out value="${list.vendorTypeId}" />', '<c:out value="${list.vendorTypeValue}" />','<c:out value="${list.remarks}"/>','<c:out value="${list.activeInd}"/>');" style="width:35px;height:15px;"/></td>
                           
                            </td>
                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </tbody>
            </table>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />
        </c:if>
        <br>
        <br>
         <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="vendors.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="vendors.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="vendors.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

</body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

