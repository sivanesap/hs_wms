<%-- 
    Document   : index
    Created on : Apr 22, 2010, 11:43:36 AM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
    function checkLogin()
    {
        if(document.login.user.value=="")
        {
            alert("Username Should not be empty");
        }
        else if(document.login.pwd.value=="")
        {
            alert("Password Should not be empty");
        }
        else
        {
            document.login.action='check.jsp';
            document.login.submit();
        }
    }
    function setNull()  
    {
        document.login.user.value="";
        document.login.pwd.value="";
    }

</script>
</head>
<body onload="setNull(); document.login.user.focus();">
    <form name="login" method="get">
        <br>
        <br>
        <br>
        <table align="center" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    Username
                </td>
                <td>
                    <input type="text" name="user" id="lblUser" size="10"/>
                </td>
            </tr>


            <tr>
                <td>
                    Password
                </td>
                <td>
                    <input type="password" name="pwd" id="lblPwd" size="10"/>
                </td>
            </tr>

        </table>
        <center><input type="button" name="Login" value="Login" onClick="checkLogin()"/></center>
    </form>
</body>
</html>
