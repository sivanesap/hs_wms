<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <script language="javascript" src="/throttle/js/validate.js"></script>  
    <%@ page import="ets.domain.contract.business.ContractTO" %>  
    <%@ page import="java.util.*" %>      
    <%@ page import="java.http.*" %>      
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <%@ page import="ets.domain.service.business.ServiceTO" %>  
    <%@ page import="ets.domain.operation.business.OperationTO" %>  
	<%! int count=0;%>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#regno').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#regno').val(tmp[1]);
                        getVehicleDetails(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>

<SCRIPT>
    

          
//function getVehicleNos(){
//    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
//    //getVehicleDetails(document.getElementById("regno"));
//} 


var httpReq;
function callAjax(){
    
   
          if( trim(document.workOrder.regno.value) == '' ){
            alert("Please Enter Vehicle Registration Number");
            document.workOrder.regno.value = '';
            document.workOrder.regno.select();
            document.workOrder.regno.focus();
            document.workOrder.kmReading.value='';
            return;
       }    
        var vehicleNo=document.workOrder.regno.value;          
        var url='/throttle/checkActualKm.do?vehicleNo='+vehicleNo;
        if ( vehicleNo != '') {
            if (window.ActiveXObject){
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest){
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() { processAjax();};
            httpReq.send(null);
        }
    
}   
function processAjax()
{
    if (httpReq.readyState == 4)
        {
            if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    var kminfo=temp.split('~');
                    document.workOrder.actualKm.value=kminfo[0];
                    document.workOrder.totalKm.value=kminfo[1];
                    document.workOrder.kmReading.focus();
                    
                }
                else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
}

          function submitPage()
          {
               if(textValidation(document.workOrder.regno,'Vehicle No'));
               else if(numberValidation(document.workOrder.kmReading,'kmReading'));
               else  if(textValidation(document.workOrder.reason,'Reason'));
               else{
                    document.workOrder.action = '/throttle/saveResetVehicleKM.do';
                    document.workOrder.submit();
               }
          }


</SCRIPT>
<div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Reset Vehicle KM"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="Reset Vehicle KM"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            
            <div class="panel panel-default">
                <div class="panel-body">


<body onload="document.workOrder.regno.focus();getVehicleNos();" >
<form name="workOrder" method="post">




<!-- pointer table -->

<!-- message table -->

<%@ include file="/content/common/message.jsp"%> 
     <table class="table table-info mb30 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2"  >Reset Vehicle KM </th>
		</tr>
		    </thead>
    </table>
  <table class="table table-info mb30 table-hover" id="bg" >
<tr>    
    <td  ><font color="red">*</font>Vehicle No</td>
<td  >
<input name="regno" id="regno" 
 maxlength="20" type="text"  class="form-control" style="width:250px;height:40px"  value=""></td>

<td  > <font color="red">*</font>Reset Reason </td>
<td  >
<textarea name="reason" class="form-control" style="width:250px;height:40px" onkeyup="maxlength(this.form.remark,300)" onChange="callAjax();"  ></textarea>
</td>
</tr>
<tr>
<td  > Total KM Run</td>
<td  ><input name="totalKm"  maxlength="20" type="text" readonly class="form-control" style="width:250px;height:40px" value=""></td>

<td  > Current KM </td>
<td  ><input name="actualKm"  maxlength="20" type="text" readonly class="form-control" style="width:250px;height:40px" value=""></td>
</tr>

<tr>
<td  ><font color="red">*</font>Reset KM </td>
<td  ><input name="kmReading"  maxlength="20" type="text"  class="form-control" style="width:250px;height:40px" value=""></td>
</tr>

<input name="cust" type="hidden" value="Existing Customer" >

</table>

<br>

<center>

<input type="button" value="Reset" name="generate" id="generate"  class="btn btn-success"  onclick="submitPage();">
<input type="reset" value="Clear"  class="btn btn-success" >

</center>
</form>
<br>
    
</body>

</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>