<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>


              <%
                        int index = 1;

            %>


            <c:if test="${vehicleList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${vehicleList != null }" >
                
                    <table class="table table-info mb30 table-hover sortable" id="table"  border="1">
                    <thead>
                          <tr height="80">
                            <th class="contentsub"><h3 align="center">S.no</h3></th>
                            <th class="contentsub"><h3 align="center">Trailer Number</h3></th>
                            <th class="contentsub"><h3 align="center">Trailer Type</h3></th>
                           <th class="contentsub"><h3 align="center">Make</h3></th>
                           <th class="contentsub"><h3 align="center">Model</h3></th>
                            <th class="contentsub"><h3 align="center">Color</h3></th>
                           <th class="contentsub"><h3 align="center">Ownership</h3></th>
                        </tr>
                    </thead>
                    
                    <%
                                String style = "text1";%>
                    <c:forEach items="${vehicleList}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td  height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td  height="30" style="padding-left:30px; "> <c:out value="${veh.trailerNo}" /> </td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.typeName}" /></td>

                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.mfrName}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.modelName}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.vehicleColor}" /></td>
                            <td  height="30" style="padding-left:30px; "><c:out value="${veh.ownership}" /></td>
                                <%--
                                <c:if test="${veh.activeInd != '0'}" >
                                    closeJobCard
                                </c:if>
                                <c:if test="${veh.activeInd == '0'}" >
                                    <a href='/throttle/createJobcard.do?vehicleId=<c:out value="${veh.vehicleId}" />'  >makeInActive </a>
                                </c:if> 
                            </td>
                            <td class="<%=style%>"  align="left"><a href="/throttle/tyresRotation.do?vehicleId=<c:out value='${veh.vehicleId}' />&axleTypeId=<c:out value='${veh.axleId}' />">Rotation</a></td>
                                --%>

                        </tr>
                    </c:forEach>
                        
                </table>
            </c:if>
            <br/>
            <br/>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>