<%-- 
    Document   : viewVehicleTax
    Created on : Sep 21, 2012, 3:07:28 PM
    Author     : entitle
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
    <head>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>

        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
         </head>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        </script>

 <script type="text/javascript">
        function addPage(){
//            alert("value");
            
            
            document.viewVehicleDetails.action = '/throttle/handleVehicleRoadTax.do';
                document.viewVehicleDetails.submit();
            
            
    }

    </script>   
    
     <script type="text/javascript">
        function submitPage(value){
           // alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                document.viewVehicleDetails.submit();

            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleRoadTax.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleTaxList.do'
                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>
    
    
    

    
       
    
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.VehicleTax" text="Vehicle Tax"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicles" text="Vehicles"/></a></li>
                    <li class=""><spring:message code="hrms.label.VehicleTax" text="Vehicle Tax"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >


            <%@ include file="/content/common/message.jsp" %>


                  <!--<table class="table table-info mb30 table-hover"  >-->
                      <table class="table table-info mb30 table-hover" id="bg" >
                 
                
                                  <!--<table class="table table-info mb30 table-hover" id="report" >-->
                                      
                                   <thead> 
                                       <tr>
                                           <th colspan="4"> 
                                               View Tax
                                           </th>
                                       </tr>
                                       
                                       </thead>
                                       <tr>
                                        <td>Vehicle Number</td><td><input type="text" id="regno" name="regNo" value=""  class="form-control" style="width:250px;height:40px" autocomplete="off"></td>
<td><input type="button" class="btn btn-success" style="width:100px;height:40px" onclick="submitPage(this.name);" name="search" value="Search">
                               
    <input type="button" class="btn btn-success" style="width:100px;height:40px" onclick="addPage();" name="add" value="Add">
                                    </tr>
                                        
                                </table>
                   

<%
            int index = 1;

%>


            <c:if test="${RoadTaxList == null }" >
                <br>
                <center><font color="red" size="2"> No records found </font></center>
            </c:if>
            <c:if test="${RoadTaxList != null }" >
                 <table class="table table-info mb30 table-hover" id="table" >
                     <thead>

                    <tr >
                        <th  ><div >Sno</div></th>
                        <th  ><div >Vehicle Number</div></th>
                        <th ><div >Tax Receipt No</div></th>
                        <th  ><div >Tax Receipt Date</div></th>
                        <th ><div >Tax Paid Location</div></th>
                        <th ><div >Tax Amount</div></th>
                        <th  ><div >Next Tax Date</div></th>
                        <th ><div >&nbsp;</div></th>
                    </tr>
                    </thead>
                    <%
            String style = "text1";%>
                    <c:forEach items="${RoadTaxList}" var="Rtl" >
                        <%
            if ((index % 2) == 0) {
                style = "text1";
            } else {
                style = "text2";
            }%>
                        <tr>
                            <td   style="padding-left:30px; "> <%= index %> </td>
                            <td   style="padding-left:30px; "> <c:out value="${Rtl.regNo}" /></td>
                            <td   style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptno}" /></td>
                            <td   style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptdate}" /></td>
                            <td   style="padding-left:30px; "><c:out value="${Rtl.roadtaxpaidlocation}" /></td>
                            <td   style="padding-left:30px; "><c:out value="${Rtl.roadtaxamount}" /></td>
                            <td   style="padding-left:30px; "><c:out value="${Rtl.nextRoadTaxDate}" /></td>
                            <td   style="padding-left:30px; "><a href='/throttle/vehicleTaxDetail.do?vehicleId=<c:out value="${Rtl.vehicleId}" />&id=<c:out value="${Rtl.id}" />'>alter </a> </td>
                        </tr>
                        <% index++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>
<script language="javascript" type="text/javascript">
             setFilterGrid("table");
         </script>
         <div id="controls">
             <div id="perpage">
                 <select onchange="sorter.size(this.value)">
                     <option value="5"  selected="selected">5</option>
                     <option value="10">10</option>
                     <option value="20">20</option>
                     <option value="50">50</option>
                     <option value="100">100</option>
                 </select>
                 <span>Entries Per Page</span>
             </div>
             <div id="navigation">
                 <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                 <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                 <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                 <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
             </div>
             <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
         </div>
         <script type="text/javascript">
             var sorter = new TINY.table.sorter("sorter");
             sorter.head = "head";
             sorter.asc = "asc";
             sorter.even = "evenrow";
             sorter.odd = "oddrow";
             sorter.evensel = "evenselected";
             sorter.oddsel = "oddselected";
             sorter.paginate = true;
             sorter.currentid = "currentpage";
             sorter.limitid = "pagelimit";
             sorter.init("table", 1);
        </script>

        </form>
    </body>
<!--    <script type="text/javascript">
        function submitPage(value){
           // alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleTaxList.do';
                document.viewVehicleDetails.submit();

            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleRoadTax.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleTaxList.do'
                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }

    </script>-->
    
<!--    <script>
        fuction addPage(){
            alert("t");
document.viewVehicleDetails.action = '/throttle/handleVehicleRoadTax.do';
                document.viewVehicleDetails.submit();
        }
    </script>-->
    
    
    </div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
