
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>

<!--//style-->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>-->
<script type="text/javascript">



    function onKeyPressBlockCharacters(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z]+$/;

        return !reg.test(keychar);

    }
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


</script>


<script>

    $(document).ready(function() {
        $("#axleTyre").validationEngine();
    });

    function validateTypeFor(){
        var typeFor = document.getElementById("typeFor").value;

        if(typeFor == 1){
            //document.getElementById("fAxleConfig1").style.display = 'block';
            //document.getElementById("fAxleConfig2").style.display = 'block';
            document.getElementById("axleLeft1").value = '1';
            document.getElementById("axleRight1").value = '1';
            document.getElementById("axleViewLeft0").value = '1';
            document.getElementById("axleViewRight0").value = '1';
        }else if(typeFor == 2){
            //document.getElementById("fAxleConfig1").style.display = 'none';
            //document.getElementById("fAxleConfig2").style.display = 'none';
            document.getElementById("axleLeft1").value = '0';
            document.getElementById("axleRight1").value = '0';
            document.getElementById("axleViewLeft0").value = '0';
            document.getElementById("axleViewRight0").value = '0';
        }if(typeFor == 0){
            //document.getElementById("fAxleConfig1").style.display = 'none';
            //document.getElementById("fAxleConfig2").style.display = 'none';
            document.getElementById("axleLeft1").value = '0';
            document.getElementById("axleRight1").value = '0';
            document.getElementById("axleViewLeft0").value = '0';
            document.getElementById("axleViewRight0").value = '0';
        }
    }
</script>

 <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="vehicle.lable.axleMaster"  text="axleMaster"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
          <li class="active"><spring:message code="vehicle.lable.axleMaster"  text="axleMaster"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onload="axleTypeMaster.reset();">
        <form name="axleTypeMaster" id="axleTyre" method="POST" action="/throttle/saveVehicleAxleMaster.do" >
            <%@ include file="/content/common/message.jsp" %>
            <br>
            
            <table class="table table-info mb30 table-hover" >
                <thead>

                <tr>
                    <th colspan="4" ><spring:message code="trucks.label.Axle Master"  text="Add Axle Type"/></th>
                </tr>
                </thead>

                <tr>
                    <td>&nbsp;&nbsp;<font color="red">*</font><spring:message code="trucks.label.ThisisFor"  text="default text"/></td>
                    <td>
                        <select class="form-control" style="width:260px;height:40px;" name="typeFor" id="typeFor" onchange="validateTypeFor();" >
                            <option value="0">-<spring:message code="trucks.label.select"  text="default text"/>-</option>
                            <option value="1"><spring:message code="trucks.label.Truck"  text="default text"/></option>
                            <option value="2"><spring:message code="trucks.label.Trailer"  text="default text"/></option>
                        </select>
                    </td>
                    
                    
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;<font color="red">*</font><spring:message code="trucks.label.AxleTypeName"  text="default text"/></td>
                    <td><input type="text" name="axleTypeName" id="axleTypeName"  class="form-control" style="width:260px;height:40px;"   maxlength="50"></td>


                </tr>
                <tr>

                     <td>&nbsp;&nbsp;<font color="red">*</font><spring:message code="trucks.label.AxleCount"  text="default text"/></td>
                    <td><input type="text" name="axleCount" id="axleCount"  class="form-control" style="width:260px;height:40px;"   maxlength="50"  onchange="positionAddRow(),deleteRow();"  ></td>
                </tr>



                    <table class="table table-info mb30 table-hover" id="positionTable" >
                    <thead>
                    <tr >                        
                        <th  align="center" ><spring:message code="trucks.label.PositionName"  text="default text"/></th>
                        <th  ><spring:message code="trucks.label.Left"  text="default text"/> </th>
                        <th  ><spring:message code="trucks.label.Right"  text="default text"/></th>
                    </tr>
                    </thead>
                    <tr id="fAxleConfig1" >
                        
                        <td><label  name="vehicleFronts" id="vehicleFronts1"><spring:message code="trucks.label.Front"  text="default text"/></label><input type="hidden" name="vehicleFront" id="vehicleFront1" value="Front"/></td>
                        <td><input type="text" class="form-control" style="width:240px;height:40px;" name="axleLeft" id="axleLeft1" value="0" readonly /></td>
                        <td><input type="text" class="form-control" style="width:240px;height:40px;"  name="axleRight" id="axleRight1" value="0" readonly /></td>
                    </tr>
                    <tr>
                        
                        <td><label  name="vehicleFronts" id="vehicleFronts2"><spring:message code="trucks.label.Rear"  text="default text"/>-1</label><input type="hidden" name="vehicleFront" id="vehicleFront2" value="Rear-1"/></td>
                        <td><input type="text" class="form-control" style="width:240px;height:40px;" name="axleLeft" id="axleLeft2" value="2" readonly/></td>
                        <td><input type="text" class="form-control" style="width:240px;height:40px;" name="axleRight" id="axleRight2" value="2" readonly/></td>
<!--                        <td><input type="button" name="AddRow" id="AddRow" value="AddRow" onclick="positionAddRow();"/></td>-->
                    </tr>
                    <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="2"/>
                </table>
                <script type="text/javascript">
                    function positionAddRow() {
                        var typeFor = document.getElementById("typeFor").value;
                        if(typeFor == 0 ){
                            alert("please select the Type");
                            document.getElementById("typeFor").focus();
                            return;
                        }
                        var rowCount = "";
                        var style = "";
                         var axleCount = document.getElementById('axleCount').value;
                         axleCount = parseInt(axleCount)-2;
                         var typeFor = document.getElementById("typeFor").value;
                        if(typeFor == 2){
                            axleCount++;
                        }
                        for (var i = 0; i < parseInt(axleCount); i++) {

                        rowCount = document.getElementById('selectedRowCount').value;
                        rowCount++;
                        var tab = document.getElementById("positionTable");
                        var newrow = tab.insertRow(rowCount);
                        newrow.id = 'rowId' + rowCount;

//                        var cell = newrow.insertCell(0);
//                        var cell1 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
//                        cell.setAttribute("className", style);
//                        cell.innerHTML = cell1;

                        cell = newrow.insertCell(0);
                        var cell2 = "<td  height='30'><input type='hidden' name='vehicleFront' id='vehicleFront" + rowCount + "' value='Rear-" + (rowCount - 1) + "'  class='form-control' style='width:240px;height:40px'   /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>Rear-" + (rowCount - 1) + "</label></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell2;

                        cell = newrow.insertCell(1);
                        var cell3 = "<td height='30' ><input type='text' value = '2' readonly name='axleLeft' id='axleLeft" + rowCount + "' maxlength='13'   class='form-control' style='width:240px;height:40px' onclick='removePositionLeftAxle(" + rowCount + ");' onchange='setViewPositionLeftAxle(" + rowCount + ");' /></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell3;

                        cell = newrow.insertCell(2);
                        var cell4 = "<td  height='30'><input type='text' value = '2' readonly name='axleRight' id='axleRight" + rowCount + "' maxlength='13'  class='form-control' style='width:240px;height:40px' onclick='removePositionRightAxle(" + rowCount + ");' onchange='setViewPositionRightAxle(" + rowCount + ");'  /></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell4;

//                        cell = newrow.insertCell(4);
//                        var cell5 = "<td height='30' ><input type='button' name='addRow' id='addRow' maxlength='13'   size='20'  value='Addrow' onclick='positionAddRow()'/></td>";
//                        cell.setAttribute("className", style);
//                        cell.innerHTML = cell5;

                        document.getElementById('selectedRowCount').value = rowCount;
                        viewPositionAddRow(rowCount);
                        }

                    }

                    function deleteRow(){

                   
        rowCount--;
        var oRow = src.parentNode.parentNode;
//        alert(oRow);
        var dRow = document.getElementById('positionTable');
        //once the row reference is obtained, delete it passing in its rowIndex
        dRow.deleteRow(oRow.rowIndex);
        document.getElementById("selectedRowCount").value--;
        document.getElementById('axleCount').value = "";

       }

                    function setViewPositionLeftAxle(positionCount) {
                        var axleLeftCount = document.getElementById('axleLeft' + positionCount).value;
                        var columnName = "axleViewLeft" + (positionCount - 1);
                        for (var i = 0; i < parseInt(axleLeftCount); i++) {
                            var element = document.createElement("input");
                            element.setAttribute("type", "text");
                            element.setAttribute("value", "1");
                            element.setAttribute("name", "axleViewLeft" + (positionCount - 1));
                            element.setAttribute("id", columnName + ""+ i);
                            element.setAttribute("style", "width:80px");
                            var foo = document.getElementById("axleViewLeft" + (positionCount - 1));
                            foo.appendChild(element);
                        }


                    }
                    function removePositionLeftAxle(positionCount) {
                        var axleLeftCount = document.getElementById('axleLeft' + positionCount).value;
                        var columnName = "axleViewLeft" + (positionCount - 1);
                        for (var i = 0; i < parseInt(axleLeftCount); i++) {
                            $("#"+columnName + ""+ i).remove();
                        }
                        document.getElementById('axleLeft' + positionCount).value = "";

                    }
                    function setViewPositionRightAxle(positionCount) {
                        var axleRightCount = document.getElementById('axleRight' + positionCount).value;
                        var columnName = "axleViewRight" + (positionCount - 1);
                        for (var i = 0; i < parseInt(axleRightCount); i++) {
                            var element = document.createElement("input");
                            element.setAttribute("type", "text");
                            element.setAttribute("value", "1");
                            element.setAttribute("name", "axleViewRight" + (positionCount - 1));
                            element.setAttribute("id", columnName + ""+ i);
                            element.setAttribute("style", "width:80px");
//                            element.setAttribute("class", "textbox");
                            var foo = document.getElementById("axleViewRight" + (positionCount - 1));
                            foo.appendChild(element);
                        }
                    }
                    function removePositionRightAxle(positionCount) {
                        var axleRightCount = document.getElementById('axleRight'+positionCount).value;
                        var columnName = "axleViewRight" + (positionCount - 1);
                        for (var i = 0; i < parseInt(axleRightCount); i++) {
                            $("#"+columnName + ""+ i).remove();
                        }
                        document.getElementById('axleRight'+positionCount).value = "";
                    }

                </script>


                </tr>
            </table>
            <br>
            <center>
                <input type="submit" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" name="Submit" onClick="submitPage()">
            </center>
            

            

            <div id="tyreConfig" style="display:none;">

            <table class="table table-info mb30 table-hover" id="positionViewTable" >
            
                <thead>
                    <tr>
                        <th colspan="3" ><spring:message code="trucks.label.AxleTyreConfiguration"  text="default text"/></th>
                    </tr>
                </thead>
                <tr id="fAxleConfig2">
                    <td>
                        <label  name="vehicleViewFront" id="vehicleViewFront0" ><spring:message code="trucks.label.Front"  text="default text"/></label>
                    </td>
                    <td >
                        <input type="text"  class="form-control" style="width:260px;height:40px;"  name="axleViewLeft" id="axleViewLeft0"  value="0" readonly/>
                    </td>
                    <td >
                        <input type="text"  class="form-control" style="width:260px;height:40px;"  name="axleViewRight" id="axleViewRight0"  value="0" readonly/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label   name="vehicleViewFront" id="vehicleViewFront1" ><spring:message code="trucks.label.Rear"  text="default text"/>-1</label>
                    </td>
                    <td>
                        <input type="text" class="form-control" style="width:240px;height:40px;"  name="axleViewLeft" id="axleViewLeft11"  value="1" readonly/>
                        <input type="text" class="form-control" style="width:240px;height:40px;" name="axleViewLeft" id="axleViewLeft12"  value="1" readonly/>
                    </td>
                    <td>
                        <input type="text" class="form-control" style="width:240px;height:40px;" name="axleViewRight" id="axleViewRight11"  value="1" readonly/>
                        <input type="text" class="form-control" style="width:240px;height:40px;" name="axleViewRight" id="axleViewRight12" value="1" readonly/>
                    </td>
                </tr>
                <input type="hidden" name="positionRowCount" id="positionRowCount" value="1"/>
            </table>
            </div>

                <script type="text/javascript">
                    function viewPositionAddRow(positionCount) {
                        var rowCount = "";
                        var style = "";
                        rowCount = document.getElementById('positionRowCount').value;
                        rowCount++;


                        var tab = document.getElementById("positionViewTable");
                        var newrow = tab.insertRow(rowCount);
                        newrow.id = 'rowId' + rowCount;

                        cell = newrow.insertCell(0);
                        var cell1 = "<td  height='30'><label name='vehicleViewFront' id='vehicleViewFront" + rowCount + "'>Rear-" + (rowCount) + "</label></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell1;

                        cell = newrow.insertCell(1);
                        var cell2 = "<td  height='30'  ><label  id='axleViewLeft" + rowCount + "'></label></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell2;

                        cell = newrow.insertCell(2);
                        var cell3 = "<td   height='30' ><label  id='axleViewRight" + rowCount + "'></label></td>";
                        cell.setAttribute("className", style);
                        cell.innerHTML = cell3;

                        document.getElementById('positionRowCount').value = rowCount;
                    }
                    
                    
                </script>
                <style>
                input
                {
                    -moz-border-radius: 1px;
                    border-radius: 1px;
                    border:solid 1px black;
                    padding:5px;
                }
            </style>

                <br>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>