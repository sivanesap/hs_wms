<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
        
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle AMC Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleAMC.amcCompanyName.value){
                    errMsg = errMsg+"AMC Company Name is not filled\n";
                }
                if(document.VehicleAMC.amcAmount.value){
                    errMsg = errMsg+"AMC Amount is not filled\n";
                }
                if(document.VehicleAMC.amcDuration.value){
                    errMsg = errMsg+"AMC Duration is not filled\n";
                }
                if(document.VehicleAMC.amcChequeDate.value){
                    errMsg = errMsg+"AMC Cheque Date is not filled\n";
                }
                if(document.VehicleAMC.amcChequeNo.value){
                    errMsg = errMsg+"AMC Cheque No is not filled\n";
                }
                if(document.VehicleAMC.amcFromDate.value){
                    errMsg = errMsg+"AMC From Date is not filled\n";
                }
                if(document.VehicleAMC.amcToDate.value){
                    errMsg = errMsg+"AMC To Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            /*var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleAMC.vehicleId.value = vehicleValues[0]
                            document.VehicleAMC.chassisNo.value = vehicleValues[2]
                            document.VehicleAMC.engineNo.value = vehicleValues[2]
                            document.VehicleAMC.vehicleMake.value = vehicleValues[4]
                            document.VehicleAMC.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }*/
        </script>

    </head>
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Alter Vehicle AMC " text="Alter Vehicle AMC"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Alter Vehicle AMC" text="Alter Vehicle AMC"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
                

    <body onLoad="">
        <form name="VehicleAMC" action="/throttle/updateVehicleAMC.do">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <!--<h2 align="center">Vehicle AMC Details</h2>-->
            <table class="table table-info mb30 table-hover"  >
                <thead>

                <tr>
                    <th  colspan="4">Vehicle Details</th>
                </tr>
                </thead>
                <c:if test="${vehicleDetail != null}">
                    <c:forEach items="${vehicleDetail}" var="VDet" >
                        <tr>
                            <td >Vehicle No</td>
                            <td ><input type="text" name="regNo" id="regNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                            <td >Make</td>
                            <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td >Model</td>
                            <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.modelName}" />' ></td>
                            <td >Usage</td>
                            <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" style="width:240px;height:40px"  ></td>
                        </tr>
                        <tr>
                            <td >Engine No</td>
                            <td ><input type="text" name="engineNo" id="engineNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.engineNo}" />' ></td>
                            <td >Chassis No</td>
                            <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" style="width:240px;height:40px" value='<c:out value="${VDet.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                        <thead>
                <tr>
                    <th colspan="4">Insurance Details</th>
                </tr>
                        </thead>
                <c:if test="${vehicleAMC != null}">
                    <c:forEach items="${vehicleAMC}" var="vAmc" >
                        <tr>
                            <td >AMC Company</td>
                            <td ><input type="text" name="amcCompanyName" id="amcCompanyName" class="form-control" style="width:240px;height:40px" value='<c:out value="${vAmc.amcCompanyName}" />' ><input type="hidden" name="amcId" id="amcId" value='<c:out value="${vAmc.amcId}" />' ></td>
                            <td >AMC Amount</td>
                            <td ><input type="text" name="amcAmount" id="amcAmount" class="form-control" style="width:240px;height:40px" value='<c:out value="${vAmc.amcAmount}" />' ></td>
                        </tr>
                        <tr>
                            <td >AMC Duration</td>
                            <td ><input type="text" name="amcDuration" id="amcDuration" class="form-control" style="width:240px;height:40px"  value='<c:out value="${vAmc.amcDuration}" />' ></td>
                            <td >AMC Cheque Date</td>
                            <td ><input type="text" name="amcChequeDate" id="amcChequeDate" class="datepicker form-control" style="width:240px;height:40px"  value='<c:out value="${vAmc.amcChequeDate}" />' ></td>
                        </tr>
                        <tr>
                            <td >AMC Cheque No</td>
                            <td ><input type="text" name="amcChequeNo" id="amcChequeNo" class="form-control"  style="width:240px;height:40px" value='<c:out value="${vAmc.amcChequeNo}" />' ></td>
                            <td >AMC From Date Date</td>
                            <td ><input type="text" name="amcFromDate" id="amcFromDate" class="datepicker form-control" style="width:240px;height:40px"  value='<c:out value="${vAmc.amcFromDate}" />' ></td>
                        </tr>
                        <tr>
                            <td >AMC To Date</td>
                            <td ><input type="text" name="amcToDate" id="amcToDate" class="form-control" style="width:240px;height:40px"  value='<c:out value="${vAmc.amcToDate}" />' ></td>
                            <td >Remarks</td>
                            <td ><input type="text" name="remarks" id="remarks" class="form-control" style="width:240px;height:40px"  value='<c:out value="${vAmc.remarks}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
               
            </table>
            <br>
            <center>
                <br>
                <input type="submit" class="btn btn-success" value=" Update " />
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>

