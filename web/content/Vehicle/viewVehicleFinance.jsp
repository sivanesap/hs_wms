<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>

        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
        </script>

    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Finance Update" text="Vehicle Finance Update"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicles" text="Vehicles"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Vehicle Finance Update" text="Vehicle Finance Update"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>


            <%@ include file="/content/common/message.jsp" %>


            
                                <table class="table table-info mb30 table-hover" id="bg" >
                                    <thead>
                                        <tr>
                                            <th colspan="4">
                                                Vehicle Finance
                                            </th>
                                        </tr>
                                    </thead>
                                    <tr >
                                        <td>Vehicle Number</td><td><input type="text" id="regno" name="regNo" value="" class="form-control" style="width:240px;height:40px" autocomplete="off"></td>
                                    
                                    
                                        <td><input type="button" class="btn btn-success" style="width:100px;height:40px" onclick="submitPage(this.name);" name="search" value="Search"></td>
                                        <td><input type="button" class="btn btn-success" style="width:100px;height:40px" onclick="addVehicle(this.name);" name="search" value="Add"></td>
                                    </tr>
                                </table>
                            
                    
            <br>
            
<%
            int index = 1;
           
%>


            <c:if test="${VehicleInsList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${VehicleInsList != null }" >
            <table class="table table-info mb30 table-hover"  >
                <thead>
                    
              
                    <tr>

                        <th  >SNo</th>
                        <th  >Vehicle Number</th>
                        <th  >Bank Name</th>
                        <th  >Finance Amount</th>
                        <th  >EMI Months</th>
                        <th  >EMI Amount</th>
                        <th  >EMI Start Date</th>
                        <th  >EMI End Date</th>
                        <th  >Pay Mode</th>
                        <th  >Action</th>
                    </tr>
                      </thead>
                    <%
            String style = "text1";%>
                    <c:forEach items="${VehicleInsList}" var="veh" >
                        <%
            if ((index % 2) == 0) {
                style = "text1";
            } else {
                style = "text2";
            }%>
                        <tr>
                            <td > <%= index %> </td>
                            <td > <c:out value="${veh.regNo}" /></td>
                            <td ><c:out value="${veh.bankerName}" /></td>
                            <td ><c:out value="${veh.financeAmount}" /></td>
                            <td ><c:out value="${veh.emiMonths}" /></td>
                            <td ><c:out value="${veh.emiAmount}" /></td>
                            <td ><c:out value="${veh.emiStartDate}" /></td>
                            <td ><c:out value="${veh.emiEndDate}" /></td>
                            <td ><c:out value="${veh.payMode}" /></td>
                            <td ><a href='/throttle/vehicleFinanceDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&financeId=<c:out value="${veh.financeId}" />'>Alter </a> </td>
                        </tr>
                        <% index++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>

            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
         function addVehicle(){
             document.viewVehicleDetails.action = '/throttle/handleVehicleFinance.do';
                document.viewVehicleDetails.submit();
        }
        </script>
        
    <script type="text/javascript">
        function submitPage(value){
            //alert(value);
            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicleDetails.GoTo.value;
                    document.viewVehicleDetails.pageNo.value=temp;
                    document.viewVehicleDetails.button.value=value;
                    document.viewVehicleDetails.action = '/throttle/vehicleFinanceList.do';
                    document.viewVehicleDetails.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicleDetails.last.value;
                    document.viewVehicleDetails.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicleDetails.action = '/throttle/vehicleFinanceList.do';
                document.viewVehicleDetails.submit();
              
            }else if(value == 'add'){
                document.viewVehicleDetails.action = '/throttle/handleVehicleFinance.do';
                document.viewVehicleDetails.submit();
            }else{
                document.viewVehicleDetails.action='/throttle/vehicleFinanceList.do'
                document.viewVehicleDetails.submit();
            }
        }


        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){

            if( regNo!='null'){
                document.viewVehicleDetails.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicleDetails.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicleDetails.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicleDetails.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicleDetails.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }
        
       

    </script>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>

