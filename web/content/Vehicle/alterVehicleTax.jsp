
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<head>
    <title>Vehicle Insurance Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            //alert('hai');
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function() {
            //	alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });

        });
    </script>

    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }



        function validate() {
            var errMsg = "";
            if (document.VehicleInsurance.regNo.value) {
                errMsg = errMsg + "Vehicle Registration No is not filled\n";
            }
            if (document.VehicleInsurance.insuranceCompanyName.value) {
                errMsg = errMsg + "Insurance Company is not filled\n";
            }
            if (document.VehicleInsurance.premiumNo.value) {
                errMsg = errMsg + "Premium No is not filled\n";
            }
            if (document.VehicleInsurance.premiumPaidAmount.value) {
                errMsg = errMsg + "Premium paid Amount is not filled\n";
            }
            if (document.VehicleInsurance.premiumPaidDate.value) {
                errMsg = errMsg + "Premium paid Date is not filled\n";
            }
            if (document.VehicleInsurance.vehicleValue.value) {
                errMsg = errMsg + "Vehicle Value is not filled\n";
            }
            if (document.VehicleInsurance.premiumExpiryDate.value) {
                errMsg = errMsg + "Premium expiry Date is not filled\n";
            }
            if (errMsg != "") {
                alert(errMsg);
                return false;
            } else {
                return true;
            }
        }


        var httpRequest;
        function getVehicleDetails(regNo)
        {

            if (regNo != "") {
                var url = "/throttle/getVehicleDetailsInsurance.do?regNo1=" + regNo;
                url = url + "&sino=" + Math.random();
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);
            }
        }


        function processRequest()
        {
            if (httpRequest.readyState == 4) {

                if (httpRequest.status == 200) {
                    if (httpRequest.responseText.valueOf() != "") {
                        var detail = httpRequest.responseText.valueOf();
                        var vehicleValues = detail.split("~");
                        document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                        document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                        document.VehicleInsurance.engineNo.value = vehicleValues[2]
                        document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                        document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                    }
                }
                else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    </script>

</head>

  <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Road Tax Details" text="Vehicle Road Tax Details"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Vehicle Road Tax Details" text="Vehicle Road Tax Details"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">

<body onLoad="setImages(1, 0, 0, 0, 0, 0);">
    <form name="VehicleInsurance" action="/throttle/alterVehicleTax.do">
        <%--<%@ include file="/content/common/path.jsp" %>--%>
        <%@ include file="/content/common/message.jsp" %>
        <h2 align="center">Vehicle Road Tax Details</h2>
        <table width="800" align="center" class="table table-info mb30 table-hover" cellpadding="0" cellspacing="0">
<thead>
            <tr>
                <th  colspan="4">Vehicle Details</th>
            </tr>
            </thead>
            <c:if test="${vehicleRoadTax != null}">
                <c:forEach items="${vehicleRoadTax}" var="VDet" >
                    <tr>
                        <td >Vehicle No</td>
                        <td ><input type="text" name="regNo" id="regNo" class="form-control" value='<c:out value="${VDet.regno}" />' style="width:240px;height:40px"  ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                        <td >Make</td>
                        <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />' style="width:240px;height:40px" ></td>
                    </tr>
                    <tr>
                        <td >Model</td>
                        <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' style="width:240px;height:40px"  ></td>
                        <td >Usage</td>
                        <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" style="width:240px;height:40px"  ></td>
                    </tr>
                    <tr>
                        <td >Engine No</td>
                        <td ><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' style="width:240px;height:40px"  ></td>
                        <td >Chassis No</td>
                        <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' style="width:240px;height:40px"  ></td>
                    </tr>
                </c:forEach>
            </c:if>
                    <thead>
            <tr>
                <th  colspan="4">Road Tax Details</th>
            </tr>
            </thead>
            <c:if test="${vehicleTax != null}">
                <c:forEach items="${vehicleTax}" var="VeT" >
                    <tr>
                        <td >Receipt No</td>
                        <td ><input type="text" name="roadTaxReceiptNo" id="roadTaxReceiptNo" class="form-control" value='<c:out value="${VeT.roadtaxreceiptno}" />' style="width:240px;height:40px"  ><input type="hidden" name="id" id="id" value='<c:out value="${VeT.id}" />' /> </td>
                        <td >Receipt Date</td>
                        <td ><input type="text" name="roadTaxReceiptDate" id="roadTaxReceiptDate" class="datepicker form-control" value='<c:out value="${VeT.roadtaxreceiptdate}" />' style="width:240px;height:40px"  ></td>
                    </tr>
                    <tr>
                        <td >Location</td>
                        <td ><input type="text" name="roadTaxPaidLocation" id="roadTaxPaidLocation" class="form-control" value='<c:out value="${VeT.roadtaxpaidlocation}" />' style="width:240px;height:40px"  ></td>
                        <td >Amount</td>
                        <td ><input type="text" name="roadTaxPaidAmount" id="roadTaxPaidAmount" class="form-control" value='<c:out value="${VeT.roadtaxamount}" />' style="width:240px;height:40px"  ></td>
                    </tr>
                    <tr>
                        <td >Period</td>
                        <td ><select name="roadTaxPeriod" id="roadTaxPeriod" class="form-control" style="width:240px;height:40px" >
                                <option>Quarterly</option>
                                <option>Half Yearly</option>
                                <option>Yearly</option>
                            </select>
                        </td>
                        <td >Road Tax From Date</td>
                        <td ><input type="text" name="roadTaxFromDate" id="roadTaxFromDate" class="datepicker form-control" value='<c:out value="${VeT.roadTaxFromDate}" />' style="width:240px;height:40px" ></td>
                    </tr>
                    <tr>
                        <td >Road Tax To Date</td>
                        <td ><input type="text" name="nextRoadTaxDate" id="nextRoadTaxDate" class="datepicker form-control" value='<c:out value="${VeT.nextRoadTaxDate}" />' style="width:240px;height:40px"  ></td>
                        <td >Remarks</td>
                        <td ><input type="text" name="roadTaxRemarks" id="roadTaxRemarks" class="form-control" value='<c:out value="${VeT.remarks}" />' style="width:240px;height:40px"  ></td>                    
                    </tr>
                </c:forEach>
            </c:if>
            <tr>
                <td align="center"  colspan="4"></td>
            </tr>
        </table>
        <br>
        <center>
            <!--Upload Copy Of Bills&emsp;<input type="file" />
            <br>-->
            <br>
            <input type="submit" class="btn btn-success" value=" Update " />
        </center>
</body>
<%--<%@ include file="/content/common/NewDesign/commonParameters.jsp" %>--%>
</form>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>  

