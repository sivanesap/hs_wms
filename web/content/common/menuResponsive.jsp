<html>
<head>
<title> Responsive Brattle Foods </title>
<meta name=�viewport� content=�width=device-width, initial-scale=1, maximum-scale=1?>
<style>
body {
margin: 0 auto;
background: #fff;
}

p {
margin-bottom: 1em;
background: #E08B14;
font: 3.2em sans-serif;
text-transform: uppercase;
letter-spacing: .2em;
text-align: center;
color: #fff;
}
/*layout styles*/
@media all and (min-width: 780px) {
/*layout*/
body {
/*width: 85%;*/
max-width: 1280px;
min-width: 960px;
}
}

@media all and (min-width: 481px) and (max-width: 780px) {
/*layout*/
body {
/*width: 90%;*/
max-width: 780px;
min-width: 485px;
font-size: 80%;
}

}
@media all and (max-width: 480px) {
/*layout*/
body {
/*width: 90%;*/
max-width: 480px;
}

}
</style>


</head>
<body>
<p>Menu</p>
<center>
    <table>
        <tr>
            <td><a href="viewTripSheets.do?respStatus=Y&startRequest=Y&endRequest=N">start trip</</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><a href="viewTripSheets.do?respStatus=Y&startRequest=N&endRequest=Y">end trip</</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><a href="emptyTripPlanning.do?respStatus=Y">empty trip</</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><a href="/throttle/logout.do?respStatus=Y">logout</</td>
        </tr>
    </table>
    <br>
</center>
</body>
</html>