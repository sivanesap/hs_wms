<%-- 
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Customer" text="Customer"/></a></li>
            <li class=""><spring:message code="hrms.label.Customer Contract Master" text="Customer Contract Master"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="customerContract"  method="post" >

                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <c:if test="${contractDetails != null}">
                        <c:forEach items="${contractDetails}" var="customer">

                            <table  align="center"  class="table table-info mb30 table-hover" id="bg">
                                <thead>
                                    <tr>
                                        <th colspan="4" height="30" >Customer Contract Master</th>
                                    </tr>
                                </thead>

                                <tr>
                                    <td >Customer Name</td>
                                    <td >&nbsp;<c:out value="${customer.customerName}"/></td>
                                    <td >Customer Code</td>
                                    <td >&nbsp;<c:out value="${customer.customerCode}"/></td>
                                </tr>
                                <tr>
                                    <td >Contract From</td>
                                    <td >&nbsp;<c:out value="${customer.contractFrom}"/></td>
                                    <td >Contract To</td>
                                    <td >&nbsp;<c:out value="${customer.contractTo}"/></td>
                                </tr>
                                <tr>
                                    <td >Contract No</td>
                                    <td >&nbsp;<c:out value="${customer.contractNo}"/></td>
                                    <td >Billing Type</td>
                                    <td >&nbsp;<c:out value="${customer.billingTypeName}"/></td>
                                </tr>
                            </table>
                            <br>
                        </c:forEach></c:if>

                        <!--                        <div id="tabs">
                                                    <ul class="">
                                                        <li><a href="#fullTruck"><span>Full Truck</span></a></li>
                                                        <li><a href="#weightBreak"><span>Weight Break </span></a></li>
                                                    </ul>-->



                        <div id="tabs">
                            <ul class="nav nav-tabs">
                                <!--<li class="active" data-toggle="tab"><a href="#fullTruck"><span>Full Truck</span></a></li>-->
                                <li  class="active" data-toggle="tab"><a href="#weightBreak"><span>Weight Break </span></a></li>
                            </ul>

                            <div id="fullTruck" style="display:none">



                                <div id="routeFullTruck">
                                <c:if test="${contractRouteDetails != null}">
                                    <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                        <thead>
                                            <tr>
                                                <th align="center"><h3>S.No</h3></th>
                                        <th align="center"><h3>Vehicle Type</h3></th>
                                        <th align="center"><h3>Origin</h3></th>
                                        <th align="center"><h3>Destination</h3></th>
                                        <th align="center"><h3>Contract Route Code</h3></th>
                                        <th align="center"><h3>Rate With Reefer</h3></th>
                                        <th align="center"><h3>Rate Without Reefer</h3></th>
                                        <th align="center"><h3>Effective Date</h3></th>
                                        <th align="center"><h3>Travel Kms</h3></th>
                                        <th align="center"><h3>Travel Hours</h3></th>
                                        <th align="center"><h3>Travel Minutes</h3></th>
                                        <th align="center"><h3>Active Status</h3></th>
                                        </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${contractRouteDetails}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=classText%>"  ><%=index++%></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.vehicleTypeName}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.firstPointName}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.finalPointName}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.routeContractCode}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.rateWithReefer}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.rateWithoutReefer}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.effectiveDate}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.travelKm}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.travelHours}"/></td>
                                                    <td class="<%=classText%>"   ><c:out value="${route.travelMinutes}"/></td>
                                                    <td class="<%=classText%>"   >
                                                        <c:if test="${route.activeInd == 'Y'}">
                                                            Active
                                                        </c:if>
                                                        <c:if test="${route.activeInd == 'N'}">
                                                            In Active
                                                        </c:if>
                                                    </td>
                                                </tr>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                        </div>

                        <div id="weightBreak">



                            <div id="routeWeightBreak">

                                <c:if test="${contractWeightDetails != null}">
                                    <table align="center"  id="table" class="table table-info mb30 table-hover sortable"  >
                                        <thead>
                                            <tr>
                                                <th align="center">S.No</th>
                                                <th align="center">Origin</th>
                                                <th align="center">Destination</th>
                                                <th align="center">Contract Route Code</th>
                                                <th align="center">Rate With Reefer</th>
                                                <th align="center">Rate Without Reefer</th>
                                                <th align="center">WeightFromKg</th>
                                                <th align="center">WeightToKg</th>
                                                <th align="center">Effective Date</th>
                                                <th align="center">Travel Kms</th>
                                                <th align="center">Travel Hours</th>
                                                <th align="center">Travel Minutes</th>
                                                <th align="center">Active Status/h3></th>
                                            </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${contractWeightDetails}" var="weight">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td   ><%=index++%></td>
                                                    <td    ><c:out value="${weight.firstPointName}"/></td>
                                                    <td    ><c:out value="${weight.finalPointName}"/></td>
                                                    <td    ><c:out value="${weight.routeContractCode}"/></td>
                                                    <td    ><c:out value="${weight.rateWithReefer}"/></td>
                                                    <td    ><c:out value="${weight.rateWithoutReefer}"/></td>
                                                    <td    ><c:out value="${weight.weightFromKg}"/></td>
                                                    <td    ><c:out value="${weight.weightToKg}"/></td>
                                                    <td    ><c:out value="${weight.effectiveDate}"/></td>
                                                    <td    ><c:out value="${weight.travelKm}"/></td>
                                                    <td    ><c:out value="${weight.travelHours}"/></td>
                                                    <td    ><c:out value="${weight.travelMinutes}"/></td>
                                                    <td    >
                                                        <c:if test="${weight.activeInd == 'Y'}">
                                                            Active
                                                        </c:if>
                                                        <c:if test="${weight.activeInd == 'N'}">
                                                            In Active
                                                        </c:if>
                                                    </td>
                                                </tr>

                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>

                            </div>


                            <!--<a  class="pretab" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>-->      
                        </div>     
                        <script>
                            function saveCustomerContract() {
                                document.customerContract.action = "/throttle/saveCustomerContractDetails.do";
                                document.customerContract.submit();
                            }

                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                            $(".pretab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected - 1);
                            });

                        </script>


                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
