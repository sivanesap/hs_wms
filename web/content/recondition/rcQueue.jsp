

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ page import="ets.domain.mrs.business.MrsTO" %> 
    <%@ page import="ets.domain.recondition.business.ReconditionTO" %> 
    <title>RC Queue</title>
</head>
<body>


<script>
    
    function submitPage(){  
        if(document.rcQueue.vendorId.value=='0'){
            alert('Please select vendor' );
            return;
        }    
        if(checkRcExists() == 'fail'){                        
            return;
        }    
        if(validate() == 'fail'){                        
            return;
        }           
        document.rcQueue.action="/throttle/generateRcWo.do"     
        document.rcQueue.submit();                
    }    
    function submitPage1(){  
        if(validate() == 'fail'){                        
            return;
        }    
        //document.getElementById("buttonStyle").style.visibility='hidden';
        document.rcQueue.action="/throttle/handleUpdateRcQueueRcs.do"     
        document.rcQueue.submit();                
    }  
    
    function generateRc(val)
    {
        var items = document.getElementsByName("itemIds");
        var queue = document.getElementsByName("rcQueueIds");
        var categoryId= document.getElementsByName("categoryId");
        
        if(categoryId[val].value!='1011'){
            document.rcQueue.action="/throttle/handleGenerateRcItem.do?itemId="+items[val].value+'&rcQueueId='+queue[val].value ;
            document.rcQueue.submit();    
        }else{
            
        window.open('/throttle/content/recondition/rtTyre.jsp?rcQueueId='+queue[val].value+'&itemId='+items[val].value, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');    
    }
}    


function checkRcExists()
{
    var rcNumber = document.getElementsByName("rcNumbers");
    var rcNo = document.getElementsByName("rcNo");
    var index = document.getElementsByName("selectedIndex");
    var counter = 0;
    
    for(var i=0;i<rcNumber.length;i++){
        if(index[i].checked == true){
            if(rcNumber[i].value == '0' && (rcNo[i].value == '0' || rcNo[i].value == '')){
                counter++;
                break;
            }    
        }    
    }
    if(parseInt(counter)>0){
        alert("For Selected Item no Rc code is Generated");
        return "fail";
    }else{
    return "pass";
}
}    

function validateVendor()
{
    
    var selectedIndex = document.getElementsByName("selectedIndex");    

    for(var i=0;i<selectedIndex.length;i++){
        if(selectedIndex[i].checked == true ){
            if(vendor[i].value == '0'){
                alert("Please select vendor for item");
                vendor[i].focus();
                return 'fail';
            }
        }
    }
return 'pass';

}    


function validate()
{
    var items = document.getElementsByName("itemIds");    
    var selectedIndex = document.getElementsByName("selectedIndex");
    var rcCode = document.getElementsByName("rcNo");
    var siz = items.length;
    var cntr = 0;
    for(var i=0;i<selectedIndex.length;i++){
        if(selectedIndex[i].checked == true ){
            cntr++;
        }                        
    }
    if(cntr == 0){
        alert("Please select any one item");
        return 'fail';
    }

    cntr = 0;
    for(var i=0;i<selectedIndex.length;i++){                    
        for(var j=0;j<selectedIndex.length;j++){
            if( (selectedIndex[i].checked == true) && (selectedIndex[j].checked == true) ){
                if( (items[i].value == items[j].value) && (rcCode[i].value==rcCode[j].value  ) && (i != j)   ){
                    cntr++;  
                }
            }                        
        }                    
        if( parseInt(cntr) > 0){
            alert("Rc Codes Should not be Repeated");                        
            return "fail";
            break;
        }  
    }                
    return "pass";                
}

var chec=0;
function scrapPage(){
    var selectedIndex = document.getElementsByName("selectedIndex");
    for(var i=0;i<selectedIndex.length;i++){
        if(selectedIndex[i].checked == true ){
            chec++;
        }                        
    }
    if(chec == 0){
        alert("Please select any one item");
        return 'fail';
    }
    document.rcQueue.action='/throttle/scrapRc.do';
    document.rcQueue.submit();
}

    function submitWindow()
    {
       document.rcQueue.action = '/throttle/rcQueue.do';            
       document.rcQueue.submit();                
    }   


function checkItems(val)
{
    var items = document.getElementsByName("selectedIndex");
    items[val].checked=true;
}
</script>

<form name="rcQueue" method="post">                    
<%@ include file="/content/common/path.jsp" %>            
<!-- pointer table -->

<!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    

<% int index = 0;%>                

<table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="bg" class="border">              
<%index = 0;
            String classText = "";
            int counter = 0;
            int counter1 = 0;
            int oddEven = 0;
%>
<c:set var="mont" value="(Months)" />
<c:if test = "${rcQueue != null}" >
<tr>
    <td colspan="9" align="center" class="text2" height="30"><strong>RC Queue </strong></td>
</tr>  
<tr>
    <td class="contenthead" height="30"><div class="contenthead">Company Name</div></td>
    <td class="contenthead" height="30"><div class="contenthead">Mfr Code</div></td>
    <td class="contenthead" height="30"><div class="contenthead">Papl Code</div></td>                       
    <td class="contenthead" height="30"><div class="contenthead">Item Name</div></td>
    <td class="contenthead" height="30"><div class="contenthead">RC Code</div></td>
    <td class="contenthead" height="30"><div class="contenthead">Created Date</div></td>
    <td class="contenthead" height="30"><div class="contenthead">Elapsed Days</div></td>                            
    <td class="contenthead" height="30"><div class="contenthead">Select</div></td>                                                                                                  
</tr>                
<c:forEach items="${rcQueue}" var="rc"> 


<%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
%>
<tr>
<td class="<%=classText %>" height="30"><c:out value="${rc.companyName}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${rc.mfrCode}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${rc.paplCode}"/></td>                       
<td class="<%=classText %>" height="30"><c:out value="${rc.itemName}"/></td>

<td class="<%=classText %>" height="30">  
<c:choose>
<c:when test="${rc.rcNumber=='0'}" > 
    
    <!-- For Tyre Items  -->       
    <c:if test="${(rc.categoryId!='1011') && (rc.vehicleId != '0' )  }">  
        <% counter1 = 0;%>
        <c:if test = "${previousRcs != null}" >
            <c:forEach items="${previousRcs}" var="item"> 
                <c:if test = "${(item.vehicleId==rc.vehicleId ) && (item.itemId==rc.itemId) && (item.jobCardId!=rc.jobCardId)  }"> 
                    <% if (counter1 == 0) {%>    
                    <select name="rcNo" class="textbox" >
                        <% }%>
                        <% counter++;
            counter1++;%>    
                        <option value="<c:out value="${item.rcItemId}"/>" > <c:out value="${item.rcItemId}"/>-<c:out value="${item.rcDuration}"/>-<c:out value="${mont}"/> </option>
                        
                        <% if (counter1 == 0) {  %>  
                    </select>   
                    <% }%>
                    
                </c:if>
            </c:forEach>      
        </c:if>
    </c:if> 
    

    <!-- srini 28/07/12  start -->

    

    <!-- srini 28/07/12  end -->
     <!-- For general Items  -->
    <c:if test="${ (rc.categoryId=='111011')  && (rc.vehicleId != '0' ) }">
        <% counter1 = 0;%>
        <c:if test = "${previousRcs != null}" >
            <c:forEach items="${previousRcs}" var="item">
                <c:if test = "${(item.vehicleId==rc.vehicleId ) && (item.itemId==rc.itemId)  && (item.jobCardId!=rc.jobCardId) }">
                    <% if (counter1 == 0) {%>
                    <select name="rcNo" class="textbox" >
                        <% }%>
                        <% counter++;
            counter1++;%>
                        <option value="<c:out value="${item.rcItemId}"/>" > <c:out value="${item.tyreNo}"/>-<c:out value="${item.rcDuration}"/><c:out value="${mont}"/> </option>

                        <% if (counter1 == 0) {%>
                    </select>
                    <% }%>

                </c:if>
            </c:forEach>
        </c:if>
    </c:if>

    
    <% if (counter == 0) {%>
    <input type="hidden" readonly name="rcNo" value="<c:out value="${rc.rcNumber}"/>" >    
           <a href="#" onclick="generateRc('<%= index %>')" >GenerateRcId</a>
    <% } else {
                counter = 0;
            }%>
    
    
    
    
    
</c:when>
<c:otherwise>
<input type="text" size="5" readonly name="rcNo" value="<c:out value="${rc.rcNumber}"/>" >    
       </c:otherwise>
</c:choose>                             


</td>

<td class="<%=classText %>" height="30"><c:out value="${rc.createdDate}"/></td>
<td class="<%=classText %>" height="30"><c:out value="${rc.elapsedDays}"/> </td>                                                                                                                                                                   
<td class="<%=classText %>" height="30"> <input type="checkbox" name="selectedIndex" value="<%= index %>" > </td>                                                        
<input type="hidden" name="itemIds" value=<c:out value="${rc.itemId}"/> > 
<input type="hidden" name="rcNumbers" value=<c:out value="${rc.rcNumber}"/> >                             
<input type="hidden" name="rcQueueIds" value=<c:out value="${rc.rcQueueId}"/> >                             
<input type="hidden" name="categoryId" value=<c:out value="${rc.categoryId}"/> >                             

</tr>
<%
            index++;
%>

</c:forEach>                 
</table>     

<div align="center" class="text2">

Vendor Name
&nbsp;&nbsp;&nbsp;&nbsp;
    <select name="vendorId" class="textbox" onChange='checkItems(<%= index %>)'>
        <option value='0' > --Select-- </option>
        <c:if test = "${vendorList != null}" >       
            <c:forEach items="${vendorList}" var="vend">                                              
                    <option value=<c:out value="${vend.vendorId}" /> > <c:out value="${vend.vendorName}" />  </option>                                            
            </c:forEach>      
        </c:if> 
    </select>                                                                                                         

</div>    


<br>
<div class="text2" align="center"> Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="textbox" name="remarks" ></textarea> </div>
<center>  
    <br>
    <br>
    <% if(session.getAttribute("companyId").equals("1022")  ) { %>
    <input type="button" class="button" name="wo" value="Generate WO" onClick="submitPage();" >
    <% } %>
    <input type="button" id="buttonStyle" class="button" style="visibility:visible" name="update" value="generate Rc Code" onClick="submitPage1();" >
    <% if(session.getAttribute("companyId").equals("1022")  ) { %>
    <input type="button" class="button"  name="scrap" value="Scrap" onClick="scrapPage();" >
    <% } %>    
</center>           
</c:if>             
</form>
</body>
</html>
