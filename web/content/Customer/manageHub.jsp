<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="ets.domain.customer.business.CustomerTO" %>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>
</head>
<script language="javascript">
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerName.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#custName').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    $(document).ready(function () {
        $('#customerCode').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerCode.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#customerCode').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#customerCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    //    function checkValue(value,id){
    //                if(value == '' && id=='custName'){
    //                   $('#customerCode').attr('readonly', true);
    //                   document.getElementById('customerId').value = '';
    //                }
    //                if(value == '' && id=='customerCode'){
    //                   $('#custName').attr('readonly', true);
    //                   document.getElementById('customerCode').value = '';
    //                }
    //            }
    //

    function submitPage(value) {
        if (value == "add") {
            document.manufacturer.action = '/throttle/handleViewAddHub.do';
            document.manufacturer.submit();
        } else if (value == 'search') {
            document.manufacturer.action = '/throttle/handleViewCustomer.do';
            document.manufacturer.submit();
        }
    }

    function viewConsignorDetails(custId,custName) {
        window.open('/throttle/handleConsignorUpdate.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
                                                       
     function viewConsigneeDetails(custId,custName) {
        window.open('/throttle/handleConsigneeUpdate.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
     function uploadCustContractDetails(custId,custName) {
        window.open('/throttle/handleUploadCustContract.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageCustomer" text="Manage Hub"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.ManageCustomer" text="Manage Customer"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="manufacturer" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" id="report" style="width:80%" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >View Hub Details</th>
                            </tr>
 </thead>

                            <tr>
                                <th height="30"><font color="red">*</font>Hub Code</th>
                                <th><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" style="width:250px;height:40px" onclick="checkValue(this.value, this.id)"></th>
                                <th height="30"><font color="red">*</font>Hub Name</th>
                                <th>
                                    <c:if test = "${CustomerLists != null}" >
                                        <c:forEach items="${CustomerLists}" var="customer">
                                    <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customer.custId}"/>" class="form-control" style="width:250px;height:40px">
                                    </c:forEach>
                                    </c:if>
                                    <input type="text" name="custName" id="custName" value="<c:out value="${custName}"/>" class="form-control" style="width:250px;height:40px" onclick="checkValue(this.value, this.id)"></th>

                            </tr>
                       
                        <tr>
                            <td colspan="4" align="center">
                                <input type="button"   value="Search" class="btn btn-success"  name="search" onClick="submitPage('search')">&nbsp;&nbsp;
                                <input type="button"   value="Add" class="btn btn-success"  name="Add" onClick="submitPage('add')">
                            </td>
                        </tr>
                    </table>


<c:if test = "${CustomerLists != null}" >
    <table class="table table-info mb30 table-hover" id="table" style="width:90%" >
        <thead>
            <tr height="40">
                <th>S.No</th>
                <th>Hub Name</th>
                <th>Phone NO</th>
                <th>Email Id</th>
                <th>Active</th>
                <th>View/Edit</th>
                </tr>
        </thead>
        <tbody>
            
            <% int index = 0, sno = 1;%>
            <c:forEach items="${CustomerLists}" var="customer">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr height="30">
                    <td align="left" ><%=sno%></td>
                     <td align="left" ><c:out value="${customer.hubName}"/> </td>
                    <td align="left" ><c:out value="${customer.phoneNo}"/> </td>
                     <td align="left" ><c:out value="${customer.email}"/> </td>
                                <td align="left" ><c:out value="${customer.activeInd}"/> </td>
                    <td align="left" ><a href="/throttle/viewEditHub.do?customerId=<c:out value="${customer.custId}"/>&customerName=<c:out value="${customer.custName}"/>">view/edit</a></td>
                    </tr>
                <%
                            index++;
                            sno++;
                %>
            </c:forEach>

        </tbody>
    </table>
</c:if>
<script language="javascript" type="text/javascript">
    setFilterGrid("table");
</script>
<div id="controls">
    <div id="perpage">
        <select onchange="sorter.size(this.value)">
            <option value="5" selected="selected">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select>
        <span>Entries Per Page</span>
    </div>
    <div id="navigation">
        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
    </div>
    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
</div>
<script type="text/javascript">
    var sorter = new TINY.table.sorter("sorter");
    sorter.head = "head";
    sorter.asc = "asc";
    sorter.desc = "desc";
    sorter.even = "evenrow";
    sorter.odd = "oddrow";
    sorter.evensel = "evenselected";
    sorter.oddsel = "oddselected";
    sorter.paginate = true;
    sorter.currentid = "currentpage";
    sorter.limitid = "pagelimit";
    sorter.init("table", 1);
</script>
</form>
</body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
<!--<td align="left" >
                        <c:if test="${(customer.secondaryBillingTypeId=='1')}" >
                            Fixed KM Based
                        </c:if>

                        <c:if test="${(customer.secondaryBillingTypeId=='2')}" >
                            Actual KM Based
                        </c:if>
                        <c:if test="${(customer.secondaryBillingTypeId=='0')}" >
                            NA
                        </c:if>

                    </td>-->