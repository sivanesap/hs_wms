<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>


    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
</head>
<script language="javascript">
    function submitPage() {
        debugger;
        if (document.getElementById('branchName').value == '') {
            alert("please enter the Branch Name")
            $("#branchName").focus();
        } else if (document.getElementById('branchCode').value == '') {
            alert("please select the Branch Code")
            $("#branchCode").focus();}
         else if (document.getElementById('branchCity').value == '') {
            alert("please select the Branch City")
            $("#branchCity").focus();
        } else if (document.getElementById('branchPercent').value == '') {
            alert("please enter the branch Percent")
            $("#branchPercent").focus();}
        else if (document.getElementById('branchAddress').value == '') {
            alert("please enter the Branch address")
            $("#branchAddress").focus();}
        else if (document.getElementById('custState').value == '') {
            alert("please enter the State")
            $("#custState").focus();}

        else {
            $("#add").hide();
            document.editbranch.action = '/throttle/editBranch.do';
            document.editbranch.submit();
        }
    }
    
    function backPage(){
         document.editcustomer.action = '/throttle/handleViewCustomer.do';
        document.editcustomer.submit();
    }

    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
            document.getElementById("panNo").value = nPANNo.toUpperCase();
            var ObjVal = nPANNo;
            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
            var patternArray = ObjVal.match(pancardPattern);
            if (patternArray == null) {
                alert("PAN Card No Invalid ");
                return false;
            } else {
                return true;
            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }

    function setFocus() {
        document.editcustomer.custCode.focus();
    }
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };

    });
</script>


<script>
    function setAddValue() {
        var address = document.getElementById("custAddress").value;
        var city = document.getElementById("custCity").value;
        var State = document.getElementById("custState").value;
        var temp = State.split('-');
        var billingAddressIds = document.getElementsByName("billingAddressIds");
        var billingNameAddress = document.getElementsByName("billingNameAddress");
        var stateId = document.getElementsByName("stateId");

        for (var i = 0; i < billingAddressIds.length; i++) {
            billingNameAddress[0].value = address + ", " + city;
            stateId[0].value = temp[1];
            billingNameAddress[0].readOnly = true;
            stateId[0].readOnly = true;
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.EditCustomer" text="Edit Branch"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.EditCustomer" text="Edit Customer"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();">
                <form name="editbranch"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <c:if test="${getCustomerDetails != null}">
                        <c:forEach items="${getCustomerDetails}" var="cudl">
                            <table class="table table-info mb30 table-hover" id="bg" >
                                <thead>
                                    <tr align="center">
                                        <th colspan="4" align="center"  height="30"><div >EDIT Branch</div></th>
                                    </tr>
                                </thead>
                                <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>Branch Code</td>
                                    <td  height="30"><input name="customerId" id="customerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.customerId}"/>" onClick="ressetDate(this);">
                                        <input name="branchCode" id="branchCode" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchCode}"/>" ></td>

                            <td><font color="red">*</font>Branch Name</td>
                            <td>
                                <input type="text"  name="branchName" id="branchName"  value="<c:out value="${cudl.branchName}"/>" class="form-control" style="width:250px;height:40px;">
                            </td>
                                </tr>  
                                <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>Branch Address</td>
                                    <td  height="30"><input name="branchAddress1" id="branchAddress1" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchAddress}"/>" onClick="ressetDate(this);">
                                        <input name="branchAddress" id="branchAddress" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchAddress}"/>" ></td>

                            <td><font color="red">*</font>Royalty Percent</td>
                            <td>
                                <input type="text"  name="branchPercent" id="branchPercent"  value="<c:out value="${cudl.branchPercent}"/>" class="form-control" style="width:250px;height:40px;">
                            </td>
                                </tr>  
                                <input type="hidden"  name="branchId" id="branchId"  value="<c:out value="${cudl.branchId}"/>" class="form-control" style="width:250px;height:40px;">
                                <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>City</td>
                                     <td  height="30">  <select name="branchCity" id="branchCity" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                    <option value="">Choose One</option>
                                    <c:if test = "${CityList != null}" >
                                        <c:forEach items="${CityList}" var="Type">
                                            <option value='<c:out value="${Type.cityId}" />'><c:out value="${Type.cityName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <script>
            document.getElementById("branchCity").value = '<c:out value="${cudl.branchCity}"/>';
                                </script>
<!--                                    <td  height="30"><input name="customerId" id="customerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchCity}"/>" onClick="ressetDate(this);">
                                        <input name="customerName" id="customerName" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchCity}"/>" ></td>-->

                            <td><font color="red">*</font>Warehouse</td>
                            <td  height="30">  <select name="hub" id="hub" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                    <option value="">Choose One</option>
                                    <c:if test = "${hubList != null}" >
                                        <c:forEach items="${hubList}" var="Type">
                                            <option value='<c:out value="${Type.hubId}" />'><c:out value="${Type.hubName}" /></option>
                                        </c:forEach >
                                    </c:if>
                                </select>
                            </td>
                            <script>
            document.getElementById("hub").value = '<c:out value="${cudl.hub}"/>';
                                </script>
                            
                                </tr>
                                <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>Phone Numer</td>
                                    <td  height="30"><input name="customerId" id="customerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.phoneNo}"/>" onClick="ressetDate(this);">
                                        <input name="phoneNo" id="phoneNo" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.phoneNo}"/>" ></td>

                            <td><font color="red">*</font>Email</td>
                            <td>
                                <input type="text"  name="email" id="email"  value="<c:out value="${cudl.email}"/>" class="form-control" style="width:250px;height:40px;">
                            </td>
                                </tr>  
                                
                                    <tr>
                                    <!--<td  height="30"><input name="custCode" id="custCode" type="text" class="form-control" style="width:250px;height:40px" value="" maxlength="8"></td>-->
                                    <td  height="30"><font color="red">*</font>State</td>
                                    <td  height="30">
                                        <select name="custState" id="custState" class="form-control" required data-placeholder="Choose One" style="width:250px;height:40px" onchange="setAddValue()">
                                            <option value="0">Choose One</option>
                                            <c:if test = "${stateList != null}" >
                                                <c:forEach items="${stateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateId}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                     </td>
<!--                                    <td  height="30"><input name="customerId" id="customerId" type="hidden" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.branchCity}"/>" onClick="ressetDate(this);">
                                        <input name="state" id="state" type="text" class="form-control" style="width:250px;height:40px" value="<c:out value="${cudl.state}"/>" ></td>-->
                                             <script>
            document.getElementById("custState").value = '<c:out value="${cudl.state}"/>';
                                </script>

                            <td><font color="red">*</font>GST</td>
                            <td>
                                <input type="text"  name="gstNo" id="gstNo"  value="<c:out value="${cudl.gstNo}"/>" class="form-control" style="width:250px;height:40px;">
                            </td>
                                </tr>  
                                
                                <tr>
                                     <td  width="80" >Active </td>
                            <td  width="80"> <select name="activeInd" id="activeInd" class="form-control" style="width:250px;height:40px" style="width:125px;" >
                                    <option value="" selected>--select--</option>
                                    <option value="Y" >YES</option>
                                    <option value="N" >NO</option>
                                </select></td>
                                <td></td>
                                 <script>
            document.getElementById("activeInd").value = '<c:out value="${cudl.activeInd}"/>';
                                </script>
                                </tr>
                            </table>
                        </c:forEach >
                    </c:if>

                    
                    <center>
                        <input type="button" value="SAVE" class="btn btn-success" onClick="submitPage();">
                        <input type="button" value="CANCEL" class="btn btn-success" onClick="backPage();">
                    </center>


                    <script>
                        var rowCount = 1;
                        var sno = 0;
                        var httpRequest;
                        var httpReq;
                        var rowIndex = 0;
                        var styl = '';
                        function addRow()
                        {

                            if (parseInt(rowCount) % 2 == 0)
                            {
                                styl = "text2";
                            } else {
                                styl = "text1";
                            }




                            sno++;
                            document.editcustomer.selectedIndex.value = sno;
                            var tab = document.getElementById("items");
                            var iRowCount = tab.getElementsByTagName('tr').length;
                            //                            alert("len:" + iRowCount);
                            rowCount = iRowCount;
                            var newrow = tab.insertRow(rowCount);
                            sno = rowCount;
                            rowIndex = rowCount;
                            //alert("rowIndex:"+rowIndex);
                            var cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' > <input type='hidden'  name='billingAddressIds' value='' > " + sno + "</td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            var cell = newrow.insertCell(1);
                            var cell0 = "<td class='text1' height='25' ><textarea name='billingNameAddress' id='billingNameAddress" + rowIndex + "' cols='100' rows='4' style='width:250px;height:40px'></textarea></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;

                            cell = newrow.insertCell(2);
                            var cell1 = "<td class='text1' height='25'><select class='form-control' name='stateId' id='stateId" + rowIndex + "' required data-placeholder='Choose One' style='width:250px;height:40px'><option value='0'>--Select--</option><c:forEach items='${stateList}' var='Type'><option value='<c:out value='${Type.stateId}'/>'><c:out value='${Type.stateName}'/></option></c:forEach></select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(3);
                            var cell2 = "<td class='text1' height='25'><input name='gstNo' id='gstNo" + rowIndex + "' class='form-control'   type='text' style='width:250px;height:40px'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(4);
                            cell2 = "<td class='text1' height='25'><select class='form-control' name='activeInd' id='activeInd" + rowIndex + "' style='width:250px;height:40px'><option value='0'>--Select--</option><option value='Y'>YES</option> <option value='N'>NO</option></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            
                            if (rowCount > 1) {
                            cell = newrow.insertCell(5);
                            var cell2 = "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete" + rowIndex + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell2;
                            }

                            //        getItemNames(rowIndex);
                            //        var itemCode = document.getElementsByName("itemCode");
                            //        itemCode[rowCount - 1].focus();

                            rowIndex++;
                            rowCount++;

                        }
                    </script>
                    <script>
                    function deleteRow(src) {
                        rowIndex--;
                        var oRow = src.parentNode.parentNode;
                        var dRow = document.getElementById('items');
                        dRow.deleteRow(oRow.rowIndex);
                        document.getElementById("selectedRowCount").value--;
                    }

                </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
