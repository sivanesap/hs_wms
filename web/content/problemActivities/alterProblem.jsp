<%-- 
    Document   : addVendor
    Created on : Mar 8, 2009, 12:48:39 PM
    Author     : karudaiyar Subramaniam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>   
    
</head>
<script>
    function submitpage()
    {
         if(isSelect(document.dept.sectionId,"sectionName")){           
       } else if(textValidation(document.dept.problemName,"Problem Name")){               
               } else if(textValidation(document.dept.discription,"Vendor Address")){     
     }else{
         document.dept.action= "/throttle/alterProblem.do";
        document.dept.submit();
    }
    }
   

</script>

<body   >
<form name="dept"  method="post"     >
<%@ include file="/content/common/path.jsp" %>
     

<%@ include file="/content/common/message.jsp" %>

    


<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
 

    <c:if test="${GetProblemDetailUnique != null}">
        <c:forEach items="${GetProblemDetailUnique}" var="vnd" >
            
           <tr height="30">
<td colspan="5" class="contenthead">Alter Problem</td>
</tr>
            <tr>  
 <td class="text2" height="30">Section<c:out value="${comp.sectionId}" /></td>
                            <td class="text2"  ><select class="textbox" name="sectionId" style="width:125px" >
                                    <option value="0">---Select---</option>
                                    <c:if test = "${SectionList != null}" >
                                        <c:forEach items="${SectionList}" var="Dept"> 
                                            <c:choose>
                                                <c:when test="${vnd.sectionId == Dept.sectionId}" >
                                                    <option selected value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value='<c:out value="${Dept.sectionId}" />'><c:out value="${Dept.sectionName}" /></option>
                                                </c:otherwise>
                                            </c:choose>                                            
                                        </c:forEach >
                                    </c:if>  	
                            </select></td>
            </tr>                                             
            <tr>
                <td class="text1" height="30">Problem Name </td>
                <td class="text1" height="30">
                    <input type="hidden" name="problemId" value="<%=request.getAttribute("problemId")%>"  >
                       <input name="problemName" type="text" class="textbox" value="<c:out value="${vnd.problemName}"/>"  ></td>
            </tr>
            
            <tr>
                <td class="text2" height="30">Description</td>
                <td class="text2" height="30"><textarea class="textbox" name="discription"><c:out value="${vnd.discription}" /></textarea></td>
            </tr>
            
            <tr>
                <td class="text2" height="30">Status </td>
                <td class="text2" height="30">
                        <select name="activeInd" class="textbox" style="width:125px" >
                        <c:if test="${(vnd.activeInd=='y') || (vnd.activeInd=='Y')}" >
                        <option value="Y" selected>Active</option><option value="N">InActive</option>
                        </c:if>
                        <c:if test="${(vnd.activeInd=='n') || (vnd.activeInd=='N')}" >
                        <option value="Y" >Active</option><option value="N" selected>InActive</option>                           
                        </c:if>                            
                        </select>                    
                    
                </td>
            </tr>
   </c:forEach>  
    </c:if>    
</table>



<br>

 


<center>
    <input type="button" value="Alter" class="button" onClick="submitpage();">
</center>
</form>
</body>
</html>

