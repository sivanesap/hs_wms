<%--     
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
        String menuPath = "Finance >> Excel";
        request.setAttribute("menuPath", menuPath);
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "OrderDetails-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${ordersList != null}" >
                <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="40">
                            <th>Sno</th>
                            <th>LoanProposalID</th>
                            <th>Customer</th>                    
                            <th>Batch Number</th>
                            <th>Batch Date</th>
                            <th>Branch</th>
                            <th>Member Offer Price</th>
                            <th>Order Status</th>
                    <th>Check</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                        int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${ordersList}" var="order">

                            <tr height="30">
                                <td align="left" ><%=sno%></td>                                
                                <td align="left" ><c:out value="${order.orderNo}"/></td>                        
                                <td align="left" ><c:out value="${order.clientName}"/></td>                        
                                <td align="left" ><c:out value="${order.batchNumber}"/></td>
                                <td align="left" ><c:out value="${order.batchDate}"/></td>
                                <td align="left" ><c:out value="${order.branchName}"/></td>                        
                                <td align="left" ><c:out value="${order.memberOfferPrice}"/></td>                        
                                <td align="left" >                           
                                    <c:if test="${order.cancel == 1}">
                                        Order Canceled
                                    </c:if>
                                    <c:if test="${order.cancel != 1}">
                                <c:if test="${order.invoiceStatus == 0}">
                                    Order Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 1}">
                                    Scheduled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 2}">
                                    Invoice Created
                                </c:if>
                                <c:if test="${order.invoiceStatus == 3}">
                                    OFD
                                </c:if>
                                <c:if test="${order.invoiceStatus == 4}">
                                    Delivered
                                </c:if>
                                <c:if test="${order.invoiceStatus == 5}">
                                    Returned
                                </c:if>
                                <c:if test="${order.invoiceStatus == 8}">
                                    Cancelled
                                </c:if>
                                <c:if test="${order.invoiceStatus == 9}">
                                    OFD
                                </c:if>
                            </c:if>
                                             <td>
                            <c:if test="${order.whStatus == 0}">
                                <font color="red"><b>Warehouse Invalid</b></font>
                            </c:if>
                            <c:if test="${order.whStatus != 0}">
                                <c:if test="${order.branchStatus == 0}">                                
                                        <font color="red"><b>Branch Invalid</b></font>
                                </c:if>
                                <c:if test="${order.branchStatus != 0}">
                                    <c:if test="${order.itemStatus == 0}">
                                    <font color="red"><b>Product Invalid</b></font>
                                    </c:if>
                                    <c:if test="${order.itemStatus != 0}">
                                    -
                                    </c:if>
                                </c:if>
                            </c:if>
                                    
                                    
                        </td>

                            </tr>
                            <%index++;%>
                            <%sno++;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <br>
            <br>

        </form>
    </body>
</html>