<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveCustomerOutStandingHistory.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/customerOutStandingHistoryUpload.do';
            document.upload.submit();
        }
    }
</script>


<!--    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head> -->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CustomerOutstandingUpload" text="Customer Outstanding Upload"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.CustomerOutstandingUpload" text="Customer Outstanding Upload"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body>
        <c:if test="${customerOutStandingList == null}">
        <form name="upload" method="post" enctype="multipart/form-data">
        </c:if>
        <c:if test="${customerOutStandingList != null}">
        <form name="upload" method="post">
        </c:if>
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            
            
            

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <c:if test="${customerOutStandingList == null && updateCustomerOutStanding == null}">
            <table class="table table-info mb30 table-hover" id="bg" >	
                <thead>
                <tr>
                    <th  colspan="4">Customer Outstanding Upload</th>
                </tr>
                </thead>
                <tr>
                    <td >Select file</td>
                    <td ><input type="file" name="importCustomerOutstanding" id="importCustomerOutstanding" ></td>                             
                    <td >Last Transaction Date</td>
                    <td ><c:out value="${lastUploadCustomerOutStandingDate}"/></td>                             
                </tr>
                <tr>
                    <td colspan="4"  align="center" ><input type="button" value="Submit" class="btn btn-success" name="Submit" onclick="submitPage(this.value)" >
                </tr>
            </table>
            </c:if>    
            
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${customerOutStandingList != null}">
               <table class="table table-info mb30 table-hover" id="bg" >	
                <thead>
                    <tr>
                        <th >S No</th>
                        <th >Status</th>
                        <th >Customer Code</th>
                        <th >Customer Name</th>
                        <th >Previous Outstanding Amount</th>
                        <th >Outstanding Amount</th>
                    </tr>
                </thead>
                    <c:forEach items="${customerOutStandingList}" var="outstanding">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td  ><font color="green"><%=i++%></font></td>
                                <td  ><font color="green"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${outstanding.customerId}"/>" />OK</font></td>
                                <td  ><font color="green"><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${outstanding.customerCode}"/>" /><c:out value="${outstanding.customerCode}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${outstanding.customerName}"/>" /><c:out value="${outstanding.customerName}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="previousOutStandingAmount" id="previousOutStandingAmount" value="<c:out value="${outstanding.amount}"/>" /><c:out value="${outstanding.amount}"/></font></td>
                                <td  ><font color="green"><input type="hidden" name="outStandingAmount" id="outStandingAmount" value="<c:out value="${outstanding.outStandingAmount}"/>" /><c:out value="${outstanding.outStandingAmount}"/></font></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                
                <c:if test="${updateCustomerOutStanding == null}">
                <center>
                    <input type="button" class="btn btn-success" value="Proceed" onclick="submitPage(this.value)"/>
                </center>
                </c:if>
            </c:if>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>