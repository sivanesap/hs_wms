<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>
<script>
    function searchPage() {
        document.invoiceSearch.action = "/throttle/handleViewOrderInvoice.do";
        document.invoiceSearch.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Invoice List" text="View Invoice List"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.View Invoice List" text="View Invoice List"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%
                            String menuPath = "Invoice List >> View / Edit";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="invoiceSearch" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" id="report" >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Invoice</th>
                            </tr>
                        </thead>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                        </tr>
                        <tr>             
                            <td></td>
                            <td></td>
                            <td  colspan="2"><input  class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                        </tr>
                    </table>
        </div>
    </div>

    <c:if test = "${ordersInvoiceList != null}" >
        <table class="table table-info mb30 table-hover" id="table" >
            <thead>
                <tr height="40">
                    <th>Sno</th>
                    <th>LoanProposalID</th>
                    <th>Invoice No</th>
                    <th>Invoice Date</th>
                    <th>Serial No</th>
                    <th>Barcode</th>
                    <th>Path</th>
                    <th>View</th>
                </tr>
            </thead>
            <% int index = 0;
                        int sno = 1;
            %>
            <tbody>
                <c:forEach items="${ordersInvoiceList}" var="order">

                    <tr height="30">
                        <td align="left" ><%=sno%></td>
                        <td align="left" ><c:out value="${order.loanProposalId}"/></td>
                        <td>
                            <!--<a href='/throttle/viewOrderInvoiceDetails.do?invoiceId=<c:out value="${order.invoiceId}"/>'></a>-->
                            <c:out value="${order.invoiceCode}"/>
                        </td>
                        <td align="left" ><c:out value="${order.invoiceDate}"/></td>
                        <td align="left" ><c:out value="${order.serialNumber}"/></td>
                        <td align="left" ><c:out value="${order.barCode}"/></td>
                        <td align="left" ><a href="<c:out value="${order.path}"/>" target="_blank" download>
                                <span class="label label-Primary"><font size="2">INVOICE</font></span> </a>
                        </td>

                        <td>
                            <a href="#"   onclick="generateInvoice('<c:out value="${order.orderId}"/>')" >
                                <span class="label label-danger"><font size="2">VIEW</font></span> 
                            </a>

                        </td>
                    </tr>
                    <%index++;%>
                    <%sno++;%>
                </c:forEach>
            </tbody>
        </table>
    </c:if>
    <script>
        function generateInvoice(orderId) {
            var param = "0";
            document.invoiceSearch.action = "/throttle/getInvoiceForUpdate.do?orderId=" + orderId + "&param=" + param;
            document.invoiceSearch.submit();
        }


    </script>
    <script language="javascript" type="text/javascript">
        setFilterGrid("table");
    </script>

</form>
</body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
