<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">    
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">


    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripMerging() {
        var tripId = document.getElementById('tripIdValue').value;
        var temp = tripId.split(",");
        if (document.getElementById('tripTypeValue').value != "" && document.getElementById('tripIdValue').value != "") {
            document.CNoteSearch.action = "/throttle/emptyTripMergingDetails.do";
            document.CNoteSearch.submit();
        } else {
            alert("Please select trip and proceed");
        }
    }



    var cntr = 0;
    var temp = "";
    function setTripId(val) {
        var tripId = document.getElementById('tripId' + val).value;
        var tripType = document.getElementById('tripType' + val).value;
        if (tripType == 'E' && document.getElementById('tripTypeValue').value == '') {
            alert("please select loaded trip first");
            document.getElementById('tripTypeValue').value == '';
            document.getElementById('selectedIndex' + val).checked = false;
            temp ="";
        } else {
            if (temp == '') {
                temp = tripId;
                document.getElementById('selectedIndex' + val).checked = true;
            } else if (temp != '' && document.getElementById('tripTypeValue').value == '') {
                temp = temp + "," + tripId;
                document.getElementById('selectedIndex' + val).checked = true;
            } else if (temp != '' && document.getElementById('tripTypeValue').value != '' && document.getElementById('tripType' + val).value == 'E') {
                if (temp.indexOf(tripId) != -1) {
                    document.getElementById('selectedIndex' + val).checked = false;
//                    alert("check"+temp);
                    var tempNew = temp.split(",");
                    var tempNew1 = "";
                    for(var i=0; i<tempNew.length; i++){
                        if(tripId == tempNew[i]){
                         //tempNew1 = '';   
                        }else{
                          tempNew1 =   tempNew1 +","+tempNew[i];
                        }
                        temp = tempNew1;
                    }
                } else {
                    temp = temp + "," + tripId;
                    document.getElementById('selectedIndex' + val).checked = true;
                }
            } else {
                var x;
                var r = confirm("Click ok for change the trip merging!");
                if (r == true) {
                    document.getElementById('tripTypeValue').value = "";
                    tripId = "";
                    tripType = "";
                    document.getElementById('tripIdValue').value = "";
                    var selected = document.getElementsByName('selectedIndex');
                    temp = "";
                    for (var i = 0; i < selected.length; i++) {
                        document.getElementById('selectedIndex' + i).checked = false;
                    }
                } else {
                    document.getElementById('selectedIndex' + val).checked = false;
                }
            }
        }
        if (tripType == 'L' && document.getElementById('tripTypeValue').value == '' && tripId != '' && tripType !='') {
            document.getElementById('tripTypeValue').value = tripType + "-" + tripId;
        }

//        alert(temp);
        document.getElementById('tripIdValue').value = temp;
    }



    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }



    function searchPage() {
        document.CNoteSearch.action = "/throttle/emptyTripMerging.do";
        document.CNoteSearch.submit();
    }



   

     $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#regno').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleRegNoForMerging.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                            alert("Invalid Vehicle No");
                            $('#regno').val('');
                            $('#vehicleId').val('');
                            
                            $('#regno').fous();
                        }else{
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.vehicleNo;
                var id = ui.item.vehicleId;
                
                $('#regno').val(value);
                $('#vehicleId').val(id);
                
               
               
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.vehicleNo;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
        };
    });


</script>

<div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Trip Details"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

<body>
    <%
                String menuPath = "Operation >> Empty Trip Merging";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
        
        
 <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >Trip Details</th>
		</tr>
		    </thead>
    </table>
            <table class="table table-info mb30 table-hover" id="report" >
                       
                                <tr>
                                    <td >Customer Name</td>
                                    <td>
                                        <input type="hidden"     style="width:250px;height:40px" name="tripTypeValue" id="tripTypeValue" value=""/>
                                        <input type="hidden"     style="width:250px;height:40px" name="tripIdValue" id="tripIdValue"  value=""/>
                                        <input type="hidden"     style="width:250px;height:40px" name="customerId" id="customerId"  value="<c:out value="${customerId}"/>"/>
                                        <input type="text"    class="form-control" style="width:240px;height:40px" name="customerName" id="customerName" value="<c:out value="${customerName}"/>"/>
                                    </td>

                                    <td>Trip Code</td>
                                    <td><input type="text"     class="form-control" style="width:240px;height:40px"  name="tripCode" id="tripCode" value="<c:out value="${tripCode}"/>"/></td>


                                </tr>
                                <tr>

                                    <td><font color="red"></font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red"></font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                </tr>
                                <tr>
                                    <td>Vehicle No</td>
                                    <td>
                                    <!--    <select name="vehicleId" id="vehicleId"     style="width:250px;height:40px"style="height:20px; width:122px;"" >
                                            <c:if test="${vehicleList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vehicleList}" var="vehicleList">
                                                    <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>  -->
                                     <input name="vehicleId" id="vehicleId" maxlength="10"   type="hidden"       class="form-control" style="width:240px;height:40px" value='<c:out value="${vehicleId}"/>'>
                                      <input name="regno" id="regno" maxlength="10"   type="text"       class="form-control" style="width:240px;height:40px"  value='<c:out value="${regNo}"/>'>
</tr>
</table>
                                <tr>
    <center>
                                    </td>
                                    <td colspan="2" align="center"><input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                                </center>
                                     </tr>
                            <br>
                            <br>
                                      
              
        
        <c:if test = "${emptyTripList == null}" >
            <center><font color="red" style="text-align: right">No Records Found</font></center>
        </c:if>    
        <c:if test = "${emptyTripList != null}" >
           <table class="table table-info mb30 table-hover sortable" id="table" >
                <thead>
                    <tr >
                        <th>Sno</th>
                        <th>Consignment No</th>
                        <th>Trip Code</th>
                        <th>Vehicle No</th>
                        <th>Route </th>
                        <th>Trip Start Date </th>
                        <th>Trip Start Time </th>
                        <th>Trip End Date</th>
                        <th>Trip End Time</th>
                        <th>Trip Type</th>
                        <th>Status</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${emptyTripList}" var="cnl">

                        <tr height="30">
                            <td align="left" ><%=sno%></td>

                            <td align="left" > <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentId}"/>');"><c:out value="${cnl.consignmentNote}"/></a></td>
                            <td align="left" >
                                <input type="hidden" name="tripId" id="tripId<%=index%>" value="<c:out value="${cnl.tripId}"/>"/>
                                <a href="#" onclick="viewTripDetails('<c:out value="${cnl.tripId}"/>');"><c:out value="${cnl.tripCode}"/></a>
                            </td>
                            <td align="left" >
                                <a href="#" onclick="viewVehicleDetails('<c:out value="${cnl.vehicleId}"/>')"><c:out value="${cnl.regNo}"/></a>
                            </td>

                            <td align="left" ><c:out value="${cnl.routeInfo}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartTime}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndTime}"/></td>
                            <td align="left" >
                                <c:if test="${cnl.tripType == 'E'}">
                                    Empty Trip
                                </c:if>
                                <c:if test="${cnl.tripType == 'L'}">
                                    Loaded Trip
                                </c:if>
                            </td>
                            <td align="left" >
                                <c:out value="${cnl.statusName}"/>
                            </td>
                            <td align="left" >
                                <input type="hidden" name="tripType" id="tripType<%=index%>" value="<c:out value="${cnl.tripType}"/>" />
                                <input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" value="<c:out value="${cnl.tripId}"/>" onclick="setTripId('<%=index%>');" />
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
       <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        <center>
            &nbsp;&nbsp;<input type="button" class="btn btn-success" name="trip"  value="Empty Trip Merging" onclick="submitPageForTripMerging()"/>
        </center>
    </form>
    </c:if>
</body>

</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>