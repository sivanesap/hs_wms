<%-- 
    Document   : billingdetail
    Created on : Dec 10, 2013, 2:58:42 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>

        <script>

            function submitBill() {
                //alert("hi");
                var invoiceIdVal = document.viewbillGeneration.invoiceIdValue.value;
                document.viewbillGeneration.action = "/throttle/submitBill.do?invoiceId=" + invoiceIdVal;
                document.viewbillGeneration.submit();

            }
            function editInvoice(tripId) {
                var invoiceIdVal = document.viewbillGeneration.invoiceIdValue.value;
                document.viewbillGeneration.action = "/throttle/editInvoiceAmount.do?invoiceId=" + invoiceIdVal + "&tripId=" + tripId;
                document.viewbillGeneration.submit();

            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function goback() {
                history.go(-1);
            }

            function popUp(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                if (http.status != 404) {
                    popupWindow = window.open(
                            url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
                else {
                    var url1 = "/throttle/content/trip/fileNotFound.jsp";
                    popupWindow = window.open(
                            url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                }
            }
            function importExcel(tripId) {
                document.viewbillGeneration.action = '/throttle/content/trip/temperatureLogUpload.jsp?tripId=' + tripId;
                document.viewbillGeneration.submit();
            }


            function viewGraphBlobData(tripGraphId, tripId) {
                window.open('/throttle/content/trip/displayDalaLogBlobData.jsp?tripGraphId=' + tripGraphId + '&tripId=' + tripId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body onload="showDiv(1)">
        <form name="viewbillGeneration" method="post">
            <br>
            Finance >> Generate Invoice

            <c:set var="routeInfo" value="" />
            <c:set var="VehicleInfo" value="" />
            <div id="tabs" >
                <ul>
                    <li><a href="#invoice"><span>Edit Invoice Details</span></a></li>
                    <li><a href="#courierDetails"><span>Courier Details</span></a></li>
                    <li><a href="#podDetail"><span>Trip POD Details</span></a></li>
                    <li><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <c:if test = "${trip.reeferRequired eq 'Yes'}" >
                                <li><a href="#temperatureLog"><span>Temperature Log</span></a></li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                </ul>

                <div id="courierDetails">

                    <table>
                        <tr>
                            <td>Courier Reference No</td>
                            <td><input type="text" name="courierNo" id="courierNo" value="<c:out value="${courierNo}"/>"/></td>
                            <td>Courier Reference Remarks</td>
                            <td><textarea name="courierRemarks" id="courierRemarks" cols="20" rows="4" style="width: 150px;"><c:out value="${courierRemarks}"/></textarea></td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button" class="button" value="Save Courier Details" name="Save" style="width: 200px;" onclick="saveCourierDetails(<c:out value="${tripId}"/>);"/>
                        &nbsp;&nbsp;&nbsp;
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                    </center>
                    <% int index12 = 1;%>

                    <c:if test = "${courierDetails == null}" >
                        <center><font color="red">No Records Found</font></center>
                            </c:if>
                            <c:if test = "${courierDetails != null}" >
                        <table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Trip Code</td>
                                <td class="contenthead" height="30" >Invoice Code</td>
                                <td class="contenthead" height="30" >Courier No</td>
                                <td class="contenthead" height="30" >Courier Remarks</td>
                            </tr>
                            <c:forEach items="${courierDetails}" var="courierDetails">
                                <%
                                            String classText12 = "";
                                            int oddEven12 = index12 % 2;
                                            if (oddEven12 > 0) {
                                                classText12 = "text1";
                                            } else {
                                                classText12 = "text2";
                                            }
                                %>
                                <tr>
                                    <td class="<%=classText12%>" height="30" ><%=index12++%></td>
                                    <td class="<%=classText12%>" height="30" ><c:out value="${courierDetails.tripCode}" /></td>
                                    <td class="<%=classText12%>" height="30" ><c:out value="${courierDetails.invoiceCode}" /></td>
                                    <td class="<%=classText12%>" height="30" ><c:out value="${courierDetails.courierNo}" /></td>
                                    <td class="<%=classText12%>" height="30" ><c:out value="${courierDetails.courierRemarks}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>

                    </c:if>
                    <br>
                    <br>
                </div>
                <div id="bpclDetail">
                    <% int index10 = 1;%>

                    <c:if test = "${bpclTransactionHistory == null}" >
                        <center><font color="red">No Records Found</font></center>
                            </c:if>
                            <c:if test = "${bpclTransactionHistory != null}" >
                                <c:set var="totalAmount" value="0"/>
                        <table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Trip Code</td>
                                <td class="contenthead" height="30" >Vehicle No</td>
                                <td class="contenthead" height="30" >Transaction History Id</td>
                                <td class="contenthead" height="30" >BPCL Transaction Id</td>
                                <td class="contenthead" height="30" >BPCL Account Id</td>
                                <td class="contenthead" height="30" >Dealer Name</td>
                                <td class="contenthead" height="30" >Dealer City</td>
                                <td class="contenthead" height="30" >Transaction Date</td>
                                <td class="contenthead" height="30" >Accounting Date</td>
                                <td class="contenthead" height="30" >Transaction Type</td>
                                <td class="contenthead" height="30" >Currency</td>
                                <td class="contenthead" height="30" >Transaction Amount</td>
                                <td class="contenthead" height="30" >Volume Document No</td>
                                <td class="contenthead" height="30" >Amount Balance</td>
                                <td class="contenthead" height="30" >Petromiles Earned</td>
                                <td class="contenthead" height="30" >Odometer Reading</td>
                            </tr>
                            <c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
                                <%
                                            String classText10 = "";
                                            int oddEven10 = index10 % 2;
                                            if (oddEven10 > 0) {
                                                classText10 = "text1";
                                            } else {
                                                classText10 = "text2";
                                            }
                                %>
                                <tr>
                                    <td class="<%=classText10%>" height="30" ><%=index10++%></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.tripCode}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.vehicleNo}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountId}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerName}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerCity}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionDate}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountingDate}" /></td>
                                    <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionType}" /></td>
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.currency}" /></td>
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amount}" /></td>
                                    <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
                                    <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
                                </tr>
                            </c:forEach >


                            <tr>
                                <td class="contenthead" height="30" style="text-align: right" colspan="12">Total Amount</td>
                                <td class="contenthead" height="30" style="text-align: right"  ><c:out value="${totalAmount}"/></td>
                                <td class="contenthead" height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        <br/>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>
                </div>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <c:if test = "${trip.reeferRequired eq 'Yes'}" >
                            <div id="temperatureLog">

                                <c:if test="${viewTempLogDetails != null}">
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <th  class="contenthead">S No&nbsp;</th>
                                            <th  class="contenthead">Trip Code</th>
                                            <th class="contenthead">Temperature Log Excel </th>
                                            <th class="contenthead">Log Sheet Status </th>
                                            <th class="contenthead">Approval Status </th>

                                        </tr>
                                        <c:forEach items="${viewTempLogDetails}" var="tempLog">
                                            <c:set var="tripId" value="${tempLog.tripId}"/>
                                            <c:set var="approvalStatus" value="${tempLog.tempApprovalStatus}"/>
                                            <% int index2 = 1;%>
                                            <%
                                               String classText3 = "";
                                               int oddEven = index2 % 2;
                                               if (oddEven > 0) {
                                                   classText3 = "text1";
                                               } else {
                                                   classText3 = "text2";
                                               }
                                            %>
                                            <tr>
                                                <td class="<%=classText3%>" ><%=index2++%></td>
                                                <td class="<%=classText3%>" ><input type="hidden" name="tripId" id="tripId" value="<c:out value="${tempLog.tripId}"/>" />
                                                    <input type="hidden" name="tripCode" id="tripCode" value="<c:out value="${tempLog.tripCode}"/>" />  <c:out value="${tempLog.tripCode}"/>
                                                    <input type="hidden" name="approvalStatus" id="approvalStatus" value="" />  </td>

                                                <td class="<%=classText3%>" >
                                                    <a onclick="viewGraphBlobData('<c:out value="${tempLog.tripGraphId}"/>', '<c:out value="${tempLog.tripId}"/>')" href="#"><c:out value="${tempLog.tempLogFile}"/></a>
                                                </td>
                                                <td class="<%=classText3%>" ><c:out value="${tempLog.status}"/></td>
                                                <td class="<%=classText3%>" >
                                                    <c:if test="${approvalStatus == '0'}">
                                                        Waiting for Request
                                                    </c:if>
                                                    <c:if test="${approvalStatus == '1'}">
                                                        Waiting for Approval
                                                    </c:if>
                                                    <c:if test="${approvalStatus == '2'}">
                                                        Approved
                                                    </c:if>
                                                    <c:if test="${approvalStatus == '3'}">
                                                        Rejected
                                                    </c:if>
                                                </td>
                                                </td>
                                            </tr>
                                        </c:forEach>                                  <br/>
                                        <br/>
                                        <br/>
                                    </table>
                                    <script type="text/javascript">
                                        function popUp1(url) {
                                            var http = new XMLHttpRequest();
                                            http.open('HEAD', url, true);
                                            http.send();
                                            if (http.status != 404) {
                                                popupWindow = window.open(
                                                        url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                                            }
                                            else {
                                                var url1 = "/throttle/content/trip/fileNotFound.jsp";
                                                popupWindow = window.open(
                                                        url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
                                            }
                                        }
                                        function TempApproval() {
                                            document.getElementById("approvalStatus").value = 1;
                                            document.viewbillGeneration.action = '/throttle/saveTemperatureLogRequest.do';
                                            document.viewbillGeneration.submit();
                                        }
                                    </script>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <center>
                                        <c:if test="${approvalStatus == 0}">
                                            <input type="button" class="button" name="Save" style="width:200px;" value="Request Approval" onclick="TempApproval();" />
                                            &nbsp;&nbsp;<input type="button"   value="Upload Temperature Log" class="button" name="search" onClick="importExcel(<c:out value="${tripId}"/>)" style="width:200px">
                                        </c:if>
                                        <c:if test="${approvalStatus == '2'}">
                                            <input type="button" class="button" name="Save" style="width:200px;" value="Submit Bill" onclick="submitBill();" />
                                        </c:if>
                                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                                    </center>
                                </c:if>
                            </div>
                        </c:if></c:forEach></c:if>
                        <div id="podDetail">
                    <c:if test="${viewPODDetails != null}">
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th class="contenthead">City Name</th>
                                <th class="contenthead">POD file Name</th>
                                <th class="contenthead">LR Number</th>
                                <th class="contenthead">POD Remarks</th>
                            </tr>
                            <% int index2 = 1;%>
                            <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                <%
                                   String classText3 = "";
                                   int oddEven = index2 % 2;
                                   if (oddEven > 0) {
                                       classText3 = "text1";
                                   } else {
                                       classText3 = "text2";
                                   }
                                %>
                                <tr>
                                    <td class="<%=classText3%>" ><%=index2++%></td>
                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                    <td class="<%=classText3%>" >
                                        <a onclick="viewPODFiles('<c:out value="${viewPODDetails.tripPodId}"/>')" href="#"><c:out value="${viewPODDetails.podFile}"/></a>
                                    </td>
                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                </tr>
                            </c:forEach>
                            <br/>
                            <br/>
                            <br/>
                        </table>
                        <br/>
                        <br/>
                        <br/>
                        <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                        <script>
                            function viewPODFiles(tripPodId) {
                                window.open('/throttle/content/trip/displayBlobData.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                            }
                        </script>
                    </c:if>
                </div>

                <div id="invoice" >
                    <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr><td class="text1">Edit Mode</td>
                            <td class="text1">
                                <select name="editMode" id="editMode" onchange="showDiv(this.value);">
                                    <option value="1">Discount</option>
                                    <option value="2">Change Freight</option>
                                </select>
                            </td></tr>
                    </table>
                    <div id="discountDiv">
                        <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr class="text1" >
                                <td>Invoice Amount</td>
                                <td><input type="text" name="discountFreighgt" id="discountFreight" value="<c:out value="${freightAmount}"/>" readonly/></td>
                            </tr>
                            <tr class="text2">
                                <td>Discount Amount</td>
                                <td><input type="text" name="discountAmount" id="discountAmount" value="" onkeyup="calculateDiscount();" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;"/></td>
                            </tr>
                            <tr class="text1">
                                <td>Billed Amount</td>
                                <td><input type="text" name="billedAmount" id="billedAmount" value="" readonly/></td>
                            </tr>
                        </table>
                    </div>
                                <input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingType}"/>"/>
                    <div id="changeFreightDiv">
                        <c:if test="${billingType == 1}">
                            <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr class="text1">
                                    <td>Billing Type Name</td>
                                    <td>Point To Point</td>
                                </tr>
                                <tr  class="text2">
                                    <td>Invoice Amount</td>
                                    <td><c:out value="${freightAmount}"/></td>
                                </tr>
                                <tr  class="text1">
                                    <td>Billed Amount</td>
                                    <td><input type="text" name="billedAmountPointToPoint" id="billedAmountPointToPoint" value="<c:out value="${freightAmount}"/>" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;"/></td>
                                </tr>

                            </table>    
                        </c:if>
                        <c:if test="${billingType == 2}">
                            <table  border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr class="contenthead">
                                    <td>Billing Type Name</td>
                                    <td>Point To Point Weight Based</td>
                                </tr>
                                <tr>
                                    <td>Current Freight</td>
                                    <td>
                                        <table  border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr class="text1">
                                                <td>Weight&nbsp;*&nbsp;Rate</td>
                                                <td><c:out value="${totalWeight}"/>&nbsp;*&nbsp;<c:out value="${ratePerKm}"/></td>
                                            </tr>
                                            <tr  class="text2">
                                                <td>Invoice Amount</td>
                                                <td><c:out value="${freightAmount}"/></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="contenthead">
                                    <td>New Freight</td>
                                    <td>
                                        <table  border="0" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr  class="text1">
                                                <td>Weight&nbsp;*&nbsp;Rate</td>
                                                <td><input type="text" name="weight" id="weight" value="<c:out value="${totalWeight}"/>" onkeyup="calculateWeightFreight();"/>
                                                    &nbsp;*&nbsp;
                                                    <input type="text" name="ratePerKg" id="ratePerKg" value="<c:out value="${ratePerKm}"/>" onkeyup="calculateWeightFreight();"/>
                                                    </td>
                                            </tr>
                                            <tr  class="text2">
                                                <td>Invoice Amount</td>
                                                <td><input type="text" name="billedAmountPointToPointWeight" id="billedAmountPointToPointWeight" value="<c:out value="${freightAmount}"/>" onkeypress="return onKeyPressBlockCharacters(event)" onpaste="return false;"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </c:if>

                    </div>

                    <script type="text/javascript">
                        function calculateWeightFreight() {
                            var weight = $("#weight").val();
                            var ratePerKg = $("#ratePerKg").val();
                            var billedAmountPointToPointWeight = $("#billedAmountPointToPointWeight").val();
                            if (billedAmountPointToPointWeight == '') {
                                billedAmountPointToPointWeight = 0;
                            }
                            var res = parseFloat(weight) * parseFloat(ratePerKg);
                           // $("#billedAmountPointToPointWeight").val(parseFloat(res)).toFixed(2);
                            document.getElementById('billedAmountPointToPointWeight').value = parseFloat(res).toFixed(2);
                        }
                        function calculateDiscount() {
                            var discountFreight = $("#discountFreight").val();
                            var discountAmount = $("#discountAmount").val();
                            if (discountAmount == '') {
                                discountAmount = 0;
                            }
                            var res = parseFloat(discountFreight) - parseFloat(discountAmount);
                           // $("#billedAmount").val(parseFloat(res)).toFixed(2);
                            document.getElementById('billedAmount').value = parseFloat(res).toFixed(2);
                        }
                        function showDiv(val) {
                            if (val == 1) {
                                $("#discountDiv").show();
                                $("#changeFreightDiv").hide();
                            } else if (val == 2) {
                                $("#discountDiv").hide();
                                $("#changeFreightDiv").show();
                            }
                        }
                        
                        function updateInvoiceAmount(tripId,invoiceId){
                            var editMode = $("#editMode").val();
                            if(editMode == 1){
                                var discountAmount = $("#discountAmount").val();
                                var billedAmount = $("#billedAmount").val();
                                if(discountAmount == ''){
                                    alert("Please enter the discount amount");
                                    $("#discountAmount").focus();
                                }else{
                                    document.viewbillGeneration.action = "/throttle/updateInvoiceAmount.do?invoiceId=" + invoiceId + "&tripId=" + tripId;
                                    document.viewbillGeneration.submit();    
                                }
                            }else if(editMode == 2){
                                    document.viewbillGeneration.action = "/throttle/updateInvoiceAmount.do?invoiceId=" + invoiceId + "&tripId=" + tripId;
                                    document.viewbillGeneration.submit();    
                            }
                        }
                    </script>
                    <center>
                        <input type="button" class="button"  value="Save" onClick="updateInvoiceAmount(<c:out value="${tripId}"/>,<c:out value="${invoiceId}"/>);" >
                    </center>
                </div>
            </div>
            <script>
                $(".nexttab").click(function() {
                    var selected = $("#tabs").tabs("option", "selected");
                    $("#tabs").tabs("option", "selected", selected + 1);
                });
            </script>
        </form>
    </body>
</html>