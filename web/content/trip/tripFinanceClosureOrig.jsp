<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String todayDate = sdf.format(today);
        %>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



        </script>
        <script type="text/javascript" language="javascript">
            function submitPage(){
                document.tripExpense.action = '/throttle/saveTripOtherExpense.do';
                document.tripExpense.submit();
            }


            function setExpenseTypeDetails(value){
                document.getElementById('marginValue').readOnly = true;
                if(value == '1'){//bill to customer
                    document.getElementById('taxPercentage').value = '';
                    document.getElementById('expenses').value = '';
                    document.getElementById('billModeTemp').disabled = false;
                    document.getElementById('taxPercentage').readOnly = false;
                }
                else{ // do not bill to customer
                    document.getElementById('billMode').value = 0;
                    document.getElementById('billModeTemp').value = 0;
                    document.getElementById('taxPercentage').value = 0;
                    document.getElementById('taxPercentage').readOnly = true;

                    document.getElementById('billModeTemp').disabled = true;

                    document.getElementById('marginValue').value = 0;

                    document.getElementById('expenses').value = '0';
                    document.getElementById('netExpense').value = '0';
                }
            }
            function setBillMode(){
                document.getElementById('billMode').value = document.getElementById('billModeTemp').value;
                var billModeValue = document.getElementById('billModeTemp').value;
                document.getElementById('marginValue').value = 0;
                if(billModeValue == '2'){//Not pass thru
                    document.getElementById('marginValue').readOnly = false;
                }else {
                    document.getElementById('marginValue').readOnly = true;
                }
            }

            function calTotalExpenses(){
                //                var tax = document.getElementById('taxPercentage'+sno).value;
                var expenseAmount = document.getElementById('expenses').value;
                //                var totalAmount = tax / 100 * expenseAmount;
                //                var netAmount =  Math.round(parseFloat(totalAmount).toFixed(0))  + Math.round(parseFloat(expenseAmount).toFixed(0))   ;
                //                document.getElementById('netExpense'+sno).value = netAmount;
                document.getElementById('netExpense').value = expenseAmount;
            }

            function totalNetAmount(){
                var tax = document.getElementsByNames("").value;
                var expenseAmount = document.getElementById('expenses').value;
                var totalAmount = tax / 100 * expenseAmount;
                var netAmount =  Math.round(parseFloat(totalAmount).toFixed(0))  + Math.round(parseFloat(expenseAmount).toFixed(0))   ;
                document.getElementById('netExpense').value = netAmount;
            }

            function openPopup1(tripExpenseId){
                var url = '/throttle/viewExpensePODDetails.do?tripExpenseId='+tripExpenseId;
                window.open( url, 'PopupPage', 'height=500,width=700,scrollbars=yes,resizable=yes');
            }




        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
    </head>
    <body >
        <form name="tripExpense" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                        <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                            <tr id="exp_table" >
                                                <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                <td> <c:out value="${trip.orderRevenue}" /></td>

                                            </tr>
                                            <tr id="exp_table" >
                                                <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                <td> <c:out value="${trip.orderExpense}" /></td>

                                            </tr>
                                            <c:set var="profitMargin" value="" />
                                            <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                            <c:set var="orderExpense" value="${trip.orderExpense}" />
                                            <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                            <%
                                                        String profitMarginStr = "" + (Double) pageContext.getAttribute("profitMargin");
                                                        String revenueStr = "" + (String) pageContext.getAttribute("orderRevenue");
                                                        float profitPercentage = 0.00F;
                                                        if (!"".equals(revenueStr) && !"".equals(profitMarginStr)) {
                                                            profitPercentage = Float.parseFloat(profitMarginStr) * 100 / Float.parseFloat(revenueStr);
                                                        }


                                            %>
                                            <tr id="exp_table" >
                                                <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                                    <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                <td>

                                                <td>
                                            </tr>
                                        </table>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>


            <br>
            <br>
            <br>
            <br>
            <table width="100%">
                <% int loopCntr = 0;%>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <% if (loopCntr == 0) {%>
                        <tr>
                            <td class="contenthead" >Trip Code: <c:out value="${trip.tripCode}"/></td>
                            <td class="contenthead" >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                            <td class="contenthead" >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                            <td class="contenthead" >Status: <c:out value="${trip.status}"/></td>
                        </tr>
                        <% }%>
                        <% loopCntr++;%>
                    </c:forEach>
                </c:if>
            </table>
            <div id="tabs" >
                <ul>

                    <li><a href="#otherExpDetail"><span>Other Expenses</span></a></li>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>
                    <li><a href="#startDetail"><span>Trip Start Details</span></a></li>
                    <li><a href="#endDetail"><span>Trip End Details</span></a></li>
                    <li><a href="#statusDetail"><span>Status History</span></a></li>
                    <!--
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                    <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                    <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>

                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                                <tr>
                                    <!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                                                                <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                    <td class="text1">Consignment No(s)</td>
                                    <td class="text1">
                                        <c:out value="${trip.cNotes}" />
                                    </td>
                                    <td class="text1">Billing Type</td>
                                    <td class="text1">
                                        <c:out value="${trip.billingType}" />
                                    </td>
                                </tr>
                                <tr>
                                    <!--                            <td class="text2">Customer Code</td>
                                                                <td class="text2">BF00001</td>-->
                                    <td class="text2">Customer Name</td>
                                    <td class="text2">
                                        <c:out value="${trip.customerName}" />
                                        <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${trip.customerName}" />'>
                                        <input type="hidden" name="tripSheetId" Id="tripSheetId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                    </td>
                                    <td class="text2">Customer Type</td>
                                    <td class="text2" colspan="3" >
                                        <c:out value="${trip.customerType}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text1">Route Name</td>
                                    <td class="text1">
                                        <c:out value="${trip.routeInfo}" />
                                    </td>
                                    <!--                            <td class="text1">Route Code</td>
                                                                <td class="text1" >DL001</td>-->
                                    <td class="text1">Reefer Required</td>
                                    <td class="text1" >
                                        <c:out value="${trip.reeferRequired}" />
                                    </td>
                                    <td class="text1">Order Est Weight (MT)</td>
                                    <td class="text1" >
                                        <c:out value="${trip.totalWeight}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text2">Vehicle Type</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleTypeName}" />
                                    </td>
                                    <td class="text2">Vehicle No</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleNo}" />

                                    </td>
                                    <td class="text2">Vehicle Capacity (MT)</td>
                                    <td class="text2">
                                        <c:out value="${trip.vehicleTonnage}" />

                                    </td>
                                </tr>

                                <tr>
                                    <td class="text1">Veh. Cap [Util%]</td>
                                    <td class="text1">
                                        <c:out value="${trip.vehicleCapUtil}" />
                                    </td>
                                    <td class="text1">Special Instruction</td>
                                    <td class="text1">-</td>
                                    <td class="text1">Trip Schedule</td>
                                    <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                                </tr>


                                <tr>
                                    <td class="text2">Driver </td>
                                    <td class="text2" colspan="5" >
                                        <c:out value="${trip.driverName}" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text1">Product Info </td>
                                    <td class="text1" colspan="5" >
                                        <c:out value="${trip.productInfo}" />
                                    </td>

                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br/>
                    <br/>

                    <c:if test = "${expiryDateDetails != null}" >
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                            </tr>
                            <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                <tr>
                                    <td class="text2">Vehicle FC Valid UpTo</td>
                                    <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                </tr>
                                <tr>
                                    <td class="text1">Vehicle Insurance Valid UpTo</td>
                                    <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                </tr>
                                <tr>
                                    <td class="text2">Vehicle Permit Valid UpTo</td>
                                    <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                </tr>
                                <tr>
                                    <td class="text2">Road Tax Valid UpTo</td>
                                    <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>

                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
                <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                            </tr>
                            <% int index2 = 1;%>
                            <c:forEach items="${tripPointDetails}" var="tripPoint">
                                <%
                                            String classText1 = "";
                                            int oddEven = index2 % 2;
                                            if (oddEven > 0) {
                                                classText1 = "text1";
                                            } else {
                                                classText1 = "text2";
                                            }
                                %>
                                <tr >
                                    <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                 <div id="preStart">

                    <c:if test = "${tripPreStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                            <tr>
                            <td class="contenthead" colspan="4" >Trip Pre Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Pre Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                              <td class="text1" height="30" >Trip Pre Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                            </tr>
                              <tr>
                                <td class="text2" height="30" >Trip Pre Start Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                            <c:if test = "${tripDetails != null}" >
                                    <td class="text2">Trip Pre Start Location / Distance</td>
                                    <c:forEach items="${tripDetails}" var="trip">
                            <td class="text2"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                    </c:forEach>
                            </c:if>
                            </tr>
                            <tr>
                                    <td class="text1" height="30" >Trip Pre Start Remarks</td>
                                    <td class="text1" height="30" ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                            </tr>
                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                 <div id="startDetail">
                    <c:if test = "${tripStartDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripStartDetails}" var="startDetails">
                           <tr>
                            <td class="contenthead" colspan="6" > Trip Start Details</td>
                        </tr>
                                <tr >
                                <td class="text1" height="30" >Trip Planned Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Actual Start Date</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                <td class="text1" height="30" >Trip Planned Start Time</td>
                                    <td class="text1" height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text2" height="30" >Trip Actual Start Time</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                    <td class="text2" height="30" >Trip Start Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                <td class="text2" height="30" >Trip Start Reefer Reading(HM)</td>
                                    <td class="text2" height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                            </tr>

                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                 <div id="statusDetail">
                    <% int index1 = 1; %>

                    <c:if test = "${statusDetails != null}" >
                        <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Status Name</td>
                                <td class="contenthead" height="30" >Remarks</td>
                                <td class="contenthead" height="30" >Created User Name</td>
                                <td class="contenthead" height="30" >Created Date</td>
                            </tr>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                 <%
                                        String classText = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                                <tr >
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>
                <div id="endDetail">
                    <c:if test = "${tripEndDetails != null}" >
                        <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <c:forEach items="${tripEndDetails}" var="endDetails">
                                <tr>
                                    <td class="contenthead" colspan="6" > Trip End Details</td>
                                </tr>
                                <tr>
                                    <td class="text1" height="30" >Trip Planned End Date</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                    <td class="text1" height="30" >Trip Actual End Date</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.endDate}" /></td>
                                    <td class="text1" height="30" >Trip Planned End Time</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                </tr>
                                <tr>
                                    <td class="text2" height="30" >Trip Actual End Time</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endTime}" /></td>
                                    <td class="text2" height="30" >Trip End Odometer Reading(KM)</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                    <td class="text2" height="30" >Trip End Reefer Reading(HM)</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.endHM}" /></td>
                                </tr>
                                <tr>
                                    <td class="text1" height="30" >Total Odometer Reading(KM)</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                    <td class="text1" height="30" >Total Reefer Reading(HM)</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                    <td class="text1" height="30" >Total Duration Hours</td>
                                    <td class="text1" height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                </tr>
                                <tr>
                                    <td class="text2" height="30" >Total Days</td>
                                    <td class="text2" height="30" ><c:out value="${endDetails.totalDays}" />
                                    <td class="text2" height="30" colspan ="4"></td>
                                </tr>

                            </c:forEach >
                        </table>
                    </c:if>
                    <br>
                    <br>
                </div>

                <div id="otherExpDetail" >
                    <div style="border: #ffffff solid" >
                        <%int count = 0;%>
                        <input type="hidden" name="count" id="count" value="<c:out value="${otherExpenseDetailsSize}"/>"/>
                        <c:if test="${otherExpenseDetails != null}">
                            <h4>Expense Details</h4>
                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <th width="50" class="contenthead">S No&nbsp;</th>
                                    <th class="contenthead">Expense Name</th>
                                    <th class="contenthead">Driver Name</th>
                                    <th class="contenthead">Expense Date</th>
                                    <th class="contenthead">Expense Remarks</th>
                                    <th class="contenthead">Expense Type</th>
                                    <th class="contenthead">Bill Mode</th>
                                    <th class="contenthead">Margin Value</th>
                                    <th class="contenthead">Applicable Tax Percentage</th>
                                    <th class="contenthead">Expense Amount</th>
                                    <th class="contenthead">Total Expenses</th>
                                    <th class="contenthead">Upload Bill Copy</th>
                                    <th class="contenthead">Edit</th>
                                </tr>
                                <% int index5 = 1;%>
                                <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                    <%
                                                String classText5 = "";
                                                int oddEven3 = index5 % 2;
                                                if (oddEven3 > 0) {
                                                    classText5 = "text1";
                                                } else {
                                                    classText5 = "text2";
                                                }
                                    %>
                                    <%count++;%>
                                    <tr>
                                        <td class="<%=classText5%>" ><%=index5++%></td>
                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseName}"/>&nbsp;</td>
                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.employeeName}"/>&nbsp;</td>
                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseDate}"/>&nbsp;</td>
                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseRemarks}"/>&nbsp;</td>
                                        <c:if test = "${expenseDetails.expenseType == '1'}" >
                                            <td class="<%=classText5%>" >Bill To Customer </td>
                                        </c:if>
                                        <c:if test = "${expenseDetails.expenseType == '2'}" >
                                            <td class="<%=classText5%>" >Do Not Bill To Customer </td>
                                        </c:if>
                                        <c:if test = "${expenseDetails.expenseType == '0'}" >
                                            <td class="<%=classText5%>" >&nbsp; </td>
                                        </c:if>
                                        <c:if test = "${expenseDetails.passThroughStatus == '1'}" >
                                            <td class="<%=classText5%>" >Pass Through </td>
                                        </c:if>
                                        <c:if test = "${expenseDetails.passThroughStatus == '2'}" >
                                            <td class="<%=classText5%>" > No Pass Through</td>
                                        </c:if>
                                        <c:if test = "${expenseDetails.passThroughStatus == '0'}" >
                                            <td class="<%=classText5%>" >&nbsp;</td>
                                        </c:if>
                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.marginValue}"/>&nbsp;</td>
                                        <td class="<%=classText5%>" >
                                            <c:if test="${expenseDetails.totalExpenseAmount != null}">
                                                <c:out value="${expenseDetails.totalExpenseAmount}"/>
                                            </c:if>
                                            <c:if test="${expenseDetails.totalExpenseAmount == null}">
                                                &nbsp;
                                            </c:if>
                                        </td>

                                    <td class="<%=classText5%>" ><a href="#" onclick="openPopup1('<c:out value="${expenseDetails.tripExpenseId}" />');" >UploadBillCopy</a>
                                    <td class="<%=classText5%>" height="30"><input type="checkbox" name="edit" value="" id="edit<%=count%>" onclick="modifyExpenseDetails(<%=count%>)"/>
                                        <input type="hidden" name="expenseId" id="expenseId" value="<c:out value="${expenseDetails.tripExpenseId}"/>"/>
                                        <input type="hidden" name="employeeNames" id="employeeNames<%=count%>" value="<c:out value="${expenseDetails.employeeId}"/>"/>
                                        <input type="hidden" name="expenseNames" id="expenseNames<%=count%>" value="<c:out value="${expenseDetails.expenseId}"/>"/>
                                        <input type="hidden" name="expenseDates" id="expenseDates<%=count%>" value="<c:out value="${expenseDetails.expenseDate}"/>"/>
                                        <input type="hidden" name="expenseRemarkss" id="expenseRemarkss<%=count%>" value="<c:out value="${expenseDetails.expenseRemarks}"/>"/>
                                        <input type="hidden" name="expenseTypes" id="expenseTypes<%=count%>" value="<c:out value="${expenseDetails.expenseType}"/>"/>
                                        <input type="hidden" name="passThroughStatuss" id="passThroughStatuss<%=count%>" value="<c:out value="${expenseDetails.passThroughStatus}"/>"/>
                                        <input type="hidden" name="taxPercentages" id="taxPercentages<%=count%>" value="<c:out value="${expenseDetails.taxPercentage}"/>"/>
                                        <input type="hidden" name="marginValues" id="marginValues<%=count%>" value="<c:out value="${expenseDetails.marginValue}"/>"/>
                                        <input type="hidden" name="totalExpenseAmounts" id="totalExpenseAmounts<%=count%>" value="<c:out value="${expenseDetails.expenseValue}"/>"/></td>
                                    </tr>
                                </c:forEach>

                            </table>
                        </c:if>
                        <br>
                        <script>
                                        function modifyExpenseDetails(sno){
                                            //alert("hi");
                                            var count = parseInt(document.getElementById("count").value);
                                            for (var i = 1; i <= count; i++) {
                                                if(i != sno) {
                                                    document.getElementById("edit"+i).checked = false;
                                                } else {
                                                    document.getElementById("edit"+i).checked = true;
                                                    document.getElementById('expenseName').value=document.getElementById("expenseNames"+i).value;
                                                    document.getElementById('employeeName').value=document.getElementById("employeeNames"+i).value;
                                                    document.getElementById('expenseDate').value=document.getElementById("expenseDates"+i).value;
                                                    document.getElementById('expenseRemarks').value=document.getElementById("expenseRemarkss"+i).value;
                                                    document.getElementById('expenseType').value=document.getElementById("expenseTypes"+i).value;
                                                    document.getElementById('billModeTemp').value=document.getElementById("passThroughStatuss"+i).value;
                                                    document.getElementById('taxPercentage').value=document.getElementById("taxPercentages"+i).value;
                                                    document.getElementById('marginValue').value=document.getElementById("marginValues"+i).value;
                                                    document.getElementById('netExpense').value=document.getElementById("totalExpenseAmounts"+i).value;
                                                }
                                            }
                                        }
                                    </script>
                        <table class="border" width="100%" border="0" cellpadding="0"  id="suppExpenseTBL" >
                            <tr >
                                <th class="contenthead"><font color='red'>*</font>Expense Name</th>
                                <th class="contenthead"><font color='red'>*</font>Driver Name</th>
                                <th class="contenthead"><font color='red'>*</font>Expense Date</th>
                                <th class="contenthead">Expense Remarks</th>
                                <th class="contenthead"><font color='red'>*</font>Expense Type</th>
                                <th class="contenthead">Bill Mode</th>
                                <th class="contenthead">Margin Value</th>
                                <th class="contenthead">Applicable Tax Percentage</th>
                                <th class="contenthead"><font color='red'>*</font>Expense Value</th>
                                <th class="contenthead">Total Expenses</th>
                            </tr>
                            <tr>

                                <td class="text1"><select class="textbox" id="expenseName"   name="expenseName"><option selected value="0">-select-</option><c:if test = "${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"><option  value="<c:out value="${expense.expenseId}" />"><c:out value="${expense.expenseName}" /> </c:forEach ></c:if> </select></td>
                                <td class="text1" height="25" >
                                    <select class="textbox" id="employeeName" style="width:125px"  name="employeeName" >
                                        <option selected value=0>---Select---</option>
                                        <c:if test = "${driverNameDetails != null}" >
                                            <c:forEach items="${driverNameDetails}" var="driverName">
                                                <option  value="<c:out value="${driverName.employeeId}" />"><c:out value="${driverName.employeeName}" />
                                                </c:forEach ></c:if>
                                        </select></td>
                                    <td class="text1"><input type="text"  name="expenseDate" id="expenseDate" class="datepicker" value="<%=todayDate%>" ></td>
                                <td class="text1" height="25" ><textarea rows="3" cols="15" class="textbox" name="expenseRemarks" id="expenseRemarks"></textarea></td>
                                <td class="text1"><select class="textbox" id="expenseType"   name="expenseType" onchange="setExpenseTypeDetails(this.value)"><option value="0">-Select-</option><option value="1">Bill To Customer</option><option value="2">Do Not Bill Customer</option></select></td>
                                <td class="text1"><input type="hidden"  name="billMode" id="billMode" value = "0" ><select disabled class="textbox" id="billModeTemp"   name="billModeTemp" onchange="setBillMode()"><option value="0">-Select-</option><option value="1">Pass Through</option><option value="2">No Pass Through</option></select></td>
                                <td class="text1" height="30" ><input type="text"  name="marginValue" id="marginValue"  onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value = "" ></td>
                                <td class="text1" height="30" ><input type="text"  name="taxPercentage" id="taxPercentage"  onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value = "" ></td>
                                <td class="text1" height="10" ><input type="text"  name="expenses" id="expenses" class="textbox" value=""  onchange="calTotalExpenses()"></td>
                                <td class="text1" height="10" ><input readonly type="text" name="netExpense" id="netExpense" onKeyPress="return onKeyPressBlockCharacters(event);"   value="" ></td>
                            </tr>
                        </table>

                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Save " name="Save expenses" onclick="submitPage();"/>
                        </center>
                        <br/>
                        <br/>
                    </div>

                </div>

                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>

            </div>
        </form>
    </body>
</html>