<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>
<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<style>
    #rcorners1 {
        border-radius: 35px;
        background: url(paper.gif);
        background-position: left top;
        background-repeat: repeat;
        padding: 5px; 
        width: 400px;
        height: 150px;
    }
</style>

<script>
            function initMap() {
                var service = new google.maps.DirectionsService;
                var start = new google.maps.LatLng("22.81434", "86.18916");
                var mapOptions = {
                    zoom: 7

                };
                var map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
                var routePoint = document.getElementsByName("locId");
                var latitude = document.getElementsByName("latitude");
                var longitude = document.getElementsByName("longitude");
                var w = routePoint.length;
                
                // list of points
                var stations = [];
                var bounds = [];

                bounds.push({
                    lat: "22.81434", lng: "86.18916"
                });

                for (var j = 0; j < w; j++) {

                    stations.push({
                        lat: latitude[j].value, lng: longitude[j].value, name: routePoint[j].value
                    });
                    bounds.push({
                        lat: latitude[j].value, lng: longitude[j].value
                    });
                }

                // Zoom and center map automatically by stations (each station will be in visible map area)
                var lngs = stations.map(function(station) {
                    return station.lng;
                });
                var lats = stations.map(function(station) {
                    return station.lat;
                });
                var lngss = bounds.map(function(station) {
                    return station.lng;
                });
                var latss = bounds.map(function(station) {
                    return station.lat;
                });

                map.fitBounds({
                    west: Math.min.apply(null, lngss),
                    east: Math.max.apply(null, lngss),
                    north: Math.min.apply(null, latss),
                    south: Math.max.apply(null, latss),
                });


                var marker = start;




                var image = '/throttle/images/warehouse.png';
                var beachMarker = new google.maps.Marker({
                    map: map,
                    position: marker,
                    animation: google.maps.Animation.DROP,
                    title: "OWM ",
                    icon: image

                });

                // Show stations on the map as markers
                for (var i = 0; i < stations.length; i++) {

                    var marker = new google.maps.LatLng(stations[i].lat, stations[i].lng);

                    //            image = '/throttle/images/wowPIC.jpg';

                    var image = '/throttle/images/truck.png';
                    var beachMarker = new google.maps.Marker({
                        map: map,
                        position: marker,
                        animation: google.maps.Animation.DROP,
                        title: stations[i].name,
                        icon: image
                    });
                }

                // Divide route to several parts because max stations limit is 25 (23 waypoints + 1 origin + 1 destination)
                for (var i = 0, parts = [], max = 25 - 1; i < stations.length; i = i + max)
                    parts.push(stations.slice(i, i + max + 1));
                // Service callback to process service results
                var service_callback = function(response, status) {
                    if (status != 'OK') {
                        console.log('Directions request failed due to  ' + status);
                        return;
                    }
                    var renderer = new google.maps.DirectionsRenderer;
                    renderer.setMap(map);
                    renderer.setOptions({suppressMarkers: true, preserveViewport: true, polylineOptions: {
                            strokeColor: 'black', strokeWeight: 3, strokeOpacity: 1.0
                        }});
                    renderer.setDirections(response);
                    //            computeTotalDistance(response);

                };
                //        alert("Parts===" + parts.length);
                // Send requests to service to get route (for stations count <= 25 only one request will be sent)
                for (var i = 0; i < parts.length; i++) {
                    // Waypoints does not include first station (origin) and last station (destination)
                    var waypoints = [];
                    for (var j = 0; j < parts[i].length; j++) {
                        //                waypoints.push({location: parts[i][j], stopover: false});
                        waypoints.push({location: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng), stopover: false});
                        // Service options
                        var service_options = {
                            origin: start,
                            destination: new google.maps.LatLng(parts[i][j].lat, parts[i][j].lng),
                            optimizeWaypoints: true,
                            travelMode: google.maps.TravelMode.DRIVING
                        };

                        // Send request
                        service.route(service_options, service_callback);
                    }
                }

            }


</script>



</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>
    function submitPageForTripGeneration() {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');

        var tripType = $("#tripType").val();
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }

        if (cntr > 0) {
            $("#Confirm").hide();
            document.CNoteSearch.action = "/throttle/saveTripSheet.do?consignmentOrderNos=" + temp + "&tripType=" + tripType;
            document.CNoteSearch.submit();

        } else {
            alert("Please select consignment order and proceed");
        }
    }



    function searchPage(val) {
        document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + val + "&consignmentOrderIds=";
        document.CNoteSearch.submit();
    }

</script>

<body>
    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" >
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                    <li class=""><spring:message code="hrms.label.RunSheet Planning" text="RunSheet Planning"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" style="width:70%" >
                        <thead><tr><th colspan="4">CONSIGNMENT ORDERS</th></tr></thead>
                        <tr style="display:none;">
                            <td >Customer Name</td>
                            <td>
                                <input type="hidden"  name="customerId" id="customerId" style="width:120px;" value="<c:out value="${customerId}"/>"/>
                                <input type="hidden"  name="tripType" id="tripType" style="width:120px;" value="<c:out value="${tripType}"/>"/>
                                <input type="hidden"  name="cbt" id="cbt" value="0"/>
                                <input type="text"  name="customerName" id="customerName" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerName}"/>"/>
                            </td>

                            <td >Customer Order Ref No</td>
                            <td><input type="text"  name="customerOrderReferenceNo" id="customerOrderReferenceNo" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>
                        </tr>
                        <tr style="display:none;">
                            <td >Consignment Order No</td>
                            <td><input type="text"  name="consignmentOrderReferenceNo" id="consignmentOrderReferenceNo" class="form-control" style="width:250px;height:40px" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                            <td>Status</td>
                            <td><select name="status"  id="status1"  class="form-control" style="width:250px;height:40px" >
                                    <option value="">--Select--</option>
                                    <option value="5">Order Created</option>
                                    <option value="4">Order Cancelled</option>
                                    <option value="3">Order On Hold</option>
                                </select>
                                <script>
                                    document.getElementById("status").value = '<c:out value="${status}"/>';
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Order Date </td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>" autocomplete="off"></td>
                            <td></td>
                            <td height="30">
                                <input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')">
                                <input name="toDate" id="toDate" type="hidden" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <c:set var="totalQty" value="${0}"/>
                    <c:set var="totalCBM" value="${0}"/>
                    <c:if test = "${consignmentList != null}" >

                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Order No</th>
                                    <th>Consignment No</th>
                                    <th>Consignment Date</th>                                    
                                    <th>Customer Name </th>
                                    <th>Consignor </th>
                                    <th>Consignee </th>                                                                        
                                    <th>Qty</th>
                                    <th>Total Weight</th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${consignmentList}" var="cnl">

                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                    <c:out value="${cnl.orderReferenceNo}"/>
                                </td>
                                <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                    <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                                    <div style="display:none;">
                                        <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="<c:out value="${cnl.consignmentOrderId}"/>" checked />
                                    </div>
                                </td>
                                <td align="left" ><c:out value="${cnl.consignmentOrderDate}"/></td>                                        
                                <td align="left" ><c:out value="${cnl.customerName}"/></td>
                                <td align="left" ><c:out value="${cnl.consigmentDestination}"/>
                                    <input type="hidden" style="width: 184px;" name="latitude" id="latitude<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.latitude}"/>"  readOnly maxlength="50">
                                    <input type="hidden" style="width: 184px;" name="longitude" id="longitude<%=index%>"   value="<c:out value="${cnl.longitude}"/>" readOnly class="form-control" style="width:250px;height:40px" >
                                    <input type="hidden" style="width: 184px;" name="locId" id="locId<%=index%>"   value="<c:out value="${cnl.consigmentDestination}"/>" readOnly class="form-control" style="width:250px;height:40px" >
                                </td> 
                                <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>

                                <td align="left" >                                    
                                    <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${cnl.totalPackages}" />  
                                    <c:set var="totalQty" value="${totalQty+cnl.totalPackages}"/>
                                </td>
                                <td align="left" >
                                  <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${cnl.totalWeight}" />  
                                    <c:set var="totalCBM" value="${totalCBM+cnl.totalWeight}"/>
                                </td>

                                </tr>
                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <c:if test = "${consignmentListSize > 0}" >
                        <table>
                            <tr valign="top">
                                <td>
                                    <table align="left">
                                        <div id="mapdiv"   style="width:800px; height:400px"></div></table>

                                </td>
                                <td>&nbsp;&nbsp;
                                </td>
                                <td>

                                    <table id="rcorners1" align="right" style="background-color:#AAE4F1" style="width:80%;height:40px" >
                                        <tr >
                                            <td colspan="4" align="center"><b><u>Summary</u></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b>Total Orders
                                                    </b></font>
                                            </td>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b> <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${consignmentListSize}" /> No(s)</b></font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b>Total Quantity
                                                    </b></font>
                                            </td>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalQty}" />  No(s)
                                                    </b></font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b> Total Weight
                                                    </b></font>
                                            </td>
                                            <td  style="text-align: center" >
                                                <font color="#380099"><b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalCBM}" /> KG
                                                    </b></font>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <center>
                            <input type="button" class="btn btn-success" name="trip" id="Confirm" value="Proceed" onclick="submitPageForTripGeneration()" />
                        </center>
                    </c:if>
                    </form>
                    </body>
                </div>
            </div>
        </div>
        <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&callback=initMap"></script>
        <%@ include file="../common/NewDesign/settings.jsp" %>