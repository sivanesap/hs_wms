<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->

         <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>

         
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script language="">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityFromName.do",
                            dataType: "json",
                            data: {
                                cityFrom: request.term,
                                cityToId: document.getElementById('cityToId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if (items == '') {
                                    $('#cityFromId').val('');
                                    $('#cityFrom').val('');
                                    $('#cityToId').val('');
                                    $('#cityTo').val('');
                                } else {
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        $('#cityTo').focus();
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                $('#cityTo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityToName.do",
                            dataType: "json",
                            data: {
                                cityTo: request.term,
                                cityFromId: document.getElementById('cityFromId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if (items == '') {
                                    $('#cityToId').val('');
                                    $('#cityTo').val('');
                                } else {
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityToId').val(tmp[0]);
                        $('#cityTo').val(tmp[1]);
                        $('#emptyTripRemarks').focus();
                        fetchRouteInfo();
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                }

            });


            var httpRequest;
            function checkRouteCode() {
                var cityFromId = document.getElementById('cityFromId').value;
                var cityToId = document.getElementById('cityToId').value;
                if (cityFromId != '' && cityToId != '') {
                    var url = '/throttle/checkRouteDetails.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
                    if (window.ActiveXObject) {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } else if (window.XMLHttpRequest) {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() {
                        processRequest();
                    };
                    httpRequest.send(null);
                }
            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        $("#showHideRouteStatus").show();
                        if (val != "" && val != 'null') {
                            $("#routeStatus").text('Route Exists Code is :' + val);
                        } else {
                            $("#routeStatus").text('Route Does Not Exists');
                            $("#showHideRouteKm").show();
                            $("#showHideRouteExpense").show();
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }



            var httpReq;
            var temp = "";
            function fetchRouteInfo() {
                var originId = document.getElementById('cityFromId').value;
                var destinationId = document.getElementById('cityToId').value;
                var vehicleTypeId = document.getElementById('vehicleTypeId').value;
                if (originId != '') {
                    var url = "/throttle/checkPreStartRoute.do?preStartLocationId=" + originId + "&originId=" + destinationId + "&vehicleTypeId=" + vehicleTypeId;

                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() {
                        processFetchRouteCheck();
                    };
                    httpReq.send(null);
                }

            }

            function processFetchRouteCheck()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        //alert(temp);
                        if (temp != '' && temp != null && temp != 'null') {
                            var tempVal = temp.split('-');
                            $("#totalKm").val(tempVal[0]);
                            $("#totalHours").val(tempVal[1]);
                            $("#totalMinutes").val(tempVal[2]);
                            $("#expense").val(tempVal[3]);
                            document.getElementById("totalKm").readOnly = true;
                            document.getElementById("totalHours").readOnly = true;
                            document.getElementById("totalMinutes").readOnly = true;
                            document.getElementById("expense").readOnly = true;
                            $("#showHideRouteStatus").hide();
                            $("#routeStatus").text('');
                        } else {
                            $("#showHideRouteStatus").show();
                            $("#routeStatus").text('Route Does Not Exists');
                            document.getElementById("totalKm").readOnly = true;
                            document.getElementById("totalHours").readOnly = true;
                            document.getElementById("totalMinutes").readOnly = true;
                            document.getElementById("expense").readOnly = true;
                        }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }
            //end ajax for pre location

            function submitPage() {
                    if ($("#routeStatus").text() != '')  {
                        alert("Please Check " + $("#routeStatus").text());
                        return;
                    }else if (document.getElementById('cityFromId').value == '' || document.getElementById('cityFrom').value == '') {
                        alert("choose the from location");
                        document.getElementById('cityFrom').focus();
                        return;
//                    }else if (document.getElementById('cityFromId').value == '' || document.getElementById('cityFrom').value != '') {
//                        alert("Invalid from location");
//                        document.getElementById('cityFrom').focus();
//                        return;
                    } else if (document.getElementById('cityToId').value == '' || document.getElementById('cityTo').value == '') {
                        alert("choose the to location");
                        document.getElementById('cityTo').focus();
                        return;
//                    } else if (document.getElementById('cityToId').value == '' || document.getElementById('cityTo').value != '') {
//                        alert("Invalid the to location");
//                        document.getElementById('cityTo').focus();
//                        return;
                    } else if (document.getElementById('totalKm').value == '') {
                        alert("Please Enter total km");
                        document.getElementById('totalKm').focus();
                        return;
                    } else if (document.getElementById('totalHours').value == '') {
                        alert("Please Enter total hours");
                        document.getElementById('totalHours').focus();
                        return;
                    } else if (document.getElementById('expense').value == '') {
                        alert("Please Enter trip expense");
                        document.getElementById('expense').focus();
                        return;
                    } else {
                        $("#save").hide();
                        document.trip.action = "/throttle/updateEmptyTripRouteDetails.do";
                        document.trip.submit();
                    }
                }


        </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label." text=""/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label." text=" "/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


    <body>

        <form name="trip" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@include file="/content/common/message.jsp" %>
            <br>


            <div id="tabs" >
                <ul>

                    <li><a href="#rpouteDetailNew"><span>New Route Details</span></a></li>
                    <li><a href="#routeDetailNow"><span>Route Details Now</span></a></li>
                </ul>
            
            <div id="rpouteDetailNew">
                <table  class="table table-info mb30 table-hover" id="bg">
                    <thead><tr>
                    <th  colspan="4" >Trip Details</th>
                        </tr></thead>
                <tr>
                    <td >Trip Start Date</td>
                    <td ><c:out value="${tripStartDate}"/></td>
                    <td >Trip Start Time</td>
                    <td ><c:out value="${tripStartTime}"/></td>
                </tr>

                <tr>
                    <td class="text1">Origin</td>
                    <td class="text1"><input type="hidden" name="origin" Id="cityFromId" class="form-control" style="width:250px;height:40px" value=''>
                        <input type="text" name="cityFrom" Id="cityFrom" class="form-control" style="width:250px;height:40px" value='' onchange="resetCityFromId()">
                    </td>
                    <td class="text1">Destination</td>
                    <td class="text1"><input type="hidden" name="destination" Id="cityToId" class="form-control" style="width:250px;height:40px" value=''>
                        <input type="text" name="cityTo" Id="cityTo" class="form-control" style="width:250px;height:40px" value='' onchange="resetCityToId()"></td>
                </tr>
                <script>
                        function resetCityFromId(){
                            if(document.getElementById("cityFromId").value == ""){
                                $("#cityFrom").val('');
                            }
                        }
                        function resetCityToId(){
                            if(document.getElementById("cityToId").value == ""){
                                $("#cityTo").val('');
                            }
                        }
                </script>

                
                <tr>
                    <td >Travel Km</td>
                    <td ><input type="text" name="totalKm" id="totalKm" class="form-control" style="width:250px;height:40px" value="" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                    <td >Travel Time</td>
                    <td >Hours:<input type="text" name="totalHours" Id="totalHours" class="textbox" style="width:60px;height:40px" value=""   onKeyPress="return onKeyPressBlockCharacters(event);">Minutes:<input type="text" name="totalMinutes" Id="totalMinutes" class="textbox" style="width:60px;height:40px" value=""  style="width: 40px" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                </tr>
                <tr>
                    <td class="text1">Expense</td>
                    <td class="text1"><input type="text" name="expense" Id="expense" class="form-control" style="width:250px;height:40px" value=""  onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                    <td class="text1">Remarks</td>
                    <td><textarea name="emptyTripRemarks" id="emptyTripRemarks" class="form-control" style="width:300px;height:50px"></textarea></td>
                </tr>
                <tr><td colspan="6" align="center"><input type="button" class="btn btn-success" id="save" name="Save" value="Update Empty Trip Route" style="width: 200px" onclick="submitPage()"/></td></tr>
            </table>
            </div>
            <div id="routeDetailNow">
               <table  class="table table-info mb30 table-hover" id="bg">
                   <thead> <tr>
                    <th  colspan="4" >Trip Details</th>
                       </tr></thead>
                
                <tr>
                    <td >Trip Start Date</td>
                    <td ><c:out value="${tripStartDate}"/>
                    <input type="hidden" name="vehicleTypeId" Id="vehicleTypeId" class="form-control" style="width:250px;height:40px" value="<c:out value="${vehicleTypeId}"/>"></td>
                    <td >Trip Start Time</td>
                    <td ><c:out value="${tripStartTime}"/></td>
                </tr>

                <tr>
                    <td class="text1">Trip Code</td>
                    <td class="text1"><input type="hidden" name="tripSheetId" Id="tripSheetId" class="form-control" style="width:250px;height:40px" value="<c:out value="${tripId}"/>"><c:out value="${tripCode}"/></td>
                    <td class="text1">Route Info</td>
                    <td class="text1"><c:out value="${routeInfo}"/></td>
                </tr>

                <tr>
                    <td >Travel Km</td>
                    <td ><c:out value="${totalKm}"/></td>
                    <td >Travel Time</td>
                    <td >Days:<c:out value="${estimatedTransitDays}"/>&nbsp;Hrs:<c:out value="${estimatedTransitHours}"/></td>
                </tr>
                <tr>
                    <td class="text1">Estimated Expense</td>
                    <td class="text1"><c:out value="${estimatedExpense}"/></td>
                    <td class="text1">Estimated Advance Per Day</td>
                    <td class="text1"><c:out value="${estimatedAdvancePerDay}"/></td>
                </tr>
            </table>
            </div>
            </div>
            
        </form>
    </body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>