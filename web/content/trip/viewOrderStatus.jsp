<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>



</head>
<!--<script language="javascript" type="text/javascript">
            setFilterGrid("table");
             function orderStatus(tripSheetId) {
            window.open('/throttle/handleViewOrderStatus.do?tripSheetId=' + tripSheetId +'&value=1', 'PopupPage', 'height = 400, width = 800, scrollbars = yes, resizable = yes');
        }
        </script>-->
<script>
    function searchPage() {
        document.invoiceSearch.action = "/throttle/viewOrderDetails.do";
        document.invoiceSearch.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.View Order Status" text="View Order Status"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.View Order Status" text="View Order Status"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%
                            String menuPath = "Order List >> View / Edit";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="invoiceSearch" method="post">
                    <c:if test = "${runsheetprintList != null}" >
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Created By</th>                                    
                                </tr>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${runsheetprintList}" var="order">

                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" ><c:out value="${order.status}"/> </td>
                                        <td align="left" ><c:out value="${order.createdOn}"/> </td>
                                        <td align="left" ><c:out value="${order.userName}"/> </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <br>
                    <c:if test = "${podList != null}" >
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>
                                    <th>Delivery Type</th>
                                    <th>File</th>
                                    <th>LatLong</th>
                                    <th>Created On</th>
                                    <th>Created By</th>                                    
                                </tr>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${podList}" var="pod">

                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>
                                        <td align="left" ><c:out value="${pod.deliveryType}"/> </td>                                        
                                        <td align="left" >
                                            <a href="<c:out value="${pod.path}"/>" style="color:#ff6699;" target="_blank"><span ><c:out value="${pod.fileName}"/> </span> </a></td> 

                                        <td align="left" ><c:out value="${pod.latLong}"/> </td>
                                        <td align="left" ><c:out value="${pod.createdOn}"/> </td>
                                        <td align="left" ><c:out value="${pod.userName}"/> </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>



                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
