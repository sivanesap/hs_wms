<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function setValuesConsignee(sno, consigneeId, customerId, consigneeName, contactPerson, phoneNo, address1, address2, email, cityId, stateId, stateName, remarks, activeInd, pinCode, customerName) {

        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById('consigneeId').value = consigneeId;
        document.getElementById('customerId').value = customerId;
        document.getElementById('custName').value = customerName;
        document.getElementById('consigneeName').value = consigneeName;
        document.getElementById('contactPerson').value = contactPerson;
        document.getElementById('phoneNo').value = phoneNo;
        document.getElementById('address1').value = address1;
        document.getElementById('address2').value = address2;
        document.getElementById('email').value = email;
        document.getElementById('cityId').value = cityId;
        document.getElementById('stateId').value = stateId;
        document.getElementById('remarks').value = remarks;
        document.getElementById('activeInd').value = activeInd;
        document.getElementById('pinCode').value = pinCode;

    }

    function submitPage()
    {
        //        alert("im in test");
//        var custId=document.getElementById("customerId").value;
//        var custName=document.getElementById("custName").value;
        var errStr = "";
        if (document.getElementById("docketNo").value == "") {
            errStr = "Please enter Docket Number.\n";
            alert(errStr);
            document.getElementById("docketNo").focus();
        } else if (document.getElementById("invoiceNumber").value == "") {
            errStr = "Please select valid Invoice Number.\n";
            alert(errStr);
            document.getElementById("invoiceNumber").focus();
        } else if (document.getElementById("invoiceValue").value == "") {
            errStr = "Please select valid Invoice Value.\n";
            alert(errStr);
            document.getElementById("invoiceValue").focus();
        } else if (document.getElementById("invoiceDate").value == "") {
            errStr = "Please enter Invoice Date.\n";
            alert(errStr);
            document.getElementById("invoiceDate").focus();
        } 
        if (errStr == "") {
            document.touchPoint.action = "/throttle/updateTouctPoind.do";
            document.touchPoint.method = "post";
            document.touchPoint.submit();
        }
    }

</script>

<script type="text/javascript">

    function consigneeNameAllowAlphabet() {
        var field = document.consignee.consigneeName.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.consigneeName.value = "";
            document.consignee.consigneeName.focus();
        }
    }
    function contactPersonAllowAlphabet() {
        var field = document.consignee.contactPerson.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.contactPerson.value = "";
            document.consignee.contactPerson.focus();
        }
    }
    function phoneNoAllowAlphabet() {
        var field = document.consignee.phoneNo.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.phoneNo.value = "";
            document.consignee.phoneNo.focus();
        }
    }
    function consigneeNameAltAllowAlphabet() {
        var field = document.consignee.consigneeNameAlt.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.consigneeNameAlt.value = "";
            document.consignee.consigneeNameAlt.focus();
        }
    }
    function contactPersonAltAllowAlphabet() {
        var field = document.consignee.contactPersonAlt.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.contactPersonAlt.value = "";
            document.consignee.contactPersonAlt.focus();
        }
    }
    function phoneNoAltAllowAlphabet() {
        var field = document.consignee.phoneNoAlt.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.phoneNoAlt.value = "";
            document.consignee.phoneNoAlt.focus();
        }
    }



    var httpRequest;
    function getStateName(cityId) {
        var url = '/throttle/handleStateName.do?cityId=' + cityId;
        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("GET", url, true);

        httpRequest.onreadystatechange = function() {
            processRequest();
        };

        httpRequest.send(null);
    }

    function processRequest() {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                if (httpRequest.responseText.valueOf() != "") {
                    var val = httpRequest.responseText.valueOf();
                    var state = val.split('~');
                    //                            alert(state[0]);
                    //                            alert(state[1]);

                    //                    document.getElementById("stateId").value = state[0];
                    //                    document.getElementById("stateName").value = state[1];
                } else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    }

    function checkConsigneeName(value) {
        var customerId = document.getElementById('custId').value;
        var url = '/throttle/checkConsigneeName.do';
        $.ajax({
            url: url,
            data: {customerId: customerId, consignName: value},
            type: "POST",
            success: function(data)
            {
                if (data > 0) {
                    //                                alert("already Exits");
                    var consigneeName = $('#consigneeName').val();
                    $("#StatusNew").text("ConsingeeName " + consigneeName + " Already Exits");
                    $("#StatusNew").css('color', 'red');
                    $('#consigneeName').val('');
                    $('#consigneeName').focus();
                } else {
                    $("#StatusNew").text("");
                }
                //                            $('#contractSet').html(data);
            }
        });
    }

    //    $(document).ready(function() {
    //        // Use the .autocomplete() method to compile the list based on input from user
    //        $('#custName').autocomplete({
    //            source: function(request, response) {
    //                $.ajax({
    //                    url: "/throttle/getCustomerNameDetails.do",
    //                    dataType: "json",
    //                    data: {
    //                        custName: request.term,
    //                        customerId: document.getElementById('customerId').value
    //                    },
    //                    success: function(data, textStatus, jqXHR) {
    //                        var items = data;
    //                        response(items);
    //                    },
    //                    error: function(data, type) {
    //                        console.log(type);
    //                    }
    //                });
    //            },
    //            minLength: 1,
    //            select: function(event, ui) {
    //                $("#custName").val(ui.item.Name);
    //                var $itemrow = $(this).closest('tr');
    //                var value = ui.item.Name;
    //                var tmp = value.split('-');
    //                $('#customerId').val(tmp[0]);
    //                $('#custName').val(tmp[1]);
    //
    ////                                document.getElementById('custName').readOnly = true;
    //                return false;
    //            }
    //        }).data("ui-autocomplete")._renderItem = function(ul, item) {
    //            var itemVal = item.Name;
    //            var temp = itemVal.split('-');
    //            itemVal = '<font color="green">' + temp[1] + '</font>';
    //            return $("<li></li>")
    //                    .data("item.ui-autocomplete", item)
    //                    .append("<a>" + itemVal + "</a>")
    //                    .appendTo(ul);
    //        };
    //
    //    })
</script>
<body onload="document.consignee.consigneeName.focus();">
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Consignee" text="Invoice Details"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Consignee" text="Invoice Details"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">


                <form name="touchPoint"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>

                            <c:if test = "${getLrNumber != null}" >
                                            <c:forEach items="${getLrNumber}" var="lr">
                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                        <table class="table table-info mb30 table-hover" id="bg" >     
                            <thead>
                                <tr>
                                    <th colspan="4">Invoice Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td ><font color="red">*</font>Docket No&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;</td>
                                <td >
                                    <input type="hidden" name="tripSheetId" id="tripSheetId"  value="<c:out value="${tripSheetId}"/>"  class="form-control" style="width:240px;height:40px" >
                                    <input type="text" name="docketNo" readonly id="docketNo"  value="<c:out value="${lr.lrNumber}" />"  class="form-control" style="width:240px;height:40px" >
                                </td>
                                <td  ><font color="red">*</font>Invoice No</td>
                                <td  >
                                    <input type="text" name="invoiceNumber" id="invoiceNumber"  value=""   class="form-control" style="width:240px;height:40px" >
                                </td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Invoice Value</td>
                                <td >
                                    <input type="text" name="invoiceValue" id="invoiceValue"  value=""  class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:240px;height:40px" >
                                </td>
                                <td  ><font color="red">*</font>Invoice Date</td>
                                <td  >
                                    <input type="text" name="invoiceDate" id="invoiceDate" class="datepicker  form-control" style="width:240px;height:40px" value="" >
                                </td>
                            </tr>

                        </table>
                    </c:forEach>
                    </c:if>
                        <center>
                            <input type="button" class="btn btn-success" name="Save" value="Save" id="save" style="width:100px;height:40px;" onclick="submitPage();" />
                        </center>
                        <br>
                        <br>
                        <br>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>