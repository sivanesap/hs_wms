<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">    
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

     
    function viewConsignmentDetails(orderId) {
            window.open('/throttle/getConsignmentDetails.do?consignmentOrderId='+orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripGeneration() {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //alert(temp);
        //alert(cntr);
        if (cntr > 0) {
            //alert("am here..");
            document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp;
            //alert("am here..1:"+document.CNoteSearch.action);
            document.CNoteSearch.submit();
            //alert("am here..2");
        } else {
            alert("Please select consignment order and proceed");
        }
    }



    function searchPage(val) {
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param="+val;
            document.CNoteSearch.submit();
    }


     function importExcel() {
            document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
            document.CNoteSearch.submit();
    }
    
   
    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }
</script>

<body>
    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
        <%@ include file="/content/common/path.jsp" %>
        <br>
        <br>
        <%--
        <table width="900" height="50" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1;width:850;">CONSIGNMENT ORDERS</li>
                        </ul>
                        <div id="first">
                            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                <tr>
                                    <td >Customer Name</td>
                                    <td>
                                        <input type="hidden" class="textbox" name="customerId" id="customerId" style="width: 110px" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" class="textbox" name="customerName" id="customerName" style="width: 110px" value="<c:out value="${customerName}"/>"/>
                                    </td>

                                    <td >Customer Order Ref No</td>
                                    <td><input type="text" class="textbox" name="customerOrderReferenceNo" id="customerOrderReferenceNo" style="width: 110px" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>

                                    <td >Consignment Order No</td>
                                    <td><input type="text" class="textbox" name="consignmentOrderReferenceNo" id="consignmentOrderReferenceNo" style="width: 110px" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><select name="status"  id="status" class="textbox" style="width:120px;" >
                                            <option value="">--Select--</option>
                                            <option value="5">Order Created</option>
                                            <option value="4">Order Cancelled</option>
                                            <option value="3">Order On Hold</option>
                                        </select>
                                        <script>
                                            document.getElementById("status").value = '<c:out value="${status}"/>';
                                        </script>    
                                    </td>
                                    <td><font color="red">*</font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red">*</font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                </tr>
                                <tr align="center">
                                    <td colspan="2"><input type="button" class="button"   value="Search" onclick="searchPage()"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        --%>
        <table width="900" height="50" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1;width:850;">CONSIGNMENT ORDERS</li>
                        </ul>
                        <div id="first">
                            <table width="830" cellpadding="0" cellspacing="5" border="0" align="center" class="table4" >
                                <tr>
                                    <td >Customer Name</td>
                                    <td>
                                        <input type="hidden" class="textbox" name="customerId" id="customerId" style="width:120px;" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" class="textbox" name="customerName" id="customerName" style="height:16px; width:120px;" value="<c:out value="${customerName}"/>"/>
                                    </td>

                                    <td >Customer Order Ref No</td>
                                    <td><input type="text" class="textbox" name="customerOrderReferenceNo" id="customerOrderReferenceNo" style="height:16px; width:120px;" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>

                                    <td >Consignment Order No</td>
                                    <td><input type="text" class="textbox" name="consignmentOrderReferenceNo" id="consignmentOrderReferenceNo" style="height:16px; width:120px;" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><select name="status"  id="status" class="textbox" style="height:19px; width:120px;" >
                                            <option value="">--Select--</option>
                                            <option value="5">Order Created</option>
                                            <option value="4">Order Cancelled</option>
                                            <option value="3">Order On Hold</option>
                                        </select>
                                        <script>
                                            document.getElementById("status").value = '<c:out value="${status}"/>';
                                        </script>
                                    </td>
                                    <td><font color="red">*</font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="height:16px; width:120px;"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red">*</font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" style="height:16px; width:120px;" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                </tr>
                                <tr align="center">
<!--                                    <td colspan="2"><input type="button"   value="Generate Excel" class="button" name="search" onClick="exportExcel()" style="width:150px"></td>
                                    <td colspan="2"><input type="button"   value="Upload Trip Planning" class="button" name="search" onClick="importExcel()" style="width:200px"></td>-->
                                    <td colspan="2"><input type="button" class="button"   value="Search" onclick="searchPage()"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <c:if test = "${consignmentList != null}" >
            <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                <thead>
                    <tr height="40">
                        <th><h3>Sno</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Consignment Date</h3></th>
                        <th><h3>Customer Name </h3></th>
                        <th><h3>Origin </h3></th>
                        <th><h3>Destination</h3> </th>
                        <th><h3>Status</h3> </th>
                        <th><h3>Vehicle Required On </h3></th>
                        <th><h3>Time To Start </h3></th>
                        <th><h3>Payment Type</h3></th>
                        <th><h3>Proceed Payment</h3></th>
                        <th><h3>View Request</h3></th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${consignmentList}" var="cnl">
                        <c:if test = "${cnl.advanceStatus == 0 && cnl.paymentType == 2}" >
                            <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                               
                                <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                            </td>
                            <td align="left" ><c:out value="${cnl.consignmentOrderDate}"/></td>

                            <td align="left" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                            <td align="left" >Order Created</td>
                            <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                                <td align="left" >
                            <c:if test="${cnl.timeLeftValue > 0}">
                                    <font color="green"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>
                                    <c:if test="${cnl.timeLeftValue <= 0}">
                                <font color="red"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>

                            <td align="left" >
                            <c:if test="${cnl.paymentType == 1}">
                                Credit
                            </c:if>
                            <c:if test="${cnl.paymentType == 2}">
                                Advance
                            </c:if>
                            <c:if test="${cnl.paymentType == 3}">
                                Part Payment
                            </c:if>
                            <c:if test="${cnl.paymentType == 4}">
                                To Pay
                            </c:if>
                            </td>    
                            <td align="left" >
                            <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                            <c:if test="${cnl.advanceRequestStatus == 0}">
                             <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>Proceed To Pay</a>
                            </c:if>
                            <c:if test="${cnl.advanceRequestStatus == 1}">
                             &nbsp;
                            </c:if>
                            </td>
                            <td align="left" >
                             <a href='/throttle/viewConsignmentPaymentRequest.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>View</a>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                        </c:if>
                        <c:if test = "${cnl.advanceToPayStatus == 0  && cnl.paymentType == 3}" >
                            <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                               
                                <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                            </td>
                            <td align="left" ><c:out value="${cnl.consignmentOrderDate}"/></td>

                            <td align="left" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                            <td align="left" >Order Created</td>
                            <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                                <td align="left" >
                            <c:if test="${cnl.timeLeftValue > 0}">
                                    <font color="green"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>
                                    <c:if test="${cnl.timeLeftValue <= 0}">
                                <font color="red"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>

                            <td align="left" >
                            <c:if test="${cnl.paymentType == 1}">
                                Credit
                            </c:if>
                            <c:if test="${cnl.paymentType == 2}">
                                Advance
                            </c:if>
                            <c:if test="${cnl.paymentType == 3}">
                                Part Payment
                            </c:if>
                            <c:if test="${cnl.paymentType == 4}">
                                To Pay
                            </c:if>
                            </td>    
                            <td align="left" >
                            <input type="hidden" name="count" id="count" value="<%=sno%>"/>    
                            <c:if test="${cnl.advanceToPayRequestStatus == 0}">
                             <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>Proceed To Pay</a>
                            </c:if>
                            <c:if test="${cnl.advanceToPayRequestStatus == 1}">
                             &nbsp;
                            </c:if>
                            </td>
                            <td align="left" >
                             <a href='/throttle/viewConsignmentPaymentRequest.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>View</a>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                        </c:if>
                        <c:if test = "${cnl.toPayStatus == 0  && cnl.paymentType == 4}" >
                            <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                               
                                <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                            </td>
                            <td align="left" ><c:out value="${cnl.consignmentOrderDate}"/></td>

                            <td align="left" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                            <td align="left" >Order Created</td>
                            <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                                <td align="left" >
                            <c:if test="${cnl.timeLeftValue > 0}">
                                    <font color="green"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>
                                    <c:if test="${cnl.timeLeftValue <= 0}">
                                <font color="red"><c:out value="${cnl.timeLeft}"/></font>
                                    </c:if>

                            <td align="left" >
                            <c:if test="${cnl.paymentType == 1}">
                                Credit
                            </c:if>
                            <c:if test="${cnl.paymentType == 2}">
                                Advance
                            </c:if>
                            <c:if test="${cnl.paymentType == 3}">
                                Part Payment
                            </c:if>
                            <c:if test="${cnl.paymentType == 4}">
                                To Pay
                            </c:if>
                            </td>    
                            <td align="left" >
                            <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                            <c:if test="${cnl.toPayRequestStatus == 0}">
                             <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>Proceed To Pay</a>
                            </c:if>
                            <c:if test="${cnl.toPayRequestStatus == 1}">
                             &nbsp;
                            </c:if>
                            </td>
                            <td align="left" >
                             <a href='/throttle/viewConsignmentPaymentRequest.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&customerId=<c:out value="${cnl.customerId}"/>&cNoteNo=<c:out value="${cnl.consignmentNoteNo}"/>&nextPage=1'>View</a>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                        </c:if>

                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 1);
        </script>
        
    </form>
</body>
</html>
