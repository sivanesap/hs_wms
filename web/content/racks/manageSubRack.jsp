<%-- 
Document   : manageSubRack
Created on : Nov 6, 2008, 7:06:42 PM
Author     : shankar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %> 
<title>JSP Page</title>
</head>
<script >
function submitPage(value){
if(value == "add"){
document.manageRocks.action = '/throttle/addSubRack.do';
document.manageRocks.submit();
}else if(value == 'alter'){
document.manageRocks.action = '/throttle/handleViewSubRackAlter.do';
document.manageRocks.submit();
}
}
</script>
<body>
<form name="manageRocks" method="post" >
  <%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table width="524" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="90" align="left" height="30" class="contentsub"><div class="contentsub">Rack Name</div></td>
<td width="105" align="left" height="30" class="contentsub"><div class="contentsub">SubRack Name</div></td>
<td width="155" align="left" height="30" class="contentsub"><div class="contentsub">SubRack Description</div></td>
<td width="66" align="left" height="30" class="contentsub"><div class="contentsub">Status</div></td>
</tr>
<% int index=0; %>
  <c:if test = "${subRackLists != null}" >
      <c:forEach items="${subRackLists}" var="subRack"> 
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>	
<tr>
<td class="<%=classText %>" height="30" style="padding-left:30px;"><c:out value="${subRack.rackName}"/> </td>
<td class="<%=classText %>" height="30" style="padding-left:30px;"><c:out value="${subRack.subRackName}"/> </td>
<td class="<%=classText %>" height="30" style="padding-left:30px;"><c:out value="${subRack.subRackDescription}"/> </td>
<td class="<%=classText %>" height="30" style="padding-left:30px;"><c:out value="${subRack.subRackStatus}"/> </td>
</tr>
 <%
  index++;
  %>
</c:forEach>
</c:if>    
</table>
<br>
<center>
<input type="button" class="button" value="Add" name="add" onClick="submitPage(this.name)">
<input type="button" class="button" value="Alter" name="alter" onClick="submitPage(this.name)">
</center>
<input type="hidden" value="" name="reqfor">

</form>
</body>

</html>
