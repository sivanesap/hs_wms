<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
    function submitPage()
    {
         var checValidate = selectedItemValidation();                
    }
    function setSelectbox(i){
    var selected=document.getElementsByName("selectedIndex") ;
    selected[i].checked = 1;
} 
function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var rackName = document.getElementsByName("rackNameList");
var rackDescription = document.getElementsByName("rackDescriptionList");
var rackStatus = document.getElementsByName("rackStatusList");
var chec=0;
for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(rackName[i],'Rack name')){       
        return;
   }     
 if(textValidation(rackDescription[i],'Rack Description')){ 
        return;
   }     
 if(textValidation(rackStatus[i],'Rack Status')){ 
        return;
   }  
   document.alterracks.action='/throttle/handleUpdateRack.do';
   document.alterracks.submit();
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
rackName[0].focus();
}
}
</script>
<body>
<form name="alterracks" method="post" >
     <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
<tr>
<td >
<%@ include file="/content/common/path.jsp" %>
</td></tr></table>
<!-- pointer table -->

<!-- message table -->
<table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
<tr>
<td >
<%@ include file="/content/common/message.jsp" %>
</td></tr></table>
<table width="524" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="57" height="30" class="contentsub"><div class="contentsub">Rack Id</div></td>
<td width="144" height="30" class="contentsub"><div class="contentsub">Rack Name</div></td>
<td width="194" height="30" class="contentsub"><div class="contentsub">Rack Description</div></td>
<td width="92" height="30" class="contentsub"><div class="contentsub">Status</div></td>
<td width="37" height="30" class="contentsub"><div class="contentsub">Select</div></td>
</tr>
<% int index=0; %>
<c:if test = "${rackLists != null}" >
 <c:forEach items="${rackLists}" var="rack"> 
  <%
    String classText = "";
    int oddEven = index % 2;
    if (oddEven > 0) {
    classText = "text2";
    } else {
    classText = "text1";
    }
    %>
<tr>
<td class="<%=classText %>" height="30"><input type="hidden" class="textbox" name="rackIdList" value="<c:out value='${rack.rackId}'/>"><c:out value='${rack.rackId}'/></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="rackNameList" value="<c:out value='${rack.rackName}'/>" onchange="setSelectbox('<%= index %>');"></td>
<td class="<%=classText %>" height="30"><textarea class="textbox" name="rackDescriptionList" onchange="setSelectbox('<%= index %>');" > <c:out value='${rack.rackDescription}'/></textarea></td>
<td class="<%=classText %>" height="30"><select class="textbox" name="rackStatusList" onchange="setSelectbox('<%= index %>');">
<c:if test="${(rack.rackStatus=='n') || (rack.rackStatus=='N')}" >
 <option value="Y" >Active</option><option value="N" selected>InActive</option>                           
 </c:if>   
<c:if test="${(rack.rackStatus=='y') || (rack.rackStatus=='Y')}" >
 <option value="Y" selected>Active</option><option value="N">InActive</option>
</c:if>
</select></td>
<td width="37" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
 <%
    index++;
  %>
</c:forEach>
  </c:if> 
</tr>
</table>
<br>
<center>
<input type="button" value="Save" class="button" onclick="submitPage();">
</center>
</form>
</body>
</html>
