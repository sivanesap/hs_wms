
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"> </script> 
        <title>MRSList</title>
    </head>
    <body>
        
        
        <script>
            function submitPag(){
                
                document.mpr.dat1.value = dateFormat(document.mpr.dat1);
                document.mpr.dat2.value = dateFormat(document.mpr.dat2);
                alert(document.mpr.dat1.value);
                document.mpr.action="/throttle/mprApprovalList.do";
                document.mpr.submit();
            }

            <%--Hari--%>
                function details(indx){                    
    var url = '/throttle/handleReqIdDetail.do?requestId='+indx;
    window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
}
            
        </script>
        
        <form name="mpr"  method="post" >                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
            <br>
   
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg">
                <tr>
                    <td colspan="9" align="center" class="contenthead" height="30">Stock Transfer Request List </td>
                </tr>
                <tr>
                    <td class="contentsub" height="30">Request No</td>
                    <td class="contentsub" height="30">To Service Point</td>
                    <td class="contentsub" height="30">Created Date</td>
                    <td class="contentsub" height="30">Required Date</td>
                    <td class="contentsub" height="30">Elapsed Days</td>
                    <td class="contentsub" height="30">Status</td>
                </tr>
                <% int index = 0;
            String classText = "";
            int oddEven = 0;
                %>
                <c:if test = "${requestList != null}" >
                    <c:forEach items="${requestList}" var="req"> 
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><a href="#" onClick="details('<c:out value="${req.requestId}"/> ')" ><c:out value="${req.requestId}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.companyName}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${req.createdDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.reqDate}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${req.elapsedDays}"/></td>                                                                        
                            <td class="<%=classText %>" height="30"><c:out value="${req.status}"/></td>                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach>
                </c:if>                  
            </table>                        
        </form>
    </body>
</html>
