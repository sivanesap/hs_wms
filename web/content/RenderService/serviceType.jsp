<%--
    Document   : serviceTypeMaster
    Created on : Dec 3, 2013, 10:43:13 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script type="text/javascript">



    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#cityNameStatus").text();
        if(document.getElementById("serviceTypeName").value == ""){
            errStr = "Please enter cityName.\n";
            alert(errStr);
            document.getElementById("serviceTypeName").focus();
        }else if(nameCheckStatus != "") {
            errStr ="Sevice Type Name Already Exists.\n";
            alert(errStr);
            document.getElementById("serviceTypeName").focus();
        }
        if(errStr == "") {
        document.serviceTypeMaster.action="/throttle/saveServiceTypeMaster.do";
        document.serviceTypeMaster.method="post";
        document.serviceTypeMaster.submit();
        }
    }

    function setValues(sno,serviceTypeId,serviceTypeName,serviceTypeDesc,status){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("serviceTypeId").value = serviceTypeId;
        document.getElementById("serviceTypeName").value = serviceTypeName;
        document.getElementById("serviceTypeDesc").value = serviceTypeDesc;
        document.getElementById("status").value = status;
    }
//
//
//
//
//
//
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

var httpRequest;
function checkServiceTypeName() {

        var serviceTypeName = document.getElementById('serviceTypeName').value;

        
            var url = '/throttle/checkServiceTypeName.do?serviceTypeName=' + serviceTypeName ;
           if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
      
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#serviceNameStatus").text('Please Check Service Type Name  :' + $("#serviceTypeName").val()+' is Already Exists');
                } else {
                     $("#nameStatus").hide();
                    $("#serviceNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }



 
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Service Type Master Brattle Food</title>
    </head>
    <body onload="document.serviceTypeMaster.serviceTypeName.focus();">
        <form name="serviceTypeMaster"  method="POST">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="cityId" id="cityId" value=""  />
                <tr>
            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                 <tr>
                    <td class="contenthead" colspan="4" >Service Type Master</td>
                </tr>
                <tr>
                    <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="serviceNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Service Type Name</td>
                    <td class="text1"><input type="text" name="serviceTypeName" id="serviceTypeName" class="textbox" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkServiceTypeName();" autocomplete="off" maxlength="50"></td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Description</td>
                    <td class="text1"><input type="hidden" name="serviceTypeId" id="serviceTypeId" value="" class="textbox">
                        <input type="text" name="serviceTypeDesc" id="serviceTypeDesc" value="" class="textbox"></td>
                </tr>
                <tr>

                    <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td class="text2">
                        <select  align="center" class="textbox" name="status" id="status" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                    <td class="text2">&nbsp;&nbsp;</td>
                    <td class="text2">&nbsp;&nbsp;</td>
                </tr>


            </table>
                    </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">


                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <h2 align="center">Service Type List</h2>


            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Service Type Name </h3></th>
                        <th><h3>Service Type Desc</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
            <c:if test = "${serviceTypeList != null}">
                        <c:forEach items="${serviceTypeList}" var="cml">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${cml.serviceTypeName}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.desc}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.status}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${cml.serviceTypeId}" />','<c:out value="${cml.serviceTypeName}" />','<c:out value="${cml.desc}" />','<c:out value="${cml.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>



            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</html>