<%-- 
    Document   : shipmentUploadview
    Created on : Mar 16, 2021, 10:34:29 AM
    Author     : DELL
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">

     function searchPage() {
        document.invoiceSearch.action = "/throttle/shipmentUploadView.do";
        document.invoiceSearch.submit();
    }
    
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function uploadContract() {
        document.upload.action = "/throttle/uploadCustomerContract.do";
        document.upload.method = "post";
        document.upload.submit();
    }

    function uploadPage() {
        document.upload.action = "/throttle/saveCustomerContractUpload.do?&param=save";
        document.upload.method = "post";
        document.upload.submit();
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="Shipment Upload View"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="Shipment Upload View"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <form name="upload"  method="post" enctype="multipart/form-data">
                <%--<%@ include file="/content/common/path.jsp" %>--%>

                <%@ include file="/content/common/message.jsp" %>

                <table class="table table-info mb30 table-hover" id="uploadTable"  >
                    <thead> 
                        <tr>
                            <th  colspan="4" height="30" >Shipment Upload Details</th>
                        </tr>
                    </thead>
                    <tr>    
                        <td><font color="red">*</font>Hub</td>
                        <td><select id="hub" name="hub" style="width:250px;height:40px" >
                                <option value="">---Select One ---</option>    <c:forEach items="${originList}" var="ct">
                                    <option value="<c:out value="${ct.origin}"/>"><c:out value="${ct.origin}"/></option>
                                </c:forEach>
                            </select> </td>

                        <td><font color="red">*</font>Shipment Type</td>
                        <td><select id="shipmentType" name="shipmentType" style="width:250px;height:40px" >
                                <option value="">---Select One ---</option>    <c:forEach items="${originList}" var="ct">
                                    <option value="<c:out value="${ct.shipmentType}"/>"><c:out value="${ct.shipmentType}"/></option>
                                </c:forEach>
                            </select> </td>
                    </tr>
                    <tr>
                        <td><font color="red">*</font>From Date</td>
                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                        <td><font color="red">*</font>To Date</td>
                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                    </tr>
                    <tr>             
                        <td></td>
                        <td></td>
                        <td  colspan="2"><input  class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                    </tr>
                </table>
       </div>
    </div>
                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="40">
                            <th>S No</th>
                            <th>Upload Id</th>
                            <th>Hub Name</th>
                            <th>Date</th>
                            <th>Upload Time</th>
                            <th>Shipment Id</th>
                            <th>User Name</th>
                            <th>Noof Qty</th>
                            <th>Action</th>
                      </tr>
                  </thead>
               </table>
    <div id="ptp" style="overflow: auto">
        <div class="inpad">

            <c:if test = "${getContractList != null}" >

                <table class="table table-info mb30 table-hover" id="table" >

                    <thead>
                        <tr height="40">
                            <th  height="30" >S No</th>
                            <th  height="30" >Shipment Id</th>
                            <th  height="30" >Customer Name</th>
                            <th  height="30" >City</th>
                            <th  height="30" >Pin Code</th>
                            <th  height="30" >Delivery Hub</th>
                            <th  height="30" >Assigned Executive</th>
                            <th  height="30" >Origin</th>
                            <th  height="30" >Current Hub</th>
                            <th  height="30" >Status</th>                                                    
                            <th  height="30" >Note</th>
                            <th  height="30" >Amount</th>
                            <th  height="30" >Weight</th>
                            <th  height="30" >Last Modified</th>
                            <th  height="30" >Last Received</th>
                            <th  height="30" >Shipment Type</th>
                            <th  height="30" >Error</th>
                        </tr>
                    </thead>
                    <% int index = 0; 
                     int sno = 1;
                    %> 
                    <tbody>

                        <c:forEach items= "${getContractList}" var="contractList">
                            <tr height="30">
                                <td align="left"><%=sno%></td>
                                <td align="left"   ><c:out value="${contractList.shipmentId}"/></td>
                                <td align="left"   ><c:out value="${contractList.customerName}"/></td>
                                <td align="left"   ><c:out value="${contractList.city}"/></td>
                                <td align="left"   ><c:out value="${contractList.pinCode}"/></td>

                                <td align="left"   ><c:out value="${contractList.deliveryHub}"/></td>
                                <td align="left"   ><c:out value="${contractList.assignedExecutive}"/></td>
                                <td align="left"   ><c:out value="${contractList.origin}"/></td>
                                <td align="left"   ><c:out value="${contractList.currentHub}"/></td>
                                <td align="left"   ><c:out value="${contractList.status}"/></td>
                                <td align="left"   ><c:out value="${contractList.note}"/></td>

                                <td align="left"   ><c:out value="${contractList.amount}"/></td>
                                <td align="left"   ><c:out value="${contractList.weight}"/></td>
                                <td align="left"   ><c:out value="${contractList.lastModified}"/></td>
                                <td align="left"   ><c:out value="${contractList.lastReceived}"/></td>
                                <td align="left"   ><c:out value="${contractList.shipmentType}"/></td>

                                <td style="color:red"> 
                                    <c:if test="${contractList.error==1}">
                                        Invalid city Id 
                                    </c:if>
                                    <c:if test="${contractList.error==2}">
                                        Invalid Pin code
                                    </c:if>
                                    <c:if test="${contractList.error==3}">
                                        Invalid delivery Hub
                                    </c:if>
                                    <c:if test="${contractList.error==4}">
                                        Invalid origin Hub
                                    </c:if>
                                    <c:if test="${contractList.error==5}">
                                        Invalid current Hub
                                    </c:if>

                                </td>

                            </tr> 
                            <%
                            sno++;
                            index++;
                            %>
                        </c:forEach>
                    </tbody>
                </table>

                <center>

                    <td colspan="0"><input type="button" class="btn btn-success" value="Submit" name="submitPage" id="submitPage" onclick="uploadPage();"></td>
                </center>
            </c:if>
        </div>
    </div>     

</form>
</body>
</div>
</div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>


