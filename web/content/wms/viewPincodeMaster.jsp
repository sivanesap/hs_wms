<%--
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Pincode Master"  text="Pincode Master"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Pincode Master"  text="Master"/></a></li>
            <li class="active">Pincode Master</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setValues(<c:out value="${stateId}"/>,<c:out value="${cityId}"/>);">
                <form name="pincode" method="post">
                    <c:if test="${pincodeTemp==null}">
                <table class="table table-info mb30 table-hover" id="pincode">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">Pincode Master</th>
                          </tr></thead>  
                        <tr>
                           <td align="center"><br><text style="color:red">*</text>
                                State
                            </td>
                            <td><br><select id="stateId" name="stateId" style="width:200px;height:30px">
                              <option value="">---Select One ---</option>    <c:forEach items="${stateList}" var="st">
                                      <option value="<c:out value="${st.stateId}"/>"><c:out value="${st.stateName}"/></option>
                            </c:forEach>
                              </select></td>
                           <td align="center"><br><text style="color:red">*</text>
                                City
                            </td>
                            <td><br><select id="cityId" name="cityId" style="width:200px;height:30px" >
                              <option value="">---Select One ---</option>    <c:forEach items="${cityList}" var="ct">
                                      <option value="<c:out value="${ct.cityId}"/>"><c:out value="${ct.cityName}"/></option>
                            </c:forEach>
                              </select></td>
                           
                            </tr>
                           
                           
                       
                </table>  
                            <center><br><br><input type="button" class="btn btn-success" value="Search" onclick="searchPage()"></center>
                 
                      
                        </c:if>
                       
                    <c:if test="${pincodeMaster!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Pincode</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Type</th>
                                <th>Place</th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${pincodeMaster}" var="pin">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${pin.pincode}"/></td>
                                        <td><c:out value="${pin.stateName}"/></td>
                                        <td><c:out value="${pin.cityName}"/></td>
                                        <td><c:out value="${pin.type}"/></td>
                                        <td><c:out value="${pin.place}"/></td>
                                        
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                            <table>
                            
                             <table>

                    </table>  
                 </c:if>
 <script>
   
     function searchPage() {
         if(document.getElementById("stateId").value==""){
             alert("Select State");
             document.getElementById("stateId").focus();
         }else if(document.getElementById("cityId").value==""){
             alert("Select City");
             document.getElementById("cityId").focus();
         }
         else{
            document.pincode.action = '/throttle/viewPincodeMaster.do?&param=search';
            document.pincode.submit();
        }
    }
    
                </script>
                           
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>s