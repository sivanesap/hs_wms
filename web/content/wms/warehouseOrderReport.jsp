<%-- 
    Document   : warehouseOrderReport
    Created on : 22 Feb, 2021, 11:44:36 AM
    Author     : entitle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
   
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.WarehouseWise Order report"  text="WarehouseWise Order report"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.WarehouseWise Order report"  text="Report"/></a></li>
            <li class="active">WarehouseWise Order report</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setDate();">
                <form name="whreport" method="post">
                   
                <table class="table table-info mb30 table-hover" id="pincode">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">WarehouseWise Order report</th>
                          </tr></thead>  
                    <tr>
                        <td>From Date</td>
                        <td><input type="text" class="datepicker" style="width:200px;height:35px" id="fromDate" name="fromDate"></td>
                        <td>To Date</td>
                        <td><input type="text" class="datepicker" style="width:200px;height:35px" id="toDate" name="toDate"></td>
                    </tr>
                    <tr>
                        <td>State</td>
                   <td> <select id="stateId" name="stateId" style="width:200px;height:40px" value="">
                        <option value="">---select---</option>
                        <c:forEach items="${stateList}" var="st">
                                      <option value="<c:out value="${st.stateId}"/>"><c:out value="${st.stateName}"/></option>
                            </c:forEach></select></td>
                        <td>WareHouse</td>
                        <td> <select id="whId" name="whId" value="" style="width:200px;height:40px">
                        <option value="">---select---</option>
                        <c:forEach items="${whList}" var="wh"><option value="<c:out value="${wh.whId}"/>"><c:out value="${wh.whName}"/></option>
                        </c:forEach></select></td>
                    </tr>
                    <script>document.getElementById("whId").value=<c:out value="${hubId}"/></script>
                         </table>  
                        <table>
                            <center>
                                <input type="button" value="Search" id="search" class="btn btn-success" onclick="submitPage();">
                                &emsp;<input type="reset" id="clears" class="btn btn-success" value="Clear">
                            </center><br><br>

                        </table>
                       
                   <c:if test="${warehouseOrderReport!=null}">
                 <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>Warehouse Name</th>
                                <th>Trip Planned Qty</th>
                                <th>OFD</th>
                                <th>Delivered</th>
                                <th>Returned</th>
                                <th>Cancelled by H&S</th>
                                <th>Cancelled by 72N</th>
                                <th>Total</th>
                            </tr>

                        </thead>
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${warehouseOrderReport}" var="wor">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td ><c:out value="${wor.whName}"/></td>
                                        <td><c:out value="${wor.tripQty}"/></td>
                                        <td><c:out value="${wor.ofd}"/></td>
                                        <td><c:out value="${wor.delivered}"/></td>
                                        <td><c:out value="${wor.returned}"/></td>
                                        <td><c:out value="${wor.cancelled}"/></td>
                                        <td><c:out value="${wor.cancelled1}"/></td>
                                        <td><b><c:out value="${wor.tripQty+wor.ofd+wor.delivered+wor.returned+wor.cancelled+wor.cancelled1}"/></b>
                                        <c:set var="totTripQty" value="${totTripQty+wor.tripQty}"/>
                                        <c:set var="totOfd" value="${totOfd+wor.ofd}"/>
                                        <c:set var="totDelivered" value="${totDelivered+wor.delivered}"/>
                                        <c:set var="totReturned" value="${totReturned+wor.returned}"/>
                                        <c:set var="totCancelled" value="${totCancelled+wor.cancelled}"/>
                                        <c:set var="totCancelled1" value="${totCancelled1+wor.cancelled1}"/>
                                        <c:set var="tot" value="${totTripQty+totOfd+totDelivered+totReturned+totCancelled+totCancelled1}"/></td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach> 
                                    <tr>
                                    <td><b>Total</b></td>
                                    <td><b><c:out value="${totTripQty}"/></b></td>
                                    <td><b><c:out value="${totOfd}"/></b></td>
                                    <td><b><c:out value="${totDelivered}"/></b></td>
                                    <td><b><c:out value="${totReturned}"/></b></td>
                                    <td><b><c:out value="${totCancelled}"/></b></td>
                                    <td><b><c:out value="${totCancelled1}"/></b></td>
                                    <td><b><c:out value="${tot}"/></b></td>
                                    </tr>
                            </tbody>
                            
                    </table> 
                            </c:if>  
 <script>
   function setDate(){
       var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();
    today = dd+'-'+mm+'-'+yyyy;
    document.getElementById("fromDate").value=today;
    document.getElementById("toDate").value=today;
   }
     function submitPage() {
        var stateId=document.getElementById("stateId").value;
        var whId=document.getElementById("whId").value;
        if(stateId=="" && whId==""){
         alert("select any state or warehouse");   
        }else{
            document.whreport.action = '/throttle/warehouseOrderReport.do?&param=search';
            document.whreport.submit();
        }
    }
   
   
    
    
                </script>
                            
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

