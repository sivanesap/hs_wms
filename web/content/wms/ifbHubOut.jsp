
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.IFB Shipment Details"  text="IFB Shipment Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.IFB Shipment Details"  text="Master"/></a></li>
            <li class="active">IFB Shipment Details</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="hubout" method="post">

                    <table class="table table-info mb30 table-hover"  >

                        <thead>
                        <th colspan="4">Shipment Pickup Details</th>
                        </thead><tr>
                            <td><font color="red">*</font>Vehicle No</td>
                            <td><input type="text" id="vehicleNo" class="form-control" style="width:240px;height: 40px" name="vehicleNo" value=""></td>
                            <td><font color="red">*</font>Driver Name</td>
                            <td><input type="text" id="driverName" class="form-control" style="width:240px;height: 40px" name="driverName" value=""></td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Driver Mobile</td>
                            <td><input type="text" id="driverMobile" class="form-control" style="width:240px;height: 40px" name="driverMobile" value=""></td>
                        </tr>
                    </table>
                    <table class="table table-info mb30 table-hover"  >
                        <thead>
                        <th colspan="4">Invoice Scanning</th>
                        </thead>
                        <tr>
                            <td>Invoice No</td>
                            <td><input type="text" id="serial" class="form-control" style="width:240px;height: 40px" name="serial" value="" onchange="scanningArray(this.value);"></td>
                        </tr>
                    </table>

                    <table class="table table-info mb30 table-hover" id="table"  >	
                        <thead>
                            <tr height="30">
                                <th>S.No</th>
                                <th>Customer Name</th>
                                <th>LR No</th>
                                <th>Invoice No</th>
                                <th>Invoice Date</th>  
                                <th>From Warehouse</th>  
                                <th>To Warehouse</th>  
                                <th>Total Qty</th>
                                <th>City</th>
                                <th>Pincode</th>
                                <th>Gross Value</th>
                                <th>Eway_Bill No</th>
                                <th>Eway_Bill Expiry</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <%
                            int sno = 0;
                        %>
                        <tbody>
                            <c:forEach items="${hubOutList}" var="pc">
                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>

                                <tr>
                                    <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.customerName}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.lrNo}" /></td>  
                                    <td class="<%=className%>"  align="left">  <a href="#" onclick="viewMaterialDetails('<c:out value="${pc.orderId}" />');"><u><c:out value="${pc.invoiceNo}" /></u></a></td>  
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceDate}" /></td>  
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.currentHub}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.whName}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.city}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.pincode}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.grossValue}" /></td> 
                                    <td class="<%=className%>" align="left" id="eway<%=sno%>"> <input name="ewayBillNo" id="ewayBillNo<%=sno%>" onchange="setEwayNo('<%=sno%>', this.value)" value="<c:out value="${ifb.ewayBillNo}"/>"  class="form-control" style="width:180px;height:35px"> </td>
                                    <td class="<%=className%>" align="left" id="expiry<%=sno%>"> <input name="ewayBillExpiryDate" id="ewayBillExpiryDate<%=sno%>" onchange="setEwayExpiry('<%=sno%>', this.value)" value="<c:out value="${ifb.ewayExpiry}"/>"  class="datepicker form-control" style="width:180px;height:35px"> </td>
                                    <td align="left"   ><input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" disabled value="<c:out value="${ifb.lrId}"/>" onclick="checkSelectStatus('<%=sno%>', this, '<c:out value="${ifb.ewayBillNo}"/>', '<c:out value="${ifb.ewayExpiry}"/>')"></td>
                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" /> 
                            <input type="hidden" name="ewayBillNoTemp" id="ewayBillNoTemp<%=sno%>" value="0"  class="form-control" style="width:180px;height:35px"> 
                            <input type="hidden" name="ewayBillExpiryTemp" id="ewayBillExpiryTemp<%=sno%>" value="0"  class="datepicker form-control" style="width:180px;height:35px">
                            <input type="hidden" name="orderId" id="orderId<%=sno%>" value="<c:out value="${pc.orderId}" />" /> 
                            <input type="hidden" name="hubId" id="hubId<%=sno%>" value="<c:out value="${pc.whId}" />" /> 
                            <input type="hidden" name="serNo" id="serNo<%=sno%>" value="<c:out value="${pc.invoiceNo}" />" /> 
                            </tr>
                            <script>
                               if('<c:out value="${pc.grossValue}" />'<=50000){
                                   document.getElementById("ewayBillNo<%=sno%>").value=0;
                                   document.getElementById("ewayBillExpiryDate<%=sno%>").value=0;
                                   document.getElementById("ewayBillNo<%=sno%>").style.display="none";
                                   document.getElementById("ewayBillExpiryDate<%=sno%>").style.display="none";
                                   document.getElementById("expiry<%=sno%>").innerHTML="-";
                                   document.getElementById("eway<%=sno%>").innerHTML="-";
                               }
                            </script>
                        </c:forEach>
                        </tbody>
                    </table>

                    <input type="hidden" name="count" id="count" value="<%=sno%>" />

                    <br>
                    <script>
                        var arr1 = [];
                        var arr2 = [];
                        var ser = document.getElementById("count").value;
//                            alert(ser);
                        var i = 0;
                        for (var x = 1; x <= ser; x++) {
                            arr1[i] = document.getElementById("serNo" + x).value;
                            i++;
                        }
                        function scanningArray(value) {
                            if (arr1.indexOf(value) != -1) {
                                if (arr2.indexOf(value) != -1) {
                                    alert("Already Scanned");
                                    document.getElementById("serial").value = "";
                                } else {
                                    arr2.push(value);
                                    for (var k = 1; k <= ser; k++) {
                                        if (document.getElementById("serNo" + k).value === value) {
                                            document.getElementById("selectedIndex" + k).checked = true;
                                            document.getElementById("selectedStatus" + k).value = "1";
                                        }
                                    }
                                    document.getElementById("serial").value = "";
                                }
                            } else {
                                alert("Not a Listed Invoice");
                                document.getElementById("serial").value = "";
                            }
                            return arr2;
                        }
                        function checkSelectStatus(sno, obj) {
                            var val = document.getElementsByName("selectedStatus");
                            if (obj.checked == true) {
                                document.getElementById("selectedStatus" + sno).value = 1;
                            } else if (obj.checked == false) {
                                document.getElementById("selectedStatus" + sno).value = 0;
                            }
                            var vals = 0;
                            for (var i = 0; i <= val.length; i++) {
                            }
                        }
                        function submitPage() {
                            if (arr2.length >= 1) {
                                if (document.getElementById("vehicleNo").value == "") {
                                    alert("Enter Vehicle No");
                                    document.getElementById("vehicleNo").focus();
                                } else if (document.getElementById("driverName").value == "") {
                                    alert("Enter Driver Name");
                                    document.getElementById("driverName").focus();
                                } else if (document.getElementById("driverMobile").value == "") {
                                    alert("Enter Driver Mobile No");
                                    document.getElementById("driverMobile").focus();
                                } else {
                                    document.getElementById("hub").style.display = "none";
                                    document.hubout.action = "/throttle/ifbHubOut.do?&param=update";
                                    document.hubout.submit();
                                }
                            } else {
                                alert("Select atleast One Order to Hub Out")
                            }
                        }
                        function setEwayNo(sno, ewayNo) {
                            if (ewayNo != "") {
                                document.getElementById("ewayBillNoTemp" + sno).value = ewayNo;
                            }
                        }
                        function setEwayExpiry(sno, ewayExpiry) {
                            if (ewayExpiry != "") {
                                document.getElementById("ewayBillExpiryTemp" + sno).value = ewayExpiry;
                            }
                        }
                        function viewMaterialDetails(lrId) {
                            window.open('/throttle/ifbHubOut.do?lrId=' + lrId + '&param=serial', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                        }
                    </script>
                    <center>
                        <input type="button" class="btn btn-success" name="hub" id="hub" value="Update" onclick="submitPage()" />
                    </center>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

