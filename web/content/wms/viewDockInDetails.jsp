<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.View DockIn Detail"  text="View DockIn Detail"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.View DockIn Detail"  text="ViewDockInDetail"/></a></li>
            <li class="active">View DockIn Detail</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setValues(<c:out value="${serialTempSize}"/>)">
                <form name="grn" method="post" enctype="multipart/form-data">
                    <table class="table table-info mb30 table-hover" id="serial">
                        <c:if test="${getDockInDetails != null}">
                            <c:forEach items="${getDockInDetails}" var="grn">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <thead >
                            <tr >                                
                                <th align="center">Asn No</th>
                                <th align="center">Asn Date</th>
                                <th align="center">Supplier Plant</th>
                                <th align="center">Dock In</th>
                                <th align="center">GateIn Time</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><c:out value="${grn.asnNo}"/></td>
                                <td><c:out value="${grn.asnDate}"/></td> 
                                <td><c:out value="${grn.custName}"/></td>
                                <td>Dock <c:out value="${grn.dock}"/></td> 
                                <td><c:out value="${grn.gtnTime}"/></td>
                            </tr>
                        </tbody>
                         </c:forEach>
                            </c:if> 
                    </table>          
                    <table class="table table-info mb30 table-bordered">
                        <thead >
                            <tr >                                
                                <th>Customer</th>
                                <th>Product</th>
                                <th>Product Code</th>
                                <th>HSN Code</th>
                                <th>Shortage</th>
                                <th>Excess</th>
                                <th>Damaged</th>
                                <th>Input Quantity</th>                               
                            </tr>
                        </thead>
                        <c:if test="${getDockInDetails != null}">
                            <%      int index1 = 0;
                                    int sno1 = 1;
                            %>
                            <tbody>
                                <c:forEach items="${getDockInDetails}" var="grn">
                                    <%
                                                String className = "text1";
                                                if ((index1 % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td ><c:out value="${grn.custName}"/></td>
                                        <td><c:out value="${grn.itemName}"/></td>
                                        <td><c:out value="${grn.skuCode}"/></td>
                                        <td><c:out value="${grn.hsnCode}"/></td>
                                        <td><c:out value="${grn.shortage}"/></td>
                                        <td><c:out value="${grn.excess}"/></td>
                                        <td><c:out value="${grn.damaged}"/></td>
                                        <td><c:out value="${grn.inpQty}"/>
                                        </td>
                            
                                    </tr>
                                    <%index1++;%>
                                    <%sno1++;%>
                                </c:forEach>     
                            </tbody>
                        </c:if>
                    </table>  
                    <br>
                    <table class="table table-info mb30 table-bordered" style="width: 90%">
                        <thead >
                            <tr >                             
                                <th>Invoice No</th>
                                <th>Invoice Date</th>
                                <th>EwaybillNo</th>
                                <th>Upload Invoice</th>
                                <th>Upload Ewaybill</th>
                            </tr>
                        </thead>
                        <c:if test="${getDockInDetails != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${getDockInDetails}" var="grn">
                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >                                        
                                <input type="hidden" name="selectedIndexs" id="selectedIndexs<%=sno - 1%>" value="0"/>
                                <c:set var="whId" value="${grn.whId}"/>
                                <input type="hidden" name="asnId" id="asnId<%=sno - 1%>" value="<c:out value="${grn.asnId}"/>"/>
                                <input type="hidden" name="itemId" id="itemId<%=sno - 1%>" value="<c:out value="${grn.itemId}"/>"/>
                                <input type="hidden" name="custName" id="custName<%=sno - 1%>" value="<c:out value="${grn.custName}"/>"/>
                                <input type="hidden" name="custId" id="custId" value="<c:out value="${grn.custId}"/>"/>
                                <input type="hidden" name="itemName" id="itemName<%=sno - 1%>" value="<c:out value="${grn.itemName}"/>"/>
                                <input type="hidden" name="skuCode" id="skuCode<%=sno - 1%>" value="<c:out value="${grn.skuCode}"/>"/>
                                <input type="hidden" name="hsnCode" id="hsnCode<%=sno - 1%>" value="<c:out value="${grn.hsnCode}"/>"/>
                                <input type="hidden" name="whIds" id="whIds<%=sno - 1%>" value="<c:out value="${grn.whId}"/>"/>
                                <input type="hidden" name="whId" id="whId" value="<c:out value="${whId}"/>"/>
                                <input type="hidden" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onchange="saveTempSerialNos();"/> 
                                <input type="hidden" name="grnId" id="grnId<%=sno - 1%>" style="width:120px" class="form-control" value="<c:out value="${grn.grnId}"/>"/>

                                <td><input type="text" name="invoiceNo" id="invoiceno<%=sno - 1%>" style="width:120px" class="form-control" value="<c:out value="${grn.invoiceNo}"/>"/></td>
                                <td><input type="text" name="invoiceDate" id="invoiceDate<%=sno - 1%>" style="width:120px" class="datepicker , form-control" value="<c:out value="${grn.invoiceDate}"/>"/></td>
                                <td><input type="text" name="ewaybillNo" id="ewaybillNo<%=sno - 1%>" style="width:120px" class="form-control" value=""/></td>
                                <td height='30'><input type='file' name='file' size='110' style="width:200px" /></td>
                                <td height='30'><input type='file' name='file1' size='110' style="width:200px" /></td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>  
                                <input type="hidden" value="<%=sno-1%>" id="checkinv" name="checkinv">
                            </tbody>
                        </c:if>
                    </table>  
                    <br>
                    <center>
                        <input type="button" class="btn btn-success" name="Update" value="Update" onclick="saveGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                    </center><br>
                    <c:if test="${serialTempDetails !=null}">
                        <table class="table table-info mb30 table-bordered" style="width: 40%" align="center">
                            <tr height="40" Style="background-color: #5BC0DE;">
                                <th><font color="white">Sno</font></th>
                                <th><font color="white">Serial No</font></th>
                                <th><font color="white">Qty</font></th>
                                <th><font color="white">Bin Name</font></th>
                                    
                            </tr>

                            <% int index = 0,sno = 1; %>
                            <c:forEach items="${serialTempDetails}" var="grn">
                                <tr>
                                    <% 
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                    %>

                                <tr height="30" border="0">
                                    <td align="left"><%=sno%></td>
                                    <td align="left"><input type="hidden" id="serialNos<%=index%>" name="serialNos" value='<c:out value="${grn.serialNo}"/>'/><c:out value="${grn.serialNo}"/></td>
                                    <td  align="left"><c:out value="${grn.qty}"/></td>
                                    <td  align="left"><input type="hidden" id="binIds<%=index%>" name="binIds" value='<c:out value="${grn.binId}"/>'/><c:out value="${grn.binName}"/></td>
                                    <%
                                               index++;
                                               sno++;
                                    %>
                                </tr>
                            </c:forEach>
                                
                                <input type="hidden" name="inpQty" id="inpQty" value="">
                        </table>
                    </c:if>
                    <br>
                    <div id="receivedGRNTempDetails" style="width: 90%">
                    </div>
                    <script>
                        function setValues(val){
                         document.getElementById("inpQty").value=val;
                        }
                        function saveGrnSerialNos() {
                            var sno=document.getElementById("checkinv").value;
//                            alert(sno);
                            var count=0;
                            for(var x=0; x<sno;x++){
//                                alert(document.getElementById('invoiceno'+x).value);
                                if(document.getElementById('invoiceno'+x).value=="" || document.getElementById('invoiceno'+x).value==0){
                                    count++;
                                }
                            }
//                            alert(count);
                            if(count==0){
                            document.grn.action = '/throttle/saveGrnSerialStockDetails.do';
                            document.grn.submit();
                        }else{
                            alert("Please Enter Invoice Number");
                        }
                        }
                    </script> 

                    
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </div>
</div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

