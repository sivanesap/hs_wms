<%--
    Document   : grnDetails
    Created on : Jan 21, 2021, 12:47:26 PM
    Author     : throttle
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Route Planning"  text="Route Planning"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Route Planning"  text="Operations"/></a></li>
            <li class="active">Route Planning</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="setRouteId(<c:out value="${routeId}"/>);">
                <form name="routeplan" method="post">
                    
                <table class="table table-info mb30 table-hover" id="pincode"  style="width:80%">
                            <div align="center" style="height:15px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                    <thead><tr>
                    <th class="contenthead" colspan="8">Route Planning</th>
                          </tr></thead>  
                        <tr>
                           <td align="center" style="font-size: 18px"><br><text style="color:red">*</text>
                                Routes
                            </td>
                            <td><br><select id="routeId" name="routeId" style="width:250px;height:40px">
                              <option value="">---Select One ---</option>    <c:forEach items="${routeMaster}" var="st">
                                      <option value="<c:out value="${st.routeId}"/>"><c:out value="${st.routeName}"/></option>
                            </c:forEach>
                              </select></td>
                        <td align="left"><center><br><input type="button" class="btn btn-success" value="Search" onclick="searchPage()" style="width:150px"></center></td>   
                </tr>
                

                </table>  
       
                 
                       
                    <c:if test = "${saveOrderRouteListSize != 0}" >

                        <table class="table table-info mb30 table-hover" id="table" style="width:100%" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Customer </th>                                    
                                    <th>LoanProposalID</th>
                                    <th>Invoice No</th>
                                    <th>Serial No</th>
                                    <th>Branch</th>
                                    <th>Product</th>                                                                                                            
                                    <th>Route</th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${saveOrderRouteList}" var="sol">
                                   <tr height="30">
                                <td align="left"><%=sno%>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                </td>
                                <td align="left" ><c:out value="${sol.clientName}"/></td>
                                <td align="left" ><c:out value="${sol.loanProposalID}"/></td>
                                <td align="left" ><c:out value="${sol.invoiceCode}"/></td>
                                <td align="left" ><c:out value="${sol.serialNumber}"/></td>
                                <td align="left" ><c:out value="${sol.branchName}"/></td>
                                <td align="left" ><c:out value="${sol.productID}"/></td>                                
                              <td align="left"><c:out value="${sol.routeName}"/></td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                                
                            </c:forEach>
                            </tbody>
                        </table>
                               
                    </c:if>

                    <c:if test = "${getOrderRouteList != null}" >
                        <table>
                              <center><input type="button" class="btn btn-success" value="Save" onclick="saveOrder();" style="width:150px"><br></center><br><br>
                        </table>
                        <table class="table table-info mb30 table-hover" id="table" style="width:100%" >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>                                    
                                    <th>Customer </th>                                    
                                    <th>LoanProposalID</th>
                                    <th>Invoice No</th>
                                    <th>Serial No</th>
                                    <th>Branch</th>
                                    <th>Product</th>                                                                                                            
                                    <th>Select 
                                        <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/>
                                    </th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${getOrderRouteList}" var="cnl">
                                   <tr height="30">
                                <td align="left"><%=sno%>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <input type="hidden" style="width: 184px;" name="orderId" id="orderId<%=index%>" class="form-control" style="width:250px;height:40px"   value="<c:out value="${cnl.orderId}"/>"  readOnly maxlength="50">
                                </td>
                                <td align="left" ><c:out value="${cnl.clientName}"/></td>
                                <td align="left" ><c:out value="${cnl.loanProposalID}"/></td>
                                <td align="left" ><c:out value="${cnl.invoiceCode}"/></td>
                                <td align="left" ><c:out value="${cnl.serialNumber}"/></td>
                                <td align="left" ><c:out value="${cnl.branchName}"/></td>
                                <td align="left" ><c:out value="${cnl.productID}"/></td>                                
                                <td align="center" >
                                    <c:if test="${cnl.serialNumber=='' || cnl.serialNumber=='0'}">
                                        <font color="red"><b>Invalid SerialNo</b></font>
                                    </c:if>
                                    <c:if test="${cnl.serialNumber!='' && cnl.serialNumber!='0'}">
                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<c:out value="${cnl.orderId}"/>"  onclick="checkSelectStatus(<%=sno%>, this);" />
                                    <input type="hidden" id="orderIds" name="orderIds" value="">
                                    <input type="hidden" id="whId" name="whId" value="<c:out value="${cnl.whId}"/>">
                                    </c:if>
                                    <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" />                                    
                                </td>
                                </tr>
                                <%index++;%>
                                <%sno++;%>
                                
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <table class="table table-info mb30 table-hover" style="width:70%">
                    <tr>
                <td align="center" style="font-size:18px"><br>Employee Name</td>
                            
                           <td><br> <select id="empId" name="empId" style="width:250px;height:40px" >
                              <option value="">---Select One ---</option>    <c:forEach items="${empDetails}" var="ct">
                                      <option value="<c:out value="${ct.empId}"/>"><c:out value="${ct.empName}"/></option>
                            </c:forEach>
                              </select></td>
                <td align="left"><center><br><input type="button" class="btn btn-success" value="Complete" onclick="complete();" style="width:150px"></center></td>
            
            </tr></table>
 <script>
     function complete(){
         var empId=document.getElementById("empId").value;
         if(empId==''){
             alert("Please Select Employee Name");
         }else{
     document.routeplan.action='/throttle/addRoutePlanning.do?param=complete' 
     document.routeplan.submit();
     }
 }
     
     function checkAll() {
                            var inputs = document.getElementsByName("selectedIndex");
                            for (var i = 0; i < inputs.length; i++) {
                                if (inputs[i].type == "checkbox") {
                                    if (inputs[i].checked == true) {
                                        inputs[i].checked = false;
                                    } else if (inputs[i].checked == false) {
                                        inputs[i].checked = true;
                                    }
                                }
                            }
                        }
     
   function saveOrder(){
     document.routeplan.action='/throttle/addRoutePlanning.do?param=saveOrder' 
     document.routeplan.submit();
    }
   
   function setRouteId(routeId){
       document.getElementById("routeId").value=routeId;
       
   }
     function searchPage() {
         if(document.getElementById("routeId").value==""){
             alert("Select Route");
             document.getElementById("routeId").focus();
         }else{
            document.routeplan.action = '/throttle/addRoutePlanning.do?&param=search';
            document.routeplan.submit();
        }
    }
    
                </script>
                           
                       
               </form>
        </body>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                   
        </div>
    </div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>s