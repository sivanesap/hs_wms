<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PackingList"  text="Update Dispatch Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="stores.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="stores.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Delivery"  text="Inbound"/></a></li>
            <li class="active"><spring:message code="stores.label.PickList"  text="Update Dispatch Details"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">

                <!--                                <body onload="setFocus();getItemNames();">-->

                <form name="searchPart"  id="dispatch" method="post"  >

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4"><spring:message code="stores.label.Dispatch"  text="Dispatch "/></th></tr></thead>
                        <tr>
                            <td ><font color="red">*</font>Vehicle Reporting Date</td>
                            <td ><input type="text" class="datepicker form-control" style="width:260px;height:40px;" name="reportDate" id="reportDate"/>
                            </td>

                            <td ><font color="red">*</font>Vehicle No</td>
                            <td ><input type="text" class="form-control" style="width:260px;height:40px;" name="vehicleId" id="vehicleId" />
                            </td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Transporter Name</td>
                            <td ><input type="text" class="form-control" style="width:260px;height:40px;" name="vendorId" id="vendorId"/>
                            </td>
                            <td ><font color="red">*</font>Reference No</td>
                            <td ><input type="text" class="form-control" style="width:260px;height:40px;" name="refNo" id="refNo"/>
                            </td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Dispatch Date</td>
                            <td ><input type="text" class="datepicker form-control" style="width:260px;height:40px;" name="dispatchDate" id="dispatchDate"/>
                            </td>
                            <td ><font color="red">*</font>Dispatch Time</td>
                            <td ><input type="time" class="form-control" style="width:260px;height:40px;" name="dispatchTime" id="dispatchTime"/>
                            </td>
                        </tr>
                    </table>


                    <br>
                    <center>
                        <input type="button" value="<spring:message code="stores.label.Delivery Note"  text="Update"/>" id="filer" class="btn btn-success" onClick="getDeliveryNotePrint();">                
                    </center>




                    <script>
                        $("#filer").click(function() {
                            window.location = 'uploadFiles/invoice.pdf';
                        });
                        
                        function getDeliveryNotePrint() {
                            var val = 1;
                            if ($("#vendorId").val() == "") {
                                alert("please Enter Transporter Name");
                                return;
                            } else if ($("#vehicleId").val() == "") {
                                alert("please Enter Vehicle No");
                                return;
                            } else if ($("#refNo").val() == "") {
                                alert("please Enter Reference No");
                                return;
                            } else if ($("#reportDate").val() == "") {
                                alert("please Enter Vehicle Reporting Date");
                                return;
                            } else {
                                window.open('/throttle/getDeliveryNotePrint.do?deliveryNoteId=' + val, "Print Run Sheet",
                                        "width=900,height=800,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            }
                        }

                        function generatePickList() {
                            alert("Delivery Note Generated ...!");
                        }
                    </script>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>

    </div>
</div>





<%@ include file="../common/NewDesign/settings.jsp" %>


