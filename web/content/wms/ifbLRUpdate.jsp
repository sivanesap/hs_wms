
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript">

    function savePage() {
        document.upload.action = "/throttle/ifbLRUpdate.do?&param=save";
        document.upload.method = "post";
        document.upload.submit();
    }
    function checkSelectStatus(sno, obj, ewayNo, ewayExpiry) {
        var val = document.getElementsByName("selectedStatus");
        if (ewayNo != "") {
            document.getElementById("ewayBillNoTemp" + sno).value = ewayNo;
        }
        if (ewayExpiry != "") {
            document.getElementById("ewayBillExpiryTemp" + sno).value = ewayExpiry;
        }
        if (obj.checked == true) {
            document.getElementById("ewayBillNo" + sno).disabled = false;
            document.getElementById("ewayBillExpiryDate" + sno).disabled = false;
            document.getElementById("selectedStatus" + sno).value = 1;
        } else if (obj.checked == false) {
            document.getElementById("selectedStatus" + sno).value = 0;
            document.getElementById("ewayBillNo" + sno).disabled = true;
            document.getElementById("ewayBillExpiryDate" + sno).disabled = true;
        }
        var vals = 0;
        for (var i = 0; i <= val.length; i++) {
        }
    }
    function setEwayNo(sno, ewayNo) {
        if (ewayNo != "") {
            document.getElementById("ewayBillNoTemp" + sno).value = ewayNo;
        }
    }
    function setEwayExpiry(sno, ewayExpiry) {
        if (ewayExpiry != "") {
            document.getElementById("ewayBillExpiryTemp" + sno).value = ewayExpiry;
        }
    }

    function viewMaterialDetails(lrId) {
        window.open('/throttle/ifbHubOut.do?lrId=' + lrId + '&param=serial', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="IFB LR Update"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="IFB LR Update"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
                <form name="upload"  method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <div id="ptp" style="overflow: auto">
                        <div class="inpad">
                            <table class="table table-info mb30 table-hover" id="table" >

                                <thead>
                                    <tr height="40">
                                        <th  height="30" >S.No</th>
                                        <th  height="30" >Invoice</th>
                                        <th  height="30" >LR No</th>
                                        <th  height="30" >Uploaded Date</th>
                                        <th  height="30" >Customer Name</th>
                                        <th  height="30" >Pincode</th>
                                        <th  height="30" >Box Qty</th>
                                        <th  height="30" >Amount</th>
                                        <th  height="30" >E-Way Bill No</th>
                                        <th  height="30" >E-Way Bill Expiry Date</th>
                                        <th  height="30" >Select</th>

                                    </tr>
                                </thead>
                                <% int index = 0;
                                    int sno = 1;
                                %>
                                <tbody>

                                    <c:if test = "${ifbLRList != null}" >
                                        <c:forEach items= "${ifbLRList}" var="ifb">
                                            <tr height="30">
                                                <td align="left"><%=sno%></td>
                                                <td align="left"   > <a href="#" onclick="viewMaterialDetails('<c:out value="${ifb.lrId}" />');"><u><c:out value="${ifb.invoiceNo}" /></u></a></td>
                                                <td align="left"   ><c:out value="${ifb.lrNo}"/></td>
                                                <td align="left"   ><c:out value="${ifb.createdDate}"/></td>
                                                <td align="left"   ><c:out value="${ifb.customerName}"/></td>
                                                <td align="left"   ><c:out value="${ifb.pincode}"/></td>
                                                <td align="left"   ><c:out value="${ifb.qty}"/></td>
                                                <td align="left"   ><c:out value="${ifb.amount}"/></td>
                                                <td align="left"> <input name="ewayBillNo" id="ewayBillNo<%=sno%>" onchange="setEwayNo('<%=sno%>', this.value)" value="<c:out value="${ifb.ewayBillNo}"/>" disabled  class="form-control" style="width:180px;height:35px"> </td>
                                                <td align="left"> <input name="ewayBillExpiryDate" id="ewayBillExpiryDate<%=sno%>" disabled onchange="setEwayExpiry('<%=sno%>', this.value)" value="<c:out value="${ifb.ewayExpiry}"/>"  class="datepicker form-control" style="width:180px;height:35px"> </td>
                                                <td align="left"   ><input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" value="<c:out value="${ifb.lrId}"/>" onclick="checkSelectStatus('<%=sno%>', this, '<c:out value="${ifb.ewayBillNo}"/>', '<c:out value="${ifb.ewayExpiry}"/>')"></td>
                                        <input type="hidden" name="ewayBillNoTemp" id="ewayBillNoTemp<%=sno%>" value="0"  class="form-control" style="width:180px;height:35px"> 
                                        <input type="hidden" name="ewayBillExpiryTemp" id="ewayBillExpiryTemp<%=sno%>" value="0"  class="datepicker form-control" style="width:180px;height:35px">
                                        <input name="lrId" id="lrId<%=sno%>" type="hidden" value="<c:out value="${ifb.lrId}"/>" class="form-control" style="width:250px;height:40px" >
                                        <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" /> 
                                        </tr>
                                        <%
                                            sno++;
                                            index++;
                                        %>
                                    </c:forEach>
                                    <input type="hidden" name="sno" id="sno" value="<%=sno%>">
                                </c:if>
                                </tbody>
                            </table>
                            <center>
                                <input type="button" class="btn btn-success"   value="Save" onclick="savePage()">
                            </center>

                        </div>
                    </div>    

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>