
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<script type="text/javascript">

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="vehicle.lable.SKU UPDATE"  text="SKU UPDATE"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="vehicle.lable.axleMaster"  text="axleMaster"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onLoad="setImages(1, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
                <form name="viewVehicleDetails"  method="post" >



                    <%@ include file="/content/common/message.jsp" %>

                    <br>

                    <%
                                int index = 1;

                    %>


                        <c:if test="${picklistMaster != null }" >
                        <table class="table table-info mb30 table-hover " id="table" >
                            <thead>
                                <tr>
                                    <th><spring:message code="trucks.label.Sno"  text="default text"/></th>
                                <th><spring:message code="trucks.label.ItemName"  text="Item Name"/></th>
                            <th><spring:message code="trucks.label.SkuCode"  text="Sku Code"/></th>
                            <th><spring:message code="trucks.label.HsnCode"  text="Hsn Code"/></th>
                            <th><spring:message code="trucks.label.View"  text="Serial List"/></th>
                            </tr>
                            </thead>
                            <tbody>
                                <%String style = "text1";%>
                                <c:forEach items="${picklistMaster}" var="veh" >
                                    <%
                                                if ((index % 2) == 0) {
                                                    style = "text1";
                                                } else {
                                                    style = "text2";
                                                }%>
                                    <tr>
                                        <td > <%= index++%> </td>
                                        <td > <c:out value="${veh.itemName}" /> </td>
                                        <td > <c:out value="${veh.skuCode}" /> </td>
                                        <td ><c:out value="${veh.hsnCode}" /></td>

                                        <td >
                                            <a href='/throttle/skuViewUpdate.do?itemId=<c:out value="${veh.itemId}" />'  >Show </a>
                                            &nbsp;
                                            &nbsp;
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <center>
                        <input type="button" class="btn btn-success" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                    </center>
                    <br>
                    <br>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>