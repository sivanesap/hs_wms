<%--
Document   : delivery note
Created on : oct 31, 2020, 4:06:44 PM
Author     : Hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Barcode</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>


</head>
<body>
    <center><b>BARCODE PRINT</b></center>
    <form name="waybillPrint">
        <br>     
        <br>     
                    <table style="font-family: Arial, Helvetica, sans-serif;width: 40%;"  align="center"  cellpadding="0"  cellspacing="0" border>
                            <thead>
                                <tr height="30">
                                    <th style="font-size:10px;">Item Code</th>
                                    <th style="font-size:10px;">Barcode</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% for(int i=1;i<11;i++){%>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">AC101RFH45 </td>
                                        <td align="center" style="font-size:10px;"><br><div id="inputdata<%=i%>"></div></td>
                         
                                        <script type="text/javascript">
                                        /* <![CDATA[ */
                                        function get_object(id) {
                                            var object = null;
                                            if (document.layers) {
                                                object = document.layers[id];
                                            } else if (document.all) {
                                                object = document.all[id];
                                            } else if (document.getElementById) {
                                                object = document.getElementById(id);
                                            }
                                            return object;
                                        }
                                        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                                        get_object("inputdata<%=i%>").innerHTML = DrawCode39Barcode('AC101RFH45', 0);
                                        /* ]]> */
                                    </script>
                    </tr>
                    <%}%>
                            </tbody>    
                    </table>
                    <br>
                    <br>
 
                </div>
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
        <%--</c:if>--%>
    </form>

</body>
</html>









