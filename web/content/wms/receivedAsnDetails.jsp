<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/></a></li>
            <li class="active">Receive GRN</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="grn" method="post">
                    <table class="table table-info mb30 table-bordered" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Customer</th>
                                <th>Product </th>
                                <th>Product Code</th>
                                <th>HSN Code</th>
                                <th>Qty</th>
                            </tr>
                        </thead>
                        <c:if test="${asnDetails != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${asnDetails}" var="asn">
                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${asn.custName}"/></td>
                                        <td><c:out value="${asn.itemName}"/></td>
                                        <td><c:out value="${asn.skuCode}"/></td>
                                        <td><c:out value="${asn.hsnCode}"/></td>
                                        <td><c:out value="${asn.requestQty}"/>
                                            <input type="hidden" name="selectedIndexs" id="selectedIndexs<%=sno - 1%>" value="0"/>
                                            <input type="hidden" name="asnId" id="asnId<%=sno - 1%>" value="<c:out value="${asn.asnId}"/>"/>
                                            <input type="hidden" name="itemId" id="itemId<%=sno - 1%>" value="<c:out value="${asn.itemId}"/>"/>
                                            <input type="hidden" name="custId" id="custId<%=sno - 1%>" value="<c:out value="${asn.custId}"/>"/>
                                            <input type="hidden" name="asnDetId" id="asnDetId<%=sno - 1%>" value="<c:out value="${asn.asnDetId}"/>"/>
                                            <input type="hidden" name="custName" id="custName<%=sno - 1%>" value="<c:out value="${asn.custName}"/>"/>
                                            <input type="hidden" name="itemName" id="itemName<%=sno - 1%>" value="<c:out value="${asn.itemName}"/>"/>
                                            <input type="hidden" name="skuCode" id="skuCode<%=sno - 1%>" value="<c:out value="${asn.skuCode}"/>"/>
                                            <input type="hidden" name="hsnCode" id="hsnCode<%=sno - 1%>" value="<c:out value="${asn.hsnCode}"/>"/>
                                            <input type="hidden" name="reqQuantity" id="reqQuantity<%=sno - 1%>" value="<c:out value="${asn.requestQty}"/>"/>
                                        </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>
                        </c:if>
                    </table>  
                    <table class="table table-info mb30 table-hover" id="serial">
                            <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                            </div>
                        <tr>
                            <td align="center">Input Qty</td>
                            <td><input type="text" id="inpQty" name="inpQty" value="" class="form-control" style="width:100px; height:30px"> 
                       </td>
                        </tr>
                        <tr>
                            <td>
                                Product Condition
                            </td>
                            <td><select id="proCon" name="proCon" style="width:250px;height:40px">
                                <option value="Good">Good</option>
                                <option value="Damaged">Damaged</option>>
                            </select></td>
                            
                            <td>
                                UOM
                            </td>
                            <td><select id="Uom" name="Uom" style="width:250px;height:40px">
                                <option value="Pack">Pack</option>
                                <option value="Piece">Piece</option>>
                            </select></td>
                        </tr>
                        <tr>
                            <td align="center">
                                SERIAL NO : 
                            </td>
                            <td>
                                <input type="text" id="serialNo" name="serialNo" value="" class="form-control" style="width:250px" onchange="saveTempSerialNos();"/> 
                            </td>
                        </tr>
                        
                    </table>                        
                    
                    <br> 
    <div id="tableContent"></div>
<!--                    <div id="receivedGRNTempDetails">
                   </div>-->
                        <script>
                            var serial = [];
                            var ipqt=[];
                            var uomarr=[];
                            var sno=[];
                            var procon=[];
                            var arr=[];
                            var i=0;
                            function saveTempSerialNos() {
//                                alert("sssss")
//                                var insertStatus=0;
//                                var serialNo = document.grn.serialNo.value;
//                                var asnId = document.grn.asnId.value;
//                                var itemId = document.grn.itemId.value;
//                                var ipQty= document.grn.ipQty.value;
//                                var proCon=document.grn.proCon.value;
//                                var uom=document.grn.uom.value;
//              
//                                   var statusName = "";
                                   serial[i]=serialNo.value;
                                   ipqt[i]=inpQty.value;
                                   uomarr[i]=Uom.value;
                                   procon[i]=proCon.value;
                                   sno[i]=i+1;
                                   i++;
                                   const tmpl = (sno,serial,uomarr,procon,ipqt) => `
                                   <table class="table table-info mb30 table-bordered" id="table"><thead>
                                   <tr><th>S.No<th>Serial No<th>UOM<th>Product Condition<th>Input Qty<tbody>
                                   ${sno.map( (cell, index) => `
                                   <tr><td><input type="hidden" id="sNo" name="sNo" value="${cell}" />${cell}<td><input type="hidden" id="serialNos" name="serialNos" value="${serial[index]}" />${serial[index]}<td><input type="hidden" id="uom" name="uom" value="${uomarr[index]}" />${uomarr[index]}<td><input type="hidden" id="proCond" name="proCond" value="${procon[index]}" />${procon[index]}<td><input type="hidden" id="ipQty" name="ipQty" value="${ipqt[index]}"/>${ipqt[index]}</tr>
                                   `).join('')}
                                   </table>`;
                                    tableContent.innerHTML=tmpl(sno,serial,uomarr,procon,ipqt);
//                                   arr=[serial[],ipqt[],uomarr[]];
//                                   alert(arr[]);
//                                   var url = "";
////                                   if (serialNo != "") {
//                                       url = "./saveSerialTempDetails.do";
//                                       statusName = "Added";
////                                   }
//                                    $('#receivedGRNTempDetails').html('');
//                                   $.ajax({
//                                       url: url,
//                                       data: {serialNo: serialNo,asnId: asnId, itemId: itemId,  ipQty: ipQty, proCon:proCon, uom:uom
//                                       },
//                                       type: "GET",
//                                       success: function(response) {
////                                           alert("eee");
////                                          String temp[] = response.split(~);
//                                           if (serialNo == "0"){
//                                               insertStatus = 0;
//                                               $("#StatusMsg").text("SerialNo "+serialNo+" "+statusName + " failed.").css("color", "red");
//                                           } else if(serialNo != "" && serialNo !="0") {
////                                               $("#StatusMsg").text("SerialNo "+serialNo+" "+statusName + " sucessfully.").css("color", "green");
//                                           }
//                                           $('#receivedGRNTempDetails').html(response);
//                                       },
//                                       error: function(xhr, status, error) {
//                                       }
//                                   });
//                                   $("#serialNo").val("");
//                                   return insertStatus;

                               }
                              
                        </script>
                        
                         <script>
                            function resetTheDetails() {
                                $('#receivedGRNTempDetails').html('');
                            }
                            function saveGrnSerialNos() {
                                document.grn.action = '/throttle/saveGrnSerialDetails.do';
                                document.grn.submit();
                            }
                         </script>
                         
                        <center>
                            <input type="button" class="btn btn-success" name="Update" value="Update" onclick="saveGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

