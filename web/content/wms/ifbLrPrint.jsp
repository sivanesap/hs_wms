<html>
    <head>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script src="/throttle/js/JsBarcode.all.min.js"></script>
    <style type='text/css'>
        body{
            color:#000000; background-color:#ffffff;
            font-family: "Times New Roman", Times, serif; font-size:10pt;}

        table, th, td {
            border: 1px solid black;
            padding:5px;
            border-collapse: collapse;
        }
    </style>
    <script>
    JsBarcode("#barcode", "30021050470000");
    </script>
    <style type="text/css">
        @media print {
            #printbtn {
                display :  none;
            }
        }
    </style>
    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
        }
    </style>
</head>

<body>
    <div style="border:2px #126879 double;width:640px;height:470px;alignment-adjust: center;margin-top:50px;margin-left: 30px">
        <div style="width:60%;text-align: center;height: 50px;padding-top: 10px;font-family:Times;float: left ">
            <font size="4.5"><b>DOMESTIC CONSIGNMENT NOTE</b></font><br>
            <font size="3.5"><c:out value="${companyName}"/><br>Transporter ID:</font><br><br>
           <div style="width:90%;line-height: 18px;text-align: left;height:120px;margin-left: 30px;border:1.5px black solid;padding-left: 5px;font-size: 13px">
            From (Sender/Consignor)<br> <b>Company : <c:out value="${custName}"/></b><br>
            <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
            <div style="width:81%;float:right"><c:out value="${custAddress}"/></div>
        </div><br>
            <div style="width:90%;text-align: left;height:120px;margin-left: 30px;border:1.5px black solid;padding-left: 5px">
            To (Receiver/Consignee)
        </div>
        </div>
        <div style="width:30%;text-align: center;height: 50px;padding-top: 10px;font-family: Times;float: right">
            <img src="images/HSSupplyLogo.png" style="width:58px;height:60px">
            <div>
                
            </div>
        </div>
        

    </div>
    <div style="border:2px #126879 double;width:640px;height:470px;alignment-adjust: center;margin-top:50px;margin-left: 30px">
        <div style="width:60%;text-align: center;height: 50px;padding-top: 10px;font-family:Times;float: left ">
            <font size="4.5"><b>DOMESTIC CONSIGNMENT NOTE</b></font><br>
            <font size="3.5"><c:out value="${companyName}"/><br>Transporter ID:</font><br><br>
           <div style="width:90%;line-height: 18px;text-align: left;height:120px;margin-left: 30px;border:1.5px black solid;padding-left: 5px;font-size: 13px">
            From (Sender/Consignor)<br> <b>Company : <c:out value="${custName}"/></b><br>
            <div style="width:19%;float:left"><b>Address&nbsp;&nbsp;&nbsp;:</b></div>
            <div style="width:81%;float:right"><c:out value="${custAddress}"/></div>
        </div><br>
            <div style="width:90%;text-align: left;height:120px;margin-left: 30px;border:1.5px black solid;padding-left: 5px">
            To (Receiver/Consignee)
        </div>
        </div>
        <div style="width:30%;text-align: center;height: 50px;padding-top: 10px;font-family: Times;float: right">
            <img src="images/HSSupplyLogo.png" style="width:58px;height:60px">
        </div>

    </div>
</body>