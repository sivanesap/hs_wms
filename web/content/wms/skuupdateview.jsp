
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<!--FOR google City Starts-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script type="text/javascript">
    function submitpage()
    {
        if (textValidation(document.dept.serialNumber, "serialNumber")) {
            return;
        }
        document.dept.action = "/throttle/handleUpdateSku.do";
        document.dept.submit();
    }




    function setValues(sno, serialNumber) {
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("serialNumber").value = serialNumber;
        if (serialNumber == 1) {
            document.getElementById("serialNumber").checked = true;
        } else {
            document.getElementById("serialNumber").checked = false;
        }
    }
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Mfr"  text="Make"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="trucks.label.FleetName"  text="Fleet"/></a></li>
            <li class="active"><spring:message code="stores.label.Mfr"  text="Make"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body>
                <form name="dept"  method="post">

                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th colspan="2"  ><div ><spring:message code="trucks.label.AddManufacturer"  text="default text"/></div></th>
                        </tr>
                        </thead>
                        <tr>
                            <td  ><font color=red>*</font>Serial Number</td>
                            <td  ><input name="serialNumber" maxlength="35" id="serialNumber" type="text" class="form-control" style="width:200px;height:40px;" ></td>
                        </tr>

                        <input name="perc" maxlength="10" type="hidden" class="form-control" style="width:200px;height:40px;" value="0">
                        <tr>

                            <td  ><font color=red>*</font>Item Name </td>
                            <td>
                                <select  style="width:200px;height:40px" type="text" class="form-control" id="itemId" name="itemId">
                                    <option value="">-----------select-----------</option>
                                    <c:forEach items="${getItemList}" var="item">
                                        <option value="<c:out value="${item.itemId}"/>">
                                            <c:out value="${item.itemName}"/></option>



                                    </c:forEach>



                                </select>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td  ><font color=red>*</font><spring:message code="trucks.label.ActivityGroup"  text="default text"/></td>
                            <td  >
                                <select  style='width:200px;' class="form-control" style="width:260px;height:40px;" id="groupId"  name="groupId" >
                                    <option  value=0>---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                    <option  selected value=1><spring:message code="trucks.label.VolvoGroup"  text="default text"/></option>
                                    <option  value=2><spring:message code="trucks.label.NonVolvoGroup"  text="default text"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none;" >
                            <td  ><font color=red>*</font><spring:message code="trucks.label.BodyWorksGroup"  text="default text"/></td>
                            <td  >
                                <select  style='width:200px;' class="form-control" style="width:260px;height:40px;" id="bodyGroupId"  name="bodyGroupId" >
                                    <option  value=0>---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                    <option  value=3><spring:message code="trucks.label.Cargo"  text="default text"/></option>
                                    <option  value=4><spring:message code="trucks.label.HighEnd(Luxury)"  text="default text"/></option>
                                    <option  selected value=5><spring:message code="trucks.label.Ordinary"  text="default text"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        </tr>

                    </table>
                    <center>
                        <input type="button" value="<spring:message code="trucks.label.Save"  text="default text"/>" class="btn btn-success" onclick="submitpage();">
                        &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="trucks.label.Clear"  text="default text"/>">
                    </center>
                    <br>

                    <table class="table table-info mb30 table-hover " id="table">

                        <thead>

                            <tr >
                                <th>S.No</th>
                                <th>Serial No</th>
                                <th>SKU Code</th>
                                <th align="right">Edit</th>
                            </tr>
                        </thead>
                        <tbody>


                            <% int sno = 0;%>
                            <c:if test = "${picklistMaster != null}">
                                <c:forEach items="${picklistMaster}" var="cml">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td   align="left"> <%= sno%> </td>
                                        <td   align="left"><c:out value="${cml.serialNumber}"/></td>
                                        <td   align="left"><c:out value="${cml.itemName}"/></td>
                                        <td  align="left"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cml.serialNumber}" />');" /></td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        </c:if>
                        <input type="hidden" name="mfrId" id="mfrId" > 
                    </table>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    
                    <br>
                    <!--<div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>-->

                    </body>
                    </div>


                    </div>
                    </div>





                    <%@ include file="/content/common/NewDesign/settings.jsp" %>