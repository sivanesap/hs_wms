
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.IFB Hub In"  text="IFB Hub Receive"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.IFB Hub Receive"  text="Master"/></a></li>
            <li class="active">IFB Hub Receive</li>
        </ol>
    </div>
</div>

<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>  

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="hubin" method="post">
                     <table class="table table-info mb30 table-hover"  >
                        <thead>
                        <th colspan="4">Invoice Scanning</th>
                        </thead>
                        <tr>
                            <td>Invoice No</td>
                            <td><input type="text" id="serial" class="form-control" style="width:240px;height: 40px" name="serial" value="" onchange="scanningArray(this.value);"></td>
                        </tr>
                    </table>

                    <table class="table table-info mb30 table-hover" id="table"  >	
                        <thead>
                            <tr height="30">
                                <th>S.No</th>
                                <th>Customer Name</th>
                                <th>LR No</th>
                                <th>Invoice No</th>
                                <th>Invoice Date</th>  
                                <th>HubOut Date</th>
                                <th>From Warehouse</th>  
                                <th>Total Qty</th>
                                <th>City</th>
                                <th>Pincode</th>
                                <th>Gross Value</th>
                                <th>Hub In Eway_Bill No</th>
                                <th>Hub In Eway_Bill Expiry</th>
                                <th>Vehicle No</th>
                                <th>Driver Name</th>
                                <th>Driver Mobile</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <%
                            int sno = 0;
                        %>
                        <tbody>
                            <c:forEach items="${ifbHubinList}" var="pc">
                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>

                                <tr>
                                    <td class="<%=className%>"  align="left"> <%=sno%> </td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.customerName}" /></td>
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.lrNo}" /></td>  
                                    <td class="<%=className%>"  align="left">  <a href="#" onclick="viewMaterialDetails('<c:out value="${pc.orderId}" />');"><u><c:out value="${pc.invoiceNo}" /></u></a></td>  
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.invoiceDate}" /></td>  
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.currentHub}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.qty}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.createdDate}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.city}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.pincode}" /></td> 
                                    <td class="<%=className%>"  align="left"> <c:out value="${pc.grossValue}" /></td> 
                                    <td class="<%=className%>" align="left"> <c:out value="${pc.ewayBillNo}"/></td>
                                    <td class="<%=className%>" align="left"> <c:out value="${pc.ewayExpiry}"/></td>
                                    <td class="<%=className%>" align="left"> <c:out value="${pc.vehicleNo}"/></td>
                                    <td class="<%=className%>" align="left"> <c:out value="${pc.driverName}"/></td>
                                    <td class="<%=className%>" align="left"> <c:out value="${pc.mobileNo}"/></td>
                                    <td align="left"   ><input type="checkbox" id="selectedIndex<%=sno%>" name="selectedIndex" disabled value="<c:out value="${pc.lrId}"/>" onclick="checkSelectStatus('<%=sno%>', this)"></td>
                            <input type="hidden" name="selectedStatus" id="selectedStatus<%=sno%>" value="0" /> 
                            <input type="hidden" name="orderId" id="orderId<%=sno%>" value="<c:out value="${pc.orderId}" />" /> 
                            <input type="hidden" name="serNo" id="serNo<%=sno%>" value="<c:out value="${pc.invoiceNo}" />" /> 
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <input type="hidden" name="count" id="count" value="<%=sno%>" />

                    <br>
                    <script>
                        var arr1 = [];
                        var arr2 = [];
                        var ser = document.getElementById("count").value;
//                            alert(ser);
                        var i = 0;
                        for (var x = 1; x <= ser; x++) {
                            arr1[i] = document.getElementById("serNo" + x).value;
                            i++;
                        }
                        function scanningArray(value) {
                            if (arr1.indexOf(value) != -1) {
                                if (arr2.indexOf(value) != -1) {
                                    alert("Already Scanned");
                                    document.getElementById("serial").value = "";
                                } else {
                                    arr2.push(value);
                                    for (var k = 1; k <= ser; k++) {
                                        if (document.getElementById("serNo" + k).value === value) {
                                            document.getElementById("selectedIndex" + k).checked = true;
                                            document.getElementById("selectedStatus" + k).value = "1";
                                        }
                                    }
                                    document.getElementById("serial").value = "";
                                }
                            } else {
                                alert("Not a Listed Invoice");
                                document.getElementById("serial").value = "";
                            }
                            return arr2;
                        }
                        function checkSelectStatus(sno, obj) {
                            var val = document.getElementsByName("selectedStatus");
                            if (obj.checked == true) {
                                document.getElementById("selectedStatus" + sno).value = 1;
                            } else if (obj.checked == false) {
                                document.getElementById("selectedStatus" + sno).value = 0;
                            }
                            var vals = 0;
                            for (var i = 0; i <= val.length; i++) {
                            }
                        }
                        function submitPage() {
                            if (arr2.length >= 1) {
                                    document.getElementById("hub").style.display="none";
                                    document.hubin.action = "/throttle/ifbHubIn.do?&param=update";
                                    document.hubin.submit();
                                } else {
                                alert("Select atleast One Order to Hub Out")
                            }
                        }
                        function viewMaterialDetails(lrId) {
                            window.open('/throttle/ifbHubOut.do?lrId=' + lrId + '&param=serial', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                        }
                    </script>
                    <center>
                        <input type="button" class="btn btn-success" name="hub" id="hub" value="Update" onclick="submitPage()" />
                    </center>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>


                </form>
            </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>

    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

