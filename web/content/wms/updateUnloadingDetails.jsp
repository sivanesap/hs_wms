<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Update Unloading Details"  text="Update Unloading Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Update Unloading Details"  text="Inbound"/></a></li>
            <li class="active">Update Unloading Details</li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload="">
                <form name="gtn" id="gtn" method="post">

                    <table class="table table-info mb30 table-hover" id="serial">
                        <div align="center" style="height:20px;" id="StatusMsg">&nbsp;&nbsp;
                        </div>
                        <c:if test="${gtnDetails != null}">

                            <tr>
                                <td align="center"><b>Unloader Type</b></td>
                                <td>
                                    <select id="type" name="type" onchange="setVal(this.value);" class="form-control" style="height:40px;width:240px;text-align: center">
                                        <option  value="0">------Select One------</option>
                                        <option value="1">Inhouse</option>
                                        <option value="2">Contract Labour</option>
                                    </select>
                                </td>
                                <td align="center" id="hour" style="display:none">Hour Cost</td>
                                <td align="center" id="tot" style="display:none">Total Cost</td>
                                <td id="hour1" style="display:none">
                                    <input id="hourCost" class="form-control" name="hourCost" class="form-control" style="height:40px;width:240px"/>
                                    
                                </td>
                                <td id="tot1" style="display:none">
                                    <input id="totalCost" class="form-control" name="totalCost" class="form-control" style="height:40px;width:240px"/>
                                    
                                </td>
                            </tr>     
                        </c:if>
                        <tr>
                            <td align="center">
                                <b>VAS</b>
                            </td>
                            <td>

                                <select id="vas" name="vas" style="width:240px;height:40px" class="form-control" onchange="showActivities(this.value);">
                                    <option  value="0">------Select One------</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </td>
                            <td align="center" id="act" style="display:none">Activities</td>
                            <td id="act1" style="display:none"><select id="Activities" name="Activities" style="width:240px;height:40px" class="form-control">
                                    <option value="1">Repacking</option>
                                    <option value="2">Stickering</option>
                                    <option value="3">Other Vas</option>
                                </select>   
                            </td>
                        </tr>
                        <tr id="act2" style="display:none">
                            <td align="center">
                                Unit Price
                            </td>
                            <td>
                                <input type="text" class="form-control" id="unitPrice" name="unitPrice" style="width:240px;height:40px"/>
                            </td>
                            <td align="center">
                                Total Units
                            </td>
                            <td>
                                <input type="text" class="form-control" id="totalUnits" name="totalUnits" style="width:240px;height:40px"/>
                            </td>
                        </tr>
                        <tr id="act3" style="display:none">
                            <td align="center">
                                Total Cost
                            </td>
                            <td>
                                <input type="text" class="form-control" id="totalCost" name="totalCost" style="width:240px;height:40px"/>
                            </td>
                        </tr>
                    </table>                        

                    <br> 
                    <div id="tableContent"></div>
                    <script>
                        function setVal(value) {
                            if (value == 0) {
                                $('#hour').hide();
                                $('#hour1').hide();
                                $('#tot').hide();
                                $('#tot1').hide();
                            } else if (value == 1) {
                                $('#tot').hide();
                                $('#tot1').hide();
                                $('#hour').show();
                                $('#hour1').show();
                            } else {
                                $('#tot').show();
                                $('#tot1').show();
                                $('#hour').hide();
                                $('#hour1').hide();
                            }
                        }
                        function showActivities(val) {
                            if (val == 1) {
                                $('#act').show();
                                $('#act1').show();
                                $('#act2').show();
                                $('#act3').show();
                            } else {
                                $('#act').hide();
                                $('#act1').hide();
                                $('#act2').hide();
                                $('#act3').hide();
                            }
                        }
                    </script>

                    <script>
                        function saveGrnSerialNos() {
                            document.gtn.action = '/throttle/updateUnloadingDetails.do?param=save';
                            document.gtn.submit();
                        }
                    </script>

                    <center>
                        <input type="button" class="btn btn-success" name="Update" value="Update" onclick="saveGrnSerialNos(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                    </center>
                </form>
                </body>
        </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
    </div>
</div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

