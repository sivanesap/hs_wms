<%--
Document   : delivery note
Created on : oct 31, 2020, 4:06:44 PM
Author     : Hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delivery Note Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>


</head>
<body>
    <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
        <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>
                <div id="printDiv" >
                    <br><center><u><b>Dispatch Challan</b></u></center><br>
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;"  align="center"  cellpadding="0"  cellspacing="0" border="1">
                        <tr >
                            <td  style="margin: 0px;height:25px;width:175px;font-weight:bold;">
                                <div style="width:200px;height:35px;float:left;display:block;"><img src="/throttle/images/HSSupplyLogo.png" style="width: 100px; height: 60px;"></div>                                
                            </td>
                           <td align="left" style="height:25px;width:525px;font-size:10px;font-weight:bold;size: 5px;">
                                &ensp;HS Supply Solution Pvt Ltd<br/>
                                <br/>
                                &ensp;No 30, Chandralaya Apartment, 
                                <br>&ensp;Judge Jambulingam Rd, Mylapore,
                                <br>&ensp;Chennai,Tamil Nadu. 600004<br/>
                            </td>
                            <td >
                                <div id="externalbox" style="width:3in;margin: 10px;">
                                    <div align="center" style="font-size:12px;"><b>Delivery Note Ref No</b></div>
                                    <div id="inputdata"></div>
                                    <div align="center" style="font-size:12px;"><b>Delivery Note Date : 01-11-2020 10:10:00</b></div>
                                </div>
                            </td>
                        </tr>
<!--                    </table>
                    <table style="font-family: Arial, Helvetica, sans-serif;border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:solid 1px;border-left:solid 1px;width: 100%;height:100px;" align="center"  cellpadding="0"  cellspacing="0" >-->
                        <tr>
                            <td style="height:30px;width:175ppx;font-weight:bold;">
                                <%--<c:set var="runSheetRefNo" value="" />--%>
                                <div>
                                    <table>                                       
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Delivery Location </b></td>
                                            <td style="font-size:10px;">: Coimbatore
                                            </td>
                                        </tr>
                                         <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>No Of pick List </b></td>
                                            <td style="font-size:10px;">: 6</td>
                                        </tr>
                                         <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>No Of Pack List </b></td>
                                            <td style="font-size:10px;">: 2</td>
                                        </tr>
<!--                                        <tr style="height: 20px">
							<td style="font-size:10px;"><b>Opening KM: <c:out value="${run.startKm}"/></b></td>
							 <td style="font-size:10px;" align="center"><b>&emsp;Closing KM: <c:out value="${run.endKm}"/></b></td>
					                                            
                                        </tr>-->
                                    </table>
                                </div>
                            </td>
                            <td align="left" style="height:30px;width:550px;font-weight:bold;">
                                <div>
                                    <table>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px"><b>Transporter </b></td>
                                            <td style="font-size:10px;">: 
                                               National Transport
                                            </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Driver </b></td>
                                            <td style="font-size:10px;">: 
                                                Ragu
                                            </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Driver Mobile</b></td>
                                            <td style="font-size:10px;">: 
                                                984578889
                                            </td>
                                        </tr>
                                        
                                        
                                    </table>
                                </div>
                            </td>
                            <td style="height:30px;width:275px;text-align: right">
                                <div>
                                    <table>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;">&emsp;<b>Vehicle No</b></td>
                                            <td style="font-size:10px;"> : TN13OA8654 </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Packing</b></td>
                                            <td style="font-size:10px;"> : Pack/20-21/001 , Pack/20-21/002</td>
                                        </tr>
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Vehicle Type </b></td>
                                            <td style="font-size:10px;">: <c:out value="${run.vehicleTypeName}"/></td>
                                        </tr>-->
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Starting Time: <c:out value="${run.startDate}"/></b></td>
                                            <td style="font-size:10px;" align="center"><b>&emsp;&emsp;&emsp;&emsp;Closing Time: <c:out value="${run.endDate}"/></b></td>
                                        </tr>
                                        -->

                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                                     
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;"  align="center"  cellpadding="0"  cellspacing="0" border>
                            <thead>
                                <tr height="40">
                                    <th style="font-size:10px;width: 5%;">S.No</th>
                                    <th style="font-size:10px;width: 15%;">Item Code</th>
                                    <th style="font-size:10px;width: 15%;">Delivery Note No</th>
                                    <th style="font-size:10px;width: 8%;">Company Name</th>
                                    <th style="font-size:10px;width: 7%;">Spec</th>
                                    <th style="font-size:10px;width: 7%;">Company Code</th>
                                    <!--<th style="font-size:10px; width: 15%;">Group</th>-->
                                    <th style="font-size:10px; width: 17%;">Order Qty</th>
                                    <th style="font-size:10px;width: 10%;">UOM</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">1</td>
                                        <td align="center" style="font-size:10px;">AC101RFH45 </td>
                                        <td align="center" style="font-size:10px;">1337 </td>
                                        <td align="center" style="font-size:10px;">Voltas </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH45</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">2</td>
                                        <td align="center" style="font-size:10px;">COOKER101 </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">2019 </td>
                                        <td align="center" style="font-size:10px;">COOKER101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">3</td>
                                        <td align="center" style="font-size:10px;">COOKER102 </td>
                                        <td align="center" style="font-size:10px;">1340 </td>
                                        <td align="center" style="font-size:10px;">Prestige </td>
                                        <td align="center" style="font-size:10px;">2018 </td>
                                        <td align="center" style="font-size:10px;">COOKER102</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">4</td>
                                        <td align="center" style="font-size:10px;">AC101RFH50 </td>
                                        <td align="center" style="font-size:10px;">1341 </td>
                                        <td align="center" style="font-size:10px;">kelvinator </td>
                                        <td align="center" style="font-size:10px;">2018 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH50</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">20.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">5</td>
                                        <td align="center" style="font-size:10px;">MIXIE101 </td>
                                        <td align="center" style="font-size:10px;">1342 </td>
                                        <td align="center" style="font-size:10px;">Butterflty </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">MIXIE101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">20.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">6</td>
                                        <td align="center" style="font-size:10px;">Fridge101 </td>
                                        <td align="center" style="font-size:10px;">1342 </td>
                                        <td align="center" style="font-size:10px;">Samsung </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">Fridge101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">4.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>                               -->
                                    </tr>
                            </tbody>    
                    </table>
                </div><br>
            <br>
            <br><center><u><b>Gate Pass</b></u></center><br>
            <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;border:2px solid black"  align="center"  cellpadding="0"  cellspacing="0">
                        <tr >
                            <td colspan="3" style="margin: 0px;height:110px;width:400px;font-size:12px;font-weight:bold;size: 5px;">
                                <div style="width:50px;padding-bottom:30px ;height:35px;float:left;display:block;padding-left: 270px"><img src="/throttle/images/HSSupplyLogo.png" style="width: 100px; height: 60px;"><br></div>                                
                                <div style="padding-left:50px;width:200px;height:35px;float:right;display:block;padding-right: 270px">  &ensp;<font size="2.5"><u>HS Supply Solution Pvt Ltd</u></font>
                                <br/>
                                &ensp;No 30, Chandralaya Apartment, 
                                <br>&ensp;Judge Jambulingam Rd, <br>
                                &ensp;Mylapore,
                                <br>&ensp;Chennai,Tamil Nadu. 600004<br/><br><br></div>                                
                            </td>
                           
                        </tr>
                        <tr >
                            <td style="height:70px;width:350px;border-right: none">
                                <%--<c:set var="runSheetRefNo" value="" />--%>
                                <div>
                                    <table>                                       
                                        <tr style="height: 30px">
                                            <td colspan="2" style="font-size:10px;"><b>Vehicle No</b>&emsp; :&emsp;&emsp;&emsp; TN13OA8654 </td>
                                        </tr>
                                         <tr style="height: 30px">
                                             <td colspan="2" style="font-size:10px;"><b>Transporter</b>&nbsp;&nbsp;&nbsp;:&emsp;&emsp;&emsp; National Transport</td>
                                       </tr>
                                         <tr style="height: 30px">
                                           <td colspan="2" style="font-size:10px;"><b>Driver</b>&emsp;&emsp;&emsp; :&emsp;&emsp;&emsp; Ragu</td>
                                       
                                        </tr>
<!--                                        <tr style="height: 20px">
							<td style="font-size:10px;"><b>Opening KM: <c:out value="${run.startKm}"/></b></td>
							 <td style="font-size:10px;" align="center"><b>&emsp;Closing KM: <c:out value="${run.endKm}"/></b></td>
					                                            
                                        </tr>-->
                                    </table>
                                </div>
                            </td>
                            <td align="left" style="height:80px;width:450px;border-right: none;border-left: none">
                                <div>
                                    <table>
                                        <tr style="height: 30px">
                                            <td colspan="2" style="font-size:10px;"><b>Invoice No</b>&emsp; :&emsp;&emsp;&emsp; 202105179091 </td>
                                        </tr>
                                         <tr style="height: 30px">
                                             <td colspan="2" style="font-size:10px;"><b>LR No</b>&emsp;&emsp;&emsp;&nbsp;:&emsp;&emsp;&emsp; LR/2021/00012</td>
                                       </tr>
                                         <tr style="height: 30px">
                                           <td colspan="2" style="font-size:10px;"><b>Quantity</b>&emsp;&emsp; :&emsp;&emsp;&emsp; 74</td>
                                       
                                        </tr>
                                        </tr>
                                        
                                        
                                    </table>
                                </div>
                            </td>
                            <td style="height:90px;width:350px;text-align: left;border-left: none">
                                <div>
                                    <table>
                                        <tr align="left" style="height: 30px">
                                            <td colspan="2" style="font-size:10px;"><b>Vehicle Out Time</b>: 17-05-2021 15:40:00 </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td colspan="2" style="font-size:10px;"><b>Driver Mobile</b>&emsp;&nbsp;&nbsp;: &nbsp;984578889 </td>
                                        </tr>
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Vehicle Type </b></td>
                                            <td style="font-size:10px;">: <c:out value="${run.vehicleTypeName}"/></td>
                                        </tr>-->
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Starting Time: <c:out value="${run.startDate}"/></b></td>
                                            <td style="font-size:10px;" align="center"><b>&emsp;&emsp;&emsp;&emsp;Closing Time: <c:out value="${run.endDate}"/></b></td>
                                        </tr>
                                        -->

                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right" style="font-size: 12px;font-weight: bold;padding:20px;padding-top: 60px">Authorised Signatory</td>
                        </tr>
                        </table>
            <br><br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
        <%--</c:if>--%>
    </form>
    <script type="text/javascript">
        /* <![CDATA[ */
        function get_object(id) {
            var object = null;
            if (document.layers) {
                object = document.layers[id];
            } else if (document.all) {
                object = document.all[id];
            } else if (document.getElementById) {
                object = document.getElementById(id);
            }
            return object;
        }
        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
        get_object("inputdata").innerHTML = DrawCode39Barcode('DN0001', 0);
        /* ]]> */
    </script>
</body>
</html>









