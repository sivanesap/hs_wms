<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript">
    function submitPage(value) {

        if (value == 'Update') {
            document.repair.action = '/throttle/customerhaltingapproval.do';
            document.repair.submit();
        }
    }

</script>       

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Received ASN"  text="Received ASN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Received ASN"  text="Received ASN"/></a></li>
            <li class="active">Received ASN</li>
        </ol>
    </div>
</div>

<script>

    function setValue() {

        var selectedTrip = document.getElementsByName('selectedIndex');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedTrip.length; i++) {
            if (selectedTrip[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }
    }

    function searchPage(val) {
        var selectedLhc = document.getElementsByName('selectedIndex');
        var lhcId = document.getElementsByName('lhcId');
        var approvStatus = document.getElementsByName('approvStatus');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedLhc.length; i++) {
//            alert("i="+i);
            if (selectedLhc[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
//                alert("selected :"+selectedTrip[i].value);
//                alert("trip expense id :"+tripExpenseId[i].value);
//                alert("approvStatus"+approvStatus[i].value);
                if (cntr == 0) {
                    temp = lhcId[i].value;
                } else {
                    temp = temp + "," + lhcId[i].value;
                }
                cntr++;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }

        if (temp != "") {
//            alert("Hai:"+temp); 
            document.repair.action = "/throttle/lhcApproval.do?param=" + val + "&lhcIds=" + temp;
            document.repair.submit();

        } else {
            alert("Please select any one and proceed....");
        }

    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);">
                <form name="repair" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <table  class="table table-info mb30 table-hover"  align="left" style="width:50%;">
                        <tr style="display:none;" >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="">
                            </td>
                            <td colspan="4" >&nbsp;</td>
                        </tr>
                    </table>
                    <c:if test="${asnMasterList == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Asn No</th>
                                <th>Asn Req No</th>
                                <th>Asn Date</th>
                                <th>Vendor Name</th>
                                <th>Product Name</th>
                                <th>Asn Remarks</th>
                                <th>Asn Quantity</th>
                                <th>GateIn Quantity</th>
                                <th>Pending Quantity</th>
                                <th>Asn Path</th>
                            </tr>

                        </thead>
                        <c:if test="${asnMasterList != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${asnMasterList}" var="asn">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${asn.asnNo}"/></td>
                                        <td ><c:out value="${asn.asnReqId}"/></td>
                                        <td><c:out value="${asn.asnDate}"/></td>
                                        <td><c:out value="${asn.vendorName}"/></td>
                                        <td><c:out value="${asn.itemName}"/></td>
                                        <td><c:out value="${asn.asnRemarks}"/></td>
                                        <td><c:out value="${asn.asnQty}"/></td>
                                        <td><c:out value="${asn.poQuantity}"/></td>
                                        <td><c:out value="${asn.asnQty-asn.poQuantity}"/></td>
                                        <td align="left" ><a href="<c:out value="${asn.asnPath}"/>" target="_blank" download>
                                                <span class="label label-Primary"><font size="2">PDF</font></span> </a>
                                        </td>
                                    </tr>

                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>

                        </c:if>


                    </table>
                    <div>
                        <center>

                            <!--<input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;-->
                        </center>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                          
                        
                    <script type="text/javascript">
                        
                    </script>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

