<%-- 
    Document   : saveRoute
    Created on : Feb 17, 2021, 2:41:43 PM
    Author     : hp
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="ets.domain.customer.business.CustomerTO" %>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>
</head>
<script language="javascript">
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#custName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerName.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#custName').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#custName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    $(document).ready(function () {
        $('#customerCode').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getCustomerCode.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#customerId').val('');
                            $('#customerCode').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#customerId').val(tmp[0]);
                $('#customerCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    //    function checkValue(value,id){
    //                if(value == '' && id=='custName'){
    //                   $('#customerCode').attr('readonly', true);
    //                   document.getElementById('customerId').value = '';
    //                }
    //                if(value == '' && id=='customerCode'){
    //                   $('#custName').attr('readonly', true);
    //                   document.getElementById('customerCode').value = '';
    //                }
    //            }
    //

    function submitPage(value) {
        if (value == "add") {
            document.manufacturer.action = '/throttle/handleViewAddCustomer.do';
            document.manufacturer.submit();
        } else if (value == 'search') {
            document.manufacturer.action = '/throttle/handleViewCustomer.do';
            document.manufacturer.submit();
        }
    }

    function viewConsignorDetails(custId,custName) {
        window.open('/throttle/handleConsignorUpdate.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
                                                       
     function viewConsigneeDetails(custId,custName) {
        window.open('/throttle/handleConsigneeUpdate.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
     function uploadCustContractDetails(custId,custName) {
        window.open('/throttle/handleUploadCustContract.do?custId='+custId + "&custName="+custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.RouteDetails" text="Route Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
            <li class=""><spring:message code="hrms.label.RouteMaster" text="Route Master"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="manufacturer" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                  

                          


<c:if test = "${routeMasterList != null}" >
    <table class="table table-info mb30 table-hover" id="table" style="width:100%" >
        <thead>
            <tr height="40">
                <th>S No</th>
                <th>State Name</th>
                <th>WareHouse Name</th>
                <th>Route Code</th>
                <th>Route Name</th>   
            </tr>
        </thead>
        <tbody>
            <% int index = 0, sno = 1;%>
            <c:forEach items="${routeMasterList}" var="route">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr height="30">
                    <td align="left" ><%=sno%></td>
                    <td align="left" ><c:out value="${route.stateName}"/> </td>
                     <td align="left" ><c:out value="${route.whName}"/> </td>
                      <td align="left" ><c:out value="${route.routeCode}"/> </td>
                       <td align="left" ><c:out value="${route.routeName}"/> </td>
                    
                <%
                            index++;
                            sno++;
                %>
            </c:forEach>

        </tbody>
    </table>
</c:if>


   
    

</form>
</body>
</div>
</div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
<!--<td align="left" >
                        <c:if test="${(customer.secondaryBillingTypeId=='1')}" >
                            Fixed KM Based
                        </c:if>

                        <c:if test="${(customer.secondaryBillingTypeId=='2')}" >
                            Actual KM Based
                        </c:if>
                        <c:if test="${(customer.secondaryBillingTypeId=='0')}" >
                            NA
                        </c:if>

                    </td>-->
