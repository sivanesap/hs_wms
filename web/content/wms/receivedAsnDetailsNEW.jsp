<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript">
    function submitPage(value) {

        if (value == 'Update') {
            document.repair.action = '/throttle/customerhaltingapproval.do';
            document.repair.submit();
        }
    }

</script>       

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Receive GRN"  text="Receive GRN"/></a></li>
            <li class="active">Receive GRN</li>
        </ol>
    </div>
</div>

<script>

    function setValue() {

        var selectedTrip = document.getElementsByName('selectedIndex');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedTrip.length; i++) {
            if (selectedTrip[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }
    }

    function searchPage(val) {
        var selectedLhc = document.getElementsByName('selectedIndex');
        var lhcId = document.getElementsByName('lhcId');
        var approvStatus = document.getElementsByName('approvStatus');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedLhc.length; i++) {
//            alert("i="+i);
            if (selectedLhc[i].checked == true) {
                document.getElementById('selectedIndexs' + i).value = 1;
//                alert("selected :"+selectedTrip[i].value);
//                alert("trip expense id :"+tripExpenseId[i].value);
//                alert("approvStatus"+approvStatus[i].value);
                if (cntr == 0) {
                    temp = lhcId[i].value;
                } else {
                    temp = temp + "," + lhcId[i].value;
                }
                cntr++;
            } else {
                document.getElementById('selectedIndexs' + i).value = 0;
            }
        }

        if (temp != "") {
//            alert("Hai:"+temp); 
            document.repair.action = "/throttle/lhcApproval.do?param=" + val + "&lhcIds=" + temp;
            document.repair.submit();

        } else {
            alert("Please select any one and proceed....");
        }

    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <%@ include file="/content/common/message.jsp" %>
            <body onload=" onloadAddrow();">
                <form name="repair" method="post">
                    <table class="table table-info mb30 table-bordered" >
                        <thead >
                            <tr >
                                <th>S.No</th>
                                <th>Customer</th>
                                <th>Item Name </th>
                                <th>SKU Code</th>
                                <th>HSN Code</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        <c:if test="${asnDetails != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${asnDetails}" var="asn">
                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td>
                                            <%=sno%>
                                        </td>
                                        <td ><c:out value="${asn.custName}"/></td>
                                        <td><c:out value="${asn.itemName}"/></td>
                                        <td><c:out value="${asn.skuCode}"/></td>
                                        <td><c:out value="${asn.hsnCode}"/></td>
                                        <td><c:out value="${asn.actualQuantity}"/>
                                            <input type="hidden" name="selectedIndexs" id="selectedIndexs<%=sno - 1%>" value="0"/>
                                            <input type="hidden" name="asnDetId" id="asnDetId<%=sno - 1%>" value="<c:out value="${asn.asnDetId}"/>"/>
                                            <input type="hidden" name="custName" id="custName<%=sno - 1%>" value="<c:out value="${asn.custName}"/>"/>
                                            <input type="hidden" name="itemName" id="itemName<%=sno - 1%>" value="<c:out value="${asn.itemName}"/>"/>
                                            <input type="hidden" name="skuCode" id="skuCode<%=sno - 1%>" value="<c:out value="${asn.skuCode}"/>"/>
                                            <input type="hidden" name="hsnCode" id="hsnCode<%=sno - 1%>" value="<c:out value="${asn.hsnCode}"/>"/>
                                            <input type="hidden" name="actualQuantity" id="actualQuantity<%=sno - 1%>" value="<c:out value="${asn.actualQuantity}"/>"/>
                                        </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>
                        </c:if>
                    </table>  
                    <br>
                    <table class="table table-info mb30 table-hover" id="items">
                        <thead>
                            <tr>
                                <th colspan="6"><spring:message code="stores.label.GRN Details"  text="GRN Details"/></th>
                            </tr>
                            <tr>
                                <th>SNo</th>                                
                                <th width="20%">SKU Code</th>
                                <th>Act. Qty</th>
                                <th>SerialNo</th>
                                <th>Rec. Qty</th>
                                <th>Damaged Qty</th>
                            </tr>
                        </thead>
                        <script>

                            function onloadAddrow() {
                                var asnDetId = document.getElementsByName("asnDetId");                                
                                var itemName = document.getElementsByName("itemName");
                                var skuCode = document.getElementsByName("skuCode");
                                var hsnCode = document.getElementsByName("hsnCode");
                                var actualQuantity = document.getElementsByName("actualQuantity");

                                for (var j = 0; j < skuCode.length; j++) {

                                    for (var i = 0; i < parseFloat(actualQuantity[j].value); i++) {                                    
                                    addRow(asnDetId[j].value, skuCode[j].value, actualQuantity[j].value);
                                    }
                                }
                            }

                            var httpRequest;
                            var httpReq;
                            var rowCount = 2;
                            var sno = 0;
                            var rowIndex = 0;
                            function addRow(asnDetId, skuCode, actualQuantity)
                            {
                                sno++;
                                var tab = document.getElementById("items");
                                var newrow = tab.insertRow(rowCount);

                                var cell = newrow.insertCell(0);
                                var cell0 = "<td class='text1' height='25' ><input type='hidden' name='asnDetId' id='asnDetId" + rowIndex + "' value='" + asnDetId + "'> " + sno + "</td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell0;

                                cell = newrow.insertCell(1);
                                var cell2 = "<td class='text1' height='25'><input name='skuCode' id='skuCode" + rowIndex + "'  value='" + skuCode + "' readonly class='form-control'  type='hidden'>" + skuCode + "</td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell2;

                                cell = newrow.insertCell(2);
                                var cell4 = " <td class='text1' height='25'><input name='actualQuantitys' id='actualQuantitys" + rowIndex + "'  class='form-control'  type='hidden' value='1'>1</td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell4;
                                
                                var cell = newrow.insertCell(3);
                                var cell0 = "<td class='text1' height='25' ><input name='serailNO' id='serailNO" + rowIndex + "' class='form-control' value='' type='text'></td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell0;

                                cell = newrow.insertCell(4);
                                var cell2 = "<td class='text1' height='25'><input type='text' name='receivedQty' id='receivedQty" + rowIndex + "' class='form-control' value='0'/></td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell2;

                                cell = newrow.insertCell(5);
                                var cell4 = " <td class='text1' height='25'><input type='text' name='damagedQty' id='damagedQty" + rowIndex + "' class='form-control' value='0'/></td>";
                                cell.setAttribute("className", "text2");
                                cell.innerHTML = cell4;

                                rowIndex++;
                                rowCount++;

                            }
                        </script>
                    </table>

                    <div>
                        <center>
                            <input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                        </center>
                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

