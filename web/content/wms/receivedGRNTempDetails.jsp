<%-- 
    Document   : receivedGRNTempDetails
    Created on : 12 Dec, 2020, 3:00:21 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<html>
    <body>
            <%@ include file="/content/common/message.jsp" %>
        <form id="receivedGRNTempDetails" action="post">
            <c:if test="${serialTempDetails !=null}">
                <div align="center" style="height:20px;" id="StatusMsgs">&nbsp;&nbsp;<c:out value="${serialNoAlreadyExists}"/></div>
            <table class="table table-info mb30 table-bordered" id="table">
                   <tr height="40" Style="background-color: #5BC0DE;" style="width:80%">
                       <th><font color="white">Sno</font></th>
                       <th><font color="white">Serial No</font></th>
                       <th><font color="white">Product Condition</th>
                       <th><font color="white">Qty</font></th>
                       
                   </tr>

            <% int index = 0,sno = 1; %>
            <c:forEach items="${serialTempDetails}" var="grn">
            <tr>
            <% 
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        
               <tr height="30" border="0">
                   <td align="left"><%=sno%></td>
                   <td align="left"><input type="hidden" id="serialNos<%=index%>" name="serialNos" value='<c:out value="${grn.serialNo}"/>'/><c:out value="${grn.serialNo}"/></td>
                   <td  align="left" type="hidden" id="proCons<%=index%>" name="proCons" >
                       <c:if test="${grn.proCon =='0'}">
                       Not Mentioned
                       </c:if>
                       <c:if test="${grn.proCon =='1'}">
                       Good
                       </c:if>
                       <c:if test="${grn.proCon =='2'}">
                       Damaged
                       </c:if>
                       
                   </td>
                   <td  align="left"><c:out value="${grn.qty}"/></td>
               <%
                          index++;
                          sno++;
               %>
                   </tr>
            </c:forEach>
               </table>
            </c:if>

        </form>
    </body>
</html>
