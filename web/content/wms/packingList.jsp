<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
   
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Packing List </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
                <li class="">Dispatch Packing List</li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="">
                    <form name="pack" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr >
                                    <th >S.No</th>
                                    <th  >PackingNo</th>
                                    <th >Origin </th>
                                    <th >Destination </th>
                                    <th >PickOrder </th>
                                    <th >UOM</th>
                                    <th >VAS</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr height="30">                    
                                    <td> 1 </td>                                         
                                    <td> Pick/2021/001 </td>                                    
                                    <td> Chennai</td>                                           
                                    <td> Coimbatore</td>                                           
                                    <td> 10.0</td>                                           
                                    <td> Nos</td>             
                                    <td> <input type="button" value="VAS" onclick="vasPage();" class="btn btn-success"></td>             
                                    <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                </tr>

                                <tr height="30">                    
                                    <td> 2 </td>                                         
                                    <td> Pick/2021/002 </td> 
                                    <td> Chennai</td>                                           
                                    <td> Coimbatore</td>
                                    <td> 10.0</td>                                           
                                    <td> Nos</td>                                           
                                    <td> <input type="button" value="VAS" onclick="vasPage();" class="btn btn-success"></td>
                                    <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                </tr>
                            </tbody>



                        </table>
                        <br>
                        <center>
                            <input type="button" value="Generate" class="btn btn-success" onClick="generatePickList()">                
                        </center>
                        <script>
                            function generatePickList() {
                                alert("Delivery Note Generated ...!");
                            }
                            function vasPage(){
                                   window.open('/throttle/packingVas.do', 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                            }
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>



