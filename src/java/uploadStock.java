
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
----------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver        Date                          Author                     Change
 * ---------------------------------------------------------------------------
 * 1.0     Jul 28, 2008                     SriniR			           Created
 *
 **************************************************************************/
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance data.
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class uploadStock {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String args[]) {
//
//        String attDetails[] = new String[200];
//        File fl = null;
//        FileInputStream fis = null;
//        BufferedInputStream bis = null;
//        DataInputStream dis = null;
//        String record = null;
//        String fileName = "";
//        ResultSet rs = null;
//        ResultSet rsItemId = null;
//        ResultSet rsPriceId = null;
//
//
//        PreparedStatement pstmt = null;
//        PreparedStatement pselect = null;
//        PreparedStatement newStock = null;
//        PreparedStatement newStock1 = null;
//        PreparedStatement selectItemId = null;
//        PreparedStatement selectPriceId = null;
//
//
//        float tax = 12.5f;
//        float price = 0.0f;
//        float priceWithTax = 0.0f;
//        int insertStatus = 0;
//        int priceId = 0;
//        int itemId = 0;
//        String paplCode = null;
//
//
//        String priceType = "INVOICE";
//        String insertPrice = "insert into  papl_item_price_master(item_id,tax_percentage,price,price_type,price_with_tax,active_ind,created_on) values(?,?,?,?,?,?,current_timestamp())";
//        String selectLastPriceId = "select price_id from papl_item_price_master where  item_id=? and created_on>=now()";
//        String insertStockNew = "insert into papl_stock_balance_new values(?,?,?,?)";
//        String getItemId = "select item_id,papl_code from papl_item_master where papl_code=?";
//        String getPriceId = "select price_id from papl_item_price_master where item_id=? and price=? ";
//        Connection conn = null;
//
//        try {
//            fileName = "/root/Desktop/NokiaMissing.csv";
//
//            System.out.println("fileName:" + fileName);
//
//            System.out.println("MAX MEMORY = " + Runtime.getRuntime().maxMemory() / 1048576 + "MB");
//            System.out.println("TOTAL MEMORY = " + Runtime.getRuntime().totalMemory() / 1048576 + "MB");
//            System.out.println("FREE MEMORY = " + Runtime.getRuntime().freeMemory() / 1048576 + "MB");
//
//
//            try {
//                fl = new File(fileName);
//                fis = new FileInputStream(fl);
//                bis = new BufferedInputStream(fis);
//                dis = new DataInputStream(bis);
//            } catch (FileNotFoundException fnfexcp) {
//                fnfexcp.printStackTrace();
//            }
//
////get connection
//            try {
//// This is used to access the Database using thin client.
//                Class.forName("com.mysql.jdbc.Driver");
//                conn = DriverManager.getConnection("jdbc:mysql://192.168.0.6/parveen", "parveen", "admin");
//                conn.setAutoCommit(false);
//            } catch (Exception excp) {
//                excp.printStackTrace();
//            }
//
//
//            while ((record = dis.readLine()) != null) {
//
//                if (record.trim().length() > 0) {
//                    attDetails = record.split(",");
//
//                    System.out.println("attDetails len=" + attDetails.length);
//
//                    try {
//
//                        pstmt = conn.prepareStatement(insertPrice);
//                        pselect = conn.prepareStatement(selectLastPriceId);
//                        newStock = conn.prepareStatement(insertStockNew);
//                        newStock1 = conn.prepareStatement(insertStockNew);
//
//
//
//
//
//
//
//
//
//
//
//                        System.out.println("Integer.parseInt(attDetails[9].trim())"+Float.parseFloat(attDetails[9].trim()));
//                        System.out.println("Integer.parseInt(attDetails[9].trim())"+(Float.parseFloat(attDetails[9].trim())+1));
//
//
//                        if (Float.parseFloat(attDetails[9].trim()) > 0) {
//                            System.out.println("New Actual Price > 0");
//                            try{
//
//                            pstmt.setInt(1, Integer.parseInt(attDetails[0].trim()));
//                            pstmt.setFloat(2, tax);
//                            pstmt.setFloat(3, Float.parseFloat(attDetails[9].trim()));
//                            pstmt.setString(4, priceType);
//                            price = Float.parseFloat(attDetails[9].trim());
//                            priceWithTax = (float) ((12.5/100)+ price);
//                            pstmt.setFloat(5, priceWithTax);
//                            pstmt.setString(6, "Y");
//
//
//                                System.out.println("Every thing assigned Success..May Execution is prob");
//                        insertStatus = pstmt.executeUpdate();
//                        System.out.println("Price Master Status is-->" + insertStatus);
//
//                            }
//                            catch(Exception e)
//                            {
//                                e.printStackTrace();
//                                System.out.println("Price with tax error...");
//                            }
//
//                        insertStatus = 0;
//                            System.out.println("Before Last Insert");
//
//                            pselect.setInt(1, Integer.parseInt(attDetails[0].trim()));
//                        rsPriceId = pselect.executeQuery();
//                            System.out.println("After Last Insert");
//                        while(rsPriceId.next()){
//                        priceId = rsPriceId.getInt(1);
//                        System.out.println("Price _id-->"+priceId);
//                        }
//
//
//
//                        newStock1.setInt(1,1015);
//                        newStock1.setInt(2,Integer.parseInt(attDetails[0].trim()));
//                        newStock1.setInt(3,priceId);
//                        newStock1.setFloat(4,Float.parseFloat(attDetails[7].trim()));
//
//
//                        insertStatus = newStock1.executeUpdate();
//                        System.out.println("Stock Master Status price > 0 is-->" + insertStatus);
//
//
//
//                        }else if(!(Float.parseFloat(attDetails[9].trim()) > 0) && Float.parseFloat(attDetails[7].trim()) > 0){
//
//                            newStock.setInt(1,1015);
//                        newStock.setInt(2,Integer.parseInt(attDetails[0].trim()));
//                        newStock.setInt(3,Integer.parseInt(attDetails[3].trim()));
//                        newStock.setFloat(4,Float.parseFloat(attDetails[7].trim()));
//                        System.out.println("Float.parseFloat(attDetails[7].trim())-->"+Float.parseFloat(attDetails[7].trim()));
//
//                        insertStatus = newStock.executeUpdate();
//                        System.out.println("Stock Master Insert price < 0 Status is-->" + insertStatus);
//                        }
//                        else
//                        {
//
//                            System.out.println("Both Price && Quantity are Zero..");
//                            System.out.println("Float.parseFloat(attDetails[7].trim())-->"+Float.parseFloat(attDetails[7].trim()));
//                            System.out.println("Float.parseFloat(attDetails[9].trim())-->"+Float.parseFloat(attDetails[9].trim()));
//                        }
//
//
//                    } catch (Exception sqlException) {
//
//                        sqlException.printStackTrace();
//                        System.out.println("record=" + record);
//                    }
//
//                }
//                conn.commit();
//
//            }
//            conn.close();
//
//
//        } catch (Exception excp) {
//            excp.printStackTrace();
//            try {
//                conn.rollback();
//                conn.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }


        String string = "Rajesh kumar";
        System.out.println("String : " + string);
        String substring = string.substring(3);
        System.out.println("String after 3rd index:" + substring);
        substring = string.substring(0, 6);
        System.out.println("substring"+substring);
   
    }
}

