
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
----------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver        Date                          Author                     Change
 * ---------------------------------------------------------------------------
 * 1.0     Jul 28, 2008                     SriniR			           Created
 *
 **************************************************************************/
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance data.
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class uploadStock1 {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String args[]) {

        String attDetails[] = new String[200];
        File fl = null;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        String record = null;
        String fileName = "";
        ResultSet rs = null;
        ResultSet rsItemId = null;
        ResultSet rsPriceId = null;
        ResultSet rsTax = null;


        PreparedStatement pstmt = null;
        PreparedStatement pselect = null;
        PreparedStatement newStock = null;
        PreparedStatement newStock1 = null;
        PreparedStatement selectItemId = null;
        PreparedStatement selectPriceId = null;
        PreparedStatement selectTax = null;


        float tax = 12.5f;
        float price = 0.0f;
        float priceWithTax = 0.0f;
        int insertStatus = 0;
        int priceId = 0;
        int itemId = 0;
        String paplCode = null;


        String priceType = "INVOICE";
        String insertPrice = "insert into  papl_item_price_master(item_id,tax_percentage,price,price_type,price_with_tax,active_ind,created_on) values(?,?,?,?,?,?,current_timestamp())";
        String selectLastPriceId = "select price_id from papl_item_price_master where  item_id=? and created_on>=now()";
        String insertStockNew = "insert into papl_stock_balance_new values(?,?,?,?)";
        String getItemId = "select ifnull(item_id,count(*)) as item_id,ifnull(papl_code,count(*)) as papl_code from papl_item_master where papl_code=?";
        String getPriceId = "SELECT  ifnull(price_id,count(*)) as price_id FROM papl_item_price_Master where item_id=? and price=?  and price_type='invoice'";
        String getTax = "select ifnull(tax_percentage,0)as tax from papl_item_price_master where item_id=?  and price_type='invoice' order by price_id desc limit 1";
        Connection conn = null;

        try {
            fileName = "/root/Desktop/RedhillsMissing.csv";

            System.out.println("fileName:" + fileName);

            System.out.println("MAX MEMORY = " + Runtime.getRuntime().maxMemory() / 1048576 + "MB");
            System.out.println("TOTAL MEMORY = " + Runtime.getRuntime().totalMemory() / 1048576 + "MB");
            System.out.println("FREE MEMORY = " + Runtime.getRuntime().freeMemory() / 1048576 + "MB");


            try {
                fl = new File(fileName);
                fis = new FileInputStream(fl);
                bis = new BufferedInputStream(fis);
                dis = new DataInputStream(bis);
            } catch (FileNotFoundException fnfexcp) {
                fnfexcp.printStackTrace();
            }

//get connection
            try {
// This is used to access the Database using thin client.
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://192.168.0.6/parveen", "parveen", "admin");
                conn.setAutoCommit(false);
            } catch (Exception excp) {
                excp.printStackTrace();
            }


            while ((record = dis.readLine()) != null) {

                if (record.trim().length() > 0) {
                    attDetails = record.split(",");

                    System.out.println("attDetails len=" + attDetails.length);

                    try {

                        pstmt = conn.prepareStatement(insertPrice);
                        pselect = conn.prepareStatement(selectLastPriceId);
                        newStock = conn.prepareStatement(insertStockNew);
                        newStock1 = conn.prepareStatement(insertStockNew);

                        selectItemId = conn.prepareStatement(getItemId);
                        selectPriceId = conn.prepareStatement(getPriceId);
                        selectTax = conn.prepareStatement(getTax);


                        System.out.println("attDetails[0].trim()-->" + attDetails[0].trim());
                        System.out.println("Float.parseFloat(attDetails[2].trim())-->" + Float.parseFloat(attDetails[2].trim()));
                        System.out.println("Float.parseFloat(attDetails[4].trim())-->" + Float.parseFloat(attDetails[4].trim()));
                        if (Float.parseFloat(attDetails[2].trim()) > 0 && Float.parseFloat(attDetails[4].trim()) > 0) {
                            selectItemId.setString(1, attDetails[0].trim());
                            rsItemId = selectItemId.executeQuery();

                            while (rsItemId.next()) {
                                paplCode = rsItemId.getString(2);
                                itemId = rsItemId.getInt(1);
                                System.out.println("Papl_code-->" + paplCode + "Item_id-->" + itemId);

                            }
                            price = Float.parseFloat(attDetails[4].trim());

                            if (itemId != 0) {
                                selectPriceId.setInt(1, itemId);
                                selectPriceId.setFloat(2, price);
                                rsPriceId = selectPriceId.executeQuery();
                                while (rsPriceId.next()) {
                                    priceId = rsPriceId.getInt(1);
                                    System.out.println("Old PriceId-->" + priceId);
                                }
                            } else {
                                System.out.println("No item Id for Papl_code-->" + attDetails[0].trim());
                            }


                            if (itemId != 0 && priceId == 0) {
                                System.out.println("Price Id is 0 Here-->" + priceId);
                                try {
                                    try {
                                        selectTax.setInt(1, itemId);
                                        System.out.println("B4 Execution" + itemId);
                                        rsTax = selectTax.executeQuery();
                                        System.out.println("Safe Execution");
                                        while (rsTax.next()) {
                                            tax = rsTax.getFloat(1);
                                            System.out.println("Tax value from previous price_id" + tax);
                                        }

                                        pstmt.setInt(1, itemId);
                                        pstmt.setFloat(3, price);
                                        pstmt.setString(4, priceType);
                                        pstmt.setString(6, "Y");
                                        if (tax == 1.0) {
                                            System.out.println("Constant Tax 12.5");
                                            pstmt.setFloat(2, 12.5f);
                                            priceWithTax = (float) ((12.5f / 100) + price);
                                            pstmt.setFloat(5, priceWithTax);
                                        } else {
                                            System.out.println("Dynamic Tax" + tax);
                                            pstmt.setFloat(2, tax);
                                            priceWithTax = (float) ((tax / 100) + price);
                                            pstmt.setFloat(5, priceWithTax);
                                        }
                                    } catch (Exception e) {
                                        System.out.println("Tax Error da...");
                                    }

                                    System.out.println("Every thing assigned Success..May Execution is prob");
                                    insertStatus = pstmt.executeUpdate();
                                    System.out.println("Price Master Status is-->" + insertStatus);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println("Price with tax error...");
                                }

                                insertStatus = 0;
                                System.out.println("Before Last Insert");

                                pselect.setInt(1, itemId);
                                rsPriceId = pselect.executeQuery();

                                System.out.println("After Last Insert");
                                while (rsPriceId.next()) {
                                    priceId = rsPriceId.getInt(1);
                                    System.out.println("Price _id-->" + priceId + "ItemId-->" + itemId);
                                }



                                newStock1.setInt(1, 1022);
                                newStock1.setInt(2, itemId);
                                newStock1.setInt(3, priceId);
                                newStock1.setFloat(4, Float.parseFloat(attDetails[2].trim()));


                                insertStatus = newStock1.executeUpdate();
                                System.out.println("Stock Master Status price > 0 is-->" + insertStatus);

                            } else if (itemId != 0 && priceId != 0) {
                                System.out.println("Have Both Item Id and Price Id");
                                newStock1.setInt(1, 1022);
                                newStock1.setInt(2, itemId);
                                newStock1.setInt(3, priceId);
                                newStock1.setFloat(4, Float.parseFloat(attDetails[2].trim()));
                                insertStatus = newStock1.executeUpdate();
                                System.out.println("Stock Master Status price < 0 is-->" + insertStatus);

                            } else {
                                System.out.println("Have no item Id for-->" + attDetails[0].trim());
                            }
                        } else {
                            System.out.println("Both Quantity && Price are Zero Here");
                            System.out.println("Float.parseFloat(attDetails[2].trim()" + Float.parseFloat(attDetails[2].trim()));
                            System.out.println("Float.parseFloat(attDetails[4].trim()" + Float.parseFloat(attDetails[4].trim()));
                        }

                    } catch (Exception sqlException) {

                        sqlException.printStackTrace();
                        System.out.println("record=" + record);
                    }

                }
                conn.commit();

            }
            conn.close();


        } catch (Exception excp) {
            excp.printStackTrace();
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
