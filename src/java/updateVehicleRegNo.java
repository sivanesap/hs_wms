
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateVehicleRegNo {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://203.124.105.244:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement vehicleRegNo = null;
                statement = con.createStatement();

// updating records
                String sql = "UPDATE papl_jobcard_master SET reg_no=? WHERE job_card_Id=?";
                System.out.println("Updated successfully");
                vehicleRegNo = con.prepareStatement(sql);
                String sql1 = "select reg_no,job_card_Id from papl_direct_jobcard a, papl_vehicle_reg_no b  where a.vehicle_Id = b.vehicle_Id and b.Active_Ind = 'Y'";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                while (rs.next()) {
                    String regNo = rs.getString(1);
                    int jobCardId = rs.getInt(2);
                    System.out.println("JobCradId = " + regNo);
                    vehicleRegNo.setString(1,regNo);
                    vehicleRegNo.setInt(2,jobCardId);
                    System.out.println("RegNo = " + regNo+"---jobCradId---"+jobCardId);
                    updateStatus = vehicleRegNo.executeUpdate();
                    i++;
                }
                System.out.println("totalRecordsUpdate = " + i);

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
