package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;


public class TimeSeriesBean implements DatasetProducer, CategoryToolTipGenerator, CategoryItemLinkGenerator,Serializable  {

	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(TimeSeriesBean.class);
	    String startDate;
	    String endDate;
	    String strIOU;
	    String region;
	   String[] seriesNames = new String[10];
	    String[][] linkNames = new String[10][10];
	    String[][] links = new String[10][10];
	    int counter = 0;
	    int count = 0;
	    /**
		 *  Produces some random data.
		 */

	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	TimeSeries s1 = new TimeSeries("L&G European Index Trust", Month.class);
	        s1.add(new Month(2, 2001), 181.8);
	        s1.add(new Month(3, 2001), 167.3);
	        s1.add(new Month(4, 2001), 153.8);
	        s1.add(new Month(5, 2001), 167.6);
	        s1.add(new Month(6, 2001), 158.8);
	        s1.add(new Month(7, 2001), 148.3);
	        s1.add(new Month(8, 2001), 153.9);
	        s1.add(new Month(9, 2001), 142.7);
	        s1.add(new Month(10, 2001), 123.2);
	        s1.add(new Month(11, 2001), 131.8);
	        s1.add(new Month(12, 2001), 139.6);
	        s1.add(new Month(1, 2002), 142.9);
	        s1.add(new Month(2, 2002), 138.7);
	        s1.add(new Month(3, 2002), 137.3);
	        s1.add(new Month(4, 2002), 143.9);
	        s1.add(new Month(5, 2002), 139.8);
	        s1.add(new Month(6, 2002), 137.0);
	        s1.add(new Month(7, 2002), 132.8);

	        TimeSeries s2 = new TimeSeries("L&G UK Index Trust", Month.class);
	        s2.add(new Month(2, 2001), 129.6);
	        s2.add(new Month(3, 2001), 123.2);
	        s2.add(new Month(4, 2001), 117.2);
	        s2.add(new Month(5, 2001), 124.1);
	        s2.add(new Month(6, 2001), 122.6);
	        s2.add(new Month(7, 2001), 119.2);
	        s2.add(new Month(8, 2001), 116.5);
	        s2.add(new Month(9, 2001), 112.7);
	        s2.add(new Month(10, 2001), 101.5);
	        s2.add(new Month(11, 2001), 106.1);
	        s2.add(new Month(12, 2001), 110.3);
	        s2.add(new Month(1, 2002), 111.7);
	        s2.add(new Month(2, 2002), 111.0);
	        s2.add(new Month(3, 2002), 109.6);
	        s2.add(new Month(4, 2002), 113.2);
	        s2.add(new Month(5, 2002), 111.6);
	        s2.add(new Month(6, 2002), 108.8);
	        s2.add(new Month(7, 2002), 101.6);

	        TimeSeriesCollection dataset = new TimeSeriesCollection();
	        dataset.addSeries(s1);
	        dataset.addSeries(s2);
	        return dataset;
	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}

		/**
		 * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset, int, int)
		 */
		public String generateToolTip(CategoryDataset arg0, int series, int arg2) {

			return linkNames[arg2][series];
		}

		/**
	     * Returns a link target for a special data item.
	     */
	    public String generateLink(Object data, int series, Object category) {

	    	if(series == 0)
	    	{
		    	count = counter++;
	    		return links[count][series];
	    	}
	    	else {
	    		return links[count][series];
	    	}
	    }

	}


