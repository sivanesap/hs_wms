package ets.cewolf.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.time.SimpleTimePeriod;
import de.laures.cewolf.DatasetProduceException;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import de.laures.cewolf.DatasetProducer;



public class GanttBean implements DatasetProducer, Serializable {


	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(GanttBean.class);

		/**
		 *  Produces some random data.
		 */
	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");
	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	 String ds1 = "January 1, 2000";
	    	 String ds2 = "February 1, 2000";
	    	 String ds3 = "March 1, 2000";
	    	 String ds4 = "April 1, 2000";
	    	 String ds5 = "May 1, 2000";
	    	 String ds6 = "June 1, 2000";
	    	 String ds7 = "July 1, 2000";
	    	 String ds9 = "September 1, 2000";
	    	 String ds10 = "October 1, 2000";
	    	 String ds11 = "November 1, 2000";
	    	 String ds12 = "December 1, 2000";
	    	 String ds13 = "January 1, 2001";

	    	 String ds1_1 = "January 30, 2000";
	    	 String ds2_2 = "February 27, 2000";
	    	 String ds3_3 = "March 30, 2000";
	    	 String ds4_4 = "April 30, 2000";
	    	 String ds5_5 = "May 30, 2000";
	    	 String ds6_6 = "June 30, 2000";
	    	 String ds8_8 = "August 30, 2000";
	    	 String ds9_9 = "September 30, 2000";
	    	 String ds10_10 = "October 30, 2000";
	    	 String ds11_11 = "November 30, 2000";
	    	 String ds12_12 = "December 30, 2000";
	    	 String ds13_13 = "January 30, 2001";

	    	 Date d1= new Date();
	    	 Date d2= new Date();
	    	 Date d3= new Date();
	    	 Date d4= new Date();
	    	 Date d5= new Date();
	    	 Date d6= new Date();
	    	 Date d7= new Date();
	    	 Date d9= new Date();
	    	 Date d10= new Date();
	    	 Date d11= new Date();
	    	 Date d12= new Date();
	    	 Date d13= new Date();
	    	 Date d1_1= new Date();
	    	 Date d2_2= new Date();
	    	 Date d3_3= new Date();
	    	 Date d4_4= new Date();
	    	 Date d5_5= new Date();
	    	 Date d6_6= new Date();
	    	 Date d8_8= new Date();
	    	 Date d9_9= new Date();
	    	 Date d10_10= new Date();
	    	 Date d11_11= new Date();
	    	 Date d12_12= new Date();
	    	 Date d13_13= new Date();

	    	 DateFormat df = DateFormat.getDateInstance();
	         try {
				 d1 = df.parse(ds1);
				d2 = df.parse(ds2);
				d3 = df.parse(ds3);
				d4 = df.parse(ds4);
				d5 = df.parse(ds5);
				d6 = df.parse(ds6);
				d7 = df.parse(ds7);
				d9 = df.parse(ds9);
				d10 = df.parse(ds10);
				d11 = df.parse(ds11);
				d12 = df.parse(ds12);
				d13 = df.parse(ds13);

				d1_1 = df.parse(ds1_1);
				d2_2 = df.parse(ds2_2);
				d3_3 = df.parse(ds3_3);
				d4_4 = df.parse(ds4_4);
				d5_5 = df.parse(ds5_5);
				d6_6 = df.parse(ds6_6);
				d8_8 = df.parse(ds8_8);
				d9_9 = df.parse(ds9_9);
				d10_10 = df.parse(ds10_10);
				d11_11 = df.parse(ds11_11);
				d12_12 = df.parse(ds12_12);
				d13_13 = df.parse(ds13_13);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    	final TaskSeries s1 = new TaskSeries("Scheduled");
	        s1.add(new Task("Write Proposal",
	        		new SimpleTimePeriod(d1,
	        				d1_1)));
	        s1.add(new Task("Obtain Approval",
	        		new SimpleTimePeriod(d2,
	        				d2_2)));
	        s1.add(new Task("Requirements Analysis",
	        		new SimpleTimePeriod(d3,
	        				d3_3)));
	        s1.add(new Task("Design Phase",
	        		new SimpleTimePeriod(d4,
	        				d4_4)));
	        s1.add(new Task("Design Signoff",
	        		new SimpleTimePeriod(d5,
	        				d5_5)));
	        s1.add(new Task("Alpha Implementation",
	        		new SimpleTimePeriod(d6,
	        				d6_6)));
	        s1.add(new Task("Design Review",
	        		new SimpleTimePeriod(d7,
	        				d8_8)));
	        s1.add(new Task("Revised Design Signoff",
	        		new SimpleTimePeriod(d9,
	        				d9_9)));
	        s1.add(new Task("Beta Implementation",
	        		new SimpleTimePeriod(d10,
	        				d10_10)));
	        s1.add(new Task("Testing",
	        		new SimpleTimePeriod(d11,
	        				d11_11)));
	        s1.add(new Task("Final Implementation",
	        		new SimpleTimePeriod(d12,
	        				d12_12)));
	        s1.add(new Task("Signoff",
	        		new SimpleTimePeriod(d13,
	        				d13_13)));
	        final TaskSeries s2 = new TaskSeries("Actual");
	        s2.add(new Task("Write Proposal",
	        		new SimpleTimePeriod(d1,
	        				d1_1)));
	        s2.add(new Task("Obtain Approval",
	        		new SimpleTimePeriod(d2,
	        				d2_2)));
	        s2.add(new Task("Requirements Analysis",
	        		new SimpleTimePeriod(d3,
	        				d3_3)));
	        s2.add(new Task("Design Phase",
	        		new SimpleTimePeriod(d4,
	        				d4_4)));
	        s2.add(new Task("Design Signoff",
	        		new SimpleTimePeriod(d5,
	        				d5_5)));
	        s2.add(new Task("Alpha Implementation",
	        		new SimpleTimePeriod(d6,
	        				d6_6)));
	        s2.add(new Task("Design Review",
	        		new SimpleTimePeriod(d7,
	        				d8_8)));
	        s2.add(new Task("Revised Design Signoff",
	        		new SimpleTimePeriod(d9,
	        				d9_9)));
	        s2.add(new Task("Beta Implementation",
	        		new SimpleTimePeriod(d10,
	        				d10_10)));
	        s2.add(new Task("Testing",
	        		new SimpleTimePeriod(d11,
	        				d11_11)));
	        s2.add(new Task("Final Implementation",
	        		new SimpleTimePeriod(d12,
	        				d12_12)));
	        s2.add(new Task("Signoff",
	        		new SimpleTimePeriod(d13,
	        				d13_13)));
	        final TaskSeriesCollection collection = new TaskSeriesCollection();
	        collection.add(s1);
	        collection.add(s2);

	        return collection;
	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}


}





