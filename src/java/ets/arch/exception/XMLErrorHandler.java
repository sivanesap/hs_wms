/*----------------------------------------------------------------------------
 * XMLErrorHandler.java
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietory information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 ---------------------------------------------------------------------------*/
package ets.arch.exception;
/**
 * Modification Log
 * ---------------------------------------------------------------------
 * Ver   Date              Modified By               Description
 * ---------------------------------------------------------------------
 * 1.0   26 Jan 2008       Srinivasan.R             Initial Version
 *
 ***********************************************************************/
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * XMLErrorHandler:All runtime exceptions of FP will throw an object of this type.
 * <br>This Class gets the details of the error thrown from
 *  xml holding those values.
 * <br>An error is logged in the Log file with the error details.
 * @author Srinivasan.R
 * @version 1.0 26 Jan 2008
 * @since   Event Portal Iteration 0
 */
public class XMLErrorHandler extends DefaultHandler {

/**
 * validationError - validationError
 */
boolean validationError = false;
 /**
  * saxParseException -Instance of SAXParseException
  */
  SAXParseException saxParseException = null;
  /**
   * @param exception - SAXParseException
   * @throws SAXException - SAXException
   */
  public void error(SAXParseException exception)
      throws SAXException {
   validationError = true;
   saxParseException = exception;
  }
  /**
   * @param exception - SAXParseException
   * @throws SAXException - SAXException
   */
  public void fatalError(SAXParseException exception)
      throws SAXException {
   validationError = true;
   saxParseException=exception;
  }
  /**
   * @param exception - SAXParseException
   * @throws SAXException - SAXException
   */
  public void warning(SAXParseException exception)
      throws SAXException { }
/**
 * @return validationError
 * @author Arivu_Selvaraj
 */
public boolean getValidationError() {
	// TODO Auto-generated method stub
	return this.validationError;
}
/**
 * @return saxParseException
 * @author Arivu_Selvaraj
 */
public SAXParseException getSaxParseException() {
	// TODO Auto-generated method stub
	return this.saxParseException;
}
}
