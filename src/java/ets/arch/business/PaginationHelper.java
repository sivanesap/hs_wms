/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.arch.business;

import ets.arch.util.FPUtil;

/**
 *
 * @author vijay
 */
public class PaginationHelper {
    
    private int noOfRecordsPerPage = 0;
    private int pageNoToBeDisplayed = 0;
    private int startIndex = 0;
    private int endIndex = 0;
    private int totalRecords = 0;
    private int totalNoOfPages = 0;
    static FPUtil fpUtil = FPUtil.getInstance();
    
    public PaginationHelper() {
        noOfRecordsPerPage = Integer.parseInt(fpUtil.getInstance().getProperty("NO_OF_RECORDS_PER_PAGE"));
        //////System.out.println("noOfRecordsPerPage"+noOfRecordsPerPage);
    }

    
    public int getEndIndex() {
        return endIndex;
    }



    public int getNoOfRecordsPerPage() {
        return noOfRecordsPerPage;
    }



    public int getPageNoToBeDisplayed(int pageNo, String buttonClicked) {
        
        if (buttonClicked.equalsIgnoreCase("Prev")){
            pageNoToBeDisplayed = pageNo - 1;
            startIndex = (pageNoToBeDisplayed-1)* noOfRecordsPerPage;
        }else if (buttonClicked.equalsIgnoreCase("Next")){
            pageNoToBeDisplayed = pageNo + 1;
            startIndex = (pageNoToBeDisplayed-1)* noOfRecordsPerPage;
        }else if (buttonClicked.equalsIgnoreCase("GoTo")){
            pageNoToBeDisplayed = pageNo;
            startIndex = (pageNoToBeDisplayed-1)* noOfRecordsPerPage;
        }else {
            pageNoToBeDisplayed = 1;
            startIndex = 0;
        }
        endIndex = noOfRecordsPerPage;
        //////System.out.println("startIndex"+startIndex);
        //////System.out.println("endIndex"+endIndex);
        return pageNoToBeDisplayed;
    }



    public int getStartIndex() {
        return startIndex;
    }



    public int getTotalNoOfPages() {
        return totalNoOfPages;
    }



    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
        //compute total no of pages.
        totalNoOfPages = totalRecords / noOfRecordsPerPage;
        if (totalRecords > (noOfRecordsPerPage*totalNoOfPages)) {
            totalNoOfPages++;
        }
        //////System.out.println("totalRecords"+totalRecords);
    }
    
    

}
