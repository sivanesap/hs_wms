package ets.domain.goods.web;

/**
 *
 * @author vidya
 */
public class GoodsCommand {

    public GoodsCommand() {
    }
    
    
    
    
    
  String gdId="";
  String[] gdIds=null;
  String approvedId="";
  String remark="";
  String inTime="";
  String outTime="";
  String[] remarks=null;
  String refNo="";
  String description="";
  // variables for search option
  String gdDate="";
  String gdStatus="";
  String goodsToDate="";
  String goodsFromDate="";
  String goodType="";
  String gdNo="";
  String goodsStatus="";
String createdOn="";

    public String getGoodType() {
        return goodType;
    }

    public void setGoodType(String goodType) {
        this.goodType = goodType;
    }

   



    public String getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(String goodsStatus) {
        this.goodsStatus = goodsStatus;
    }


    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getGdNo() {
        return gdNo;
    }

    public void setGdNo(String gdNo) {
        this.gdNo = gdNo;
    }
  

  
  
    public String getGdStatus() {
        return gdStatus;
    }

    public void setGdStatus(String gdStatus) {
        this.gdStatus = gdStatus;
    }

    public String getGoodsFromDate() {
        return goodsFromDate;
    }

    public void setGoodsFromDate(String goodsFromDate) {
        this.goodsFromDate = goodsFromDate;
    }

    public String getGoodsToDate() {
        return goodsToDate;
    }

    public void setGoodsToDate(String goodsToDate) {
        this.goodsToDate = goodsToDate;
    }
  
  

    public String getGdDate() {
        return gdDate;
    }

    public void setGdDate(String gdDate) {
        this.gdDate = gdDate;
    }
  
  

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }
  
  
   
    String[] in=null;
             String[] out=null;

    public String[] getIn() {
        return in;
    }

    public void setIn(String[] in) {
        this.in = in;
    }

    public String[] getOut() {
        return out;
    }

    public void setOut(String[] out) {
        this.out = out;
    }
             
             

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String[] getRemarks() {
        return remarks;
    }

    public void setRemarks(String[] remarks) {
        this.remarks = remarks;
    }
   
   
  
    public String getApprovedId() {
        return approvedId;
    }

    public void setApprovedId(String approvedId) {
        this.approvedId = approvedId;
    }

    public String getGdId() {
        return gdId;
    }

    public void setGdId(String gdId) {
        this.gdId = gdId;
    }

    public String[] getGdIds() {
        return gdIds;
    }

    public void setGdIds(String[] gdIds) {
        this.gdIds = gdIds;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    
        
  
          
}
