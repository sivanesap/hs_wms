package ets.domain.goods.business;

/**
 *
 * @author vidya
 */
public class GoodsTO {
     public GoodsTO() {
    }
          
  String gdId="";
  String[] gdIds=null;
  String approvedId="";
  String remark="";
  String inTime="";
  String outTime="";
  String[] remarks=null;
  String gdType="";
  String outtime="";
  String intime="";
  String refNo="";
  String description="";
  
   // variables for search option
  String gdDate="";
  String gdStatus="";
  String goodsToDate="";
  String goodsFromDate="";
  String gdNo="";
  String createdOn="";
  String status="";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
  

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
  
  

    public String getGdDate() {
        return gdDate;
    }

    public void setGdDate(String gdDate) {
        this.gdDate = gdDate;
    }

    public String getGdNo() {
        return gdNo;
    }

    public void setGdNo(String gdNo) {
        this.gdNo = gdNo;
    }

    public String getGdStatus() {
        return gdStatus;
    }

    public void setGdStatus(String gdStatus) {
        this.gdStatus = gdStatus;
    }

    public String getGoodsFromDate() {
        return goodsFromDate;
    }

    public void setGoodsFromDate(String goodsFromDate) {
        this.goodsFromDate = goodsFromDate;
    }

    public String getGoodsToDate() {
        return goodsToDate;
    }

    public void setGoodsToDate(String goodsToDate) {
        this.goodsToDate = goodsToDate;
    }
  
  

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }
  
  

    public String getIntime() {
        return intime;
    }

    public void setIntime(String intime) {
        this.intime = intime;
    }
  
  

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }
  
  

    public String getGdType() {
        return gdType;
    }

    public void setGdType(String gdType) {
        this.gdType = gdType;
    }
  
  
   

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String[] getRemarks() {
        return remarks;
    }

    public void setRemarks(String[] remarks) {
        this.remarks = remarks;
    }
  
  
    public String[] getGdIds() {
        return gdIds;
    }

    public void setGdIds(String[] gdIds) {
        this.gdIds = gdIds;
    }

  
    public String getApprovedId() {
        return approvedId;
    }

    public void setApprovedId(String approvedId) {
        this.approvedId = approvedId;
    }

    public String getGdId() {
        return gdId;
    }

    public void setGdId(String gdId) {
        this.gdId = gdId;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    
  
  
  
  
  
}
