/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.billing.business;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 *
 * @author Nivan
 */
public class BillingTO {
    
private SqlMapClient session = null;
private String gstCode = "";
private String gstName = "";
private String gstPercentage = "";
private String totalTax = "";
private String activeInd = "";
private String tripSheetIds = "";
private String weightmentExpenseType = "";
private String weightmentCharge = "";
private String otherExpense = "";
private String shippingBillNo = null;
private String remarks = null;
private String greenTax = null;
private String containerNo = null;
private String tripContainerId = null;
private String consignmentConatinerId = null;
private String vehicleId = null;
private String empId = null;
private String grNo = null;
private String detaintionCharge = null;
private String tollCharge = null;
private String movementType = null;
private String timeElapsedValue = null;
private String shipingBillNo = null;
private String billOfEntryNo = null;
private String containerTypeName = null;
private String billingParty = null;
private String billingPartyId = null;
private String freightAmount = null;
private String podCount = null;
private String totalAmount = null;
private String otherExpenseAmount = null;
private String userId = null;
private String fromDate = null;
private String toDate = null;
private String customerName = null;
private String customerId = null;
private String tripId = null;
private String totalKmRun = null;
private String totalWeightage = null;
private String consignmentNoteNo = null;
private String billingTypeId = null;
private String consigmentDestination = null;
private String freightCharges = null;
private String consignmentOrderId = null;
private String tripCode = null;
private String status = null;
private String tempReeferRequired = null;
private String routeInfo = null;
private String tripStartTime = null;
private String totalDays = null;
private String tripTransitHours = null;
private String billingType = null;
private String customerType = null;
private String vehicleType = null;
private String startDate = null;
private String startTime = null;
private String endDate = null;
private String endTime = null;
private String estimatedRevenue = null;
private String totalKM = null;
private String totalHrs = null;
private String tripTransitDays = null;
private String driverName = null;
private String totalExpenses = null;
private String expenseToBeBilledToCustomer = null;
private String expenseName = null;
private String expenseRemarks = null;
private String passThroughStatus = null;
private String marginValue = null;
private String taxPercentage = null;
private String taxPercentageD = null;
private String expenseValue = null;
private String expenseId = null;
private String noOfTrips = null;
private String totalRevenue = null;
private String totalExpToBeBilled = null;
private String grandTotal = null;
private String invoiceCode = null;
private String invoiceNo = null;
private String invoiceId = null;
private String invoiceDetailId = null;
private String statusId = null;
private String tripSheetId = null;
private String orderCount = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTotalKmRun() {
        return totalKmRun;
    }

    public void setTotalKmRun(String totalKmRun) {
        this.totalKmRun = totalKmRun;
    }

    public String getTotalWeightage() {
        return totalWeightage;
    }

    public void setTotalWeightage(String totalWeightage) {
        this.totalWeightage = totalWeightage;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getConsigmentDestination() {
        return consigmentDestination;
    }

    public void setConsigmentDestination(String consigmentDestination) {
        this.consigmentDestination = consigmentDestination;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTempReeferRequired() {
        return tempReeferRequired;
    }

    public void setTempReeferRequired(String tempReeferRequired) {
        this.tempReeferRequired = tempReeferRequired;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTripTransitDays() {
        return tripTransitDays;
    }

    public void setTripTransitDays(String tripTransitDays) {
        this.tripTransitDays = tripTransitDays;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getExpenseToBeBilledToCustomer() {
        return expenseToBeBilledToCustomer;
    }

    public void setExpenseToBeBilledToCustomer(String expenseToBeBilledToCustomer) {
        this.expenseToBeBilledToCustomer = expenseToBeBilledToCustomer;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getPassThroughStatus() {
        return passThroughStatus;
    }

    public void setPassThroughStatus(String passThroughStatus) {
        this.passThroughStatus = passThroughStatus;
    }

    public String getMarginValue() {
        return marginValue;
    }

    public void setMarginValue(String marginValue) {
        this.marginValue = marginValue;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTaxPercentageD() {
        return taxPercentageD;
    }

    public void setTaxPercentageD(String taxPercentageD) {
        this.taxPercentageD = taxPercentageD;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTotalExpToBeBilled() {
        return totalExpToBeBilled;
    }

    public void setTotalExpToBeBilled(String totalExpToBeBilled) {
        this.totalExpToBeBilled = totalExpToBeBilled;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getPodCount() {
        return podCount;
    }

    public void setPodCount(String podCount) {
        this.podCount = podCount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOtherExpenseAmount() {
        return otherExpenseAmount;
    }

    public void setOtherExpenseAmount(String otherExpenseAmount) {
        this.otherExpenseAmount = otherExpenseAmount;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getBillingPartyId() {
        return billingPartyId;
    }

    public void setBillingPartyId(String billingPartyId) {
        this.billingPartyId = billingPartyId;
    }

    public String getBillOfEntryNo() {
        return billOfEntryNo;
    }

    public void setBillOfEntryNo(String billOfEntryNo) {
        this.billOfEntryNo = billOfEntryNo;
    }

    public String getShipingBillNo() {
        return shipingBillNo;
    }

    public void setShipingBillNo(String shipingBillNo) {
        this.shipingBillNo = shipingBillNo;
    }

    public String getTimeElapsedValue() {
        return timeElapsedValue;
    }

    public void setTimeElapsedValue(String timeElapsedValue) {
        this.timeElapsedValue = timeElapsedValue;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getDetaintionCharge() {
        return detaintionCharge;
    }

    public void setDetaintionCharge(String detaintionCharge) {
        this.detaintionCharge = detaintionCharge;
    }

    public String getTollCharge() {
        return tollCharge;
    }

    public void setTollCharge(String tollCharge) {
        this.tollCharge = tollCharge;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getConsignmentConatinerId() {
        return consignmentConatinerId;
    }

    public void setConsignmentConatinerId(String consignmentConatinerId) {
        this.consignmentConatinerId = consignmentConatinerId;
    }

    public String getTripContainerId() {
        return tripContainerId;
    }

    public void setTripContainerId(String tripContainerId) {
        this.tripContainerId = tripContainerId;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getGreenTax() {
        return greenTax;
    }

    public void setGreenTax(String greenTax) {
        this.greenTax = greenTax;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getShippingBillNo() {
        return shippingBillNo;
    }

    public void setShippingBillNo(String shippingBillNo) {
        this.shippingBillNo = shippingBillNo;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getWeightmentCharge() {
        return weightmentCharge;
    }

    public void setWeightmentCharge(String weightmentCharge) {
        this.weightmentCharge = weightmentCharge;
    }

    public String getWeightmentExpenseType() {
        return weightmentExpenseType;
    }

    public void setWeightmentExpenseType(String weightmentExpenseType) {
        this.weightmentExpenseType = weightmentExpenseType;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getTripSheetIds() {
        return tripSheetIds;
    }

    public void setTripSheetIds(String tripSheetIds) {
        this.tripSheetIds = tripSheetIds;
    }

    public SqlMapClient getSession() {
        return session;
    }

    public void setSession(SqlMapClient session) {
        this.session = session;
    }

    public String getGstCode() {
        return gstCode;
    }

    public void setGstCode(String gstCode) {
        this.gstCode = gstCode;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

   
    
}
