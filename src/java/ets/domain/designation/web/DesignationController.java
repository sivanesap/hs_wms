/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
package ets.domain.designation.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.designation.business.DesignationBP;
import ets.domain.designation.business.DesignationTO;

import ets.domain.users.business.LoginBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.util.FPLogUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
public class DesignationController extends BaseController {

    /** Creates a new instance of __NAME__ */
    public DesignationController() {
    }
    DesignationCommand designationCommand;
    DesignationBP designationBP;
    //   LeaveBP leaveBP;
    LoginBP loginBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

//    public LeaveBP getLeaveBP() {
    //       return leaveBP;
    //   }

    //   public void setLeaveBP(LeaveBP leaveBP) {
//        this.leaveBP = leaveBP;
    //   }
    public DesignationBP getDesignationBP() {
        return designationBP;
    }

    public void setDesignationBP(DesignationBP designationBP) {
        this.designationBP = designationBP;
    }

    /**
     * This method used to bind the request values to the command object.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
           initialize(request);

    }

    /**
     * This method used to View Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleDesignManage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "HRMS  >>  Manage Designation ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
                String pageTitle = "Manage Designation";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList DesignaList = new ArrayList();
                DesignaList = designationBP.processGetDesignaList();
                request.setAttribute("DesignaList", DesignaList);
                path = "content/designation/manageDesignation.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Add Information of Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddDesignationPage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        designationCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "HRMS  >>  Manage Designation  >> Add ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try{
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        if (!loginBP.checkAuthorisation(userFunctions, "Designation-Add")) {
            path = "content/common/NotAuthorized.jsp";
        } else {
            String pageTitle = "Add Designation";
            request.setAttribute("pageTitle", pageTitle);
            path = "content/designation/addDesignation.jsp";
        }
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view handleAddDesignationPage --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Insert Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddDesig(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        designationCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {

            String menuPath = "HRMS  >>  Manage Designation  >> Add ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            DesignationTO designationTO = new DesignationTO();
            if (designationCommand.getDesigName() != null && designationCommand.getDesigName() != "") {
                designationTO.setDesigName(designationCommand.getDesigName());
            }

            if (designationCommand.getDescription() != null && designationCommand.getDescription() != "") {
                designationTO.setDescription(designationCommand.getDescription());
            }

            String pageTitle = "Manage Designation";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/designation/manageDesignation.jsp";

            insertStatus = designationBP.processInsertDesigDetails(designationTO, userId);
            request.removeAttribute("DesignaList");
            ArrayList DesignaList = new ArrayList();
            DesignaList = designationBP.processGetDesignaList();
            request.setAttribute("DesignaList", DesignaList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Designation Added Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Get Grade According To Designations
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public void handleCheckDesignationName(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) throws IOException {
        try {

            designationCommand = command;

            String designationName = designationCommand.getDesigName();

            String checkStatus = "";
            checkStatus = designationBP.processCheckDesigName(designationName);
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            writer.print(checkStatus);
            writer.close();
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to retrieve pagination data --> " + exception);
            exception.printStackTrace();
        }


    }

    /**
     * This method used to View Alter Desgination Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterDesigPage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        path = "content/designation/alterDesig.jsp";
        try {
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        if (!loginBP.checkAuthorisation(userFunctions, "Designation-Alter")) {
            path = "content/common/NotAuthorized.jsp";
        } else {
            designationCommand = command;
            String menuPath = "HRMS  >>  Manage Designation  >> Add ";
            ArrayList DesignaList = new ArrayList();
            DesignaList = designationBP.processGetDesignaList();

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Add Designation";
            request.setAttribute("DesignaList", DesignaList);
            request.setAttribute("pageTitle", pageTitle);
        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleModifyDesignation(HttpServletRequest request, HttpServletResponse reponse, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        designationCommand = command;
        String path = "";

        try {
            String menuPath = "HRMS  >>  Manage Designation  >> Alter ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            int index = 0;
            int modify = 0;
            String[] desigids = designationCommand.getDesigIds();
            String[] designames = designationCommand.getDesigNames();
            String[] activeStatus = designationCommand.getActiveInds();
            String[] selectedIndex = designationCommand.getSelectedIndex();
            String[] description = designationCommand.getDescriptions();
            int UserId = (Integer) session.getAttribute("userId");


            ArrayList List = new ArrayList();
            DesignationTO designationTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                designationTO = new DesignationTO();
                index = Integer.parseInt(selectedIndex[i]);

                designationTO.setDesigId(Integer.parseInt(desigids[index]));
                designationTO.setDesigName(designames[index]);
                designationTO.setActiveInd(activeStatus[index]);
                designationTO.setDescription(description[index]);
                List.add(designationTO);
            }
            String pageTitle = "Manage Designation";
            path = "content/designation/manageDesignation.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = designationBP.processModifyDesignaDetails(List, UserId);
            request.removeAttribute("DesignaList");
            ArrayList DesignaList = new ArrayList();
            DesignaList = designationBP.processGetDesignaList();
            request.setAttribute("DesignaList", DesignaList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Designation Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleGradeManage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        designationCommand = command;
        String path = "";
        try {
            HttpSession session = request.getSession();
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Grade-List")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
            String menuPath = "HRMS  >>  Manage Grade ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            path = "content/designation/manageGrade.jsp";
            ArrayList GradeList = new ArrayList();
            String pageTitle = "Manage Grade";
            request.setAttribute("pageTitle", pageTitle);
            int desigId = Integer.parseInt(designationCommand.getDesignationId());
            request.setAttribute("DesigId", designationCommand.getDesignationId());
            request.setAttribute("desigName", designationCommand.getDesigName());

            GradeList = designationBP.processGetGradeList(desigId);
            request.setAttribute("GradeList", GradeList);
            }
        //          }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Add Information of Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddGradePage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        try {
            HttpSession session = request.getSession();
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Grade-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
            String menuPath = "HRMS  >>  Manage Grade  >>  Add";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Add Grade";
            request.setAttribute("pageTitle", pageTitle);
            int desigId = Integer.parseInt(designationCommand.getDesignationId());
            request.setAttribute("DesigId", designationCommand.getDesignationId());
            request.setAttribute("desigName", designationBP.processGetDesigName(desigId));

            path = "content/designation/addGrade.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }


        return new ModelAndView(path);
    }

    /**
     * This method used to Add Grade Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddGrade(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        designationCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        try {
            String menuPath = "HRMS  >>  Manage Grade  >>  Add";
            String desigName = "";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            // if (!loginBP.checkAuthorisation(userFunctions, "Grade-Add")) {
            //         path = "content/common/NotAuthorized.jsp";
            //    } else {
            String pageTitle = "Manage Designation";
            request.setAttribute("pageTitle", pageTitle);
            DesignationTO designationTO = new DesignationTO();

            int desigId = 0;
            if (designationCommand.getDesignationId() != null && designationCommand.getDesignationId() != "") {
                desigId = Integer.parseInt(designationCommand.getDesignationId());
                designationTO.setDesigId(Integer.parseInt(designationCommand.getDesignationId()));
            }

            if (designationCommand.getGradeName() != null && designationCommand.getGradeName() != "") {
                designationTO.setGradeName(designationCommand.getGradeName());
            }

            if (designationCommand.getDescription() != null && designationCommand.getDescription() != "") {
                designationTO.setDescription(designationCommand.getDescription());
            }

            if (designationCommand.getDesigName() != null && designationCommand.getDesigName() != "") {
                desigName = designationCommand.getDesigName();
                request.setAttribute("desigName", desigName);
            }

            int UserId = (Integer) session.getAttribute("userId");
            int insertStatus = 0;

            path = "content/designation/addGrade.jsp";

            insertStatus = designationBP.processInsertgradeDetails(designationTO, UserId);

            int addLeaveConfig = 0;

            path = "content/designation/manageGrade.jsp";
//                addLeaveConfig = leaveBP.insertLeaveDetails(insertStatus, desigId, UserId);

//            ArrayList GradeList = new ArrayList();
//            GradeList = designationBP.getGradeList();
//            request.setAttribute("GradeList", GradeList);

            ArrayList GradeList = new ArrayList();
            GradeList = designationBP.processGetGradeList(desigId);
            request.setAttribute("GradeList", GradeList);

            request.setAttribute("DesigId", desigId);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grade Details Added Successfully");
        //       }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View Alter Information of Grade Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterGradePage(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        designationCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        try {
            String menuPath = "HRMS  >>  Manage Grade  >>  Alter";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
             if (!loginBP.checkAuthorisation(userFunctions, "Grade-Alter")) {
                   path = "content/common/NotAuthorized.jsp";
             } else {
            String pageTitle = "Alter Grade";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList GradeList = new ArrayList();
            int desigId = Integer.parseInt(designationCommand.getDesignationId());
            request.setAttribute("DesigId", designationCommand.getDesignationId());
            request.setAttribute("DesigName", designationBP.processGetDesigName(desigId));
            GradeList = designationBP.processGetGradeList(desigId);
            request.setAttribute("GradeList", GradeList);
            path = "content/designation/alterGrade.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify Grade Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleModifyGrade(HttpServletRequest request, HttpServletResponse reponse, DesignationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        designationCommand = command;
        String path = "";
        try {
            String menuPath = "HRMS  >>  Manage Grade  >>  Alter";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //       if (!loginBP.checkAuthorisation(userFunctions, "Grade-Modify")) {
            //          path = "content/common/NotAuthorized.jsp";
            //       } else {

            int index = 0;
            int modify = 0;
            String desigName = designationCommand.getDesigName();
            String[] ids = designationCommand.getGradeIds();
            String[] names = designationCommand.getGradeNames();
            String[] descs = designationCommand.getDescriptions();
            String[] activeStatus = designationCommand.getActiveInds();
            String[] selectedIndex = designationCommand.getSelectedIndex();
            int UserId = (Integer) session.getAttribute("userId");


            ArrayList List = new ArrayList();
            DesignationTO designationTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                designationTO = new DesignationTO();
                index = Integer.parseInt(selectedIndex[i]);

                designationTO.setGradeId(Integer.parseInt(ids[index]));
                designationTO.setGradeName(names[index]);
                designationTO.setDescription(descs[index]);
                designationTO.setActiveInd(activeStatus[index]);
                List.add(designationTO);
            }
            path = "content/designation/manageGrade.jsp";
            modify = designationBP.processModifyGradeDetails(List, UserId);
            String pageTitle = "Manage Grade";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList GradeList = new ArrayList();
            int desigId = Integer.parseInt(designationCommand.getDesignationId());
            GradeList = designationBP.processGetGradeList(desigId);
            request.setAttribute("GradeList", GradeList);
            request.setAttribute("desigName", desigName);
            request.setAttribute("DesigId", desigId);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Grade Details Modified Successfully");
        //      }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to grade data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void handleGetGradeForDesig(HttpServletRequest request, HttpServletResponse response, DesignationCommand command) throws IOException {
        PrintWriter pw = response.getWriter();
        DesignationTO designationTO = new DesignationTO();
        ArrayList gradeList = new ArrayList();
        try {

            designationCommand = command;
            if (designationCommand.getDesigId() != null && !"".equals(designationCommand.getDesigId())) {
                designationTO.setDesigId(Integer.parseInt(designationCommand.getDesigId()));
            }
            int desigId = Integer.parseInt(designationCommand.getDesigId());

            String returnValue = "0-Select- ";
            String gradeDetails = "";
            gradeList = designationBP.getActiveGradeList(desigId);

            Iterator itr = gradeList.iterator();
            while (itr.hasNext()) {
                designationTO = (DesignationTO) itr.next();
                returnValue = returnValue + "," + designationTO.getGradeName();
            }
            pw.print(returnValue);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to retrieve pagination data --> " + exception);
            exception.printStackTrace();
        }
    }
}
