/*---------------------------------------------------------------------------
 * ServiceCommand.java
 * Mar 5, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/
package ets.domain.renderservice.web;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class RenderServiceCommand {

    private String oldTyerNo = "";
    private String remarks = "";
    private String newTyerNo = "";
    private String odometerReading = "";
    private String changeDate = "";
    private String newTyerype = "";
    private String tyerCompanyName = "";
    private String labourRemarks = "";
    private String invoiceNo = "";
    private String invoiceRemarks = "";
    private String invoiceAmount = "";
    private String invoiceDate = "";
    private String totalCost = "";
    private String totalLabour = "";
    private String totalAmount = "";
    private String vatAmount = "";
    private String vatRemarks = "";
    private String serviceTaxAmount = "";
    private String serviceTaxRemarks = "";
    private String billSubGroupId = "";
    private String billSubGroupName = "";
    private String billSubGroupDesc = "";
    private String billGroupId = "";
    private String billGroupName = "";
    private String activeInd = "";
    private String jobCardId = "";
    private String[] activityId = null;
    private String[] activityAmount = null;
    private String[] billNo = null;
    private String[] itemId = null;
    private String[] quantity = null;
    private String[] qty = null;
    private String[] price = null;
    private String[] tax = null;
    private String[] lineItemAmount = null;
    private String[] problemId = null;
    private String[] selectedIndex = null;
    private String[] causeId = null;
    private String approve = "";
    private String sparesPercent = "";
    private String labourPercent = "";
    private String bodyRepairTotal = "";
    private String spares = "";
    private String labour = "";
    private String total = "";
    private String discount = "";
    private String nett = "";
    private String billNumber = "";
    private String inVoiceType = "";
    private String inVoiceNo = "";
    //Hari
    private String date = null;
//    bala
    private String laborhike = "";

    public String getLaborhike() {
        return laborhike;
    }

    public void setLaborhike(String laborhike) {
        this.laborhike = laborhike;
    }
//    bala ends

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String[] getActivityId() {
        return activityId;
    }

    public void setActivityId(String[] activityId) {
        this.activityId = activityId;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String[] getProblemId() {
        return problemId;
    }

    public void setProblemId(String[] problemId) {
        this.problemId = problemId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getCauseId() {
        return causeId;
    }

    public void setCauseId(String[] causeId) {
        this.causeId = causeId;
    }

    public String[] getActivityAmount() {
        return activityAmount;
    }

    public void setActivityAmount(String[] activityAmount) {
        this.activityAmount = activityAmount;
    }

    public String[] getBillNo() {
        return billNo;
    }

    public void setBillNo(String[] billNo) {
        this.billNo = billNo;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String[] getItemId() {
        return itemId;
    }

    public void setItemId(String[] itemId) {
        this.itemId = itemId;
    }

    public String getLabour() {
        return labour;
    }

    public void setLabour(String labour) {
        this.labour = labour;
    }

    public String[] getLineItemAmount() {
        return lineItemAmount;
    }

    public void setLineItemAmount(String[] lineItemAmount) {
        this.lineItemAmount = lineItemAmount;
    }

    public String getNett() {
        return nett;
    }

    public void setNett(String nett) {
        this.nett = nett;
    }

    public String[] getQuantity() {
        return quantity;
    }

    public void setQuantity(String[] quantity) {
        this.quantity = quantity;
    }

    public String getSpares() {
        return spares;
    }

    public void setSpares(String spares) {
        this.spares = spares;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String[] getPrice() {
        return price;
    }

    public void setPrice(String[] price) {
        this.price = price;
    }

    public String getBodyRepairTotal() {
        return bodyRepairTotal;
    }

    public void setBodyRepairTotal(String bodyRepairTotal) {
        this.bodyRepairTotal = bodyRepairTotal;
    }

    public String getLabourPercent() {
        return labourPercent;
    }

    public void setLabourPercent(String labourPercent) {
        this.labourPercent = labourPercent;
    }

    public String getSparesPercent() {
        return sparesPercent;
    }

    public void setSparesPercent(String sparesPercent) {
        this.sparesPercent = sparesPercent;
    }

    public String[] getTax() {
        return tax;
    }

    public void setTax(String[] tax) {
        this.tax = tax;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getInVoiceType() {
        return inVoiceType;
    }

    public void setInVoiceType(String inVoiceType) {
        this.inVoiceType = inVoiceType;
    }

    public String getInVoiceNo() {
        return inVoiceNo;
    }

    public void setInVoiceNo(String inVoiceNo) {
        this.inVoiceNo = inVoiceNo;
    }

    public String[] getQty() {
        return qty;
    }

    public void setQty(String[] qty) {
        this.qty = qty;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getBillGroupId() {
        return billGroupId;
    }

    public void setBillGroupId(String billGroupId) {
        this.billGroupId = billGroupId;
    }

    public String getBillGroupName() {
        return billGroupName;
    }

    public void setBillGroupName(String billGroupName) {
        this.billGroupName = billGroupName;
    }

    public String getBillSubGroupDesc() {
        return billSubGroupDesc;
    }

    public void setBillSubGroupDesc(String billSubGroupDesc) {
        this.billSubGroupDesc = billSubGroupDesc;
    }

    public String getBillSubGroupId() {
        return billSubGroupId;
    }

    public void setBillSubGroupId(String billSubGroupId) {
        this.billSubGroupId = billSubGroupId;
    }

    public String getBillSubGroupName() {
        return billSubGroupName;
    }

    public void setBillSubGroupName(String billSubGroupName) {
        this.billSubGroupName = billSubGroupName;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceRemarks() {
        return invoiceRemarks;
    }

    public void setInvoiceRemarks(String invoiceRemarks) {
        this.invoiceRemarks = invoiceRemarks;
    }

    public String getServiceTaxAmount() {
        return serviceTaxAmount;
    }

    public void setServiceTaxAmount(String serviceTaxAmount) {
        this.serviceTaxAmount = serviceTaxAmount;
    }

    public String getServiceTaxRemarks() {
        return serviceTaxRemarks;
    }

    public void setServiceTaxRemarks(String serviceTaxRemarks) {
        this.serviceTaxRemarks = serviceTaxRemarks;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalLabour() {
        return totalLabour;
    }

    public void setTotalLabour(String totalLabour) {
        this.totalLabour = totalLabour;
    }

    public String getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(String vatAmount) {
        this.vatAmount = vatAmount;
    }

    public String getVatRemarks() {
        return vatRemarks;
    }

    public void setVatRemarks(String vatRemarks) {
        this.vatRemarks = vatRemarks;
    }

    public String getLabourRemarks() {
        return labourRemarks;
    }

    public void setLabourRemarks(String labourRemarks) {
        this.labourRemarks = labourRemarks;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getNewTyerNo() {
        return newTyerNo;
    }

    public void setNewTyerNo(String newTyerNo) {
        this.newTyerNo = newTyerNo;
    }

    public String getNewTyerype() {
        return newTyerype;
    }

    public void setNewTyerype(String newTyerype) {
        this.newTyerype = newTyerype;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getOldTyerNo() {
        return oldTyerNo;
    }

    public void setOldTyerNo(String oldTyerNo) {
        this.oldTyerNo = oldTyerNo;
    }

    public String getTyerCompanyName() {
        return tyerCompanyName;
    }

    public void setTyerCompanyName(String tyerCompanyName) {
        this.tyerCompanyName = tyerCompanyName;
    }


}
