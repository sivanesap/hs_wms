/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scrap.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.scrap.business.ScrapTO;
import ets.domain.util.FPLogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class ScrapDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "ScrapDAO";

    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList doDisposeInList() {
        Map map = new HashMap();

        ScrapTO scrapTO = new ScrapTO();
        ArrayList disposeInList = null;
        try {
            disposeInList = (ArrayList) getSqlMapClientTemplate().queryForList("scrap.DisposeInList", map);
            System.out.println("scrapList size in dao " + disposeInList.size());
            System.out.println("servicePoint" + scrapTO.getCompName());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return disposeInList;
    }
    //doDisposeAllList   
    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public int doDisposeAllList(int kgRate, int totalAmountAll, ArrayList kgAmountUnique, ArrayList noAmountUnique, int userId, int companyId, ScrapTO scrapTO) {
        int status = 0;
        int lastId = 0;
        int status1 = 0;
        int status2 = 0;
        Map map = new HashMap();

        map.put("kgTotalWeight", scrapTO.getKgTotalWg());
        map.put("kgRate", kgRate);
        map.put("toMakeKgTotal", scrapTO.getKgTotalAmount());

        map.put("totalAmountAll", totalAmountAll);
        map.put("userId", userId);
        map.put("companyId", companyId);
        System.out.println("kgTotalWeight" + scrapTO.getKgTotalWg());
        System.out.println("kgRate" + kgRate);
        System.out.println("toMakeKgTotal" + scrapTO.getKgTotalAmount());
        System.out.println("totalAmountAll" + totalAmountAll);

        try {

            String kgAmount = "";
            lastId = (Integer) getSqlMapClientTemplate().insert("scrap.saveDisposeMaster", map);            
            System.out.println("lastId" + lastId);
            map.put("disposeId", lastId);
            //no
            Iterator itr = noAmountUnique.iterator();
            while (itr.hasNext()) {
                scrapTO = new ScrapTO();
                scrapTO = (ScrapTO) itr.next();
                System.out.println("scrapIds="+scrapTO.getScrapId());
                System.out.println("unitPrice="+scrapTO.getUnitPrice());
                System.out.println("amount="+scrapTO.getNoAmountUnique());
                map.put("unitPrice", scrapTO.getUnitPrice());
                map.put("scrapId", scrapTO.getScrapId());
                map.put("amount", scrapTO.getNoAmountUnique());
                status1 = (Integer) getSqlMapClientTemplate().update("scrap.saveDisposeNo", map);
            }
            //kg
            Iterator itr1 = kgAmountUnique.iterator();
            while (itr1.hasNext()) {
                scrapTO = new ScrapTO();
                scrapTO = (ScrapTO) itr1.next();
                System.out.println("scrapIds Kg="+scrapTO.getScrapId());
                System.out.println("kgRate="+kgRate );
                System.out.println("amount Kg="+scrapTO.getKgAmountUnique());                                
                map.put("kgRate", kgRate);
                map.put("scrapIdkg", scrapTO.getScrapId());
                map.put("amountkg", scrapTO.getKgAmountUnique());
                status2 = (Integer) getSqlMapClientTemplate().update("scrap.saveDisposeKg", map);
            }


        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return (status1+status2) ;
    }

    //doSectionWiseList
    /**
     * This  method used to Alter Parts List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception Model
     */
    public ArrayList doSectionWiseList(ScrapTO scrapTO) {
        int status = 0;
        int lastId = 0;
        int status1 = 0;
        int status2 = 0;
        Map map = new HashMap();

        map.put("sectionId", scrapTO.getSectionId());
        map.put("uomId", scrapTO.getUomId());
        ArrayList sectionWiseList = new ArrayList();


        try {
            sectionWiseList = (ArrayList) getSqlMapClientTemplate().queryForList("scrap.SectionWiseList", map);



        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return sectionWiseList;
    }
    
    
    
    public ArrayList getScrapApprovalItems(ScrapTO scrapTO) {
        Map map = new HashMap();
        map.put("fromDate", scrapTO.getFromDate());
        map.put("toDate", scrapTO.getToDate());
        map.put("paplCode", scrapTO.getPaplCode());
        map.put("itemName", scrapTO.getItemName());
        
        ArrayList scapList = new ArrayList();

        try {
            scapList = (ArrayList) getSqlMapClientTemplate().queryForList("scrap.searchScrapApprovalItems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return scapList;
    }
    
    public int doScrapItemsQty(ScrapTO scrapTO , int userId) {
        Map map = new HashMap();
        map.put("qty", scrapTO.getQuandity());
        map.put("approveId", scrapTO.getApproveId());        
        map.put("priceId", scrapTO.getPriceId());        
        map.put("approver", scrapTO.getApprovedBy());
        map.put("status", scrapTO.getStatus());
        map.put("itemId", scrapTO.getItemId());
        map.put("userId", userId);
        
        int status = 0;

        try {
            status = getSqlMapClientTemplate().update("scrap.approvedScrapQty", map);
            status = getSqlMapClientTemplate().update("scrap.scrapItem", map);
            if(scrapTO.getStatus().equalsIgnoreCase("APPROVE")){
                map.put("qty", -Float.parseFloat(scrapTO.getQuandity()));
                status = getSqlMapClientTemplate().update("scrap.updateStock", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return status;
    }
    
    public int doRejectScrapReq(ScrapTO scrapTO , int userId) {
        Map map = new HashMap();
//        map.put("qty", scrapTO.getQuandity());
        map.put("approveId", scrapTO.getApproveId());        
        map.put("status", "REJECT");        
//        map.put("priceId", scrapTO.getPriceId());        
//        map.put("approver", scrapTO.getApprovedBy());
//        map.put("status", scrapTO.getStatus());
        
        int status = 0;

        try {
            status = getSqlMapClientTemplate().update("scrap.rejectScrapReq", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return status;
    }
    
    public int doScrapReturnStock(ScrapTO scrapTO , int userId) {
        Map map = new HashMap();
        
        map.put("approveId", scrapTO.getApproveId());        
        map.put("approver", scrapTO.getApprovedBy());
        map.put("userId", userId);
        map.put("priceId", scrapTO.getPriceId());       
        map.put("status", "RETURN");    
        map.put("itemId", scrapTO.getItemId());
        map.put("qty", Float.parseFloat(scrapTO.getQuandity()));
        
        int status = 0;

        try {
            status = getSqlMapClientTemplate().update("scrap.returnScrapQty", map);
            status = getSqlMapClientTemplate().update("scrap.updateScrapMaster", map);
            status = getSqlMapClientTemplate().update("scrap.rejectScrapReq", map);
            status = getSqlMapClientTemplate().update("scrap.updateStock", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "GetPartsDetail", sqlException);
        }
        return status;
    }
}
