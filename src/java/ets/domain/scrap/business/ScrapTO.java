/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scrap.business;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class ScrapTO {

    String[] compIdsForKg = null;
    String[] compNamesForKg = null;
    String[] itemIdsForKg = null;
    String[] itemNamesForKg = null;
    String[] paplCodesForKg = null;
    String[] quanditysForKg = null;
    String[] uomIdsForKg = null;
    String[] uomNamesForKg = null;
    String[] compIdsForOthers = null;
    String[] compNamesForOthers = null;
    String[] itemIdsForOthers = null;
    String[] itemNamesForOthers = null;
    String[] paplCodesForOthers = null;
    String[] quanditysForOthers = null;
    String[] uomIdsForOthers = null;
    String[] uomNamesForOthers = null;
    int total = 0;
    String[] unitPrices = null;
    String[] unitPricesForOthers = null;
    String itemId = "";
    String itemName = "";
    String paplCode = "";
    String uomName = "";
    String uomId = "";
    String sectionId = "";
    String sectionName = "";
    String compId = "";
    String compName = "";
    String KgunitPrice = "";
    String unitPrice = "";
    String quandity = "";
    String servicePointName = "";
    String servicePointId = "";
    String[] selectedIndex = null;
    String[] sectionIds = null;
    String[] sectionNames = null;
    String[] paplCodes = null;
    String[] itemNames = null;
    String[] uomNames = null;
    String[] itemIds = null;
    String[] uomIds = null;
    String[] quanditys = null;
    String[] servicePointIds = null;
    String[] servicePointNames = null;
    String scrapId = "";
    String lastId = "";
    String kgTotalRate = "";
    String totalAmountAll = "";
    int kgTotalWg = 0;
    int kgTotalAmount = 0;
    String fromDate = "";
    String toDate = "";
    
    String[] approveIds = null;
    String approveId = "";
    String status = "";
    String approvedBy = "";
    String requestedQty = "";
    String priceId = "";
    String stkQty = "";
    
    

    public int getKgTotalAmount() {
        return kgTotalAmount;
    }

    public void setKgTotalAmount(int kgTotalAmount) {
        this.kgTotalAmount = kgTotalAmount;
    }

    public int getKgTotalWg() {
        return kgTotalWg;
    }

    public void setKgTotalWg(int kgTotalWg) {
        this.kgTotalWg = kgTotalWg;
    }
    String noAmountUnique = "";
    String kgAmountUnique = "";

    public String getKgAmountUnique() {
        return kgAmountUnique;
    }

    public void setKgAmountUnique(String kgAmountUnique) {
        this.kgAmountUnique = kgAmountUnique;
    }

    public String getNoAmountUnique() {
        return noAmountUnique;
    }

    public void setNoAmountUnique(String noAmountUnique) {
        this.noAmountUnique = noAmountUnique;
    }

    public String getLastId() {
        return lastId;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public String getTotalAmountAll() {
        return totalAmountAll;
    }

    public void setTotalAmountAll(String totalAmountAll) {
        this.totalAmountAll = totalAmountAll;
    }

    public String getKgTotalRate() {
        return kgTotalRate;
    }

    public void setKgTotalRate(String kgTotalRate) {
        this.kgTotalRate = kgTotalRate;
    }

    public void setScrapId(String scrapId) {
        this.scrapId = scrapId;
    }

    public String getScrapId() {
        return scrapId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getKgunitPrice() {
        return KgunitPrice;
    }

    public void setKgunitPrice(String KgunitPrice) {
        this.KgunitPrice = KgunitPrice;
    }

    public String[] getUnitPricesForOthers() {
        return unitPricesForOthers;
    }

    public void setUnitPricesForOthers(String[] unitPricesForOthers) {
        this.unitPricesForOthers = unitPricesForOthers;
    }

    public String[] getUnitPrices() {
        return unitPrices;
    }

    public void setUnitPrices(String[] unitPrices) {
        this.unitPrices = unitPrices;
    }

    public String[] getCompIdsForKg() {
        return compIdsForKg;
    }

    public void setCompIdsForKg(String[] compIdsForKg) {
        this.compIdsForKg = compIdsForKg;
    }

    public String[] getCompIdsForOthers() {
        return compIdsForOthers;
    }

    public void setCompIdsForOthers(String[] compIdsForOthers) {
        this.compIdsForOthers = compIdsForOthers;
    }

    public String[] getCompNamesForKg() {
        return compNamesForKg;
    }

    public void setCompNamesForKg(String[] compNamesForKg) {
        this.compNamesForKg = compNamesForKg;
    }

    public String[] getCompNamesForOthers() {
        return compNamesForOthers;
    }

    public void setCompNamesForOthers(String[] compNamesForOthers) {
        this.compNamesForOthers = compNamesForOthers;
    }

    public String[] getItemIdsForKg() {
        return itemIdsForKg;
    }

    public void setItemIdsForKg(String[] itemIdsForKg) {
        this.itemIdsForKg = itemIdsForKg;
    }

    public String[] getItemIdsForOthers() {
        return itemIdsForOthers;
    }

    public void setItemIdsForOthers(String[] itemIdsForOthers) {
        this.itemIdsForOthers = itemIdsForOthers;
    }

    public String[] getItemNamesForKg() {
        return itemNamesForKg;
    }

    public void setItemNamesForKg(String[] itemNamesForKg) {
        this.itemNamesForKg = itemNamesForKg;
    }

    public String[] getItemNamesForOthers() {
        return itemNamesForOthers;
    }

    public void setItemNamesForOthers(String[] itemNamesForOthers) {
        this.itemNamesForOthers = itemNamesForOthers;
    }

    public String[] getPaplCodesForKg() {
        return paplCodesForKg;
    }

    public void setPaplCodesForKg(String[] paplCodesForKg) {
        this.paplCodesForKg = paplCodesForKg;
    }

    public String[] getPaplCodesForOthers() {
        return paplCodesForOthers;
    }

    public void setPaplCodesForOthers(String[] paplCodesForOthers) {
        this.paplCodesForOthers = paplCodesForOthers;
    }

    public String[] getQuanditysForKg() {
        return quanditysForKg;
    }

    public void setQuanditysForKg(String[] quanditysForKg) {
        this.quanditysForKg = quanditysForKg;
    }

    public String[] getQuanditysForOthers() {
        return quanditysForOthers;
    }

    public void setQuanditysForOthers(String[] quanditysForOthers) {
        this.quanditysForOthers = quanditysForOthers;
    }

    public String[] getUomIdsForKg() {
        return uomIdsForKg;
    }

    public void setUomIdsForKg(String[] uomIdsForKg) {
        this.uomIdsForKg = uomIdsForKg;
    }

    public String[] getUomIdsForOthers() {
        return uomIdsForOthers;
    }

    public void setUomIdsForOthers(String[] uomIdsForOthers) {
        this.uomIdsForOthers = uomIdsForOthers;
    }

    public String[] getUomNamesForKg() {
        return uomNamesForKg;
    }

    public void setUomNamesForKg(String[] uomNamesForKg) {
        this.uomNamesForKg = uomNamesForKg;
    }

    public String[] getUomNamesForOthers() {
        return uomNamesForOthers;
    }

    public void setUomNamesForOthers(String[] uomNamesForOthers) {
        this.uomNamesForOthers = uomNamesForOthers;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getItemNames() {
        return itemNames;
    }

    public void setItemNames(String[] itemNames) {
        this.itemNames = itemNames;
    }

    public String[] getPaplCodes() {
        return paplCodes;
    }

    public void setPaplCodes(String[] paplCodes) {
        this.paplCodes = paplCodes;
    }

    public String getQuandity() {
        return quandity;
    }

    public void setQuandity(String quandity) {
        this.quandity = quandity;
    }

    public String[] getQuanditys() {
        return quanditys;
    }

    public void setQuanditys(String[] quanditys) {
        this.quanditys = quanditys;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String[] getSectionIds() {
        return sectionIds;
    }

    public void setSectionIds(String[] sectionIds) {
        this.sectionIds = sectionIds;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String[] getSectionNames() {
        return sectionNames;
    }

    public void setSectionNames(String[] sectionNames) {
        this.sectionNames = sectionNames;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getServicePointId() {
        return servicePointId;
    }

    public void setServicePointId(String servicePointId) {
        this.servicePointId = servicePointId;
    }

    public String[] getServicePointIds() {
        return servicePointIds;
    }

    public void setServicePointIds(String[] servicePointIds) {
        this.servicePointIds = servicePointIds;
    }

    public String getServicePointName() {
        return servicePointName;
    }

    public void setServicePointName(String servicePointName) {
        this.servicePointName = servicePointName;
    }

    public String[] getServicePointNames() {
        return servicePointNames;
    }

    public void setServicePointNames(String[] servicePointNames) {
        this.servicePointNames = servicePointNames;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String[] getUomIds() {
        return uomIds;
    }

    public void setUomIds(String[] uomIds) {
        this.uomIds = uomIds;
    }

    public String[] getUomNames() {
        return uomNames;
    }

    public void setUomNames(String[] uomNames) {
        this.uomNames = uomNames;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String[] getApproveIds() {
        return approveIds;
    }

    public void setApproveIds(String[] approveIds) {
        this.approveIds = approveIds;
    }

    public String getApproveId() {
        return approveId;
    }

    public void setApproveId(String approveId) {
        this.approveId = approveId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getRequestedQty() {
        return requestedQty;
    }

    public void setRequestedQty(String requestedQty) {
        this.requestedQty = requestedQty;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getStkQty() {
        return stkQty;
    }

    public void setStkQty(String stkQty) {
        this.stkQty = stkQty;
    }
    
    
    
}
