/*------------------------------------------------------------------------------
 * FactoryPortalConstants.java
 * Mar 19, 2008
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/

package ets.domain.util;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver     Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0    Mar 19, 2008                       ES		            Created
 *
 *****************************************************************************/

/**
 * <br>FactoryPortalConstants object :This class have all the Event Protal constants.
 * @author Satheeshkumar P
 * @version 1.0 19 Mar 2008
 * @since   Event Portal Iteration 0
*/
public class FactoryPortalConstants {

	/*
	 * the values need to be configured in the Properties file and read from
	 * there.
	 *
	 */
	/**
	 * DEFAULT_COMMAND_OBJECT- DEFAULT_COMMAND_OBJECT
	 */
	public static final String DEFAULT_COMMAND_OBJECT = "command";

	/**
	 * ERROR_VIEW- ERROR_VIEW
	 */
	public static final String ERROR_VIEW = ".fp.error";


	/**
	 * JOBGRP_VIEW
	 */
	public static final String JOBGRP_VIEW = ".fp.addJobGroup";

	/***************************************************************************
	 * Constants for Event Inbox
	 **************************************************************************/
	/**
	 * FACTORY_INBOX_VIEW - FACTORY_INBOX_VIEW
	 */
	public static final String FACTORY_INBOX_VIEW = ".fp.factoryInbox";


	/*
	 * Event Inbox Request statuses.
	 */
	/**
	 * FIRS_NEW - FIRS_NEW
	 */
	public static final String FIRS_NEW = "New � Yet to be processed";

	/**
	 * FIRS_UNSUPPORTED_DOC -
	 */
	public static final String FIRS_UNSUPPORTED_DOC =
										"UnSupported Document Type ";

	/**
	 * FIRS_DUPLICATE
	 */
	public static final String FIRS_DUPLICATE = "Duplicate Request";

	/**
	 * FIRS_JOB_CREATED
	 */
	public static final String FIRS_JOB_CREATED = "Job created";

	/**
	 * FIRS_SUBSCRIPTION_UNAVAILABLE
	 */
	public static final String FIRS_SUBSCRIPTION_UNAVAILABLE =
												"Subscription UnAvailable";

	/**
	 * FIRS_DEPENDENT_DOC_UNAVAILABLE
	 */
	public static final String FIRS_DEPENDENT_DOC_UNAVAILABLE =
											"Dependent Document UnAvailable";

	/**
	 * FIRS_DEPENDENT_DOC_INPROCESSING
	 */
	public static final String FIRS_DEPENDENT_DOC_INPROCESSING =
										"Dependent Document In Processing";
	/**
	 * FIRS_FILE_MOVE_IN_PROGRESS
	 */
	public static final String FIRS_FILE_MOVE_IN_PROGRESS = "FILE MOVE IN PROGRESS";
	/***************************************************************************
	 * End of constants for Event Inbox
	 **************************************************************************/



	/**
	 * JOB_STATUS_SUBS_UNAVIAL
	 */
	public static final String JOB_STATUS_SUBS_UNAVIAL =
												"Subscription UnAvailable";

	/**
	 * JOB_STATUS_DEPEND_DOC_UNAVAIL
	 */
	public static final String JOB_STATUS_DEPEND_DOC_UNAVAIL =
										"Dependent Document UnAvailable";

	/**
	 * JOB_STATUS_DUPLICATE_REQ
	 */
	public static final String JOB_STATUS_DUPLICATE_REQ =
												"Duplicate Request";

	/**
	 * JOB_TYPE
	 */
	public static final String JOB_TYPE = "MANUAL";

	/**
	 * JOB_STATUS_TO_BE_PROCESSED
	 */
	public static final String JOB_STATUS_TO_BE_PROCESSED =
														"To be Processed";

	/**
	 * JOB_STATUS_IN_PROCESSING
	 */
	public static final String JOB_STATUS_IN_PROCESSING = "In Processing"; //9

	/**
	 * JOB_STATUS_PROCESSING_COMPLETE
	 */
	public static final String JOB_STATUS_PROCESSING_COMPLETE
										= "Processing Complete";//10

	/**
	 * JOB_STATUS_PROCESSING_FAILED
	 */
	public static final String JOB_STATUS_PROCESSING_FAILED =
													"Processing Failed"; //11

	/**
	 * JOB_STATUS_DEPEND_DOC_IN_PROCESS
	 */
	public static final String JOB_STATUS_DEPEND_DOC_IN_PROCESS =
								"Dependent Document In Processing"; //12

	/**
	 * JOB_STATUS_RAW_DATA_FILE_NOT_FOUND
	 */
	public static final String JOB_STATUS_RAW_DATA_FILE_NOT_FOUND =
										"Raw Data File - Not Found"; //13

	/**
	 * JOB_STATUS_MATCHING_WORKORDER_STATUS_NOT_FOUND
	 */
	public static final String JOB_STATUS_MATCHING_WORKORDER_STATUS_NOT_FOUND =
			"Matching workorder / status details not found in Event"; //14

	/**
	 * BAD_DOC_DELIV
	 */
	public static final String BAD_DOC_DELIV = "15";//"Bad Doc Delivered";

	/**
	 *     UNSUPPORTED_DOC_TYPE
	 */
	public static final String UNSUPPORTED_DOC_TYPE =
										"Unsupported Document Type";

	/**
	 * SPRING_LOG4J_LOCATION
	 */
	public static final String SPRING_LOG4J_LOCATION =
										"classpath:springLog4j.properties";
/**
 * PROD_ENVIRONMENT
 */
	public static final String PROD_ENVIRONMENT = "PRODUCTION";


}
