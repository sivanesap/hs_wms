package ets.domain.wms.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.wms.data.WmsDAO;
import ets.domain.wms.business.WmsTO;

import java.util.ArrayList;
import java.util.Iterator;

public class WmsBP {

    public static ArrayList getWhList(SecondaryOperationTO SecondaryOperationTO) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public WmsBP() {
    }
    WmsDAO wmsDAO;

    public WmsDAO getWmsDAO() {
        return wmsDAO;
    }

    public void setWmsDAO(WmsDAO wmsDAO) {
        this.wmsDAO = wmsDAO;
    }

    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises ..
     */
    public ArrayList getAsnMasterList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnMasterList = new ArrayList();
        asnMasterList = wmsDAO.getAsnMasterList(wmsTo);
        return asnMasterList;
    }

    public ArrayList getAsnDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnDetails = new ArrayList();
        asnDetails = wmsDAO.getAsnDetails(wmsTo);
        return asnDetails;
    }

    public ArrayList getviewDeliveryRequest(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList asnMasterList = new ArrayList();
        asnMasterList = wmsDAO.getviewDeliveryRequest(wmsTo);
        return asnMasterList;
    }

    public ArrayList getDeliveryRequestUpdate(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getDeliveryRequestUpdate = new ArrayList();
        getDeliveryRequestUpdate = wmsDAO.getDeliveryRequestUpdate(wmsTo);
        return getDeliveryRequestUpdate;
    }

    public int getDeliveryRequestFinalUpdate(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = wmsDAO.getDeliveryRequestFinalUpdate(wmsTo, userId, session);

            if (status > 0) {
                session.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int saveSerialTempDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = wmsDAO.saveSerialTempDetails(wmsTo, userId);
        return insertStatus;
    }

    //     public int saveGrnSerialDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
    //        int insertStatus = 0;
    //        insertStatus = wmsDAO.saveGrnSerialDetails(wmsTo, userId);
    //        return insertStatus;
    //    }
    public int saveGrnSerialDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();
            insertStatus = wmsDAO.saveGrnSerialDetails(wmsTo, userId, session);
            System.out.println("saveGrnSerialDetails---" + insertStatus);
            session.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }

    public int saveGrnSerialStockDetails(WmsTO wmsTo, String actualFilePath, String actualFilePath1, String fileSaved, String fileSaved1, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        SqlMapClient session = wmsDAO.getSqlMapClient();
        try {
            session.startTransaction();

            String path = wmsDAO.grnImageFileUpload(wmsTo, actualFilePath, fileSaved, session);
            String path1 = wmsDAO.grnEwayImageFileUpload(wmsTo, actualFilePath1, fileSaved1, session);
            insertStatus = wmsDAO.saveGrnSerialStockDetails(wmsTo, path, userId, session);
            System.out.println("saveGrnSerialStockDetails--BP-" + insertStatus);
            session.commitTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return insertStatus;
    }

    public ArrayList getSerialTempDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList serialTempDetails = new ArrayList();
        serialTempDetails = wmsDAO.getSerialTempDetails(wmsTo);
        return serialTempDetails;
    }

    public ArrayList getSerialTempDetailsUnsed(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList serialTempDetails = new ArrayList();
        serialTempDetails = wmsDAO.getSerialTempDetailsUnsed(wmsTo);
        return serialTempDetails;
    }

    public String serialNoAlreadyExists(String SerialNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.serialNoAlreadyExists(SerialNo);
        return status;
    }

    public String checkRouteCode(String routeCode) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.checkRouteCode(routeCode);
        return status;
    }

    public String invoiceNoAlreadyExists(String invoiceNo) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = wmsDAO.InvoiceNoAlreadyExists(invoiceNo);
        return status;
    }

    public ArrayList getDockInList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getDockInList = new ArrayList();
        getDockInList = wmsDAO.getDockInList(wmsTo);
        return getDockInList;
    }

    public ArrayList getDockInDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList dockInDetails = new ArrayList();
        dockInDetails = wmsDAO.getDockInDetails(wmsTo);
        return dockInDetails;
    }

    public ArrayList getBinList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getBinList = new ArrayList();
        getBinList = wmsDAO.getBinList(wmsTo);
        return getBinList;
    }

    public ArrayList grnViewDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList grnViewDetails = new ArrayList();
        grnViewDetails = wmsDAO.grnViewDetails(wmsTo);
        return grnViewDetails;
    }

    public ArrayList viewSerialNumberDetails(String grnId) throws FPRuntimeException, FPBusinessException {
        ArrayList viewSerialNumberDetails = new ArrayList();
        viewSerialNumberDetails = wmsDAO.viewSerialNumberDetails(grnId);
        return viewSerialNumberDetails;
    }

    public ArrayList getOrderDetailsList(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList ordersList = new ArrayList();
        ordersList = wmsDAO.getOrderDetailsList(wmsTo);
        return ordersList;
    }

    public ArrayList getWareHouseList() throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouseList = new ArrayList();
        getWareHouseList = wmsDAO.getWareHouseList();
        return getWareHouseList;
    }

    public String getBarcodeMaster() throws FPRuntimeException, FPBusinessException {
        String getBarcodeMaster = "";
        getBarcodeMaster = wmsDAO.getBarcodeMaster();
        return getBarcodeMaster;
    }

    public ArrayList getBarcodeDetails(String itemId,WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        ArrayList getBarcodeDetails = new ArrayList();
        getBarcodeDetails = wmsDAO.getBarcodeDetails(itemId,wmsTo);
        return getBarcodeDetails;
    }

    public int saveTempGrnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int saveTempGrnDetails = 0;
        saveTempGrnDetails = wmsDAO.saveTempGrnDetails(wmsTo, userId);
        return saveTempGrnDetails;
    }

    public int updateSerialTempDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int updateSerialTempDetails = 0;
        updateSerialTempDetails = wmsDAO.updateSerialTempDetails(wmsTo, userId);
        return updateSerialTempDetails;
    }

    public int generateBarcode(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int generateBarcode = 0;
        generateBarcode = wmsDAO.generateBarcode(wmsTo, userId);
        return generateBarcode;
    }

    public int delGrnTempSerial(String tempId, String grnId) throws FPRuntimeException, FPBusinessException {
        int delGrnTempSerial = 0;
        delGrnTempSerial = wmsDAO.delGrnTempSerial(tempId, grnId);
        return delGrnTempSerial;
    }

    public ArrayList getPoCode(WmsTO wmsTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getPoCode = new ArrayList();
        getPoCode = wmsDAO.getPoCode(wmsTO, userId);
        return getPoCode;
    }

    public ArrayList getCustList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCustList = new ArrayList();
        getCustList = wmsDAO.getCustList(wmsTO);
        return getCustList;
    }

    public ArrayList getItemList(int userId, String custId) throws FPRuntimeException, FPBusinessException {
        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.getItemList(userId, custId);
        return getItemList;
    }

    public int saveGtnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {
        int saveGtnDetails = 0;
        saveGtnDetails = wmsDAO.saveGtnDetails(wmsTo, userId);
        return saveGtnDetails;
    }

    public ArrayList getReceivedGateIn(int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList getReceivedGateIn = new ArrayList();
        getReceivedGateIn = wmsDAO.getReceivedGateIn(userId);
        return getReceivedGateIn;
    }

    public ArrayList getGtnDetails(WmsTO wmsTo, int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList gtnDetails = new ArrayList();
        gtnDetails = wmsDAO.getGtnDetails(wmsTo, userId);
        return gtnDetails;
    }

    public ArrayList getUserDetails(int UserId) throws FPRuntimeException, FPBusinessException {

        ArrayList getUserDetails = new ArrayList();
        getUserDetails = wmsDAO.getUserDetails(UserId);
        return getUserDetails;
    }

    public ArrayList getBinDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getBinDetails = new ArrayList();
        getBinDetails = wmsDAO.getBinDetails(wmsTo);
        return getBinDetails;
    }
//        public ArrayList getSerialDetails() throws FPRuntimeException, FPBusinessException {
//
//        ArrayList getSerialDetails = new ArrayList();
//        getSerialDetails = wmsDAO.getSerialDetails();
//        return getSerialDetails;
//    }

    public ArrayList getProList(int userId, WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList getProList = new ArrayList();
        getProList = wmsDAO.getProList(userId, wmsTo);
        return getProList;
    }

    public ArrayList proListDetails(int userId, WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {

        ArrayList proListDetails = new ArrayList();
        proListDetails = wmsDAO.proListDetails(userId, wmsTo);
        return proListDetails;
    }

    public ArrayList getSerialDets(String gtnId, String grnId, String itemId, String asnId) throws FPRuntimeException, FPBusinessException {

        ArrayList getSerialDets = new ArrayList();
        getSerialDets = wmsDAO.getSerialDet(gtnId, grnId, itemId, asnId);
        return getSerialDets;
    }

    public ArrayList getStateList() throws FPRuntimeException, FPBusinessException {

        ArrayList getStateList = new ArrayList();
        getStateList = wmsDAO.getStateList();
        return getStateList;
    }

    public ArrayList getWhList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWhList = new ArrayList();
        getWhList = wmsDAO.getWhList();
        return getWhList;
    }

    public ArrayList warehouseOrderReport(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList warehouseOrderReport = new ArrayList();
        warehouseOrderReport = wmsDAO.warehouseOrderReport(wms);
        return warehouseOrderReport;
    }

    public ArrayList getItemLists(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.getItemLists(userId);
        return getItemList;
    }

    public int insertAsnDetails(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {

            status = wmsDAO.insertAsnDetails(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList processGetItemList(String whId) throws FPRuntimeException, FPBusinessException {

        ArrayList getItemList = new ArrayList();
        getItemList = wmsDAO.processGetItemList(whId);
        return getItemList;

    }

    public ArrayList getSerialNumber(String itemId, String userWhId) throws FPRuntimeException, FPBusinessException {

        ArrayList getSerialNumber = new ArrayList();
        getSerialNumber = wmsDAO.processGetSerialNumber(itemId, userWhId);
        return getSerialNumber;

    }

    public ArrayList getWareToHouseList(String userWhId) throws FPRuntimeException, FPBusinessException {
        ArrayList getWareHouseList = new ArrayList();
        getWareHouseList = wmsDAO.getWareToHouseList(userWhId);
        return getWareHouseList;
    }

    public int insertTransferQty(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {

            status = wmsDAO.insertTransferQty(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList processVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = wmsDAO.ProcessVendorList();
        return vendorList;
    }

    public ArrayList barCodeDetails(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList barCodeDetails = new ArrayList();
        barCodeDetails = wmsDAO.barCodeDetails(wmsTO);
        return barCodeDetails;
    }

    public String getUserWarehouse(int userId) throws FPRuntimeException, FPBusinessException {
        String getUserWarehouse = "";
        getUserWarehouse = wmsDAO.getUserWarehouse(userId);
        return getUserWarehouse;
    }

    public ArrayList serialNumberDetails(WmsTO wms) throws FPRuntimeException, FPBusinessException {
        ArrayList serialNumberDetails = new ArrayList();
        serialNumberDetails = wmsDAO.getserialNumberDetails(wms);
        return serialNumberDetails;
    }

    public int shipmentUpload(WmsTO wmsTO, int userId) {
        int shipmentUpload = 0;
        shipmentUpload = wmsDAO.shipmentUpload(wmsTO, userId);
        return shipmentUpload;
    }

    public int uploadRunsheet(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadRunsheet = 0;
        uploadRunsheet = wmsDAO.uploadRunsheet(wmsTO);
        return uploadRunsheet;
    }

    public int ifbOrderUpload(WmsTO wmsTO, int userId, String whId) {
        int ifbOrderUpload = 0;
        ifbOrderUpload = wmsDAO.ifbOrderUpload(wmsTO, userId, whId);
        return ifbOrderUpload;
    }

    public int deleteIfbUploadTemp(int userId) {
        int deleteIfbUploadTemp = 0;
        deleteIfbUploadTemp = wmsDAO.deleteIfbUploadTemp(userId);
        return deleteIfbUploadTemp;
    }

    public int deleteNormalOrderTemp(int userId) {
        int deleteNormalOrderTemp = 0;
        deleteNormalOrderTemp = wmsDAO.deleteNormalOrderTemp(userId);
        return deleteNormalOrderTemp;
    }

    public int deleteReplacementOrderTemp(int userId) {
        int deleteReplacementOrderTemp = 0;
        deleteReplacementOrderTemp = wmsDAO.deleteReplacementOrderTemp(userId);
        return deleteReplacementOrderTemp;
    }

    public int deleteReturnOrderTemp(int userId) {
        int deleteReturnOrderTemp = 0;
        deleteReturnOrderTemp = wmsDAO.deleteReturnOrderTemp(userId);
        return deleteReturnOrderTemp;
    }

    public int deleteRunsheetTemp(int userId) {
        int deleteRunsheetTemp = 0;
        deleteRunsheetTemp = wmsDAO.deleteRunsheetTemp(userId);
        return deleteRunsheetTemp;
    }

    public ArrayList getRunsheetUpload(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getRunsheetUpload = new ArrayList();
        getRunsheetUpload = wmsDAO.getRunsheetUpload(wms);
        return getRunsheetUpload;
    }

    public ArrayList getIFBList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getIFBList = new ArrayList();
        getIFBList = wmsDAO.getIFBList(wms);
        return getIFBList;
    }

    public int uploadIfbOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadIfbOrder = 0;
        uploadIfbOrder = wmsDAO.uploadIfbOrder(wmsTO);
        return uploadIfbOrder;
    }

    public int saveIfbLr(String[] lrId, String[] ewayBillNo, String[] ewayExpiry, String[] selected) throws FPRuntimeException, FPBusinessException {
        int saveIfbLr = 0;
        saveIfbLr = wmsDAO.saveIfbLr(lrId, ewayBillNo, ewayExpiry, selected);
        return saveIfbLr;
    }

    public ArrayList ifbLRList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRList = wmsDAO.ifbLRList(wmsTO);
        return ifbLRList;
    }

    public ArrayList getIfbRunsheetList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbRunsheetList = wmsDAO.getIfbRunsheetList(wmsTO);
        return getIfbRunsheetList;
    }

    public ArrayList getFkRunsheetList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFkRunsheetList = wmsDAO.getFkRunsheetList(wmsTO);
        return getFkRunsheetList;
    }

    public ArrayList getwmsItemReport(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getwmsItemReport = wmsDAO.getwmsItemReport(wmsTO);
        return getwmsItemReport;
    }

    public ArrayList getwmsItemReportView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getwmsItemReport = wmsDAO.getwmsItemReportView(wmsTO);
        return getwmsItemReport;
    }

    public int UpdateSku(WmsTO wmsTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;

        try {

            status = wmsDAO.UpdateSku(wmsTo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public ArrayList hubOutList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutList = wmsDAO.hubOutList(wmsTO);
        return hubOutList;
    }

    public ArrayList hubOutListView(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutListView = wmsDAO.hubOutListView(wmsTO);
        return hubOutListView;
    }

    public ArrayList hubOutPrintDetails(String lrId, String compId) throws FPRuntimeException, FPBusinessException {
        ArrayList hubOutPrintDetails = wmsDAO.hubOutPrintDetails(lrId,compId);
        return hubOutPrintDetails;
    }

    public ArrayList getIfbOrderDetails(String lrId) throws FPRuntimeException, FPBusinessException {
        ArrayList getIfbOrderDetails = wmsDAO.getIfbOrderDetails(lrId);
        return getIfbOrderDetails;
    }

    public int updateIfbHubout(String[] ewayBillNo, String[] ewayExpiry,String[] orderIds, String[] selectedStatus, String[] hubId, String vehicleNo,String driverName,String driverMobile,String date) throws FPRuntimeException, FPBusinessException {
        int updateIfbHubout = 0;
        updateIfbHubout = wmsDAO.updateIfbHubout(ewayBillNo,ewayExpiry,orderIds, selectedStatus, hubId,vehicleNo,driverName,driverMobile,date);
        return updateIfbHubout;
    }

    public int updateHubIn(String[] orderIds, String[] selectedStatus) throws FPRuntimeException, FPBusinessException {
        int updateHubIn = 0;
        updateHubIn = wmsDAO.updateHubIn(orderIds, selectedStatus);
        return updateHubIn;
    }

    public ArrayList ifbHubinList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbHubinList = wmsDAO.ifbHubinList(wmsTO);
        return ifbHubinList;
    }

    public ArrayList getCustomerContract(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getContractList = new ArrayList();
        getContractList = wmsDAO.getCustomerContract(wms);
        return getContractList;
    }

    public int uploadCustomerContract(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadCustomerContractist = 0;
        uploadCustomerContractist = wmsDAO.uploadCustomerContract(wmsTO);
        return uploadCustomerContractist;
    }

    public int SaveOrderApproval(WmsTO wmsTO, int userId) {
        int SaveOrderApproval = 0;
        SaveOrderApproval = wmsDAO.SaveOrderApproval(wmsTO, userId);

        return SaveOrderApproval;
    }

    public ArrayList getreplacementList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getreplacementList = new ArrayList();
        getreplacementList = wmsDAO.getreplacementList(wms);
        return getreplacementList;
    }

    public int uploadReplacementOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadReplacementOrder = 0;
        uploadReplacementOrder = wmsDAO.uploadReplacementOrder(wmsTO);
        return uploadReplacementOrder;
    }

    public int replacementContractUpload(WmsTO wmsTO, int userId) {
        int replacementContractUpload = 0;
        replacementContractUpload = wmsDAO.replacementContractUpload(wmsTO, userId);

        return replacementContractUpload;
    }

    public ArrayList getreturnList(WmsTO wms) throws FPRuntimeException, FPBusinessException {

        ArrayList getreturnList = new ArrayList();
        getreturnList = wmsDAO.getreturnList(wms);
        return getreturnList;
    }

    public int uploadReturnordertemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        int uploadReturnordertemp = 0;
        uploadReturnordertemp = wmsDAO.uploadReturnordertemp(wmsTO);
        return uploadReturnordertemp;
    }

    public int returnContractUpload(WmsTO wmsTO, int userId) {
        SqlMapClient session = wmsDAO.getSqlMapClient();
        int returnContractUpload = 0;
        returnContractUpload = wmsDAO.returnContractUpload(wmsTO, userId);
        return returnContractUpload;
    }

    public ArrayList ifbLRUploadList(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRUploadList = wmsDAO.ifbLRUploadList(wmsTO);
        return ifbLRUploadList;
    }

    public ArrayList ifbLRPrintlist(String lrId) throws FPRuntimeException, FPBusinessException {
        ArrayList ifbLRPrintlist = wmsDAO.ifbLRPrintlist(lrId);
        return ifbLRPrintlist;
    }
}
