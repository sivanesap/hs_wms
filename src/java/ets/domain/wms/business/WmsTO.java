package ets.domain.wms.business;

/**
 *
 * @author hp
 */
public class WmsTO {

    public WmsTO() {
    }

    private double unusedQty = 0;
    private double actQty = 0;
    private double reqQty = 0;
    private String binId = "";
    private String binName = "";
    private String whId = "";
    private String invoiceNo = "";
    private String ewaybillNo = "";
    private String invoiceDate = "";
    private String grnId = "";
    private String requestQty = "";
    private String qty = "";
    private String serialNos[] = null;
    private String whIds[] = null;
    private String serialNo = "";
    private String asnDetId = "";
    private String custName = "";
    private String itemName = "";
    private String skuCode = "";
    private String hsnCode = "";
    private String actualQuantity = "";

    private String itemId = "";
    private String productId = "";
    private String serialNumber = "";
    private String orderId = "";
    private String orderType = "";
    private String batchDate = "";
    private String branchName = "";
    private String delRequestId = "";
    private String model = "";
    private String productID = "";
    private String createdDate = "";
    private String path = "";
    private String reMarks = "";
    private String loanProposalId = "";
    private String barCode = "";
    private String asnId = "";
    private String asnNo = "";
    private String asnDate = "";
    private String vendorName = "";
    private String asnPath = "";
    private String asnRemarks = "";
    private String CustomerName = "";
    private String userId = "";
    private String warehouseId = "";
//    kova
    private String gtnNo = "";
    private String gtnId = "";
    private String gtnDate = "";
    private String gtnTime = "";
    private String custId = "";
    private String dock = "";
    private String vehicleType = "";
    private String vehicleNo = "";
    private String driverName = "";
    private String remarks = "";

    private String grnNo = "";
    private String grnDate = "";
    private String asnReqId = "";
    private String excessQty = "";
    private String fromDate = "";
    private String toDate = "";
    private String whName = "";
    private String proCon = "";
    private String ipQty[] = null;
    private String uom[] = null;
    private String proCond[] = null;
    private String uom1 = "";
    private String inpQty = "";
    private String proCode = "";
    private String proId = "";
    private String proName = "";
    private String CustId = "";
    private String poName = "";
    private String poNos[] = null;
    private String poQty[] = null;
    private String poQuantity = "";
    private String empId = "";
    private String empName = "";
    private String gtnDetId = "";
    private String empUserId = "";
    private String excess = "";
    private String damaged = "";
    private String shortage = "";
    private String binFlag = "";
    private String binIds[] = null;
    private String serialNoTemp = "";
    private String binIdTemp = "";
    private String proConTemp = "";
    private String serialNoTemp1 = "";
    private String binIdTemp1 = "";
    private String proConTemp1 = "";
    private Double asnQty = null;
    private String prevPoQuantity = "";
    private String serials = "";
    private String tempIds = "";
    private String uoms = "";
    private String bins = "";
    private String proCons = "";
    private String grnId1 = "";
    private String barcode = "";
    private String stateId = "";
    private String stateName = "";
    private String tripQty = "";
    private String ofd = "";
    private String delivered = "";
    private String returned = "";
    private String cancelled = "";
    private String cancelled1 = "";
    private int count = 0;
    private String warehouseToId = "";
    private String[] selectedIndex = null;
    private String vendorId = "";

    private String poDate = "";
    private String PoRequestId = "";
    private String poNumber = "";

    private String remark = "";
    private String Quantity = "";
    private String stockId = "";
    private String asnid = "";
    private String loanid = "";
    private String statusName = "";
    private String runSheetId = "";
    private String trackingId = "";
    private String mobileNo = "";
    private String name = "";
    private String address = "";
    private String shipmentType = "";
    private String codAmount = "";
    private String assignedUser = "";
    private String status = "";
    private String date = "";
    private String amountPaid = "";
    private String receiverName = "";
    private String receiver = "";
    private String relation = "";
    private String mobile = "";
    private String signature = "";
    private String vertical = "";
    private String orderNo = "";
    private String productDetails = "";
    private String billDoc = "";
    private String billDate = "";
    private String purchaseOrderNumber = "";
    private String matlGroup = "";
    private String material = "";
    private String materialDescription = "";
    private String billQty = "";
    private String city = "";
    private String shipTo = "";
    private String partyName = "";
    private String gstinNumber = "";
    private String grossValue = "";
    private String pincode = "";
    private String flag = "";
    private String error = "";
    private String lrId = "";
    private String lrNo = "";
    private String ewayBillNo = "";
    private String ewayExpiry = "";
    private String skuId = "";
    private String pickupaddress = "";
    private String itemNos = "";
    private String pickupDate = "";
    private String lastStatus = "";
    private String shipmentId = "";
    private String pinCode = "";
    private String deliveryHub = "";
    private String assignedExecutive = "";
    private String origin = "";
    private String currentHub = "";
    private String note = "";
    private String amount = "";
    private String weight = "";
    private String lastModified = "";
    private String lastReceived = "";
    private String shipmentTierType = "";
    private String reverseShipmentId = "";
    private String bagStatus = "";
    private String type = "";
    private String price = "";
    private String latestStatus = "";
    private String currentLocation = "";
    private String latestUpdateDateTime = "";
    private String firstReceivedHub = "";
    private String firstReceiveDateTime = "";
    private String hubNotes = "";
    private String csNotes = "";
    private String numberofAttempts = "";
    private String consignmentId = "";
    private String customerPromiseDate = "";
    private String logisticsPromiseDate = "";
    private String onHoldByOpsReason = "";
    private String onHoldByOpsDate = "";
    private String bagId = "";
    private String firstAssignedHubName = "";
    private String deliveryPinCode = "";
    private String lastReceivedDateTime = "";
    private String exchange = "";
    private String dispatchId = "";
    private String barcodeId = "";

    public String getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(String barcodeId) {
        this.barcodeId = barcodeId;
    }

    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOnHoldByOpsDate() {
        return onHoldByOpsDate;
    }

    public void setOnHoldByOpsDate(String onHoldByOpsDate) {
        this.onHoldByOpsDate = onHoldByOpsDate;
    }

    public String getBagId() {
        return bagId;
    }

    public void setBagId(String bagId) {
        this.bagId = bagId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getDeliveryHub() {
        return deliveryHub;
    }

    public void setDeliveryHub(String deliveryHub) {
        this.deliveryHub = deliveryHub;
    }

    public String getAssignedExecutive() {
        return assignedExecutive;
    }

    public void setAssignedExecutive(String assignedExecutive) {
        this.assignedExecutive = assignedExecutive;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getCurrentHub() {
        return currentHub;
    }

    public void setCurrentHub(String currentHub) {
        this.currentHub = currentHub;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastReceived() {
        return lastReceived;
    }

    public void setLastReceived(String lastReceived) {
        this.lastReceived = lastReceived;
    }

    public String getShipmentTierType() {
        return shipmentTierType;
    }

    public void setShipmentTierType(String shipmentTierType) {
        this.shipmentTierType = shipmentTierType;
    }

    public String getReverseShipmentId() {
        return reverseShipmentId;
    }

    public void setReverseShipmentId(String reverseShipmentId) {
        this.reverseShipmentId = reverseShipmentId;
    }

    public String getBagStatus() {
        return bagStatus;
    }

    public void setBagStatus(String bagStatus) {
        this.bagStatus = bagStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(String latestStatus) {
        this.latestStatus = latestStatus;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getLatestUpdateDateTime() {
        return latestUpdateDateTime;
    }

    public void setLatestUpdateDateTime(String latestUpdateDateTime) {
        this.latestUpdateDateTime = latestUpdateDateTime;
    }

    public String getFirstReceivedHub() {
        return firstReceivedHub;
    }

    public void setFirstReceivedHub(String firstReceivedHub) {
        this.firstReceivedHub = firstReceivedHub;
    }

    public String getFirstReceiveDateTime() {
        return firstReceiveDateTime;
    }

    public void setFirstReceiveDateTime(String firstReceiveDateTime) {
        this.firstReceiveDateTime = firstReceiveDateTime;
    }

    public String getHubNotes() {
        return hubNotes;
    }

    public void setHubNotes(String hubNotes) {
        this.hubNotes = hubNotes;
    }

    public String getCsNotes() {
        return csNotes;
    }

    public void setCsNotes(String csNotes) {
        this.csNotes = csNotes;
    }

    public String getNumberofAttempts() {
        return numberofAttempts;
    }

    public void setNumberofAttempts(String numberofAttempts) {
        this.numberofAttempts = numberofAttempts;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getCustomerPromiseDate() {
        return customerPromiseDate;
    }

    public void setCustomerPromiseDate(String customerPromiseDate) {
        this.customerPromiseDate = customerPromiseDate;
    }

    public String getLogisticsPromiseDate() {
        return logisticsPromiseDate;
    }

    public void setLogisticsPromiseDate(String logisticsPromiseDate) {
        this.logisticsPromiseDate = logisticsPromiseDate;
    }

    public String getOnHoldByOpsReason() {
        return onHoldByOpsReason;
    }

    public void setOnHoldByOpsReason(String onHoldByOpsReason) {
        this.onHoldByOpsReason = onHoldByOpsReason;
    }

    public String getFirstAssignedHubName() {
        return firstAssignedHubName;
    }

    public void setFirstAssignedHubName(String firstAssignedHubName) {
        this.firstAssignedHubName = firstAssignedHubName;
    }

    public String getDeliveryPinCode() {
        return deliveryPinCode;
    }

    public void setDeliveryPinCode(String deliveryPinCode) {
        this.deliveryPinCode = deliveryPinCode;
    }

    public String getLastReceivedDateTime() {
        return lastReceivedDateTime;
    }

    public void setLastReceivedDateTime(String lastReceivedDateTime) {
        this.lastReceivedDateTime = lastReceivedDateTime;
    }

    public String getPickupaddress() {
        return pickupaddress;
    }

    public void setPickupaddress(String pickupaddress) {
        this.pickupaddress = pickupaddress;
    }

    public String getItemNos() {
        return itemNos;
    }

    public void setItemNos(String itemNos) {
        this.itemNos = itemNos;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getLrId() {
        return lrId;
    }

    public void setLrId(String lrId) {
        this.lrId = lrId;
    }

    public String getLrNo() {
        return lrNo;
    }

    public void setLrNo(String lrNo) {
        this.lrNo = lrNo;
    }

    public String getEwayBillNo() {
        return ewayBillNo;
    }

    public void setEwayBillNo(String ewayBillNo) {
        this.ewayBillNo = ewayBillNo;
    }

    public String getEwayExpiry() {
        return ewayExpiry;
    }

    public void setEwayExpiry(String ewayExpiry) {
        this.ewayExpiry = ewayExpiry;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getBillDoc() {
        return billDoc;
    }

    public void setBillDoc(String billDoc) {
        this.billDoc = billDoc;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getMatlGroup() {
        return matlGroup;
    }

    public void setMatlGroup(String matlGroup) {
        this.matlGroup = matlGroup;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterialDescription() {
        return materialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    public String getBillQty() {
        return billQty;
    }

    public void setBillQty(String billQty) {
        this.billQty = billQty;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getGstinNumber() {
        return gstinNumber;
    }

    public void setGstinNumber(String gstinNumber) {
        this.gstinNumber = gstinNumber;
    }

    public String getGrossValue() {
        return grossValue;
    }

    public void setGrossValue(String grossValue) {
        this.grossValue = grossValue;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRunSheetId() {
        return runSheetId;
    }

    public void setRunSheetId(String runSheetId) {
        this.runSheetId = runSheetId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getCodAmount() {
        return codAmount;
    }

    public void setCodAmount(String codAmount) {
        this.codAmount = codAmount;
    }

    public String getAssignedUser() {
        return assignedUser;
    }

    public void setAssignedUser(String assignedUser) {
        this.assignedUser = assignedUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public String getAsnid() {
        return asnid;
    }

    public void setAsnid(String asnid) {
        this.asnid = asnid;
    }

    public String getLoanid() {
        return loanid;
    }

    public void setLoanid(String loanid) {
        this.loanid = loanid;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getPoDate() {
        return poDate;
    }

    public void setPoDate(String poDate) {
        this.poDate = poDate;
    }

    public String getPoRequestId() {
        return PoRequestId;
    }

    public void setPoRequestId(String PoRequestId) {
        this.PoRequestId = PoRequestId;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getWarehouseToId() {
        return warehouseToId;
    }

    public void setWarehouseToId(String warehouseToId) {
        this.warehouseToId = warehouseToId;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getCancelled1() {
        return cancelled1;
    }

    public void setCancelled1(String cancelled1) {
        this.cancelled1 = cancelled1;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getTripQty() {
        return tripQty;
    }

    public void setTripQty(String tripQty) {
        this.tripQty = tripQty;
    }

    public String getOfd() {
        return ofd;
    }

    public void setOfd(String ofd) {
        this.ofd = ofd;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGrnId1() {
        return grnId1;
    }

    public void setGrnId1(String grnId1) {
        this.grnId1 = grnId1;
    }

    public String getSerials() {
        return serials;
    }

    public void setSerials(String serials) {
        this.serials = serials;
    }

    public String getTempIds() {
        return tempIds;
    }

    public void setTempIds(String tempIds) {
        this.tempIds = tempIds;
    }

    public String getUoms() {
        return uoms;
    }

    public void setUoms(String uoms) {
        this.uoms = uoms;
    }

    public String getBins() {
        return bins;
    }

    public void setBins(String bins) {
        this.bins = bins;
    }

    public String getProCons() {
        return proCons;
    }

    public void setProCons(String proCons) {
        this.proCons = proCons;
    }

    public String getPrevPoQuantity() {
        return prevPoQuantity;
    }

    public void setPrevPoQuantity(String prevPOQuantity) {
        this.prevPoQuantity = prevPOQuantity;
    }

    public Double getAsnQty() {
        return asnQty;
    }

    public void setAsnQty(Double asnQty) {
        this.asnQty = asnQty;
    }

    public String getSerialNoTemp1() {
        return serialNoTemp1;
    }

    public void setSerialNoTemp1(String serialNoTemp1) {
        this.serialNoTemp1 = serialNoTemp1;
    }

    public String getBinIdTemp1() {
        return binIdTemp1;
    }

    public void setBinIdTemp1(String binIdTemp1) {
        this.binIdTemp1 = binIdTemp1;
    }

    public String getProConTemp1() {
        return proConTemp1;
    }

    public void setProConTemp1(String proConTemp1) {
        this.proConTemp1 = proConTemp1;
    }

    public String getSerialNoTemp() {
        return serialNoTemp;
    }

    public void setSerialNoTemp(String serialNoTemp) {
        this.serialNoTemp = serialNoTemp;
    }

    public String getBinIdTemp() {
        return binIdTemp;
    }

    public void setBinIdTemp(String binIdTemp) {
        this.binIdTemp = binIdTemp;
    }

    public String getProConTemp() {
        return proConTemp;
    }

    public void setProConTemp(String proConTemp) {
        this.proConTemp = proConTemp;
    }

    public String[] getBinIds() {
        return binIds;
    }

    public void setBinIds(String[] binIds) {
        this.binIds = binIds;
    }

    public String getBinFlag() {
        return binFlag;
    }

    public void setBinFlag(String binFlag) {
        this.binFlag = binFlag;
    }

    public String getExcess() {
        return excess;
    }

    public void setExcess(String excess) {
        this.excess = excess;
    }

    public String getDamaged() {
        return damaged;
    }

    public void setDamaged(String damaged) {
        this.damaged = damaged;
    }

    public String getShortage() {
        return shortage;
    }

    public void setShortage(String shortage) {
        this.shortage = shortage;
    }

    public String getEmpUserId() {
        return empUserId;
    }

    public void setEmpUserId(String empUserId) {
        this.empUserId = empUserId;
    }

    public String getGtnDetId() {
        return gtnDetId;
    }

    public void setGtnDetId(String gtnDetId) {
        this.gtnDetId = gtnDetId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPoQuantity() {
        return poQuantity;
    }

    public void setPoQuantity(String poQuantity) {
        this.poQuantity = poQuantity;
    }

    public String getGtnNo() {
        return gtnNo;
    }

    public void setGtnNo(String gtnNo) {
        this.gtnNo = gtnNo;
    }

    public String getGtnId() {
        return gtnId;
    }

    public void setGtnId(String gtnId) {
        this.gtnId = gtnId;
    }

    public String[] getPoNos() {
        return poNos;
    }

    public void setPoNos(String[] poNos) {
        this.poNos = poNos;
    }

    public String[] getPoQty() {
        return poQty;
    }

    public void setPoQty(String[] poQty) {
        this.poQty = poQty;
    }

    public String getGtnTime() {
        return gtnTime;
    }

    public void setGtnTime(String gtnTime) {
        this.gtnTime = gtnTime;
    }

    public String getDock() {
        return dock;
    }

    public void setDock(String dock) {
        this.dock = dock;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCustId() {
        return CustId;
    }

    public void setCustId(String CustId) {
        this.CustId = CustId;
    }

    public String getPoName() {
        return poName;
    }

    public void setPoName(String poName) {
        this.poName = poName;
    }

    public String getGtnDate() {
        return gtnDate;
    }

    public void setGtnDate(String gtnDate) {
        this.gtnDate = gtnDate;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getUom1() {
        return uom1;
    }

    public void setUom1(String uom1) {
        this.uom1 = uom1;
    }

    public String getInpQty() {
        return inpQty;
    }

    public void setInpQty(String inpQty) {
        this.inpQty = inpQty;
    }

    public String[] getIpQty() {
        return ipQty;
    }

    public void setIpQty(String[] ipQty) {
        this.ipQty = ipQty;
    }

    public String[] getUom() {
        return uom;
    }

    public void setUom(String[] uom) {
        this.uom = uom;
    }

    public String[] getProCond() {
        return proCond;
    }

    public void setProCond(String[] proCond) {
        this.proCond = proCond;
    }

    public String getProCon() {
        return proCon;
    }

    public void setProCon(String proCon) {
        this.proCon = proCon;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String CustomerName) {
        this.CustomerName = CustomerName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

    public String getAsnNo() {
        return asnNo;
    }

    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    public String getAsnDate() {
        return asnDate;
    }

    public void setAsnDate(String asnDate) {
        this.asnDate = asnDate;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAsnPath() {
        return asnPath;
    }

    public void setAsnPath(String asnPath) {
        this.asnPath = asnPath;
    }

    public String getAsnRemarks() {
        return asnRemarks;
    }

    public void setAsnRemarks(String asnRemarks) {
        this.asnRemarks = asnRemarks;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getActualQuantity() {
        return actualQuantity;
    }

    public void setActualQuantity(String actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    public String getAsnDetId() {
        return asnDetId;
    }

    public void setAsnDetId(String asnDetId) {
        this.asnDetId = asnDetId;
    }

    public String getDelRequestId() {
        return delRequestId;
    }

    public void setDelRequestId(String delRequestId) {
        this.delRequestId = delRequestId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getReMarks() {
        return reMarks;
    }

    public void setReMarks(String reMarks) {
        this.reMarks = reMarks;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getLoanProposalId() {
        return loanProposalId;
    }

    public void setLoanProposalId(String loanProposalId) {
        this.loanProposalId = loanProposalId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public double getUnusedQty() {
        return unusedQty;
    }

    public void setUnusedQty(double unusedQty) {
        this.unusedQty = unusedQty;
    }

    public double getActQty() {
        return actQty;
    }

    public void setActQty(double actQty) {
        this.actQty = actQty;
    }

    public double getReqQty() {
        return reqQty;
    }

    public void setReqQty(double reqQty) {
        this.reqQty = reqQty;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public String getWhId() {
        return whId;
    }

    public void setWhId(String whId) {
        this.whId = whId;
    }

    public String getEwaybillNo() {
        return ewaybillNo;
    }

    public void setEwaybillNo(String ewaybillNo) {
        this.ewaybillNo = ewaybillNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getGrnId() {
        return grnId;
    }

    public void setGrnId(String grnId) {
        this.grnId = grnId;
    }

    public String getRequestQty() {
        return requestQty;
    }

    public void setRequestQty(String requestQty) {
        this.requestQty = requestQty;
    }

    public String[] getSerialNos() {
        return serialNos;
    }

    public void setSerialNos(String[] serialNos) {
        this.serialNos = serialNos;
    }

    public String[] getWhIds() {
        return whIds;
    }

    public void setWhIds(String[] whIds) {
        this.whIds = whIds;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getGrnNo() {
        return grnNo;
    }

    public void setGrnNo(String grnNo) {
        this.grnNo = grnNo;
    }

    public String getGrnDate() {
        return grnDate;
    }

    public void setGrnDate(String grnDate) {
        this.grnDate = grnDate;
    }

    public String getAsnReqId() {
        return asnReqId;
    }

    public void setAsnReqId(String asnReqId) {
        this.asnReqId = asnReqId;
    }

    public String getExcessQty() {
        return excessQty;
    }

    public void setExcessQty(String excessQty) {
        this.excessQty = excessQty;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

}
