package ets.domain.wms.data;

import ets.domain.util.ThrottleConstants;
import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.wms.business.WmsTO;
import ets.domain.wms.web.S3Helper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.TimeZone;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author hp
 */
public class WmsDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "WmsDAO";
    public int lim = 0;
    private String agreedFuelPrice;

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    //asnMasterList
    public ArrayList getviewDeliveryRequest(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnMasterList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            asnMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getviewDeliveryRequest", map);
            System.out.println("asnMasterList.size()-----" + asnMasterList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnMasterList;

    }

    public ArrayList getDeliveryRequestUpdate(WmsTO wmsTO) {
        Map map = new HashMap();
        map.put("orderId", wmsTO.getOrderId());
        System.out.println("map = " + map);
        ArrayList getDeliveryRequestUpdate = new ArrayList();
        try {
            getDeliveryRequestUpdate = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDeliveryRequestUpdate", map);
            System.out.println("getDeliveryRequestUpdate size=" + getDeliveryRequestUpdate.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDeliveryRequestUpdate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDeliveryRequestUpdate List", sqlException);
        }

        return getDeliveryRequestUpdate;
    }

    public int getDeliveryRequestFinalUpdate(WmsTO wmsTO, int userId, SqlMapClient session) {
        synchronized (this) {
            Map map = new HashMap();

            int status = 0;

            try {

                map.put("delRequestId", wmsTO.getDelRequestId());
                map.put("loanProposalId", wmsTO.getLoanProposalId());
                map.put("productId", wmsTO.getProductId());
                map.put("ItemId", wmsTO.getItemId());
                map.put("warehouseId", wmsTO.getWarehouseId());
                map.put("serialNumber", wmsTO.getSerialNumber());
                map.put("orderId", wmsTO.getOrderId());
                // map.put("qty", Integer.parseInt(wmsTO.getQty()) - 1);

                System.out.println(" getDeliveryRequestFinalUpdate : " + map);

                status = (Integer) session.update("wms.updateWmsStock", map);
                System.out.println(" updateWmsStock : " + status);

                status = (Integer) session.update("wms.updateSerialNo", map);
                System.out.println(" updateSerialNo : " + status);

                status = (Integer) session.update("wms.insertPostApi", map);
                System.out.println(" insertPostApi : " + status);

                status = (Integer) session.update("wms.updateDeliveryStatus", map);
                System.out.println(" updateDeliveryStatus : " + status);

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
                /*
                 * Log the exception and propagate to the calling class
                 */
                FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
                FPLogUtils.fpErrorLog("sqlException" + sqlException);
                throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
            }
            return status;
        }
    }

    public ArrayList getAsnMasterList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnMasterList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            asnMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getAsnMasterList", map);
            System.out.println("asnMasterList.size()-----" + asnMasterList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnMasterList;

    }

    //asnMasterList
    public ArrayList getAsnDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList asnDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            asnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getAsnDetails", map);
            System.out.println("asnDetails.size()-----" + asnDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return asnDetails;

    }

    public int saveSerialTempDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            if (wmsTo.getSerialNo() != null) {
                map.put("serialNo", wmsTo.getSerialNo());
                map.put("asnId", wmsTo.getAsnId());
                map.put("itemId", wmsTo.getItemId());
                map.put("qty", "1");
                map.put("userId", userId);
                map.put("proCon", wmsTo.getProCon());
                map.put("uom", wmsTo.getUom());
                status = (Integer) getSqlMapClientTemplate().update("wms.saveSerialTempDetails", map);
                System.out.println("saveSerialTempDetails-----" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveSerialTempDetails", sqlException);
        }
        return status;

    }

    public int saveGrnSerialDetails(WmsTO wmsTo, int userId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());
            map.put("empUserId", wmsTo.getEmpUserId());
            map.put("ipQty", wmsTo.getIpQty().length);
            map.put("gtnId", wmsTo.getGtnId());
            System.out.println("empUserId=========" + wmsTo.getEmpUserId());
            int GRNNo = 0;
            GRNNo = (Integer) session.queryForObject("wms.getGRNCode", map);
            System.out.println("grnNo-----" + GRNNo);

            String GRNNoSequence = "" + GRNNo;

            if (GRNNoSequence == null) {
                GRNNoSequence = "00001";
            } else if (GRNNoSequence.length() == 1) {
                GRNNoSequence = "0000" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 2) {
                GRNNoSequence = "000" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 3) {
                GRNNoSequence = "00" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 4) {
                GRNNoSequence = "0" + GRNNoSequence;
            } else if (GRNNoSequence.length() == 5) {
                GRNNoSequence = "" + GRNNoSequence;
            }

            String grnNo = "GRN/2021/" + GRNNoSequence;
            map.put("grnNo", grnNo);
            System.out.println("grnNo---" + grnNo);

            int grnId = (Integer) session.insert("wms.saveGrnMaster", map);
            map.put("grnId", grnId);
            System.out.println("grnId-----" + grnId);

            map.put("reqQty", wmsTo.getActQty());
            map.put("actQty", wmsTo.getActQty());
            map.put("unusedQty", wmsTo.getUnusedQty());

            status = (Integer) session.update("wms.saveGrnDetails", map);
            System.out.println("status-----" + status);

            String checkQty = (String) session.queryForObject("wms.getGRNReceivedQty", map);
            System.out.println("checkQty-----" + checkQty);

            if (checkQty != null) {
                String[] checkQtys = checkQty.split("~");

                double poQty = Double.parseDouble(checkQtys[0]);
                double grnQty = Double.parseDouble(checkQtys[1]);

                if (grnQty >= poQty) {
                    status = (Integer) session.update("wms.updateGrnStatusInAsnMaster", map);
                    System.out.println("saveGrnDetails-----" + status);
                }
            }

            status = (Integer) session.update("wms.updateGRNTempStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public ArrayList getSerialTempDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList serialTempDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("grnId", wmsTo.getGrnId());
            System.out.println("MAP----" + map);
            serialTempDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialTempDetails", map);
            System.out.println("serialTempDetails.size()-----" + serialTempDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return serialTempDetails;

    }

    public ArrayList getSerialTempDetailsUnsed(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList serialTempDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            serialTempDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialTempDetailsUnsed", map);
            System.out.println("serialTempDetails.size()-----" + serialTempDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return serialTempDetails;

    }

    public String serialNoAlreadyExists(String SerialNo) {
        Map map = new HashMap();
        String status = "";
        int serialNoAlreadyExistsGrn = 0;
        int serialNoAlreadyExistsTemp = 0;
        try {
            System.out.println("serialnoexistence" + SerialNo);
            map.put("serialNo", SerialNo);
            System.out.println("MAP----" + map);

            serialNoAlreadyExistsGrn = (Integer) getSqlMapClientTemplate().queryForObject("wms.serialNoAlreadyExistsGrn", map);
            System.out.println("serialNoAlreadyExistsGrn-----" + serialNoAlreadyExistsGrn);

            if (serialNoAlreadyExistsGrn == 0) {
                serialNoAlreadyExistsTemp = (Integer) getSqlMapClientTemplate().queryForObject("wms.serialNoAlreadyExistsTemp", map);
                System.out.println("serialNoAlreadyExistsTemp-----" + serialNoAlreadyExistsTemp);
                if (serialNoAlreadyExistsTemp > 0) {
                    status = "2";
                } else {
                    status = "3";
                }
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String checkRouteCode(String routeCode) {
        Map map = new HashMap();
        String status = "";
        int routeCodeExist = 0;
        try {
            System.out.println("routeCodeExistence" + routeCode);
            map.put("routeCode", routeCode);
            System.out.println("MAP----" + map);

            routeCodeExist = (Integer) getSqlMapClientTemplate().queryForObject("wms.routeCodeExist", map);

            if (routeCodeExist > 0) {
                status = "1";
            } else {
                status = "2";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public String InvoiceNoAlreadyExists(String invoiceNo) {
        Map map = new HashMap();
        String status = "";
        int InvoiceNoAlreadyExists = 0;
        try {
            System.out.println("invoiceNoExistence" + invoiceNo);
            map.put("invoiceNo", invoiceNo);
            System.out.println("MAP----" + map);

            InvoiceNoAlreadyExists = (Integer) getSqlMapClientTemplate().queryForObject("wms.invoiceNoAlreadyExists", map);

            if (InvoiceNoAlreadyExists == 0) {
                status = "2";
            } else {
                status = "1";
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "serialTempDetails", sqlException);
        }
        return status;

    }

    public ArrayList getDockInList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDockInList = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            getDockInList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDockInList", map);
            System.out.println("getDockInList.size()-----" + getDockInList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDockInList;

    }

    //asnMasterList
    public ArrayList getDockInDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getDockInDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("grnId", wmsTo.getGrnId());
            map.put("gtnId", wmsTo.getGtnId());
            getDockInDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getDockInDetails", map);
            System.out.println("getDockInDetails.size()-----" + getDockInDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getDockInDetails", sqlException);
        }
        return getDockInDetails;

    }

    public int saveGrnSerialStockDetails(WmsTO wmsTo, String path, int userId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        int stockId = 0;
        int update = 0;
        FileInputStream fis = null;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());

            map.put("grnId", wmsTo.getGrnId());
            map.put("whId", wmsTo.getWhId());
            map.put("inpQty", wmsTo.getInpQty());
            map.put("fileName", path);
            map.put("userId", userId);
            System.out.println("asnid grnid itemid" + wmsTo.getAsnId() + "  " + wmsTo.getItemId() + "  " + wmsTo.getGrnId());
            int excessQty = (Integer) session.queryForObject("wms.getExcessQty", map);
            int damagedQty = (Integer) session.queryForObject("wms.getDamagedQty", map);
            System.out.println("excessQty======" + excessQty);
            map.put("excessQty", excessQty);
            map.put("damagedQty", damagedQty);
            String stockIds = (String) session.queryForObject("wms.getStockId", map);

            if (stockIds == null) {
                System.out.println("stock map......" + map);
                stockId = (Integer) session.insert("wms.insertStockDetails", map);
                System.out.println("insertStockDetails-----" + status);
            } else {
                System.out.println("stock map......" + map);
                stockId = (Integer) session.update("wms.updateStockDetails", map);
                System.out.println("updateStockDetails-----" + status);
                stockId = Integer.parseInt(stockIds);
            }

            map.put("stockId", stockId);
            ArrayList serialGood = new ArrayList();
            serialGood = (ArrayList) session.queryForList("wms.getSerialGood", map);
            if (serialGood.size() > 0) {
                Iterator itr = serialGood.iterator();
                while (itr.hasNext()) {
                    wmsTo = (WmsTO) itr.next();
                    map.put("serialNo", wmsTo.getSerialNoTemp());
                    map.put("binId", wmsTo.getBinIdTemp());
                    update = (Integer) session.update("wms.saveGoodGrnSerialDetails", map);
                    System.out.println("saveGrnSerialDetails-----" + update);
                }
            }
            ArrayList serialOthers = new ArrayList();
            serialOthers = (ArrayList) session.queryForList("wms.getSerialOthers", map);
            if (serialOthers.size() > 0) {
                Iterator itr = serialOthers.iterator();
                while (itr.hasNext()) {
                    wmsTo = (WmsTO) itr.next();
                    map.put("serialNo", wmsTo.getSerialNoTemp1());
                    map.put("binId", wmsTo.getBinIdTemp1());
                    map.put("proConTemp", wmsTo.getProConTemp1());
                    update = (Integer) session.update("wms.saveBadGrnSerialDetails", map);
                    System.out.println("saveBadSerialDetails-----" + update);
                }
            }
            status = (Integer) session.update("wms.updateDockInStatus", map);
            System.out.println("updateDockInStatus-----" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public String grnImageFileUpload(WmsTO wmsTo, String actualFilePath, String fileSaved, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        String path = "";
        FileInputStream fis = null;

        try {

            map.put("invoiceNo", wmsTo.getInvoiceNo());
            map.put("invoiceDate", wmsTo.getInvoiceDate());
            map.put("eWaybillNo", wmsTo.getEwaybillNo());
            map.put("grnId", wmsTo.getGrnId());
            map.put("fileName", fileSaved);

            System.out.println("actualFilePath = " + actualFilePath);
            if (actualFilePath != null) {
//                File file = new File(actualFilePath);
//                System.out.println("file = " + file);
//                fis = new FileInputStream(file);
//                System.out.println("fis = " + fis);
//                byte[] podFile = new byte[(int) file.length()];
//                System.out.println("podFile = " + podFile);
//                fis.read(podFile);
//                fis.close();
//                map.put("podFile", podFile);

                try {

//                    //final String URLLink = "http://localhost:8080/HSServiceAPI/api/v1/bfil/updateGRNImage";
//                    final String URLLink = "http://52.66.244.61//HSServiceAPI/api/v1/bfil/updateGRNImage";
//
//                    String payload = "[{\"folder\":\"GRN\", \"filename\":\"" + fileSaved + "\" ,\"grnid\":\"0\"}]";
//                    System.out.println("payload " + payload);
//
//                    URL myurl = new URL(URLLink);
//                    HttpURLConnection connection = null;
//                    StringBuffer jsonString = new StringBuffer();
//                    String line;
//
//                    connection = (HttpURLConnection) myurl.openConnection();
//                    connection.setDoOutput(true);
//                    connection.setRequestMethod("PUT");
//                    connection.setRequestProperty("Accept", "application/json");
//                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
//                    writer.write(payload);
//                    writer.close();
//
//                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    while ((line = br.readLine()) != null) {
//                        jsonString.append(line);
//                    }
//                    br.close();
//                    connection.disconnect();
//                    System.out.println("jsonString :" + jsonString);
//
//                    path = jsonString.toString();
                    map.put("actualFilePath", fileSaved);
                    System.out.println("path========" + fileSaved);

                    System.out.println("map updateGrnMaster----- -----" + map);
                    status = (Integer) session.update("wms.updateGrnMaster", map);
                    System.out.println("updateGrnMaster-----" + status);

                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return path;

    }

    public String grnEwayImageFileUpload(WmsTO wmsTo, String actualFilePath, String fileSaved, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int status = 0;
        String path = "";
        FileInputStream fis = null;

        try {

            map.put("grnId", wmsTo.getGrnId());
            map.put("fileName", fileSaved);
            System.out.println("actualFilePath = " + actualFilePath);
            if (actualFilePath != null) {
//                File file = new File(actualFilePath);
//                System.out.println("file = " + file);
//                fis = new FileInputStream(file);
//                System.out.println("fis = " + fis);
//                byte[] podFile = new byte[(int) file.length()];
//                System.out.println("podFile = " + podFile);
//                fis.read(podFile);
//                fis.close();
//                map.put("podFile", podFile);

                try {

//                    //final String URLLink = "http://localhost:8080/HSServiceAPI/api/v1/bfil/updateGRNImage";
//                    final String URLLink = "http://52.66.244.61//HSServiceAPI/api/v1/bfil/updateGRNImage";
//
//                    String payload = "[{\"folder\":\"GRN\", \"filename\":\"" + fileSaved + "\" ,\"grnid\":\"0\"}]";
//                    System.out.println("payload " + payload);
//
//                    URL myurl = new URL(URLLink);
//                    HttpURLConnection connection = null;
//                    StringBuffer jsonString = new StringBuffer();
//                    String line;
//
//                    connection = (HttpURLConnection) myurl.openConnection();
//                    connection.setDoOutput(true);
//                    connection.setRequestMethod("PUT");
//                    connection.setRequestProperty("Accept", "application/json");
//                    connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
//                    writer.write(payload);
//                    writer.close();
//
//                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                    while ((line = br.readLine()) != null) {
//                        jsonString.append(line);
//                    }
//                    br.close();
//                    connection.disconnect();
//                    System.out.println("jsonString :" + jsonString);
//
//                    path = jsonString.toString();
                    map.put("actualFilePath", fileSaved);
                    System.out.println("path========" + fileSaved);

                    System.out.println("map updateGrnEwayPath----- -----" + map);
                    status = (Integer) session.update("wms.updateGrnEwayPath", map);
                    System.out.println("updateGrnEwayPath-----" + status);

                } catch (Exception e) {
                    // e.printStackTrace();
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return path;

    }

    public ArrayList getBinList(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBinList = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            getBinList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinList", map);
            System.out.println("getBinList.size()-----" + getBinList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getBinList;

    }

    public String getBarcodeMaster() {
        Map map = new HashMap();
        int BarcodeMaster = 0;
        String getBarcodeMaster = "";
        try {
            BarcodeMaster = (Integer) getSqlMapClientTemplate().queryForObject("wms.getBarcodeMaster", map);
            String barSequence = "" + BarcodeMaster;

            if (barSequence == null) {
                barSequence = "00001";
            } else if (barSequence.length() == 1) {
                barSequence = "0000" + barSequence;
            } else if (barSequence.length() == 2) {
                barSequence = "000" + barSequence;
            } else if (barSequence.length() == 3) {
                barSequence = "00" + barSequence;
            } else if (barSequence.length() == 4) {
                barSequence = "0" + barSequence;
            } else if (barSequence.length() == 5) {
                barSequence = "" + barSequence;
            }
            getBarcodeMaster = barSequence;
            System.out.println("getBarcodeMaster-----" + getBarcodeMaster);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getBarcodeMaster;

    }

    public ArrayList getBarcodeDetails(String itemId, WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBarcodeDetails = new ArrayList();
        try {
            map.put("custId",wmsTo.getCustId());
            map.put("itemId",itemId);
            System.out.println("map   ++++"+map);
            getBarcodeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBarcodeDetails", map);
            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getBarcodeDetails;

    }

    public int generateBarcode(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            String barcode = "";
            int barSequence = Integer.parseInt(wmsTo.getBarcode());

            System.out.println("BarcodeNo===" + barSequence);
            map.put("itemId", wmsTo.getItemId());
            map.put("custId", wmsTo.getCustId());
            map.put("userId", wmsTo.getUserId());
            int i = 0;
            int Batch = 0;
            Batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getBarcodeBatch", map);
            System.out.println("batch========" + Batch);
            System.out.println("barCode========" + wmsTo.getBarcode());
            System.out.println("count========" + wmsTo.getCount());
            map.put("batch", Batch + 1);
            for (i = 1; i <= wmsTo.getCount(); i++) {
                if (barSequence == 0) {
                    barcode = "BAR/2021/" + "00001";
                } else if (String.valueOf(barSequence).length() == 1) {
                    barcode = "BAR/2021/" + "0000" + barSequence;
                } else if (String.valueOf(barSequence).length() == 2) {
                    barcode = "BAR/2021/" + "000" + barSequence;
                } else if (String.valueOf(barSequence).length() == 3) {
                    barcode = "BAR/2021/" + "00" + barSequence;
                } else if (String.valueOf(barSequence).length() == 4) {
                    barcode = "BAR/2021/" + "0" + barSequence;
                } else if (String.valueOf(barSequence).length() == 5) {
                    barcode = "BAR/2021/" + "" + barSequence;
                }
                map.put("count", i);
                map.put("barcode", barcode);
                System.out.println("barCodeSeq===" + barcode);
                barSequence++;
                status = (Integer) getSqlMapClientTemplate().update("wms.generateBarcode", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveSerialTempDetails", sqlException);
        }
        return status;

    }

    public ArrayList grnViewDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList grnViewDetails = new ArrayList();
        try {
            map.put("userId", wmsTo.getUserId());
            map.put("fromDate", wmsTo.getFromDate());
            map.put("toDate", wmsTo.getToDate());
            map.put("whId", wmsTo.getWhId());
//            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            grnViewDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.grnViewDetails", map);
            System.out.println("grnViewDetails.size()-----" + grnViewDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("grnViewDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "grnViewDetails", sqlException);
        }
        return grnViewDetails;

    }

    public ArrayList viewSerialNumberDetails(String grnId) {
        Map map = new HashMap();
        ArrayList viewSerialNumberDetails = new ArrayList();
        try {
            map.put("grnId", grnId);
//            map.put("itemId", wmsTo.getItemId());
            System.out.println("MAP----" + map);
            viewSerialNumberDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.viewSerialNumberDetails", map);
            System.out.println("grnViewDetails.size()-----" + viewSerialNumberDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewSerialNumberDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "viewSerialNumberDetails", sqlException);
        }
        return viewSerialNumberDetails;

    }

    public ArrayList getOrderDetailsList(WmsTO wmsTo) {
        Map map = new HashMap();
        map.put("fromDate", wmsTo.getFromDate());
        map.put("toDate", wmsTo.getToDate());
        map.put("whId", wmsTo.getWhId());
        System.out.println("map = " + map);
        ArrayList ordersList = new ArrayList();
        try {
            if (wmsTo.getWhId() != null) {
                ordersList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getOrderDetailsList", map);
            }
            System.out.println("getOrderDetailsList size=" + ordersList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderDetailsList List", sqlException);
        }

        return ordersList;
    }

    public ArrayList getWareHouseList() {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList getWareHouseList = new ArrayList();
        try {
            getWareHouseList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareHouseList", map);
            System.out.println("getWareHouseList size=" + getWareHouseList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getWareHouseList;
    }

    public int saveTempGrnDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("empId", wmsTo.getEmpId());
            map.put("gtnDetId", wmsTo.getGtnDetId());

            System.out.println("serial length......" + wmsTo.getSerialNos().length);

            System.out.println("uom......" + wmsTo.getIpQty().length);

            int flag = (Integer) getSqlMapClientTemplate().update("wms.grnFlagClose", map);
            for (int i = 0; i < wmsTo.getSerialNos().length; i++) {
                if (wmsTo.getSerialNos()[i] != null) {
                    int uom;
                    int procond;
                    if (wmsTo.getUom()[i].equals("Pack")) {
                        uom = 1;
                    } else {
                        uom = 2;
                    }
                    if (wmsTo.getProCond()[i].equals("Good")) {
                        procond = 1;
                    } else if (wmsTo.getProCond()[i].equals("Bad")) {
                        procond = 2;
                    } else {
                        procond = 3;
                    }
                    map.put("serialNo", wmsTo.getSerialNos()[i]);
                    map.put("binId", wmsTo.getBinIds()[i]);
                    map.put("uom", uom);
                    map.put("proCond", procond);
                    status = (Integer) getSqlMapClientTemplate().update("wms.saveTempGrnDetails", map);
                    System.out.println("saveGrnSerialDetails-----" + status);

                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public int updateSerialTempDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("userId", userId);
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            System.out.println("itemidinupdateserial" + wmsTo.getItemId());
            map.put("gtnDetId", wmsTo.getGtnDetId());
            map.put("grnId", wmsTo.getGrnId());
            System.out.println("grnid in udpate serialtemp" + wmsTo.getGrnId());
            System.out.println("serial length......" + wmsTo.getSerialNos().length);

            System.out.println("uom......" + wmsTo.getIpQty().length);

            int flag = (Integer) getSqlMapClientTemplate().update("wms.grnFlagClose", map);
            for (int i = 0; i < wmsTo.getSerialNos().length; i++) {
                if (wmsTo.getSerialNos()[i] != null) {
                    int uom;
                    int procond;
                    if (wmsTo.getUom()[i].equals("Pack")) {
                        uom = 1;
                    } else {
                        uom = 2;
                    }
                    if (wmsTo.getProCond()[i].equals("Good")) {
                        procond = 1;
                    } else if (wmsTo.getProCond()[i].equals("Damaged")) {
                        procond = 2;
                    } else {
                        procond = 3;
                    }
                    map.put("serialNo", wmsTo.getSerialNos()[i]);
                    map.put("binId", wmsTo.getBinIds()[i]);
                    map.put("uom", uom);
                    map.put("proCond", procond);
                    status = (Integer) getSqlMapClientTemplate().update("wms.updateSerialTempDetails", map);
                    System.out.println("updateSerialTempDetails-----" + status);

                }

            }
            map.put("qty", wmsTo.getSerialNos().length);
            int updateGrnMasterQty = (Integer) getSqlMapClientTemplate().update("wms.updateGrnMasterQty", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public int delGrnTempSerial(String tempId, String grnId) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("tempId", tempId);
            map.put("grnId", grnId);
            System.out.println("tempId====" + tempId);
            status = (Integer) getSqlMapClientTemplate().update("wms.delGrnTempSerial", map);
            int reduceGrnQty = (Integer) getSqlMapClientTemplate().update("wms.reduceGrnQty", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "saveGrnSerialDetails", sqlException);
        }
        return status;

    }

    public ArrayList getPoCode(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        ArrayList productList = new ArrayList();
        try {
            map.put("userId", userId);
            productList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getPoCode", map);
            System.out.println(" getVehicleList =" + productList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProductList", sqlException);
        }

        return productList;
    }

    public ArrayList getCustList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList custList = new ArrayList();
        try {
            custList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getCustList", map);
            System.out.println(" getCustList =" + custList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return custList;
    }

    public ArrayList getItemList(int userId, String custId) {
        Map map = new HashMap();
        ArrayList getItemList = new ArrayList();
        try {
            map.put("userId", userId);
            map.put("custId", custId);
            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemList", map);
            System.out.println(" getItemList =" + getItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemList;
    }

    public int saveGtnDetails(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        map.put("userId", userId);
        String poNo = "";
        SqlMapClient session = getSqlMapClient();
        int status = 0;
        int updateStatus = 0;
        try {
            int GTNNo = 0;
            GTNNo = (Integer) session.queryForObject("wms.getGTNCode", map);

            System.out.println("gtnNo-----" + GTNNo);

            String GTNNoSequence = "" + GTNNo;

            if (GTNNoSequence == null) {
                GTNNoSequence = "00001";
            } else if (GTNNoSequence.length() == 1) {
                GTNNoSequence = "0000" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 2) {
                GTNNoSequence = "000" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 3) {
                GTNNoSequence = "00" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 4) {
                GTNNoSequence = "0" + GTNNoSequence;
            } else if (GTNNoSequence.length() == 5) {
                GTNNoSequence = "" + GTNNoSequence;
            }

            String gtnNo = "GTN/2021/" + GTNNoSequence;
            map.put("gtnNo", gtnNo);
            System.out.println("gtnNo---" + gtnNo);
            map.put("invoiceNo", wmsTO.getInvoiceNo());
            map.put("invoiceDate", wmsTO.getInvoiceDate());
            map.put("gtnDate", wmsTO.getGtnDate());
            map.put("gtnTime", wmsTO.getGtnTime());
            map.put("custId", wmsTO.getCustId());
            map.put("dock", wmsTO.getDock());
            map.put("vehicleType", wmsTO.getVehicleType());
            map.put("vehicleNo", wmsTO.getVehicleNo());
            map.put("driverName", wmsTO.getDriverName());
            map.put("remarks", wmsTO.getRemarks());
            int gtnId = (Integer) session.insert("wms.saveGtnMaster", map);
            map.put("gtnId", gtnId);
            System.out.println("gtnId-----" + gtnId);
            System.out.println("length of wms getPoNos" + wmsTO.getPoNos().length);
            System.out.println("length of wms getPoQty" + wmsTO.getPoQty().length);
            for (int i = 0; i < wmsTO.getPoNos().length; i++) {
                if (i == 0) {
                    map.put("invoiceNo1", wmsTO.getInvoiceNo());
                } else {
                    map.put("invoiceNo1", wmsTO.getInvoiceNo() + "-" + i);
                }
                poNo = wmsTO.getPoNos()[i];
                if (poNo != null) {
                    String[] poNo1 = poNo.split("-");
                    System.out.println("asnId in DAO" + poNo1[0]);
                    String asnId = poNo1[0];
                    String itemId = poNo1[1];
                    map.put("asnId", asnId);
                    map.put("itemId", itemId);
                    map.put("poQty", wmsTO.getPoQty()[i]);
                    status = (Integer) getSqlMapClientTemplate().update("wms.saveGtnDetails", map);
                    int totalAsnQty = (Integer) session.queryForObject("wms.totalAsnQty", map);
                    int totalGtnQty = (Integer) session.queryForObject("wms.totalGtnQty", map);
                    System.out.println("totalasn and item qty  " + asnId + "   " + itemId + "   " + totalAsnQty + "   " + totalGtnQty);
                    if (totalAsnQty == totalGtnQty && totalAsnQty != 0) {
                        updateStatus = (Integer) getSqlMapClientTemplate().update("wms.updateAsnStatus", map);
                    }
                }
                System.out.println("status---" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGtnDetails", sqlException);
        }

        return status;
    }

    public ArrayList getReceivedGateIn(int userId) {
        Map map = new HashMap();
        ArrayList getReceivedGateIn = new ArrayList();
        try {
            map.put("userId", userId);
            System.out.println("userId==========" + userId);
            getReceivedGateIn = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getReceivedGateIn", map);
            System.out.println("getReceivedGateIn.size()-----" + getReceivedGateIn.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getReceivedGateIn;

    }

    public ArrayList getGtnDetails(WmsTO wmsTo, int userId) {
        Map map = new HashMap();
        ArrayList gtnDetails = new ArrayList();
        try {
            map.put("asnId", wmsTo.getAsnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("gtnId", wmsTo.getGtnId());
            map.put("userId", userId);
            System.out.println("asnId======----" + wmsTo.getAsnId());
            System.out.println("itemId======----" + wmsTo.getItemId());
            System.out.println("gtnId======----" + wmsTo.getGtnId());
            gtnDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getGtnDetails", map);
            System.out.println("asnDetails.size()-----" + gtnDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return gtnDetails;

    }

    public ArrayList getUserDetails(int userId) {
        Map map = new HashMap();
        ArrayList getUserDetails = new ArrayList();
        try {
            map.put("userId", userId);
            getUserDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getUserDetails", map);
            System.out.println("asnDetails.size()-----" + getUserDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getUserDetails;

    }

    public ArrayList getBinDetails(WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getBinDetails = new ArrayList();
        try {
            map.put("itemId", wmsTo.getItemId());
            System.out.println("itemId=+++++" + wmsTo.getItemId());
            getBinDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getBinDetails", map);
            System.out.println("getBinDetails.size()-----" + getBinDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getBinDetails;

    }
//    public ArrayList getSerialDetails() {
//        Map map = new HashMap();
//        ArrayList getSerialDetails = new ArrayList();
//        try {
//            getSerialDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialDetails", map);
//            System.out.println("getBinDetails.size()-----" + getSerialDetails.size());
//
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
//        }
//        return getSerialDetails;
//
//    }

    public ArrayList getProList(int userId, WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList getProList = new ArrayList();
        try {
            map.put("gtnId", wmsTo.getGtnId());
            map.put("userId", userId);
            getProList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getProList", map);
            System.out.println("proListDetails.size()-----" + getProList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getProList;

    }

    public ArrayList proListDetails(int userId, WmsTO wmsTo) {
        Map map = new HashMap();
        ArrayList proListDetails = new ArrayList();
        try {
            map.put("gtnId", wmsTo.getGtnId());
            map.put("itemId", wmsTo.getItemId());
            map.put("asnId", wmsTo.getAsnId());
            map.put("userId", userId);
            proListDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.proListDetails", map);
            System.out.println("proListDetails.size()-----" + proListDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return proListDetails;

    }

    public ArrayList getSerialDet(String gtnId, String grnId, String itemId, String asnId) {
        Map map = new HashMap();
        ArrayList getSerialDet = new ArrayList();
        try {
            map.put("gtnId", gtnId);
            map.put("itemId", itemId);
            map.put("asnId", asnId);
            getSerialDet = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialDet", map);
            System.out.println("getBinDetails.size()-----" + getSerialDet.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getSerialDet;

    }

    public ArrayList getStateList() {
        Map map = new HashMap();
        ArrayList getStateList = new ArrayList();
        try {
            getStateList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getStateList", map);
            System.out.println("stateListSize" + getStateList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getStateList;

    }

    public ArrayList getWhList() {
        Map map = new HashMap();
        ArrayList getWhList = new ArrayList();
        try {
            getWhList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWhList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return getWhList;

    }

    public ArrayList warehouseOrderReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList warehouseOrderReport = new ArrayList();
        try {
            map.put("fromDate", wms.getFromDate());
            map.put("toDate", wms.getToDate());
            map.put("stateId", wms.getStateId());
            map.put("whId", wms.getWhId());
            System.out.println("map in warehouseOrderReport========" + map);
            warehouseOrderReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.warehouseOrderReport", map);
            System.out.println("warehouseOrderReport========" + warehouseOrderReport);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return warehouseOrderReport;

    }

    public int insertAsnDetails(WmsTO wmsTO) {

        Map map = new HashMap();

        int status1 = 0;
        int status2 = 0;
        int status = 0;

        try {
            map.put("remark", wmsTO.getRemark());

            map.put("PoRequestId", wmsTO.getPoRequestId());
            map.put("poNumber", wmsTO.getPoNumber());
            map.put("vendorId", wmsTO.getVendorId());
            map.put("poDate", wmsTO.getPoDate());
            map.put("warehouseId", wmsTO.getWarehouseId());
            map.put("status", "0");

            System.out.println("mapppppp==========" + map);
            // map.put("qty", Integer.parseInt(wmsTO.getQty()) - 1);

            status1 = (Integer) getSqlMapClientTemplate().insert("wms.updateAnsdetailsOne", map);

            map.put("Quantity", wmsTO.getQuantity());
            map.put("asnId", status1);
            map.put("price", "0.00");
            map.put("active", "Y");

            map.put("itemId", wmsTO.getItemId());

            System.out.println("mappp222=====" + map);
            status2 = (Integer) getSqlMapClientTemplate().update("wms.updateAnsdetailsTwo", map);

            status = status1 + status2;

            System.out.println(" updateDeliveryStatus : " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList getItemLists(int userId) {
        Map map = new HashMap();
        ArrayList getItemList = new ArrayList();
        try {
            map.put("userId", userId);

            getItemList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemLists", map);
            System.out.println(" getItemList =" + getItemList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemList;
    }

    public ArrayList processGetSerialNumber(String itemId, String userWhId) {
        Map map = new HashMap();
        ArrayList getSerialNumber = new ArrayList();
        try {

            map.put("itemId", itemId);
            map.put("userWhId", userWhId);
            map.put("status", "0");
            System.out.println("itemmm============DAO" + map);

            getSerialNumber = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialNumber", map);
            System.out.println(" getSerialNumber =" + getSerialNumber.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getSerialNumber;
    }

    public ArrayList getWareToHouseList(String userWhId) {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList getWareHouseList = new ArrayList();
        try {
            map.put("userWhId", userWhId);
            getWareHouseList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getWareToHouseList", map);
            System.out.println("getWareHouseList size=" + getWareHouseList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return getWareHouseList;
    }

    public int insertTransferQty(WmsTO wmsTO) {

        Map map = new HashMap();

        int status1 = 0;
        int status2 = 0;
        int status = 0;

        try {
            map.put("qty", wmsTO.getQuantity());
            map.put("WarehouseToId", wmsTO.getWarehouseToId());
            map.put("WarehouseId", wmsTO.getWarehouseId());
            map.put("ItemId", wmsTO.getItemId());
            status = (Integer) getSqlMapClientTemplate().insert("wms.transferQty", map);

            System.out.println("loop entered");

            for (int i = 0; i < wmsTO.getSelectedIndex().length; i++) {

                if ("1".equals(wmsTO.getSelectedIndex()[i])) {

                    map.put("serialId", wmsTO.getSerialNos()[i]);
                    map.put("stockDetails", status);
                    System.out.println("mapppp2222" + map);
                    status1 = (Integer) getSqlMapClientTemplate().update("wms.insertSerial", map);
                }
            }

            System.out.println(" updateDeliveryStatus : " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList ProcessVendorList() {

        Map map = new HashMap();
        ArrayList vendorList = new ArrayList();

        try {

            vendorList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getVendorList", map);
            System.out.println("vendorList======" + vendorList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return vendorList;
    }

    public ArrayList barCodeDetails(WmsTO wmsTO) {

        Map map = new HashMap();
        ArrayList barCodeDetails = new ArrayList();

        try {
            map.put("custId", wmsTO.getCustId());
            map.put("itemId", wmsTO.getItemId());
            barCodeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.barCodeDetails", map);
            System.out.println("vendorList======" + barCodeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "asnMasterList", sqlException);
        }
        return barCodeDetails;
    }

    public ArrayList processGetItemList(String whId) {
        Map map = new HashMap();
        ArrayList getItemLists = new ArrayList();
        try {

            map.put("whId", whId);
            System.out.println("itemmm============DAO" + map);

            getItemLists = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getItemListsDetails", map);
            System.out.println(" getItemList =" + getItemLists.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getZoneList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleList", sqlException);
        }

        return getItemLists;
    }

    public String getUserWarehouse(int userId) {
        Map map = new HashMap();
        String getUserWarehouse = "";
        try {
            map.put("userId", userId);
            getUserWarehouse = (String) getSqlMapClientTemplate().queryForObject("wms.getUserWarehouse", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getBinList", sqlException);
        }
        return getUserWarehouse;
    }

    public ArrayList getserialNumberDetails(WmsTO wmsTO) {
        Map map = new HashMap();
//        map.put("fromDate", wmsTo.getFromDate());
//        map.put("toDate", tripTO.getToDate());
//        System.out.println("map = " + map);
        ArrayList serialNumberDetails = new ArrayList();
        try {
            map.put("serialNumber", wmsTO.getSerialNo());
            serialNumberDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getSerialNumberDetails", map);
            System.out.println("getWareHouseList size=" + serialNumberDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWareHouseList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWareHouseList List", sqlException);
        }

        return serialNumberDetails;
    }

    public int shipmentUpload(WmsTO WmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        ArrayList getRunsheetUpload = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            map.put("userId", WmsTO.getUserId());
            System.out.println("user Id   " + WmsTO.getUserId());
            System.out.println("--1111--");
            getRunsheetUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRunsheetUpload", map);
            System.out.println("getContractUpload---" + getRunsheetUpload.size());
            Iterator itr = getRunsheetUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("runSheetId", WmsTO.getRunSheetId());
                map.put("trackingId", WmsTO.getTrackingId());
                map.put("mobileNo", WmsTO.getMobileNo());
                map.put("name", WmsTO.getName());
                map.put("address", WmsTO.getAddress());
                map.put("shipmentType", WmsTO.getShipmentType());
                map.put("codAmount", WmsTO.getCodAmount());
                map.put("assignedUser", WmsTO.getAssignedUser());
                map.put("status", WmsTO.getStatus());
                map.put("date", WmsTO.getDate());
                map.put("amountPaid", WmsTO.getAmountPaid());
                map.put("receiverName", WmsTO.getReceiverName());
                map.put("relation", WmsTO.getRelation());
                map.put("receiver", WmsTO.getReceiver());
                map.put("mobile", WmsTO.getMobile());
                map.put("signature", WmsTO.getSignature());
                map.put("vertical", WmsTO.getVertical());
                map.put("orderNo", WmsTO.getOrderNo());
                map.put("productDetails", WmsTO.getProductDetails());
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    int saveRunsheetUpload = (Integer) getSqlMapClientTemplate().update("wms.saveRunsheetUpload", map);
                    int updateShipmentMaster = (Integer) getSqlMapClientTemplate().update("wms.updateShipmentMaster", map);
                }

            }
            int clearPreviousData = (Integer) getSqlMapClientTemplate().update("wms.deleteRunsheetTemp", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int uploadRunsheet(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadRunsheet = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", wmsTO.getUserId());
        map.put("runSheetId", wmsTO.getRunSheetId());
        System.out.println("runSheetId=" + wmsTO.getRunSheetId());
        map.put("trackingId", wmsTO.getTrackingId());
        System.out.println("trackingId=" + wmsTO.getTrackingId());
        map.put("mobileNo", wmsTO.getMobileNo());
        System.out.println("mobileNo=" + wmsTO.getMobileNo());
        map.put("name", wmsTO.getName());
        System.out.println("name=" + wmsTO.getName());
        map.put("address", wmsTO.getAddress());
        System.out.println("address=" + wmsTO.getAddress());

        map.put("shipmentType", wmsTO.getShipmentType());
        System.out.println("shipmentType=" + wmsTO.getShipmentType());
        map.put("codAmount", wmsTO.getCodAmount());
        System.out.println("codAmount=" + wmsTO.getCodAmount());
        map.put("assignedUser", wmsTO.getAssignedUser());
        System.out.println("assignedUser=" + wmsTO.getAssignedUser());
        map.put("status", wmsTO.getStatus());
        System.out.println("status=" + wmsTO.getStatus());
        map.put("date", wmsTO.getDate());
        System.out.println("date=" + wmsTO.getDate());
        map.put("amountPaid", wmsTO.getAmountPaid());
        System.out.println("amountPaid=" + wmsTO.getAmountPaid());
        map.put("receiverName", wmsTO.getReceiverName());
        System.out.println("receiverName=" + wmsTO.getReceiverName());
        map.put("relation", wmsTO.getRelation());
        System.out.println("relation=" + wmsTO.getRelation());
        map.put("receiver", wmsTO.getReceiver());
        System.out.println("receiver=" + wmsTO.getReceiver());
        map.put("mobile", wmsTO.getMobile());
        System.out.println("mobile=" + wmsTO.getMobile());
        map.put("signature", wmsTO.getSignature());
        System.out.println("signature=" + wmsTO.getSignature());
        map.put("vertical", wmsTO.getVertical());
        System.out.println("vertical=" + wmsTO.getVertical());
        map.put("orderNo", wmsTO.getOrderNo());
        System.out.println("orderNo=" + wmsTO.getOrderNo());
        map.put("productDetails", wmsTO.getProductDetails());
        System.out.println("productDetails=" + wmsTO.getProductDetails());

        try {

            String trackingId = (String) getSqlMapClientTemplate().queryForObject("wms.checkTrackingId", map);
            System.out.println("cityId = " + trackingId);
            if (!"".equals(trackingId) && trackingId != null) {
                trackingId = trackingId;
            } else {
                trackingId = "0";
            }
            if (trackingId != "0") {
                map.put("error", "0");
            } else {
                map.put("error", "1");
            }

            System.out.println("map map map = " + map);
            uploadRunsheet = (Integer) getSqlMapClientTemplate().insert("wms.uploadRunsheetTemp", map);
            System.out.println("uploadShipment size=====" + uploadRunsheet);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadShipment Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadShipment", sqlException);
        }
        return uploadRunsheet;
    }

    public ArrayList getRunsheetUpload(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getContractList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            getContractList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getRunsheetUpload", map);
            System.out.println("getContractList========" + getContractList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getContractList;

    }

    public int ifbOrderUpload(WmsTO WmsTO, int userId, String whId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int lrNo = 0;
        int checkInvoice = 0;
        int pinWhCheck = 0;
        ArrayList getIfbOrderUpload = new ArrayList();
        WmsTO wmsTO = new WmsTO();
        try {
            System.out.println("--1111--");
            map.put("userId", userId);
            map.put("whId", whId);
            int ifbBatch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", ifbBatch);
            getIfbOrderUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderUpload", map);
            System.out.println("getIfbOrderUpload---" + getIfbOrderUpload.size());
            Iterator itr = getIfbOrderUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("billDoc", WmsTO.getBillDoc());
                map.put("billDate", WmsTO.getBillDate());
                map.put("purchaseOrderNumber", WmsTO.getPurchaseOrderNumber());
                map.put("matlGroup", WmsTO.getMatlGroup());
                map.put("material", WmsTO.getMaterial());
                map.put("materialDescription", WmsTO.getMaterialDescription());
                map.put("billQty", WmsTO.getBillQty());
                map.put("city", WmsTO.getCity());
                map.put("shipTo", WmsTO.getShipTo());
                map.put("partyName", WmsTO.getPartyName());
                map.put("gstinNumber", WmsTO.getGstinNumber());
                map.put("grossValue", WmsTO.getGrossValue());
                map.put("pincode", WmsTO.getPincode());
                map.put("flag", WmsTO.getFlag());
                if ("YES".equalsIgnoreCase(WmsTO.getExchange())) {
                    map.put("exchange", "2");
                } else {
                    map.put("exchange", "1");
                }
                System.out.println("map = " + map);
                checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbInvoice", map);
                pinWhCheck = (Integer) getSqlMapClientTemplate().queryForObject("wms.pinWhCheck", map);
                if ((whId).equals(pinWhCheck + "")) {
                    map.put("hubFlag", "0");
                    map.put("invoiceStatus", "0");
                } else {
                    map.put("hubFlag", "1");
                    map.put("invoiceStatus", "5");
                }
                if (checkInvoice == 0) {
                    int getIfbOrderUploads = (Integer) getSqlMapClientTemplate().update("wms.insertIFBOrder", map);
                }
            }
            ArrayList invoiceNos = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbInvoiceNos", map);
            Iterator itr1 = invoiceNos.iterator();
            while (itr1.hasNext()) {
                WmsTO = (WmsTO) itr1.next();
                map.put("invoiceNo", WmsTO.getInvoiceNo().split("~")[0]);
                Double gross = Double.parseDouble(WmsTO.getInvoiceNo().split("~")[1]);
                System.out.println("invoiceNo==" + WmsTO.getInvoiceNo().split("~")[0]);
                lrNo = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbLr", map);
                String LRNoSequence = "" + lrNo;

                if (LRNoSequence == null) {
                    LRNoSequence = "00001";
                } else if (LRNoSequence.length() == 1) {
                    LRNoSequence = "0000" + LRNoSequence;
                } else if (LRNoSequence.length() == 2) {
                    LRNoSequence = "000" + LRNoSequence;
                } else if (LRNoSequence.length() == 3) {
                    LRNoSequence = "00" + LRNoSequence;
                } else if (LRNoSequence.length() == 4) {
                    LRNoSequence = "0" + LRNoSequence;
                } else if (LRNoSequence.length() == 5) {
                    LRNoSequence = "" + LRNoSequence;
                }

                if (gross <= 50000) {
                    map.put("ewayBillNo", "0");
                    map.put("ewayBillExpiry", "0");
                    System.out.println("gross  ==" + gross);
                    int updateOrder = (Integer) getSqlMapClientTemplate().update("wms.updateIfbInvoiceStatus", map);
                } else {
                    map.put("ewayBillNo", "");
                    map.put("ewayBillExpiry", "");
                }
                String lr = "IFB/2021/" + LRNoSequence;
                map.put("lrNo", lr);
                System.out.println("lrNo---" + lr);

                int lrId = (Integer) getSqlMapClientTemplate().insert("wms.saveIfbLr", map);
                map.put("lrId", lrId);
                insertStatus = (Integer) getSqlMapClientTemplate().update("wms.updateLrId", map);
                System.out.println("lrId-----" + lrId);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int deleteIfbUploadTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteIfbUploadTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int saveIfbLr(String[] lrId, String[] ewayBillNo, String[] ewayExpiry, String[] selected) {
        Map map = new HashMap();
        int saveIfbLr = 0;
        try {
            for (int i = 0; i < selected.length; i++) {
                if ("1".equals(selected[i])) {
                    map.put("lrId", lrId[i]);
                    map.put("ewayBillNo", ewayBillNo[i]);
                    map.put("ewayExpiry", ewayExpiry[i]);
                    saveIfbLr = (Integer) getSqlMapClientTemplate().update("wms.saveIfbEway", map);
                    int updateIfbOrderStatus = (Integer) getSqlMapClientTemplate().update("wms.updateIfbOrderStatus", map);
                    System.out.println("saveIfbLr---" + saveIfbLr);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return saveIfbLr;
    }

    public int uploadIfbOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadIfbOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        int pincode = 0;
        map.put("whId", wmsTO.getWhId());
        map.put("billDoc", wmsTO.getBillDoc());
        map.put("billDate", wmsTO.getBillDate());
        map.put("purchaseOrderNumber", wmsTO.getPurchaseOrderNumber());
        map.put("matlGroup", wmsTO.getMatlGroup());
        map.put("material", wmsTO.getMaterial());
        map.put("materialDescription", wmsTO.getMaterialDescription());
        map.put("billQty", wmsTO.getBillQty());
        map.put("city", wmsTO.getCity());
        map.put("shipTo", wmsTO.getShipTo());
        map.put("partyName", wmsTO.getPartyName());
        map.put("gstinNumber", wmsTO.getGstinNumber());
        map.put("grossValue", wmsTO.getGrossValue());
        map.put("userId", wmsTO.getUserId());
        map.put("exchange", wmsTO.getExchange());
        System.out.println("map    " + map);
        try {
            int batch = (Integer) getSqlMapClientTemplate().queryForObject("wms.getIfbBatch", map);
            map.put("batch", batch);
            if ("".equals(wmsTO.getPincode())) {
                pincode = (Integer) getSqlMapClientTemplate().queryForObject("wms.getDealerPincode", map);
            } else {
                pincode = Integer.parseInt(wmsTO.getPincode());
            }
            map.put("pincode", pincode);
            int pincheck = (Integer) getSqlMapClientTemplate().queryForObject("secondaryOperation.pinExistsMaster", map);
            int checkInvoice = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkIfbInvoice", map);
            if (pincheck > 0) {
                if (checkInvoice == 0) {
                    map.put("status", "0");
                } else {
                    map.put("status", "2");
                }
            } else {
                map.put("status", "1");
            }
            System.out.println("map map map = " + map);
            uploadIfbOrder = (Integer) getSqlMapClientTemplate().insert("wms.insertIFBOrderTemp", map);
            System.out.println("uploadIfbOrder size=====" + uploadIfbOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadIfbOrder;
    }

    public ArrayList getIFBList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIFBList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getIFBList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderUpload", map);
            System.out.println("getIFBList========" + getIFBList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIFBList;

    }

    public ArrayList ifbLRList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbLRList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            ifbLRList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRList", map);
            System.out.println("ifbLRList========" + ifbLRList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbLRList;

    }

    public ArrayList getIfbRunsheetList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getIfbRunsheetList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getIfbRunsheetList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbRunsheetList", map);
            System.out.println("getIfbRunsheetList========" + getIfbRunsheetList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbRunsheetList;

    }

    public ArrayList getFkRunsheetList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getFkRunsheetList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getFkRunsheetList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getFkRunsheetList", map);
            System.out.println("getFkRunsheetList========" + getFkRunsheetList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getFkRunsheetList;

    }

    public ArrayList getwmsItemReport(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getwmsItemReport = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            getwmsItemReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getwmsItemReport", map);
            System.out.println("getwmsItemReport========" + getwmsItemReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getwmsItemReport;

    }

    public ArrayList getwmsItemReportView(WmsTO wms) {
        Map map = new HashMap();
        ArrayList getwmsItemReport = new ArrayList();
        try {
            map.put("itemId", wms.getItemId());
            System.out.println("map    " + map);
            getwmsItemReport = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getwmsItemReportView", map);
            System.out.println("getwmsItemReport========" + getwmsItemReport.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getwmsItemReport;

    }

    public int UpdateSku(WmsTO wmsTO) {

        Map map = new HashMap();

        int status1 = 0;
        int status2 = 0;
        int status = 0;

        try {
            map.put("itemId", wmsTO.getItemId());

            map.put("serialNumber", wmsTO.getSerialNumber());

            System.out.println("mapppppp==========" + map);
            // map.put("qty", Integer.parseInt(wmsTO.getQty()) - 1);

            status1 = (Integer) getSqlMapClientTemplate().update("wms.updateSku", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }
        return status;
    }

    public ArrayList hubOutList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList hubOutList = new ArrayList();
        try {
            map.put("userId", wmsTO.getUserId());
            System.out.println("map    in hubout    " + map);
            hubOutList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutList", map);
            System.out.println("hubOutList========" + hubOutList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutList;

    }

    public ArrayList hubOutListView(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList hubOutListView = new ArrayList();
        try {
            map.put("userId", wmsTO.getUserId());
            map.put("lrId", wmsTO.getLrId());
            System.out.println("map    in hubout    " + map);
            hubOutListView = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutListView", map);
            System.out.println("hubOutList========" + hubOutListView.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutListView;
    }

    public ArrayList hubOutPrintDetails(String lrId, String compId) {
        Map map = new HashMap();
        ArrayList hubOutPrintDetails = new ArrayList();
        try {
            map.put("lrId", lrId);
            map.put("compId", compId);
            System.out.println("map    in hubout    " + map);
            hubOutPrintDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.hubOutPrintDetails", map);
            System.out.println("hubOutList========" + hubOutPrintDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return hubOutPrintDetails;
    }

    public ArrayList getIfbOrderDetails(String lrId) {
        Map map = new HashMap();
        ArrayList getIfbOrderDetails = new ArrayList();
        try {
            map.put("lrId", lrId);
            System.out.println("map    in hubout    " + map);
            getIfbOrderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getIfbOrderDetails", map);
            System.out.println("getIfbOrderDetails========" + getIfbOrderDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return getIfbOrderDetails;

    }

    public int updateIfbHubout(String[] ewayBillNo, String[] ewayExpiry, String[] orderIds, String[] selectedStatus, String[] hubId, String vehicleNo, String driverName, String driverMobile, String date) {
        Map map = new HashMap();
        int updateIfbHubout = 0;
        int updateIfbLrHubout = 0;
        try {
            System.out.println("map    " + map);
            map.put("date", date);
            int sequence = (Integer) getSqlMapClientTemplate().queryForObject("wms.dispatchIdSequence", map);
            String x = sequence + "";
            if (x.length() == 1) {
                map.put("dispatchId", date + "00" + x);
            } else if (x.length() == 2) {
                map.put("dispatchId", date + "0" + x);
            } else if (x.length() == 3) {
                map.put("dispatchId", date + x);
            }
            map.put("vehicleNo", vehicleNo);
            map.put("driverName", driverName);
            map.put("driverMobile", driverMobile);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("orderId", orderIds[i]);
                    map.put("hubId", hubId[i]);
                    map.put("ewayNo", ewayBillNo[i]);
                    map.put("ewayExpiry", ewayExpiry[i]);
                    System.out.println("map   " + map);
                    updateIfbHubout = (Integer) getSqlMapClientTemplate().update("wms.updateIfbHubout", map);
                    updateIfbLrHubout = (Integer) getSqlMapClientTemplate().update("wms.updateIfbLrHubout", map);
                    System.out.println("updateIfbHubout---" + updateIfbHubout);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateIfbHubout;
    }

    public int updateHubIn(String[] orderIds, String[] selectedStatus) {
        Map map = new HashMap();
        int updateHubIn = 0;
        int updateHubInTime = 0;
        try {
            System.out.println("map    " + map);
            for (int i = 0; i < orderIds.length; i++) {
                if ("1".equals(selectedStatus[i])) {
                    map.put("orderId", orderIds[i]);
                    Double gross = (Double) getSqlMapClientTemplate().queryForObject("wms.getIfbGrossValue", map);
                    if (gross <= 50000) {
                        map.put("status", "1");
                    } else {
                        map.put("status", "0");
                    }
                    System.out.println("map   " + map);
                    updateHubIn = (Integer) getSqlMapClientTemplate().update("wms.updateHubIn", map);
                    updateHubInTime = (Integer) getSqlMapClientTemplate().update("wms.updateHubInTime", map);
                    System.out.println("updateHubIn---" + updateHubIn);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return updateHubIn;
    }

    public ArrayList ifbHubinList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbHubinList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            System.out.println("map    " + map);
            ifbHubinList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbHubinList", map);
            System.out.println("ifbHubinList========" + ifbHubinList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbHubinList;

    }

    public ArrayList getCustomerContract(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getContractList = new ArrayList();
        try {
            getContractList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getOrderApproval", map);
            System.out.println("getContractList========" + getContractList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getContractList;

    }

    public int uploadCustomerContract(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadCustomerContract = 0;
        int deleteCustomerContract = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("shipmentId", wmsTO.getShipmentId());
        System.out.println("shipmentId===========>>>" + wmsTO.getShipmentId());
        map.put("customerName", wmsTO.getCustomerName());
        System.out.println("customerName=" + wmsTO.getCustomerName());
        map.put("city", wmsTO.getCity());
        System.out.println("city=" + wmsTO.getCity());
        map.put("pinCode", wmsTO.getPinCode());
        map.put("pincode", wmsTO.getPinCode());
        System.out.println("pinCode=" + wmsTO.getPinCode());
        map.put("deliveryHub", wmsTO.getDeliveryHub());
        System.out.println("deliveryHub=" + wmsTO.getDeliveryHub());
        if (wmsTO.getNote().equals("")) {
            map.put("note", "-");
        } else {
            map.put("note", wmsTO.getNote());
            System.out.println("point4Name=" + wmsTO.getNote());
        }
        if (wmsTO.getShipmentId().equals("")) {
            map.put("shipmentId", "-");
        } else {
            map.put("shipmentId", wmsTO.getShipmentId());
            System.out.println("point4Name=" + wmsTO.getShipmentId());
        }
        if (wmsTO.getAssignedExecutive().equals("")) {
            map.put("assignedExecutive", "-");
        } else {
            map.put("assignedExecutive", wmsTO.getNote());
            System.out.println("assignedExecutive=" + wmsTO.getAssignedExecutive());
        }
        if (wmsTO.getOrigin().equals("")) {
            map.put("origin", "-");
        } else {
            map.put("origin", wmsTO.getNote());
            System.out.println("origin=" + wmsTO.getOrigin());
        }
        if (wmsTO.getLastModified().equals("")) {
            map.put("lastModified", "-");
        } else {
            map.put("lastModified", wmsTO.getNote());
            System.out.println("lastModified=" + wmsTO.getLastModified());
        }
        if (wmsTO.getLastReceived().equals("")) {
            map.put("lastReceived", "-");
        } else {
            map.put("lastReceived", wmsTO.getNote());
            System.out.println("lastReceived=" + wmsTO.getLastReceived());
        }

        map.put("assignedExecutive", wmsTO.getAssignedExecutive());
        System.out.println("assignedExecutive=" + wmsTO.getAssignedExecutive());
        map.put("origin", wmsTO.getOrigin());
        System.out.println("origin=" + wmsTO.getOrigin());
        map.put("currentHub", wmsTO.getCurrentHub());
        System.out.println("currentHub=" + wmsTO.getCurrentHub());
        map.put("status", wmsTO.getStatus());
        System.out.println("status=" + wmsTO.getStatus());
        map.put("note", wmsTO.getNote());
        System.out.println("note=" + wmsTO.getNote());
        map.put("amount", wmsTO.getAmount());
        System.out.println("amount=" + wmsTO.getAmount());
        map.put("weight", wmsTO.getWeight());
        System.out.println("weight=" + wmsTO.getWeight());
        map.put("lastModified", wmsTO.getLastModified());
        System.out.println("lastModified=" + wmsTO.getLastModified());
        map.put("lastReceived", wmsTO.getLastReceived());
        System.out.println("lastReceived=" + wmsTO.getLastReceived());
        map.put("shipmentType", wmsTO.getShipmentType());
        System.out.println("shipmentType=" + wmsTO.getShipmentType());
        map.put("orderType", "1");

        try {

            int checkShipmentId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkShipmentId", map);
            if (checkShipmentId > 0) {
                map.put("error", "2");
            } else {
                int checkHub = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDeliveryHub", map);
                if (checkHub == 0) {
                    map.put("error", "3");
                } else {
                    map.put("error", "0");
                }
            }

            System.out.println("map map map = " + map);
            uploadCustomerContract = (Integer) getSqlMapClientTemplate().insert("wms.uploadCustomerContract", map);
            System.out.println("uploadCustomerContract size=====" + uploadCustomerContract);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadCustomerContract Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadCustomerContract", sqlException);
        }
        return uploadCustomerContract;
    }

    public int SaveOrderApproval(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getSaveOrderApproval = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            map.put("whId", wmsTO.getWhId());
            map.put("userId", userId);
            System.out.println("RRRRR---%%%--");
            getSaveOrderApproval = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getOrderApproval", map);
            System.out.println("getContractUpload---" + getSaveOrderApproval.size());
            Iterator itr = getSaveOrderApproval.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("shipmentId", WmsTO.getShipmentId());
                map.put("customerName", WmsTO.getCustomerName());
                map.put("city", WmsTO.getCity());
                map.put("pinCode", WmsTO.getPinCode());
                map.put("deliveryHub", WmsTO.getDeliveryHub());
                map.put("assignedExecutive", WmsTO.getAssignedExecutive());
                map.put("origin", WmsTO.getOrigin());
                map.put("currentHub", WmsTO.getCurrentHub());
                map.put("status", WmsTO.getStatus());
                map.put("note", WmsTO.getNote());
                map.put("amount", WmsTO.getAmount());
                map.put("weight", WmsTO.getWeight());
                map.put("lastModified", WmsTO.getLastModified());
                map.put("lastReceived", WmsTO.getLastReceived());
                map.put("shipmentType", WmsTO.getShipmentType());
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().insert("wms.getContractList", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteUploadTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getreplacementList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getreplacementList = new ArrayList();
        try {
            int update = 0;

            getreplacementList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreplacementList", map);
            System.out.println("getreplacementList========" + getreplacementList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getreplacementList;

    }

    public int uploadReplacementOrder(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadReplacementOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("shipmentId", wmsTO.getShipmentId());
        System.out.println("shipmentId=" + wmsTO.getShipmentId());
        map.put("shipmentType", wmsTO.getShipmentType());
        System.out.println("shipmentType=" + wmsTO.getShipmentType());
        map.put("shipmentTierType", wmsTO.getShipmentTierType());
        System.out.println("shipmentTierType=" + wmsTO.getShipmentTierType());
        map.put("reverseShipmentId", wmsTO.getReverseShipmentId());
        System.out.println("reverseShipmentId=" + wmsTO.getReverseShipmentId());
        map.put("bagStatus", wmsTO.getBagStatus());
        System.out.println("bagStatus=" + wmsTO.getBagStatus());

        map.put("type", wmsTO.getType());
        System.out.println("type=" + wmsTO.getType());
        map.put("price", wmsTO.getPrice());
        System.out.println("price=" + wmsTO.getPrice());
        map.put("latestStatus", wmsTO.getLatestStatus());
        System.out.println("latestStatus=" + wmsTO.getLatestStatus());
        map.put("deliveryHub", wmsTO.getDeliveryHub());
        System.out.println("deliveryHub=" + wmsTO.getDeliveryHub());
        map.put("currentLocation", wmsTO.getCurrentLocation());
        System.out.println("currentLocation=" + wmsTO.getCurrentLocation());
        map.put("latestUpdateDateTime", wmsTO.getLatestUpdateDateTime());
        System.out.println("latestUpdateDateTime=" + wmsTO.getLatestUpdateDateTime());
        map.put("firstReceivedHub", wmsTO.getFirstReceivedHub());
        System.out.println("firstReceivedHub=" + wmsTO.getFirstReceivedHub());
        map.put("firstReceiveDateTime", wmsTO.getFirstReceiveDateTime());
        System.out.println("firstReceiveDateTime=" + wmsTO.getFirstReceiveDateTime());
        map.put("hubNotes", wmsTO.getHubNotes());
        System.out.println("hubNotes=" + wmsTO.getHubNotes());
        map.put("csNotes", wmsTO.getCsNotes());
        System.out.println("csNotes=" + wmsTO.getCsNotes());
        map.put("numberofAttempts", wmsTO.getNumberofAttempts());
        System.out.println("numberofAttempts=" + wmsTO.getNumberofAttempts());
        map.put("bagId", wmsTO.getBagId());
        System.out.println("bagId=" + wmsTO.getBagId());
        map.put("consignmentId", wmsTO.getConsignmentId());
        System.out.println("consignmentId=" + wmsTO.getConsignmentId());
        map.put("customerPromiseDate", wmsTO.getCustomerPromiseDate());
        System.out.println("customerPromiseDate=" + wmsTO.getCustomerPromiseDate());
        map.put("logisticsPromiseDate", wmsTO.getLogisticsPromiseDate());
        System.out.println("logisticsPromiseDate=" + wmsTO.getLogisticsPromiseDate());
        map.put("onHoldByOpsReason", wmsTO.getOnHoldByOpsReason());
        System.out.println("onHoldByOpsReason=" + wmsTO.getOnHoldByOpsReason());
        map.put("onHoldByOpsDate", wmsTO.getOnHoldByOpsDate());
        System.out.println("onHoldByOpsDate=" + wmsTO.getOnHoldByOpsDate());
        map.put("firstAssignedHubName", wmsTO.getFirstAssignedHubName());
        System.out.println("firstAssignedHubName=" + wmsTO.getFirstAssignedHubName());
        map.put("deliveryPinCode", wmsTO.getDeliveryPinCode());
        map.put("pincode", wmsTO.getDeliveryPinCode());
        System.out.println("deliveryPinCode=" + wmsTO.getDeliveryPinCode());
        map.put("lastReceivedDateTime", wmsTO.getLastReceivedDateTime());
        map.put("orderType", 2);
        System.out.println("lastReceivedDateTime=" + wmsTO.getLastReceivedDateTime());

        try {

            int checkShipmentId = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkShipmentId", map);
            if (checkShipmentId > 0) {
                map.put("error", "2");
            } else {
                int checkHub = (Integer) getSqlMapClientTemplate().queryForObject("wms.checkDeliveryHub", map);
                if (checkHub == 0) {
                    map.put("error", "3");
                } else {
                    map.put("error", "0");
                }
            }
            System.out.println("map map map = " + map);
            uploadReplacementOrder = (Integer) getSqlMapClientTemplate().insert("wms.uploadReplacementOrder", map);
            System.out.println("uploadNormalOrder size=====" + uploadReplacementOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadReplacementOrder;
    }

    public int replacementContractUpload(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getShipmentUpload = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            System.out.println("RRRRR---%%%--");
            getShipmentUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreplacementList", map);
            System.out.println("getShipmentUpload---" + getShipmentUpload.size());
            Iterator itr = getShipmentUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("shipmentId", WmsTO.getShipmentId());
                map.put("shipmentType", WmsTO.getShipmentType());
                map.put("shipmentTierType", WmsTO.getShipmentTierType());
                map.put("reverseShipmentId", WmsTO.getReverseShipmentId());
                map.put("bagStatus", WmsTO.getBagStatus());
                map.put("type", WmsTO.getType());
                map.put("price", WmsTO.getPrice());
                map.put("latestStatus", WmsTO.getLatestStatus());
                map.put("deliveryHub", WmsTO.getDeliveryHub());
                map.put("currentLocation", WmsTO.getCurrentLocation());
                map.put("latestUpdateDateTime", WmsTO.getLatestUpdateDateTime());
                map.put("firstReceivedHub", WmsTO.getFirstReceivedHub());
                map.put("firstReceiveDateTime", WmsTO.getFirstReceiveDateTime());
                map.put("hubNotes", WmsTO.getHubNotes());
                map.put("csNotes", WmsTO.getCsNotes());
                map.put("numberofAttempts", WmsTO.getNumberofAttempts());
                map.put("bagId", WmsTO.getBagId());
                map.put("consignmentId", WmsTO.getConsignmentId());
                map.put("customerPromiseDate", WmsTO.getCustomerPromiseDate());
                map.put("logisticsPromiseDate", WmsTO.getLogisticsPromiseDate());
                map.put("onHoldByOpsReason", WmsTO.getOnHoldByOpsReason());
                map.put("onHoldByOpsDate", WmsTO.getOnHoldByOpsDate());
                map.put("firstAssignedHubName", WmsTO.getFirstAssignedHubName());
                map.put("deliveryPinCode", WmsTO.getDeliveryPinCode());
                map.put("lastReceivedDateTime", WmsTO.getLastReceivedDateTime());
                map.put("whId", wmsTO.getWhId());
                map.put("userId", userId);
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().update("wms.saveReplacementOrders", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereplacementTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public ArrayList getreturnList(WmsTO wmsTO) {
        Map map = new HashMap();
        ArrayList getreturnList = new ArrayList();
        try {
            int update = 0;

            getreturnList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreturnlistupload", map);
            System.out.println("getList========" + getreturnList);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getContractList", sqlException);
        }
        return getreturnList;

    }

    public int uploadReturnordertemp(WmsTO wmsTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int uploadReplacementOrder = 0;
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("trackingId", wmsTO.getTrackingId());
        map.put("pickupaddress", wmsTO.getPickupaddress());
        map.put("pinCode", wmsTO.getPinCode());
        map.put("pincode", wmsTO.getPinCode());
        map.put("itemName", wmsTO.getItemName());
        map.put("itemNos", wmsTO.getItemNos());
        map.put("createdDate", wmsTO.getCreatedDate());
        map.put("pickupDate", wmsTO.getPickupDate());
        map.put("Type", wmsTO.getType());
        map.put("lastStatus", wmsTO.getLastStatus());
        map.put("remarks", wmsTO.getRemarks());
        map.put("numberofAttempts", wmsTO.getNumberofAttempts());

        try {
            map.put("error", "0");

            System.out.println("map map map = " + map);
            uploadReplacementOrder = (Integer) getSqlMapClientTemplate().insert("wms.uploadReturnOrdertemp", map);
            System.out.println("uploadNormalOrder size=====" + uploadReplacementOrder);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("uploadNormalOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "uploadNormalOrder", sqlException);
        }
        return uploadReplacementOrder;
    }

    public int returnContractUpload(WmsTO wmsTO, int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int updateStatus = 0;
        int deleteStatus = 0;
        ArrayList getShipmentUpload = new ArrayList();
        WmsTO WmsTO = new WmsTO();
        try {
            System.out.println("RRRRR---%%%--");
            getShipmentUpload = (ArrayList) getSqlMapClientTemplate().queryForList("wms.getreturnlistupload", map);
            System.out.println("getShipmentUpload---" + getShipmentUpload.size());
            Iterator itr = getShipmentUpload.iterator();
            while (itr.hasNext()) {
                WmsTO = (WmsTO) itr.next();
                map.put("trackingId", WmsTO.getTrackingId());
                map.put("pickupaddress", WmsTO.getPickupaddress());
                map.put("pinCode", WmsTO.getPinCode());
                map.put("pincode", WmsTO.getPinCode());
                map.put("itemName", WmsTO.getItemName());
                map.put("itemNos", WmsTO.getItemNos());
                map.put("createdDate", WmsTO.getCreatedDate());
                map.put("pickupDate", WmsTO.getPickupDate());
                map.put("Type", WmsTO.getType());
                map.put("lastStatus", WmsTO.getLastStatus());
                map.put("remarks", WmsTO.getRemarks());
                map.put("numberofAttempts", WmsTO.getNumberofAttempts());
                map.put("whId", wmsTO.getWhId());
                map.put("userId", userId);
                System.out.println("map = " + map);
                if ("0".equals(WmsTO.getError())) {
                    updateStatus = (Integer) getSqlMapClientTemplate().insert("wms.saveReturnOrdersupload", map);
                }
                deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereturnTemp", map);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return insertStatus;
    }

    public int deleteNormalOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteUploadTemp", map);
            System.out.println("deleteUploadTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteReplacementOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereplacementTemp", map);
            System.out.println("deletereplacementTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteReturnOrderTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deletereturnTemp", map);
            System.out.println("deletereturnTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public int deleteRunsheetTemp(int userId) {
        Map map = new HashMap();
        int deleteStatus = 0;
        try {
            map.put("userId", userId);
            deleteStatus = (Integer) getSqlMapClientTemplate().delete("wms.deleteRunsheetTemp", map);
            System.out.println("deleteRunsheetTemp---" + deleteStatus);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return deleteStatus;
    }

    public ArrayList ifbLRUploadList(WmsTO wms) {
        Map map = new HashMap();
        ArrayList ifbLRUploadList = new ArrayList();
        try {
            map.put("userId", wms.getUserId());
            map.put("fromDate", wms.getFromDate());
            System.out.println("map    " + map);
            ifbLRUploadList = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRUploadList", map);
            System.out.println("ifbLRList========" + ifbLRUploadList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "getIFBList", sqlException);
        }
        return ifbLRUploadList;

    }

    public ArrayList ifbLRPrintlist(String lrId) {
        Map map = new HashMap();
        ArrayList ifbLRPrintlist = new ArrayList();
        try {
            map.put("lrId", lrId);
            ifbLRPrintlist = (ArrayList) getSqlMapClientTemplate().queryForList("wms.ifbLRPrintlist", map);
            System.out.println("ifbLRPrintlist---" + ifbLRPrintlist.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCancelcharges Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "insertJobCancelcharges", sqlException);
        }
        return ifbLRPrintlist;
    }
}
