package ets.domain.wms.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.business.PaginationHelper;
import ets.arch.web.BaseController;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.racks.business.RackBP;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.wms.business.WmsBP;
import ets.domain.wms.business.WmsTO;
import java.io.File;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import jxl.Workbook;
import jxl.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import jxl.WorkbookSettings;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.domain.mrs.web.MrsCommand;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class WmsController extends BaseController {

    WmsCommand wmsCommand;
    WmsBP wmsBP;

    public WmsCommand getWmsCommand() {
        return wmsCommand;
    }

    public void setWmsCommand(WmsCommand wmsCommand) {
        this.wmsCommand = wmsCommand;
    }

    public WmsBP getWmsBP() {
        return wmsBP;
    }

    public void setWmsBP(WmsBP wmsBP) {
        this.wmsBP = wmsBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        //   int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        System.out.println("userId" + userId);
        //  System.out.println("loginRecordId" + loginRecordId);
        String getUserFunctionsActive = "";
        String uri = request.getRequestURI();
//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
            String temp[] = getUserFunctionsActive.split("~");
            System.out.println("function Id" + temp[0]);
            System.out.println("function Name  " + temp[0]);
            int status1 = 0;
            int functionId = Integer.parseInt(temp[0]);
            String functionName = temp[1];
//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
        }
        binder.closeNoCatch();
        initialize(request);
    }

    /**
     * This method used to View Vendor Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewDeliveryRequest(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/deliveryRequestMaster.jsp";
        String pageTitle = "viewDeliveryRequest";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewDeliveryRequest";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            wmsTo.setUserId(userId + "");
            ArrayList deliveryRequestList = new ArrayList();
            deliveryRequestList = wmsBP.getviewDeliveryRequest(wmsTo);
            request.setAttribute("deliveryRequestList", deliveryRequestList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
//        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
//            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
//            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
//                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getDeliveryRequestUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();

        String pageTitle = "View Order Details";
        menuPath = "Operation >>  Order Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        Date dNow = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dNow);
        cal.add(Calendar.DATE, 0);
        dNow = cal.getTime();

        try {
            path = "content/trip/deliveryRequestUpdate.jsp";
            String orderId = request.getParameter("orderId");

            wmsTO.setOrderId(orderId);
            ArrayList getDeliveryRequestUpdate = new ArrayList();
            request.setAttribute("orderId", orderId);
            getDeliveryRequestUpdate = wmsBP.getDeliveryRequestUpdate(wmsTO);
            System.out.println("getDeliveryRequestUpdate " + getDeliveryRequestUpdate.size());
            request.setAttribute("getDeliveryRequestUpdate", getDeliveryRequestUpdate);
            WmsTO wmsTO1 = new WmsTO();
            if (getDeliveryRequestUpdate.size() > 0) {
                Iterator itr = getDeliveryRequestUpdate.iterator();
                while (itr.hasNext()) {
                    wmsTO1 = (WmsTO) itr.next();
                    request.setAttribute("delRequestId", wmsTO1.getDelRequestId());
                    request.setAttribute("loanProposalId", wmsTO1.getLoanProposalId());
                    request.setAttribute("createdDate", wmsTO1.getCreatedDate());
                    request.setAttribute("productId", wmsTO1.getProductId());
                    request.setAttribute("serialNumber", wmsTO1.getSerialNumber());
                    request.setAttribute("orderId", wmsTO1.getOrderId());
                    request.setAttribute("qty", wmsTO1.getQty());
                    request.setAttribute("customerName", wmsTO1.getCustomerName());
                    request.setAttribute("model", wmsTO1.getModel());
                    request.setAttribute("branch", wmsTO1.getBranchName());
                    request.setAttribute("orderDate", wmsTO1.getBatchDate());
                    request.setAttribute("itemId", wmsTO1.getItemId());
                    request.setAttribute("warehouseId", wmsTO1.getWarehouseId());
                }
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*
         * run time exception has occurred. Directed to error page.
         */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateDeliveryStatus(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String delRequestId = request.getParameter("delRequestId");
            String loanProposalId = request.getParameter("loanProposalId");
            String createdDate = request.getParameter("createdDate");
            String productId = request.getParameter("productId");
            String serialNumber = request.getParameter("serialNumber");
            String orderId = request.getParameter("orderId");
            String qty = request.getParameter("qty");
            String itemId = request.getParameter("itemId");
            String warehouseId = request.getParameter("warehouseId");

            wmsTO.setDelRequestId(delRequestId);
            wmsTO.setLoanProposalId(loanProposalId);
            wmsTO.setCreatedDate(createdDate);
            wmsTO.setProductId(productId);
            wmsTO.setSerialNumber(serialNumber);
            wmsTO.setOrderId(orderId);
            wmsTO.setQty(qty);
            wmsTO.setItemId(itemId);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setUserId(userId + "");

            int status = wmsBP.getDeliveryRequestFinalUpdate(wmsTO, userId);
            System.out.println("status :" + status);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request Updated Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Request Not Updated.");
            }

            path = "content/wms/deliveryRequestMaster.jsp";

            ArrayList deliveryRequestList = new ArrayList();
            deliveryRequestList = wmsBP.getviewDeliveryRequest(wmsTO);
            request.setAttribute("deliveryRequestList", deliveryRequestList);

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewAsnMasterList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/receivedAsnMaster.jsp";
        String pageTitle = "viewAsnMasterList";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewAsnMasterList";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList asnMasterList = new ArrayList();
            wmsTo.setUserId(userId + "");
            asnMasterList = wmsBP.getAsnMasterList(wmsTo);
            request.setAttribute("asnMasterList", asnMasterList);

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewReceivedGRN(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        path = "content/wms/receivedAsnDetails.jsp";
        String pageTitle = "viewReceivedGRN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewReceivedGRN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String asnId = request.getParameter("asnId");
            wmsTo.setAsnId(asnId);
            ArrayList asnDetails = new ArrayList();
            asnDetails = wmsBP.getAsnDetails(wmsTo);
            request.setAttribute("asnDetails", asnDetails);
            System.out.println("asnDetails.size()----" + asnDetails.size());

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void saveSerialTempDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String SerialNo = request.getParameter("serialNo");
            String serialNoAlreadyExists = "";
            serialNoAlreadyExists = wmsBP.serialNoAlreadyExists(SerialNo);
            System.out.println("serialnoexist==========" + serialNoAlreadyExists);
            if ("1".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Used Serial No";
            } else if ("2".equals(serialNoAlreadyExists)) {
                existsStatus = "Already Scanned Serial No";
            } else if ("3".equals(serialNoAlreadyExists)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkRouteCode(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String routeCode = request.getParameter("routeCode");
            String checkRouteCode = "";
            checkRouteCode = wmsBP.checkRouteCode(routeCode);
            if ("1".equals(checkRouteCode)) {
                existsStatus = "Route Code Exists";
            } else if ("2".equals(checkRouteCode)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("serialNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public void checkInvoiceExist(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String existsStatus = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            String InvoiceNo = request.getParameter("invoiceNo");
            String invoiceNoAlreadyExists = "";
            invoiceNoAlreadyExists = wmsBP.invoiceNoAlreadyExists(InvoiceNo);
            System.out.println("serialnoexist==========" + invoiceNoAlreadyExists);
            if ("1".equals(invoiceNoAlreadyExists)) {
                existsStatus = "Already Used InvoiceNo";
            } else if ("2".equals(invoiceNoAlreadyExists)) {
                existsStatus = ""; //new Serial No
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(existsStatus);
            writer.close();
            request.setAttribute("invoiceNoAlreadyExists", existsStatus);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
        }
    }

    public ModelAndView saveGrnSerialDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setAsnId(request.getParameter("asnId"));
            wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            wmsTO.setUom(request.getParameterValues("uom"));
            wmsTO.setProCond(request.getParameterValues("proCond"));
            wmsTO.setIpQty(request.getParameterValues("ipQty"));
            if (wmsCommand.getItemId() != null && wmsCommand.getItemId() != "") {
                wmsTO.setItemId(wmsCommand.getItemId());
            }
            wmsTO.setQty(request.getParameter("qty"));
            wmsTO.setBinIds(request.getParameterValues("bins"));
            wmsTO.setEmpUserId(request.getParameter("empUserId"));
            wmsTO.setGtnDetId(request.getParameter("gtnDetId"));
            wmsTO.setCustId(request.getParameter("custId"));
            wmsTO.setGtnId(request.getParameter("gtnId"));

            int tempGrnDetails = 0;

            ArrayList serialTempDetails = new ArrayList();
            tempGrnDetails = wmsBP.saveTempGrnDetails(wmsTO, userId);
            if (tempGrnDetails > 0) {
                serialTempDetails = wmsBP.getSerialTempDetailsUnsed(wmsTO);
                request.setAttribute("serialTempDetails", serialTempDetails);
                double reqQty = Double.parseDouble(request.getParameter("reqQuantity"));
                double actQty = serialTempDetails.size();
                wmsTO.setReqQty(reqQty);
                wmsTO.setActQty(actQty);

                double unusedQty = 0;
                System.out.println("unusedQty-----" + unusedQty);
                wmsTO.setUnusedQty(unusedQty);

                int status = 0;
                status = wmsBP.saveGrnSerialDetails(wmsTO, userId);
                System.out.println("saveSerialTempDetails---" + status);

                ArrayList asnMasterList = new ArrayList();
                wmsTO.setUserId(userId + "");
                asnMasterList = wmsBP.getAsnMasterList(wmsTO);
                request.setAttribute("asnMasterList", asnMasterList);

                System.out.println("asnMasterList......" + asnMasterList.size());
                if (status > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Created Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Created Failed");

                }
            }
            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewDockInList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        String pageTitle = "viewDockInList";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewDocketList";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            wmsTo.setUserId(userId + "");
            ArrayList getDockInList = new ArrayList();
            getDockInList = wmsBP.getDockInList(wmsTo);
            request.setAttribute("getDockInList", getDockInList);

            path = "content/wms/viewDockInList.jsp";

            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewDockInDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "viewGRNDetails  >>  View ";
        path = "content/wms/viewDockInDetails.jsp";
        String pageTitle = "viewGRNDetails";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewGRNDetails";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String asnId = request.getParameter("asnId");
            String grnId = request.getParameter("grnId");
            String itemId = request.getParameter("itemId");
            String gtnId = request.getParameter("gtnId");
            wmsTo.setAsnId(asnId);
            wmsTo.setItemId(itemId);
            wmsTo.setGrnId(grnId);
            wmsTo.setGtnId(gtnId);
            System.out.println("itemId----" + itemId);
            ArrayList getDockInDetails = new ArrayList();
            getDockInDetails = wmsBP.getDockInDetails(wmsTo);
            request.setAttribute("getDockInDetails", getDockInDetails);
            System.out.println("getDockInDetails.size()----" + getDockInDetails.size());

            ArrayList serialTempDetails = new ArrayList();
            serialTempDetails = wmsBP.getSerialTempDetails(wmsTo);
            request.setAttribute("serialTempDetails", serialTempDetails);
            int serialTempSize = serialTempDetails.size();
            request.setAttribute("serialTempSize", serialTempSize);
            ArrayList getBinList = new ArrayList();
            getBinList = wmsBP.getBinList(wmsTo);
            request.setAttribute("getBinList", getBinList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveGrnSerialStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        wmsCommand = command;
        String menuPath = "DockIn Details";
        int insertStatus = 0;
        ModelAndView mv = null;
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int j = 0;
        int n = 0;
        int p = 0;
        int q = 0;
        int r = 0;
        int s = 0;
        int t = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] serialNos = new String[500];
        String[] binIds = new String[500];
        String itemId = "";
        String asnId = "";
        String inpQty = "";
        String invoiceNo = "";
        String invoiceDate = "";
        String ewaybillNo = "";
        String whId = "";
        String grnId = "";
        String binId = "";
        String custId = "";
        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Dockin Details";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                System.out.println("this is tht");
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        // actualServerFilePath = "D:\\HS\\Images";
                        System.out.println("Server Path == " + actualServerFilePath);
//                        tempServerFilePath = actualServerFilePath.replace("//", "////");
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();

                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {

                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "//" + fileSaved[j];
                            actualFilePath = actualServerFilePath + "//" + tempFilePath;
                            System.out.println("tempPath..." + tempFilePath);
                            System.out.println("actPath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            System.out.println("fileSize..." + fileSize);
                            File f1 = new File(actualFilePath);
                            System.out.println("check " + f1.isFile());
                            f1.renameTo(new File(tempFilePath[j]));
                            System.out.println("tempPath = " + tempFilePath);
                            System.out.println("actPath = " + actualFilePath);
//                            String parts = actualFilePath.substring(actualFilePath.lastIndexOf("\\"));
//                            String part1 = parts.replace("\\", "");
                        }

                        System.out.println("fileName..." + fileName);
                        j++;
                    } else if (partObj.isParam()) {
                        if (partObj.getName().equals("itemId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            itemId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("asnId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            asnId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("inpQty")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            inpQty = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("custId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            custId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("invoiceDate")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            invoiceDate = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("ewaybillNo")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            ewaybillNo = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("whId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            whId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("grnId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            grnId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("binId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            binId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("serialNos")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            serialNos[s] = paramPart.getStringValue();
                            System.out.println("serialNos " + serialNos);
                            s++;
                        }
                        if (partObj.getName().equals("binIds")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            binIds[t] = paramPart.getStringValue();
                            System.out.println("serialNos " + serialNos);
                            t++;
                        }

                    }
                }
            }

//             wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            String file = "";
            String file1 = "";
            String saveFile = "";
            String saveFile1 = "";
            int status = 0;

            wmsTO.setItemId(itemId);
            wmsTO.setAsnId(asnId);
            wmsTO.setInvoiceNo(invoiceNo);
            wmsTO.setInvoiceDate(invoiceDate);
            wmsTO.setEwaybillNo(ewaybillNo);
            wmsTO.setWhId(whId);
            wmsTO.setGrnId(grnId);
            wmsTO.setBinId(binId);
            wmsTO.setRequestQty(inpQty);
            wmsTO.setSerialNos(serialNos);
            wmsTO.setBinIds(binIds);
            wmsTO.setCustId(custId);
            wmsTO.setInpQty(inpQty);
            System.out.println("value of j" + j);
            file = tempFilePath[0];
            file1 = tempFilePath[1];
            System.out.println("file :" + file);
            System.out.println("file :" + file1);
            saveFile = fileSaved[0];
            saveFile1 = fileSaved[1];
            System.out.println("saveFile :" + saveFile);
            System.out.println("saveFile :" + saveFile1);

            status = wmsBP.saveGrnSerialStockDetails(wmsTO, file, file1, saveFile, saveFile1, userId);

            System.out.println("saveGrnSerialStockDetails---" + status);

            wmsTO.setUserId(userId + "");
            ArrayList getDockInList = new ArrayList();
            getDockInList = wmsBP.getDockInList(wmsTO);
            request.setAttribute("getDockInList", getDockInList);

            System.out.println("getDockInList......" + getDockInList.size());
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Stock Created Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Stock Failed");

            }
            path = "content/wms/viewDockInList.jsp";
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView grnView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View Trip Sheet Report";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Grn View Repor";
            request.setAttribute("pageTitle", pageTitle);
            WmsTO wmsTO = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            String userIds = String.valueOf(userId);
            wmsTO.setUserId(userIds);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String whId = request.getParameter("fleetCenterId");
            wmsTO.setWhId(whId);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList grnViewDetails = new ArrayList();
            grnViewDetails = wmsBP.grnViewDetails(wmsTO);
            request.setAttribute("grnViewDetails", grnViewDetails);

            String param = request.getParameter("param");
            if (param.equals("ExportExcel")) {
                path = "content/report/grnViewReportExcel.jsp";
            } else {
                path = "content/wms/grnViewReport.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView putAwayList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Operation  >>  View Trip Sheet Report";
        String statusId1 = request.getParameter("statusId");
        System.out.println("pass value:" + request.getParameter("successMessage"));
        request.setAttribute("errorMessage", request.getParameter("msg"));

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Grn View Repor";
            request.setAttribute("pageTitle", pageTitle);
            WmsTO wmsTO = new WmsTO();
            int userId = (Integer) session.getAttribute("userId");
            String userIds = String.valueOf(userId);
            wmsTO.setUserId(userIds);

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String whId = request.getParameter("fleetCenterId");
            wmsTO.setWhId(whId);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList grnViewDetails = new ArrayList();
            grnViewDetails = wmsBP.grnViewDetails(wmsTO);
            request.setAttribute("grnViewDetails", grnViewDetails);

            String param = request.getParameter("param");
            if ("print".equals(param)) {
                path = "content/report/putAwayListDetails.jsp";
            } else {
                path = "content/wms/putAwayList.jsp";
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewSerialNumberDetails(HttpServletRequest request, HttpServletResponse response) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Vehicle  >>  View Vehicle Detail";
//        reportCommand = command;
//        ReportTO report = new ReportTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int UserId = (Integer) session.getAttribute("userId");
        try {
            //            if (!loginBP.checkAuthorisation(userFunctions, "Vehicle-Add")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            pageTitle = "Alter Vehicle";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("pageTitle", pageTitle);
            String param = request.getParameter("param");
            System.out.println("param==" + param);
            if (param.equals("ExportExcel")) {
                request.setAttribute("grnNo", (request.getParameter("grnNo")));
                request.setAttribute("asnNo", (request.getParameter("asnNo")));
                request.setAttribute("asnDate", (request.getParameter("asnDate")).split(" ")[0]);
                request.setAttribute("grnDate", (request.getParameter("grnDate")));
                path = "content/report/grnViewReportExcel.jsp";
            } else {
                path = "content/wms/viewSerialNumberDetails.jsp";
            }
            String grnId = request.getParameter("grnId");
            request.setAttribute("grnId", grnId);
            System.out.println("grnId==" + grnId);
//            report.setItemId(itemId);

            ArrayList viewSerialNumberDetails = new ArrayList();
            viewSerialNumberDetails = wmsBP.viewSerialNumberDetails(grnId);
            System.out.println("viewSerialNumberDetails.size() = " + viewSerialNumberDetails.size());
            request.setAttribute("viewSerialNumberDetails", viewSerialNumberDetails);
            // }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve viewStockQtyDetails data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewOrderDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();

        String pageTitle = "View Order Details";
        menuPath = "Operation >>  Order Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            String param = request.getParameter("param");
            System.out.println("param vale is:" + param);

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

            if (param != null && param.equals("exportExcel")) {
                path = "content/trip/shipmentViewReportExcel.jsp";
            } else {
                path = "content/wms/shipmentViewReport.jsp";
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            String whId = request.getParameter("fleetCenterId");
            wmsTO.setWhId(whId);

            request.setAttribute("whId", whId);
            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

            ArrayList ordersList = new ArrayList();
            ordersList = wmsBP.getOrderDetailsList(wmsTO);
            System.out.println("ordersList " + ordersList.size());
            request.setAttribute("ordersList", ordersList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView inboundGateIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        wmsCommand = command;
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTO = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (wmsCommand.getInvoiceNo() != null && wmsCommand.getInvoiceNo() != "") {
                wmsTO.setInvoiceNo(wmsCommand.getInvoiceNo());
            }
            if (wmsCommand.getInvoiceDate() != null && wmsCommand.getInvoiceDate() != "") {
                wmsTO.setInvoiceDate(wmsCommand.getInvoiceDate());
            }
            if (wmsCommand.getGtnDate() != null && wmsCommand.getGtnDate() != "") {
                wmsTO.setGtnDate(wmsCommand.getGtnDate());
            }
            if (wmsCommand.getGtnTime() != null && wmsCommand.getGtnTime() != "") {
                wmsTO.setGtnTime(wmsCommand.getGtnTime());
            }
            if (wmsCommand.getCustId() != null && wmsCommand.getCustId() != "") {
                wmsTO.setCustId(wmsCommand.getCustId());
            }
            if (wmsCommand.getDock() != null && wmsCommand.getDock() != "") {
                wmsTO.setDock(wmsCommand.getDock());
            }
            if (wmsCommand.getVehicleType() != null && wmsCommand.getVehicleType() != "") {
                wmsTO.setVehicleType(wmsCommand.getVehicleType());
            }
            if (wmsCommand.getVehicleNo() != null && wmsCommand.getVehicleNo() != "") {
                wmsTO.setVehicleNo(wmsCommand.getVehicleNo());
            }
            if (wmsCommand.getDriverName() != null && wmsCommand.getDriverName() != "") {
                wmsTO.setDriverName(wmsCommand.getDriverName());
            }
            if (wmsCommand.getRemarks() != null && wmsCommand.getRemarks() != "") {
                wmsTO.setRemarks(wmsCommand.getRemarks());
            }

            String param = request.getParameter("param");
            System.out.println("param===" + param);
            if (param != null && param.equals("Save")) {
                System.out.println("param.invoiceno===" + wmsCommand.getInvoiceNo());
                System.out.println("param.date===" + wmsCommand.getInvoiceDate());
                System.out.println("param.gtndate===" + wmsCommand.getGtnDate());
                System.out.println("param.gtntime===" + wmsCommand.getGtnTime());
                System.out.println("param.Remarks===" + wmsCommand.getRemarks());
                System.out.println("param.dock===" + wmsCommand.getDock());
                System.out.println("param.equals===" + wmsCommand.getVehicleNo());
                System.out.println("param.equals===" + wmsCommand.getVehicleType());

                wmsTO.setPoNos(request.getParameterValues("poNo"));
                wmsTO.setPoQty(request.getParameterValues("poQty"));
                int saveGtnDetails = 0;
                saveGtnDetails = wmsBP.saveGtnDetails(wmsTO, userId);
                path = "content/wms/inboundGateIn.jsp";
                if (saveGtnDetails != 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Gate In Updated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GTN Stock Failed");
                }
            } else {
                path = "content/wms/inboundGateIn.jsp";
            }
//           
            ArrayList poCode = new ArrayList();
            poCode = wmsBP.getPoCode(wmsTO, userId);
            request.setAttribute("poCode", poCode);
            ArrayList custList = new ArrayList();
            custList = wmsBP.getCustList(wmsTO);
            request.setAttribute("custList", custList);
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));
            String invoiceDate = "";
            String gtnDate = "";
            invoiceDate = request.getParameter("invoiceDate");
            gtnDate = request.getParameter("gtnDate");

            if (invoiceDate != null) {
                request.setAttribute("invoiceDate", invoiceDate);
                wmsTO.setInvoiceDate(invoiceDate);
            } else {
                invoiceDate = "";
            }

            if (gtnDate != null) {
                wmsTO.setGtnDate(gtnDate);
                request.setAttribute("gtnDate", gtnDate);
            } else {
                gtnDate = "";
            }

            String endDate = dateFormat.format(curDate);

            if ("".equals(gtnDate)) {
                gtnDate = endDate;
                wmsTO.setToDate(gtnDate);
            }

            request.setAttribute("gtnDate", gtnDate);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView receivedGateIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Received GateIn  >>  View ";

        String pageTitle = "receivedGateIn";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "receivedGateIn";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";
            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView unloadingUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Unloading Update  >>  View ";

        String pageTitle = "Unloading Update";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Unloading Update";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/unloadingUpdate.jsp";
            // }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateUnloadingDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Inbound  >>  View ";
        String pageTitle = "Unloading Update Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Update Unloading Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Unloaded Successfully.");
            }
            String gtnId = request.getParameter("gtnId");
            wmsTo.setGtnId(gtnId);
            request.setAttribute("gtnId", gtnId);
            ArrayList proList = new ArrayList();
            proList = wmsBP.getProList(userId, wmsTo);
            request.setAttribute("gtnDetails", proList);
            String itemId = request.getParameter("itemId");
            String asnId = request.getParameter("asnId");
            path = "content/wms/updateUnloadingDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewReceivedGTN(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Manage Vendor  >>  View ";
        String pageTitle = "viewReceivedGTN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        // int loginRecordId = (Integer) session.getAttribute("loginRecordId");
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "viewReceivedGTN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String gtnId = request.getParameter("gtnId");
            wmsTo.setGtnId(gtnId);
            request.setAttribute("gtnId", gtnId);
            ArrayList proList = new ArrayList();
            proList = wmsBP.getProList(userId, wmsTo);
            request.setAttribute("gtnDetails", proList);
            String itemId = request.getParameter("itemId");
            String asnId = request.getParameter("asnId");
            if (asnId != "" && itemId != "") {
                wmsTo.setItemId("itemId");
                wmsTo.setAsnId("asnId");
//            ArrayList serialDetails=new ArrayList();
//            serialDetails = wmsBP.getSerialDetails();
//            request.setAttribute("serialDetails",serialDetails);
//            ArrayList gtnDetails = new ArrayList();
//            gtnDetails = wmsBP.getGtnDetails(wmsTo, userId);
                ArrayList userDetails = new ArrayList();
                userDetails = wmsBP.getUserDetails(userId);
//            ArrayList binDetails=new ArrayList();
//            binDetails=wmsBP.getBinDetails(wmsTo);
//            request.setAttribute("binDetails",binDetails);
                request.setAttribute("userDetails", userDetails);
//            request.setAttribute("gtnDetails", gtnDetails);
//            System.out.println("gtnDetails.size()----"+gtnDetails.size());
                path = "content/wms/receivedGtnDetails.jsp";
            } else {
                path = "content/wms/receivedGtnDetails.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getBinDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getBinDetails = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String itemId = "";

            response.setContentType("text/html");
            itemId = request.getParameter("item");
            wmsTO.setItemId(itemId);
            System.out.println("itemId=+++++" + itemId);
            getBinDetails = wmsBP.getBinDetails(wmsTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getBinDetails.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("Id", wmsTO.getBinId());
                jsonObject.put("Name", wmsTO.getBinName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public void getItemList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {
        HttpSession session = request.getSession();
        wmsCommand = command;
        WmsTO wmsTO = new WmsTO();

        ArrayList getItemList = new ArrayList();
        PrintWriter pw = response.getWriter();
        try {
            String custId = "";
            int userId = (Integer) session.getAttribute("userId");
            custId = request.getParameter("custId");
            getItemList = wmsBP.getItemList(userId, custId);
            response.setContentType("text/html");
            JSONArray jsonArray = new JSONArray();
            Iterator itr = getItemList.iterator();
            int cntr = 0;
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                wmsTO = (WmsTO) itr.next();
                jsonObject.put("Id", wmsTO.getItemId());
                jsonObject.put("Name", wmsTO.getItemName());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
                cntr++;
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    public ModelAndView editSerialDetail(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Edit GRN  >>  Edit ";
        path = "content/wms/editSerialDetail.jsp";
        String pageTitle = "EditGRN";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "EditGRN";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String itemId = "";
            ArrayList serialList = new ArrayList();
            ArrayList getBinDetails = new ArrayList();
            String gtnId = request.getParameter("gtnId");
            wmsTo.setGtnId(gtnId);
            String grnId = request.getParameter("grnId");
            wmsTo.setGrnId(grnId);
            ArrayList userDetails = new ArrayList();
            userDetails = wmsBP.getUserDetails(userId);
            request.setAttribute("userDetails", userDetails);
            request.setAttribute("gtnId", gtnId);
            ArrayList proList = new ArrayList();
            proList = wmsBP.getProList(userId, wmsTo);
            request.setAttribute("proList", proList);
            itemId = request.getParameter("itemId");
            request.setAttribute("grnId", grnId);

            int delGrnTempSerial = 0;
            String tempId = request.getParameter("tempId");
            String custId = request.getParameter("custId");
            if (tempId != "" && tempId != null) {
                delGrnTempSerial = wmsBP.delGrnTempSerial(tempId, grnId);
            }

            if (itemId != "" && itemId != null) {
                String asnId = request.getParameter("asnId");
                wmsTo.setAsnId(request.getParameter("asnId"));
                request.setAttribute("asnId", request.getParameter("asnId"));
                wmsTo.setItemId(itemId);
                ArrayList proListDetails = new ArrayList();
                proListDetails = wmsBP.proListDetails(userId, wmsTo);
                Iterator itr = proListDetails.iterator();
                WmsTO wms1 = new WmsTO();
                while (itr.hasNext()) {
                    wms1 = (WmsTO) itr.next();
                    request.setAttribute("custId", wms1.getCustId());
                    custId = wms1.getCustId();
                }
                request.setAttribute("proListDet", proListDetails);
                request.setAttribute("itemId", itemId);
                System.out.println("itemId=+++++" + itemId);
                getBinDetails = wmsBP.getBinDetails(wmsTo);
                request.setAttribute("binDetails", getBinDetails);
                System.out.println("binListSize" + getBinDetails.size());
                serialList = wmsBP.getSerialDets(gtnId, grnId, itemId, asnId);
                request.setAttribute("serialDet", serialList);
                wmsTo.setCustId(custId);
                ArrayList barcodes = wmsBP.getBarcodeDetails(itemId, wmsTo);
                request.setAttribute("barcodes", barcodes);
                request.setAttribute("size", serialList.size());
                System.out.println("getSerialDet.size() = " + serialList.size());

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView editGrnSerialDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        wmsCommand = command;
        HttpSession session = request.getSession();
        String path = "";

        int userId = (Integer) session.getAttribute("userId");

        WmsTO wmsTO = new WmsTO();
        try {
            wmsTO.setAsnId(request.getParameter("asnId"));
            wmsTO.setSerialNos(request.getParameterValues("serialNos"));
            wmsTO.setUom(request.getParameterValues("uom"));
            wmsTO.setProCond(request.getParameterValues("proCond"));
            wmsTO.setIpQty(request.getParameterValues("ipQty"));
            wmsTO.setQty(request.getParameter("qty"));
            wmsTO.setBinIds(request.getParameterValues("bins"));
            wmsTO.setEmpUserId(request.getParameter("empUserId"));
            wmsTO.setGtnDetId(request.getParameter("gtnDetId"));
            wmsTO.setGtnId(request.getParameter("gtnId"));
            wmsTO.setGrnId(request.getParameter("grnId1"));
            wmsTO.setItemId(request.getParameter("itemId1"));

            int updateSerialTempDetails = 0;
            updateSerialTempDetails = wmsBP.updateSerialTempDetails(wmsTO, userId);
            if (updateSerialTempDetails > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "GRN Created Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "GRN Created Failed");

            }
            ArrayList receivedGateIn = new ArrayList();
            receivedGateIn = wmsBP.getReceivedGateIn(userId);
            request.setAttribute("receivedGateIn", receivedGateIn);
            path = "content/wms/receivedGateIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView addBarcodeMaster(HttpServletRequest request, HttpServletResponse reponse, WmsCommand command) throws IOException {
        System.out.println("ProductCategory...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        wmsCommand = command;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String barcodeMaster = "";
        WmsTO wmsTO = new WmsTO();
        String menuPath = "";
        int generateBarcode = 0;
        menuPath = "Operation  >> handleBarcodeMaster ";
        String pageTitle = "addBarcodeMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {
            ArrayList custList = new ArrayList();
            ArrayList barCodeDetails = new ArrayList();
            custList = wmsBP.getCustList(wmsTO);
            request.setAttribute("custList", custList);

            barcodeMaster = wmsBP.getBarcodeMaster();
            request.setAttribute("barcodeMaster", barcodeMaster);

            String param = request.getParameter("param");
            if ("Generate".equals(param)) {
                wmsTO.setBarcode(request.getParameter("barCodeNo"));
                wmsTO.setCount(Integer.parseInt(request.getParameter("count")));
                wmsTO.setCustId(request.getParameter("custId"));
                wmsTO.setItemId(request.getParameter("itemId"));
                generateBarcode = wmsBP.generateBarcode(wmsTO, userId);
                if (generateBarcode > 0) {
                    request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Barcode Generated Successfully.");
                } else {
                    request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Barcode Generation Failed");

                }
                barcodeMaster = wmsBP.getBarcodeMaster();
                request.setAttribute("barcodeMaster", barcodeMaster);
                path = "content/wms/addBarcodeMaster.jsp";
            } else if ("Search".equals(param)) {
                wmsTO.setCustId(request.getParameter("custId"));
                wmsTO.setItemId(request.getParameter("itemId"));
                request.setAttribute("custId", request.getParameter("custId"));
                request.setAttribute("itemId", request.getParameter("itemId"));
                barCodeDetails = wmsBP.barCodeDetails(wmsTO);
                request.setAttribute("barCodeDetails", barCodeDetails);

                path = "content/wms/addBarcodeMaster.jsp";
            } else {

                path = "content/wms/addBarcodeMaster.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView warehouseOrderReport(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String pageTitle = "";
        String menuPath = "Report >>  Hub Report";
        wmsCommand = command;
        WmsTO wms = new WmsTO();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        int userId = (Integer) session.getAttribute("userId");
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            request.setAttribute("pageTitle", pageTitle);
            String param = request.getParameter("param");

            ArrayList stateList = new ArrayList();
            stateList = wmsBP.getStateList();
            request.setAttribute("stateList", stateList);

            ArrayList whList = new ArrayList();
            whList = wmsBP.getWhList();
            request.setAttribute("whList", whList);

            if ("search".equals(param)) {

                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                wms.setFromDate(fromDate);
                wms.setToDate(toDate);

                String whId = request.getParameter("whId");
                wms.setWhId(whId);
                String stateId = request.getParameter("stateId");
                wms.setStateId(stateId);
                ArrayList warehouseOrderReport = new ArrayList();
                warehouseOrderReport = wmsBP.warehouseOrderReport(wms);
                request.setAttribute("warehouseOrderReport", warehouseOrderReport);
                path = "content/wms/warehouseOrderReport.jsp";
            } else {
                path = "content/wms/warehouseOrderReport.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to get Hub data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewAsnMaster(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            Path = "content/wms/addAsnRecords.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }
    
     public ModelAndView waveCreation(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            Path = "content/wms/picklist.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }
     public ModelAndView getPickListPrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            Path = "content/wms/picklistPrint.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }
     public ModelAndView barCodePrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            Path = "content/wms/printBarCode.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);

            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareHouseList();
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView updateAnsRecord(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String remark = request.getParameter("remark");
            String Quantity = request.getParameter("Quantity");
            String warehouseId = request.getParameter("warehouseId");
            String itemId = request.getParameter("itemId");
            String vendorId = request.getParameter("vendorId");
            String poDate = request.getParameter("poDate");
            String PoRequestId = request.getParameter("PoRequestId");
            String poNumber = request.getParameter("poNumber");

            System.out.println("remark");
            System.out.println("Quantity" + Quantity);
            System.out.println("remark" + poDate);
            System.out.println("remark" + PoRequestId);

            wmsTO.setRemark(remark);
            wmsTO.setQuantity(Quantity);
            wmsTO.setPoRequestId(PoRequestId);
            wmsTO.setPoNumber(poNumber);
            wmsTO.setVendorId(vendorId);
            wmsTO.setPoDate(poDate);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setItemId(itemId);

            int status = wmsBP.insertAsnDetails(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/addAsnRecords.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "ASN added Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "ASN Not Updated.");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewStockDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String param = request.getParameter("param");
            if ("search".equals(param)) {
                System.out.println("param==" + param);
                String itemId = request.getParameter("itemid");
                String userWhId = request.getParameter("userWhId");
                String itemid = request.getParameter("itemId");
                System.out.println("itemid=====" + itemid);
                request.setAttribute("item1", itemid);
                String Quantity = request.getParameter("Quantity");
                request.setAttribute("Quantity", Quantity);
                String TransferQty = request.getParameter("TransferQty");
                request.setAttribute("TransferQty", TransferQty);
                String item = itemid.split("-")[0];
                String userName = itemid.split("-")[1];
                String userName2 = itemid.split("-")[2];

                ArrayList getSerialNumber = new ArrayList();
                getSerialNumber = wmsBP.getSerialNumber(itemId, userWhId);
                request.setAttribute("getSerialNumber", getSerialNumber);
                System.out.println("getSerialNumberController====" + getSerialNumber.size());
            }
            Path = "content/wms/addStockDetails.jsp";
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            System.out.println("ControllerSizegetList=====" + getItemList.size());
            ArrayList vendorList = new ArrayList();
            vendorList = wmsBP.processVendorList();
            System.out.println("ControllerSize=====" + vendorList.size());
            request.setAttribute("vendorList", vendorList);
            String getUserWarehouse = wmsBP.getUserWarehouse(userId);
            String userWhId = getUserWarehouse.split("-")[0];
            String userWhName = getUserWarehouse.split("-")[1];
            request.setAttribute("userWhId", userWhId);
            request.setAttribute("userWhName", userWhName);
            ArrayList getWareHouseList = new ArrayList();
            getWareHouseList = wmsBP.getWareToHouseList(userWhId);
            System.out.println("getWareHouseList " + getWareHouseList.size());
            request.setAttribute("getWareHouseList", getWareHouseList);
            ArrayList getItemDetails = new ArrayList();
            getItemDetails = wmsBP.processGetItemList(userWhId);
            request.setAttribute("getItemDetails", getItemDetails);

        } catch (FPRuntimeException exception) {
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView saveTransferDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();
            String transfer = request.getParameter("TransferQty");
            System.out.println("transferrr===" + transfer);
            String[] serialNos = request.getParameterValues("serNo");
            for (int i = 0; i < serialNos.length; i++) {

                System.out.println("serNo in controller   " + serialNos[i]);
            }
            System.out.println("seialllno======" + serialNos);
            String[] selectedIndex = request.getParameterValues("selectedIndex");
            for (int i = 0; i < selectedIndex.length; i++) {

                System.out.println("serNo in controller   " + selectedIndex[i]);

            }
            wmsTO.setSelectedIndex(selectedIndex);
            wmsTO.setSerialNos(serialNos);
            String warehouseId = request.getParameter("userWhId");
            String itemid = request.getParameter("itemId");
            String item = itemid.split("-")[0];
            System.out.println("itemId====" + item);
            System.out.println("itemId====" + itemid);
            String warehouseToId = request.getParameter("warehouseToId");

            wmsTO.setQuantity(transfer);
            wmsTO.setWarehouseToId(warehouseToId);
            wmsTO.setWarehouseId(warehouseId);
            wmsTO.setItemId(item);

            int status = wmsBP.insertTransferQty(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/addStockDetails.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "Request Not Updated.");
            }

            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewFindSerialNumber(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String Path = "";
        String menuPath = "GATE IN  >>  Insert ";

        String pageTitle = "GTN Details";
        wmsCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        String custId = "";
        custId = request.getParameter("custId");
        System.out.println("custtt=========" + custId);
        WmsTO wmsTO = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "GTN Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        try {

            String param = request.getParameter("param");
            if ("search".equals(param)) {

                String Serial = request.getParameter("serial");
                request.setAttribute("serialNumber", Serial);

                wmsTO.setSerialNo(Serial);
                System.out.println("Serial=====" + Serial);
                ArrayList serialNumberDetails = new ArrayList();
                serialNumberDetails = wmsBP.serialNumberDetails(wmsTO);
                request.setAttribute("serialNumberDetails", serialNumberDetails);

                Path = "content/wms/findSerialNumber.jsp";
            } else {
                Path = "content/wms/findSerialNumber.jsp";
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(Path);

    }

    public ModelAndView runsheetUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Runsheet Upload";
        menuPath = "wms >>  Runsheet Upload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                int shipmentupload = wmsBP.shipmentUpload(wmsTO, userId);
            } else {
                int status = wmsBP.deleteRunsheetTemp(userId);
            }
//            System.out.println("status11111111111111111111111111===="+shipmentUpload);
            path = "content/wms/runsheetUpload.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveRunsheetUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        String runSheetId = "";
        String trackingId = "";
        String mobileNo = "";
        String name = "";
        String address = "";
        String shipmentType = "";
        String codAmount = "";
        String assignedUser = "";
        String status = "";
        String date = "";
        String amountPaid = "";
        String receiverName = "";
        String relation = "";
        String receiver = "";
        String mobile = "";
        String signature = "";
        String vertical = "";
        String orderNo = "";
        String productDetails = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("88888888");
                                    WmsTO wmsTO = new WmsTO();
                                    wmsTO.setUserId(userId + "");
                                    count++;

                                    runSheetId = s.getCell(1, i).getContents();
                                    System.out.println("99999 " + runSheetId);
                                    wmsTO.setRunSheetId(runSheetId);

                                    trackingId = s.getCell(2, i).getContents();
                                    wmsTO.setTrackingId(trackingId);

                                    mobileNo = s.getCell(3, i).getContents();
                                    wmsTO.setMobileNo(mobileNo);

                                    name = s.getCell(4, i).getContents();
                                    wmsTO.setName(name);

                                    address = s.getCell(5, i).getContents();
                                    wmsTO.setAddress(address);

                                    shipmentType = s.getCell(6, i).getContents();
                                    wmsTO.setShipmentType(shipmentType);

                                    codAmount = s.getCell(7, i).getContents();
                                    wmsTO.setCodAmount(codAmount);

                                    assignedUser = s.getCell(8, i).getContents();
                                    wmsTO.setAssignedUser(assignedUser);

                                    status = s.getCell(9, i).getContents();
                                    wmsTO.setStatus(status);

                                    date = s.getCell(10, i).getContents();
                                    wmsTO.setDate(date);

                                    amountPaid = s.getCell(11, i).getContents();
                                    wmsTO.setAmountPaid(amountPaid);

                                    receiverName = s.getCell(12, i).getContents();
                                    wmsTO.setReceiverName(receiverName);

                                    relation = s.getCell(13, i).getContents();
                                    wmsTO.setRelation(relation);
                                    System.out.println("55555555" + relation);

                                    receiver = s.getCell(14, i).getContents();
                                    wmsTO.setReceiver(receiver);
                                    System.out.println("66666666" + receiver);

                                    mobile = s.getCell(15, i).getContents().toString();
                                    wmsTO.setMobile(mobile);

                                    signature = s.getCell(16, i).getContents().toString();
                                    wmsTO.setSignature(signature);

                                    vertical = s.getCell(17, i).getContents().toString();
                                    wmsTO.setVertical(vertical);

                                    orderNo = s.getCell(18, i).getContents().toString();
                                    wmsTO.setOrderNo(orderNo);

                                    productDetails = s.getCell(19, i).getContents().toString();
                                    wmsTO.setProductDetails(productDetails);

                                    status1 = wmsBP.uploadRunsheet(wmsTO);
                                    System.out.println("status====" + status);

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getRunsheetUpload = new ArrayList();
            getRunsheetUpload = wmsBP.getRunsheetUpload(wms);
            request.setAttribute("getContractList", getRunsheetUpload);
            path = "content/wms/runsheetUpload.jsp";

            if (getRunsheetUpload.size() > 0) {
                System.out.println("status====check================status" + status);
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView ifbOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbOrderUpload";
        menuPath = "wms >>  ifbOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        String whId = (String) session.getAttribute("hubId");
        System.out.println("whId    " + whId);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                wmsTO.setWhId(whId);
                System.out.println("whId    " + whId);
                int ifbOrderUpload = wmsBP.ifbOrderUpload(wmsTO, userId, whId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "LR Updated Successfully");
                int clearData = wmsBP.deleteIfbUploadTemp(userId);
                path = "content/wms/ifbOrderUpload.jsp";
            } else {
                int clearData = wmsBP.deleteIfbUploadTemp(userId);
                path = "content/wms/ifbOrderUpload.jsp";
            }
//            System.out.println("status11111111111111111111111111===="+returnOrderUpload);
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadIfbOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        String billDoc = "";
        String billDate = "";
        String purchaseOrderNumber = "";
        String matlGroup = "";
        String material = "";
        String materialDescription = "";
        String billQty = "";
        String city = "";
        String shipTo = "";
        String partyName = "";
        String gstinNumber = "";
        String grossValue = "";
        String pincode = "";
        String exchange = "";
//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            String hubId = (String) session.getAttribute("hubId");
            userId = (Integer) session.getAttribute("userId");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadedxls/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("11111111111111111111");
                                    WmsTO wmsTO = new WmsTO();
                                    count++;

                                    wmsTO.setUserId(userId + "");
                                    billDoc = s.getCell(0, i).getContents();
                                    System.out.println("AAAAAAAAAAAAAAAA " + billDoc);
                                    wmsTO.setBillDoc(billDoc);

                                    billDate = s.getCell(1, i).getContents();
                                    wmsTO.setBillDate(billDate);

                                    purchaseOrderNumber = s.getCell(2, i).getContents();
                                    wmsTO.setPurchaseOrderNumber(purchaseOrderNumber);

                                    matlGroup = s.getCell(3, i).getContents();
                                    wmsTO.setMatlGroup(matlGroup);

                                    material = s.getCell(4, i).getContents();
                                    wmsTO.setMaterial(material);

                                    materialDescription = s.getCell(5, i).getContents();
                                    wmsTO.setMaterialDescription(materialDescription);

                                    billQty = s.getCell(6, i).getContents();
                                    wmsTO.setBillQty(billQty);

                                    city = s.getCell(7, i).getContents();
                                    wmsTO.setCity(city);

                                    shipTo = s.getCell(8, i).getContents();
                                    wmsTO.setShipTo(shipTo);

                                    partyName = s.getCell(9, i).getContents();
                                    wmsTO.setPartyName(partyName);

                                    gstinNumber = s.getCell(10, i).getContents();
                                    wmsTO.setGstinNumber(gstinNumber);

                                    grossValue = s.getCell(11, i).getContents();
                                    wmsTO.setGrossValue(grossValue.replace(",", ""));

                                    pincode = s.getCell(12, i).getContents();
                                    wmsTO.setPincode(pincode);
                                    System.out.println("vvvvvvvvvvvvv" + pincode);

                                    exchange = s.getCell(13, i).getContents();
                                    wmsTO.setExchange(exchange);

                                    wmsTO.setWhId(hubId);
                                    status1 = wmsBP.uploadIfbOrder(wmsTO);
                                    System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "IFB order updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            wms.setUserId(userId + "");
            ArrayList getIFBList = new ArrayList();
            getIFBList = wmsBP.getIFBList(wms);
            request.setAttribute("getContractList", getIFBList);
            path = "content/wms/ifbOrderUpload.jsp";

            if (getIFBList.size() > 0) {
                System.out.println("getContractList====check================status" + getIFBList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "IFB Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView ifbLRUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {
            String invoiceNo = "";
            String[] lrId = request.getParameterValues("lrId");
            String[] ewayBillNo = request.getParameterValues("ewayBillNoTemp");
            String[] ewayExpiry = request.getParameterValues("ewayBillExpiryTemp");
            String[] selected = request.getParameterValues("selectedStatus");
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                saveIfbLr = wmsBP.saveIfbLr(lrId, ewayBillNo, ewayExpiry, selected);
            }
            ArrayList ifbLRList = wmsBP.ifbLRList(wmsTO);
            request.setAttribute("ifbLRList", ifbLRList);
            path = "content/wms/ifbLRUpdate.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbRunsheet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Ifb Runsheet";
        menuPath = "wms >>  Ifb Runsheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {

            String param = request.getParameter("param");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }
            wmsTO.setUserId(userId + "");

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if ("Excel".equals(param)) {
                System.out.println("param==" + param);
                path = "content/wms/ifbRunsheetExcel.jsp";
            } else {
                System.out.println("param==" + param);
                path = "content/wms/ifbRunsheet.jsp";
            }
            ArrayList getIfbRunsheetList = new ArrayList();
            getIfbRunsheetList = wmsBP.getIfbRunsheetList(wmsTO);
            System.out.println("here in controller");
            request.setAttribute("ifbRunsheetList", getIfbRunsheetList);
            int TripSheetSize = getIfbRunsheetList.size();
            request.setAttribute("runsheetSize", TripSheetSize);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView flipkartRunsheet(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "Flipkart Runsheet";
        menuPath = "wms >>  Flipkart Runsheet";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {

            String param = request.getParameter("param");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date curDate = new Date();
            System.out.println(dateFormat.format(curDate));

            String fromDate = "";
            String toDate = "";

            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");

            if (fromDate != null) {
                request.setAttribute("fromDate", fromDate);
                wmsTO.setFromDate(fromDate);
            } else {
                fromDate = "";
            }

            if (toDate != null) {
                wmsTO.setToDate(toDate);
                request.setAttribute("toDate", toDate);
            } else {
                toDate = "";
            }
            wmsTO.setUserId(userId + "");

            System.out.println(dateFormat.format(curDate));
            String endDate = dateFormat.format(curDate);
            String startDate = "";
            String[] temp = null;
            if (!"".equals(endDate)) {
                temp = endDate.split("-");
                startDate = "1" + "-" + temp[1] + "-" + temp[2];
            }
            if ("".equals(fromDate)) {
                fromDate = startDate;
                wmsTO.setFromDate(fromDate);
            }
            if ("".equals(toDate)) {
                toDate = endDate;
                wmsTO.setToDate(toDate);
            }

            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if ("Excel".equals(param)) {
                System.out.println("param==" + param);
                path = "content/wms/fkRunsheetExcel.jsp";
            } else {
                System.out.println("param==" + param);
                path = "content/wms/fkRunsheet.jsp";
            }
            ArrayList getFkRunsheetList = new ArrayList();
            getFkRunsheetList = wmsBP.getFkRunsheetList(wmsTO);
            System.out.println("here in controller");
            request.setAttribute("fkRunsheetList", getFkRunsheetList);
            int TripSheetSize = getFkRunsheetList.size();
            request.setAttribute("runsheetSize", TripSheetSize);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView skuUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        ArrayList getwmsItemReport = new ArrayList();
        ArrayList getPickDetails = new ArrayList();
        String param = "";
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;

        PrintWriter pw = response.getWriter();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String action = "";
        String pageTitle = "Picklist Master";
        menuPath = "Masters  >> View Picklist Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            getwmsItemReport = wmsBP.getwmsItemReport(wmsTO);
            request.setAttribute("picklistMaster", getwmsItemReport);
            param = request.getParameter("param");
            path = "content/wms/skuupdate.jsp";

            if (getwmsItemReport.size() == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No records found");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView skuViewUpdate(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws IOException {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }

        ArrayList getwmsItemReport = new ArrayList();
        ArrayList getPickDetails = new ArrayList();
        String param = "";
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;

        PrintWriter pw = response.getWriter();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String pageTitle = "Picklist Master";
        menuPath = "Masters  >> View Picklist Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String itemId = "";
            itemId = request.getParameter("itemId");
            request.setAttribute("itemId", itemId);
            wmsTO.setItemId(itemId);
            ArrayList getItemList = new ArrayList();
            getItemList = wmsBP.getItemLists(userId);
            request.setAttribute("getItemList", getItemList);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            getwmsItemReport = wmsBP.getwmsItemReportView(wmsTO);
            request.setAttribute("picklistMaster", getwmsItemReport);
            param = request.getParameter("param");
            path = "content/wms/skuupdateview.jsp";

            if (getwmsItemReport.size() == 0) {
                request.setAttribute(ParveenErrorConstants.ERROR_KEY, "No records found");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateSku(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();

        String menuPath = "Operation  >>  View TripSheet ";
        String path = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            WmsTO wmsTO = new WmsTO();

            String itemId = request.getParameter("itemId");
            System.out.println("itemId");
            wmsTO.setItemId(itemId);

            String serialNumber = request.getParameter("serialNumber");
            System.out.println("serialNumber");
            wmsTO.setSerialNumber(serialNumber);

            int status = wmsBP.UpdateSku(wmsTO);
            System.out.println("status :" + status);
            path = "content/wms/skuupdateview.jsp";
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            } else {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Request added Successfully.");
            }
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save trip sheet data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubOut(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String menuPath = "";
        String param = "";
        String pageTitle = "ifbHubOut";
        menuPath = "Masters  >> ifbHubOut ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            param = request.getParameter("param");
            String[] orderIds = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            String[] hubId = request.getParameterValues("hubId");
            String[] ewayBillNo = request.getParameterValues("ewayBillNoTemp");
            String[] ewayExpiry = request.getParameterValues("ewayBillExpiryTemp");
            String vehicleNo = request.getParameter("vehicleNo");
            String driverName = request.getParameter("driverName");
            String driverMobile = request.getParameter("driverMobile");
            int updateIfbHubout = 0;
            Date date = new Date();
            String str = new SimpleDateFormat("yyyy-MM-dd").format(date);
            String date1 = str.replace("-", "");
            if ("update".equals(param)) {
                System.out.println("reached hub out update   " + orderIds.length);
                updateIfbHubout = wmsBP.updateIfbHubout(ewayBillNo, ewayExpiry, orderIds, selectedStatus, hubId, vehicleNo, driverName, driverMobile, date1);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub out done Successfully.");
                path = "content/wms/ifbHubOut.jsp";
            } else if ("serial".equals(param)) {
                String lrId = request.getParameter("lrId");
                ArrayList ifbDetails = wmsBP.getIfbOrderDetails(lrId);
                request.setAttribute("ifbDetails", ifbDetails);
                path = "content/wms/ifbOrderDetails.jsp";
            } else {
                path = "content/wms/ifbHubOut.jsp";
            }
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList hubOutList = wmsBP.hubOutList(wmsTO);
            request.setAttribute("hubOutList", hubOutList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to addPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubOutView(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        WmsTO wmsTO = new WmsTO();
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        String companyId = (String) session.getAttribute("companyId");
        String menuPath = "";
        String param = "";
        String pageTitle = "ifbHubOutView";
        menuPath = "Masters  >> ifbHubOutView ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            param = request.getParameter("param");
            if ("print".equals(param)) {
                String lrId = request.getParameter("lrId");
                request.setAttribute("lrId", lrId);
                wmsTO.setLrId(lrId);
                request.setAttribute("vehicleNo", request.getParameter("vehicleNo"));
                request.setAttribute("driverName", request.getParameter("driverName"));
                request.setAttribute("driverMobile", request.getParameter("driverMobile"));
                ArrayList hubOutPrintDetails = wmsBP.hubOutPrintDetails(lrId, companyId);
                WmsTO wmsTO1 = new WmsTO();
                Iterator itr = hubOutPrintDetails.iterator();
                while (itr.hasNext()) {
                    wmsTO1 = (WmsTO) itr.next();
                    request.setAttribute("fromWhName", (wmsTO1.getCurrentHub()).split("~")[0]);
                    request.setAttribute("fromWhAddress", (wmsTO1.getCurrentHub()).split("~")[1]);
                    request.setAttribute("fromWhPhone", (wmsTO1.getCurrentHub()).split("~")[2]);
                    request.setAttribute("fromWhGst", (wmsTO1.getCurrentHub()).split("~")[3]);
                    request.setAttribute("toWhName", (wmsTO1.getDeliveryHub()).split("~")[0]);
                    request.setAttribute("toWhAddress", (wmsTO1.getDeliveryHub()).split("~")[1]);
                    request.setAttribute("toWhPhone", (wmsTO1.getDeliveryHub()).split("~")[2]);
                    request.setAttribute("toWhGst", (wmsTO1.getDeliveryHub()).split("~")[3]);
                    request.setAttribute("date", wmsTO1.getCreatedDate());
                    request.setAttribute("dispatchId", wmsTO1.getDispatchId());
                }
                path = "content/wms/hubOutPrint.jsp";
            } else {
                path = "content/wms/ifbHubOutView.jsp";
            }
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList hubOutList = wmsBP.hubOutListView(wmsTO);
            request.setAttribute("hubOutList", hubOutList);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to addPincodeMaster --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        }
        return new ModelAndView(path);
    }

    public ModelAndView ifbHubIn(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int updateHubIn = 0;
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            String[] orderIds = request.getParameterValues("orderId");
            String[] selectedStatus = request.getParameterValues("selectedStatus");
            if ("update".equals(param)) {
                updateHubIn = wmsBP.updateHubIn(orderIds, selectedStatus);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub In done Successfully.");
            }

            ArrayList ifbHubinList = wmsBP.ifbHubinList(wmsTO);
            request.setAttribute("ifbHubinList", ifbHubinList);
            path = "content/wms/ifbHubIn.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView normalOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "normalOrderApproval";
        menuPath = "wms >>  normalOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            int status = wmsBP.deleteNormalOrderTemp(userId);
            path = "content/wms/normalOrderApproval.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView saveNormalOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String shipmentId = "";
        String customerName = "";
        String city = "";
        String pinCode = "";
        String deliveryHub = "";
        String assignedExecutive = "";
        String origin = "";
        String currentHub = "";
        String status = "";
        String note = "";
        String amount = "";
        String weight = "";
        String lastModified = "";
        String lastReceived = "";
        String shipmentType = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("11111111111111111111111111");
                                    WmsTO wmsTO = new WmsTO();
                                    count++;

                                    shipmentId = s.getCell(0, i).getContents();
                                    System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                    wmsTO.setShipmentId(shipmentId);

                                    customerName = s.getCell(1, i).getContents();
                                    wmsTO.setCustomerName(customerName);

                                    city = s.getCell(2, i).getContents();
                                    wmsTO.setCity(city);

                                    pinCode = s.getCell(3, i).getContents();
                                    wmsTO.setPinCode(pinCode);

                                    deliveryHub = s.getCell(4, i).getContents();
                                    wmsTO.setDeliveryHub(deliveryHub);

                                    assignedExecutive = s.getCell(5, i).getContents();
                                    wmsTO.setAssignedExecutive(assignedExecutive);

                                    origin = s.getCell(6, i).getContents();
                                    wmsTO.setOrigin(origin);

                                    currentHub = s.getCell(7, i).getContents();
                                    wmsTO.setCurrentHub(currentHub);

                                    status = s.getCell(8, i).getContents();
                                    wmsTO.setStatus(status);

                                    note = s.getCell(9, i).getContents();
                                    wmsTO.setNote(note);

                                    amount = s.getCell(10, i).getContents();
                                    wmsTO.setAmount(amount);

                                    weight = s.getCell(11, i).getContents();
                                    wmsTO.setWeight(weight);

                                    lastModified = s.getCell(12, i).getContents();
                                    wmsTO.setLastModified(lastModified);
                                    System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvv" + lastModified);

                                    lastReceived = s.getCell(13, i).getContents();
                                    wmsTO.setLastReceived(lastReceived);
                                    System.out.println("mmmmmmmmmmmmmmmmmmmmmmm" + lastReceived);

                                    shipmentType = s.getCell(14, i).getContents().toString();
                                    wmsTO.setShipmentType(shipmentType);

                                    custContractUpload.add(wmsTO);

                                    status1 = wmsBP.uploadCustomerContract(wmsTO);
                                    System.out.println("status====" + status);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getContractList = new ArrayList();
            getContractList = wmsBP.getCustomerContract(wms);
            request.setAttribute("getContractList", getContractList);
            path = "content/wms/normalOrderApproval.jsp";

            if (getContractList.size() > 0) {
                System.out.println("status====check================status" + status);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView SaveOrderApproval(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderApproval";
        menuPath = "wms >>  SaveOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            System.out.println("hubId" + hubId);
            wmsTO.setWhId(hubId);
            int status = wmsBP.SaveOrderApproval(wmsTO, userId);
            System.out.println("status====" + status);
            path = "content/wms/normalOrderApproval.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView replacementOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "replacementOrderUpload";
        menuPath = "wms >>  replacementOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String param = request.getParameter("param");
            int status = wmsBP.deleteReplacementOrderTemp(userId);
//            System.out.println("status11111111111111111111111111===="+replacementOrderUpload);
            path = "content/wms/replacementOrderUpload.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView uploadReplacementOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String shipmentId = "";
        String shipmentType = "";
        String shipmentTierType = "";
        String reverseShipmentId = "";
        String bagStatus = "";
        String type = "";
        String price = "";
        String latestStatus = "";
        String deliveryHub = "";
        String currentLocation = "";
        String latestUpdateDateTime = "";
        String firstReceivedHub = "";
        String firstReceiveDateTime = "";
        String hubNotes = "";
        String csNotes = "";
        String numberofAttempts = "";
        String bagId = "";
        String consignmentId = "";
        String customerPromiseDate = "";
        String logisticsPromiseDate = "";
        String onHoldByOpsReason = "";
        String onHoldByOpsDate = "";
        String firstAssignedHubName = "";
        String deliveryPinCode = "";
        String lastReceivedDateTime = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("11111111111111111111111111");
                                    WmsTO wmsTO = new WmsTO();
                                    count++;

                                    shipmentId = s.getCell(0, i).getContents();
                                    System.out.println("AAAAAAAAAAAAAAAA " + shipmentId);
                                    wmsTO.setShipmentId(shipmentId);

                                    shipmentType = s.getCell(1, i).getContents();
                                    wmsTO.setShipmentType(shipmentType);

                                    shipmentTierType = s.getCell(2, i).getContents();
                                    wmsTO.setShipmentTierType(shipmentTierType);

                                    reverseShipmentId = s.getCell(3, i).getContents();
                                    wmsTO.setReverseShipmentId(reverseShipmentId);

                                    bagStatus = s.getCell(4, i).getContents();
                                    wmsTO.setBagStatus(bagStatus);

                                    type = s.getCell(5, i).getContents();
                                    wmsTO.setType(type);

                                    price = s.getCell(6, i).getContents();
                                    wmsTO.setPrice(price);

                                    latestStatus = s.getCell(7, i).getContents();
                                    wmsTO.setLatestStatus(latestStatus);

                                    deliveryHub = s.getCell(8, i).getContents();
                                    wmsTO.setDeliveryHub(deliveryHub);

                                    currentLocation = s.getCell(9, i).getContents();
                                    wmsTO.setCurrentLocation(currentLocation);

                                    latestUpdateDateTime = s.getCell(10, i).getContents();
                                    wmsTO.setLatestUpdateDateTime(latestUpdateDateTime);

                                    firstReceivedHub = s.getCell(11, i).getContents();
                                    wmsTO.setFirstReceivedHub(firstReceivedHub);

                                    firstReceiveDateTime = s.getCell(12, i).getContents();
                                    wmsTO.setFirstReceiveDateTime(firstReceiveDateTime);
                                    System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvv" + firstReceiveDateTime);

                                    hubNotes = s.getCell(13, i).getContents();
                                    wmsTO.setHubNotes(hubNotes);
                                    System.out.println("mmmmmmmmmmmmmmmmmmmmmmm" + hubNotes);

                                    csNotes = s.getCell(14, i).getContents().toString();
                                    wmsTO.setCsNotes(csNotes);

                                    numberofAttempts = s.getCell(15, i).getContents().toString();
                                    wmsTO.setNumberofAttempts(numberofAttempts);

                                    bagId = s.getCell(16, i).getContents().toString();
                                    wmsTO.setBagId(bagId);

                                    consignmentId = s.getCell(17, i).getContents().toString();
                                    wmsTO.setConsignmentId(consignmentId);

                                    customerPromiseDate = s.getCell(18, i).getContents().toString();
                                    wmsTO.setCustomerPromiseDate(customerPromiseDate);

                                    logisticsPromiseDate = s.getCell(19, i).getContents().toString();
                                    wmsTO.setLogisticsPromiseDate(logisticsPromiseDate);

                                    onHoldByOpsReason = s.getCell(20, i).getContents().toString();
                                    wmsTO.setOnHoldByOpsReason(onHoldByOpsReason);

                                    onHoldByOpsDate = s.getCell(21, i).getContents().toString();
                                    wmsTO.setOnHoldByOpsDate(onHoldByOpsDate);

                                    firstAssignedHubName = s.getCell(22, i).getContents().toString();
                                    wmsTO.setFirstAssignedHubName(firstAssignedHubName);

                                    deliveryPinCode = s.getCell(23, i).getContents().toString();
                                    wmsTO.setDeliveryPinCode(deliveryPinCode);

                                    lastReceivedDateTime = s.getCell(24, i).getContents().toString();
                                    wmsTO.setLastReceivedDateTime(lastReceivedDateTime);

                                    custContractUpload.add(wmsTO);

                                    status1 = wmsBP.uploadReplacementOrder(wmsTO);
                                    System.out.println("status====" + status1);
//                                    getContractUpload = wmsBP.getContractUpload(wmsTO);

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getreplacementList = new ArrayList();
            getreplacementList = wmsBP.getreplacementList(wms);
            request.setAttribute("getreplacementList", getreplacementList);
            path = "content/wms/replacementOrderUpload.jsp";

            if (getreplacementList.size() > 0) {
                System.out.println("getContractList====check================status" + getreplacementList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView replacementContractUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderUpload";
        menuPath = "wms >>  SaveOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            int status = wmsBP.replacementContractUpload(wmsTO, userId);
            System.out.println("status====" + status);
            path = "content/wms/replacementOrderUpload.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView returnOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "returnOrderUpload";
        menuPath = "wms >>  returnOrderUpload";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            int status = wmsBP.deleteReturnOrderTemp(userId);
            String param = request.getParameter("param");
//            System.out.println("status11111111111111111111111111===="+returnOrderUpload);

            path = "content/wms/returnOrderUpload.jsp";
//            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "shipment Uploaded Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView savereturnOrder(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {

        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        ModelAndView mv = null;
//        FileInputStream fis = null;
        HttpSession session = null;
        String menuPath = "";
        String path = "";
        String newFileName = "", tempFilePath = "", actualFilePath = "";
        String uploadedFileName = "", tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "", fileFormat = "";
        int userId = 0;
        wmsCommand = command;
        String pageTitle = "Upload Zone List";
//        menuPath = "Masters >> Upload Zone List";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, menuPath);
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;

        ArrayList getContractUpload = new ArrayList();
        ArrayList custContractUpload = new ArrayList();
        String trackingId = "";
        String pickupaddress = "";
        String pinCode = "";
        String itemName = "";
        String itemNos = "";
        String createdDate = "";
        String pickupDate = "";
        String Type = "";
        String lastStatus = "";
        String remarks = "";
        String numberofAttempts = "";

//        int status = 0;
        int status1 = 0;
        Map map = new HashMap();
        HashSet hs = new HashSet();
        try {
            Object mapValue = null;
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            String custId = request.getParameter("custId");
            String contractId = request.getParameter("contractId");
            System.out.println("custId----" + custId);
            System.out.println("contractId----" + contractId);
            request.setAttribute("custId", custId);
            request.setAttribute("contractId", contractId);
            isMultipart = ServletFileUpload.isMultipartContent(request);
            System.out.println("part Name:" + isMultipart);

//            int deleteUploadTemp = wBP.deleteUploadTemp();
//            System.out.println("deleteUploadTemp----"+deleteUploadTemp);
            if (isMultipart) {

                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                        System.out.println("Server Path == " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        System.out.println("Server Path After Replace== " + tempServerFilePath);
                        fPart = (FilePart) partObj;
                        uploadedFileName = fPart.getFileName();
                        System.out.println("uploadedFileName====" + uploadedFileName);
//                        String[] temp = null;
//                        temp = uploadedFileName.split(".");
//                        String fileFormat = temp[1];
                        String[] splitFileNames = uploadedFileName.split("\\.");
                        fileFormat = splitFileNames[1];
                        System.out.println("fileFormat==" + fileFormat);
                        if ("xls".equals(fileFormat)) {
                            System.out.println("Ses");
                            if (!"".equals(uploadedFileName) && uploadedFileName != null) {
                                String[] splitFileName = uploadedFileName.split("\\.");
                                fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                                System.out.println("splitFileName[0]=" + splitFileName[0]);
                                System.out.println("splitFileName[0]=" + splitFileName[1]);

                                fileName = fileSavedAs;
                                tempFilePath = tempServerFilePath + "/" + fileSavedAs;
                                actualFilePath = actualServerFilePath + "/" + uploadedFileName;
                                System.out.println("tempPath..." + tempFilePath);
                                System.out.println("actPath..." + actualFilePath);
                                long fileSize = fPart.writeTo(new java.io.File(actualFilePath));
                                System.out.println("fileSize..." + fileSize);
                                File f1 = new File(actualFilePath);
                                System.out.println("check " + f1.isFile());
                                f1.renameTo(new File(tempFilePath));
                                System.out.println("tempPath = " + tempFilePath);
//                                System.out.println("actPath = " + actualFilePath);
//                                String parts = actualFilePath.substring(actualFilePath.lastIndexOf("//"));
//                                String part1 = parts.replace("//", "");
                                System.out.println("11111111111111111111111111");

                                int count = 0;
                                WorkbookSettings ws = new WorkbookSettings();
                                ws.setLocale(new Locale("en", "EN"));
                                Workbook workbook = Workbook.getWorkbook(new File(actualFilePath), ws);
                                Sheet s = workbook.getSheet(0);
                                System.out.println("rows" + userId + s.getRows());

                                for (int i = 1; i < s.getRows(); i++) {
                                    System.out.println("11111111111111111111111111");
                                    WmsTO wmsTO = new WmsTO();
                                    count++;

                                    trackingId = s.getCell(0, i).getContents();
                                    System.out.println("AAAAAAAAAAAAAAAA " + trackingId);
                                    wmsTO.setTrackingId(trackingId);

                                    pickupaddress = s.getCell(1, i).getContents();
                                    wmsTO.setPickupaddress(pickupaddress);

                                    pinCode = s.getCell(2, i).getContents();
                                    wmsTO.setPinCode(pinCode);

                                    itemName = s.getCell(3, i).getContents();
                                    wmsTO.setItemName(itemName);

                                    itemNos = s.getCell(4, i).getContents();
                                    wmsTO.setItemNos(itemNos);

                                    createdDate = s.getCell(5, i).getContents();
                                    wmsTO.setCreatedDate(createdDate);

                                    pickupDate = s.getCell(6, i).getContents();
                                    wmsTO.setPickupDate(pickupDate);

                                    Type = s.getCell(7, i).getContents();
                                    wmsTO.setType(Type);

                                    lastStatus = s.getCell(8, i).getContents();
                                    wmsTO.setLastStatus(lastStatus);

                                    remarks = s.getCell(9, i).getContents();
                                    wmsTO.setRemarks(remarks);

                                    numberofAttempts = s.getCell(10, i).getContents();
                                    wmsTO.setNumberofAttempts(numberofAttempts);

                                    custContractUpload.add(wmsTO);

                                    status1 = wmsBP.uploadReturnordertemp(wmsTO);
                                    System.out.println("status====" + status1);

                                }

                            }
                        } else {
                            request.setAttribute("errorMessage", "Contract Updated Failed:");
                        }
                    }
                }
            }
            WmsTO wms = new WmsTO();
            ArrayList getreplacementList = new ArrayList();
            getreplacementList = wmsBP.getreturnList(wms);
            request.setAttribute("getreplacementList", getreplacementList);
            path = "content/wms/returnOrderUpload.jsp";

            if (getreplacementList.size() > 0) {
                System.out.println("getContractList====check================status" + getreplacementList);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Contract Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }

        return new ModelAndView(path);

    }

    public ModelAndView saveReturnOrderUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "SaveOrderApproval";
        menuPath = "wms >>  SaveOrderApproval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        try {
            String hubId = (String) session.getAttribute("hubId");
            wmsTO.setWhId(hubId);
            int status = wmsBP.returnContractUpload(wmsTO, userId);
            System.out.println("status====" + status);

            path = "content/wms/returnOrderUpload.jsp";
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Order Approval updated Successfully");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView ifbLrUpload(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {
            String fromDate = request.getParameter("fromDate");
            String invoiceNo = "";
            String param = request.getParameter("param");
            wmsTO.setFromDate(fromDate + "");
            ArrayList ifbLRUploadList = wmsBP.ifbLRUploadList(wmsTO);
            request.setAttribute("ifbLRUploadList", ifbLRUploadList);
            path = "content/wms/ifbLrUpload.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }
    
    public ModelAndView packingList(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "packingList";
        menuPath = "wms >>  packingList";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        wmsTO.setUserId(userId + "");
        try {
            String param = request.getParameter("param");
            path = "content/wms/packingList.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView packingVas(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Inbound  >>  View ";
        String pageTitle = "Unloading Update Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Packing List Vas";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
            }
            path = "content/wms/packingVas.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView deliveryNote(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Outbound  >>  View ";
        String pageTitle = "Dispatch Delivery Note";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Dispatch Delivery Note";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
            }
            path = "content/wms/dispatchDeliveryNote.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView dispatchDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Outbound  >>  View ";
        String pageTitle = "Dispatch Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Dispatch Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
            }
            path = "content/wms/dispatchDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView updateDispatchDetails(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Outbound  >>  View ";
        String pageTitle = "Update Dispatch Details";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Update Dispatch Details";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            if ("save".equals(param)) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully.");
            }
            path = "content/wms/updateDispatchDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView getDeliveryNotePrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) throws FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        HttpSession session = request.getSession();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String path = "";
        String menuPath = "Outbound  >>  View ";
        String pageTitle = "Dispatch Delivery Note";
        int userId = (Integer) session.getAttribute("userId");
        WmsTO wmsTo = new WmsTO();
        request.setAttribute("pageTitle", pageTitle);
        pageTitle = "Dispatch Delivery Note";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            String param = request.getParameter("param");
            path = "content/wms/dispatchDeliveryNotePrint.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve ASN data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView ifbLrPrint(HttpServletRequest request, HttpServletResponse response, WmsCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        wmsCommand = command;
        String menuPath = "";
        WmsTO wmsTO = new WmsTO();
        String pageTitle = "ifbLRUpdate";
        menuPath = "wms >>  ifbLRUpdate";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
//         int ewayBillNo = (Integer) session.getAttribute("ewayBillNoTemp");
//         System.out.println("ewayBillNo"+ ewayBillNo);
        int saveIfbLr = 0;
        wmsTO.setUserId(userId + "");
        try {
            String lrId = request.getParameter("lrId");

            ArrayList ifbLRPrintlist = wmsBP.ifbLRPrintlist(lrId);
            WmsTO wmsTO1 = new WmsTO();
            Iterator itr = ifbLRPrintlist.iterator();
            while (itr.hasNext()) {
                wmsTO1 = (WmsTO) itr.next();
                request.setAttribute("customerName", wmsTO1.getCustomerName());
                request.setAttribute("pincode", wmsTO1.getPincode());
                request.setAttribute("gstNo", wmsTO1.getGstinNumber());
                request.setAttribute("phoneNo", wmsTO1.getMobileNo());
                request.setAttribute("city", wmsTO1.getCity());
                request.setAttribute("custName", wmsTO1.getCustName().split("~")[0]);
                request.setAttribute("custAddress", wmsTO1.getCustName().split("~")[1]);
                request.setAttribute("custCity", wmsTO1.getCustName().split("~")[2]);
                request.setAttribute("custPincode", wmsTO1.getCustName().split("~")[3]);
                request.setAttribute("custPhone", wmsTO1.getCustName().split("~")[4]);
                request.setAttribute("ewayBillNo", wmsTO1.getEwayBillNo());
                request.setAttribute("ewayExpiry", wmsTO1.getEwayExpiry());
                request.setAttribute("lrNo", wmsTO1.getLrNo());
                request.setAttribute("invoiceNo", wmsTO1.getInvoiceNo());
                request.setAttribute("grossValue", wmsTO1.getGrossValue());
                request.setAttribute("qty", wmsTO1.getQty());
            }
            request.setAttribute("ifbLRPrintlist", ifbLRPrintlist);
            path = "content/wms/ifbLrPrint.jsp";

//            ArrayList ifbLRList = wmsBP.ifbLRList(wmsTO);
//            request.setAttribute("ifbLRList", ifbLRList);
//            path = "content/wms/ifbLrPrint.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
}
