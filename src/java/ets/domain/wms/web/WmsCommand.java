package ets.domain.wms.web;

/**
 *
 * @author vidya
 */
public class WmsCommand {

    public WmsCommand() {
    }
    private String stateId = "";

    private String serialNos[] = null;
    private String serialNo = "";
    private String itemId = "";
    private String asnId = "";
    private String proCon="";
    private String ipQty="";
    private String uom="";
    private String invoiceNo="";
    private String invoiceDate="";
    private String gtnDate="";
    private String gtnTime="";
    private String custId="";
    private String dock="";
    private String vehicleType="";
    private String vehicleNo="";
    private String driverName="";
    private String remarks="";

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getGtnDate() {
        return gtnDate;
    }

    public void setGtnDate(String gtnDate) {
        this.gtnDate = gtnDate;
    }

    public String getGtnTime() {
        return gtnTime;
    }

    public void setGtnTime(String gtnTime) {
        this.gtnTime = gtnTime;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getDock() {
        return dock;
    }

    public void setDock(String dock) {
        this.dock = dock;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    
    public String getProCon() {
        return proCon;
    }

    public void setProCon(String proCon) {
        this.proCon = proCon;
    }

    public String getIpQty() {
        return ipQty;
    }

    public void setIpQty(String ipQty) {
        this.ipQty = ipQty;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String[] getSerialNos() {
        return serialNos;
    }

    public void setSerialNos(String[] serialNos) {
        this.serialNos = serialNos;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getAsnId() {
        return asnId;
    }

    public void setAsnId(String asnId) {
        this.asnId = asnId;
    }

}
