/*---------------------------------------------------------------------------
 * ContractBP.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
-------------------------------------------------------------------------*/

package ets.domain.contract.web;

/****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver              Date                      Author                    Change
 * ---------------------------------------------------------------------------
 * 1.0           Mar 3, 2009              vijay			       Created
 *
 ******************************************************************************/
public class ContractCommand {
    
            private String custId="";
            private String sdate="";
            private String edate="";
            private String []mfrIds=null;
            private String []modIds=null;
            private String []sparePers=null;
            private String []labourPers=null;
            private String[] activeInds=null;
            private String []selectedIndex=null;
            private String mfrId="";

    public String[] getSparePers() {
        return sparePers;
    }

    public void setSparePers(String[] sparePers) {
        this.sparePers = sparePers;
    }
            

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }
            

    public String[] getActiveInds() {
        return activeInds;
    }

    public void setActiveInds(String[] activeInds) {
        this.activeInds = activeInds;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

   
    public String[] getLabourPers() {
        return labourPers;
    }

    public void setLabourPers(String[] labourPers) {
        this.labourPers = labourPers;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getModIds() {
        return modIds;
    }

    public void setModIds(String[] modIds) {
        this.modIds = modIds;
    }


   
    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }
     
            

}
