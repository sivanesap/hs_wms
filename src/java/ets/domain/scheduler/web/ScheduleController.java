/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.sendInvoice;
import ets.arch.util.sendService;
import ets.arch.util.sendVendorInvoice;
import ets.arch.web.BaseController;
import ets.domain.company.business.CompanyTO;

import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.business.SchedulerBP;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.thread.web.SendEmailDetails;
import ets.domain.thread.web.SendSMS;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.trip.business.TripTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
//import throttlegps.gpsActivity;
//import throttlegps.main.GpsLocationMain;

//excel
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 *
 * @author Arun
 */
public class ScheduleController extends BaseController {

    LoginBP loginBP;
    SchedulerBP schedulerBP;
    ReportBP reportBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SchedulerBP getSchedulerBP() {
        return schedulerBP;
    }

    public void setSchedulerBP(SchedulerBP schedulerBP) {
        this.schedulerBP = schedulerBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        System.out.println("executeScheduler : request.getRequestURI() = " + request.getRequestURI());

    }

    public static String applicationServer;
    public static String serverIpAddress;
    int runHour = 0;
    int runMinute = 0;

    public void executeScheduler() throws FPRuntimeException, FPBusinessException {
        ReportTO reportTO = new ReportTO();
        ReportTO reportTO1 = new ReportTO();

        try {

            int smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);

            if (smsSchedulerStatus == 1) {

                ThrottleConstants.PROPS_VALUES = loadProps();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar c = Calendar.getInstance();
                Date date = new Date();
                c.setTime(date);
                String systemTime = sdf.format(c.getTime()).toString();
                String[] dateTemp = systemTime.split(" ");
                String[] timeTemp = dateTemp[1].split(":");

                runHour = Integer.parseInt(timeTemp[0]);
                runMinute = Integer.parseInt(timeTemp[1]);

                String statusAPI = ThrottleConstants.statusAPI;
                String podAPI = ThrottleConstants.podAPI;

                String[] statusTemp = statusAPI.split(":");
                String[] podTemp = podAPI.split(":");

                System.out.println("runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);

                System.out.println("syshour:" + statusTemp[0]);
                System.out.println("syshour:" + statusTemp[1]);

                //////////////////////////////////////// STATUS UPDATE /////////////////////////////////////////////
                
//                if (runMinute % 2 == 0) {                    
//                    System.out.println("INV API Status Update Details : start");
//                    int invAPI = reportBP.getAPIInvoiceStatus();
//                    System.out.println("INV API Status Update Details : End = " + invAPI);                    
//                    
//                    System.out.println("Image API Call : start");
//                    int imgOrderAPI = reportBP.getOrderImageAPICall();
//                    System.out.println("Image API Call : end "+imgOrderAPI);
//                    
//                    System.out.println("Image API Call : start");
//                    int imgAPI = reportBP.getGRNImageAPICall();
//                    System.out.println("Image API Call : end "+imgAPI);
//                }
//   
//                if (runMinute % 3 == 0) {
//                    System.out.println("OFD API Status Update Details : start");
//                    int ofd = reportBP.getOFDAPIDetails();
//                    System.out.println("OFD API Status Update Details : End = " + ofd);
//                    
//                    System.out.println("Stock API Stock Update Details : start");
//                    int updates = reportBP.getStockAPIDetails();
//                    System.out.println("Stock API STK Update Details : End = " + updates);
//                    
//                    System.out.println("One Time Stock API Stock Update Details : start");
//                    updates = reportBP.getOneTimeStockAPIDetails();
//                    System.out.println("One Time Stock API STK Update Details : End = " + updates);
//
//                }
//
//                if (runMinute % 5 == 0) {
//                    
//                    reportTO = new ReportTO();
//                    System.out.println("Delivery API delivery Update Details : start");
//                    int update = reportBP.getAPIStatusDetails();
//                    System.out.println("Delivery API delivery Update Details : End = " + update);
//                }
                if (runHour == 11 && runMinute == 16) {

                    String content = "";
                    String[] tempContent = null;
                    ArrayList whList = new ArrayList();
                    ReportTO repTO = new ReportTO();

                    whList = reportBP.getWarehouseList();
                    System.out.println("Wh custList : " + whList.size());
                    Iterator itr = whList.iterator();

                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        repTO.setWhId(repTO.getWhId());
                        repTO.setWhName(repTO.getWhName());
                        repTO.setStateId(repTO.getStateId());
                        content = reportBP.dailyMailReport(repTO);
                        System.out.println("DSR content = " + content);

                        if (!"".equals(content)) {

                            tempContent = content.split("~");

                            reportTO.setMailSubjectTo("Daily Report Excel");
                            reportTO.setMailSubjectCc("Daily Report Excel");

                            reportTO.setMailTypeId("2");
                            reportTO.setMailContentTo(tempContent[0]);
                            reportTO.setMailContentCc(tempContent[0]);
//                            reportTO.setMailIdTo(repTO.getWhMail());
                            reportTO.setMailIdTo("sivanesa007@gmail.com");
                            reportTO.setMailIdCc("arunkumar.kanagaraj@newage-global.com");
                            reportTO.setFileName(tempContent[1]);
                            reportTO.setFilePath(tempContent[2]);

                            int insertStatus = reportBP.insertMailDetails(reportTO, 1403);
                            System.out.println("insertStatus==" + insertStatus);

                        }
                    }
                }
                ArrayList emailList = new ArrayList();
                Iterator itr = null;

                try {
//                                      emailList = reportBP.getEmailList();
                    System.out.println("emailList ------------- " + emailList);
                    System.out.println("emailList ------------- " + emailList.size());
                    itr = emailList.iterator();
                    while (itr.hasNext()) {
                        reportTO1 = (ReportTO) itr.next();
                        System.out.println("reportTO ----- mail contenet  " + reportTO1.getMailContentTo());
                        System.out.println("applicationServer ----------- " + applicationServer);
                        reportTO1.setServerName(applicationServer);
                        reportTO1.setServerIpAddress(serverIpAddress);
                        new EmailSSL(reportTO1).start();
                    }

                } catch (Exception exp) {
                    exp.printStackTrace();

                    ReportTO rTO = new ReportTO();
                    rTO.setMailSubjectTo("Email not delivered");
                    rTO.setMailContentTo(exp.getMessage());
                    rTO.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO.setMailIdCc("throttleerror@gmail.com");

                    rTO = null;
                } finally {
                    emailList = null;
                    itr = null;
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            reportTO = null;

        }

    }

    public HashMap<String, String> loadProps() throws FPRuntimeException, FPBusinessException {

        try {

            ArrayList list = new ArrayList();
            list = loginBP.LoadProps();
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            //System.out.println("list -------" + list);
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ThrottleConstants.PROPS_VALUES;
    }

}
