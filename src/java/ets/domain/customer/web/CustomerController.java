package ets.domain.customer.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.web.BaseController;
import ets.domain.customer.business.CustomerBP;
import ets.domain.customer.business.CustomerTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import java.io.*;
import java.net.UnknownHostException;
import java.util.Date;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author vidya
 */
public class CustomerController extends BaseController {
    
    CustomerCommand customerCommand;
    CustomerBP customerBP;
    LoginBP loginBP;
    
    public CustomerBP getCustomerBP() {
        return customerBP;
    }
    
    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }
    
    public CustomerCommand getCustomerCommand() {
        return customerCommand;
    }
    
    public void setCustomerCommand(CustomerCommand customerCommand) {
        this.customerCommand = customerCommand;
    }
    
    public LoginBP getLoginBP() {
        return loginBP;
    }
    
    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }
    
    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);
        
    }
    
    public ModelAndView handleViewAddScreen(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> Add Customer ";
        String pageTitle = "Add Customer";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                ArrayList billingTypeList = new ArrayList();
                billingTypeList = customerBP.getBillingTypeList();
                request.setAttribute("billingTypeList", billingTypeList);
                
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);
                
                ArrayList companyLists = new ArrayList();
                companyLists = loginBP.getCompanyList();
                request.setAttribute("companyLists", companyLists);
                
                ArrayList organizationList = new ArrayList();
                organizationList = customerBP.getOrganizationList();
                request.setAttribute("organizationList", organizationList);
                
                ArrayList customerGroup = new ArrayList();
                customerGroup = customerBP.getCustomerGroupList();
                request.setAttribute("customerGroup", customerGroup);
                path = "content/Customer/addCustomer.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddCustomer(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        int insertContractRouteMater = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getSecondaryBillingTypeId() != null && customerCommand.getSecondaryBillingTypeId() != "") {
                customerTO.setSecondaryBillingTypeId(customerCommand.getSecondaryBillingTypeId());
            }
            if (customerCommand.getCustName() != null && customerCommand.getCustName() != "") {
                customerTO.setCustName(customerCommand.getCustName());
            }
            if (customerCommand.getBillingTypeId() != null && customerCommand.getBillingTypeId() != "") {
                customerTO.setBillingTypeId(customerCommand.getBillingTypeId());
            }
            if (customerCommand.getPaymentType() != null && customerCommand.getPaymentType() != "") {
                customerTO.setPaymentType(customerCommand.getPaymentType());
            }
            if (customerCommand.getCustContactPerson() != null && customerCommand.getCustContactPerson() != "") {
                customerTO.setCustContactPerson(customerCommand.getCustContactPerson());
            }
            if (customerCommand.getCustAddress() != null && customerCommand.getCustAddress() != "") {
                customerTO.setCustAddress(customerCommand.getCustAddress());
            }
            if (customerCommand.getCustCity() != null && customerCommand.getCustCity() != "") {
                customerTO.setCustCity(customerCommand.getCustCity());
            }
            if (customerCommand.getCustState() != null && customerCommand.getCustState() != "") {
                customerTO.setCustState(customerCommand.getCustState());
            }
            if (customerCommand.getCustPhone() != null && customerCommand.getCustPhone() != "") {
                customerTO.setCustPhone(customerCommand.getCustPhone());
            }
            if (customerCommand.getCustMobile() != null && customerCommand.getCustMobile() != "") {
                customerTO.setCustMobile(customerCommand.getCustMobile());
            }
            if (customerCommand.getCustEmail() != null && customerCommand.getCustEmail() != "") {
                customerTO.setCustEmail(customerCommand.getCustEmail());
            }
            if (customerCommand.getAccountManagerId() != null && customerCommand.getAccountManagerId() != "") {
                customerTO.setAccountManagerId(customerCommand.getAccountManagerId());
            }
            if (customerCommand.getCustomerGroupId() != null && customerCommand.getCustomerGroupId() != "") {
                customerTO.setCustomerGroupId(customerCommand.getCustomerGroupId());
            }
            System.out.println("customerCommand.getCustomerGroupId()---" + customerCommand.getCustomerGroupId());
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                customerTO.setCustomerId(customerCommand.getCustomerId());
            }
            if (customerCommand.getCustomerName() != null && customerCommand.getCustomerName() != "") {
                customerTO.setCustomerName(customerCommand.getCustomerName());
            }
            if (customerCommand.getCustStatus() != null && customerCommand.getCustStatus() != "") {
                customerTO.setCustStatus(customerCommand.getCustStatus());
            }
            if (customerCommand.getCustType() != null && customerCommand.getCustType() != "") {
                customerTO.setCustType(customerCommand.getCustType());
            }
            if (customerCommand.getDetentionAmount() != null && customerCommand.getDetentionAmount() != "") {
                customerTO.setDetentionAmount(customerCommand.getDetentionAmount());
            }
            if (customerCommand.getDetentionPeriod() != null && customerCommand.getDetentionPeriod() != "") {
                customerTO.setDetentionPeriod(customerCommand.getDetentionPeriod());
            }
            if (customerCommand.getPanNo() != null && customerCommand.getPanNo() != "") {
                customerTO.setPanNo(customerCommand.getPanNo());
            }
            if (customerCommand.getTtaCustType() != null && customerCommand.getTtaCustType() != "") {
                customerTO.setTtaCustType(customerCommand.getTtaCustType());
            }
//            if (customerCommand.getStateId() != null && customerCommand.getStateId() != "") {
//                 customerTO.setStateId(customerCommand.getStateId());
//                 System.out.println("getStateId"+customerCommand.getStateId());
//             }
//             if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
//                 customerTO.setGstNo(customerCommand.getGstNo());
//                 System.out.println("getGstNo"+customerCommand.getGstNo());
//             }
            if (customerCommand.getOrganizationId() != null && customerCommand.getOrganizationId() != "") {
                customerTO.setOrganizationId(customerCommand.getOrganizationId());
                System.out.println("getOrganizationId" + customerCommand.getOrganizationId());
            }
            
            String displayCustName = request.getParameter("displayCustName");
            String customerGroup = request.getParameter("customerGroup");
            String creditLimit = request.getParameter("creditLimit");
            String creditDays = request.getParameter("creditDays");
            String[] billingAddressIds = request.getParameterValues("billingAddressIds");
            String[] billingNameAddress = request.getParameterValues("billingNameAddress");
            String[] stateId = request.getParameterValues("stateId");
            String[] gstNo = request.getParameterValues("gstNo");
            String[] activeInd = request.getParameterValues("activeInd");
            customerTO.setCreditLimit(creditLimit);
            customerTO.setCreditDays(creditDays);
            customerTO.setGroupId(customerGroup);
            customerTO.setDisplayCustName(displayCustName);
            String custCode = "";
            custCode = customerBP.getCustomerCode(customerTO, insertContractRouteMater);
            request.setAttribute("custCode", custCode);
            //                if (customerCommand.getTancem() != null && customerCommand.getTancem()!= "") {
            //                customerTO.setTancem(customerCommand.getTancem());
            //           }
            path = "content/Customer/manageCustomer.jsp";
//            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
//                update = customerBP.editUpdateCustomer(customerTO, userId);
//            } else {
//                status = customerBP.processInsertCustomer(customerTO, userId);
//            }
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                update = customerBP.editUpdateCustomer(customerTO, userId, billingAddressIds, billingNameAddress, stateId, gstNo, activeInd);
            } else {
                status = customerBP.processInsertCustomer(customerTO, userId, billingAddressIds, billingNameAddress, stateId, gstNo, activeInd);
            }
            
            CustomerList = customerBP.processCustomerLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Customer Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleViewCustomer(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList customerList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Customer";
        menuPath = "Customer  >>Customer Details ";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Customer-View")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            String customerId = "";
            String customerCode = "";
            String custName = "";
            String contractType = "";
            String customerType = "";
            customerId = request.getParameter("customerId");
            contractType = request.getParameter("contractType");
            customerCode = request.getParameter("customerCode");
            custName = request.getParameter("custName");
            customerType = request.getParameter("customerType");
            customerTO.setCustomerId(customerId);
            customerTO.setContractType(contractType);
            customerTO.setCustomerType(customerType);
            System.out.println("customerId" + customerId);
            System.out.println("ContractType" + contractType);
            
            String roleId = "" + (Integer) session.getAttribute("RoleId");
            int userId = (Integer) session.getAttribute("userId");
            customerTO.setRoleId(roleId);
            customerTO.setUserId(userId + "");
            
            path = "content/Customer/manageCustomer.jsp";
            request.setAttribute("customerId", customerId);
            request.setAttribute("custName", custName);
            request.setAttribute("customerCode", customerCode);
            request.setAttribute("ContractType", contractType);
            customerList = customerBP.processCustomerLists(customerTO);
            request.setAttribute("CustomerLists", customerList);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleViewAlterScreen(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Alter")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                String pageTitle = "Alter Customer";
                request.setAttribute("pageTitle", pageTitle);
                ArrayList customerList = new ArrayList();
                path = "content/Customer/alterCustomer.jsp";
                customerList = customerBP.processCustomerList();
                System.err.append("customerlist size in alter path" + customerList.size());
                request.setAttribute("CustomerList", customerList);
                
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view customerList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleUpdateCustomer(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        try {
            String menuPath = "Customer  >>  Customer Details";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int update = 0;
            String[] idList = customerCommand.getCustIdList();
            String[] nameList = customerCommand.getCustNameList();
            String[] typeList = customerCommand.getCustTypeList();
            String[] contactPersonList = customerCommand.getCustContactPersonList();
            String[] addressList = customerCommand.getCustAddressList();
            String[] cityList = customerCommand.getCustCityList();
            String[] stateList = customerCommand.getCustStateList();
            String[] phoneList = customerCommand.getCustPhoneList();
            String[] mobileList = customerCommand.getCustMobileList();
            String[] emailList = customerCommand.getCustEmailList();
            String[] statusList = customerCommand.getCustStatusList();
            String[] selectedIndex = customerCommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");
            ArrayList List = new ArrayList();
            CustomerTO customerTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                customerTO = new CustomerTO();
                index = Integer.parseInt(selectedIndex[i]);
                customerTO.setCustId(Integer.parseInt(idList[index]));
                customerTO.setCustName(nameList[index]);
                customerTO.setCustomerType(typeList[index]);
                System.out.println("typeList[index]: " + typeList[index]);
                customerTO.setCustContactPerson(contactPersonList[index]);
                customerTO.setCustCity(cityList[index]);
                customerTO.setCustAddress(addressList[index]);
                customerTO.setCustState(stateList[index]);
                customerTO.setCustPhone(phoneList[index]);
                customerTO.setCustMobile(mobileList[index]);
                customerTO.setCustEmail(emailList[index]);
                customerTO.setCustStatus(statusList[index]);
                List.add(customerTO);
            }
            path = "content/Customer/manageCustomer.jsp";
            update = customerBP.processUpdateCustomer(List, userId);
            String pageTitle = "Manage Customer";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList customerList = new ArrayList();
            customerList = customerBP.processCustomerList();
            request.setAttribute("CustomerLists", customerList);
            
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //CustomerController

    public ModelAndView handleViewContract(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Contract";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList contractList = new ArrayList();
        menuPath = "Contract  >> Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
            path = "content/contract/manageContract.jsp";
            //customerList = customerBP.processContractList();
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {

         FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         }*/ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewContract --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddContract(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Contract  >> Manage Contract ";
        String pageTitle = "Manage Contract";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
            if (customerCommand.getContManufacturerName() != null && customerCommand.getContManufacturerName() != "") {
                customerTO.setContManufacturerName(customerCommand.getContManufacturerName());
            }
            if (customerCommand.getContModelName() != null && customerCommand.getContModelName() != "") {
                customerTO.setContModelName(customerCommand.getContModelName());
            }
            if (customerCommand.getContPercentAgeUpSpares() != null && customerCommand.getContPercentAgeUpSpares() != "") {
                customerTO.setContPercentAgeUpSpares(customerCommand.getContPercentAgeUpSpares());
            }
            if (customerCommand.getContPercentAgeUpLabour() != null && customerCommand.getContPercentAgeUpLabour() != "") {
                customerTO.setContPercentAgeUpLabour(customerCommand.getContPercentAgeUpLabour());
            }
            if (customerCommand.getContStatus() != null && customerCommand.getContStatus() != "") {
                customerTO.setContStatus(customerCommand.getContStatus());
            }
            path = "content/contract/addContract.jsp";
            // status = customerBP.processInsertContract(customerTO, userId);

            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Inserted Successfully");
            }
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {

         FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Contract Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customers  >> Bunk Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/manageBunkDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewBunkDetailsV(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        System.out.println("Block2......");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkName = request.getParameter("bunkName");
        System.out.println("bunkName.." + bunkName);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            /*if (!loginBP.checkAuthorisation(userFunctions, "Bunk-View")) {
             path = "content/common/NotAuthorized.jsp";
             } else {*/
            path = "content/Customer/manageBunkDetails.jsp";
            bunkList = customerBP.processBunkList(bunkName);
            request.setAttribute("bunkList", bunkList);
            System.out.println("bunkList.." + bunkList);
            //}
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleCreateBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/addBunkDetails.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } /*catch (FPBusinessException exception) {
         /*
         * run time exception has occurred. Directed to error page.
         */ /*FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
         request.setAttribute(ParveenErrorConstants.ERROR_KEY,
         exception.getErrorMessage());
         } */ catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand customerCommand) throws IOException {
        System.out.println("Block4...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bunkList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkName = request.getParameter("bunkName");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBunkName() != null && customerCommand.getBunkName() != "") {
                customerTO.setBunkName(customerCommand.getBunkName());
            }
            if (customerCommand.getRemarks() != null && customerCommand.getRemarks() != "") {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getLocation() != null && customerCommand.getLocation() != "") {
                customerTO.setCurrlocation(customerCommand.getLocation());
            }
            if (customerCommand.getCurrRate() != null && customerCommand.getCurrRate() != "") {
                customerTO.setCurrRate(customerCommand.getCurrRate());
            }
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setBunkState(customerCommand.getState());
            }
            if (customerCommand.getActiveStatus() != null && customerCommand.getActiveStatus() != "") {
                customerTO.setBunkStatus(customerCommand.getActiveStatus());
            }
            if (customerCommand.getFuelType() != null && customerCommand.getFuelType() != "") {
                customerTO.setFuelType(customerCommand.getFuelType());
            }
            path = "content/Customer/manageBunkDetails.jsp";
            System.out.println("1");
            status = customerBP.processInsertBunk(customerTO, userId);
            System.out.println("2");
            
            bunkList = customerBP.processBunkList(bunkName);
            
            request.setAttribute("bunkList", bunkList);
            
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Added Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView alterbunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList bunkalterList = new ArrayList();
        HttpSession session = request.getSession();
        customerCommand = command;
        String menuPath = "";
        String pageTitle = "View Bunk";
        menuPath = "Customer  >>Bunk Details ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkId = request.getParameter("bunkId");
        System.out.println("bunkId.." + bunkId);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/addBunkDetails.jsp";
            
            bunkalterList = customerBP.processBunkalterList(bunkId);
            request.setAttribute("bunkalterList", bunkalterList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewBunk --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView UpdateBunkDetails(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        System.out.println("Block4...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList bunkList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Customer  >>  Customer Details ";
        String pageTitle = "View  Customer ";
        request.setAttribute("pageTitle", pageTitle);
        String bunkId = request.getParameter("bunkId");
        String bunkName = request.getParameter("bunkName");
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBunkName() != null && customerCommand.getBunkName() != "") {
                customerTO.setBunkName(customerCommand.getBunkName());
            }
            if (customerCommand.getRemarks() != null && customerCommand.getRemarks() != "") {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getLocation() != null && customerCommand.getLocation() != "") {
                customerTO.setCurrlocation(customerCommand.getLocation());
            }
            if (customerCommand.getCurrRate() != null && customerCommand.getCurrRate() != "") {
                customerTO.setCurrRate(customerCommand.getCurrRate());
            }
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setBunkState(customerCommand.getState());
            }
            if (customerCommand.getActiveStatus() != null && customerCommand.getActiveStatus() != "") {
                customerTO.setBunkStatus(customerCommand.getActiveStatus());
            }
            if (customerCommand.getFuelType() != null && customerCommand.getFuelType() != "") {
                customerTO.setFuelType(customerCommand.getFuelType());
            }
            path = "content/Customer/manageBunkDetails.jsp";
            status = customerBP.processBunkUpdateList(customerTO, userId, bunkId);
            bunkList = customerBP.processBunkList(bunkName);
            request.setAttribute("bunkList", bunkList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewEditCustomer(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Customer >>Customer Details >> View / Edit Customer ";
        String pageTitle = "View / Edit Customer";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            path = "content/Customer/editCustomer.jsp";
            //                if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
            //                    customerTO.setCustomerId(customerCommand.getCustomerId());
            //                    request.setAttribute("customerId", customerCommand.getCustomerId());
            //                    }
            System.out.println("customerId" + customerCommand.getCustomerId());
            if (customerCommand.getCustomerName() != null && customerCommand.getCustomerName() != "") {
                request.setAttribute("customerName", customerCommand.getCustomerName());
            }
            String customerId = "";
            customerId = request.getParameter("customerId");
            customerTO.setCustomerId(customerId);
            //
            ArrayList companyLists = new ArrayList();
            companyLists = loginBP.getCompanyList();
            request.setAttribute("companyLists", companyLists);
            
            ArrayList billingTypeList = new ArrayList();
            billingTypeList = customerBP.getBillingTypeList();
            request.setAttribute("billingTypeList", billingTypeList);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            ArrayList organizationList = new ArrayList();
            organizationList = customerBP.getOrganizationList();
            request.setAttribute("organizationList", organizationList);
            
            ArrayList getCustomerAddressDetails = new ArrayList();
            getCustomerAddressDetails = customerBP.getCustomerAddressDetails(customerTO);
            request.setAttribute("getCustomerAddressDetails", getCustomerAddressDetails);

            //
            //                if (customerCommand.getCustomerId() != null && !"".equals(customerCommand.getCustomerId())) {
            //                customerTO.setCustomerId(customerCommand.getCustomerId());
            //                }
            //                System.out.println("customerId2"+customerCommand.getCustomerId());
            ArrayList customerGroup = new ArrayList();
            customerGroup = customerBP.getCustomerGroupList();
            request.setAttribute("customerGroup", customerGroup);
            ArrayList getCustomerDetails = new ArrayList();
            getCustomerDetails = customerBP.getCustomerDetails(customerTO);
            request.setAttribute("getCustomerDetails", getCustomerDetails);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public void getCustomerName(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String custName = "";
            response.setContentType("text/html");
            custName = request.getParameter("custName");
            customerTO.setCustName(custName);
            customerList = customerBP.getCustomerName(customerTO);
            System.out.println("customerList.size() = " + customerList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = customerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                customerTO = (CustomerTO) itr.next();
                jsonObject.put("Name", customerTO.getCustomerId() + "-" + customerTO.getCustName() + "-" + customerTO.getStateId() + "-" + customerTO.getPanNo());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }
    
    public void getCustomerCode(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        HttpSession session = request.getSession();
//        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
//                JsonTO jsonTO = new JsonTO();
        ArrayList customerList = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String customerCode = "";
            response.setContentType("text/html");
            customerCode = request.getParameter("customerCode");
            customerTO.setCustomerCode(customerCode);
            customerList = customerBP.getCustomerCodes(customerTO);
            System.out.println("customerList.size() = " + customerList.size());
            JSONArray jsonArray = new JSONArray();
            Iterator itr = customerList.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                customerTO = (CustomerTO) itr.next();
                jsonObject.put("Name", customerTO.getCustomerId() + "-" + customerTO.getCustomerCode());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);
            
            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

//   public ModelAndView handleBranchUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
//        System.out.println("ConfigDetails...");
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        customerCommand = command;
//        String path = "";
//        int status = 0;
//        int userId = (Integer) session.getAttribute("userId");
//        CustomerTO customerTO = new CustomerTO();
//        String menuPath = "";
//        menuPath = "Master  >> BranchMaster ";
//        String pageTitle = "BranchMaster ";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        String customerName = "";
//
//        try {
//            
//            String custId = request.getParameter("custId");
//            String branchId = request.getParameter("branchId");
//            String customerId = request.getParameter("customerId");
//            if (customerId != null) {
//                customerTO.setCustomerId(customerId);
//            }
//
//            ArrayList branchList = new ArrayList();
//            branchList = customerBP.processBranchList(customerTO);
//            request.setAttribute("branchList", branchList);
//
//            ArrayList customerGroup = new ArrayList();
//             customerGroup = customerBP.getCustomerGroupList();
//                request.setAttribute("customerGroup", customerGroup);
//
//            ArrayList stateList = new ArrayList();
//            stateList = customerBP.getStateList();
//            request.setAttribute("stateList", stateList);
//            CustomerTO tpTO1 = new CustomerTO();
//
//
//            ArrayList cityList = new ArrayList();
//            cityList = customerBP.processCityList();
//            request.setAttribute("CityList", cityList);
//            
//            ArrayList CustomerList = new ArrayList();
//            CustomerList = customerBP.processBranchLists(customerTO);
//            request.setAttribute("CustomerLists", CustomerList);
//
//            path = "content/Customer/manageBranch.jsp";
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//
//    }
    public ModelAndView handleBranchInsert(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "";
        menuPath = "Master >>  Branch Master ";
        String pageTitle = "Branch Master ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageUniqueId = request.getParameter("pageId");
//        System.out.println("pageId:" + pageUniqueId);
        String sessionId = (String) session.getAttribute("uniquePageId");
//        System.out.println("sessionId:" + sessionId);
        CustomerTO customerTO = new CustomerTO();
        customerCommand = command;
        ArrayList branchList = new ArrayList();
        try {
            if (customerCommand.getBranchId() != null && !"".equals(customerCommand.getBranchId())) {
                customerTO.setBranchId(customerCommand.getBranchId());
            }
            if (customerCommand.getBranchName() != null && !"".equals(customerCommand.getBranchName())) {
                customerTO.setBranchName(customerCommand.getBranchName());
            }
            if (customerCommand.getCustomerId() != null && !"".equals(customerCommand.getCustomerId())) {
                customerTO.setCustomerId(customerCommand.getCustomerId());
            }

            //System.out.println("customer id in consignor = " + customerCommand.getCustomerIdAdd());
            if (customerCommand.getContactPerson() != null && !"".equals(customerCommand.getContactPerson())) {
                customerTO.setContactPerson(customerCommand.getContactPerson());
            }
            if (customerCommand.getPhoneNo() != null && !"".equals(customerCommand.getPhoneNo())) {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            if (customerCommand.getAddress1() != null && !"".equals(customerCommand.getAddress1())) {
                customerTO.setAddress1(customerCommand.getAddress1());
            }
            if (customerCommand.getAddress2() != null && !"".equals(customerCommand.getAddress2())) {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            if (customerCommand.getStateId() != null && !"".equals(customerCommand.getStateId())) {
                customerTO.setStateId(customerCommand.getStateId());
            }
            if (customerCommand.getCityId() != null && !"".equals(customerCommand.getCityId())) {
                customerTO.setCityId(customerCommand.getCityId());
            }
            if (customerCommand.getEmail() != null && !"".equals(customerCommand.getEmail())) {
                customerTO.setEmail(customerCommand.getEmail());
            }
            if (customerCommand.getActiveInd() != null && !"".equals(customerCommand.getActiveInd())) {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            if (customerCommand.getRemarks() != null && !"".equals(customerCommand.getRemarks())) {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getPinCode() != null && !"".equals(customerCommand.getPinCode())) {
                customerTO.setPinCode(customerCommand.getPinCode());
            }
            if (customerCommand.getGstNo() != null && !"".equals(customerCommand.getGstNo())) {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getBranchId() != null && !"".equals(customerCommand.getBranchId())) {
                System.out.println("QuotationNo:" + customerCommand.getBranchId());
                customerTO.setBranchId(customerCommand.getBranchId());
                status = customerBP.processBranchUpdate(customerTO, userId);
                
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Branch Updated SucessFully");
                branchList = customerBP.processBranchList(customerTO);
                request.setAttribute("branchList", branchList);
                path = "content/Customer/manageBranch.jsp";
            } else {
                
                status = customerBP.processBranchInsert(customerTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Branch Added Successfully");
                branchList = customerBP.processBranchList(customerTO);
                request.setAttribute("branchList", branchList);
                path = "content/Customer/manageBranch.jsp";
            }
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Branch Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleViewBranch(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        
        String menuPath = "";
        CustomerTO customerTO = new CustomerTO();
        String pageTitle = "Branch";
        menuPath = "Master >>  Branch Master";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
            path = "content/Customer/manageBranchView.jsp";
//            String customerId="";
            customerTO.setCustomerId(request.getParameter("customerId"));
            
            ArrayList branch = new ArrayList();
            branch = customerBP.getBranchView(customerTO);
            Iterator itr1 = branch.iterator();
            CustomerTO tpTO1 = new CustomerTO();
            String customerName = "";
            if (itr1.hasNext()) {
                tpTO1 = (CustomerTO) itr1.next();
                customerName = tpTO1.getCustomerName();
                request.setAttribute("customerName", customerName);
            }
            request.setAttribute("branch", branch);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View BranchList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleConsigneeUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
//        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        int configDetailsMaster = 0;
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Operation  >> configDetailsMaster ";
        String pageTitle = "configDetailsMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String custId = "";
        String custName = "";
        custId = request.getParameter("custId");
        custName = request.getParameter("custName");
        System.out.println("custId" + custId);
        
        try {
            String customerId = request.getParameter("customerId");
            request.setAttribute("customerId", customerId);
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            
            ArrayList ConsigneeList = new ArrayList();
            ConsigneeList = customerBP.processContractConsigneeList(customerTO, custId);
            request.setAttribute("ConsigneeList", ConsigneeList);
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            request.setAttribute("custName", custName);
            request.setAttribute("custId", custId);
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            path = "content/Customer/manageConsignee.jsp";
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleConsigneeInsert(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "";
        menuPath = "Master >>  Consignee ";
        String pageTitle = "Consignee ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        System.out.println("pageId:" + pageUniqueId);
        String sessionId = (String) session.getAttribute("uniquePageId");
        CustomerTO customerTO = new CustomerTO();
        
        String custId = "";
        String custName = "";
        custId = request.getParameter("custId");
        custName = request.getParameter("custName");
        System.out.println("custId" + custId);
        
        customerCommand = command;
        ArrayList ConsigneeList = new ArrayList();
        try {
            if (customerCommand.getConsigneeId() != null && !"".equals(customerCommand.getConsigneeId())) {
                customerTO.setConsigneeId(customerCommand.getConsigneeId());
            }
            if (customerCommand.getConsigneeName() != null && !"".equals(customerCommand.getConsigneeName())) {
                customerTO.setConsigneeName(customerCommand.getConsigneeName());
            }
            if (customerCommand.getCustomerId() != null && !"".equals(customerCommand.getCustomerId())) {
                customerTO.setCustomerId(customerCommand.getCustomerId());
            }
            System.out.println("getCustomerId" + customerCommand.getCustomerId());
            
            if (customerCommand.getContactPerson() != null && !"".equals(customerCommand.getContactPerson())) {
                customerTO.setContactPerson(customerCommand.getContactPerson());
            }
            if (customerCommand.getPhoneNo() != null && !"".equals(customerCommand.getPhoneNo())) {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            if (customerCommand.getAddress1() != null && !"".equals(customerCommand.getAddress1())) {
                customerTO.setAddress1(customerCommand.getAddress1());
            }
            if (customerCommand.getAddress2() != null && !"".equals(customerCommand.getAddress2())) {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            if (customerCommand.getStateId() != null && !"".equals(customerCommand.getStateId())) {
                customerTO.setStateId(customerCommand.getStateId());
            }
            if (customerCommand.getCityId() != null && !"".equals(customerCommand.getCityId())) {
                customerTO.setCityId(customerCommand.getCityId());
            }
            if (customerCommand.getEmail() != null && !"".equals(customerCommand.getEmail())) {
                customerTO.setEmail(customerCommand.getEmail());
            }
            if (customerCommand.getActiveInd() != null && !"".equals(customerCommand.getActiveInd())) {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            if (customerCommand.getRemarks() != null && !"".equals(customerCommand.getRemarks())) {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getPinCode() != null && !"".equals(customerCommand.getPinCode())) {
                customerTO.setPinCode(customerCommand.getPinCode());
            }
            
            if (customerCommand.getGstNo() != null && !"".equals(customerCommand.getGstNo())) {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getConsigneeId() != null && !"".equals(customerCommand.getConsigneeId())) {
                System.out.println("QuotationNo:" + customerCommand.getConsigneeId());
                customerTO.setConsigneeId(customerCommand.getConsigneeId());
                status = customerBP.processConsigneeUpdate(customerTO, userId);
                
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Consignee Updated SucessFully");
                ConsigneeList = customerBP.processContractConsigneeList(customerTO, custId);
                request.setAttribute("ConsigneeList", ConsigneeList);
                request.setAttribute("custId", custId);
                request.setAttribute("custName", custName);
                path = "content/Customer/manageConsignee.jsp";
            } else {
                
                status = customerBP.processConsigneeInsert(customerTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Consignee Added Successfully");
                ConsigneeList = customerBP.processContractConsigneeList(customerTO, custId);
                request.setAttribute("ConsigneeList", ConsigneeList);
                request.setAttribute("custId", custId);
                request.setAttribute("custName", custName);
                path = "content/Customer/manageConsignee.jsp";
            }
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Quotation Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public void checkConsigneeName(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        HttpSession session = request.getSession();
        //        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList checkConsigneeName = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        int status = 0;
        try {
            String consignName = "";
            String customerId = "";
            //               response.setContentType("text/html");
            consignName = request.getParameter("consignName");
            customerId = request.getParameter("customerId");
            System.out.println("consignName" + consignName);
            customerTO.setConsigneName(consignName);
            
            status = customerBP.getcheckConsigneeName(consignName, customerId);
            response.setContentType("text/css");
            
            response.getWriter().println(status);
            
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }

    //consignor
    public ModelAndView handleConsignorUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
//        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        int configDetailsMaster = 0;
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Operation  >> configDetailsMaster ";
        String pageTitle = "configDetailsMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String custId = "";
        String custName = "";
        custId = request.getParameter("custId");
        custName = request.getParameter("custName");
        System.out.println("custId" + custId);
        
        try {
            String customerId = request.getParameter("customerId");
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            
            ArrayList consignorList = new ArrayList();
            consignorList = customerBP.processContractConsignorList(customerTO, custId);
            request.setAttribute("ConsignorList", consignorList);
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            request.setAttribute("custName", custName);
            request.setAttribute("custId", custId);
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            path = "content/Customer/manageConsignor.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
        
    }
    
    public ModelAndView handleConsignorInsert(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "";
        menuPath = "Master >>  Consignor ";
        String pageTitle = "Consignor ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageUniqueId = request.getParameter("pageId");
//        System.out.println("pageId:" + pageUniqueId);
        String sessionId = (String) session.getAttribute("uniquePageId");
//        System.out.println("sessionId:" + sessionId);
        CustomerTO customerTO = new CustomerTO();
        customerCommand = command;
        ArrayList consignorList = new ArrayList();
        System.out.println("IM in CON");
        String custId = "";
        String custName = "";
        custId = request.getParameter("custId");
        custName = request.getParameter("custName");
        System.out.println("custId" + custId);
        
        try {
            if (customerCommand.getConsignorId() != null && !"".equals(customerCommand.getConsignorId())) {
                customerTO.setConsignorId(customerCommand.getConsignorId());
            }
            if (customerCommand.getConsignorName() != null && !"".equals(customerCommand.getConsignorName())) {
                customerTO.setConsignorName(customerCommand.getConsignorName());
            }
            if (customerCommand.getCustomerId() != null && !"".equals(customerCommand.getCustomerId())) {
                customerTO.setCustomerId(customerCommand.getCustomerId());
            }

            //System.out.println("customer id in consignor = " + customerCommand.getCustomerIdAdd());
            if (customerCommand.getContactPerson() != null && !"".equals(customerCommand.getContactPerson())) {
                customerTO.setContactPerson(customerCommand.getContactPerson());
            }
            if (customerCommand.getPhoneNo() != null && !"".equals(customerCommand.getPhoneNo())) {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            if (customerCommand.getAddress1() != null && !"".equals(customerCommand.getAddress1())) {
                customerTO.setAddress1(customerCommand.getAddress1());
            }
            if (customerCommand.getAddress2() != null && !"".equals(customerCommand.getAddress2())) {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            if (customerCommand.getStateId() != null && !"".equals(customerCommand.getStateId())) {
                customerTO.setStateId(customerCommand.getStateId());
            }
            if (customerCommand.getCityId() != null && !"".equals(customerCommand.getCityId())) {
                customerTO.setCityId(customerCommand.getCityId());
            }
            if (customerCommand.getEmail() != null && !"".equals(customerCommand.getEmail())) {
                customerTO.setEmail(customerCommand.getEmail());
            }
            if (customerCommand.getActiveInd() != null && !"".equals(customerCommand.getActiveInd())) {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            if (customerCommand.getRemarks() != null && !"".equals(customerCommand.getRemarks())) {
                customerTO.setRemarks(customerCommand.getRemarks());
            }
            if (customerCommand.getPinCodeAdd() != null && !"".equals(customerCommand.getPinCodeAdd())) {
                customerTO.setPinCodeAdd(customerCommand.getPinCodeAdd());
            }
            
            if (customerCommand.getGstNo() != null && !"".equals(customerCommand.getGstNo())) {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            System.out.println("im in try");
            if (customerCommand.getConsignorId() != null && !"".equals(customerCommand.getConsignorId())) {
                System.out.println("QuotationNo:" + customerCommand.getConsignorId());
                customerTO.setConsignorId(customerCommand.getConsignorId());
                status = customerBP.processConsignorUpdate(customerTO, userId);
                
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Consignor Updated SucessFully");
                consignorList = customerBP.processContractConsignorList(customerTO, custId);
                request.setAttribute("ConsignorList", consignorList);
                ArrayList cityList = new ArrayList();
                cityList = customerBP.processCityList();
                request.setAttribute("CityList", cityList);
                request.setAttribute("custId", custId);
                request.setAttribute("custName", custName);
                
                path = "content/Customer/manageConsignor.jsp";
            } else {
                
                status = customerBP.processConsignorInsert(customerTO, userId);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Consignor Added Successfully");
                consignorList = customerBP.processContractConsignorList(customerTO, custId);
                request.setAttribute("ConsignorList", consignorList);
                ArrayList cityList = new ArrayList();
                cityList = customerBP.processCityList();
                request.setAttribute("CityList", cityList);
                request.setAttribute("custId", custId);
                request.setAttribute("custName", custName);
                
                path = "content/Customer/manageConsignor.jsp";
            }
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY, exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Quotation Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public void checkConsignorName(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException, FPRuntimeException, FPBusinessException {
        HttpSession session = request.getSession();
        //        CustomerCommand = command;
        CustomerTO customerTO = new CustomerTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList checkConsigneeName = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        int status = 0;
        try {
            String consignName = "";
            String customerId = "";
//               response.setContentType("text/html");
            consignName = request.getParameter("consignName");
            customerId = request.getParameter("customerId");
            System.out.println("consignName" + consignName);
            customerTO.setConsigneName(consignName);
            
            status = customerBP.getcheckConsignorName(consignName, customerId);
            response.setContentType("text/css");
            
            response.getWriter().println(status);
            
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }
    
    public void handleStateName(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        
        customerCommand = command;
        CustomerTO customerTO = new CustomerTO();
        PrintWriter pw = response.getWriter();
        try {
            
            if (customerCommand.getCityId() != null && !"".equals(customerCommand.getCityId())) {
                customerTO.setCityId(customerCommand.getCityId());
            }
            String state = "";
            state = customerBP.processStateName(customerTO);
            //System.out.println("state ctrl::" + state);
            pw.print(state);
            pw.close();
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }
    
    public ModelAndView handleViewConsignor(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        
        String menuPath = "";
        CustomerTO customerTO = new CustomerTO();
        String pageTitle = "Consignor";
        menuPath = "Master >>  Consignor";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
            path = "content/Customer/manageConsignorView.jsp";
//            String customerId="";
            customerTO.setCustomerId(request.getParameter("customerId"));
            
            ArrayList consignor = new ArrayList();
            consignor = customerBP.getConsignorView(customerTO);
            Iterator itr1 = consignor.iterator();
            CustomerTO tpTO1 = new CustomerTO();
            String customerName = "";
            if (itr1.hasNext()) {
                tpTO1 = (CustomerTO) itr1.next();
                customerName = tpTO1.getCustomerName();
                request.setAttribute("customerName", customerName);
            }
            request.setAttribute("consignor", consignor);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View QuotationList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleViewConsignee(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        
        String menuPath = "";
        CustomerTO customerTO = new CustomerTO();
        String pageTitle = "Consignee";
        menuPath = "Master >>  Consignee";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            
            path = "content/Customer/manageConsigneeView.jsp";
//            String customerId="";
            customerTO.setCustomerId(request.getParameter("customerId"));
            
            ArrayList consignee = new ArrayList();
            consignee = customerBP.getConsigneeView(customerTO);
            Iterator itr1 = consignee.iterator();
            CustomerTO tpTO1 = new CustomerTO();
            String customerName = "";
            if (itr1.hasNext()) {
                tpTO1 = (CustomerTO) itr1.next();
                customerName = tpTO1.getCustomerName();
                request.setAttribute("customerName", customerName);
            }
            request.setAttribute("consignee", consignee);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View QuotationList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleUploadCustContract(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        
        menuPath = "Operation  >> createLHCDetails ";
        String pageTitle = "createLHCDetails ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        
        try {
            
            String customerId = request.getParameter("custId");
            String custName = request.getParameter("custName");
            
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            if (custName != null) {
                customerTO.setCustName(custName);
            }
            
            ArrayList CustContractFiless = new ArrayList();
            CustContractFiless = customerBP.getCustContractFiles(customerId);
            
            int custsize = CustContractFiless.size();
            
            System.out.println("custsize :::::" + custsize);
            
            if (custsize > 0) {
                request.setAttribute("CustContractFiless", CustContractFiless);
            } else {
                request.setAttribute("CustContractFiless", null);
            }
            
            request.setAttribute("customerId", customerId);
            request.setAttribute("custName", custName);
            
            path = "content/Customer/uploadCustContract.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to createLHCDetails --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
        
    }
    
    public ModelAndView handleSaveUploadedFile(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        customerCommand = command;
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        path = "content/Customer/uploadCustContract.jsp";
        String pageTitle = "Upload Contract";
        request.setAttribute("pageTitle", pageTitle);
        String menuPath = "Vendor  >>  ManageLHC  >> Add";

        //file upload
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        boolean isMultipart = false;
        Part partObj = null;
        FilePart fPart = null;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;
        int p = 0;
        int s = 0;

//        String customerId = "";
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        String[] desc = new String[10];
        
        String customerId = "";
        String custName = "";
        
        try {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            CustomerTO customerTO = new CustomerTO();
            
            isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((partObj = parser.readNextPart()) != null) {
                    System.out.println("part Name:" + partObj.getName());
                    if (partObj.isFile()) {
                        //actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files");
                        actualServerFilePath = getServletContext().getRealPath("/uploadFiles/customer_contract");
                        System.out.println("actualServerFilePath : " + actualServerFilePath);
                        tempServerFilePath = actualServerFilePath.replace("\\", "\\\\");
                        Date now = new Date();
                        String date = now.getDate() + "" + (now.getMonth() + 1) + "" + (now.getYear() + 1900);
                        String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds() + userId;
                        fPart = (FilePart) partObj;
                        uploadedFileName[j] = fPart.getFileName();
                        
                        if (!"".equals(uploadedFileName[j]) && uploadedFileName[j] != null) {
                            //System.out.println("partObj.getName() = " + partObj.getName());
                            String[] splitFileName = uploadedFileName[j].split("\\.");
                            fileSavedAs = splitFileName[0] + "." + splitFileName[1];
                            //System.out.println("fileSavedAs = " + fileSavedAs);
                            fileSaved[j] = splitFileName[0] + date + time + "." + splitFileName[1];
                            fileName = fileSavedAs;
                            tempFilePath[j] = tempServerFilePath + "/" + fileSaved[j];
                            System.out.println("tempServerFilePath..." + tempServerFilePath);
                            System.out.println("filename..." + fileSaved[j]);
                            System.out.println("tempPath..." + tempFilePath[j]);
                            actualFilePath = actualServerFilePath;
                            System.out.println("actPath..." + actualServerFilePath);
                            System.out.println("actualFilePath..." + actualFilePath);
                            long fileSize = fPart.writeTo(new java.io.File(tempFilePath[j]));
                            File f1 = new File(actualFilePath);
                            f1.renameTo(new File(tempFilePath[j]));
                        }
                        
                        j++;
                    } else if (partObj.isParam()) {
                        
                        if (partObj.getName().equals("customerId")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            customerId = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("desc")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            desc[j] = paramPart.getStringValue();
                        }
                        if (partObj.getName().equals("custName")) {
                            ParamPart paramPart = (ParamPart) partObj;
                            custName = paramPart.getStringValue();
                        }
                    }
                }
            }
            
            int index = j;
            String[] file = new String[index];
            String selectedIndex = "0";
            String[] saveFile = new String[index];
            String[] FileType = new String[index];
            
            customerTO.setCustomerId(customerId);
            customerTO.setCustName(custName);
            
            request.setAttribute("customerId", customerId);
            request.setAttribute("custName", custName);
            
            System.out.println("j = " + j);
            System.out.println("customerId = " + customerId);
            System.out.println("custName = " + custName);
            
            for (int x = 0; x < j; x++) {
                file[x] = tempFilePath[x];
                System.out.println("File[x] = " + file[x]);
                saveFile[x] = fileSaved[x];
                System.out.println("filename[x] = " + saveFile[x]);
            }
            
            int status = customerBP.processInsertCustContractFiles(customerTO, userId, saveFile, desc);
            request.setAttribute("status", status);
            
            ArrayList CustContractFiless = new ArrayList();
            CustContractFiless = customerBP.getCustContractFiles(customerId);
            
            int custsize = CustContractFiless.size();
            
            System.out.println("custsize :::::" + custsize);
            
            if (custsize > 0) {
                request.setAttribute("CustContractFiless", CustContractFiless);
            } else {
                request.setAttribute("CustContractFiless", null);
            }
            
            customerTO.setDocType("all");
            
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Upload  Added Successfully");
            } else if (status == 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Customer Upload not added please contact system admin");
            }
            
            path = "content/Customer/uploadCustContract.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve vendor data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
//public ModelAndView handleViewAddBranchScreen(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        String path = "";
//        String menuPath = "";
//        menuPath = "Branch >>Branch Details >> Add Branch ";
//        String pageTitle = "Add Customer";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Add")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
//               
//
//                ArrayList stateList = new ArrayList();
//                stateList = customerBP.processCityList();
//                request.setAttribute("stateList", stateList);
//                
//                ArrayList hubList = new ArrayList();
//                hubList = customerBP.processHubList();
//                request.setAttribute("hubList", hubList);
//                path = "content/Customer/addBranch.jsp";
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/comon/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to view Branch Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }

// public ModelAndView handleAddBranch(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
//        if (request.getSession().isNew()) {
//        return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        int status = 0;
//        int update = 0;
//        String path = "";
//        customerCommand = command;
//        int userId = (Integer) session.getAttribute("userId");
//        ArrayList CustomerList = new ArrayList();
//        CustomerTO customerTO = new CustomerTO();
//        String menuPath = "";
//        menuPath = "Branch  >>  Branch Details ";
//        String pageTitle = "View  Branch ";
//        request.setAttribute("pageTitle", pageTitle);
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            if (customerCommand.getBranchName() != null && customerCommand.getBranchName() != "") {
//                customerTO.setBranchName(customerCommand.getBranchName());
//            }
//            
//            System.out.println("customerCommand.getCustName()--------->"+customerCommand.getCustName());
//            if (customerCommand.getBranchCode() != null && customerCommand.getBranchCode() != "") {
//                customerTO.setBranchCode(customerCommand.getBranchCode());
//            }
//            if (customerCommand.getBranchAddress() != null && customerCommand.getBranchAddress() != "") {
//                customerTO.setBranchAddress(customerCommand.getBranchAddress());
//            }
//            if (customerCommand.getBranchCity()!= null && customerCommand.getBranchCity() != "") {
//                customerTO.setBranchCity(customerCommand.getBranchCity());
//            }
//            if (customerCommand.getBranchPercent()!= null && customerCommand.getBranchPercent() != "") {
//                customerTO.setBranchPercent(customerCommand.getBranchPercent());
//            }
//             if (customerCommand.getHub()!= null && customerCommand.getHub() != "") {
//                customerTO.setHub(customerCommand.getHub());
//            }
//             
//             if (customerCommand.getPhoneNo()!= null && customerCommand.getPhoneNo() != "") {
//                customerTO.setPhoneNo(customerCommand.getPhoneNo());
//            }
//             
//            if (customerCommand.getAddress2()!= null && customerCommand.getAddress2() != "") {
//                customerTO.setAddress2(customerCommand.getAddress2());
//            }
//            
//            if (customerCommand.getEmail()!= null && customerCommand.getEmail() != "") {
//                customerTO.setEmail(customerCommand.getEmail());
//            }
//            
//             if (customerCommand.getGstNo()!= null && customerCommand.getGstNo() != "") {
//                customerTO.setGstNo(customerCommand.getEmail());
//            }
//             
//              if (customerCommand.getState()!= null && customerCommand.getState() != "") {
//                customerTO.setState(customerCommand.getState());
//            }
//            path = "content/Customer/manageBranch.jsp";
//              
//            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
//                update = customerBP.processInsertBranch(customerTO, userId);
//            } else {
//               status = customerBP.processInsertBranch(customerTO, userId);
//            }
//
//            CustomerList = customerBP.processBranchLists(customerTO);
//            request.setAttribute("CustomerLists", CustomerList);
//            if (status > 0) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Branch Added Successfully");
//            }
//            if (update > 0) {
//                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Branch Update Successfully");
//            }
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }  catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
// public ModelAndView viewEditBranch (HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
//        if (request.getSession().isNew()) {
//            return new ModelAndView("content/common/login.jsp");
//        }
//        HttpSession session = request.getSession();
//        String path = "";
//        String menuPath = "";
//        menuPath = "Branch >>Branch Details >> View / Edit Branch ";
//        String pageTitle = "View / Edit Branch";
//        request.setAttribute("pageTitle", pageTitle);
//        CustomerTO customerTO = new CustomerTO();
//        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
//        try {
//            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            path = "content/Customer/editBranch.jsp";
//            
//            String customerId = "";
//            customerId = request.getParameter("customerId");
//            customerTO.setCustomerId(customerId);
//            
//            ArrayList getBranchDetails = new ArrayList();
//            getBranchDetails = customerBP.getBranchDetails(customerTO);
//            request.setAttribute("getCustomerDetails", getBranchDetails);
//
//        } catch (FPRuntimeException exception) {
//            /*
//             * run time exception has occurred. Directed to error page.
//             */
//            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
//            return new ModelAndView("content/comon/error.jsp");
//        } catch (Exception exception) {
//            exception.printStackTrace();
//            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
//            return new ModelAndView("content/common/error.jsp");
//        }
//        return new ModelAndView(path);
//    }
// BENJAMIN ADDED CODE
    public ModelAndView viewEditWarehouse(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Warehouse >>Warehouse Details >> View / Edit Warehouse ";
        String pageTitle = "View / Edit Warehouse";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            path = "content/Customer/editWarehouse.jsp";
            
            String customerId = "";
            customerId = request.getParameter("customerId");
            customerTO.setCustomerId(customerId);
            
            ArrayList getBranchDetails = new ArrayList();
            getBranchDetails = customerBP.getWarehouseDetails(customerTO);
            request.setAttribute("getCustomerDetails", getBranchDetails);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleBranchUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Master  >> BranchMaster ";
        String pageTitle = "BranchMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        
        try {
            
            String custId = request.getParameter("custId");
            String branchId = request.getParameter("branchId");
            String customerId = request.getParameter("customerId");
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            
            ArrayList branchList = new ArrayList();
            branchList = customerBP.processBranchList(customerTO);
            request.setAttribute("branchList", branchList);
            
            ArrayList customerGroup = new ArrayList();
            customerGroup = customerBP.getCustomerGroupList();
            request.setAttribute("customerGroup", customerGroup);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            CustomerTO tpTO1 = new CustomerTO();
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList CustomerList = new ArrayList();
            CustomerList = customerBP.processBranchLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            
            path = "content/Customer/manageBranch.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
        
    }
    
    public ModelAndView handleViewAddBranchScreen(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Branch >>Branch Details >> Add Branch ";
        String pageTitle = "Add Customer";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                
                ArrayList CityList = new ArrayList();
                CityList = customerBP.processCityList();
                request.setAttribute("CityList", CityList);
                
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);
                
                ArrayList hubList = new ArrayList();
                hubList = customerBP.processHubList();
                request.setAttribute("hubList", hubList);
                path = "content/Customer/addBranch.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Branch Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddBranch(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBranchName() != null && customerCommand.getBranchName() != "") {
                customerTO.setBranchName(customerCommand.getBranchName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getCustName());
            if (customerCommand.getBranchCode() != null && customerCommand.getBranchCode() != "") {
                customerTO.setBranchCode(customerCommand.getBranchCode());
            }
            if (customerCommand.getBranchAddress() != null && customerCommand.getBranchAddress() != "") {
                customerTO.setBranchAddress(customerCommand.getBranchAddress());
            }
            if (customerCommand.getBranchCity() != null && customerCommand.getBranchCity() != "") {
                customerTO.setBranchCity(customerCommand.getBranchCity());
            }
            if (customerCommand.getBranchPercent() != null && customerCommand.getBranchPercent() != "") {
                customerTO.setBranchPercent(customerCommand.getBranchPercent());
            }
            if (customerCommand.getHub() != null && customerCommand.getHub() != "") {
                customerTO.setHub(customerCommand.getHub());
            }
            
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setState(customerCommand.getState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            
            path = "content/Customer/manageBranch.jsp";
            
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                update = customerBP.processInsertBranch(customerTO, userId);
            } else {
                status = customerBP.processInsertBranch(customerTO, userId);
            }
            
            CustomerList = customerBP.processBranchLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Branch Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Branch Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddHub(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getHubName() != null && customerCommand.getHubName() != "") {
                customerTO.setHubName(customerCommand.getHubName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getHubName());
            
            if (customerCommand.getHubAddress() != null && customerCommand.getHubAddress() != "") {
                customerTO.setHubAddress(customerCommand.getHubAddress());
            }
            if (customerCommand.getHubCity() != null && customerCommand.getHubCity() != "") {
                customerTO.setHubCity(customerCommand.getHubCity());
            }
            if (customerCommand.getHub() != null && customerCommand.getHub() != "") {
                customerTO.setHub(customerCommand.getHub());
            }
            
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setState(customerCommand.getState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            path = "content/Customer/manageHub.jsp";
            
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                update = customerBP.processInsertBranch(customerTO, userId);
            } else {
                status = customerBP.processInsertHub(customerTO, userId);
            }
            
            CustomerList = customerBP.processHubLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Hub Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleAddWareHouse(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getWareHouseName() != null && customerCommand.getWareHouseName() != "") {
                customerTO.setWareHouseName(customerCommand.getWareHouseName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getWareHouseName());
            
            if (customerCommand.getWareHouseAddress() != null && customerCommand.getWareHouseAddress() != "") {
                customerTO.setWareHouseAddress(customerCommand.getWareHouseAddress());
            }
            if (customerCommand.getHubCity() != null && customerCommand.getHubCity() != "") {
                customerTO.setHubCity(customerCommand.getHubCity());
            }
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setState(customerCommand.getState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            
            String warehouseCode = request.getParameter("bfilCode");
            customerTO.setWhCode(warehouseCode);
            
            path = "content/Customer/manageWarehouse.jsp";
            
            if (customerCommand.getCustomerId() != null && customerCommand.getCustomerId() != "") {
                update = customerBP.processInsertBranch(customerTO, userId);
            } else {
                status = customerBP.processInsertWareHouse(customerTO, userId);
            }
            
            CustomerList = customerBP.processWareHouseLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New WareHouse Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "WareHouse Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewEditBranch(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Branch >>Branch Details >> View / Edit Branch ";
        String pageTitle = "View / Edit Branch";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            ArrayList hubList = new ArrayList();
            hubList = customerBP.processHubList();
            request.setAttribute("hubList", hubList);
            
            path = "content/Customer/editBranch.jsp";
            
            String customerId = "";
            customerId = request.getParameter("customerId");
            customerTO.setCustomerId(customerId);
            
            ArrayList getBranchDetails = new ArrayList();
            getBranchDetails = customerBP.getBranchDetails(customerTO);
            request.setAttribute("getCustomerDetails", getBranchDetails);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView viewEditHub(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Branch >>Branch Details >> View / Edit Branch ";
        String pageTitle = "View / Edit Branch";
        request.setAttribute("pageTitle", pageTitle);
        CustomerTO customerTO = new CustomerTO();
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            path = "content/Customer/editHub.jsp";
            
            String customerId = "";
            customerId = request.getParameter("customerId");
            customerTO.setCustomerId(customerId);
            
            ArrayList getBranchDetails = new ArrayList();
            getBranchDetails = customerBP.getHubDetails(customerTO);
            request.setAttribute("getCustomerDetails", getBranchDetails);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleHubUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Master  >> BranchMaster ";
        String pageTitle = "BranchMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        
        try {
            
            String custId = request.getParameter("custId");
            String branchId = request.getParameter("branchId");
            String customerId = request.getParameter("customerId");
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            
            ArrayList branchList = new ArrayList();
            branchList = customerBP.processBranchList(customerTO);
            request.setAttribute("branchList", branchList);
            
            ArrayList customerGroup = new ArrayList();
            customerGroup = customerBP.getCustomerGroupList();
            request.setAttribute("customerGroup", customerGroup);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            CustomerTO tpTO1 = new CustomerTO();
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList CustomerList = new ArrayList();
            CustomerList = customerBP.processHubLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            
            path = "content/Customer/manageHub.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
        
    }
    
    public ModelAndView handleViewAddHubScreen(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Branch >>Branch Details >> Add Branch ";
        String pageTitle = "Add Customer";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                
                ArrayList CityList = new ArrayList();
                CityList = customerBP.processCityList();
                request.setAttribute("CityList", CityList);
                
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);
                
                ArrayList hubList = new ArrayList();
                hubList = customerBP.processWarehouseList();
                request.setAttribute("hubList", hubList);
                path = "content/Customer/addHub.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Branch Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleViewAddWareHouse(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Branch >>Branch Details >> Add Branch ";
        String pageTitle = "Add Customer";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Customer-Add")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                
                ArrayList CityList = new ArrayList();
                CityList = customerBP.processCityList();
                request.setAttribute("CityList", CityList);
                
                ArrayList stateList = new ArrayList();
                stateList = customerBP.getStateList();
                request.setAttribute("stateList", stateList);
                
                ArrayList hubList = new ArrayList();
                hubList = customerBP.processWarehouseList();
                request.setAttribute("hubList", hubList);
                path = "content/Customer/addWareHouse.jsp";
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Branch Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleWareHouseUpdate(HttpServletRequest request, HttpServletResponse reponse, CustomerCommand command) throws IOException {
        System.out.println("ConfigDetails...");
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        customerCommand = command;
        String path = "";
        int status = 0;
        int userId = (Integer) session.getAttribute("userId");
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Master  >> BranchMaster ";
        String pageTitle = "BranchMaster ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        
        try {
            
            String custId = request.getParameter("custId");
            String branchId = request.getParameter("branchId");
            String customerId = request.getParameter("customerId");
            if (customerId != null) {
                customerTO.setCustomerId(customerId);
            }
            
            ArrayList branchList = new ArrayList();
            branchList = customerBP.processBranchList(customerTO);
            request.setAttribute("branchList", branchList);
            
            ArrayList customerGroup = new ArrayList();
            customerGroup = customerBP.getCustomerGroupList();
            request.setAttribute("customerGroup", customerGroup);
            
            ArrayList stateList = new ArrayList();
            stateList = customerBP.getStateList();
            request.setAttribute("stateList", stateList);
            
            ArrayList cityList = new ArrayList();
            cityList = customerBP.processCityList();
            request.setAttribute("CityList", cityList);
            
            ArrayList CustomerList = new ArrayList();
            CustomerList = customerBP.processWareHouseLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            
            path = "content/Customer/manageWarehouse.jsp";
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Bunk Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
        
    }

    public ModelAndView handleEditWareHouse(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getWareHouseName() != null && customerCommand.getWareHouseName() != "") {
                customerTO.setWareHouseName(customerCommand.getWareHouseName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getWareHouseName());
            
            if (customerCommand.getWareHouseAddress() != null && customerCommand.getWareHouseAddress() != "") {
                customerTO.setWareHouseAddress(customerCommand.getWareHouseAddress());
            }
            if (customerCommand.getHubCity() != null && customerCommand.getHubCity() != "") {
                customerTO.setHubCity(customerCommand.getHubCity());
            }
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getState() != null && customerCommand.getState() != "") {
                customerTO.setState(customerCommand.getState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            if (customerCommand.getWhID() != null && customerCommand.getWhID() != "") {
                customerTO.setWhID(customerCommand.getWhID());
            }
            
            String warehouseCode = request.getParameter("bfilCode");
            customerTO.setWhCode(warehouseCode);
            
            path = "content/Customer/manageWarehouse.jsp";
            
            status = customerBP.processUpdateWareHouse(customerTO, userId);
            
            CustomerList = customerBP.processWareHouseLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New WareHouse Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "WareHouse Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleEditHub(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getHubName() != null && customerCommand.getHubName() != "") {
                customerTO.setHubName(customerCommand.getHubName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getHubName());
            
            if (customerCommand.getHubAddress() != null && customerCommand.getHubAddress() != "") {
                customerTO.setHubAddress(customerCommand.getHubAddress());
            }
            if (customerCommand.getHubCity() != null && customerCommand.getHubCity() != "") {
                customerTO.setHubCity(customerCommand.getHubCity());
            }
            if (customerCommand.getHub() != null && customerCommand.getHub() != "") {
                customerTO.setHub(customerCommand.getHub());
            }
            
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getCustState() != null && customerCommand.getCustState() != "") {
                customerTO.setCustState(customerCommand.getCustState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            
            if (customerCommand.getHubId() != null && customerCommand.getHubId() != "") {
                customerTO.setHubId(customerCommand.getHubId());
            }
            path = "content/Customer/manageHub.jsp";
            
            status = customerBP.processUpdateHub(customerTO, userId);
            
            CustomerList = customerBP.processHubLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Hub Updated Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Hub Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView handleEditBranch(HttpServletRequest request, HttpServletResponse response, CustomerCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        int update = 0;
        String path = "";
        customerCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList CustomerList = new ArrayList();
        CustomerTO customerTO = new CustomerTO();
        String menuPath = "";
        menuPath = "Branch  >>  Branch Details ";
        String pageTitle = "View  Branch ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            if (customerCommand.getBranchName() != null && customerCommand.getBranchName() != "") {
                customerTO.setBranchName(customerCommand.getBranchName());
            }
            
            System.out.println("customerCommand.getCustName()--------->" + customerCommand.getCustName());
            if (customerCommand.getBranchCode() != null && customerCommand.getBranchCode() != "") {
                customerTO.setBranchCode(customerCommand.getBranchCode());
            }
            if (customerCommand.getBranchAddress() != null && customerCommand.getBranchAddress() != "") {
                customerTO.setBranchAddress(customerCommand.getBranchAddress());
            }
            if (customerCommand.getBranchCity() != null && customerCommand.getBranchCity() != "") {
                customerTO.setBranchCity(customerCommand.getBranchCity());
            }
            if (customerCommand.getBranchPercent() != null && customerCommand.getBranchPercent() != "") {
                customerTO.setBranchPercent(customerCommand.getBranchPercent());
            }
            if (customerCommand.getHub() != null && customerCommand.getHub() != "") {
                customerTO.setHub(customerCommand.getHub());
            }
            
            if (customerCommand.getPhoneNo() != null && customerCommand.getPhoneNo() != "") {
                customerTO.setPhoneNo(customerCommand.getPhoneNo());
            }
            
            if (customerCommand.getAddress2() != null && customerCommand.getAddress2() != "") {
                customerTO.setAddress2(customerCommand.getAddress2());
            }
            
            if (customerCommand.getEmail() != null && customerCommand.getEmail() != "") {
                customerTO.setEmail(customerCommand.getEmail());
            }
            
            if (customerCommand.getGstNo() != null && customerCommand.getGstNo() != "") {
                customerTO.setGstNo(customerCommand.getGstNo());
            }
            
            if (customerCommand.getCustState() != null && customerCommand.getCustState() != "") {
                customerTO.setCustState(customerCommand.getCustState());
            }
            if (customerCommand.getActiveInd() != null && customerCommand.getActiveInd() != "") {
                customerTO.setActiveInd(customerCommand.getActiveInd());
            }
            if (customerCommand.getBranchId() != null && customerCommand.getBranchId() != "") {
                customerTO.setBranchId(customerCommand.getBranchId());
            }
            
            path = "content/Customer/manageBranch.jsp";
            
            status = customerBP.processUpdateBranch(customerTO, userId);
            
            CustomerList = customerBP.processBranchLists(customerTO);
            request.setAttribute("CustomerLists", CustomerList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Branch Added Successfully");
            }
            if (update > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Branch Update Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert customer Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
}
