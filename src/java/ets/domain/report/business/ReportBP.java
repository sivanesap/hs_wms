/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.FPUtil;
import ets.domain.report.data.ReportDAO;
import ets.domain.section.data.SectionDAO;
import ets.domain.util.ThrottleConstants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sabreesh
 */
public class ReportBP {

    private ReportDAO reportDAO;
    private SectionDAO sectionDAO;
    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));

    public ReportDAO getReportDAO() {
        return reportDAO;
    }

    public void setReportDAO(ReportDAO repDAO) {
        this.reportDAO = repDAO;
    }

    public static FPUtil getFpUtil() {
        return fpUtil;
    }

    public static void setFpUtil(FPUtil fpUtil) {
        ReportBP.fpUtil = fpUtil;
    }

    public SectionDAO getSectionDAO() {
        return sectionDAO;
    }

    public void setSectionDAO(SectionDAO sectionDAO) {
        this.sectionDAO = sectionDAO;
    }

    public ArrayList processBillList(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = reportDAO.getBillList(mrsTO, fromDate, toDate);
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrList;
    }

    public ArrayList processSalesTaxSummary(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrSalesList = new ArrayList();
        MfrSalesList = reportDAO.getSalesBillTaxSummary(mrsTO, fromDate, toDate);
        System.out.println("salesTax size=" + MfrSalesList.size());
        if (MfrSalesList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrSalesList;
    }

    public ArrayList processPurchaseReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList purchaseList = new ArrayList();
        purchaseList = reportDAO.getPurchaseList(repTO);
        if (purchaseList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return purchaseList;
    }

    public ArrayList processComplaintList(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList complaintList = new ArrayList();
        complaintList = reportDAO.getComplaintList(repTO, fromDate, toDate);

        if (complaintList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return complaintList;
    }

    public ArrayList processStockWorthReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthList = new ArrayList();
        stockWorthList = reportDAO.getStockWorthList(repTO);
        if (stockWorthList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return stockWorthList;
    }

    public ArrayList processRateList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getRateList(repTO);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processPeriodicServiceList(int companyId, String regNo, String date) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getPeriodicServiceVehicleList(companyId, regNo, date);
        //rateList = reportDAO.getPeriodicServiceList(companyId, regNo, date);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processInsuranceDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getInsuranceDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processRoadTaxDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getRoadTaxDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processPermitDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getPermitDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processAMCDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.processAMCDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processBodyBillList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillList = new ArrayList();
        bodyBillList = reportDAO.getbodyBillList(repTO);
        if (bodyBillList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return bodyBillList;
    }

    public ArrayList processContractorActivities(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ContractorActivities = new ArrayList();
        ContractorActivities = reportDAO.getContractorActivities(repTO);
        if (ContractorActivities.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return ContractorActivities;
    }

    public ArrayList processBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processHikedBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getHikedbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList BillDetails = new ArrayList();
        BillDetails = reportDAO.getBillDetails(repTO);
        return BillDetails;
    }

    public ArrayList processActivityList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ActivityList = new ArrayList();
        ActivityList = reportDAO.getActivityList(repTO);
        return ActivityList;
    }

    public ArrayList processTotalPrices(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList TotalPrices = new ArrayList();
        TotalPrices = reportDAO.getTotalPrices(repTO);
        return TotalPrices;
    }

    public ArrayList processStockIssueReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStockIssueReport(repTO);
        if (IssueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processStoresEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStoresEffReport(repTO);
        if (IssueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processReceivedStockReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockReport(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processReceivedStockTaxSummary(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockTaxSummary(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processInVoiceItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getInvoiceItems(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processRCWOItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        ArrayList List = new ArrayList();
        receivedList = reportDAO.getRCWOItems(repTO);
        Iterator itr;
        itr = receivedList.iterator();
        ReportTO purch = null;
        String remarks = "";
        while (itr.hasNext()) {
            purch = new ReportTO();
            purch = (ReportTO) itr.next();
            remarks = purch.getRemarks();
            purch.setRemarks(remarks);
            /*
             if (remarks.length() >= 150) {
             remarks = remarks.substring(0, 150);
             }
             */
            purch.setAddressSplit(addressSplit(purch.getAddress()));
            purch.setRemarksSplit(remarksSplit(remarks, 75));
            List.add(purch);
        }

        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public ArrayList processGdDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        String fromService = "";
        String toService = "";
        serviceEffList = reportDAO.getGdDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");

        }
        return serviceEffList;
    }

    public String[] addressSplit(String longText) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public String[] remarksSplit(String longText, int textLength) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = textLength;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public ArrayList processServiceSummary(String usageTypeId, String ServiceTypId, String custId, String compId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getServiceSummary(usageTypeId, ServiceTypId, custId, compId, fromDate, toDate);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processMovingAvgReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList MovingAvg = new ArrayList();
        ArrayList MovingAverage = new ArrayList();
        float stockBalance = 0.0f;
        int itemId = 0;
        int companyId = Integer.parseInt(repTO.getCompanyId());
        String lastPurchased = "";

        System.out.println("Moving avg Com-->" + companyId);
        MovingAvg = reportDAO.getMovingAvgReport(repTO);
        if (MovingAvg.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {

            Iterator itr = MovingAvg.iterator();
            ReportTO report = new ReportTO();
            while (itr.hasNext()) {
                report = (ReportTO) itr.next();
                //System.out.println("report.getSpltQty()" + report.getSpltQty());
                String monthSplit[] = report.getSpltQty().split("-");
                //System.out.println("MonthSplit-->length" + monthSplit.length);

                report.setFirstHalfDays(monthSplit[0]);
                report.setLastHalfDays(monthSplit[1]);

                itemId = reportDAO.getItemId(report.getPaplCode());
                stockBalance = sectionDAO.getItemStock(itemId, Integer.parseInt(repTO.getCompanyId()));
                //System.out.println("stockBalance-->"+stockBalance);
                report.setItemQty(stockBalance + "");

                lastPurchased = sectionDAO.getItemStockLastPurchased(itemId, companyId);
                //System.out.println("In Section BP LastPurchased Have-->" + lastPurchased);

                String temp[] = lastPurchased.split("-");
                //System.out.println("Qty-->" + temp[0] + "Price-->" + temp[1]);

                report.setPrice(Float.valueOf(temp[1]));
                MovingAverage.add(report);
            }
        }
        return MovingAverage;
    }

    public ArrayList processSerEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getSerEffReport(repTO);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStRequestList(String fromdate, String toDate, int fromId, int toId, String type) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStRequestList(fromdate, toDate, fromId, toId, type);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillList(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillList(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillTaxSummary(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillTaxSummary(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceCostList = new ArrayList();
        serviceCostList = reportDAO.getServiceCostList(repTO);
        if (serviceCostList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceCostList;
    }

    public ArrayList processWoList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getWoList(repTO);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList processWarrantyServiceList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceList = new ArrayList();
        serviceList = reportDAO.getWarrantyServiceList(repTO);
        if (serviceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceList;
    }

    public ArrayList processTechniciansList() throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = reportDAO.techniciansList();
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-03");
        }
        return tempList;

    }

    public ArrayList processRcItemList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcItemList = new ArrayList();
        RcItemList = reportDAO.getRcItemList(repTO);
        if (RcItemList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcItemList;
    }

    public ArrayList processRcHistoryList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcHistoryList = new ArrayList();
        RcHistoryList = reportDAO.getRcHistoryList(repTO);
        if (RcHistoryList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcHistoryList;
    }

    public ArrayList processTyreList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList tyreList = new ArrayList();
        tyreList = reportDAO.getTyreList(repTO);
        if (tyreList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return tyreList;
    }

    public ArrayList processOrdersList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList tyreList = new ArrayList();
        tyreList = reportDAO.getOrdersList(repTO);
        if (tyreList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return tyreList;
    }

    public ArrayList processStockPurchase(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getStockPurchase(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processRcBillsReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getRcBillsReport(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processStatusReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList lastList = new ArrayList();
        List = reportDAO.getStatusReport(repTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Date dat = new Date();
            System.out.println("st" + dat.getSeconds());
            Iterator itr = List.iterator();
            ReportTO reportTO = null;
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                lastList = reportDAO.getPrevious(reportTO.getJobCardId(), reportTO.getRegNo());
                Iterator itr1 = lastList.iterator();
                ReportTO repotTO = null;
                if (itr1.hasNext()) {
                    repotTO = new ReportTO();
                    repotTO = (ReportTO) itr1.next();

                    reportTO.setLastProblem(reportDAO.getLastProblem(repotTO.getJobCardId()));
                    reportTO.setLastRemarks(repotTO.getLastRemarks());
                    reportTO.setLastKm(repotTO.getLastKm());
                    reportTO.setLastStatus(repotTO.getLastStatus());
                    reportTO.setLastTech(repotTO.getLastTech());
                    reportTO.setLastDate(repotTO.getLastDate());
                }
            }
            Date dat1 = new Date();
            System.out.println("et" + dat1.getSeconds());
            System.out.println("tt" + (dat1.getSeconds() - dat.getSeconds()));

        }
        return List;
    }

    public ArrayList processTaxwiseItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getTaxwiseItems(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processVehMfrComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getMfrVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getMfrVehCount(reportTO.getMfrId()));
        }
        return List;
    }

    public ArrayList processVehModelComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getModelVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getModelVehCount(reportTO.getMfrId(), reportTO.getModelId()));
        }
        return List;
    }

    public ArrayList processVehAgeComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList List1 = new ArrayList();
        int count = 0;
        List = reportDAO.getVehAgeComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            if (reportTO.getVehCount().equalsIgnoreCase("0")) {
                count++;

            }
            System.out.println("reportTO.getVehCount()" + reportTO.getVehCount());
        }
        if (count == 6) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    // rajesh
    public ArrayList salesTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList salesTrendGraph = new ArrayList();
        salesTrendGraph = reportDAO.salesTrendGraph(reportTO);
        return salesTrendGraph;
    }

    public ArrayList vendorTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vendorTrendGraph = new ArrayList();
        vendorTrendGraph = reportDAO.vendorTrendGraph(reportTO);
        return vendorTrendGraph;
    }

    public ArrayList mfr() throws FPRuntimeException, FPBusinessException {

        ArrayList mfr = new ArrayList();
        mfr = reportDAO.mfr();
        return mfr;
    }

    public ArrayList usage() throws FPRuntimeException, FPBusinessException {

        ArrayList usage = new ArrayList();
        usage = reportDAO.usage();
        return usage;
    }

    public ArrayList vehicleType() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleType = new ArrayList();
        vehicleType = reportDAO.vehicleType();
        return vehicleType;
    }

    public String processItemSuggests(String vendorName) {
        String vendorNames = "";
        vendorName = vendorName + "%";
        vendorNames = reportDAO.getItemSuggests(vendorName);
        return vendorNames;
    }

    public String problemItemSuggests(String problem) {
        String problems = "";
        problem = problem + "%";
        problems = reportDAO.getproblemSuggests(problem);
        return problems;
    }

    public ArrayList problemDistributionGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList problemDistributionGraph = new ArrayList();
        problemDistributionGraph = reportDAO.problemDistributionGraph(reportTO);
        return problemDistributionGraph;
    }

    public ArrayList getColorList(int sizeValue) throws FPRuntimeException, FPBusinessException {
        ArrayList colorList = new ArrayList();
        colorList = reportDAO.getColorList(sizeValue);
        return colorList;
    }

    public ArrayList categoryRcReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryRcReport = new ArrayList();
        categoryRcReport = reportDAO.categoryRcReport(reportTO);
        return categoryRcReport;
    }

    public ArrayList categoryNewReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryNewReport = new ArrayList();
        categoryNewReport = reportDAO.categoryNewReport(reportTO);
        return categoryNewReport;
    }
    // end rajesh

    //shankar
    public ArrayList getActiveCategories() throws FPRuntimeException, FPBusinessException {

        ArrayList categories = new ArrayList();
        categories = reportDAO.getActiveCategories();
        return categories;
    }

    public ArrayList stockWorthGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthGraph = new ArrayList();
        stockWorthGraph = reportDAO.stockWorthGraph(reportTO);
        return stockWorthGraph;
    }

    public ArrayList CompanyNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList companyNameList = new ArrayList();
        companyNameList = reportDAO.CompanyNameList(reportTO);
        return companyNameList;
    }

    public ArrayList monthNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList monthNameList = new ArrayList();
        monthNameList = reportDAO.monthNameList(reportTO);
        return monthNameList;
    }

    public ArrayList getManufacturerList() throws FPRuntimeException, FPBusinessException {

        ArrayList manufacturerList = new ArrayList();
        manufacturerList = reportDAO.getManufacturerList();
        return manufacturerList;
    }

    public ArrayList getUsageTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageTypeList = new ArrayList();
        usageTypeList = reportDAO.getUsageTypeList();
        return usageTypeList;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = reportDAO.getVehicleTypeList();
        return vehicleTypeList;
    }

    public ArrayList processCategoryStRequestList(String fromDate, String toDate, int fromSpId, int toSpId) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.processCategoryStRequestList(fromDate, toDate, fromSpId, toSpId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }
    // end shankar

    public String getVatPercentages() throws FPRuntimeException, FPBusinessException {
        String vatList = "";
        vatList = reportDAO.getVatPercentages();
        return vatList;
    }

    public ArrayList processTaxwiseServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String[] billNo = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwiseServiceCostList(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                billNo = reportTO.getBillNo().split(",");
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    Amount = reportDAO.getVatTotalAmount(billNo, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);
                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    vatValues.add(report);
                }
                Amount = 0.0f;
                report = new ReportTO();
                Amount = reportDAO.getContractAmount(billNo);
                report.setAmount(Amount);
                reportTO.setContractAmnt(Amount + "");
                report.setTaxAmount(0.0f);
                System.out.println("contr" + Amount);
                vatValues.add(report);
                reportTO.setVatValues(vatValues);
                System.out.println("service tax amount in BP++:" + reportTO.getTax());
            }

        }
        return serviceEffList;
    }

    public ArrayList processTaxwisePO(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String supplyId = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwisePO(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();

                reportTO = (ReportTO) itr.next();
                supplyId = reportTO.getSupplyId();
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    System.out.println("supplyId" + supplyId + "tax per" + temp[j]);
                    Amount = reportDAO.getPOVatTotalAmount(supplyId, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);

                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    System.out.println("Amount" + Amount + "tax" + report.getTaxAmount());
                    vatValues.add(report);
                }
                reportTO.setVatValues(vatValues);
            }

        }
        return serviceEffList;
    }

    public ArrayList gettopProblem(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList gettopProblem = new ArrayList();
        gettopProblem = reportDAO.gettopProblem(reportTO);
        return gettopProblem;
    }

    //Hari
    public ArrayList processServiceChartData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getServiceChartData(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList handleServiceDailyMIS(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        ArrayList newList = new ArrayList();
        woList = reportDAO.handleServiceDailyMIS(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr;
            itr = woList.iterator();
            ReportTO rto = null;
            ReportTO rtoNew = new ReportTO();
            String problem = "";
            int jcId = 0;
            while (itr.hasNext()) {
                rto = new ReportTO();
                rto = (ReportTO) itr.next();
                if (jcId != Integer.parseInt(rto.getJobCardId())) {
                    if (jcId != 0) {
                        newList.add(rtoNew);
                    }
                    rtoNew = new ReportTO();
                    rtoNew = rto;
                } else {
                    rtoNew.setProblemName(rtoNew.getProblemName() + "," + rto.getProblemName());
                }
                jcId = Integer.parseInt(rto.getJobCardId());

            }
        }
        System.out.println("wolist:" + woList.size());
        System.out.println("newlist:" + newList.size());
        return newList;
    }

    public ArrayList processRcExpenseGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList rcExpenseSummary = new ArrayList();
        rcExpenseSummary = reportDAO.getRcExpenseData(reportTO);
        if (rcExpenseSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseSummary;
    }

    public ArrayList processScrapGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList scrapValue = new ArrayList();
        scrapValue = reportDAO.getScrapGraphData(fromDate, toDate);
        if (scrapValue.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return scrapValue;
    }

    public ArrayList processGetColourList() throws FPRuntimeException, FPBusinessException {

        ArrayList colourList = new ArrayList();
        colourList = reportDAO.getColourList();
        if (colourList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return colourList;
    }

    public ArrayList processGetContractVendor(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList contractVendor = new ArrayList();
        contractVendor = reportDAO.getContractVendor(vendorId);
        if (contractVendor.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return contractVendor;
    }

    public ArrayList processExternalLabourBillGraphData(String fromDate, String toDate, int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList externalLabour = new ArrayList();
        externalLabour = reportDAO.getExternalLabourBillGraphData(fromDate, toDate, vendorId);
        if (externalLabour.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return externalLabour;
    }


    /*
     * Return The Count of Parts send for Serviced and received by Service point
     */
    public ArrayList processRcTrendGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList rcServiceData = new ArrayList();
        rcServiceData = reportDAO.getRcTrendGraphData(fromDate, toDate);
        if (rcServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcServiceData;
    }

    public ArrayList processVehicleServiceGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleServiceData = new ArrayList();
        vehicleServiceData = reportDAO.getVehicleServiceGraphData(reportTO);
        if (vehicleServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleServiceData;
    }

    public ArrayList processMileageGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList mileageData = new ArrayList();
        mileageData = reportDAO.getMileageGraphData(reportTO);
        if (mileageData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mileageData;
    }

    public ArrayList processRcVendorList() throws FPRuntimeException, FPBusinessException {

        ArrayList rcVendorList = new ArrayList();
        rcVendorList = reportDAO.getRcVendorList();
        if (rcVendorList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcVendorList;
    }

    public ArrayList processRcExpenseAmount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rcExpenseAmount = new ArrayList();
        rcExpenseAmount = reportDAO.getRcExpenseAmount(reportTO);
        if (rcExpenseAmount.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseAmount;
    }

    public Float processActualPrice(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        Float actualPrice = 0.0f;
        actualPrice = reportDAO.getActualPrice(reportTO);

        return actualPrice;
    }
//    bala

    public ArrayList processUsageTypewiseData(ReportTO report) throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        usageList = reportDAO.getUsageTypewiseData(report);
        if (usageList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return usageList;
    }
//    public ArrayList processServiceTypewiseSummary(ReportTO report) throws FPRuntimeException, FPBusinessException {
//
//            ArrayList serviceList = new ArrayList();
//            serviceList = reportDAO.getServiceTypewiseData(report);
//            if (serviceList.size() == 0) {
//                throw new FPBusinessException("EM-GEN-01");
//            }
//            return serviceList;
//    }
//    bala ends

    public ArrayList processTallyXMLSummary(int companyId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList tallyXMLSummary = new ArrayList();
        tallyXMLSummary = reportDAO.getTallyXMLSummary(companyId, fromDate, toDate);
        if (tallyXMLSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        System.out.println("tallyXMLSummarySize in BP:" + tallyXMLSummary.size());
        return tallyXMLSummary;
    }

    public int processTallyXMLReport(ReportTO report) throws FPRuntimeException, FPBusinessException {

        int tallyXMLReport = 0;
        tallyXMLReport = reportDAO.getTallyXMLReport(report);
        if (tallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        System.out.println("tallyXMLReportSize in BP:" + tallyXMLReport);
        return tallyXMLReport;
    }

    public ArrayList processModifyTallyXMLPage(String xmlId) throws FPRuntimeException, FPBusinessException {

        ArrayList modifyTallyXMLPage = new ArrayList();
        modifyTallyXMLPage = reportDAO.getModifyTallyXMLPage(xmlId);
        if (modifyTallyXMLPage.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        System.out.println("tallyXMLReportSize in BP:" + modifyTallyXMLPage.size());
        return modifyTallyXMLPage;
    }

    public int processModifyTallyXMLReport(ReportTO report, String xmlId) throws FPRuntimeException, FPBusinessException {

        int modifyTallyXMLReport = 0;
        modifyTallyXMLReport = reportDAO.getModifyTallyXMLReport(report, xmlId);
        if (modifyTallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        System.out.println("modifyTallyXMLReportSize in BP:" + modifyTallyXMLReport);
        return modifyTallyXMLReport;
    }

    public String handleDriverSettlement(String driName) {
        String driverName = "";
        driName = driName + "%";
        driverName = reportDAO.handleDriverSettlement(driName);
        return driverName;
    }

    public String handleVehicleNo(String regno) {
        String vehileRegNo = "";
        regno = regno + "%";
        vehileRegNo = reportDAO.handleVehicleNo(regno);
        return vehileRegNo;
    }

    public ArrayList driverSettlementReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverSettlementReport = new ArrayList();
        driverSettlementReport = reportDAO.getDriverSettlementReport(reportTO);
        if (driverSettlementReport.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        System.out.println("tallyXMLReportSize in BP:" + driverSettlementReport.size());
        return driverSettlementReport;
    }

    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList proDriverSettlement = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        proDriverSettlement = reportDAO.searchProDriverSettlement(fromDate, toDate, regNo, driId);
        if (proDriverSettlement.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList cleanerTrip = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        cleanerTrip = reportDAO.cleanerTripDetails(fromDate, toDate, regNo, driId);
        if (cleanerTrip.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fixedExpDetails = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        fixedExpDetails = reportDAO.getFixedExpDetails(fromDate, toDate, regNo, driId);
        System.out.println("fixedExpDetails.size().. BP : " + fixedExpDetails.size());
        if (fixedExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList driverExpDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        driverExpDetails = reportDAO.getDriverExpDetails(fromDate, toDate, regNo, driId);
        if (driverExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) throws FPBusinessException, FPRuntimeException {
        ArrayList AdvDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        AdvDetails = reportDAO.getAdvDetails(fromDate, toDate, regNo, driId, tripIds);
        if (AdvDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        /**
         * String[] temp = null; if (fromDate != "" && toDate != "") { temp =
         * fromDate.split("-"); String sDate = temp[2] + "-" + temp[1] + "-" +
         * temp[0]; fromDate = sDate; temp = toDate.split("-");
         *
         * String eDate = temp[2] + "-" + temp[1] + "-" + temp[0]; toDate =
         * eDate; }
         */
        fuelDetails = reportDAO.getFuelDetails(fromDate, toDate, regNo, driId);
        if (fuelDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList haltDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        haltDetails = reportDAO.getHaltDetails(fromDate, toDate, regNo, driId);
        if (haltDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return haltDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getRemarkDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getSummaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getSummaryDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        int status = 0;
        status = reportDAO.insertEposTripExpenses(tripId, expenseDesc, expenseAmt, expenseDate, expenseRemarks, userId);
        return status;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int tripCount = 0;
        tripCount = reportDAO.getTripCountDetails(fromDate, toDate, regNo, driId);
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int totalTonnage = 0;
        totalTonnage = reportDAO.getTotalTonnageDetails(fromDate, toDate, regNo, driId);
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int outKm = 0;
        outKm = reportDAO.getOutKmDetails(fromDate, toDate, regNo, driId);
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int inKm = 0;
        inKm = reportDAO.getInKmDetails(fromDate, toDate, regNo, driId);
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        ArrayList totalFuelDetails = new ArrayList();
        totalFuelDetails = reportDAO.getTotFuelDetails(fromDate, toDate, regNo, driId);
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverExpenseDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverSalaryDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int getExpense = 0;
        getExpense = reportDAO.getGeneralExpenseDetails(fromDate, toDate, regNo, driId);
        return getExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverAdvanceDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverBataDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public ArrayList getSettleCloseDetails(String fromDate, String toDate, String regNo, String driName) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        remarkDetails = reportDAO.getSettleCloseDetails(fromDate, toDate, regNo, driName);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getVehicleCurrentStatus(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleCurrentStatus = new ArrayList();
        vehicleCurrentStatus = reportDAO.getVehicleCurrentStatus(reportTO);
        return vehicleCurrentStatus;
    }

    public ArrayList getCompanyWiseTripReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList companyWiseTripReport = new ArrayList();
        companyWiseTripReport = reportDAO.getCompanyWiseTripReport(reportTO);
        return companyWiseTripReport;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList();
        return vehicleList;
    }

    public ArrayList getVehiclePerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePerformance = new ArrayList();
        vehiclePerformance = reportDAO.getVehiclePerformance(reportTO);
        return vehiclePerformance;
    }

    public ArrayList getDriverPerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverPerformance = new ArrayList();
        driverPerformance = reportDAO.getDriverPerformance(reportTO);
        return driverPerformance;
    }

    public String getDriverEmbarkedDate(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String driverEmbarked = "";
        driverEmbarked = reportDAO.getDriverEmbarkedDate(reportTO);
        return driverEmbarked;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlpsCount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsCount = new ArrayList();
        lpsCount = reportDAO.getlpsCount(reportTO);
        return lpsCount;
    }

    public ArrayList getlpsTrippedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsTrippedList = new ArrayList();
        lpsTrippedList = reportDAO.getlpsTrippedList(reportTO);
        return lpsTrippedList;
    }

    public ArrayList gettripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.gettripList(reportTO);
        return tripList;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlocationList() throws FPRuntimeException, FPBusinessException {
        ArrayList locationList = new ArrayList();
        locationList = reportDAO.getlocationList();
        return locationList;
    }

    public ArrayList getTripWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseList = new ArrayList();
        tripWiseList = reportDAO.getTripWisePandL(reportTO, userId);
        return tripWiseList;
    }

    public ArrayList getTripWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseTotal = new ArrayList();
        tripWiseTotal = reportDAO.getTripWisetotal(reportTO, userId);
        return tripWiseTotal;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseList = new ArrayList();
        vehicleWiseList = reportDAO.getVehicleWisePandL(reportTO, userId);
        return vehicleWiseList;
    }

    public ArrayList getVehicleWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseTotal = new ArrayList();
        vehicleWiseTotal = reportDAO.getVehicleWisetotal(reportTO, userId);
        return vehicleWiseTotal;
    }

    public ArrayList getCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerList();
        return customerList;
    }

    /**
     * This method used to District names List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictNameList() throws FPRuntimeException, FPBusinessException {
        ArrayList districtNameList = new ArrayList();
        districtNameList = reportDAO.getDistrictNameList();
        return districtNameList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictSummaryList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtSummaryList = new ArrayList();
        districtSummaryList = reportDAO.getDistrictSummaryList(reportTO, userId);
        return districtSummaryList;
    }

    /**
     * This method used to get District Wise Details Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtDetailsList = new ArrayList();
        districtDetailsList = reportDAO.getDistrictDetailsList(reportTO, userId);
        return districtDetailsList;
    }

    /**
     * This method used to get Trip Sheet Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsheetDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetDetailsList = new ArrayList();
        tripSheetDetailsList = reportDAO.getTripsheetDetailsList(reportTO, userId);
        return tripSheetDetailsList;
    }

    /**
     * This method used to get Consignee Wise Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsigneeWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList consigneeWiseDetailsList = new ArrayList();
        consigneeWiseDetailsList = reportDAO.getConsigneeWiseList(reportTO, userId);
        return consigneeWiseDetailsList;
    }

    /**
     * This method used to get Trip Settlement Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsettlementWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripsettlementDetailsList = new ArrayList();
        tripsettlementDetailsList = reportDAO.getTripsettlementWiseList(reportTO, userId);
        return tripsettlementDetailsList;
    }

    public ArrayList getDieselReportList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList dieselReportList = new ArrayList();
        dieselReportList = reportDAO.getDieselReportList(reportTO, userId);
        return dieselReportList;
    }

    /**
     * This method used to get ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsigneeNameSuggestions(String consigneeName) {
        //  System.out.println("i am in consignee name ajax ");
        String sugggestions = "";
        //System.out.println("consigneeName =  " + consigneeName);
        sugggestions = reportDAO.getConsigneeNameSuggestions(consigneeName);
        return sugggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getTripIdSuggestions(String tripId) {
        String sugggestions = "";
        sugggestions = reportDAO.getTripIdSuggestions(tripId);
        return sugggestions;
    }

    /**
     * This method used to get Vehicle Number Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVehicleNumberSuggestions(String vehicleNo) {
        String sugggestions = "";
        sugggestions = reportDAO.getVehicleNumberSuggestions(vehicleNo);
        return sugggestions;
    }

    public ArrayList getCustomerOverDueList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerOverDueList = new ArrayList();
        customerOverDueList = reportDAO.getCustomerOverDueList(reportTO, userId);
        return customerOverDueList;
    }

    public ArrayList getSalesDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getSalesDashboard(reportTO, userId);
        return response;
    }

    public ArrayList getOpsDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getOpsDashboard(reportTO, userId);
        return response;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountReceivableList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList accountReceivableList = new ArrayList();
        accountReceivableList = reportDAO.getAccountReceivableList(reportTO, userId);
        return accountReceivableList;
    }

    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerWiseProfitList = new ArrayList();
        customerWiseProfitList = reportDAO.getCustomerWiseProfitList(reportTO, userId);
        return customerWiseProfitList;
    }

    /**
     * This method used to get Vehicle Details List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        return vehicleDetailsList;

    }

    public ArrayList getTripSheetDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        ArrayList OperationExpensesList = new ArrayList();
        ArrayList tripEarnings = new ArrayList();
        ArrayList tripEarningsList = new ArrayList();
        tripDetails = reportDAO.getTripSheetDetails(reportTO);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = tripDetails.iterator();
        ReportTO repTO1 = new ReportTO();

        /*
         while (itr1.hasNext()) {
         rpTO = new ReportTO();
         repTO1 = (ReportTO) itr1.next();
         tripEarnings = reportDAO.getTripEarningsList(reportTO);
         reportTO.setTripId(repTO1.getTripSheetId());
         rpTO.setTripId(reportTO.getTripId());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setTripCode(repTO1.getTripCode());
         rpTO.setCustomerName(repTO1.getCustomerName());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setCustomerTypeId(repTO1.getCustomerTypeId());
         rpTO.setBillingType(repTO1.getBillingType());
         rpTO.setRouteName(repTO1.getRouteName());
         rpTO.setVehicleTypeName(repTO1.getVehicleTypeName());
         rpTO.setReeferRequired(repTO1.getReeferRequired());
         rpTO.setVehicleNo(repTO1.getVehicleNo());
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setDriverName(repTO1.getDriverName());
         rpTO.setCompanyName(repTO1.getCompanyName());
         System.out.println("the tripearnings list" + tripEarnings.size());
         if (tripEarnings.size() > 0) {
         Iterator itr2 = tripEarnings.iterator();
         while (itr2.hasNext()) {
         dprTO = (DprTO) itr2.next();
         System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
         System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
         rpTO.setVehicleId(dprTO.getVehicleId());
         rpTO.setTripId(dprTO.getTripNos());
         rpTO.setFreightAmount(dprTO.getFreightAmount());
         rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTripNos("0");
         rpTO.setFreightAmount("0.00");
         rpTO.setOtherExpenseAmount("0.00");
         //vehicleEarningsList.add(rpTO);
         }
         DprTO dpr = new DprTO();
         double driverSalary = 0;
         driverSalary = reportDAO.getTripDriverSalary(reportTO);
         reportTO.setVehicleDriverSalary(driverSalary);
         DprTO dpr1 = new DprTO();
         OperationExpensesList = reportDAO.getTripOperationExpenseList(reportTO);
         if (OperationExpensesList.size() > 0) {
         Iterator itr4 = OperationExpensesList.iterator();
         while (itr4.hasNext()) {
         dpr1 = (DprTO) itr4.next();
         rpTO.setTollAmount(dpr1.getTollAmount());
         rpTO.setFuelAmount(dpr1.getFuelAmount());
         rpTO.setDriverIncentive(dpr1.getDriverIncentive());
         rpTO.setDriverBata(dpr1.getDriverBata());
         rpTO.setMiscAmount(dpr1.getMiscAmount());
         rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
         rpTO.setRouteExpenses(dpr1.getRouteExpenses());
         rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
         rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata());
         System.out.println("repTO2.getmiscamount() = " + dpr1.getTollAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTollAmount(0.00);
         rpTO.setFuelAmount(0.00);
         rpTO.setDriverIncentive(0.00);
         rpTO.setDriverBata(0.00);
         rpTO.setDriverExpense(0.00);
         rpTO.setRouteExpenses(0.00);
         rpTO.setTripOtherExpense(0.00);
         rpTO.setTotlalOperationExpense(0.00);
         //vehicleEarningsList.add(rpTO);
         }
         rpTO.setNetExpense(rpTO.getTotlalOperationExpense());
         freightAmount = Double.parseDouble(rpTO.getFreightAmount());
         rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
         tripEarningsList.add(rpTO);
         }
         */
        return tripDetails;
    }

    /**
     * This method used to get Vehicle Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        ArrayList vehicleEarnings = new ArrayList();
        ArrayList vehicleFixedExpensesList = new ArrayList();
        ArrayList vehicleOperationExpensesList = new ArrayList();
        ArrayList vehicleMaintainExpensesList = new ArrayList();
        ArrayList vehicleEarningsList = new ArrayList();
        ArrayList EarningsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            reportTO.setVehicleId(repTO1.getVehicleId());
            vehicleEarnings = reportDAO.getVehicleEarningsList(reportTO, userId);
            if (vehicleEarnings.size() > 0) {
                Iterator itr2 = vehicleEarnings.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
                    System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
                    rpTO.setVehicleId(dprTO.getVehicleId());
                    rpTO.setTripId(dprTO.getTripId());
                    rpTO.setTripNos(dprTO.getTripNos());
                    rpTO.setFreightAmount(dprTO.getFreightAmount());
                    rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTripNos("0");
                rpTO.setFreightAmount("0.00");
                rpTO.setOtherExpenseAmount("0.00");
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr = new DprTO();
            double driverSalary = 0;
            driverSalary = reportDAO.getVehicleDriverSalary(reportTO, userId);
            reportTO.setVehicleDriverSalary(driverSalary);
            vehicleFixedExpensesList = reportDAO.getVehicleFixedExpenseList(reportTO, userId);
            Iterator itr3 = vehicleFixedExpensesList.iterator();
            while (itr3.hasNext()) {
                dpr = (DprTO) itr3.next();
                rpTO.setInsuranceAmount(dpr.getInsuranceAmount());
                rpTO.setFcAmount(dpr.getFcAmount());
                rpTO.setRoadTaxAmount(dpr.getRoadTaxAmount());
                rpTO.setPermitAmount(dpr.getPermitAmount());
                rpTO.setEmiAmount(dpr.getEmiAmount());
                rpTO.setFixedExpensePerDay(dpr.getFixedExpensePerDay());
                rpTO.setTotlalFixedExpense(dpr.getTotlalFixedExpense());
                rpTO.setVehicleDriverSalary(dpr.getVehicleDriverSalary());
                driverSalary = dpr.getVehicleDriverSalary();
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr1 = new DprTO();

            vehicleOperationExpensesList = reportDAO.getVehicleOperationExpenseList(reportTO, userId);
            if (vehicleOperationExpensesList.size() > 0) {
                Iterator itr4 = vehicleOperationExpensesList.iterator();
                while (itr4.hasNext()) {
                    dpr1 = (DprTO) itr4.next();
                    rpTO.setTollAmount(dpr1.getTollAmount());
                    rpTO.setFuelAmount(dpr1.getFuelAmount());
                    rpTO.setDriverIncentive(dpr1.getDriverIncentive());
                    rpTO.setDriverBata(dpr1.getDriverBata());
                    rpTO.setMiscAmount(dpr1.getMiscAmount());
                    rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
                    rpTO.setRouteExpenses(dpr1.getRouteExpenses());
                    rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
                    rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata() + dpr1.getTripOtherExpense() + driverSalary);
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTollAmount(0.00);
                rpTO.setFuelAmount(0.00);
                rpTO.setDriverIncentive(0.00);
                rpTO.setDriverBata(0.00);
                rpTO.setDriverExpense(0.00);
                rpTO.setRouteExpenses(0.00);
                rpTO.setTripOtherExpense(0.00);
                rpTO.setTotlalOperationExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr2 = new DprTO();
            vehicleMaintainExpensesList = reportDAO.getVehicleMaintainExpenses(reportTO, userId);
            if (vehicleMaintainExpensesList.size() > 0) {
                Iterator itr5 = vehicleMaintainExpensesList.iterator();
                while (itr5.hasNext()) {
                    dpr2 = (DprTO) itr5.next();
                    rpTO.setMaintainExpense(dpr2.getMaintainExpense());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setMaintainExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            rpTO.setNetExpense(rpTO.getTotlalFixedExpense() + rpTO.getTotlalOperationExpense() + rpTO.getMaintainExpense());
            freightAmount = Double.parseDouble(rpTO.getFreightAmount());
            rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
            vehicleEarningsList.add(rpTO);
        }
        return vehicleEarningsList;
    }

    public ArrayList getVehicleUtilizationList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUtilizationList = new ArrayList();
        vehicleUtilizationList = reportDAO.getVehicleUtilizationList(reportTO, userId);
        return vehicleUtilizationList;

    }

    public ArrayList getPopupCustomerProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerProfitReport = new ArrayList();
        popupCustomerProfitReport = reportDAO.getPopupCustomerProfitReportDetails(reportTO);
        return popupCustomerProfitReport;

    }

    public ArrayList getGPSLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList logDetails = new ArrayList();
        logDetails = reportDAO.getGPSLogDetails(reportTO, userId);
        return logDetails;

    }

    public ArrayList getDriverSettlementDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList driverSettlementDetails = new ArrayList();
        driverSettlementDetails = reportDAO.getDriverSettlementDetails(reportTO, userId);
        return driverSettlementDetails;

    }

    public ArrayList getTripLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripLogDetails = new ArrayList();
        tripLogDetails = reportDAO.getTripLogDetails(reportTO, userId);
        return tripLogDetails;

    }

    public ArrayList getBPCLTransactionDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getBPCLTransactionDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }

    public ArrayList getTripGpsStatusDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripGpsStatusDetails = new ArrayList();
        tripGpsStatusDetails = reportDAO.getTripGpsStatusDetails(reportTO);
        return tripGpsStatusDetails;

    }

    public ArrayList getTripCodeList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripCodeList = new ArrayList();
        tripCodeList = reportDAO.getTripCodeList(reportTO);
        return tripCodeList;
    }

    public ArrayList getTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getWflList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wflList = new ArrayList();
        wflList = reportDAO.getWflList(reportTO);
        return wflList;
    }

    public ArrayList getWfuList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wfuList = new ArrayList();
        wfuList = reportDAO.getWfuList(reportTO);
        return wfuList;
    }

    public ArrayList getTripInProgressList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripInProgressList = new ArrayList();
        tripInProgressList = reportDAO.getTripInProgressList(reportTO);
        return tripInProgressList;
    }

    public ArrayList getTripNotStartedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotStartedList = new ArrayList();
        tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
        return tripNotStartedList;
    }

    public ArrayList getJobCardList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = reportDAO.getJobCardList(reportTO);
        return jobCardList;
    }

    public ArrayList getFutureTripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList futureTripList = new ArrayList();
        futureTripList = reportDAO.getFutureTripList(reportTO);
        return futureTripList;
    }

    public int getWflWfuStatus(String currentDate) {
        int wflWfuStatus = 0;
        wflWfuStatus = reportDAO.getWflWfuStatus(currentDate);
        return wflWfuStatus;
    }

    public int getWflWfuLogId(String currentDate) {
        int wflwfuLogId = 0;
        wflwfuLogId = reportDAO.getWflWfuLogId(currentDate);
        return wflwfuLogId;
    }

    public String getTotalTripSummaryDetails(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getTotalTripSummaryDetails(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getTripSheetList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetList(reportTO);
        return tripSheetList;
    }

    public String getMarginWiseTripSummary(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getMarginWiseTripSummary(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getMonthWiseEmptyRunSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList monthWiseEmptyRunSummary = new ArrayList();
        monthWiseEmptyRunSummary = reportDAO.getMonthWiseEmptyRunSummary(reportTO);
        return monthWiseEmptyRunSummary;
    }

    public ArrayList getJobcardSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardSummary = new ArrayList();
        jobcardSummary = reportDAO.getJobcardSummary(reportTO);
        return jobcardSummary;
    }

    public ArrayList getTripMergingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripMergingDetails = new ArrayList();
        TripMergingDetails = reportDAO.getTripMergingDetails(reportTO, userId);
        return TripMergingDetails;

    }

    public ArrayList getVehicleReadingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleReadingDetails = new ArrayList();
        vehicleReadingDetails = reportDAO.getVehicleReadingDetails(reportTO, userId);
        return vehicleReadingDetails;

    }

    public ArrayList getTripMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripMergingList = new ArrayList();
        tripMergingList = reportDAO.getTripMergingList(reportTO, userId);
        return tripMergingList;
    }

    public ArrayList getTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotMergingList = new ArrayList();
        tripNotMergingList = reportDAO.getTripNotMergingList(reportTO, userId);
        return tripNotMergingList;
    }

    public ArrayList getCustomerMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripMergingList = new ArrayList();
        customerTripMergingList = reportDAO.getCustomerMergingList(reportTO, userId);
        return customerTripMergingList;
    }

    public ArrayList getCustomerTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripNotMergingList = new ArrayList();
        customerTripNotMergingList = reportDAO.getCustomerTripNotMergingList(reportTO, userId);
        return customerTripNotMergingList;
    }

    public ArrayList getCustomerLists(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerLists(reportTO, userId);
        return customerList;
    }

    public ArrayList getPopupCustomerMergingProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerMergingProfitReport = new ArrayList();
        popupCustomerMergingProfitReport = reportDAO.getPopupCustomerMergingProfitReportDetails(reportTO);
        return popupCustomerMergingProfitReport;

    }

    public ArrayList getTripExtraExpenseDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getTripExtraExpenseDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }
    public ArrayList getWarehouseDetails(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getWarehouseDetails = new ArrayList();
        getWarehouseDetails = reportDAO.getWarehouseDetails(userId);
        return getWarehouseDetails;

    }

    public ArrayList getVehicleDriverAdvanceDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverAdvanceDetails = new ArrayList();
        vehicleDriverAdvanceDetails = reportDAO.getVehicleDriverAdvanceDetails(reportTO, userId);
        return vehicleDriverAdvanceDetails;

    }

    public ArrayList getVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getTripVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripSheetListWfl(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetListWfl(reportTO);
        return tripSheetList;
    }

    public String getWflHours(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getWflHours(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getLatestUpdates(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList latestUpdates = new ArrayList();
        latestUpdates = reportDAO.getLatestUpdates(reportTO);
        return latestUpdates;
    }

    public ArrayList getVmrList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vmrList = new ArrayList();
        vmrList = reportDAO.getVmrList(reportTO);
        return vmrList;
    }

    public ArrayList getToPayCustomerTripDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList ToPayCustomerTripDetails = new ArrayList();
        ToPayCustomerTripDetails = reportDAO.getToPayCustomerTripDetails(reportTO, userId);
        return ToPayCustomerTripDetails;

    }

    public ArrayList getTripVmrDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripVmrDetails = new ArrayList();
        TripVmrDetails = reportDAO.getTripVmrDetails(reportTO, userId);
        return TripVmrDetails;

    }

    public ArrayList getFcWiseTripBudgetDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getFcWiseTripBudgetDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getAccountMgrPerformanceReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getAccountMgrPerformanceReportDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getRNMExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getTyreExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getRNMVehicleList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getRNMVehicleList(type);
        return vehicleList;

    }

    public ArrayList getVehicleTyreList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleTyreList(type);
        return vehicleList;

    }

    public ArrayList getRNMReportList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList rnmReportList = new ArrayList();
        rnmReportList = reportDAO.getRNMReportList(type);
        return rnmReportList;

    }

    public ArrayList getVehicleUtilizeList(String utilizeMonth, String utilizeYear) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUtilizeList = new ArrayList();
        vehicleUtilizeList = reportDAO.getVehicleUtilizeList(utilizeMonth, utilizeYear);
        return vehicleUtilizeList;

    }

    public ArrayList getTyreAgeingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tyreAgeingList = new ArrayList();
        tyreAgeingList = reportDAO.getTyreAgeingList(reportTO, userId);
        return tyreAgeingList;

    }

    public ArrayList getBrokerCustomerDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList brokerCustomerDetails = new ArrayList();
        brokerCustomerDetails = reportDAO.getBrokerCustomerDetails(reportTO, userId);
        return brokerCustomerDetails;

    }

    public ArrayList getDashboardopsTruckNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTruckNos = new ArrayList();
        getTruckNos = reportDAO.getDashboardopsTruckNos(reportTO);
        return getTruckNos;
    }

    public ArrayList getDashBoardOperationNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTruckNos = new ArrayList();
        getTruckNos = reportDAO.getDashBoardOperationNos(reportTO);
        return getTruckNos;
    }

    public ArrayList getOverAllTripNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTruckNos = new ArrayList();
        getTruckNos = reportDAO.getOverAllTripNos(reportTO);
        return getTruckNos;
    }

    public ArrayList monthNodList(int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList monthNodList = new ArrayList();
        monthNodList = reportDAO.monthNodList(userId);
        return monthNodList;
    }

    public ArrayList getTodTripNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTodTripNos = new ArrayList();
        getTodTripNos = reportDAO.getTodTripNos(reportTO);
        return getTodTripNos;
    }

    public ArrayList getMonTripNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getMonTripNos = new ArrayList();
        getMonTripNos = reportDAO.getMonTripNos(reportTO);
        return getMonTripNos;
    }

    public ArrayList getStatusWiseTripNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getStatusWiseTripNos = new ArrayList();
        getStatusWiseTripNos = reportDAO.getStatusWiseTripNos(reportTO);
        return getStatusWiseTripNos;
    }

    public ArrayList getRouteWiseTripNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getStatusWiseTripNos = new ArrayList();
        getStatusWiseTripNos = reportDAO.getRouteWiseTripNos(reportTO);
        return getStatusWiseTripNos;
    }

    public ArrayList getDashboardWorkShop(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDashboardWorkShop = new ArrayList();
        getDashboardWorkShop = reportDAO.getDashboardWorkShop(reportTO);
        return getDashboardWorkShop;
    }

    public ArrayList getVehicleUT() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.getVehicleUT();
        return vehicleMake;
    }

    public ArrayList dbTrailerMake() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerMake = new ArrayList();
        trailerMake = reportDAO.dbTrailerMake();
        return trailerMake;
    }

    public ArrayList dbTruckType() throws FPRuntimeException, FPBusinessException {
        ArrayList truckType = new ArrayList();
        truckType = reportDAO.dbTruckType();
        return truckType;
    }

    public ArrayList dbTrailerType() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerType = new ArrayList();
        trailerType = reportDAO.dbTrailerType();
        return trailerType;
    }

    public ArrayList getTrailerJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardMTD = new ArrayList();
        jobcardMTD = reportDAO.getTrailerJobCardMTD();
        return jobcardMTD;
    }

    public ArrayList getTruckJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList truckjobcardMTD = new ArrayList();
        truckjobcardMTD = reportDAO.getTruckJobCardMTD();
        return truckjobcardMTD;
    }

    public ArrayList nodReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodList = new ArrayList();
        nodList = reportDAO.nodReport(reportTO);
        return nodList;
    }

    public ArrayList nodReports(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLists = new ArrayList();
        nodLists = reportDAO.nodReports(reportTO);
        return nodLists;
    }

    public ArrayList nodReportLevel2(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel2List = new ArrayList();
        nodLevel2List = reportDAO.nodReportLevel2(reportTO);
        return nodLevel2List;
    }

    public ArrayList nodLevel3List(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel3List = new ArrayList();
        nodLevel3List = reportDAO.nodReportLevel3(reportTO);
        return nodLevel3List;
    }

    public ArrayList getVendorList() throws FPRuntimeException, FPBusinessException {
        ArrayList getVendorList = new ArrayList();
        getVendorList = reportDAO.getVendorList();
        return getVendorList;
    }

    public ArrayList getVendorPerformanceList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList VendorPerformanceList = new ArrayList();
        VendorPerformanceList = reportDAO.getVendorPerformanceList(reportTO, userId);
        return VendorPerformanceList;

    }

    public ArrayList getVendorVehicleList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList VendorPerformanceList = new ArrayList();
        VendorPerformanceList = reportDAO.getVendorVehicleList(reportTO, userId);
        return VendorPerformanceList;
    }

    public ArrayList getOverAllProfitability(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getOverAllProfitability = new ArrayList();
        getOverAllProfitability = reportDAO.getOverAllProfitability(reportTO);
        return getOverAllProfitability;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        ArrayList pendingSMSList = new ArrayList();
        pendingSMSList = reportDAO.getEmailList();
        return pendingSMSList;
    }

    public ArrayList dsrReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList dsrReport = new ArrayList();
        dsrReport = reportDAO.dsrReport(reportTO);
        return dsrReport;
    }

    public String getDailyNodReportAlert(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodList = new ArrayList();
        ArrayList nodLists = new ArrayList();
        reportTO.setUserId(1000);
        String emailFormat1 = "";
        String emailFormat2 = "";
        String emailFormat3 = "";
        String emailFormat4 = "";
        String emailFormat5 = "";
        String emailFormat7 = "";
        String emailFormat8 = "";
        String emailFormat = "";
        Iterator itr1 = null;
        Iterator itr2 = null;
        ReportTO stsTO = null;
        ReportTO stsTO1 = null;
        int zeroDayTotal = 0;
        int oneDayTotal = 0;
        int twoDayTotal = 0;
        int threeDayTotal = 0;
        int fourDayTotal = 0;
        int fiveDayTotal = 0;
        int sixDayTotal = 0;
        int sevenDayTotal = 0;
        int moreDayTotal = 0;

        int jobCardCreated = 0;
        int jobCardPlanned = 0;
        int jobCardClosed = 0;
        int jobCardBilled = 0;

        try {

            nodList = reportDAO.nodReport(reportTO);
            nodLists = reportDAO.nodReports(reportTO);

            emailFormat1 = " <html>"
                    + "<body>"
                    + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below NOD Report <br><br/>"
                    + "<body><table border='1' style='font-style: italic;' align='center' width='80%' cellpadding='0' cellspacing='0'>"
                    + "<tr>"
                    + "<th>S.No</th><th>Status Name</th><th>0 Day</th><th>1 Day</th><th>2 Day</th><th>3 Day</th><th>4 Day</th><th>5 Day</th><th>6 Day</th><th>7 Day</th><th> >7 Day</th><th>Total</th>"
                    + "</tr>";

            itr1 = nodList.iterator();
            itr2 = nodLists.iterator();
            int i = 2;
            String totalTD = "";
            String tr = "";

            while (itr2.hasNext()) {

                stsTO1 = new ReportTO();
                stsTO1 = (ReportTO) itr2.next();
                if (stsTO1.getZeroDay() != null && !"".equals(stsTO1.getZeroDay())) {
                    zeroDayTotal += Integer.parseInt((String) stsTO1.getZeroDay());
                }
                if (stsTO1.getOneDay() != null && !"".equals(stsTO1.getOneDay())) {
                    oneDayTotal += Integer.parseInt((String) stsTO1.getOneDay());
                }
                if (stsTO1.getTwoDay() != null && !"".equals(stsTO1.getTwoDay())) {
                    twoDayTotal += Integer.parseInt((String) stsTO1.getTwoDay());
                }
                if (stsTO1.getThreeDay() != null && !"".equals(stsTO1.getThreeDay())) {
                    threeDayTotal += Integer.parseInt((String) stsTO1.getThreeDay());
                }
                if (stsTO1.getFourDay() != null && !"".equals(stsTO1.getFourDay())) {
                    fourDayTotal += Integer.parseInt((String) stsTO1.getFourDay());
                }
                if (stsTO1.getFiveDay() != null && !"".equals(stsTO1.getFiveDay())) {
                    fiveDayTotal += Integer.parseInt((String) stsTO1.getFiveDay());
                }
                if (stsTO1.getSixDay() != null && !"".equals(stsTO1.getSixDay())) {
                    sixDayTotal += Integer.parseInt((String) stsTO1.getSixDay());
                }
                if (stsTO1.getSevenDay() != null && !"".equals(stsTO1.getSevenDay())) {
                    sevenDayTotal += Integer.parseInt((String) stsTO1.getSevenDay());
                }
                if (stsTO1.getMoreThanSevenDay() != null && !"".equals(stsTO1.getMoreThanSevenDay())) {
                    moreDayTotal += Integer.parseInt((String) stsTO1.getMoreThanSevenDay());
                }

                emailFormat4 = "<tr>";
                emailFormat5 = "</tr>";
                if (stsTO1.getStatusName().equals("Order Created")) {
                    emailFormat2 = "<td>1</td><td>Order Created</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO1.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO1.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO1.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO1.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO1.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO1.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO1.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO1.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO1.getMoreThanSevenDay() + "</font></td>";
                    jobCardCreated = jobCardCreated + Integer.parseInt(stsTO1.getZeroDay()) + Integer.parseInt(stsTO1.getOneDay()) + Integer.parseInt(stsTO1.getTwoDay()) + Integer.parseInt(stsTO1.getThreeDay()) + Integer.parseInt(stsTO1.getFourDay()) + Integer.parseInt(stsTO1.getFiveDay()) + Integer.parseInt(stsTO1.getSixDay()) + Integer.parseInt(stsTO1.getSevenDay()) + Integer.parseInt(stsTO1.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardCreated + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                }

            }

            while (itr1.hasNext()) {

                stsTO = new ReportTO();
                stsTO = (ReportTO) itr1.next();
                if (stsTO.getZeroDay() != null && !"".equals(stsTO.getZeroDay())) {
                    zeroDayTotal += Integer.parseInt((String) stsTO.getZeroDay());
                }
                if (stsTO.getOneDay() != null && !"".equals(stsTO.getOneDay())) {
                    oneDayTotal += Integer.parseInt((String) stsTO.getOneDay());
                }
                if (stsTO.getTwoDay() != null && !"".equals(stsTO.getTwoDay())) {
                    twoDayTotal += Integer.parseInt((String) stsTO.getTwoDay());
                }
                if (stsTO.getThreeDay() != null && !"".equals(stsTO.getThreeDay())) {
                    threeDayTotal += Integer.parseInt((String) stsTO.getThreeDay());
                }
                if (stsTO.getFourDay() != null && !"".equals(stsTO.getFourDay())) {
                    fourDayTotal += Integer.parseInt((String) stsTO.getFourDay());
                }
                if (stsTO.getFiveDay() != null && !"".equals(stsTO.getFiveDay())) {
                    fiveDayTotal += Integer.parseInt((String) stsTO.getFiveDay());
                }
                if (stsTO.getSixDay() != null && !"".equals(stsTO.getSixDay())) {
                    sixDayTotal += Integer.parseInt((String) stsTO.getSixDay());
                }
                if (stsTO.getSevenDay() != null && !"".equals(stsTO.getSevenDay())) {
                    sevenDayTotal += Integer.parseInt((String) stsTO.getSevenDay());
                }
                if (stsTO.getMoreThanSevenDay() != null && !"".equals(stsTO.getMoreThanSevenDay())) {
                    moreDayTotal += Integer.parseInt((String) stsTO.getMoreThanSevenDay());
                }

                emailFormat4 = "<tr>";
                emailFormat5 = "</tr>";
                System.out.println(" Here for status id= " + stsTO.getStatusName());

                if (stsTO.getStatusName().equals("Trip Planned")) {
                    emailFormat2 = "<td>" + i + "</td><td>" + stsTO.getStatusName() + "</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO.getMoreThanSevenDay() + "</font></td>";
                    jobCardPlanned = jobCardPlanned + Integer.parseInt(stsTO.getZeroDay()) + Integer.parseInt(stsTO.getOneDay()) + Integer.parseInt(stsTO.getTwoDay()) + Integer.parseInt(stsTO.getThreeDay()) + Integer.parseInt(stsTO.getFourDay()) + Integer.parseInt(stsTO.getFiveDay()) + Integer.parseInt(stsTO.getSixDay()) + Integer.parseInt(stsTO.getSevenDay()) + Integer.parseInt(stsTO.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardPlanned + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                } else if (stsTO.getStatusName().equals("Trip Started / Trip In Progress")) {
                    emailFormat2 = "<td>" + i + "</td><td>" + stsTO.getStatusName() + "</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO.getMoreThanSevenDay() + "</font></td>";
                    jobCardClosed = jobCardClosed + Integer.parseInt(stsTO.getZeroDay()) + Integer.parseInt(stsTO.getOneDay()) + Integer.parseInt(stsTO.getTwoDay()) + Integer.parseInt(stsTO.getThreeDay()) + Integer.parseInt(stsTO.getFourDay()) + Integer.parseInt(stsTO.getFiveDay()) + Integer.parseInt(stsTO.getSixDay()) + Integer.parseInt(stsTO.getSevenDay()) + Integer.parseInt(stsTO.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardClosed + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                } else if (stsTO.getStatusName().equals("Trip End")) {
                    emailFormat2 = "<td>" + i + "</td><td>" + stsTO.getStatusName() + "</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO.getMoreThanSevenDay() + "</font></td>";
                    jobCardBilled = jobCardBilled + Integer.parseInt(stsTO.getZeroDay()) + Integer.parseInt(stsTO.getOneDay()) + Integer.parseInt(stsTO.getTwoDay()) + Integer.parseInt(stsTO.getThreeDay()) + Integer.parseInt(stsTO.getFourDay()) + Integer.parseInt(stsTO.getFiveDay()) + Integer.parseInt(stsTO.getSixDay()) + Integer.parseInt(stsTO.getSevenDay()) + Integer.parseInt(stsTO.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardBilled + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                } else if (stsTO.getStatusName().equals("Trip Closure")) {
                    emailFormat2 = "<td>" + i + "</td><td>" + stsTO.getStatusName() + "</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO.getMoreThanSevenDay() + "</font></td>";
                    jobCardBilled = jobCardBilled + Integer.parseInt(stsTO.getZeroDay()) + Integer.parseInt(stsTO.getOneDay()) + Integer.parseInt(stsTO.getTwoDay()) + Integer.parseInt(stsTO.getThreeDay()) + Integer.parseInt(stsTO.getFourDay()) + Integer.parseInt(stsTO.getFiveDay()) + Integer.parseInt(stsTO.getSixDay()) + Integer.parseInt(stsTO.getSevenDay()) + Integer.parseInt(stsTO.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardBilled + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                } else if (stsTO.getStatusName().equals("Bill Created")) {
                    emailFormat2 = "<td>" + i + "</td><td>" + stsTO.getStatusName() + "</td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + stsTO.getZeroDay() + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + stsTO.getOneDay() + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + stsTO.getTwoDay() + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + stsTO.getThreeDay() + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + stsTO.getFourDay() + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + stsTO.getFiveDay() + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + stsTO.getSixDay() + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + stsTO.getSevenDay() + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + stsTO.getMoreThanSevenDay() + "</font></td>";
                    jobCardBilled = jobCardBilled + Integer.parseInt(stsTO.getZeroDay()) + Integer.parseInt(stsTO.getOneDay()) + Integer.parseInt(stsTO.getTwoDay()) + Integer.parseInt(stsTO.getThreeDay()) + Integer.parseInt(stsTO.getFourDay()) + Integer.parseInt(stsTO.getFiveDay()) + Integer.parseInt(stsTO.getSixDay()) + Integer.parseInt(stsTO.getSevenDay()) + Integer.parseInt(stsTO.getMoreThanSevenDay());
                    totalTD = "<td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + jobCardBilled + "</td>";
                    tr += emailFormat4 + emailFormat2 + totalTD + emailFormat5;
                }

                i = i + 1;
            }

            String finalTR = "<tr><td style='text-align: center'><font color='#050404'>&nbsp;</font></td><td style='text-align: center'><font color='#050404'>Total</font></td><td bgcolor='#11d8c7' style='text-align: center'><font color='#050404'>" + zeroDayTotal + "</font></td><td bgcolor='#15c6b7' style='text-align: center'><font color='#050404'>" + oneDayTotal + "</font></td><td bgcolor='#18baac' style='text-align: center'><font color='#050404'>" + twoDayTotal + "</font></td><td bgcolor='#f9f900' style='text-align: center'><font color='#050404'>" + threeDayTotal + "</font></td><td bgcolor='#dbdb0f' style='text-align: center'><font color='#050404'>" + fourDayTotal + "</font></td><td bgcolor='#afaf18' style='text-align: center'><font color='#050404'>" + fiveDayTotal + "</font></td><td bgcolor='#f91702' style='text-align: center'><font color='#050404'>" + sixDayTotal + "</font></td><td bgcolor='#d31f0e' style='text-align: center'><font color='#050404'>" + sevenDayTotal + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + moreDayTotal + "</font></td><td bgcolor='#aa271b' style='text-align: center'><font color='#050404'>" + (zeroDayTotal + oneDayTotal + twoDayTotal + threeDayTotal + fourDayTotal + fiveDayTotal + sixDayTotal + sevenDayTotal + moreDayTotal) + "</font></td></tr>";

            emailFormat2 = tr + "" + finalTR + "</body></html>";
            emailFormat3 = "<br><br><br>"
                    + "<html>"
                    + "<body>"
                    + "<table>"
                    + "<tr>"
                    + "<br><br><br>Regards,"
                    + "<br><br/>Kerry Indev PVT.LTD <br/><br/>This is an Auto Generated Email Please Do Not Reply"
                    + " .</p>"
                    + "</tr>"
                    + "</table>"
                    + "</body></html>";
            emailFormat = emailFormat1 + "" + emailFormat2 + "" + emailFormat3 + "";
            System.out.println("emailFormat = " + emailFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            nodList = null;
            emailFormat2 = null;
            emailFormat3 = null;
            emailFormat1 = null;
            itr1 = null;
            stsTO = null;
        }
        return emailFormat;
    }

    public int insertMailDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertMailDetails = 0;
        insertMailDetails = reportDAO.insertMailDetails(reportTO, userId);
        return insertMailDetails;
    }

    public String getCustomerDSRReportAlert(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";

        String startDate = "";
        String[] temp = null;
        if (!"".equals(systemTime)) {
            temp = systemTime.split("-");
            startDate = "1" + "-" + temp[1] + "-" + temp[2];
        }
        if ("".equals(fromDate)) {
            fromDate = startDate;
            reportTO1.setFromDate(fromDate);
        }
        if ("".equals(endDate)) {
            endDate = systemTime;
            reportTO1.setToDate(endDate);
        }

        try {

            String customerName = reportTO1.getCustomerName();
            String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
            String filename = name;
            String sheetName = "";
            HSSFWorkbook my_workbook = new HSSFWorkbook();

            boolean check = false;
            String filepath = "";
            ArrayList custDSROrigin = new ArrayList();
            custDSROrigin = reportDAO.custDSROrigin(reportTO1);
            Iterator itr1 = custDSROrigin.iterator();
            ReportTO repTO = new ReportTO();

            if (custDSROrigin.size() > 0) {
                File theDir = new File(ThrottleConstants.customerDSRReportPath);
                if (!theDir.exists()) {
                    boolean result = theDir.mkdir();
                    if (result) {
                        System.out.println(ThrottleConstants.customerDSRReportPath + " Created");
                    }
                }

                filepath = ThrottleConstants.customerDSRReportPath + name;
                System.out.println("filename = " + filepath);
            }

            while (itr1.hasNext()) {

                repTO = (ReportTO) itr1.next();
                String originId = repTO.getOrigin();
                sheetName = repTO.getCityName();
                reportTO1.setOrigin(originId);

                ArrayList customerDSRReport = new ArrayList();
                customerDSRReport = reportDAO.dsrReportMail(reportTO1);
                Iterator itr = customerDSRReport.iterator();

                HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                style.setBorderTop((short) 1);

                HSSFRow header = my_sheet.createRow(0);
                header.setHeightInPoints(20); // row hight

                HSSFCell cell1 = header.createCell((short) 0);
                style.setWrapText(true);
                cell1.setCellValue("S.No");
                cell1.setCellStyle(style);
                my_sheet.setColumnWidth((short) 0, (short) 4000);

                HSSFCell cell2 = header.createCell((short) 1);
                cell2.setCellValue("Transport type");
                cell2.setCellStyle(style);
                my_sheet.setColumnWidth((short) 1, (short) 6000);

                HSSFCell cell3 = header.createCell((short) 2);
                cell3.setCellValue("Invoice");
                cell3.setCellStyle(style);
                my_sheet.setColumnWidth((short) 2, (short) 6000);
                HSSFCell cell4 = header.createCell((short) 3);
                cell4.setCellValue("Carrier Name");
                cell4.setCellStyle(style);
                my_sheet.setColumnWidth((short) 3, (short) 6000);

                HSSFCell cell5 = header.createCell((short) 4);
                cell5.setCellValue("Docket/LR/AWB");
                cell5.setCellStyle(style);
                my_sheet.setColumnWidth((short) 4, (short) 6000);

                HSSFCell cell6 = header.createCell((short) 5);
                cell6.setCellValue("Second Leg Docket");
                cell6.setCellStyle(style);
                my_sheet.setColumnWidth((short) 5, (short) 6000);

                HSSFCell cell7 = header.createCell((short) 6);
                cell7.setCellValue("Second Leg Invoice");
                cell7.setCellStyle(style);
                my_sheet.setColumnWidth((short) 6, (short) 6000);

                HSSFCell cell8 = header.createCell((short) 7);
                cell8.setCellValue("MOT");
                cell8.setCellStyle(style);
                my_sheet.setColumnWidth((short) 7, (short) 6000);

                HSSFCell cell9 = header.createCell((short) 8);
                cell9.setCellValue("Shipping warehouse");
                cell9.setCellStyle(style);
                my_sheet.setColumnWidth((short) 8, (short) 6000);

                HSSFCell cell10 = header.createCell((short) 9);
                cell10.setCellValue("Destination City");
                cell10.setCellStyle(style);
                my_sheet.setColumnWidth((short) 9, (short) 4000);

                HSSFCell cell11 = header.createCell((short) 10);
                cell11.setCellValue("Qty");
                cell11.setCellStyle(style);
                my_sheet.setColumnWidth((short) 10, (short) 4000);

                HSSFCell cell12 = header.createCell((short) 11);
                cell12.setCellValue("No of Boxes");
                cell12.setCellStyle(style);
                my_sheet.setColumnWidth((short) 11, (short) 6000);

                HSSFCell cell13 = header.createCell((short) 12);
                cell13.setCellValue("No of Pallets");
                cell13.setCellStyle(style);
                my_sheet.setColumnWidth((short) 12, (short) 6000);

                HSSFCell cell14 = header.createCell((short) 13);
                cell14.setCellValue("Actual weight in kg");
                cell14.setCellStyle(style);
                my_sheet.setColumnWidth((short) 13, (short) 6000);

                HSSFCell cell15 = header.createCell((short) 14);
                cell15.setCellValue("Type of Vehicle");
                cell15.setCellStyle(style);
                my_sheet.setColumnWidth((short) 14, (short) 6000);

                HSSFCell cell16 = header.createCell((short) 15);
                cell16.setCellValue("Vehicle Reg");
                cell16.setCellStyle(style);
                my_sheet.setColumnWidth((short) 15, (short) 6000);

                HSSFCell cell17 = header.createCell((short) 16);
                cell17.setCellValue("Pickup Date");
                cell17.setCellStyle(style);
                my_sheet.setColumnWidth((short) 16, (short) 6000);

                HSSFCell cell18 = header.createCell((short) 17);
                cell18.setCellValue("Pickup Time");
                cell18.setCellStyle(style);
                my_sheet.setColumnWidth((short) 17, (short) 6000);

                HSSFCell cell19 = header.createCell((short) 18);
                cell19.setCellValue("Permit");
                cell19.setCellStyle(style);
                my_sheet.setColumnWidth((short) 18, (short) 6000);

                HSSFCell cell20 = header.createCell((short) 19);
                cell20.setCellValue("Customer Name");
                cell20.setCellStyle(style);
                my_sheet.setColumnWidth((short) 19, (short) 6000);

                HSSFCell cell21 = header.createCell((short) 20);
                cell21.setCellValue("End Customer Name");
                cell21.setCellStyle(style);
                my_sheet.setColumnWidth((short) 20, (short) 6000);

                HSSFCell cell22 = header.createCell((short) 21);
                cell22.setCellValue("EDD");
                cell22.setCellStyle(style);
                my_sheet.setColumnWidth((short) 21, (short) 6000);

                HSSFCell cell23 = header.createCell((short) 22);
                cell23.setCellValue("Vehicle Arrival Date");
                cell23.setCellStyle(style);
                my_sheet.setColumnWidth((short) 22, (short) 6000);

                HSSFCell cell24 = header.createCell((short) 23);
                cell24.setCellValue("Actual Delivered Date");
                cell24.setCellStyle(style);
                my_sheet.setColumnWidth((short) 23, (short) 6000);

                HSSFCell cell25 = header.createCell((short) 24);
                cell25.setCellValue("Delivered Time");
                cell25.setCellStyle(style);
                my_sheet.setColumnWidth((short) 24, (short) 6000);

                HSSFCell cell26 = header.createCell((short) 25);
                cell26.setCellValue("Status");
                cell26.setCellStyle(style);
                my_sheet.setColumnWidth((short) 25, (short) 6000);

                HSSFCell cell27 = header.createCell((short) 26);
                cell27.setCellValue("Vehicle Remarks");
                cell27.setCellStyle(style);
                my_sheet.setColumnWidth((short) 26, (short) 6000);

                HSSFCell cell28 = header.createCell((short) 27);
                cell28.setCellValue("Vehicle Running Status with Location");
                cell28.setCellStyle(style);
                my_sheet.setColumnWidth((short) 27, (short) 6000);

                HSSFCell cell29 = header.createCell((short) 28);
                cell29.setCellValue("Total Distance (KMS)");
                cell29.setCellStyle(style);
                my_sheet.setColumnWidth((short) 28, (short) 6000);

                HSSFCell cell30 = header.createCell((short) 29);
                cell30.setCellValue("Distance Covered");
                cell30.setCellStyle(style);
                my_sheet.setColumnWidth((short) 29, (short) 6000);

                HSSFCell cell31 = header.createCell((short) 30);
                cell31.setCellValue("Balance KMS");
                cell31.setCellStyle(style);
                my_sheet.setColumnWidth((short) 30, (short) 6000);

                HSSFCell cell32 = header.createCell((short) 31);
                cell32.setCellValue("Origin to Destination");
                cell32.setCellStyle(style);
                my_sheet.setColumnWidth((short) 31, (short) 6000);

                HSSFCell cell33 = header.createCell((short) 32);
                cell33.setCellValue("TAT");
                cell33.setCellStyle(style);
                my_sheet.setColumnWidth((short) 32, (short) 6000);

                HSSFCell cell34 = header.createCell((short) 33);
                cell34.setCellValue("Agreed TAT In Hours");
                cell34.setCellStyle(style);
                my_sheet.setColumnWidth((short) 33, (short) 6000);

                HSSFCell cell35 = header.createCell((short) 34);
                cell35.setCellValue("Loading Detention Days");
                cell35.setCellStyle(style);
                my_sheet.setColumnWidth((short) 34, (short) 6000);

                HSSFCell cell36 = header.createCell((short) 35);
                cell36.setCellValue("Unloading detention Days");
                cell36.setCellStyle(style);
                my_sheet.setColumnWidth((short) 35, (short) 6000);

                HSSFCell cell37 = header.createCell((short) 36);
                cell37.setCellValue("Delay/ On time");
                cell37.setCellStyle(style);
                my_sheet.setColumnWidth((short) 36, (short) 6000);

                HSSFCell cell38 = header.createCell((short) 37);
                cell38.setCellValue("Business Impact Yes/ No");
                cell38.setCellStyle(style);
                my_sheet.setColumnWidth((short) 37, (short) 6000);

                HSSFCell cell39 = header.createCell((short) 38);
                cell39.setCellValue("Delay Reason In details ");
                cell39.setCellStyle(style);
                my_sheet.setColumnWidth((short) 38, (short) 6000);

                String status = "";
                int cntr = 1;

                ReportTO reportTO = new ReportTO();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    check = true;
                    HSSFRow row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue(cntr);
                    row.createCell((short) 1).setCellValue(reportTO.getOrderType());
                    row.createCell((short) 2).setCellValue(reportTO.getInvoiceNo());
                    row.createCell((short) 3).setCellValue(reportTO.getCompanyName());
                    row.createCell((short) 4).setCellValue(reportTO.getDocketNo());
                    row.createCell((short) 5).setCellValue(reportTO.getSecondaryDocketNo());
                    row.createCell((short) 6).setCellValue(reportTO.getSecondaryInvoice());
                    row.createCell((short) 7).setCellValue(reportTO.getTripmode());
                    row.createCell((short) 8).setCellValue(reportTO.getOrigin());
                    row.createCell((short) 9).setCellValue(reportTO.getDestination());
                    row.createCell((short) 10).setCellValue(reportTO.getQuantity());
                    row.createCell((short) 11).setCellValue(reportTO.getBoxes());
                    row.createCell((short) 12).setCellValue(reportTO.getTotalPackages());
                    row.createCell((short) 13).setCellValue(reportTO.getInvoiceWeight());
                    row.createCell((short) 14).setCellValue(reportTO.getVehicleType());
                    row.createCell((short) 15).setCellValue(reportTO.getRegNo());
                    row.createCell((short) 16).setCellValue(reportTO.getTripStartDate());
                    row.createCell((short) 17).setCellValue(reportTO.getTripstarttime());
                    row.createCell((short) 18).setCellValue(reportTO.geteWayBillNo());
                    row.createCell((short) 19).setCellValue(reportTO.getConsignorName());
                    row.createCell((short) 20).setCellValue(reportTO.getConsigneeName());
                    row.createCell((short) 21).setCellValue(reportTO.getPlaneDate());
                    row.createCell((short) 22).setCellValue(reportTO.getDestinationReportingDateTime());
                    row.createCell((short) 23).setCellValue(reportTO.getTripEndTime());
                    row.createCell((short) 24).setCellValue(reportTO.getActualEndDateTime());

                    int statusVal = 0;
                    statusVal = Integer.parseInt(reportTO.getStatus());

                    if (statusVal == 8) {
                        status = "Yet to Start";
                    } else if (statusVal == 10) {
                        status = "In-Transit";
                    } else if (statusVal > 10) {
                        status = "Delivered";
                    }

                    row.createCell((short) 25).setCellValue(status);
                    row.createCell((short) 26).setCellValue(reportTO.getRemarks());
                    row.createCell((short) 27).setCellValue(reportTO.getLocation());

                    row.createCell((short) 28).setCellValue(reportTO.getStartKM());
                    row.createCell((short) 29).setCellValue(reportTO.getDistanceCovered());
                    row.createCell((short) 30).setCellValue(reportTO.getDistanceRemaining());
                    row.createCell((short) 31).setCellValue(reportTO.getOrigin() + "" + reportTO.getDestination());
                    row.createCell((short) 32).setCellValue(reportTO.getExpectedArrivalDateTime());

                    int tat = Integer.parseInt(reportTO.getExpectedArrivalDateTime());
                    int tatTime = tat * 24;

                    row.createCell((short) 33).setCellValue(tatTime);
                    row.createCell((short) 34).setCellValue("");
                    row.createCell((short) 35).setCellValue("");
                    row.createCell((short) 36).setCellValue(reportTO.getDelayReason());
                    row.createCell((short) 37).setCellValue("");
                    row.createCell((short) 38).setCellValue("");

                    cntr++;

                }
            }

            System.out.println("Your Mail excel Sheet  created");

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                String emailFormat1 = "";
                String emailFormat2 = "";
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p style='font-style: italic;'><b>Dear " + reportTO1.getCustomerName() + ", <br><br>Please find your DSR Report attached, <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>Kerry Indev<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + filename + "~" + filepath;

                if (!check) {
                    emailFormat = "";
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        return emailFormat;
    }
    public String dailyMailReport(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";

        String startDate = "";
        String[] temp = null;
        if (!"".equals(systemTime)) {
            temp = systemTime.split("-");
            startDate = "1" + "-" + temp[1] + "-" + temp[2];
        }
        if ("".equals(fromDate)) {
            fromDate = startDate;
            reportTO1.setFromDate(fromDate);
        }
        if ("".equals(endDate)) {
            endDate = systemTime;
            reportTO1.setToDate(endDate);
        }

        try {

            String whId = reportTO1.getWhId();
            String whName = reportTO1.getWhName();
            String name = whId + "_(" + endDate + ").xls";
            String filename = name;
            String sheetName = "";
            HSSFWorkbook my_workbook = new HSSFWorkbook();

            boolean check = false;
            String filepath = "";
            ArrayList dailyMailReport = new ArrayList();
            dailyMailReport = reportDAO.reportForMail(whId);
            Iterator itr1 = dailyMailReport.iterator();
            ReportTO repTO = new ReportTO();

            if (dailyMailReport.size() > 0) {
                File theDir = new File(ThrottleConstants.customerDSRReportPath);
                if (!theDir.exists()) {
                    boolean result = theDir.mkdir();
                    if (result) {
                        System.out.println(ThrottleConstants.customerDSRReportPath + " Created");
                    }
                }

                filepath = ThrottleConstants.customerDSRReportPath + name;
                System.out.println("filename = " + filepath);
            }
                sheetName=whName;
                System.out.println("sheet Name   "+sheetName);
                HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                style.setBorderTop((short) 1);
                style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

                HSSFRow header = my_sheet.createRow(0);
                header.setHeightInPoints(20); // row hight

                HSSFCell cell1 = header.createCell((short) 0);
                style.setWrapText(true);
                cell1.setCellValue("S.No");
                cell1.setCellStyle(style);
                my_sheet.setColumnWidth((short) 0, (short) 4000);

                HSSFCell cell2 = header.createCell((short) 1);
                style.setWrapText(true);
                cell2.setCellValue("Status Name");
                cell2.setCellStyle(style);
                my_sheet.setColumnWidth((short) 1, (short) 4000);

                HSSFCell cell3 = header.createCell((short) 2);
                cell3.setCellValue("Previous Day Hub Report");
                cell3.setCellStyle(style);
                my_sheet.setColumnWidth((short) 2, (short) 6000);

                HSSFCell cell4 = header.createCell((short) 3);
                cell4.setCellValue("Month Hub Report");
                cell4.setCellStyle(style);
                my_sheet.setColumnWidth((short) 3, (short) 6000);
                
                HSSFCell cell5 = header.createCell((short) 4);
                cell5.setCellValue("Previous Day State Report");
                cell5.setCellStyle(style);
                my_sheet.setColumnWidth((short) 4, (short) 6000);
                
                HSSFCell cell6 = header.createCell((short) 5);
                cell6.setCellValue("Month State Report");
                cell6.setCellStyle(style);
                my_sheet.setColumnWidth((short) 6, (short) 6000);
                int cntr=1;
                
            while (itr1.hasNext()) {

                repTO = (ReportTO) itr1.next();
                String status = "";
                    check = true;
                    HSSFRow row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue(cntr);
                    row.createCell((short) 1).setCellValue(repTO.getStatusName());
                    row.createCell((short) 2).setCellValue(repTO.getDayCount());
                    row.createCell((short) 3).setCellValue(repTO.getMonthCount());
                    row.createCell((short) 4).setCellValue(repTO.getStateDayCount());
                    row.createCell((short) 5).setCellValue(repTO.getStateMonthCount());
                   

                    cntr++;

                }

            System.out.println("Your Mail excel Sheet  created");

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                String emailFormat1 = "";
//                String emailFormat2 = "";
                emailFormat1 =reportDAO.getMailTemplate("33");
//                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat1 + "~" + filename + "~" + filepath;

                if (!check) {
                    emailFormat = "";
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        return emailFormat;
    }
    
    public String getTripPODReportAlert(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";

        String startDate = "";
        String[] temp = null;
        if (!"".equals(systemTime)) {
            temp = systemTime.split("-");
            startDate = "1" + "-" + temp[1] + "-" + temp[2];
        }
        if ("".equals(fromDate)) {
            fromDate = startDate;
            reportTO1.setFromDate(fromDate);
        }
        if ("".equals(endDate)) {
            endDate = systemTime;
            reportTO1.setToDate(endDate);
        }

        try {

            File theDir = new File(ThrottleConstants.customerDSRReportPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.customerDSRReportPath + " Created");
                }
            }

            String customerName = reportTO1.getCustomerName();
            boolean check = false;
            ArrayList tripPODReportAlert = new ArrayList();
            tripPODReportAlert = reportDAO.getTripPODReportAlert();
            Iterator itr = tripPODReportAlert.iterator();

            String name = "Trip_POD_Alert_" + endDate + ".xls";
            String filename = name;
            String filepath = ThrottleConstants.customerDSRReportPath + name;
            System.out.println("filename = " + filepath);

            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "Trip POD Report";

            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderTop((short) 1);

            HSSFRow header = my_sheet.createRow(0);
            header.setHeightInPoints(20); // row hight

            HSSFCell cell1 = header.createCell((short) 0);
            style.setWrapText(true);
            cell1.setCellValue("S.No");
            cell1.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);

            HSSFCell cell2 = header.createCell((short) 1);
            cell2.setCellValue("Customer Name");
            cell2.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cell3 = header.createCell((short) 2);
            cell3.setCellValue("Consignor Name");
            cell3.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 6000);
            HSSFCell cell4 = header.createCell((short) 3);
            cell4.setCellValue("Consignee Name");
            cell4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 6000);

            HSSFCell cell5 = header.createCell((short) 4);
            cell5.setCellValue("E-waybill No");
            cell5.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 6000);

            HSSFCell cell6 = header.createCell((short) 5);
            cell6.setCellValue("Docket No");
            cell6.setCellStyle(style);
            my_sheet.setColumnWidth((short) 5, (short) 6000);

            HSSFCell cell7 = header.createCell((short) 6);
            cell7.setCellValue("Origin");
            cell7.setCellStyle(style);
            my_sheet.setColumnWidth((short) 6, (short) 6000);

            HSSFCell cell8 = header.createCell((short) 7);
            cell8.setCellValue("Destination");
            cell8.setCellStyle(style);
            my_sheet.setColumnWidth((short) 7, (short) 6000);

            HSSFCell cell9 = header.createCell((short) 8);
            cell9.setCellValue("Total Packages");
            cell9.setCellStyle(style);
            my_sheet.setColumnWidth((short) 8, (short) 6000);

            HSSFCell cell10 = header.createCell((short) 9);
            cell10.setCellValue("Weight");
            cell10.setCellStyle(style);
            my_sheet.setColumnWidth((short) 9, (short) 4000);

            HSSFCell cell11 = header.createCell((short) 10);
            cell11.setCellValue("Vehicle Type");
            cell11.setCellStyle(style);
            my_sheet.setColumnWidth((short) 10, (short) 4000);

            HSSFCell cell12 = header.createCell((short) 11);
            cell12.setCellValue("Vehicle Reg No");
            cell12.setCellStyle(style);
            my_sheet.setColumnWidth((short) 11, (short) 6000);

            HSSFCell cell13 = header.createCell((short) 12);
            cell13.setCellValue("Invoice No");
            cell13.setCellStyle(style);
            my_sheet.setColumnWidth((short) 12, (short) 6000);

            HSSFCell cell14 = header.createCell((short) 13);
            cell14.setCellValue("Trip Start Date");
            cell14.setCellStyle(style);
            my_sheet.setColumnWidth((short) 13, (short) 6000);

            HSSFCell cell15 = header.createCell((short) 14);
            cell15.setCellValue("Destination Report Date");
            cell15.setCellStyle(style);
            my_sheet.setColumnWidth((short) 14, (short) 6000);

            HSSFCell cell16 = header.createCell((short) 15);
            cell16.setCellValue("End Date");
            cell16.setCellStyle(style);
            my_sheet.setColumnWidth((short) 15, (short) 6000);

            String status = "";
            ReportTO reportTO = new ReportTO();
            int cntr = 1;

            while (itr.hasNext()) {
                reportTO = (ReportTO) itr.next();
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);
                row.createCell((short) 0).setCellValue(cntr);
                row.createCell((short) 1).setCellValue(reportTO.getCustomerName());
                row.createCell((short) 2).setCellValue(reportTO.getConsignorName());
                row.createCell((short) 3).setCellValue(reportTO.getConsigneeName());
                row.createCell((short) 4).setCellValue(reportTO.geteWayBillNo());
                row.createCell((short) 5).setCellValue(reportTO.getDocketNo());
                row.createCell((short) 6).setCellValue(reportTO.getOrigin());
                row.createCell((short) 7).setCellValue(reportTO.getDestination());
                row.createCell((short) 8).setCellValue(reportTO.getTotalPackages());
                row.createCell((short) 9).setCellValue(reportTO.getInvoiceWeight());
                row.createCell((short) 10).setCellValue(reportTO.getVehicleType());
                row.createCell((short) 11).setCellValue(reportTO.getRegNo());
                row.createCell((short) 12).setCellValue(reportTO.getInvoiceNo());
                row.createCell((short) 13).setCellValue(reportTO.getTripStartDate());
                row.createCell((short) 14).setCellValue(reportTO.getActualEndDateTime());
                row.createCell((short) 15).setCellValue(reportTO.getDestinationReportingDateTime());
                check = true;
                cntr++;

            }

            System.out.println("Your Mail excel Sheet  created");

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                String emailFormat1 = "";
                String emailFormat2 = "";
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p style='font-style: italic;'><b>Dear Team, <br><br>Please find your Pending Trip POD Report attached, <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>Kerry Indev<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + filename + "~" + filepath;

                if (!check) {
                    emailFormat = "";
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        return emailFormat;
    }

    public ArrayList getEmailCustomerList() throws FPRuntimeException, FPBusinessException {

        ArrayList custList = new ArrayList();
        custList = reportDAO.getEmailCustomerList();
        return custList;
    }
   
    public ArrayList getWarehouseList() throws FPRuntimeException, FPBusinessException {

        ArrayList getWarehouseList = new ArrayList();
        getWarehouseList = reportDAO.getWarehouseList();
        return getWarehouseList;
    }
    
    public String getMailTemplate(String eventId) throws FPRuntimeException, FPBusinessException {
        String mailTemplate = "";
        mailTemplate = reportDAO.getMailTemplate(eventId);
        return mailTemplate;
    }

    public ArrayList vendorOutStandingReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorOutStandingReport = new ArrayList();
        vendorOutStandingReport = reportDAO.vendorOutStandingReport(reportTO);
        return vendorOutStandingReport;
    }

    public ArrayList custommerOutStandingReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList custommerOutStandingReport = new ArrayList();
        custommerOutStandingReport = reportDAO.custommerOutStandingReport(reportTO);
        return custommerOutStandingReport;
    }

    public String getCallConsignment() {
        String getCallConsignment = "";
        getCallConsignment = reportDAO.getCallConsignment();
        return getCallConsignment;
    }

    public ArrayList getCustRevenu(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCustRevenu = new ArrayList();
        getCustRevenu = reportDAO.getCustRevenu(reportTO);
        return getCustRevenu;
    }

    public ArrayList getCustProfit(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getCustProfit = new ArrayList();
        getCustProfit = reportDAO.getCustProfit(reportTO);
        return getCustProfit;
    }

    public ArrayList invoicePostDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceUnpostedReport = new ArrayList();
        invoiceUnpostedReport = reportDAO.invoiceUnpostedReport();
        return invoiceUnpostedReport;
    }

    public ArrayList advanceUnpostedReport() throws FPRuntimeException, FPBusinessException {
        ArrayList advanceUnpostedReport = new ArrayList();
        advanceUnpostedReport = reportDAO.advanceUnpostedReport();
        return advanceUnpostedReport;
    }

    public ArrayList getTripRunsheetList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripRunsheetList = new ArrayList();
        getTripRunsheetList = reportDAO.getTripRunsheetList(reportTO);
        return getTripRunsheetList;
    }

    public ArrayList getMapRunsheetList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getMapRunsheetList = new ArrayList();
        getMapRunsheetList = reportDAO.getMapRunsheetList(reportTO);
        return getMapRunsheetList;
    }

    public int getAPIStatusDetails() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getAPIStatusDetails();

        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();

            String apiId = repTO.getApiId();
            String orderId = repTO.getOrderId();
            String loanProposalID = repTO.getLoanProposalID();
            String deliveryStatus = repTO.getDeliveryStatus();
            String deliveryDate = repTO.getDeliveryDate();
            String typeofPOD = repTO.getTypeofPOD();
            String pOD = repTO.getpOD();
            String receivedBy = repTO.getReceivedBy();
            String geoTag = repTO.getGeoTag();
            String reason = repTO.getReason();
            String driverId = repTO.getDriverID();
            String barcode = repTO.getBarcode();
            String path1 = repTO.getFilePath1();
            String path2 = repTO.getFilePath2();
            String filename1 = repTO.getFilename1();
            String filename2 = repTO.getFilename2();
            String reasonsforNondelivery = repTO.getReasonsforNondelivery();
            String reasonsforMemberRefusal = repTO.getReasonsforMemberRefusal();

            System.out.println("path1 : " + path1);
            System.out.println("name1 : " + filename1);

            System.out.println("path2 : " + path2);
            System.out.println("name2 : " + filename2);

            final String URLLink = ThrottleConstants.statusAPIURL;
            final String user = ThrottleConstants.userHS;
            final String password = ThrottleConstants.passHS;

            URL myurl = new URL(URLLink);
            HttpURLConnection connection = null;
            StringBuffer jsonString = new StringBuffer();
            String line;
            String payload = "[{\"LoanProposalID\":\"" + loanProposalID + "\",\"Reason\":\"" + reasonsforNondelivery + "\","
                    + "\"DriverID\":\"" + driverId + "\",\"Barcode\":\"" + barcode + "\","
                    + "\"DeliveryStatus\":\"" + deliveryStatus + "\",\"DeliveryTime\":\"" + deliveryDate + "\","
                    + "\"TypeofPOD\":\"" + typeofPOD + "\",\"PODNumber\":\"" + pOD + "\","
                    + "\"PODImage\":\"" + path1 + "\",\"PODImageName\":\"" + filename1 + "\","
                    + "\"PODImage2\":\"" + path2 + "\",\"PODImageName2\":\"" + filename2 + "\","
                    + "\"ReceivedBy\":\"" + receivedBy + "\",\"CurrentLocation\":\"" + geoTag + "\""
                    + "}]";

            System.out.println("payload " + payload);

            connection = (HttpURLConnection) myurl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            System.out.println("jsonString :" + jsonString);

            int updat = reportDAO.updatePostedStatusDetail(apiId);

            JSONParser parser = new JSONParser();
            Object obj1 = parser.parse(jsonString.toString());
            org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
            org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

            String code = "";

            try {
                code = (String) jsonobj.get("Code");

                if (code.equals("1")) {
                    System.out.println("Status updated : " + updat);
                } else {
                    System.out.println("Status not updated.");
                }

            } catch (Exception e) {
                //e.printStackTrace();
            }
            code = jsonString.toString();
            int updatLog = reportDAO.updatePostedStatusLog(payload, code, "getAPIStatusDetails");
            System.out.println("Status updatLog" + updatLog);

        }

        return update;
    }

    public int getAPIInvoiceStatus() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        try {
            ReportTO repTO = new ReportTO();
            ArrayList getAPIStatusDetails = new ArrayList();
            getAPIStatusDetails = reportDAO.getAPIInvoiceStatus();

            Iterator itr1 = getAPIStatusDetails.iterator();
            while (itr1.hasNext()) {
                repTO = (ReportTO) itr1.next();

                String apiId = repTO.getApiId();
                String orderId = repTO.getOrderId();
                String loanProposalID = repTO.getLoanProposalID();
                String serialNo = repTO.getSerialNumber();
                String whId = repTO.getWhId();
                String skuCode = repTO.getSkuCode();

                final String URLLink = ThrottleConstants.invAPIURL;

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;
                String payload = ""
                        + "[{\"LoanProposalID\":\"" + loanProposalID + "\",\"SerialNo\":\"" + serialNo + "\","
                        + "\"WarehouseID\":\"" + whId + "\", \"SkuCode\":\"" + skuCode + "\""
                        + "}]";

                //System.out.println("payload " + payload);
                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
                System.out.println("jsonString :" + jsonString);

                int updatLog = reportDAO.updatePostedStatusLog(payload, jsonString.toString(), "InvAPIStatusDetails");
                System.out.println("Status updatLog" + updatLog);

                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

                update = reportDAO.updatePODStatusDetail(apiId);
                long codeval = (long) jsonobj.get("Code");
                System.out.println("code : " + codeval);
                if (codeval == 1) {

                    String barcode = (String) jsonobj.get("Barcode");
                    System.out.println("barcode " + barcode);
                    String InvoiceNo = (String) jsonobj.get("InvoiceNo");
                    System.out.println("InvoiceNo " + InvoiceNo);
                    String InvoicePath = (String) jsonobj.get("InvoicePath");
                    System.out.println("InvoicePath " + InvoicePath);

                    int invupdat = reportDAO.invPostDetail(barcode, InvoiceNo, InvoicePath, orderId, serialNo);
                    System.out.println("Status updated : " + invupdat);

                } else {
                    System.out.println("Status not updated.");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return update;
    }

    public int getOFDAPIDetails() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getOFDAPIDetails();
        String code = "";
        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();

            String apiId = repTO.getApiId();
            String orderId = repTO.getOrderId();
            String DriverID = repTO.getDriverID();
            String Barcode = repTO.getBarcode();
            String LatLong = repTO.getOfd_latlong();

            final String URLLink = ThrottleConstants.ofdAPIURL;

            URL myurl = new URL(URLLink);
            HttpURLConnection connection = null;
            StringBuffer jsonString = new StringBuffer();
            String line;
            String payload = ""
                    + "[{\"DriverID\":\"" + DriverID + "\",\"Barcode\":\"" + Barcode + "\","
                    + "\"LatLong\":\"" + LatLong + "\""
                    + "}]";

            System.out.println("payload " + payload);
            connection = (HttpURLConnection) myurl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            System.out.println("jsonString :" + jsonString);

            try {
                JSONParser parser = new JSONParser();
                Object obj1 = parser.parse(jsonString.toString());
                org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
                org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

                int updat = reportDAO.updateOFDDetail(apiId);

                if (code.equals("1")) {
                    long codeval = (long) jsonobj.get("Code");
                    System.out.println("code : " + codeval);
                    System.out.println("Status updated : " + updat);
                } else {
                    System.out.println("Status not updated.");
                }

                code = jsonString.toString();
                if (!code.equals("")) {
                    int updatLog = reportDAO.updatePostedStatusLog(payload, code, "OFDAPIDetails");
                    System.out.println("Status updatLog" + updatLog);
                }
            } catch (Exception e) {
                //   e.printStackTrace();
            }

        }

        return update;
    }

    public int getStockAPIDetails() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ReportTO repTO1 = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getStockAPIDetails();
        System.out.println("getAPIStatusDetails :" + getAPIStatusDetails.size());
        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();
            String reqId = repTO.getPoReqId();
            String grnId = repTO.getGrnId();
            String qty = repTO.getQuantity();
            String invNo = repTO.getInvoiceNumber();
            String invDate = repTO.getInvoiceDate();
            String ewayBillNo = repTO.getFilePath();
            String itemId = repTO.getItemId();
            String inwardStatus = repTO.getInwardStatus();
            String filePath = repTO.getFilePath();
            System.out.println("invNo :" + invNo);
            ArrayList getSerialNoDetails = new ArrayList();
            getSerialNoDetails = reportDAO.getSerialNoDetails(itemId, grnId);
            System.out.println("getSerialNoDetails :" + getSerialNoDetails.size());
            Iterator itr11 = getSerialNoDetails.iterator();
            String serialNo = "";
            int cnt = 1;
            while (itr11.hasNext()) {
                repTO1 = (ReportTO) itr11.next();
                if (cnt == 1) {
                    serialNo = "\"SerialNo" + cnt + "\": \"" + repTO1.getSerialNo() + "\"";
                } else {
                    serialNo += ", \"SerialNo" + cnt + "\": \"" + repTO1.getSerialNo() + "\"";
                }
                cnt++;
            }

            final String URLLink = ThrottleConstants.stkAPIURL;
            URL myurl = new URL(URLLink);
            HttpURLConnection connection = null;
            StringBuffer jsonString = new StringBuffer();
            String line;
            String payload = "[{"
                    + "\"PoRequestID\": \"" + reqId + "\","
                    + "\"Quantity\": \"" + qty + "\","
                    + "\"InvoiceNumber\": \"" + invNo + "\","
                    + "\"InvoiceDate\": \"" + invDate + "\","
                    + "\"EwayBill\": \"" + ewayBillNo + "\","
                    + "\"UploadDocs\": \"" + filePath + "\","
                    + "\"InwardStatus\": \"" + inwardStatus + "\","
                    + "\"SerialNumber\":{"
                    + serialNo
                    + "}}]";

            System.out.println("payload " + payload);
            connection = (HttpURLConnection) myurl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            System.out.println("jsonString :" + jsonString);

            JSONParser parser = new JSONParser();
            Object obj1 = parser.parse(jsonString.toString());
            org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
            org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

            String code = jsonString.toString();

            try {

                if (!code.equals("")) {

                    int updat = reportDAO.updateGRNDetail(grnId);
                    System.out.println("Status updated : " + updat);

                    int serialupdat = reportDAO.updateGRNSerialDetail(grnId, itemId);
                    System.out.println("Status updated : " + serialupdat);

                    int updatLog = reportDAO.updatePostedStatusLog(payload, code, "STOCKAPIDetails");
                    System.out.println("Status updatLog" + updatLog);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return update;
    }

    public int getAPIPODDetails() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ArrayList getAPIPODDetails = new ArrayList();
        getAPIPODDetails = reportDAO.getAPIPODDetails();
        Iterator itr1 = getAPIPODDetails.iterator();

        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();

            String apiId = repTO.getApiId();
            String orderId = repTO.getOrderId();

            String loanProposalID = repTO.getLoanProposalID();
            System.out.println("loanProposalID : " + loanProposalID);
            String URL = repTO.getPodURL();
            String Text1 = repTO.getText1();
            String Text2 = repTO.getText2();
            String Numeric1 = repTO.getNumeric1();
            String Numeric2 = repTO.getNumeric2();
            String Boolean = repTO.getBooleanFlag();
            String podDate = repTO.getPodDate();

            final String URLLink = ThrottleConstants.podURL;
            final String user = ThrottleConstants.userHS;
            final String password = ThrottleConstants.passHS;

            URL myurl = new URL(URLLink);
            HttpURLConnection connection = null;
            StringBuffer jsonString = new StringBuffer();
            String line;
            String payload = "{\"login\":{\"UserName\":\"" + user + "\",\"Password\":\"" + password + "\"},"
                    + "\"PodData\":[{"
                    + "\"LoanProposalID\": \"" + loanProposalID + "\", "
                    + "\"URL\": \"" + URL + "\", "
                    + "\"Type\": \"POD\", "
                    + "\"Text1\": \"" + Text1 + "\", "
                    + "\"Text2\": \"" + Text2 + "\", "
                    + "\"Numeric1\": \"" + Numeric1 + "\", "
                    + "\"Numeric2\": \"" + Numeric2 + "\", "
                    + "\"Boolean\": \"" + Boolean + "\", "
                    + "\"Date\": \"" + podDate + "\" "
                    + "}]}";
            System.out.println("" + payload);
            connection = (HttpURLConnection) myurl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            System.out.println("jsonString :" + jsonString);

            JSONParser parser = new JSONParser();
            Object obj1 = parser.parse(jsonString.toString());
            org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
            org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

            String code = "";

            try {
                code = (String) jsonobj.get("Code");

                int updat = reportDAO.updatePODStatusDetail(apiId);
                if (code.equals("1")) {
                    System.out.println("Status updated : " + updat);
                } else {
                    System.out.println("Status not updated.");
                }

            } catch (Exception e) {
                //e.printStackTrace();
            }

            code = jsonString.toString();
            int updatLog = reportDAO.updatePostedStatusLog(payload, code, "getAPIPODDetails");
            System.out.println("Status updatLog" + updatLog);
        }

        return update;
    }

    public ArrayList getOrderReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        ArrayList OperationExpensesList = new ArrayList();
        ArrayList tripEarnings = new ArrayList();
        ArrayList tripEarningsList = new ArrayList();
        ReportTO rpTO = new ReportTO();
        tripDetails = reportDAO.getOrderReport(reportTO);

        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
//        ReportTO rpTO = new ReportTO();
        Iterator itr1 = tripDetails.iterator();
        ReportTO repTO1 = new ReportTO();

        return tripDetails;
    }

    public ArrayList enqStatusList(ReportTO report) throws FPRuntimeException, FPBusinessException {
        ArrayList enqStatusList = new ArrayList();
        enqStatusList = reportDAO.enqStatusList(report);
        return enqStatusList;
    }

    public ArrayList getOrderStatustList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripRunsheetList = new ArrayList();
        getTripRunsheetList = reportDAO.getOrderStatustList(reportTO);
        return getTripRunsheetList;
    }

    public ArrayList getStockQtyReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList stockQtyList = new ArrayList();
        stockQtyList = reportDAO.getStockQtyReport(reportTO);
        return stockQtyList;
    }

    public ArrayList getStockQtyDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList stockQtyDetails = new ArrayList();
        stockQtyDetails = reportDAO.getStockQtyDetails(reportTO);
        return stockQtyDetails;
    }

    public int getGRNImageAPICall() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getGRNImageAPICall();

        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();

            String grnId = repTO.getGrnId();
            String filePath = repTO.getFilePath();

            try {

                final String URLLink = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/updateGRNImage";

                String payload = "[{\"folder\":\"GRN\", \"filename\":\"" + filePath + "\" ,\"grnid\":\"0\"}]";
                System.out.println("payload " + payload);

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;

                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
                System.out.println("jsonString :" + jsonString);

                String path = jsonString.toString();

                if (path != null) {

                    int updatLog = reportDAO.updateGrnImageStatus(grnId);
                    System.out.println("Status updatLog" + updatLog);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return update;
    }

    public int getOrderImageAPICall() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getOrderImageAPICall();

        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();

            String grnId = repTO.getGrnId();
            String filePath = repTO.getFilePath();

            try {

                final String URLLink = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/updateGRNImage";

                String payload = "[{\"folder\":\"POD\", \"filename\":\"" + filePath + "\" ,\"grnid\":\"0\"}]";
                System.out.println("payload " + payload);

                URL myurl = new URL(URLLink);
                HttpURLConnection connection = null;
                StringBuffer jsonString = new StringBuffer();
                String line;

                connection = (HttpURLConnection) myurl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                writer.write(payload);
                writer.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    jsonString.append(line);
                }
                br.close();
                connection.disconnect();
                System.out.println("jsonString :" + jsonString);

                String path = jsonString.toString();

                if (path != null) {

                    int updatLog = reportDAO.updateOrderImageStatus(grnId);
                    System.out.println("Status updatLog" + updatLog);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return update;
    }

    public int getOneTimeStockAPIDetails() throws FPRuntimeException, FPBusinessException, ParseException, IOException, org.json.simple.parser.ParseException {

        int update = 0;
        ReportTO repTO = new ReportTO();
        ReportTO repTO1 = new ReportTO();
        ArrayList getAPIStatusDetails = new ArrayList();
        getAPIStatusDetails = reportDAO.getOneTimeStockAPIDetails();
        System.out.println("getAPIStatusDetails :" + getAPIStatusDetails.size());
        Iterator itr1 = getAPIStatusDetails.iterator();
        while (itr1.hasNext()) {
            repTO = (ReportTO) itr1.next();
            String reqId = repTO.getPoReqId();
            String grnId = repTO.getGrnId();
            String qty = repTO.getQuantity();
            String itemId = repTO.getItemId();

            ArrayList getSerialNoDetails = new ArrayList();
            getSerialNoDetails = reportDAO.getSerialNoDetails(itemId, grnId);
            System.out.println("getSerialNoDetails :" + getSerialNoDetails.size());
            Iterator itr11 = getSerialNoDetails.iterator();
            String serialNo = "";
            int cnt = 1;
            while (itr11.hasNext()) {
                repTO1 = (ReportTO) itr11.next();
                if (cnt == 1) {
                    serialNo = "\"SerialNo" + cnt + "\": \"" + repTO1.getSerialNo() + "\"";
                } else {
                    serialNo += ", \"SerialNo" + cnt + "\": \"" + repTO1.getSerialNo() + "\"";
                }
                cnt++;
            }

            final String URLLink = "http://13.234.6.47/72Stock/selvam_api/one_time_stock_api.php";
            URL myurl = new URL(URLLink);
            HttpURLConnection connection = null;
            StringBuffer jsonString = new StringBuffer();
            String line;
            String payload = "[{"
                    + "\"PoRequestID\": \"" + reqId + "\","
                    + "\"SerialNumber\":{"
                    + serialNo
                    + "}}]";

            System.out.println("payload " + payload);
            connection = (HttpURLConnection) myurl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(payload);
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
            connection.disconnect();
            System.out.println("jsonString :" + jsonString);

            JSONParser parser = new JSONParser();
            Object obj1 = parser.parse(jsonString.toString());
            org.json.simple.JSONArray jsonarry = (org.json.simple.JSONArray) obj1;
            org.json.simple.JSONObject jsonobj = (org.json.simple.JSONObject) jsonarry.get(0);

            String code = "";

            try {

                code = jsonString.toString();

                int updat = reportDAO.updateGRNDetail(grnId);
                System.out.println("Status updatLog" + updat);

                int serialupdat = reportDAO.updateGRNSerialDetail(grnId, itemId);
                System.out.println("Status updated : " + serialupdat);

                int updatLog = reportDAO.updatePostedStatusLog(payload, code, "OneTimeSTOCKAPI");
                System.out.println("Status updatLog" + updatLog);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return update;
    }
}
